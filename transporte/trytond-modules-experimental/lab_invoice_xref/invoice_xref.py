# This file is part of lab_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from trytond.pyson import PYSONEncoder

class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls.company.domain = []

class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()
        cls.company.domain = []

    
class InvoiceXrefLine(ModelSQL, ModelView):
    "InvoiceXrefLine"

    __name__ = 'lab_invoice_xref.invoice_xref.line'

    company = fields.Many2One('company.company', 'Company', required=True)
    name = fields.Char('Name')
    party = fields.Many2One('party.party', 'Party', required=True)
    price = fields.Integer('Price', required=True)
    invoice = fields.Many2One('lab_invoice_xref.invoice_xref', 'InvoiceXref')
    #si se intenta visualizar, se aplica regla de company y genera
    #inexistencia de factura
    acc_invoice = fields.Many2One('account.invoice', 'Invoice', states={'invisible': True})
    
    @classmethod
    def create(cls, vlists):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')            
        Account = pool.get('account.account')

        
        Date = pool.get('ir.date')
        result = []
        for vlist in vlists:
            with Transaction().set_context(company=vlist['company']):
                invoice = Invoice()
                invoice.company = vlist['company']
                invoice.invoice_date = Date.today()
                invoice.party = vlist['party']
                invoice.on_change_party()
                invoice.type = 'out'
                invoice.on_change_type()

                
                with Transaction().set_context(_parent_invoice=invoice):
                    revenue = Account.search([('kind', '=', 'revenue')])[0]
                    invoice_line = InvoiceLine()
                    invoice_line.company = vlist['company']
                    invoice_line.type = 'line'
                    invoice_line.invoice = invoice
                    invoice_line.quantity = 1
                    invoice_line.unit_price = vlist['price']
                    invoice_line.description = 'lab lab'
                    invoice_line.account = revenue
                    invoice_line.on_change_account()
                invoice.lines = [invoice_line]
                Invoice.save([invoice])
                Invoice.post([invoice])
                vlist['acc_invoice'] = invoice.id
                result.append(vlist)
        return super(InvoiceXrefLine, cls).create(result)
    
class InvoiceXref(ModelSQL, ModelView):
    "InvoiceXref"

    __name__ = 'lab_invoice_xref.invoice_xref'

    company = fields.Many2One('company.company', 'Company', required=True, domain=[])
    lines = fields.One2Many('lab_invoice_xref.invoice_xref.line', 'invoice', 'Lines')

class InvoiceXrefLink(Wizard):
    "InvoiceLink"
    __name__ = 'lab_invoice_xref.invoice_link'

    start_state = 'open_'
    #open_no_cambia_contxto_ = StateAction('account_invoice.act_invoice_form')
    open_ = StateAction('account_invoice.wizard_pay')

    def do_open_(self, action):
        #no se puede forzar company
        with Transaction().set_context(company=1):
            pool = Pool()
        
            ctx = Transaction().context
            #Obligatorio para consulta
            line = InvoiceXrefLine.search([('id', '=', ctx.get('active_id'))])[0]
            action['pyson_context'] = PYSONEncoder().encode({
                '_parent_invoice.company': line.company.id
            })
            action['context'] = PYSONEncoder().encode({
                '_parent_invoice.company': line.company.id
            })

            #action['domain'] = PYSONEncoder().encode(
            #    [('company', '=', line.company.id)]
            #)
            return action, {'id': line.acc_invoice.id}
    
    def do_opaen_no_cambia_contexto_(self, action):
        pool = Pool()
        
        ctx = Transaction().context
        with Transaction().set_context(company=1):
            invoice_xref = InvoiceXrefLine.search([('id', '=', ctx.get('active_id'))])[0]
        #no cambia el contexto
        #action['pyson_context'] = PYSONEncoder().encode({
        #    'company': ctx.get('company')
        #})
        action['context'] = PYSONEncoder().encode({
            'company': ctx.get('company')
        })
        action['domain'] = PYSONEncoder().encode(
            [('company', '=', invoice_xref.invoice.company.id)]
        )
        #action['pyson_domain'] = PYSONEncoder().encode(
        #    [('company', '=', invoice_xref.invoice.company.id)]
        #)
        return action, {}

    def transition_open_(self):
        return 'end'
