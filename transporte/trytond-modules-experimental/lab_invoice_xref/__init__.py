# This file is part of lab_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

__all__ = ['register']

from .invoice_xref import *

def register():
    Pool.register(
        InvoiceXrefLine,
        InvoiceXref,
        Invoice,
        module='lab_invoice_xref', type_='model')
    Pool.register(
        InvoiceXrefLink,
        module='lab_invoice_xref', type_='wizard')
    Pool.register(
        module='lab_invoice_xref', type_='report')
