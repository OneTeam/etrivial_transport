# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.lab_invoice_xref.tests.test_lab_invoice_xref import suite
except ImportError:
    from .test_lab_invoice_xref import suite

__all__ = ['suite']
