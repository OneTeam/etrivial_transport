# This file is part of account_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .configuration import *
from .invoice_xref import *
from . import invoice

__all__ = ['register']


def register():
    Pool.register(
        Configuration,
        ConfigurationCompany,
        InvoiceXref,
        InvoiceXrefLine,
        InvoiceXrefLineTax,
        invoice.InvoiceMultiCompany,
        invoice.PayInvoiceStart,
        PayInvoiceXrefStart,
        module='account_invoice_xref', type_='model')
    Pool.register(
        PayInvoiceXref,
        invoice.PayInvoice,
        module='account_invoice_xref', type_='wizard')
    Pool.register(
        module='account_invoice_xref', type_='report')
