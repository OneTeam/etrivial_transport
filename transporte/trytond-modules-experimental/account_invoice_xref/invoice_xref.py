# This file is part of account_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""Facturas de referencias cruzadas

:class:`InvoiceXref` agrupa una serie de facturas.
:class:`InvoiceXrefLine` es una referencia a una factura `account.invoice`.


"""
from collections import defaultdict
from decimal import Decimal
import itertools

from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button

from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.modules.product import price_digits
from trytond.pyson import If, Bool, Eval
from trytond.pool import Pool
from trytond.transaction import Transaction


_ZERO = Decimal('0.0')

class InvoiceXref(Workflow, ModelSQL, ModelView):
    "InvoiceXref"

    __name__ = 'invoice_xref.invoice'
    _states = {
        'readonly': Eval('state') != 'draft'
    }
    _depends = ['state']
    
    state = fields.Selection([
        ('draft', 'Draft'),
        ('validated', 'Validated'),
        ('posted', 'Posted'),
        ('paid', 'Paid')
    ], 'State', readonly=True)
    
    name = fields.Char('Name',
                       required=True, states=_states, depends=_depends)
    date = fields.Date('Date', states={
        'readonly': _states['readonly'],
        'required': Bool(Eval('invoice_lines_by_party'))
    }, depends=['state', 'invoice_lines_by_party'])

    # Origen del registro
    origin = fields.Selection([
        ('', 'Any'),
        ('user', 'User'),
    ], 'Origin', states=_states, depends=_depends, select=True)
    automatic = fields.Boolean('Automatic', states=_states, depends=_depends)
    invoices = fields.One2Many('invoice_xref.invoice_multi_company', 'invoice_xref', 'Invoice', states={'readonly': True})
    lines = fields.One2Many('invoice_xref.invoice.line', 'invoice', 'Lines',
                            required=True,
                            states=_states, depends=_depends)

    untaxed_amount = fields.Function(fields.Numeric('Untaxed',
                                                    digits=(16, 2)),
                                     'get_amount')
    tax_amount = fields.Function(fields.Numeric('Tax', digits=(16,2)),
                                 'get_amount')
    total_amount = fields.Function(fields.Numeric('Total', digits=(16, 2)),
                                   'get_amount')

    amount_to_pay = fields.Function(fields.Numeric('Amount To Pay', digits=(16, 2)),
                                    'get_amount_to_pay')

    invoice_lines_by_party = fields.Function(fields.Boolean('Group Lines'),
                                                            'default_invoice_lines_by_party')
                                           
    invoices = fields.One2Many('invoice_xref.invoice_multi_company', 'invoice_xref', 'Invoices',
                                            states=_states, depends=_depends, readonly=True)
    del _states, _depends

    @classmethod
    def default_invoice_lines_by_party(cls, instances=None, name=None):
        #requiere 'default_' para valor inicial y poder ser leido por 'states'
        pool = Pool()
        configuration = pool.get('invoice_xref.configuration').instance()

        if not instances:
            return configuration.invoice_lines_by_party

        result = {}
        for instance in instances:
            result[instance.id] = configuration.invoice_lines_by_party
        return result

    @staticmethod
    def default_automatic():
        return False

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def __setup__(cls):
        super(InvoiceXref, cls).__setup__()
        cls._error_messages.update({
            'configuration_company_not_found':
            ('Configuration company not found for company %d')
        })
        cls._transitions |= set((
            ('draft', 'validated'),
            ('validated', 'posted'),
            ('draft', 'posted'),
            ('validated', 'draft'),
            ('posted', 'paid'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['validated']),
                'depends': ['state']
            },
            'validate_invoice': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
            },
            'post': {
                'invisible': ~Eval('state').in_(['draft', 'validated']),
                'depends': ['state']
            },
            'pay': {
                'invisible': (Eval('state') != 'posted') \
                | (Eval('amount_to_pay', -1) == 0),
                'depends': ['state', 'amount_to_pay']
            }
        })
        
    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate_invoice(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, invoices):
        pool = Pool()
        configuration = pool.get('invoice_xref.configuration').instance()
        Company = pool.get('company.company')
        Party = pool.get('party.party')
        InvoiceMultiCompany = pool.get('invoice_xref.invoice_multi_company')

        invoices_xref = dict()
        minvoices = set()
        for invoice_xref in invoices:
            lines_by_party = defaultdict(list)
            
            for line in invoice_xref.lines:
                lines_by_party[(line.company.id, line.party.id)].append(line)
                
            invoices_xref[invoice_xref] = lines_by_party

        for invoice_xref, lines_by_party in invoices_xref.items():
            for (company_id, party_id), lines in lines_by_party.items():
                if configuration.invoice_lines_by_party:
                    with Transaction().set_context(company=company_id,
                                                   date=invoice_xref.date):
                        invoice = cls._generate_invoice(Company(company_id),
                                                        invoice_xref.date,
                                                        Party(party_id),
                                                        lines)

                        for line in lines:
                            minvoices.add(
                                ('invoice_xref', invoice_xref.id,
                                 'invoice_id', invoice.id,
                                 'company_id', line.company.id,
                                 'party', line.party.id)
                            )
                            line.account_invoice_id = invoice.id
                        InvoiceXrefLine.save(lines)

                else:
                    for line in lines:
                        with Transaction().set_context(company=line.company.id,
                                                       date=line.invoice_date):
                            invoice = cls._generate_invoice(line.company,
                                                            line.invoice_date,
                                                            line.party,
                                                            [line])
                            line.account_invoice_id = invoice.id
                            minvoices.add(
                                ('invoice_xref', invoice_xref.id,
                                 'invoice_id', invoice.id,
                                 'company_id', line.company.id,
                                 'party', line.party.id)
                            )
                    InvoiceXrefLine.save(lines)

        vlist = []
        for minvoice in minvoices:
            it = iter(minvoice)
            vlist.append(dict(zip(it, it)))
        InvoiceMultiCompany.create(vlist)

    @classmethod
    @ModelView.button_action('account_invoice_xref.wizard_pay_xref')
    def pay(cls, invoices):
        pass

    @classmethod
    def get_amount(cls, invoices, names):
        untaxed_amount = dict((i.id, _ZERO) for i in invoices)
        tax_amount = dict((i.id, _ZERO) for i in invoices)
        total_amount = dict((i.id, _ZERO) for i in invoices)
        for invoice in invoices:
            untaxed_amount[invoice.id] = sum([
                line.untaxed_amount for line in invoice.lines
            ])
            tax_amount[invoice.id] = sum([
                line.tax_amount for line in invoice.lines
            ])
            total_amount[invoice.id] = sum([
                line.total_amount for line in invoice.lines
            ])
        return {
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
        }

    @classmethod
    def get_amount_to_pay(cls, invoices, name):
        result = {}
        for invoice in invoices:
            result[invoice.id] = sum([
                line.amount_to_pay for line in invoice.lines
            ])
        return result

    @classmethod
    def _must_get_configuration(cls, company_id):
        pool = Pool()
        Configuration = pool.get('invoice_xref.configuration_company')
        configs = Configuration.search([
            ('company', '=', company_id)
        ], limit=1)
        if not configs:
            cls.raise_user_error('configuration_company_not_found',
                                 (company_id))
        return configs[0]

    @classmethod
    def _generate_invoice_line(cls, company, invoice, line):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Account = pool.get('account.account')
        invoice_line = InvoiceLine()
        invoice_line.company = company
        invoice_line.product = line.product
        invoice_line.on_change_product()
        invoice_line.type = 'line'
        invoice_line.invoice = invoice
        invoice_line.quantity = 1
        invoice_line.unit_price = line.unit_price
        invoice_line.description = line.description 
        invoice_line.account = line.account_id
        invoice_line.taxes = list(set([tax.id for tax in line.taxes]))
        invoice_line.on_change_account()
        return invoice_line

    @classmethod
    def _generate_invoice(cls, company, invoice_date, party, lines):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        Account = pool.get('account.account')
        config = cls._must_get_configuration(company.id)
        with Transaction().set_context(company=company.id,
                                       _check_access=False):
            invoice = Invoice()
            invoice.company = company
            invoice.invoice_date = invoice_date
            invoice.party = party
            invoice.on_change_party()
            invoice.type = 'out'
            invoice.on_change_type()
            invoice.lines = [cls._generate_invoice_line(company, invoice,
                                                        line) for line in lines]
            Invoice.save([invoice])
            Invoice.post([invoice])
            return invoice

class InvoiceXrefLineTax(ModelSQL):
    'InvoiceXref Line - Tax'
    __name__ = 'invoice_xref.invoice.line.tax'

    line = fields.Many2One('invoice_xref.invoice.line', 'Line', required=True,
                           ondelete='CASCADE')
    tax = fields.Many2One('account.tax', 'Tax', required=True,
                          ondelete='RESTRICT', context={
                              'company': Eval('_parent_line.company'),
                              '_check_access': False,
                          })
    
class InvoiceXrefLine(ModelSQL, ModelView):
    "InvoiceXrefLine"

    __name__ = 'invoice_xref.invoice.line'

    _states = {
        'readonly': Eval('state', 'draft') != 'draft'
    }
    _depends = ['state']
    
    state = fields.Function(fields.Char('State'),
                            'get_state')

    account_invoice_id = fields.Integer('Account Invoice Id')
    
    invoice = fields.Many2One('invoice_xref.invoice', 'InvoiceXref',
                              ondelete='CASCADE',
                              states=_states,
                              depends=_depends,
                              required=True,
                              select=True)
    company = fields.Many2One('company.company', 'Company', required=True,
                              select=True,
                              
                              #TODO: como hacer que se seleccione por defecto,
                              #segun el invoice?
                              context={
                                  'active_id': Eval('_parent_invoice', {}).get('company', -1)
                              },
                              states=_states,
                              depends= _depends + ['_parent_invoice.company'])

    account_id = fields.Integer('Account Id', states={'readonly': True})
    account = fields.Function(fields.One2Many('account.account', None, 'Account',
                                               size=1,
                                               domain=[
                                                   ('kind', 'in', ['revenue']),
                                               ],
                                               context={
                                                   'company': Eval('company', -1),
                                                   '_check_access': False
                                               }, states=_states, depends=_depends + ['company']),
                              'get_account', 'set_account')

    party = fields.Many2One('party.party', 'Party',
                            required=True, states=_states, depends=_depends)
    invoice_date = fields.Date('Invoice Date',
                               required=True,
                               states={
                                   'readonly': _states['readonly'],
                                   'invisible': Bool(Eval('_parent_invoice', {}).get('invoice_lines_by_party'))
                               },
                               depends=_depends)

    taxes = fields.Many2Many('invoice_xref.invoice.line.tax', 'line', 'tax', 'Taxes',
                             domain=[('company', '=', Eval('company', -1))],
                             context={
                                 'company': Eval('company', -1)
                             },
                             depends=_depends + ['company'])
    product = fields.Many2One('product.product', 'Product',
                              required=True, states=_states, depends=_depends)
    unit_price = fields.Numeric('Unit Price', digits=price_digits,
                                required=True, states=_states, depends=_depends)
    description = fields.Char('Description', size=None,
                              states=_states, depends=_depends)

    untaxed_amount = fields.Function(fields.Numeric('Untaxed',
                                                    digits=(16, 2)),
                                     'get_amount')
    tax_amount = fields.Function(fields.Numeric('Tax', digits=(16,2)),
                                 'get_amount')
    total_amount = fields.Function(fields.Numeric('Total', digits=(16, 2)),
                                   'get_amount')
                                    
    account_invoice_code = fields.Function(fields.Char('Invoice'),
                                       'get_account_invoice_code')
    
    amount_to_pay_today = fields.Function(fields.Numeric('Amount To Pay Today',
                                                         digits=(16, 2)),
                                                         'get_amount_to_pay')
                                                         
    amount_to_pay = fields.Function(fields.Numeric('Amount to Pay',
                                                   digits=(16, 2)),
                                    'get_amount_to_pay')
      
    @fields.depends('account_id')
    def get_account(self, name):
        return [self.account_id]
    
    @classmethod
    def set_account(cls, instances, name, value):
        for ins in instances:
            ins.account_id = value[0][1][0]
        cls.save(instances)

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('product', 'unit_price')
    def on_change_product(self):
        if not self.unit_price:
            self.unit_price = self.product.list_price

    @property
    def account_invoice(self):
        with Transaction().set_context(
                company=self.company.id,
                _check_access=False):
            pool = Pool()
            Invoice = pool.get('account.invoice')
            invoices = Invoice.search([
                ('id', '=', self.account_invoice_id)
            ], limit=1)
            if invoices:
                return invoices[0]
            return None

    @classmethod
    def __setup__(cls):
        super(InvoiceXrefLine, cls).__setup__()
        cls._buttons.update({
            'pay': {
                'invisible': Eval('state') == 'paid',
                'depends': ['state']
            }
        })

    @classmethod
    def get_amount(cls, lines, names):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        untaxed_amount = dict((i.id, _ZERO) for i in lines)
        tax_amount = dict((i.id, _ZERO) for i in lines)
        total_amount = dict((i.id, _ZERO) for i in lines)
        for line in lines:
            invoice = line.account_invoice
            if invoice:
                with Transaction().set_context(company=line.company.id,
                                               _check_access=False):
                    result =Invoice.get_amount([invoice], names)
                    if 'untaxed_amount' in result:
                        untaxed_amount[line.id] = result['untaxed_amount'][invoice.id]
                    if 'tax_amount' in result:
                        tax_amount[line.id] = result['tax_amount'][invoice.id]
                    if 'total_amount' in result:
                        total_amount[line.id] = result['total_amount'][invoice.id]

        return {
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount
        }

    @classmethod
    def get_account_invoice_code(cls, lines, name):
        result = {}
        for line in lines:
            invoice = line.account_invoice
            if invoice:
                result[line.id] = invoice.number
            else:
                result[line.id] = ''
                
        return result

    @classmethod
    def get_amount_to_pay(cls, lines, name):
        # DEPRECATED: genera confusion cuando son lineas de una sola factura
        # ya que se muestra es el valor de la factura
        result = {}
        pool = Pool()
        Invoice = pool.get('account.invoice')
        for line in lines:
            invoice = line.account_invoice
            if invoice:
                result[line.id] = Invoice.get_amount_to_pay([invoice], name)[invoice.id]
            else:
                result[line.id] = 0
        return result

    @classmethod
    def get_state(cls, records, name):
        result = {}
        for record in records:
            invoice = record.account_invoice
            if invoice:
                result[record.id] = invoice.state
            else:
                result[record.id] = 'draft'
        return result

    @classmethod
    def default_invoice_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()


#TOMADO DE: account_invoice/invoice.py
class PayInvoiceStart(ModelView):
    'Pay Invoice'
    __name__ = 'invoice_xref.invoice.pay.start'
    amount = fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], required=True)
    currency = fields.Many2One('currency.currency', 'Currency',
                               states={'invisible': True},
                               required=True)
    currency_digits = fields.Integer('Currency Digits', readonly=True)
    description = fields.Char('Description', size=None)
    company = fields.Many2One('company.company', "Company",
                              states={'invisible': True},
                              readonly=True)
    invoice_account = fields.Integer('Invoice Account',
                                     states={'invisible': True},
                                     readonly=True)
    payment_method = fields.Integer('Payment Method',
                                    states={'invisible':True},
                                    readonly=True)
    date = fields.Date('Date', required=True)

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_currency_digits():
        return 2

    @fields.depends('currency')
    def on_change_with_currency_digits(self):
        if self.currency:
            return self.currency.digits
        return 2


class PayInvoiceXref(Wizard):
    'Pay Invoice Xref'
    __name__ = 'invoice_xref.invoice_xref.pay'

    start = StateView('invoice_xref.invoice_xref.pay.start',
                      'account_invoice_xref.xref_pay_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('OK', 'pay', 'tryton-ok', default=True)
                      ])
    pay = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PayInvoiceXref, cls).__setup__()
        cls._error_messages.update({
            'amount_greater_amount_to_pay': (
                'You can not '
                'create a payment with an amount greater than the '
                'amount to pay.')
        })

    @classmethod
    def get_reconcile_lines_for_amount(cls, invoice, amount):
        if invoice.type == 'in':
            amount *= -1
        return invoice.get_reconcile_lines_for_amount(amount)

    def default_start(self, fields):
        default = {}

        if Transaction().context['active_model'] == 'invoice_xref.invoice':
            invoices_xref = InvoiceXref.search([
                ('id', 'in', Transaction().context['active_ids'])
            ])

            invoice_xref_lines = []
            for invoice_xref in invoices_xref:
                invoice_xref_lines.extend(invoice_xref.lines)
        else:
            invoice_xref_lines = InvoiceXrefLine.search([
                ('id', 'in', Transaction().context['active_ids'])
            ])

        amount_to_pay = sum([line.amount_to_pay for line in invoice_xref_lines])
        default['amount'] = amount_to_pay
        default['lines_to_pay'] = [line.id for line in invoice_xref_lines]
        return default

    def transition_pay(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Currency = pool.get('currency.currency')
        MoveLine = pool.get('account.move.line')

        without_funds = False
        remainder_amount = self.start.amount
        if self.start.amount > self.start.amount_to_pay:
            self.raise_user_error('amount_greater_amount_to_pay')
        
        for invoice_xref_line in self.start.lines_to_pay:
            with Transaction().set_context(company=invoice_xref_line.company.id):
                invoice = invoice_xref_line.account_invoice
                amount_to_pay = invoice.amount_to_pay
                remainder_amount -= amount_to_pay
                if amount_to_pay == 0 or without_funds:
                    continue

                if remainder_amount < 0:
                    amount_to_pay += remainder_amount
                    remainder_amount = 0
                    without_funds = True
                elif remainder_amount == 0:
                    without_funds = True

                with Transaction().set_context(date=self.start.date):
                    amount_to_pay = Currency.compute(invoice.currency,
                                             amount_to_pay, invoice.company.currency)
                reconcile_lines, remainder = \
                    self.get_reconcile_lines_for_amount(invoice, amount_to_pay)

                line = None
                if not invoice.company.currency.is_zero(amount_to_pay):
                    payment_method = InvoiceXref._must_get_configuration(invoice_xref_line.company.id).payment_method
                    line = invoice.pay_invoice(amount_to_pay,
                                               payment_method, self.start.date,
                                               self.start.description, None, None)

                if remainder == Decimal('0.0'):
                    if line:
                        reconcile_lines += [line]
                    if reconcile_lines:
                        MoveLine.reconcile(reconcile_lines)
        return 'end'
    
class PayInvoiceXrefStart(ModelView):
    'Pay Invoice Xref'
    __name__ = 'invoice_xref.invoice_xref.pay.start'
    invoice_xref = fields.Many2One('invoice_xref.invoice', 'Invoice', readonly=True)
    amount = fields.Numeric('Amount', digits=(16, 2))
    date = fields.Date('Date', required=True)
    lines_to_pay = fields.Many2Many('invoice_xref.invoice.line', None, None, 
                                    'Lines To Pay',
                                    states={'readonly': True},
                                    domain=[('invoice', '=', Eval('invoice_xref'))],
                                    depends=['invoice_xref'])
    amount_to_pay = fields.Function(
        fields.Numeric('Amount', digits=(16, 2),
                       readonly=True),
        'on_change_lines_to_pay')
    description = fields.Char('Description', size=None)
    
    @fields.depends('lines_to_pay')
    def on_change_lines_to_pay(self, name=None):
        amount = sum([line.amount_to_pay for line in self.lines_to_pay])
        if name:
            return amount
        else:
            self.amount_to_pay = amount

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


