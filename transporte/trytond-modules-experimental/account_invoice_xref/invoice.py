# This file is part of account_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from decimal import Decimal

from trytond.wizard import Wizard, StateView, StateAction, StateTransition, Button
from trytond.model import ModelStorage, ModelSQL, ModelView, Workflow, fields
from trytond.pyson import If, Bool, Eval
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.cache import Cache

"""
Invoice MultiCompany

Es un espejo de 'account.invoice'
"""
class InvoiceMultiCompany(ModelSQL, ModelView):
    'InvoiceMultiCompany'

    __name__ = 'invoice_xref.invoice_multi_company'

    invoice_xref = fields.Many2One('invoice_xref.invoice', 'Invoice', states={'readonly': True}, ondelete='CASCADE')
    invoice_id = fields.Integer('Invoice ID', states={'readonly': True})
    company_id = fields.Integer('Company ID', states={'readonly': True})

    number = fields.Function(
        fields.Char('Number', readonly=True),
        'get_from_invoice')
    
    company = fields.Function(
        fields.Many2One('company.company', 'Company',
                        readonly=True),
        'get_from_invoice')

    party = fields.Many2One('party.party', 'Party', states={'readonly': True})

    untaxed_amount = fields.Function(
        fields.Numeric('Untaxed', readonly=True),
        'get_from_invoice')
    
    tax_amount = fields.Function(
        fields.Numeric('Tax', readonly=True),
        'get_from_invoice')
    
    total_amount = fields.Function(
        fields.Numeric('Total', readonly=True),
        'get_from_invoice')
    
    amount_to_pay = fields.Function(
        fields.Numeric('Amount To Pay', readonly=True),
        'get_from_invoice')

    state = fields.Function(
        fields.Char('State', readonly=True),
        'get_from_invoice')

    lines = fields.Function(
        fields.Many2Many('invoice_xref.invoice.line', None, None, 'Lines', readonly=True),
        'get_lines')

    @property
    def invoice(self):
        with Transaction().set_context(
                company=self.company_id,
                _check_access=False):
            pool = Pool()
            Invoice = pool.get('account.invoice')
            return Invoice(self.invoice_id)
        
    @fields.depends('account_invoice_id')
    def get_lines(self, name):
        pool = Pool()
        InvoiceXrefLine = pool.get('invoice_xref.invoice.line')
        return list(map(int, InvoiceXrefLine.search([('account_invoice_id', '=', self.invoice_id)])))

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'pay': {
                'invisible': (Eval('state') != 'posted') \
                | (Eval('amount_to_pay', -1) == 0),
                'depends': ['state', 'amount_to_pay']
            }
        })

        
    @fields.depends('invoice_id', 'company_id')
    def get_from_invoice(self, name):
        mapper = {
            'number': lambda x: x.number,
            'company': lambda x: x.company.id,
            'party': lambda x: x.party.id,
            'untaxed_amount': lambda x: x.untaxed_amount,
            'tax_amount': lambda x: x.tax_amount,
            'total_amount': lambda x: x.total_amount,
            'amount_to_pay': lambda x: x.amount_to_pay,
            'state': lambda x: x.state,
        }
        return mapper[name](self.invoice)

    @classmethod
    @ModelView.button_action('account_invoice_xref.wizard_pay_invoice')
    def pay(cls, lines):
        pass


class PayInvoiceStart(ModelView):
    'Pay Invoice Start'

    __name__ = 'invoice_xref.invoice.pay.start'
    amount = fields.Numeric('Amount', digits=(16, 2), required=True)
    date = fields.Date('Date', required=True)
    amount_to_pay = fields.Function(
        fields.Numeric('Amount To Pay', 
                       digits=(16, 2),
                       required=True),
        'on_change_lines_to_pay')
    
    lines_to_pay = fields.Many2Many('invoice_xref.invoice_multi_company',
                                    None, None, 'Lines To Pay',
                                    states={'readonly': True})
    
    description = fields.Char('Description', size=None)

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @fields.depends('lines_to_pay')
    def on_change_lines_to_pay(self, name=None):
        amount = sum([inv.amount_to_pay for inv in self.lines_to_pay])
        if name:
            return amount
        else:
            self.amount_to_pay = amount

class PayInvoice(Wizard):
    'Pay Invoice'

    __name__ = 'invoice_xref.invoice.pay'

    start = StateView('invoice_xref.invoice.pay.start',
                      '',
                      [
                          Button('Cancel','end', 'tryton-cancel'),
                          Button('Ok', 'pay', 'tryton-ok', default=True)
                      ])
    pay = StateTransition()

    def default_start(self, fields):
        minvoices = InvoiceMultiCompany.search([
            ('id', 'in', Transaction().context['active_ids'])
        ])
        amount_to_pay = sum([inv.amount_to_pay for inv in minvoices])
        return {
            'amount_to_pay': amount_to_pay,
            'lines_to_pay': list(map(int, minvoices))
        }


    def transition_pay(self):
        # TODO: codigo repetido en el invoice_xref.invoice_xref.pay
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Currency = pool.get('currency.currency')
        MoveLine = pool.get('account.move.line')
        InvoiceXref = pool.get('invoice_xref.invoice')
        without_funds = False
        remainder_amount = self.start.amount
        if self.start.amount > self.start.amount_to_pay:
            self.raise_user_error('amount_greater_amount_to_pay')

        for minvoice in self.start.lines_to_pay:
            with Transaction().set_context(company=minvoice.company_id,
                                           _check_access=False):
                invoice = minvoice.invoice
                amount_to_pay = invoice.amount_to_pay
                remainder_amount -= amount_to_pay
                if amount_to_pay == 0 or without_funds:
                    continue

                if remainder_amount < 0:
                    amount_to_pay += remainder_amount
                    remainder_amount = 0
                    without_funds = True
                elif remainder_amount == 0:
                    without_funds = True

                with Transaction().set_context(date=self.start.date):
                    amount_to_pay = Currency.compute(invoice.currency,
                                             amount_to_pay, invoice.company.currency)
                reconcile_lines, remainder = \
                    self.get_reconcile_lines_for_amount(invoice, amount_to_pay)

                line = None
                if not invoice.company.currency.is_zero(amount_to_pay):
                    payment_method = InvoiceXref._must_get_configuration(minvoice.company_id).payment_method
                    line = invoice.pay_invoice(amount_to_pay,
                                               payment_method, self.start.date,
                                               self.start.description, None, None)

                if remainder == Decimal('0.0'):
                    if line:
                        reconcile_lines += [line]
                    if reconcile_lines:
                        MoveLine.reconcile(reconcile_lines)
        return 'end'

    @classmethod
    def get_reconcile_lines_for_amount(cls, invoice, amount):
        # TODO: codigo repetido en el invoice_xref.invoice_xref.pay

        if invoice.type == 'in':
            amount *= -1
        return invoice.get_reconcile_lines_for_amount(amount)
