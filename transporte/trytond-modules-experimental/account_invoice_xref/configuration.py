# This file is part of account_invoice_xref.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSingleton, ModelSQL, ModelView, fields
from trytond.model import Unique
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['ConfigurationCompany', 'Configuration']

class Configuration(ModelSingleton, ModelSQL, ModelView):
    "Configuration"

    __name__ = 'invoice_xref.configuration'

    invoice_lines_by_party = fields.Boolean('Group Invoice Lines by Party?',
                                            help='Lines with same company generate'
                                            'only a invoice')

    @classmethod
    def instance(cls):
        return Configuration(1)
    
class ConfigurationCompany(ModelSQL, ModelView):
    "Configuration Company"

    __name__ = 'invoice_xref.configuration_company'

    company = fields.Many2One('company.company', 'Company', required=True)
    journal_code = fields.Char('Journal Code', required=True)
    payment_method_name = fields.Char('Payment Method Name', required=True)

    @classmethod
    def __setup__(cls):
        super(ConfigurationCompany, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('company_unique', Unique(t, t.company),
             'Company must be unique')
        ]
        cls._error_messages.update({
            'journal_not_found': ('Journal with code %s not found'),
            'payment_menthod_name_not_found': ('Pay Method not found %s')
        })

    @property
    def payment_method(self):
        return self._must_payment_method_name(self.company.id, self.payment_method_name)
    
    @property
    def journal(self):
        return self._must_journal_code(self.company.id, self.journal_code)

    @classmethod
    def create(cls, vlist):
        for vals in vlist:
            cls._must_journal_code(vals['company'], vals['journal_code'])
        return super(ConfigurationCompany, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        lines = iter(args)
        for lines, values in zip(lines, lines):
            for line in lines:
                company = values.get('company', line.company.id)
                journal_code = values.get('journal_code', line.journal_code)

                cls._must_journal_code(company, journal_code)
        super(ConfigurationCompany, cls).write(*args)

    @classmethod
    def _must_payment_method_name(cls, company_id, name):
        with Transaction().set_context(_check_access=False,
                                       company=company_id):
            pool = Pool()
            PaymentMethod = pool.get('account.invoice.payment.method')
            records = PaymentMethod.search([
                ('name', '=', name)
            ], limit=1)
            if not records:
                cls.raise_user_error('payment_method_not_found',
                                     (name))
            return records[0]

    @classmethod
    def _must_journal_code(cls, company_id, code):
        with Transaction().set_context(_check_access=False,
                                       company=company_id):
            pool = Pool()
            Journal = pool.get('account.journal')
            journal = Journal.search([
                ('code', '=', code)
            ], limit=1)
            if not journal:
                cls.raise_user_error('journal_not_found',
                                     (code))
            return journal[0]
