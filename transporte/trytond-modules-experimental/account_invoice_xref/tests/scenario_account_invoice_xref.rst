=============================
Account Invoice Xref Scenario
=============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install account_invoice_xref::

    >>> config = activate_modules('account_invoice_xref')
