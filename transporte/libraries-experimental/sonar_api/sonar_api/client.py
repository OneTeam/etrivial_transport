# -*- coding: utf-8 -*-
# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

import functools
import hashlib
import zeep

class SonarSOAPVersionException(Exception):
    pass

class Client(object):
    """
    """

    VERSION = "3e139e42e1f2e0eaa5edfd2a0c0acaa1082a5d50f31eef03f0a4f2bf35ea0197"
    
    def __init__(self, wsdl_url, username, password):
        #TODO: solo validar el esquema de los valores requeridos tanto para peticiones
        #como respuestas.
        #self._check_version(wsdl_url)
        
        self.wsdl_url = wsdl_url
        self.client = zeep.Client(wsdl_url)
        self.username = username
        self.password = password

    def _check_version(self, wsdl_url):
        import urllib
        import base64
        import hashlib
        wsdlRaw = urllib.request.urlopen(wsdl_url)
        remote_version = hashlib.sha256(base64.b32encode(wsdlRaw.read())).hexdigest()
        if self.VERSION != remote_version:
            raise SonarSOAPVersionException("invalid remote version has %s expected %s" % \
                                            (remote_version, self.VERSION))
        
    def request(self, name):
        if not hasattr(self.client.service, name):
            raise AttributeError(name)

        request = functools.partial(getattr(self.client.service, name), 
                                    self.username, self.password)
        return request

