# -*- coding: utf-8 -*-
# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

import functools
import datetime
import types

from sonar_api import client

__all__ = ['Sonar', 'SonarSOAPException']

class SonarSOAPException(Exception):

    def __init__(self, status, desc, code):
        self.status = status
        self.description = desc
        self.error_code = code

def sonar_rpc(returns='', response=None, params={}, raiseErrorCode=True, cast={}):
    """
    decorador para metodo remoto de sonar.
    :param returns: ruta separado por . para tomar ese registro como valor de retorno.
    :param response: lista de como extraer el valor de retorno.
    :param cast: permite forzar cambio de tipo antes de la consulta, es un metodo de un argumento que
    realiza el cambio de tipo.
    :param params: los parametros esperados y su tipo, el tipo puede ser un metodo de un argumento que
    retorna True en caso de ser el tipo esperado.
    :param raiseErrorCode: indica si se debe generar excepcion segun retorno de funcion remota.
    """
    
    def decorator(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            unknown_params = set(kwargs.keys()) - set(params.keys())
            if len(unknown_params) > 0:
                raise TypeError('Unknown params %s' % (unknown_params))

            for param, type_ in params.items():
                if param in kwargs:
                    valid_type = False
                    if isinstance(kwargs[param], types.FunctionType):
                        valid_type = type_(kwargs[param])
                    else:
                        valid_type = isinstance(kwargs[param], type_)
                        
                    if not valid_type:
                        raise TypeError('Argument %s invalid type,is %s expected %s' % (param, str(kwargs[param].__class__), str(type_)))

                    if param in cast:
                        kwargs[param] = cast[param](kwargs[param])
                        
            sonar_request = self.request(func.__name__)
            res = sonar_request(**kwargs)
            if response:
                res = getattr(res, response)

            status = getattr(res, 'status', None)
            description = getattr(res, 'description', None)
            error_code = getattr(res, 'error_code', None)
            
            if raiseErrorCode and status:
                if res.status != 'OK':
                    raise SonarSOAPException(status,
                                             description,
                                             error_code)
            traverse = returns.split('.')
            records = functools.reduce(lambda acc, val: getattr(acc, val), traverse, res)
            return func(self, records, response=res)

        return wrapper
    return decorator



def cast_utc_datetime(val):
    """
    Se fuerza a utc y se da formato fecha usada por sonar
    """
    
    if isinstance(val, datetime.datetime):
        if field.tzname() == None or field.tzname() == 'UTC':
            utc_val = val.replace(tzinfo=datetime.timezone.utc)
        else:
            utc_val = val.astimezone(datetime.timezone.utc)
        return utc_val.strftime("%Y-%m-%d %H:%M:%S")
    return val

class Sonar(client.Client):
    """
    Este es un conector al WSDL de sonar.

    Ejemplo de uso::
      >>> api = sonar_api.Sonar('https://sonaravl.com/b2b/Service.asmx?WSDL', 'test', 'test')
      >>> api.GET_MobileList(FleetId='')
    """
    
    @sonar_rpc('AccountInfo',
               response='accountList')
    def GET_AccountList(self, records, response=None):
        return records

    @sonar_rpc('FleetInfo',
               response='fleetList')
    def GET_FleetList(self, records, response=None):
        return records

    @sonar_rpc('Mobile',
               response='mList',
               params={'FleetId': str})
    def GET_MobileList(self, records, response=None):
        return records

    @sonar_rpc('MobileGroup',
               response='gList',
               params={'FleetId': str})
    def GET_MobileGroups(self, records, response=None):
        return records

    @sonar_rpc('ItLog',
               response='ItsLog',
               cast={'UTC_datetime_init': cast_utc_datetime, 'UTC_datetime_end': cast_utc_datetime},
               params={'mId': str, 'Itinerary': str,
                       'UTC_datetime_init': object, 'UTC_datetime_end': object})
    def GET_ItinerariesHistory(self, records, response=None):
        return records
    
    @sonar_rpc('Itinerary',
               response='itList',
               params={'GroupId': str})
    def GET_Itineraries(self, records, response=None):
        return records

    @sonar_rpc('GroupGeofence',
               response='groupGeofenceList',
               params={'GroupId': str})
    def GET_GroupGeofences(self, records, response=None):
        return records
    
    @sonar_rpc('GroupGeoFencePoints',
               response='listGeoFencePoints',
               params={'GroupId': str})
    def GET_GeofenceWithPoints(self, records, response=None):
        return records

    @sonar_rpc('MobileGeofence',
               response='mobileGeofenceList',
               params={'mId': str})
    def GET_MobileGeofences(self, records, response=None):
        return records

    @sonar_rpc('Driver',
               response='drList',
               params={'FleetId': str, 'DrvId': str})
    def GET_Drivers(self, records, response=None):
        return records

    @sonar_rpc('Driver',
               response='drList',
               params={'FleetId': str, 'DrvId': str, 'DrvDoc': str})
    def GET_Drivers_v2(self, records, response=None):
        return records

    @sonar_rpc('DriversLog',
               response='driverslogList',
               cast={'UTC_datetime_init': cast_utc_datetime, 'UTC_datetime_end': cast_utc_datetime},
               params={'mId': str, 'UTC_datetime_init': object, 'UTC_datetime_end': object})
    def GET_DriversHistory(self, records, response=None):
        return records

    @sonar_rpc('CounterLog',
               response='counterlogList',
               cast={'UTC_datetime_init': cast_utc_datetime, 'UTC_datetime_end': cast_utc_datetime},
               params={'mId': str, 'CounterID': str,
                       'UTC_datetime_init': object, 'UTC_datetime_end': object})
    def GET_CounterHistory(self, records, response=None):
        return records

    @sonar_rpc('PassengersLog',
               response='passengerslogList',
               cast={'UTC_datetime_init': cast_utc_datetime, 'UTC_datetime_end': cast_utc_datetime},
               params={'mId': str,
                       'UTC_datetime_init': object, 'UTC_datetime_end': object})
    def GET_PassengersCounter(self, records, response=None):
        return records

    @sonar_rpc('GeolocatedPassengerLog',
               response='geolocated_passenger',
               cast={'datetime_init': cast_utc_datetime, 'datetime_end': cast_utc_datetime},
               params={'mId': str,
                       'datetime_init': object, 'datetime_end': object})
    def GET_GeolocatedPassengerHistory(self, records, response=None):
        return records

    @sonar_rpc('EventLocation',
               response='evtList',
               cast={'UTC_datetime_init': cast_utc_datetime, 'UTC_datetime_end': cast_utc_datetime},
               params={'mId': str,
                       'eventID': str,
                       'UTC_datetime_init': object, 'UTC_datetime_end': object})
    def GET_EventsHistory(self, records, response=None):
        return records

    @sonar_rpc(raiseErrorCode=False,
               cast={'UTC_datetime': cast_utc_datetime},
               params={'mId': str, 'Itinerary': str, 'DrvId': str, 'UTC_datetime': object})
    def SET_ItAssign(self, records, response=None):
        if response.status == 'OK':
            return True
        return False

    @sonar_rpc(raiseErrorCode=False,
               cast={'UTC_datetime': cast_utc_datetime},
               params={'mId': str, 'Itinerary': str, 'DrvId': str,
                       'UTC_datetime': object, 'comments': str})
    def SET_ItAssign_v2(self, records, response=None):
        if response.status == 'OK':
            return True
        return False

