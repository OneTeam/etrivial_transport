=========
sonar_api
=========


.. image:: https://img.shields.io/pypi/v/sonar_api.svg
        :target: https://pypi.python.org/pypi/sonar_api

.. image:: https://img.shields.io/travis/bit4bit/sonar_api.svg
        :target: https://travis-ci.org/bit4bit/sonar_api

.. image:: https://readthedocs.org/projects/sonar-api/badge/?version=latest
        :target: https://sonar-api.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python para conectar con Sonar


* Free software: GNU General Public License v3
* Documentation: https://sonar-api.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
