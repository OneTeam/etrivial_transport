# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Created on 12/07/2018

@author: francisco.garcia@santra.com.co
'''
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval

__all__ = ['Configuration']

class Configuration(ModelSingleton,ModelSQL,ModelView):
    '''Configuracion'''
    
    __name__ = "fleet.configuration"
    
    licence_plate_default_country = fields.Many2One('country.country', 'Pais por defecto para la placa',
                            required=True)
    licence_plate_default_departament = fields.Many2One("country.subdivision",'Departamento por defecto para la placa',
                                domain=[('country', '=', Eval('licence_plate_default_country', -1)),
                                    ('parent', '=', None),],
                                required=True
                                )
    licence_plate_default_city = fields.Many2One("country.subdivision",'Municipio por defecto para la placa',
                        domain=[
                            ('country', '=', Eval('licence_plate_default_country', -1)),
                            ('parent', '=', Eval('licence_plate_default_departament',-1)),
                            ],
                        required=False
                        )
    years_live = fields.Integer('Años de vida',
                                       help="De los vehículos"
                                       )
    
    @staticmethod
    def default_years_live():
        return 20
