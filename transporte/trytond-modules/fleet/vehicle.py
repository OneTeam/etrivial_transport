# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""
Created on 30/05/2018

@author: francisco.garcia@santra.com.co
"""
from trytond.model import ModelSQL, ModelView, fields, Unique
import logging
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.exceptions import UserError

from re import match
from _datetime import date

logger = logging.getLogger(__name__)

__all__ = ['Vehicle','VehicleOdometerLog','VehicleModel','VehicleBrand',
           'VehicleState','VehicleEuropeanStandard','VehicleRegistration']

_type = [('B','Bus'),('M','Microbus'),('','')]#tambien usada en route.py

_odometer_units = [
        ('kilometros', 'Kilometros'),
        ('millas', 'Millas'),
        ('','')
        ]


class Vehicle(ModelSQL,ModelView):
    """Vehículo.

    """
    __name__ = 'fleet.vehicle'
    
    active = fields.Boolean('Activo')
    licence_plate = fields.Char('Placa',
                                required=True,
                                help="formatos validos ABC45D T1234 R12345 AB1234"
                                )
    licence_plate_country = fields.Many2One('country.country', 'Pais',
                            required=True)
    licence_plate_departament = fields.Many2One("country.subdivision",'Departamento',
                                domain=[('country', '=', Eval('licence_plate_country', -1)),
                                    ('parent', '=', None),],
                                required=True
                                )
    licence_plate_city = fields.Many2One("country.subdivision",'Ciudad de la Placa',
                        domain=[
                            ('country', '=', Eval('licence_plate_country', -1)),
                            ('parent', '=', Eval('licence_plate_departament',-1)),
                            ],
                        required=True
                        )
    chassis_serial = fields.Char('Serial del Chasis',
                                 help="Serial del Chasis, junto con la serial del motor\nforman las improntas.\n"
                                 "Tambien se conoce como número de identificación vehicular (VIN)",
                                 required=True,
                                 size=17,
                                 )
    motor_serial = fields.Char('Serial del Motor',
                                 help="Serial del Motor, junto con la serial del chasis\nforman las improntas",
                                 required=True,
                                 )
    bodywork_serial = fields.Char('Serial de la Carrocería')
    displacement = fields.Integer('Cilindraje (cc)',
                                  help="Volumen útil de todos los cilindros de un motor") 
    chassis_brand = fields.Many2One('fleet.vehicle.brand','Marca del Chasis',
                                    domain=[('chassis_make','=',True),('active','=',True)],
                            required=True,
                            help='Fabricante del vehiculo')
    bodywork_brand = fields.Many2One('fleet.vehicle.brand','Marca de la Carroceria',
                                     domain=[('bodywork_make','=',True),('active','=',True)],
                            required=True,
                            help='Fabricante de la carroceria del vehiculo')
    model = fields.Many2One('fleet.vehicle.model', 'Linea',
                            help="Linea de la marca del vehículo",
                            required=True,
                            domain=[('brand','=',Eval('chassis_brand')),('active','=',True)],
                            )
    state = fields.Many2One('fleet.vehicle.state', 'State', 
        help='Current state of the vehicle',
        )
    model_year = fields.Integer('Modelo',
                            help='Año del Modelo',
                            required=True
                            )
    end_of_life = fields.Function(fields.Date('Fin de la vida útil'),'on_change_with_end_of_life')
    doors = fields.Integer('Numero de Puertas',
                        help='Numero de puertas del Vehiculo',
                        required=True
                        )
    odometer_logs = fields.One2Many('fleet.vehicle.odometer_log','vehicle','Odometros registrados',
                                    help='propietarios vinculados a este contrato')
    odometer_unit = fields.Selection(_odometer_units, 'Unidad del odometro',
        help='Unit of the odometer ',
        required=False)
    transmission = fields.Selection([
        ('manual', 'Manual'),
        ('automatica', 'Automatica'),
        ('','')
        ],'Transmision',
        help='Tipo de Transmision usada por el vehiculo')
    fuel_type = fields.Selection([
        ('gasolina', 'Gasolina'),
        ('acpm', 'ACPM'),
        ('electrico', 'Electrico'),
        ('hibrido', 'Hibrido'),
        ('gnv','Gas Natural Vehicular'),
        ('','')
        ], 'Tipo de combustible', help='Combustible usado por el vehiculo')
    
    european_standard = fields.Many2One('fleet.vehicle.european_standard', 'Euro', 
        help='Estandar Europeo sobre emisiones',
        required=True
        )
    horsepower = fields.Integer('Caballos de Fuerza')
    power = fields.Integer('Potencia', help='Potencia (en kW) del vehiculo')
    co2 = fields.Float('CO2 Emissions', help='Emisiones CO2 del vehiculo')
    car_value = fields.Float("Valor del vehiculo", help='Valor completo del vehiculo')
    image = fields.Binary("Fotografia")
    note = fields.Text('Observaciones',
                    help='Notas del vehiculo'
                    )
    skin_publicity = fields.Boolean('Publicidad',
                                    help="Publicidad Exterior")
    transit_registrations = fields.One2Many('fleet.vehicle.registration','vehicle','Licencias de Transito',
                                      order=[('expedition_date','DESC')],
                                      )
    @staticmethod
    def default_transmission():
        return 'manual'
    
    @fields.depends('model_year')
    def on_change_with_end_of_life(self,name=None):
        Configuration = Pool().get('fleet.configuration')
        configuration = Configuration.get_singleton()
        if self.model_year:
            if hasattr(configuration, 'years_live'):
                year = configuration.years_live + self.model_year
                return date(year,12,31 )
        return None
    
#     @staticmethod
#     def default_model_year():
#         return 2010
    
#     @staticmethod
#     def default_european_standard():
#         Euro = Pool().get('fleet.vehicle.european_standard')
#         euro = Euro.search([('name','=','No Aplica')])
#         return euro[0].id
    
    @staticmethod
    def default_fuel_type():
        return 'acpm'
    
    @staticmethod
    def default_active():
        return True
    
    @staticmethod
    def default_odometer_unit():
        return 'kilometros'
    
    @classmethod
    def __setup__(cls):
        super(Vehicle, cls).__setup__()
        cls._order = [('active','ASC'),('licence_plate','ASC')]
        cls._history = True
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_licence_plate_city_plate_key', Unique(table, table.licence_plate,table.licence_plate_city),
                'Vehiculo: una placa es solo para un vehiculo'),
            ('vehicle_chassis_serial_key', Unique(table, table.chassis_serial),
                'Vehiculo: el serial del chassis debe ser unico'),
            ('vehicle_motor_serial_key', Unique(table, table.motor_serial),
                'Vehiculo: el serial del motor debe ser unico'),
        ]
    
    def pre_validate(self):
        super(Vehicle, self).pre_validate()
        if not self.check_licence_plate_co(self.licence_plate):
            raise UserError("formatos validos TTY123 ABC45D T1234 R12345 AB1234")
        if not 1960 <= self.model_year <= 2050:
            raise UserError("El modelo (año) debe ser de 4 digitos. ej: 2008")
        if not self.check_chassis_serial(self.chassis_serial):
            raise UserError("El Chassis (DVI) es de 17 caracteres: numeros del 0 al 9 "
                            "Y letras mayuscualas MENOS O Ñ Q I")


    @staticmethod
    def check_licence_plate_co(licence_plate, only_common_vehicle=False):
        """ Valida el formato de una placa de vehiculo (carro, moto, ...) colombiana
            formatos validos ABC123 ABC45D T1234 R12345 AB1234
            si se indica only_common_vehicle solo se evalua el format ABC123
        """
        expresions = ('^[A-Z]{3}[0-9]{3}$',# CARROS
                  '^[A-Z]{3}[0-9]{2}[A-Z]{1}$',# MOTOS
                  '^[T]{1}[0-9]{4}$',# ALGUNOS DE CARGA
                  '^[RS]{1}[0-9]{5}$',# REMOLQUES Y SEMIREMOLQUES
                  '^[A-Z]{2}[0-9]{4}$',# DIPLOMATICOS
        )
        if only_common_vehicle:
            expresions = expresions[0:1]
        for expresion  in expresions:
            if match(expresion, licence_plate):
                return True
        return False
        
        
    @staticmethod
    def check_chassis_serial(chassis_serial):
        """Valida el formato de una serial de chassis (VIN)
        
        expresion regular '^[A-NPR-Z0-9]{17}$'
        """
        if match('^[A-HJ-NPR-Z0-9]{17}$',chassis_serial):
            return True
        else:
            return False
    
    def get_rec_name(self, name):
        return self.licence_plate
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('licence_plate', ) + tuple(clause[1:]),
            ]
    
    @staticmethod
    def plate_to_valid_plate(plate):
        """Convierte diferentes formatos de placa en una placa valida.
        
        Formatos previstos: '[A-Za-z]{3}.?[0-9]{3}\$' con cualquier
        cantidad de espacios al inicio o final.
        return format : AAA-111
        """
        plate = plate.strip()
        if match('^[A-Za-z]{3}.?[0-9]{3}$',plate):
            return plate[0:3].upper() + plate[-3:]
    
    @staticmethod
    def default_licence_plate_country():
        Configuration = Pool().get('fleet.configuration')
        configuration = Configuration.get_singleton()
        if configuration:
            if configuration.licence_plate_default_country:
                return configuration.licence_plate_default_country.id
     
    @staticmethod
    def default_licence_plate_departament():
        Configuration = Pool().get('fleet.configuration')
        configuration = Configuration.get_singleton()
        if configuration:
            if configuration.licence_plate_default_departament:
                return configuration.licence_plate_default_departament.id
         
#     #comentado para requerir que el usuario escoja la ciudad.
#     @staticmethod
#     def default_licence_plate_city():
#         Configuration = Pool().get('public_transport.configuration')
#         configuration = Configuration.get_singleton()
#         if configuration:
#             if configuration.route_default_city:
#                 return configuration.route_default_city.id

class VehicleMixin(object):
    """Vehículo."""
    
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              ondelete='RESTRICT',
                              required=True,
                              domain=[('active','=',True)]
                              )
    

class VehicleOdometerLog(VehicleMixin,ModelSQL,ModelView):
    """Registro de Odometro de un vehiculo.

    """
    __name__ = 'fleet.vehicle.odometer_log'
    _order = [('date','ASC'),('vehicle','ASC')]
    
    date = fields.Date('Fecha',
                       help="Fecha del Registro",
                       required=True
                       )
    unit = fields.Function(fields.Selection(_odometer_units,'Unidad de medida'),'on_change_with_unit')
    value = fields.Float('Valor del odometro',
                         required=True
                         )
    
    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()
    
    @staticmethod
    def default_active():
        return True
    
    @fields.depends('vehicle')
    def on_change_with_unit(self,names=None):
        if self.vehicle:
            return self.vehicle.odometer_unit
        
    
class VehicleModel(ModelSQL,ModelView):
    """Modelos de Vehiculos."""
    
    __name__ = 'fleet.vehicle.model'
    _order = [('active','ASC'),('name','ASC')]
    
    active = fields.Boolean('Activo')
    name = fields.Char('Nombre',
                       required=True)    
    brand = fields.Many2One('fleet.vehicle.brand','Fabricante',
                            domain=[('chassis_make','=',True),('active','=',True)],
                            required=True,
                            help='Fabricante del vehiculo')
    notes = fields.Text('Observaciones')
    
    @classmethod
    def __setup__(cls):
        super(VehicleModel, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehiclemodel_name_brand', Unique(table, table.name,table.brand),
                'Vehiculo: el modelo no se puede repetir para la misma marca'),
        ]

class VehicleBrand(ModelSQL,ModelView):
    """Marcas de vehiculos."""
    
    __name__ = 'fleet.vehicle.brand'
    _order = [('active','ASC'),('name','ASC')]
    
    active = fields.Boolean('Activa')
    name = fields.Char('Nombre',
                       required=True)
    origin_country = fields.Many2One("country.country",
        'Pais de origen',
        domain=[],
        required=False
        )
    logo = fields.Binary('Logotipo')
    chassis_make = fields.Boolean('Fabrica Chassis')
    bodywork_make = fields.Boolean('Fabrica Carrocerias')
    notes = fields.Text('Observaciones')
    
    @staticmethod
    def default_active():
        return True
    
    @classmethod
    def __setup__(cls):
        super(VehicleBrand, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehiclebrand_name', Unique(table, table.name),
                'Vehiculo_marca: El nombre de la marca debe ser unico'),
        ]

class VehicleState(ModelSQL,ModelView):
    """Estado de los vehiculos."""
    
    __name__ = 'fleet.vehicle.state'
    _order = [('date','ASC'),('vehicle','ASC')]
    
    name = fields.Char('Nombre',
                       required=True)
    notes = fields.Text('Observaciones')
    
    @classmethod
    def __setup__(cls):
        super(VehicleState, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_state_name', Unique(table, table.name),
                'Vehiculo_state: El nombre de la estado debe ser unico'),
        ]
class VehicleEuropeanStandard(ModelSQL,ModelView):
    """Estandares Europeos"""
    
    __name__ = 'fleet.vehicle.european_standard'
    
    name = fields.Char('Nombre',
                       required=True)
    year = fields.Integer('Año',
                       help="Año que entró en vigor",
                       domain=[('year','>=',1992),
                               ('year','<=',2050)]
                       )
    active = fields.Boolean('Activo')
    notes = fields.Text('Observaciones')
    
    @staticmethod
    def default_active():
        return True
    
    @classmethod
    def __setup__(cls):
        super(VehicleEuropeanStandard, cls).__setup__()
        cls._order = [('active','ASC'),('name','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_europeanstandard_name', Unique(table, table.name),
                'Vehiculo_european_standard: El nombre del estandar europeo debe ser unico'),
        ]
        
class VehicleRegistration(VehicleMixin,ModelSQL,ModelView):
    """Licencia de Transito de vehiculos (mal llamada matricula)"""
     
    __name__= 'fleet.vehicle.registration'
    _name = 'code'
     
    code = fields.Char('Codigo',
                       required=True)
    party= fields.Many2One('party.party', 'Propietario',
                        required=True,
                        ondelete='RESTRICT',
                        domain=[('active','=',True)],
                        )
    and_others = fields.Boolean('Y otros',
                                help="Indica si en el espacio de propietario aparece y otros")
    registration_date = fields.Date('Fecha de Matricula',
                                    help="Fecha que se registra el vehículo ante la autoridad. "
                                    "Cambia cuando se cambia de Secretaria")
    expedition_date = fields.Date('Fecha de Expedición',
                                  required=True
                                  )
