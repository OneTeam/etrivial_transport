# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .vehicle import *
from .configuration import *
__all__ = ['register']


def register():
    Pool.register(
        Vehicle,
        VehicleOdometerLog,
        VehicleModel,
        VehicleBrand,
        VehicleState,
        Configuration,
        VehicleEuropeanStandard,
        VehicleRegistration,
        module='fleet', type_='model')
    Pool.register(
        module='fleet', type_='wizard')
    Pool.register(
        module='fleet', type_='report')
