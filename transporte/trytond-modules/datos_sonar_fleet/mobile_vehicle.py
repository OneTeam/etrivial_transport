'''
Created on 4/10/2018

@author: francisco.garcia@santra.com.co
'''
import logging

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool,PoolMeta
from trytond.wizard import Wizard, StateTransition

logger = logging.getLogger(__name__)

__all__ = ['MobileVehicle']

class MobileVehicle(ModelSQL,ModelView):
    """Vinculo entre datos_sonar.mobiles y fleet.vehicle"""
    
    __name__ = 'datos_sonar_fleet.mobile_vehicle'
    
    mobile = fields.Many2One('datos_sonar.mobiles','Mobil Sonar')
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo de Flota')
    
    @classmethod
    def __setup__(cls):
        super(MobileVehicle, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('mobile_key', Unique(table, table.mobile),
                'MobileVehicle: el mobil debe ser unico'),
            ('vehicle_key', Unique(table, table.vehicle),
                'MobileVehicle: el vehiculo debe ser unico'),
        ]