from trytond.model import ModelSQL, ModelView, fields
import logging
from trytond.pool import Pool,PoolMeta
from trytond.wizard import Wizard, StateTransition
from trytond.pyson import Eval


__all__ = ['LinkMobileWithFleet','Mobile']

class Mobile(ModelSQL,ModelView):
    
    __name__ = "datos_sonar.mobiles"
    __metaclass__ = PoolMeta
    
    fleet_vehicle = fields.Many2One('fleet.vehicle','Vehiculo de Flota @deprecated')#@deprecated
    vehicle_fleet = fields.One2One('datos_sonar_fleet.mobile_vehicle','mobile','vehicle',"Vehiculo de flota",
                                  help="Vehiculo de la Flota de sonar al que representa este mobil",
                                  domain=[('licence_plate','=',Eval('mPlaca',-1))]
                                  )
    
    @classmethod
    def link_with_fleet(cls):
        """Vincula Mobile con fleet
        
        @todo se cambio el campo que vincula mobiles con vehicles de fleet_vehicle
         por vehicle_fleet y antes era Many2One y ahora es One2One
        """
        
        Vehicle = Pool().get('fleet.vehicle')
        records = cls.search([('vehicle_fleet','=',None)])
        vehicles_records = Vehicle.search([('id','>',0)])
        vehicles = {getattr(x, 'licence_plate'):getattr(x, 'id') for x in vehicles_records}
        records_to_link =[]
        #eliminar records que no tiene vehiculo a vincular
        if records:
            for record in records:
                plate = record.mPlaca
                plate = Vehicle.plate_to_valid_plate(plate)
                if plate in vehicles.keys():
                    records_to_link.append(record)
        del records
        if records_to_link:
            logging.error('records_to_link:{}'.format(records_to_link))
            for x in range(0,len(records_to_link)):
                plate = records_to_link[x].mPlaca
                plate = Vehicle.plate_to_valid_plate(plate)
                cls.write([records_to_link[x]],{'vehicle_fleet':vehicles[plate]})
            cls.save(records_to_link)
        return

class LinkMobileWithFleet(Wizard):
    """Vincula mobiles con fleet.vehicle."""
    
    __name__ = 'datos_sonar.mobiles.link_with_fleet'
    start = StateTransition()
    
    def transition_start(self):
        Mobile = Pool().get('datos_sonar.mobiles')
        Mobile.link_with_fleet()
        return 'end'