# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from .mobiles import *
from .mobile_vehicle import *
from .vehicle import *

__all__ = ['register']


def register():
    Pool.register(
        Mobile,
        MobileVehicle,
        Vehicle,
        module='datos_sonar_fleet', type_='model')
    Pool.register(
        LinkMobileWithFleet,
        module='datos_sonar_fleet', type_='wizard')
    Pool.register(
        module='datos_sonar_fleet', type_='report')
