import logging

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool,PoolMeta
from trytond.pyson import Eval

logger = logging.getLogger(__name__)
__all__ = ['Vehicle']

class Vehicle(ModelSQL,ModelView):
    """Vehículo"""
    __name__ = 'fleet.vehicle'
    __metaclass__ = PoolMeta
    
    sonar_id = fields.Function(fields.Char('Sonar id'),'get_sonar_id')
    sonar_mobile = fields.One2One('datos_sonar_fleet.mobile_vehicle','vehicle','mobile',"Mobil Sonar",
                                  help="Mobil en la plataforma de Sonar",
                                  domain=[('mPlaca','=',Eval('licence_plate',-1))]
                                  )
    
    def get_sonar_id(self,name):
        if self.sonar_mobile:
            return self.sonar_mobile.mId