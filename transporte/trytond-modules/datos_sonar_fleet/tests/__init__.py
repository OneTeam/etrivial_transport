# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.datos_sonar_fleet.tests.test_datos_sonar_fleet import suite
except ImportError:
    from .test_datos_sonar_fleet import suite

__all__ = ['suite']
