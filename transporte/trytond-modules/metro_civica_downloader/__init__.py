# This file is part of metro_civica_downloader.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .downloader import *
__all__ = ['register']


def register():
    Pool.register(
        Download,
        Configuration,
        DownloaderForm,
        module='metro_civica_downloader', type_='model')
    Pool.register(
        Downloader,
        module='metro_civica_downloader', type_='wizard')
    Pool.register(
        module='metro_civica_downloader', type_='report')
