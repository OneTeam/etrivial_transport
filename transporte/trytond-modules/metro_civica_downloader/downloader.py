# This file is part of metro_civica_downloader.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime, timedelta
import logging
import secrets
import os

logger = logging.getLogger(__name__)

from trytond.model import ModelSingleton, ModelSQL, ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

import cliente_apps_civica as apcv

_REPORT_OPTIONS = [
     ('liquidacion-usos-transporte', 'Liquidacion Usos Transporte')
]

class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Configuration'

    __name__ = 'metro_civica_downloader.configuration'
    _rec_name = 'username'

    def get_store_path(self, name):
        configuration = Pool().get('metro_civica.configuracion')(1)
        return configuration.ruta_archivos

    username = fields.Char('Username', required=True)
    password = fields.Char('Password', required=True)
    last_password = fields.Char('Last Password', states={'invisible': True})

    timeout = fields.Integer('Timeout', required=True)
    store_path = fields.Function(fields.Char('Store Path', required=True),
                                 'get_store_path')
    user_store_path = fields.Char('User Store Path', required=True,
                                  help="Directory for download from user client")
    metro_civica_process = fields.Boolean('Link with Metro Civica Process')

    @staticmethod
    def default_metro_civica_process():
        return True

    @staticmethod
    def default_user_store_path():
        import tempfile
        return tempfile.gettempdir()
    
    @staticmethod
    def default_timeout():
        return 120
    
    @staticmethod
    def default_store_path():
        configuration = Pool().get('metro_civica.configuracion')(1)
        return configuration.ruta_archivos

    @classmethod
    def renew_password(cls):
        configuration = cls(1)
        with apcv.sesion(configuration.username,
                         configuration.password) as ctx:
            configuration.last_password = configuration.password
            configuration.password = secrets.token_hex(8)
            result = apcv.cambio_clave_operador(ctx, configuration.last_password, configuration.password)
            if not result:
                logger.debug('fallo auto_change_password', exc_info=True)
                logger.debug(str(apcv.error(ctx)))
                return
            configuration.save()


class DownloaderForm(ModelView):
    'Downloader - Form'
    __name__ = 'metro_civica_downloader.downloader.form'
    since = fields.Date('Since', required=True)
    until = fields.Date('Until', required=True)
    report = fields.Selection(_REPORT_OPTIONS, 'Report', required=True)
    system = fields.Boolean('System',
                            help='Caution! this option force download to link with metro_civica')

    background = fields.Boolean('Process in Background?')

    @staticmethod
    def default_background():
        return True

    @staticmethod
    def default_system():
        return False

class Downloader(Wizard):
    'Dowloader'
    __name__ = 'metro_civica_downloader.downloader'
    start = StateView('metro_civica_downloader.downloader.form',
                      '',
                      [Button('Cancel', 'end', 'tryton-cancel'),
                       Button('Ok', 'download', 'tryton-ok')])

    download = StateTransition()
    
    def transition_download(self):
        Download.do_download(self.start.since,
                             self.start.until,
                             self.start.report,
                             system=self.start.system,
                             enqueue=self.start.background)
        return 'end'
    
class Download(ModelSQL, ModelView):
    'Download'
    __name__ = 'metro_civica_downloader.download'

    _states = {'readonly': True}

    state = fields.Selection([
        ('in process', 'In Process'),
        ('fail', 'Fail'),
        ('ok', 'Ok')
    ], 'State', states=_states)

    report = fields.Selection(_REPORT_OPTIONS, 'Report', states=_states)

    init_at = fields.Date('Init At', states=_states)
    end_at = fields.Date('End At', states=_states)

    timeout = fields.Integer('Timeout', states=_states)
    retries = fields.Integer('Retries', states=_states)
    filepath = fields.Char('File Path', states=_states)
    binary_name = fields.Function(fields.Char('Binary Name', states={'invisible': True}),
                                  'get_binary_name')
    binary = fields.Function(fields.Binary('Binary', filename='binary_name'),
                             'get_binary')
    system = fields.Boolean('System', states=_states)

    @fields.depends('filepath')
    def get_binary_name(self, name):
        if not self.filepath:
            return None
        return os.path.basename(self.filepath)
    
    @fields.depends('filepath')
    def get_binary(self, name):
        if self.filepath:
            return open(self.filepath, 'rb').read()
        else:
            return ''

    @staticmethod
    def default_system():
        return True

    @staticmethod
    def default_retries():
        return 6
    
    @staticmethod
    def default_timeout():
        configuration = Configuration(1)
        return configuration.timeout

    @staticmethod
    def default_state():
        return 'in process'

    @classmethod
    def download(cls, *nargs):
        args = list(nargs)
        Date = Pool().get('ir.date')
        report = args.pop(0)
        until = False
        if len(args) == 1:
            at = datetime.strptime(args[0], '%Y-%m-%d').date()
        else:
            at = Date.today() - timedelta(days=1)
        init_at = at
        end_at = at
        return cls.do_download(init_at, end_at, report, system=True)
            
    @classmethod
    def do_download(cls, init_at, end_at, report, system=True, enqueue=True):
        configuration = Configuration(1)
        default_timeout = configuration.timeout

        timeout = ((end_at - init_at).days + 1) * default_timeout
        record, = cls.create([{
            'init_at': init_at,
            'end_at': end_at,
            'report': report,
            'timeout': timeout,
            'state': 'in process',
            'system': system
        }])
        if enqueue:
            with Transaction().set_context(queue_name='metro_civica_downloader'):
                cls.__queue__.process(record)
        else:
            record.process()

    def process(self):
        if self.state != 'in process':
            return

        cb = {
            'liquidacion-usos-transporte': self.reporteLiquidacionUsosTransporte
        }
        return cb[self.report]()

    def reporteLiquidacionUsosTransporte(download):
        Date = Pool().get('ir.date')
        configuration = Configuration(1)

        try:
            with apcv.sesion(configuration.username,
                             configuration.password) as ctx:
                apcv.set_timeout(ctx, download.timeout)
                init_at = datetime.combine(download.init_at, datetime.min.time())
                end_at = datetime.combine(download.end_at, datetime.max.time())
                body = apcv.reporteLiquidacionUsosTransporte(
                    ctx, init_at, end_at
                )
                filename = "liquidacion-usos-transporte-{date_at}-al-{date_to}.xlsx".format(
                    date_at=download.init_at,
                    date_to=download.end_at)
                store_path = configuration.store_path if download.system else configuration.user_store_path
                filepath = os.path.join(store_path, filename)
                download.filepath = filepath
                with open(filepath, 'wb') as f:
                    for data in body:
                        f.write(data)

                if configuration.metro_civica_process and download.system:
                    ArchivosImportados = Pool().get('metro_civica.archivos_importados')
                    archivos_importados = ArchivosImportados.search([('ruta', '=', filepath)])
                    if archivos_importados:
                        ArchivosImportados.write(archivos_importados, {'reimportar': True})
                    MetroCivica = Pool().get('metro_civica.metro_civica')
                    MetroCivica.create_total_dia()

                download.state = 'ok'
        except Exception as e:
            logger.error('sincronizando reporteLiquidacionUsosTransporte', exc_info=True)
            download.retries -= 1
            if download.retries > 0:
                with Transaction().set_context(queue_scheduled_at=timedelta(minutes=1),
                                               queue_name='metro_civica_downloader'):
                    Download.__queue__.process(download)
            else:
                download.state = 'fail'
        finally:
            download.save()
