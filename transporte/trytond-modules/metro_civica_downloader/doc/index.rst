Metro Civica Downloader Module
##############################

Este modulo permite la descarga de reportes de la plataforma de civica.

Adiciona al menu:

 * Metro Civica/Configuracion/Descargas: usuario y clave
 * Metro Civica/Descargas: lista de colas

Requerimientos
==============

 * trytond-workers : se requiere los trabajadores de tryton para el backoff
 * metro_civica tener la configuracion de la ruta de almacenamiento indicada
 * pip install git+https://repo.santra.com.co:3000/Santra/cliente_apps_civica --trusted-host repo.santra.com.co:3000

   
cron reporte 'liquidacion-usos-transporte'
------------------------------------------

Por defecto se toma el dia anterior en curso para enviar.

adicionar lanzador con los siguientes parametros:

* **model** : metro_civica_downloader.download
* **function** : download
* **args** : ['liquidacion-usos-transporte']

si se desea indicar la fecha a sincronizar, ejemplo:

**args**: ['liquidacion-usos-transporte', '2019-04-21']


   
cron cambio de contrasena automatico
------------------------------------------

adicionar lanzador con los siguientes parametros:

* **model** : metro_civica_downloader.configuration
* **function** : renew_password
* **args** : ['']
