================================
Metro Civica Downloader Scenario
================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install metro_civica_downloader::

    >>> config = activate_modules('metro_civica_downloader')
