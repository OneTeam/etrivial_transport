# This file is part of metro_civica_downloader.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


try:
    from trytond.modules.metro_civica_downloader.tests.test_metro_civica_downloader import suite
except ImportError:
    from .test_metro_civica_downloader import suite

__all__ = ['suite']
