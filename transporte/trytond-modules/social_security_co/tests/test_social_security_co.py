# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class SocialSecurityCoTestCase(ModuleTestCase):
    'Test Social Security Co module'
    module = 'social_security_co'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            SocialSecurityCoTestCase))
    return suite
