# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.social_security_co.tests.test_social_security_co import suite
except ImportError:
    from .test_social_security_co import suite

__all__ = ['suite']
