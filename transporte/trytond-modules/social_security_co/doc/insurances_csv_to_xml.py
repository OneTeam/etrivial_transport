"""
Convierte el archivo aseguradoras.csv en insurances_data.xml

Esto para importar las aseguradoras colombianas en el presente modulo
Nota: No se usa este script porque las aseguradoras se deben poder modificar
por los usuarios y el tryton no permite la edición de registros creados
a partir de los archivos xml
"""

import csv
model = 'social_security_co.insurance'
aseguradoras = open('aseguradoras.csv','r')
insureances_data = open('insureances_data.xml','w')

csvReader = csv.reader(aseguradoras)
header = next(csvReader)
name = header.index('Nombre')
code = header.index('Codigo')
active = header.index('Activo')
arl = header.index('arl')
afp = header.index('afp')
afc = header.index('afc')
ccf = header.index('ccf')
eps = header.index('eps')

insureances_data.write("""<?xml version="1.0"?>
<tryton>
    <data>
""")
for row in csvReader:
    insureances_data.write( """
        <record model="{model}"  id="{id}">
            <field name="name">{name}</field>
            <field name="code">{code}</field>
            <field name="active">{active}</field>
            <field name="arl">{arl}</field>
            <field name="afp">{afp}</field>
            <field name="afc">{afc}</field>
            <field name="ccf">{ccf}</field>
            <field name="eps">{eps}</field>
        </record>
""".format(model=model,
           id='insurance_' + row[code].strip(),
           name=row[name].strip(),
           code=row[code].strip(),
           active=row[active].strip(),
           arl=row[arl].strip(),
           afp=row[afp].strip(),
           afc=row[afc].strip(),
           ccf=row[ccf].strip(),
           eps=row[eps].strip(),
           )
        )
insureances_data.write("""
    </data>
</tryton>
"""
)
aseguradoras.close()
insureances_data.close()
