# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


from trytond.pool import Pool
from .insurance import *
from .company import *
from .employee import *

__all__ = ['register']


def register():
    Pool.register(
        Insurance,
        Company,
        Employee,
        PositionEmployee,
        JobCentre,
        EmployeeConfiguration,
        PreviewInsureanceToImport,
        module='social_security_co', type_='model')
    Pool.register(
        ImportInsureanceColombia,
        module='social_security_co', type_='wizard')
    Pool.register(
        module='social_security_co', type_='report')
