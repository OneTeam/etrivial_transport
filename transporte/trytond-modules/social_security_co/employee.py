# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Created on 16/05/2018

@author: francisco.garcia@santra.com.co
'''
from trytond.model import ModelSQL, ModelView, fields
import logging
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.model.modelsingleton import ModelSingleton
from trytond.exceptions import UserError, UserWarning

logger = logging.getLogger(__name__)

__all__ = ['Employee','PositionEmployee','JobCentre','EmployeeConfiguration']

class Employee(ModelSQL, ModelView):
    'Employee'
    
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'
    
    eps = fields.Many2One('social_security_co.insurance','Eps',
                        required=True,
                        domain=[('active','=',True),('eps','=',True)],
                        help="Empresa Prestadora de Salud"
                        )
    number_eps_beneficiary = fields.Integer('Cantidad de Beneficiarios EPS',
                                        help='Cantidad de Beneficiarios en la Empresa Prestadora de Salud'
                                        )
    afc = fields.Many2One('social_security_co.insurance','AFC',
                        required=True,
                        domain=[('active','=',True),('afc','=',True)],
                        help="Administradora del Fondo de Cesantias"
                        )
    arl = fields.Function(fields.Char('ARL',
                        help="Administradora de Riesgos Laborales"
                        ),'on_change_with_arl')
    ccf = fields.Function(fields.Char('CCF',
                        help="Caja de Compensación Familiar"
                        ),'on_change_with_ccf')
    number_ccf_beneficiary = fields.Integer('Cantidad de Beneficiarios CCF',
                                        help='Cantidad de Beneficiarios en la Caja de Compensación Familiar'
                                        )
    afp = fields.Many2One('social_security_co.insurance','AFP',
                        states={'required':~Eval('pensioned')},
                        domain=[('active','=',True),('afp','=',True)],
                        help="Administrador de Fondo de Pensiones"
                        )
    contract = fields.Char('Código Contrato',
                    help="Codigo del Contrato",
                    required=True,
                    readonly=True,
                    )
    position = fields.Many2One('company.position_employee', 'Cargo',
                                    required=True,
                                    domain=[('active','=',True),('company','=',Eval('company'))],
                                    help="Cargo que ocupa el empleado")
    note = fields.Char('Nota',
                    help="Observaciones",
                    required=False,
                    readonly=False,
                    )
    cause_of_end = fields.Char('Causa de Retiro',
                    help="Motivo de Finalización del Contrato",
                    states={'required':Bool(Eval('end_date'))},
                    readonly=False,
                    )
    job_centre = fields.Many2One('company.job_centre', 'Centro de Trabajo',
                                    required=True,
                                    domain=[('active','=',True),('company','=',Eval('company'))],
                                    help="Centro de trabajo")
    salary = fields.Numeric('Salario',
                        digits=(16, Eval('currency_digits', 2)),
                        required=True,
                        #states={'invisible':True},
                        depends=['currency_digits'])
    currency_digits = fields.Function(fields.Integer('Currency Digits',states={'invisible':True}),
                                                    'on_change_with_currency_digits')
    pensioned = fields.Boolean('Pensionado',
                            help='Indica si esta pensionado o no'
                            )
    have_contract_vigent = fields.Function(fields.Boolean('Tiene Contratos Vigentes',help="El tercero no debe tener contratos vigentes"),
                                        'on_change_with_have_contract_vigent')
    
    
    
    @fields.depends('company')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2
    
    @fields.depends('party')
    def on_change_with_have_contract_vigent(self, name=None):
        'verifica si el tercero tiene contratos vigentes'
        if self.party:
            domain=[('party','=',self.party),('end_date','=',None)]
            if self.id:
                domain += [('id','!=',self.id)]
                #es posible que se necesite verificar no contratado
                #para cada empresa y no para todas las empresas. Revisar
            records = self.search(domain)
            if records:
                return True
            else:
                return False

    @fields.depends('have_contract_vigent')
    def on_change_with_party(self, name=None):
        'Si el tercero tiene contratos vigentes lo limpia'
        if self.have_contract_vigent:
            return -1

    @fields.depends('company')
    def on_change_with_arl(self,name=None):
        if self.company:
            if hasattr(self.company.arl, 'name'):
                return self.company.arl.name
            else:
                raise UserError("La empresa no tiene configurada la ARL")    
    
    @fields.depends('company')
    def on_change_with_ccf(self,name=None):
        if self.company:
            return self.company.ccf.name
    
    @classmethod
    def _new_contract(cls):
        Configuration = Pool().get('company.employee.configuration')
        configuration = Configuration.get_singleton()
        Sequence = Pool().get('ir.sequence')
        secuence = configuration.contract_employee_sequence
        if secuence:
            return Sequence.get_id(int(secuence))
    
    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('contract'):
                values['contract'] = cls._new_contract()
        return super(Employee, cls).create(vlist)
    
    def pre_validate(self):
        super(Employee, self).pre_validate()
        self.check_have_contract_vigent()
        self.check_start_date()
        
    @fields.depends('party')
    def check_have_contract_vigent(self):
        if self.have_contract_vigent:
            raise UserError('Tercero Invalido')
    
    def check_start_date(self):
        if not self.start_date:
            raise UserError('Se Requiere Fecha de Inicio')
    
    
class PositionEmployee(ModelSQL, ModelView):
    'Puestos de trabajo'
    
    __name__ = 'company.position_employee'
    
    company = fields.Many2One('company.company', 'Empresa',
                            required=True,
                            help="Empresa a la cual pertence este Puesto de Trabajo.")
    code = fields.Char('Código',
                    help="Codigo del Puesto de Trabajo",
                    required=True,
                    )
    name = fields.Char('Nombre',
                    help="Nombre del Puesto de trabajo",
                    required=True,
                    )
    other_name = fields.Char('Nombre Alternativo',
                    help="Puede ser usado para nombres legales",
                    required=False,
                    )
    active = fields.Boolean('Activo')
    description = fields.Char('Descripción',
                            help="Descripción",
                            )
    
    @staticmethod
    def default_company():
        return Transaction().context.get('company')
    
class JobCentre(ModelSQL, ModelView):
    '''Centro de Trabajo.'''
    
    __name__ = 'company.job_centre'
    
    company = fields.Many2One('company.company', 'Empresa',
                            required=True,
                            help="Empresa a la cual pertence este Centro de Trabajo.")
    code = fields.Char('Código',
                    help="Codigo del Centro de Trabajo",
                    required=True,
                    )
    name = fields.Char('Nombre',
                    help="Nombre del Centro de Trabajo",
                    required=True,
                    )
    other_name = fields.Char('Nombre Alternativo',
                    help="Puede ser usado para nombres legales",
                    required=False,
                    )
    active = fields.Boolean('Activo')
    description = fields.Char('Descripción',
                            help="Descripción",
                            )

    @staticmethod
    def default_company():
        return Transaction().context.get('company')
    
    
class EmployeeConfiguration(ModelSingleton, ModelSQL, ModelView):
    'Party Configuration'
    __name__ = 'company.employee.configuration'
    
    contract_employee_sequence = fields.Many2One('ir.sequence', 'Secuencia para Contratos',
                                    domain=[
                                        ('code', '=', 'company.contract_employee'),
                                        ],
                                    help="Usado para generar el Codigo del Contrato de los empleados.")
