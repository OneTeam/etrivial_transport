# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Administradoras (y Aseguradoras) del sistema de Seguridad Social
Created on 16/05/2018

@author: francisco.garcia@santra.com.co
'''
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport, StateAction
import logging
import csv
from io import StringIO
from trytond.pool import Pool

logger = logging.getLogger(__name__)

__all__ = ['Insurance','ImportInsureanceColombia','PreviewInsureanceToImport']

class Insurance(ModelSQL,ModelView):
    """Aseguradoras"""
    __name__ = "social_security_co.insurance"
    
    name = fields.Char('Nombre',
                    help="Nombre de la Aseguradora",
                    required=True)
    code = fields.Char('Codigo',
                    help="Codigo de la Aseguradora",
                    required=True
                    )
    active = fields.Boolean('Activa',
                        help="Aseguradora Activa?"
                        )
    party = fields.Many2One('party.party', 'Empresa',
                            required=False,
                            help="Tercero que representa esta aseguradora.")
    description = fields.Char('Descripcion',
                            help="Descripcion",
                            )
    arl = fields.Boolean('ARL',
                         help="Indica si esta entidad administra Risgos Laborales")
    afp = fields.Boolean('AFP',
                         help="Indica si esta entidad administra Fondo de Pensiones")
    afc = fields.Boolean('AFC',
                         help="Indica si esta entidad administra Fondo de Cesantias")
    ccf = fields.Boolean('CCF',
                         help="Indica si esta entidad es Caja de Compensacion Familiar")
    eps = fields.Boolean('ARL',
                         help="Indica si esta entidad es una Entidad Prestadora de Salud")
    
    @classmethod
    def __setup__(cls):
        super(Insurance, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_social_security_co_insurance_name', Unique(table, table.name),
                'seguridad_social.aseguradoras: el nombre de la aseguradora debe ser unico'),
            ('unique_social_security_co_insurance_code', Unique(table, table.code),
                'seguridad_social.aseguradoras: el codigo de la aseguradora debe ser unico'),
        ]

class PreviewInsureanceToImport(ModelView):
    'Previsializacion de Aseguradoras a Importar'
    __name__ = 'social_security_co.insurance.import.preview'
    
    insurances = fields.Binary('Aseguradoras a importar',
                               help="En la carpeta de documentacion del modulo\n"
                               "se encuentra el archivo csv a importar",
                               required=True,
                                )
    encoding = fields.Selection([('utf-8','utf-8')],'Codificacion',
                             required=True,
                             )
    
class ImportInsureanceColombia(Wizard):
    """Importar Aseguradoras colombianas que vienen con el modulo."""
    __name__ = 'social_security_co.insurance.import'
    
    start = StateView('social_security_co.insurance.import.preview',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel',default=True),
            Button('Importar', 'import_', 'tryton-executable'),
            ])
    import_ = StateTransition()
    
    
    def transition_import_(self):
        Insurance = Pool().get('social_security_co.insurance')
        file = StringIO(self.start.insurances.decode(self.start.encoding))
        csv_data = csv.reader(file)
        csv_header = next(csv_data)
        data = list(csv_data)
        file.close()
        len_import = Insurance.import_data(csv_header,data)
        logger.info("Total insurance import:{l}".format(l=len_import))
        return 'end'
