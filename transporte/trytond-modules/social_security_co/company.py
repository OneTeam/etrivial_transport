# This file is part of social_security_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta

__all__ = ['Company',]


class Company(ModelSQL, ModelView):
    'Company'
    __metaclass__ = PoolMeta
    __name__ = 'company.company'
    
    ccf = fields.Many2One('social_security_co.insurance','CCF',
                        required=True,
                        domain=[('active','=',True),('ccf','=',True)],
                        help="Caja de Compensación Familiar"
                        )
    arl = fields.Many2One('social_security_co.insurance','ARL',
                        required=True,
                        domain=[('active','=',True),('arl','=',True)],
                        help="Administradora de Riesgos Laborales"
                        )
