# This file is part of metro_civica_fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
import logging
from trytond.pool import Pool,PoolMeta
from trytond.wizard import Wizard, StateTransition, StateView, Button
from _datetime import timedelta, datetime, date
#from dbus import bus

__all__ = ['LinkWithFleet','Metro_Civica','UsosTotalesDiarios',
           'UsosTotalesDiariosTotalizador', 'UsosTotalesDiariosTotalizadorForm']

class Metro_Civica(ModelSQL, ModelView):
    "Metro Civica"
    
    __name__ = "metro_civica.metro_civica"
    __metaclass__ = PoolMeta
    
    fleet_vehicle = fields.Many2One('fleet.vehicle','Vehiculo de Flota')
    
    @classmethod
    def link_with_fleet(cls, records=[]):
        """Vincula metro_civica con fleet."""

        Vehicle = Pool().get('fleet.vehicle')
        if not records:
            records = cls.search([('fleet_vehicle','=',None)])
        
        vehicles_records = Vehicle.search_read([], fields_names=['licence_plate', 'id'])
        vehicles = {x['licence_plate']:x['id'] for x in vehicles_records}
        for record in records:
            plate = Vehicle.plate_to_valid_plate(
                cls.equipo_a_placa(record.equipo)
            )
            if plate in vehicles:
                record.fleet_vehicle = vehicles[plate]
        cls.save(records)
        return

    @classmethod
    def procesar_registros_importados(cls, registros):
        super(Metro_Civica, cls).procesar_registros_importados(registros)
        if registros:
            cls.link_with_fleet(records=registros)
            fechas = sorted([registro.fecha for registro in registros])
            UsosTotalesDiarios.create_totales(min(fechas), max(fechas), registros=registros)

class LinkWithFleet(Wizard):
    """Vincula metro_civica con fleet.vehicle."""
    
    __name__ = 'metro_civica.metro_civica.link_with_fleet'
    start = StateTransition()
    
    def transition_start(self):
        Metro_Civica = Pool().get('metro_civica.metro_civica')
        Metro_Civica.link_with_fleet()
        return 'end'


class UsosTotalesDiariosTotalizadorForm(ModelView):
    """UsosTotalesDiarios - Totalizador Form"""

    __name__ = 'metro_civica_fleet.totalizador.form'

    since = fields.Date('Since', required=True)
    until = fields.Date('Until', required=True)

    
class UsosTotalesDiariosTotalizador(Wizard):
    """Totalizador"""

    __name__ = 'metro_civica_fleet.totalizador'
    start = StateView('metro_civica_fleet.totalizador.form', '',
                      [Button('Salir', 'end', 'tryton-cancel'),
                       Button('Generar', 'generate', 'tryton-ok')])

    generate = StateTransition()

    def transition_generate(self):
        UsosTotalesDiarios.create_totales(self.start.since, self.start.until)
        return 'end'


class UsosTotalesDiarios(ModelSQL, ModelView):
    """Suma las cantidades de usos según si son plenos conductor, plenos usuarios o integrados.
    
    Metro_Civica Totales Diarios
    """
    
    
    __name__ = 'metro_civica.usos_totales_diarios'
    fecha = fields.Date("Fecha") 
    equipo = fields.Char('Equipo',
                         help="Es diferente al equipo de Metro,\n"
                         "Es la placa extraida de equipo.")
    conductor = fields.Integer('Conductor')
    plenos = fields.Integer('Plenos')
    integrados = fields.Integer('Integrados')
    totales = fields.Integer('Totales')
    fleet_vehicle = fields.Many2One('fleet.vehicle','Vehículo de flota')
    reconteo_totales = fields.Integer('Reconteo')
    #integrados = fields.Function(fields.Integer('integrados'),'totales_tipos') 
        
    @classmethod
    def create_totales(cls,fecha_inicial,fecha_final, registros=[]):
        """Crea totales para un rango de fechas dadas.
        
        @param fecha_inicial: date object o str en formato %Y-%m-%d
        @param fecha_fina: date object o str en formato %Y-%m-%d
        """
        tales = None
        #cambiar fechas si fueron pasadas en texto
        if isinstance(fecha_inicial, str):
            fecha_inicial = date.fromordinal(datetime.strptime(fecha_inicial,'%Y-%m-%d').toordinal())
        if isinstance(fecha_final, str):
            fecha_final= date.fromordinal(datetime.strptime(fecha_final,'%Y-%m-%d').toordinal())
        
        dias = (fecha_final - fecha_inicial).days
   
        vlist = []
           
        for x in range(0,dias+1):
            dia = fecha_inicial + timedelta(days=x)
            datos = cls.totales_tipos(dia, registros=registros)
            for dato in datos:                
                anterior = cls.existe_totales(dato,datos[dato]['fecha'])
                                            
                if anterior:
                    #anterior debería devolver solo uno y no habria que recorrerlo
                    #solo usar anterior[0] o anterior = anterior[0]
                    for sumar in anterior: 
                        sumas = sumar.conductor+sumar.integrados+sumar.plenos
                        reconteo_totales = sumar.reconteo_totales
                    
                    suma_totales = datos[dato]['conductor'] + datos[dato]['plenos'] + datos[dato]['integrados']
                    if sumas != suma_totales:     
                        datos[dato]['reconteo_totales'] =  reconteo_totales + 1
                        records = cls.write([anterior][0],datos[dato])
                        tales = True                    
                else:                                                    
                    values = {'equipo' : dato,
                              'fecha' : datos[dato]['fecha'],
                              'conductor' : datos[dato]['conductor'],
                              'plenos' : datos[dato]['plenos'],
                              'integrados' : datos[dato]['integrados'],
                              'totales' : datos[dato]['totales'],
                              'reconteo_totales' : 0,
                              'fleet_vehicle' : datos[dato]['fleet_vehicle'],
                              }
                    vlist.append(values)
                    tales = False
    
        if tales == True:
            cls.save(records)
        elif tales == False: 
            records = cls.create(vlist)
            cls.save(records)
        
    @classmethod
    def totales_tipos(cls,fecha, registros=[]):
        Metro_Civica = Pool().get('metro_civica.metro_civica')
        if registros:
            dias = list(filter(lambda r: r.fecha == fecha if r.fecha else False, registros))
        else:
            dias = Metro_Civica.search([('fecha','=',fecha)])


        #puesto en set para no usar un diccionario con valores que no se usan.
        #aunque tambien se puede dejar en diccionario pero adicionar todos los
        #registros de las placas en el valor para luego solo recorrer por placa
        #los registros de la placa y no todos los registros de dias por cada palaca
        placas = set()
        for dia in dias:            
            placas.add(Metro_Civica.equipo_a_placa(dia.equipo))
            
        totales = {}
        for placa in placas:                
            totales[placa] = {}                        
            for dia in dias:
                if Metro_Civica.equipo_a_placa(dia.equipo) == placa:
                    if dia.tipo_trayecto == "Integrado SIT":
                        if 'integrados' in totales[placa]:                            
                            totales[placa]['integrados'] = dia.total_cantidad + totales[placa]['integrados']
                        else:
                            totales[placa]['integrados'] = dia.total_cantidad
                    elif 'integrados' not in totales[placa]:                        
                        totales[placa]['integrados'] = 0
                    if dia.tipo_trayecto+str(dia.valor_liquidado) == "Pleno0":
                        if 'conductor' in totales[placa]:
                            totales[placa]['conductor'] = dia.total_cantidad + totales[placa]['conductor']
                        else:
                            totales[placa]['conductor'] = dia.total_cantidad
                    elif 'conductor' not in totales[placa]:                        
                        totales[placa]['conductor'] = 0           
                    if dia.tipo_trayecto == "Pleno" and dia.valor_liquidado > 0:
                        if 'plenos' in totales[placa]:
                            totales[placa]['plenos'] =  dia.total_cantidad + totales[placa]['plenos'] - dia.operacionesmetro_cantidad
                        else:
                            totales[placa]['plenos'] =  dia.total_cantidad - dia.operacionesmetro_cantidad
                    elif 'plenos'  not in totales[placa]:                        
                        totales[placa]['plenos'] = 0
                    if 'totales' in totales[placa]:
                        totales[placa]['totales'] = totales[placa]['totales'] + dia.total_cantidad - dia.operacionesmetro_cantidad
                    else:
                        totales[placa]['totales'] = dia.total_cantidad - dia.operacionesmetro_cantidad
                    totales[placa]['fleet_vehicle'] = dia.fleet_vehicle
                    totales[placa]['fecha'] = dia.fecha
        
        return totales;        
                               
                    
    @classmethod
    def existe_totales(cls,equipo,fecha):
        busqueda = cls.search([('fecha','=',fecha),
                               ('equipo','=', equipo)])    
        if busqueda:
            return busqueda
        else:
            return False
            
        
                       
        
        
            
        
        
    
