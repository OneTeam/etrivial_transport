# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.metro_civica_fleet.tests.test_metro_civica_fleet import suite
except ImportError:
    from .test_metro_civica_fleet import suite

__all__ = ['suite']
