# This file is part of metro_civica_fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .metro_civica import *
__all__ = ['register']


def register():
    Pool.register(
        Metro_Civica,
        UsosTotalesDiarios,
        UsosTotalesDiariosTotalizadorForm,
        module='metro_civica_fleet', type_='model')
    Pool.register(
        LinkWithFleet,
        UsosTotalesDiariosTotalizador,
        module='metro_civica_fleet', type_='wizard')
    Pool.register(
        module='metro_civica_fleet', type_='report')
