# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging

from trytond.modules.datos_sonar.sonar import SonarMixin, SonarAuthMixin
from trytond.model import ModelSQL, ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button,\
                            StateAction
from trytond.pool import Pool

logger = logging.getLogger(__name__)

__all__ = ['MobileGroup','SincronizarMobileGroup',
        'SyncGeofencesAllGroups',
        ]

class MobileGroup(SonarAuthMixin,SonarMixin,ModelSQL,ModelView):
    '''    Informacion extraida usando la funcion GET_MobileGroups de la API.
       
    '''
    __name__ = "datos_sonar.mobile_group"
    _rec_name = "gName"
    
    gId = fields.BigInteger('gId')
    gName = fields.Char('gName')
    mList = fields.One2Many('datos_sonar.mobiles','group','Vehiculos')
    geofences = fields.One2Many('datos_sonar.geofence','groupID','Geocercas')
    itineraries = fields.One2Many('datos_sonar.itinerary','It_Idgroup','Itinerarios')
    
    @classmethod
    def sincronizarMobileGroupWsdl(cls):
        """Sincroniza los Grupos de Vehiculos con la Api de SonarAvl.
        
        """
        attrib_response = 'MobileGroup'
        attrib_id = 'gId'
        logger.info("Inicio sincronizacion de MobileGroup")
        mobile_groups = cls.consultar_mobiles_group()
        if mobile_groups:
            for auth,element in mobile_groups.items():
                logger.info("element:{} type:{}".format(element,type(element)))
                for Group in element[attrib_response]:
                    exist = cls.search(domain=[(attrib_id,"=",getattr(Group, attrib_id))])
                    if exist:
                        cls.updateGroup(exist,Group,auth)
                    else:
                        cls.createGroup(Group,auth)
        else:
            logger.error('error en la conexion WSDL')
        logger.info("Fin sincronizacion de Mobiles")
    
    @classmethod
    def consultar_mobiles_group(cls):
        """Consulta en la API Sonar la información de los grupos
        con sus vehículos
        
        Devuelve un diccionario donde la key es el auth.name y los
         valores el listado de mobiles de ese auth.
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        return cls.request_object_multi_auth(auth_field = configuration.wsdl_auth,
                                         target='gList',
                                         request='GET_MobileGroups',
                                         params={
                                              'FleetId':''
                                              },
                                         url=configuration.wsdl_url,
                                         log_=False)


    @classmethod
    def updateGroup(cls,record,group,auth):
        group_dict = cls.GroupToDict(group,auth)
        values = {}
        for k,v in group_dict.items():
            if getattr(record[0], k) != v:
                values[k] = v
        if len(values) > 0 :
            cls.write(record,values)
            cls.save(record)
    
    @classmethod
    def GroupToDict(cls,group,auth):
        prototype = {
            'gId' : group.gId,
            'gName' : group.gName,
            'sonar_auth' : auth,
            }
        return prototype

    @classmethod
    def createGroup(self,group,auth): 
        records = self.create([self.GroupToDict(group,auth)])
        self.save(records)
        
    @classmethod
    def allIdsFrom_gId(cls):
        """Devuelve un Diccionario con gId como key y el id como valor para todos los grupos."""
        values = cls.search_read([('id','>',0)],fields_names=['id','gId'])
        return {x['gId']:x['id'] for x in values}
    
    @classmethod
    def get_group_geofences(cls,GroupId,auth):
        """Devuelve las geocercas para un grupo"""
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        return cls.request_object(auth_field=configuration.wsdl_auth,
                                  target='groupGeofenceList.GroupGeofence',
                                  request='GET_GroupGeofences',
                                  params={
                                          'GroupId':GroupId
                                          },
                                  auth=auth,
                                  url=configuration.wsdl_url
                                  )
    
    @classmethod
    def sync_geofecences_all_groups(cls,log_=False):
        """Soncroniza las geocercas de todos los grupos.
        
        @todo: Revisar si update esta funcionando
        """
        Geofence = Pool().get('datos_sonar.geofence')
        
        groups_dict = cls.allIdsFrom_gId()
        values_create = []
        fields = ['groupID','g_index','g_name','g_active']
        
        if log_:
            logger.info("Inicio sincronizacion de las geocercas de todos los Grupos")
        
        groups = cls.search_read(['id','>',0],fields_names=['id','gId','sonar_auth'])
        
        for group in groups:
            response = cls.get_group_geofences(group['gId'],group['sonar_auth'])
            for geofence in response:
                record = Geofence.search_read([
                        ('groupID','=',groups_dict[geofence['groupID']]),
                        ('g_index','=',geofence['g_index'])
                        ])
                if record:
                    #actualiza la geocerca si ha cambiado algun campo
                    for field in fields:
                        if record[0][field] != geofence[field]:
                            value = {x:getattr(geofence, x) for x in fields}
                            value['groupID'] = groups_dict[value['groupID']]
                            Geofence.update(record[0]['id'],value)
                            break
                else:
                    value = {x:getattr(geofence, x) for x in fields}
                    value['groupID'] = groups_dict[value['groupID']]
                    values_create.append(value)
            
        records = Geofence.create(values_create)
        Geofence.save(records)
        if log_:
            logger.info("Fin sincronizacion de las geocercas de todos los Grupos")
        
       


class SincronizarMobileGroup(Wizard):
    'Sincronizar Mobiles'
    
    __name__ = 'datos_sonar.mobile_group.sincronizar'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sync_groups','tryton-go-next')
                    ]
                    )
    sync_groups = StateTransition()
    sync_group_to_mobiles = StateAction('datos_sonar.mobiles_sincronizar_group_wizard')
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar los Grupos de los vehiculos con SonarAVL?'}
    
    def transition_sync_groups(self):
        MobileGroup = Pool().get('datos_sonar.mobile_group')
        MobileGroup.sincronizarMobileGroupWsdl()
        return 'sync_group_to_mobiles'
    
    def transition_sync_group_to_mobiles(self):
        return 'end'

class SyncGeofencesAllGroups(Wizard):
    'Sincronizar Geocercas para todos los Grupos'
    
    __name__ = 'datos_sonar.mobile_group.sync_geofences_all_groups'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sync_geofences','tryton-go-next')
                    ]
                    )
    sync_geofences = StateTransition()
    #sync_group_to_mobiles = StateAction('datos_sonar.mobiles_sincronizar_group_wizard')
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar las geocercas de todos los grupos con SonarAVL?'}
    
    def transition_sync_geofences(self):
        MobileGroup = Pool().get('datos_sonar.mobile_group')
        MobileGroup.sync_geofecences_all_groups()
        return 'end'
