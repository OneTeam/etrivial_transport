# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime
import logging

from trytond.modules.datos_sonar.sonar import SonarConstantSync

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from _functools import partial

#from trytond.transaction import Transaction

logger = logging.getLogger(__name__)


__all__ =["PassengersLog"]

class PassengersLog(SonarConstantSync,ModelSQL,ModelView):
    '''
    PassengersLog
    Informacion extraida usando la funcion GET_PassengersCounter de la API.
    ejemplo:
    (PassengersLog){
               regId = 302788130
               mId = "AAB5"
               p_station = "Flores de la Colina"
               p_total = "Y"
               p_doorId = 1
               p_ingresos = 0
               p_salidas = 0
               p_bloqueos = 0
               gps_UTC = "2018-03-05 22:07:30"
               system_UTC = "2018-03-05 22:07:21"
            }
    Estructura sacada de la api
     <PassengersLog>
            <regId>long</regId>
            <mId>string</mId>
            <p_station>string</p_station>
            <p_total>string</p_total>
            <p_doorId>int</p_doorId>
            <p_ingresos>int</p_ingresos>
            <p_salidas>int</p_salidas>
            <p_bloqueos>int</p_bloqueos>
            <gps_UTC>string</gps_UTC>
            <system_UTC>string</system_UTC>
          </PassengersLog>   
    '''
    __name__ = "datos_sonar.passengers_log"
    _rec_name = "regId"
    
    #Campos requeridos para SonarConstantSync
    _field_time = 'system_utc'
    _sonar_constant_sync_request = 'GET_PassengersCounter'
    _sonar_constant_sync_target = 'passengerslogList'
    _sonar_constant_sync_array = 'PassengersLog'
    
    regId = fields.BigInteger("regId")
    mId = fields.Char("mId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    p_station = fields.Char("p_station:estacion")
    p_total = fields.Char("p_total:totalizado?",size=1)
    p_doorId = fields.Integer("p_doorId:puerta")
    p_ingresos = fields.Integer("p_ingresos")
    p_salidas = fields.Integer("p_salidas")
    p_bloqueos = fields.Integer("p_bloqueos")
    gps_UTC = fields.DateTime("gps_UTC:Tiempo")
    system_utc = fields.DateTime("system_UTC:Tiempo", required=True)
    gps_tz = fields.Function(fields.DateTime("Tiempo GPS Colombia"),"obtener_gps_tz")
    system_tz = fields.Function(fields.DateTime("Tiempo System Colombia"),"obtener_system_tz")
    
    
    def obtener_gps_tz(self,field):
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        zona_horaria = configuration.zona_horaria if configuration.zona_horaria != None else 'America/Bogota'
        try:
            from pytz import timezone
            gps_utc = self.gps_UTC.astimezone(timezone('UTC'))
            return gps_utc.astimezone(timezone(zona_horaria))
        except ModuleNotFoundError:
            logger.eror("Error: Module pytz NotFound")
            return None
    
    def obtener_system_tz(self,field):
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        zona_horaria = configuration.zona_horaria if configuration.zona_horaria != None else 'America/Bogota'
        try:
            from pytz import timezone
            return self.system_utc.astimezone(timezone(zona_horaria))
        except ModuleNotFoundError:
            logger.eror("Error: Module pytz NotFound")
            return None
    
    @classmethod
    def __setup__(cls):
        super(PassengersLog, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('regid_key', Unique(table, table.regId),
                'El regId debe ser unico')
        ]
        

    @classmethod
    def syncRespuestaAPI(cls,passengers_log_response):
        """sincroniza los eventos de un passengers_log_response 
        
        Este metodo es llamado por el modulo syncTask para la sincronizacion
        @param pasangers_log_response: objeto devuelto por la API de la funcion GET_EventsHistory()
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        logger.error("TIEMPO 4: %s.",datetime.today())
        data=[]
        fields_names=[
            "regId",
            "mId", #para relacionar el Mobile en el sistema
            "mobil_id",
            "p_station",
            "p_total",
            "p_doorId",
            "p_ingresos",
            "p_salidas",
            "p_bloqueos",
            "gps_UTC",
            "system_utc",
            ]
        for passengerslog in passengers_log_response.PassengersLog:
            if not cls.existe_regid(passengerslog.regId):
                prototype = [
                passengerslog.regId,
                passengerslog.mId,
                Mobiles.mDescriptionFromMid(passengerslog.mId),
                passengerslog.p_station,
                passengerslog.p_total,
                passengerslog.p_doorId,
                passengerslog.p_ingresos,
                passengerslog.p_salidas,
                passengerslog.p_bloqueos,
                passengerslog.gps_UTC,
                passengerslog.system_UTC,
                ]
                data.append(prototype)
        errores=cls.import_data(fields_names, data)
        logger.info("Reporte Importacion: %s.",errores)
        logger.info("TIEMPO 9: %s.",datetime.today())
        return True
    
    @classmethod
    def existe_regid(cls,regId):
        """Si existe un evento con este regID devuelve verdadero de lo contrario falso.
        
        """
        regids = cls.search([('regId','=',regId)])
        existe = True if (len(regids) > 0 ) else False
        return existe
    
    @classmethod
    def relacionarConMobile(cls):
        """Relaciona los datos importados con mobiles
        
        """
        logger.info("Inicio relacionarConMobile: %s.",datetime.today())
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        allIdsMidDict = Mobiles.allIdsMidDict()
        
        objetos = cls.search_read([('mobil_id','=',None)])
        for objeto in objetos:
            objeto['mobil_id']=allIdsMidDict[objeto['mId']]
            record = cls.search([('id','=',objeto['id'])])
            cls.write(record,objeto)
            cls.save(record)
        logger.info("Fin relacionarConMobile: %s.",datetime.today())
        
    @classmethod
    def total_for_mobil(cls,init_time,end_time,mobil_id,partial=False):
        """Devuelve una tupla con la cantidad de ingresos, salidas y bloqueos
        para el mobil entre el init_time y el end_time. si no se indica partial
        se devuelve la suma de los totalizados (p_total)
        """
        domain_ = [('mobil_id','=',mobil_id),
                   ('system_utc','>=',init_time),
                   ('system_utc','<=',end_time),
                   ]
        if not partial:
            domain_.append(('p_total','!=',''))
        logs = cls.search(domain_)
        inputs = 0
        outputs = 0
        blocked = 0
        for record in logs:
            inputs += record.p_ingresos or 0
            outputs += record.p_salidas or 0
            blocked += record.p_bloqueos or 0
        return inputs,outputs,blocked
