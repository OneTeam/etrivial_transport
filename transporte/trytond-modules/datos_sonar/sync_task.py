# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from datetime import datetime, timedelta, date
import logging
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.wizard.wizard import Wizard, StateView, Button, StateTransition
from trytond.model.descriptors import dualmethod


logger = logging.getLogger(__name__)

__all__ =["SyncTask",'CrearTareas','CrearTareasParametrizar','EliminarTareasNoEjecutadas','EliminarTareasNoEjecutadasStart']

class SyncTask(ModelSQL,ModelView):
    """Modelo que administra la sincronizacion de los datos jalados de la Plataforma SonarAVL.
    
    -Contendra las sincronizaciones segmentadas por dia que se deberan realizar
    -Campo prioridad de tipo numerico que a menor numero mayor prioridad.
    -tendra una funcion que se ejecutara en el cron cada minuto la cual ejecutara las tarea(s)
     correspondiente
    -tendra una funcion actualizara dia a dia las tareas necesarias (esta es solo para importaciones grandes)
     La idea es que habra una tarea por vehiculo por dia por cada funcion a sincronizar eso nos
     da una cantidad aproximada de 365(dias)x10(eventos)=3650(tareas)XVehiculo anualmente
    """
    __name__ = 'datos_sonar.sync_task'
    __rec_name = 'id'
    
    #diccionario con los modelos que se sincronizan por dias
    _model_for_day_sync = {
        'datos_sonar.counter_log':{'field_time':'system_utc'},
        'datos_sonar.passengers_log':{'field_time':'system_utc'},
        'datos_sonar.itinerary_history':{'field_time':'inittime'},
        }
    
    #diccionario con los modelos que se sincronizan de forma constante
    _model_for_constant_sync ={
        'datos_sonar.event_location':{'field_time':'system_gmt'},
        'datos_sonar.geo_passenger_log':{ 'field_time': 'system_time'}
        }
    
    #diccionario con los modelos validos para sincronizar
    _modelos_validos_para_sincronizar = {}
    _modelos_validos_para_sincronizar.update(_model_for_constant_sync)
    _modelos_validos_para_sincronizar.update(_model_for_day_sync)
    
    
#                 'datos_sonar.drivers':{
#                     'funcion':'GET_Drivers_v2',
#                     'parametros':{'User':None,'Password':None,'FleetId':None,'DrvId':None,'DrvDoc':None},
#                     'objeto_respuesta':'Drivers_v2Response',
#                     'objeto_array_datos':'drList',
#                     'objeto_lista_de_array':'Driver',
#                     'objeto_elemento_de_lista':'Driver',
#                     },
#                 'datos_sonar.drivers_history':{
#                     'funcion':'GET_DriversHistory',
#                     'parametros':{'User':None,'Password':None,'mId':None,'UTC_datetime_init':None,'UTC_datetime_end':None},
#                     'objeto_respuesta':'DriversHistoryResponse',
#                     'objeto_array_datos':'driverslogList',
#                     'objeto_lista_de_array':'DriversLog',
#                     'objeto_elemento_de_lista':'DriversLog',
#                     },                  
#                 }
    
    datetime_init = fields.DateTime("Fecha inicial",
                                help="Fecha inicial para la sincronizacion del evento, las fechas que se usan para dia lo ideal es que la hora sea 00:00:00",
                                required=True
                                )
    datetime_end = fields.DateTime('Fecha final',
                                help='Fecha final, la idea es que las importaciones sean maximo por dia, por lo cual si no se pone fecha final es mejor'
                                )
    mobile_id = fields.Many2One('datos_sonar.mobiles',"Mobil",
                            help="vehiculo, para sincronizar mobiles seleccione cualquiera que se ignorara",
                            required=True,
                            )  
    
    model = fields.Char('model',
                    help='modelo en el cual se ejecutara la sincronizacion ej: datos_sonar.event_location',
                    required=True,
                    )
    counter_id = fields.Integer('counterid',
                            help = "usado solo cuando el modelo es CounterLog, el valor va de 1 a 5"#@todo convertir en select
                            )
    itinerary_id = fields.Integer('itinearary_id',
                                help="Usado solo cuando el modelo es datos_sonar.itineary_history"
                                )

    priority = fields.Integer('Priority',
                            help="prioridad para la tarea, a mayor numero menor prioridad",
                            required=True,
                            )
    active = fields.Boolean('Activa',
                        help='La tarea esta activa?')
    done = fields.Boolean ('Realizada',
                        help='La tarea esta realizada? solo lo manipula el cron',
                        readonly=True,
                        )
    time_executed = fields.DateTime('Fecha de ejecucion',
                                readonly=True,
                                )
    registros_sincronizados = fields.Integer('Registros Sincronizados',
                                            help="Cantidad de registros que dovolvio la Api",
                                            readonly=True,
                                            ) 

    @classmethod
    def default_active(cls):
        return True
    
    @classmethod
    def __setup__(cls):
        super(SyncTask, cls).__setup__()
        cls._buttons.update({
                'execute_task': {
                    'invisible': Eval('done'),
                    'help': "Ejecutar la Tarea",
                    'icon': 'tryton-forward'
                    },
                })
    
    @dualmethod
    @ModelView.button
    def execute_task(cls,tasks):
        """Ejecuta Tareas"""
        Configuration = Pool().get("datos_sonar.configurations")
        Itinerary = Pool().get("datos_sonar.itinerary")
        
        configuration = Configuration.get_singleton()
        for task in tasks:
            respuesta = cls.ejecutar_sincronizacion(
                    model=task.model,
                    mId=task.mobile_id.mId,
                    UTC_date_time_init=task.datetime_init.strftime(configuration.wsdl_timeformat),
                    UTC_date_time_end=task.datetime_end.strftime(configuration.wsdl_timeformat),
                    counterId=task.counter_id,
                    Itinerary=Itinerary.It_id_from_id(task.itinerary_id)
                    )
            if not respuesta:
                logger.error("Error en la ejecucion de la tarea {} para el modelo {} del dia  {} para el mobil {} a las: {}.".format(
                    task.id,task.model,task.datetime_init,task.mobile_id,datetime.today()))
            else:
                task.done = True
                task.time_executed = respuesta['hora_ejecucion']
                task.registros_sincronizados = respuesta['registros_sincronizados']
        cls.save(tasks)
   
    
    @classmethod
    def gran_creador_tareas(cls,datetime_init,datetime_end,mobiles_id="",modelos="",prioridad_tarea=""):
        'Crea tareas para todos los vehiculos con los argumentos pasados para grandes lapsos de tiempo'
        
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        
        pasos = timedelta(days=configuration.cantidad_dias_por_tarea)
        inicio = datetime_init
        while True:
            end = inicio + pasos
            end = datetime_end if end > datetime_end else end
            SyncTask.creador_tareas(inicio,end,mobiles_id,modelos,prioridad_tarea)
            inicio = end
            if inicio >= datetime_end:
                break

    @classmethod
    def creador_tareas(cls,datetime_init,datetime_end,mobiles_id="",modelos=None,prioridad_tarea=""):
        """ Crea tareas para todos los vehiculos con los argumentos pasados
        
        si no se indica model creará tarea para todos los modelos
        @param mobiles:    lista con los mobiles objetivo de la creacion de tareas, si no se indican
        se creara para todos los mobiles.
        @param modelos: lista con los modelos objetivo de la creacion de tareas, si no se indican
        se creara para todos los modelos. 
        @attention: No debería haber diferencia de más de 5 días entre datetime_init y datetime_end
            porque algunas funciones en la API no lo permiten.
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        Itinerary = Pool().get('datos_sonar.itinerary')
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()

        if modelos:
            modelos_no_validos = [x for x in modelos if x not in cls._modelos_validos_para_sincronizar.keys()]#modelos invalidos
            if modelos_no_validos:
                logger.info("Los modelos:{m} no son validos y no se creara tareas para estos.".format(m=modelos_no_validos))
            modelos = [x for x in modelos if x in cls._modelos_validos_para_sincronizar.keys()]#solo modelos validos
        else:
            modelos = cls._modelos_validos_para_sincronizar.keys() # si no se indica ningun modelo 

        if not mobiles_id:
            mobiles_id = Mobiles.allIds() #si no se indica mobiles_id
        
        
        if not prioridad_tarea:
            prioridad_tarea = configuration.prioridad_tareas_pordefecto

        values = []
        for mobileid in mobiles_id:
            for model in modelos:
                if model == "datos_sonar.counter_log":
                    for counterid in range(configuration.inicio_rango_countersid,configuration.fin_rango_countersid + 1):
                        prototype = {
                            'datetime_init':datetime_init,
                            'datetime_end':datetime_end,
                            'mobile_id':mobileid,
                            'model':model,
                            'priority': prioridad_tarea,
                            'active': True,
                            'done' : False,
                            'counter_id' : counterid,
                            }
                        values.append(prototype)
                if model == 'datos_sonar.itinerary_history':
                    itineraries_dic = Itinerary.id_It_id_dict(invert=True)
                    logger.info(itineraries_dic)
                    for itinerary_id in itineraries_dic.keys():
                        logger.info("Itinerary_id: {i}".format(i=itinerary_id))
                        prototype = {
                        'datetime_init':datetime_init,
                        'datetime_end':datetime_end,
                        'mobile_id':mobileid,
                        'model':model,
                        'priority': prioridad_tarea,
                        'active': True,
                        'done' : False,
                        'itinerary_id': itinerary_id,
                        }
                        values.append(prototype)
                else:
                    prototype = {
                        'datetime_init':datetime_init,
                        'datetime_end':datetime_end,
                        'mobile_id':mobileid,
                        'model':model,
                        'priority': prioridad_tarea,
                        'active': True,
                        'done' : False,
                        }
                    values.append(prototype)
        records=cls.create(values)
        cls.save(records)
        return True
    
    @classmethod
    def executeTask(cls):
        """Este metodo sera llamado por el cron para realizar las tareas de sincronizacion
        
        llama la siguiente tarea (debe estar activa y no realizada) ordenada segun la prioridad,
        luego segun la fecha inicial y luego segun el mobil
        SELECT * FROM "datos_sonar_sync_task" st WHERE (st.active = TRUE) AND (st.done != TRUE)
        ORDER BY st.priority ASC,st.datetime_init ASC,st.mobile_id ASC;
        la ejecuta y al finalizar  la actualiza a realizada y guarda la fecha de ejecucion y
        almacena la cantidad de datos devueltos por la API.
        """
        Configuration = Pool().get("datos_sonar.configurations")
        configuration = Configuration.get_singleton()
        cantidad_tareas = configuration.cantidad_de_tareas_ejecucion_cron
        tasks = cls.search_read(
            domain=[('active','=',True),('done','!=',True)],
            limit=cantidad_tareas,
            order=[('priority','ASC'),('datetime_init','DESC'),('mobile_id','ASC')]
            )
        if len(tasks)>0:    
            logger.info("TIEMPO 1: %s.",datetime.today())
            Mobiles = Pool().get("datos_sonar.mobiles")
            Itinerary = Pool().get("datos_sonar.itinerary")
            all_mobiles_id_mid = Mobiles.allIdsMidDict()
            wsdl_timeformat = configuration.wsdl_timeformat
            for task in tasks:
                #logger.info("tasks: {t}".format(t=tasks))
                logger.info("limit_task: {l}".format(l=cantidad_tareas))
                logger.info("Inicio ejecucion tarea %s para el modelo %s del dia  %s para el mobil %s a las: %s",task['id'],task['model'],task['datetime_init'],task['mobile_id'],datetime.today())
                respuesta = cls.ejecutar_sincronizacion(
                    model=task['model'],
                    mId=all_mobiles_id_mid[task['mobile_id']],
                    UTC_date_time_init=task['datetime_init'].strftime(wsdl_timeformat),
                    UTC_date_time_end=task['datetime_end'].strftime(wsdl_timeformat),
                    counterId=task['counter_id'],
                    Itinerary=Itinerary.It_id_from_id(task['itinerary_id'])
                    )
                if not respuesta:
                    logger.error(f"Error en la ejecucion de la tarea {task['id']}"
                                 f"para el modelo {task['model']} del dia  "
                                 f"{task['datetime_init']} para el mobil "
                                 f"{task['mobile_id']} a las: {datetime.today()}.")
                else:
                    task['done'] = True
                    task['time_executed']=respuesta['hora_ejecucion']
                    task['registros_sincronizados']=respuesta['registros_sincronizados']
                    record=cls.search(domain=[('id','=',task['id'])])
                    cls.write(record, task)
                    cls.save(record)
                    Transaction().commit()
        else:
            logger.info("SIN TAREAS DE SINCRONIZACION POR REALIZAR {tiempo}".format(tiempo=datetime.today()))
    
    @classmethod
    def sync_for_day(cls,log_=False):
        """Ejecuta sincronizaciones que se deben hacer de forma diaria."""
        cls.constant_sync(cls._model_for_day_sync,log_=log_)
    
    @classmethod
    def is_time_for_constant_sync(cls):
        Configuration = Pool().get("datos_sonar.configurations")
        configuration = Configuration.get_singleton()
        if configuration.init_time_no_constant_sync and configuration.period_no_constant_sync:
            init = configuration.init_time_no_constant_sync
            period = configuration.period_no_constant_sync
            d_today = date.today()
            d_yesterday = d_today - timedelta(hours=24)
            dt_today = datetime.today()
            
            today_init_time = datetime.fromordinal(d_today.toordinal()) + init
            today_end_time = today_init_time + period
            
            yesterday_init_time = datetime.fromordinal(d_yesterday.toordinal()) + init
            yesterday_end_time = yesterday_init_time + period
            
            logger.info("Evaluando si es tiemp de sincronizacion "
                        "Hora: {h},"
                        "periodo ayer: {ai}-{af}, periodo hoy {hi}-{hf}".format(h=datetime.now(),
                                                                    ai=yesterday_init_time,
                                                                    af=yesterday_end_time,
                                                                    hi=today_init_time,
                                                                    hf=today_end_time))
            
        
            if yesterday_init_time <= dt_today <= yesterday_end_time:
                return False
            elif today_init_time <= dt_today <= today_end_time:
                return False
        
        return True
    
    @classmethod
    def constant_sync(cls,modelos_validos=None,log_=False):
        """Ejecuta sincronizacion constante en los modelos
        
        @attention: Es llamado por el cron para realizar las tareas de sincronizacion.
        """
        if not cls.is_time_for_constant_sync():
            if log_:
                logger.info("Tiempo de suspension de sincronizacion constante")
            return
        if log_:
            logger.info("iniciando constant_sync")
        if not modelos_validos:
            modelos_validos = cls._model_for_constant_sync
        for model in modelos_validos.keys():
            Model = Pool().get(model)
            Model.constant_sync_for_all_mobiles(log_=True)
    
    
    @classmethod
    def ejecutar_sincronizacion(cls,model,mId,UTC_date_time_init,UTC_date_time_end,counterId="",Itinerary=""):
        """Ejecuta una sincronizacion al WSDL con los parametros pasados
        
        UTC_date_time_init y UTC_date_time_end deben ser pasados en el formato que pide el WSDL (%Y-%m-%d %H:%M:%S)
        Devuelve un diccionario: {
                        'registros_sincronizados': len(respuesta.infoWSDLModelo[model]['objeto_lista_de_array']),
                        'hora_ejecucion':datetime.today(),
                        }
        o False si falla la ejecucion
        """
        Configuration = Pool().get("datos_sonar.configurations")
        Model = Pool().get(model)
        
        configuration = Configuration.get_singleton()
        #def execute_sync(cls,mId,auth_field,url,UTC_date_time_init,UTC_date_time_end,auth='',other_params={},log_=False):
        other_params = {}
        if counterId:
            other_params.update({'CounterID':counterId})
        elif Itinerary:
            other_params.update({'Itinerary':Itinerary})
        if not configuration.wsdl_auth:
            logger.info("Debe configurar credenciales de Sonar")
            return
        return Model.execute_sync(mId=mId,
                           auth_field=configuration.wsdl_auth,
                           UTC_date_time_init=UTC_date_time_init,
                           UTC_date_time_end=UTC_date_time_end,
                           auth='',
                           other_params=other_params,
                           url=configuration.wsdl_url,
                           log_=False
                           )
#                 
#         elif model == "datos_sonar.drivers":
#             infoWSDLModelo[model]['parametros']['FleetId']=""
#             infoWSDLModelo[model]['parametros']['DrvId']=""
#             infoWSDLModelo[model]['parametros']['DrvDoc']=""
#            
#         elif model == "datos_sonar.drivers_history":
#             infoWSDLModelo[model]['parametros']['mId']=mId
#             infoWSDLModelo[model]['parametros']['UTC_datetime_init']=UTC_date_time_init
#             infoWSDLModelo[model]['parametros']['UTC_datetime_end']=UTC_date_time_end    

            
class CrearTareasParametrizar(ModelView):
    ' Crear Tareas para Todos los vehiculos Start (para wizard).'
    
    __name__ = 'datos_sonar.sync_task.crear.parametrizar'
    
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                help="Vehiculos para los cuales se creará la tarea, si no se indica ninguno se usarán todos",
                                required=False,
                                )
    modelos = fields.Selection('selection_modelos',    'Modelos',
                                help="Modelos para los cuales se creará la tarea, si no se indica ninguno se usarán todos",
                                required=False,
                                )
    inicio = fields.DateTime('Fecha Inicio',
                        help="Desde cuando?",
                        required = True,
                        domain=[('inicio','<',Eval('fin'))],
                        depends=['fin']
                        )
    
    fin = fields.DateTime('fecha final',
                        help="Hasta cuando?",
                        required = True,
                        domain=[('fin','>',Eval('inicio'))],
                        depends=['inicio']
                        )
    prioridad_tarea = fields.Integer('Prioridad',
                                    help="prioridad para las tareas",
                                    required = True,
                                    )
    
    dias = fields.Function(fields.Integer('Dias a Sincronizar',domain=[('dias',">=",'0'),('dias','<=',4)]),'on_change_with_dias')
    
    @classmethod
    def default_prioridad_tarea(cls):
        return 10
        
    @fields.depends('inicio','fin')
    def on_change_with_dias(self,name=None):
        dias_a_sincronizar = (self.fin - self.inicio).days if self.inicio and self.fin else -1
        return dias_a_sincronizar
    
    @classmethod
    def selection_modelos(cls):
        SyncTask = Pool().get('datos_sonar.sync_task')
        modelos = list( (x,x) for x in SyncTask._modelos_validos_para_sincronizar.keys()) 
        return modelos

class CrearTareas(Wizard):
    'Crear tareas de sincronizacion wizard'
    __name__ = 'datos_sonar.sync_task.crear'
    
    start_state = 'parametrizar'
    
    parametrizar = StateView('datos_sonar.sync_task.crear.parametrizar',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Crear Tareas', 'crear','tryton-go-next')
                    ]
                    )
    crear = StateTransition()
    
    def transition_parametrizar(self):
        return 'crear'
    
    def transition_crear(self):
        SyncTask = Pool().get('datos_sonar.sync_task')
        mobiles = [x.id for x in self.parametrizar.mobiles]
        modelos = [self.parametrizar.modelos] if self.parametrizar.modelos else None
        SyncTask.gran_creador_tareas(self.parametrizar.inicio,self.parametrizar.fin,mobiles,modelos,self.parametrizar.prioridad_tarea)
        return 'end'


class EliminarTareasNoEjecutadasStart(ModelView):
    'Eliminar Tareas No Ejecutadas Start (para wizard).'
    
    __name__ = 'datos_sonar.sync_task.eliminar_no_ejecutadas.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea Eliminar Las tareas no ejecutadas?')
        

class EliminarTareasNoEjecutadas(Wizard):
    'Eliminar tareas no ejecutadas wizard'
    __name__ = 'datos_sonar.sync_task.eliminar_no_ejecutadas'
    
    start_state = 'confirmar'
    
    confirmar = StateView('datos_sonar.sync_task.eliminar_no_ejecutadas.start',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Eliminar Tareas', 'eliminar','tryton-delete')
                    ]
                    )
    eliminar = StateTransition()
    
    def transition_confirmar(self):
        return 'eliminar'
    
    def transition_eliminar(self):
        logger.info("eliminando tareas de syncronizacion")
        SyncTask = Pool().get('datos_sonar.sync_task')
        tareas_no_ejecutadas = SyncTask.search([('done','=',False)])
        SyncTask.delete(tareas_no_ejecutadas)
        return 'end'
