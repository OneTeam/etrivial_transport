# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
import logging
from datetime import datetime
from trytond.wizard.wizard import Wizard, Button, StateView, StateTransition
from trytond.transaction import Transaction


#from trytond.application import app #no esta arrancando el servidor con esta linea

logger = logging.getLogger(__name__)

__all__ = ['MobileAddon','SincronizarMobileaddons',
        'SincronizarMobileaddonsStart',]

class MobileAddon(ModelSQL,ModelView):
    """
    Informacion extraida usando:
    mAddons = client.factory.create('Mobile_addons')
    print mAddons

    (Mobile_addons){
       ad_type = None
       ad_description = None
       ad_deviceId = None
     }    
    """
    __name__ = "datos_sonar.mobileaddons"
    _rec_name = "ad_description"
    
    ad_type = fields.Char("ad_type")
    ad_description = fields.Char("ad_description")
    ad_deviceId = fields.Char("ad_deviceId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    
    @classmethod
    def sincronizarMobileaddonsWsdl(cls,log_=False):
        """ sincronizar Mobileaddons con Api Sonar"""
        Mobiles = Pool().get('datos_sonar.mobiles')
        mobiles_responses = Mobiles.consultar_mobiles()
        if log_:
            logger.info("Inicio sincronizacion de addons")
        if mobiles_responses:
            for mobiles in mobiles_responses.values():
                for Mobile in mobiles.Mobile:
                    if Mobile.mAddons:
                        mobil_id=Mobiles.idMobile_From_mId(Mobile.mId)
                        #remover mAddons relacionados al vehiculo
                        cls.delete(cls.search(domain=[('mobil_id','=',mobil_id)]))
                        for mAddon in Mobile.mAddons.Mobile_addons:
                            cls.CreateMobileaddons(mAddon,mobil_id)
        else:
            logger.error('error en la conexion WSDL %s')
        if log_:
            logger.info("Fin sincronizacion de Mobileaddons")
    
    @classmethod
    def CreateMobileaddons(cls,mAddon,mobil_id):
        mobileaddon=cls.MobileaddonToDict(mAddon, mobil_id)
        records = cls.create([mobileaddon])
        cls.save(records)
        
    @classmethod
    def MobileaddonToDict(cls,mAddon,mobil_id):
        prototype = {
            "ad_type" : mAddon.ad_type,
            "ad_description" : mAddon.ad_description,
            "ad_deviceId" : mAddon.ad_deviceId,
            "mobil_id" : mobil_id,
            }
        return prototype
    
class SincronizarMobileaddonsStart(ModelView):
    ' Sincronizar Mobileaddons Start (para wizard).'
    
    __name__ = 'datos_sonar.mobileaddons.sincronizar.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea Sincronizar los mobileaddons con SonarAVL?')
        
class SincronizarMobileaddons(Wizard):
    'Sincronizar Mobilesaddons'
    __name__ = 'datos_sonar.mobileaddons.sincronizar'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.mobileaddons.sincronizar.start',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        MobileAddons = Pool().get('datos_sonar.mobileaddons')
        MobileAddons.sincronizarMobileaddonsWsdl()
        return 'end'
