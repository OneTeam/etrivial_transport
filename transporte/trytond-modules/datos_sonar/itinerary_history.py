# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import csv
from io import BytesIO, StringIO
import logging
from decimal import *
from datetime import datetime, date, timedelta

from trytond.modules.datos_sonar.sonar import SonarConstantSync
from .graficos import *

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval, Bool, Not, PYSONEncoder
from trytond.transaction import Transaction

from sql.aggregate import Count, Sum, Max
from sql import Literal
from sql.operators import NotIn
from trytond.wizard.wizard import StateAction
from trytond.modules.datos_sonar import datos_sonar_lib




logger = logging.getLogger(__name__)

__all__ = ['ItineraryHistory','ItineraryHistoryPoint','ItineraryHistoryCsv',
           'ItineraryHistoryFilter','ItineraryHistoryFilterCsv',
           'ItineraryHistoryStadistic','ItineraryHistoryCsvStadistic',
           'ShowEvasion','ShowEvasionFilter','VirtualItineraryHistoryPoint',
           'VirtualItineraryHistory','VirtualItineraryHistoryParams',
           'CreateVirtualItineraryHistory','OpenEventsFromItinerarHistory',
           'QuantityItineraryHistory','QunatityItineraryHistoryParams',
           'VirtualItineraryHistoryDeleteDuplicatedParams',
           'VirtualItineraryHistoryDeleteDuplicated']

class MixinItineraryHistory(object):
    '''Mixin Itinerario Historico
    Se estan creando itinerarios historicos basados en EventLocation
    para esto se necesita replicar el Modelo ItineraryHistory pero
    para no copiar y pegar y centralizar los cambios se decide crear
    este mixin que será heredado por ItineraryHistory y
    VirtualItineraryHistory
    '''

    regId = fields.BigInteger("regId")
    mId = fields.Char("mId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    ItId = fields.Integer("ItId")
    itinerary = fields.Many2One("datos_sonar.itinerary","Itinerario")
    #Se almacena de forma statica este nombre por ser un registro historico y
    #que la referencia puede cambiar en el tiempo
    itinerary_name = fields.Char('itinerary_name')
    driver = fields.BigInteger("driver")
    auto = fields.Char("auto",size=1)
    timeUnit = fields.Char("timeUnit",size=1)
    running = fields.Char("running",size=1)
    close = fields.Char("close",size=1)
    violations = fields.Char("violations",size=1)
    leaveroute = fields.Char("leaveroute",size=1)
    canceled = fields.Char("canceled",size=1)
    inittime = fields.DateTime("inittime")
    report_init_time_co = fields.Function(fields.DateTime("init_time para Reportes:",
                                                          help="No usar en cliente"
                                                          ),'get_report_init_time_co')
    endtime = fields.DateTime("endtime")
    report_end_time_co = fields.Function(fields.DateTime("end_time para Reportes:",
                                                          help="No usar en cliente"
                                                          ),'get_report_end_time_co')
    regtime = fields.DateTime("regtime")
    userCanceled = fields.Char("userCanceled",size=1)
    comments = fields.Char("comments")
    totalIngresos = fields.Integer("totalIngresos")
    totalSalidas = fields.Integer("totalSalidas")
    totalBloqueos = fields.Integer("totalBloqueos")
    quantity_points = fields.Integer('Puntos',
                                    help="Cantidad de puntos recorridos")
    porcentage_points = fields.Numeric('% Cumplimiento',
                                    digits=(1,2),
                                    help="Porcentage de cumplimiento de los puntos"
                                    )
    
    def get_report_init_time_co(self,name=None):
        if self.inittime:
            return self.inittime - timedelta(hours=5)
    
    def get_report_end_time_co(self,name=None):
        if self.endtime:
            return self.endtime - timedelta(hours=5)
    
    @classmethod
    def search_rec_name(cls, name, clause):
        itid = ('ItId','=',clause[2].strip('%')) if clause[2].strip('%').isdigit() else ''
        domain = ['OR',
                ('mobil_id', ) + tuple(clause[1:]),
                ('itinerary_name', ) + tuple(clause[1:]),
                ]
        if itid:
            domain.append(itid)
        return domain

    def fault_points(self,names=False,index=False):
        """Devuelve una lista de tuplas con los puntos faltantes de la forma
        [(p_index,geofence_name),(...),...]
        si se indica names solo devuelve los nombres
        si se indica index solo devuelve los indices
        """
        poi = set([(x.point_index,x.geofence_name) for x in self.itinerary.ItPoints])
        pos = set([(x.p_index,x.geofence_name) for x in self.ItPointsLog])
        fault = sorted(poi - pos)
        if names:
            return [y for x,y in fault]
        if index:
            return [x for x,y in fault]
        return fault

    def movilited_passangers(self):
        """Devuelve la cantidad de pasajeros movilizados en todos los puntos
        """
        return sum([x.p_ingresos for x in self.ItPointsLog])
    
    @classmethod
    def get_quantity_for_day_for_mobil(cls,date_,mobil_id,itinerary_ids=[],minim_porcentage=0):
        """Devuelve la cantidad de itinerarios por dia por mobil
        @attention: metodo usado en el modulo public_transport
        """
        inittime , endtime = datos_sonar_lib.colombia_date2utctime(date_)
        domain_ = [('mobil_id','=',mobil_id),
                    ('inittime','>=',inittime),
                    ('endtime','<=',endtime),
                    ('porcentage_points','>=',Decimal(minim_porcentage) if minim_porcentage else Decimal(0))
                    ]
        if itinerary_ids:
            domain_.append([('itinerary','in',itinerary_ids)])
        return cls.search(domain_,count=True)
    
    @property
    def velocity_exceses(self):
        """Devuelve los excesos de velocidad del itinerario historico"""
        EventLocation = Pool().get('datos_sonar.event_location')
        Configuration = Pool().get('datos_sonar.configurations')
        
        configuration = Configuration.get_singleton()
        limit = configuration.limite_exceso_velocidad_itinerarios or 60
        
        if self.inittime and self.endtime and self.mobil_id:
            return EventLocation.get_velocity_limit_exceded(self.inittime,
                                                            self.endtime,
                                                            self.mobil_id,
                                                            limit,
                                                            count=True)
        else:
            return None

class ItineraryHistory(MixinItineraryHistory,SonarConstantSync,ModelSQL,ModelView):
    '''Itinerario Historico.'''

    __name__ = "datos_sonar.itinerary_history"
    _rec_name = "regId"

    #Campos requeridos para SonarConstantSync
    _field_time = 'inittime'
    _sonar_constant_sync_request = 'GET_ItinerariesHistory'
    _sonar_constant_sync_target = 'ItsLog'
    _sonar_constant_sync_array = 'ItLog'
    _sonar_constant_sync_subparams_mobil = True

    ItPointsLog = fields.One2Many('datos_sonar.itinerary_history_point','itinerary_history','ItPointsLog',
                                  add_remove=[('itinerary_history','=',Eval('id'))])


    @classmethod
    def __setup__(cls):
        super(ItineraryHistory, cls).__setup__()
        cls._order = [('inittime','DESC'),('mobil_id','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_ItineraryHistory_regId', Unique(table, table.regId),
                'datos_sonar.itinerary_history: solo debe haber un registro para un regId')
        ]
        #cambiado en octubre 19 del 2018
        #el campo ItId a itinerario_id y el campo ItId se definio como char
        #requiere intervencion manual en la base de datos con las consultas:
        #UPDATE datos_sonar_itinerary_history SET itinerary="ItId";
        #ALTER TABLE datos_sonar_itinerary_history DROP COLUMN "ItId";
        #Actualizar el modulo datos_sonar desde el tryton
        #UPDATE datos_sonar_itinerary_history SET "ItId" = datos_sonar_itinerary."It_id" FROM datos_sonar_itinerary  WHERE datos_sonar_itinerary_history.itinerary = datos_sonar_itinerary.id;
        

    @classmethod
    def recalculate_quantity_and_porcentage_points(cls,records,only_empty=False):
        if only_empty:
            records = [x for x in records if not(x.quantity_points and x.porcentage_points)]
        for record in records:
            if record.ItPointsLog and record.itinerary:
                record.quantity_points = len(record.ItPointsLog)
                quantity_it_points = len(record.itinerary.ItPoints)
                record.porcentage_points = Decimal(record.quantity_points/quantity_it_points).quantize(Decimal('0.00')) if (record.quantity_points and quantity_it_points) else Decimal(0)
        cls.save(records)
        
    @classmethod
    def recalculate_all_quantity_and_porcentage(cls,only_empty=False):
        """Recalcula la cantidad de puntos y porcentajes de cada itinerario para todos los itinerarios.
        
        O solo para los que estan vacios si only_empty.
        Como la cantidad de registros es muy grande este metodo no es funcional para todos los itinerarios.
        Se recomienda usar las consultas:
        UPDATE datos_sonar_itinerary_history AS a SET quantity_points = b.cantidad from (select itinerary_history,count(id) AS cantidad from datos_sonar_itinerary_history_point group by itinerary_history) AS b where id = b.itinerary_history ;
        
        UPDATE datos_sonar_itinerary_history AS f SET porcentage_points = e.porcentaje FROM
(
  SELECT a.id,trunc(a.quantity_points::numeric/c.cantidad::numeric,2) AS porcentaje from datos_sonar_itinerary_history AS a LEFT JOIN
    (
      select a.id,  b.cantidad from datos_sonar_itinerary as a LEFT JOIN (SELECT itinerary,count(id) as cantidad from datos_sonar_itinerary_point group by itinerary) as b on a.id = b.itinerary
    ) AS c ON c.id = a."itinerary"
) AS e WHERE f.id = e.id;
        """
        records = cls.search(['id','>',0])
        if records:
            cls.recalculate_quantity_and_porcentage_points(records, only_empty)

    @classmethod
    def syncRespuestaAPI(cls, ItsLog):
        """Sincroniza un ItsLog objeto devuelto de la api de Sonar Avl.
        Este metodo es llamado por el modulo syncTask para la sincronizacion
        """
        logger.error("TIEMPO 4: %s.", datetime.today())
        ItineraryHistoryPoint = Pool().get(
            'datos_sonar.itinerary_history_point')

        Mobiles = Pool().get('datos_sonar.mobiles')
        Itinerary = Pool().get('datos_sonar.itinerary')

        vlist = []
        # importando itinerary_history
        for it_log in ItsLog.ItLog:
            records = cls.search([('regId', '=', it_log.regId)])
            if records:
                ItineraryHistoryPoint.delete(
                    ItineraryHistoryPoint.search(
                        [('itinerary_history', '=', records[0].id)])
                    )
            itinerary = Itinerary.id_from_It_id(it_log.ItId)

            if hasattr(it_log.ItPointsLog, 'ItPointLog'):
                list_ItPoints = cls.ItPointsLog_to_list(
                    it_log.ItPointsLog.ItPointLog,
                    it_log.ItId,
                    [])
            else:
                list_ItPoints = []
            quantity_points = len(list_ItPoints)
            quantity_ItPoints = Itinerary.quantity_points(itinerary)
            if not (quantity_ItPoints and quantity_points):
                porcentage_points = Decimal(0)
            else:
                porcentage_points = Decimal(quantity_points/quantity_ItPoints).quantize(
                    Decimal('0.00'))
            values = {
                "regId": it_log.regId,
                "mId": it_log.mId,
                "mobil_id": Mobiles.idMobile_From_mId(it_log.mId),
                "ItId": it_log.ItId,
                "itinerary": itinerary,
                'itinerary_name': Itinerary.It_name_from_It_id(it_log.ItId),
                "driver": it_log.driver,
                "auto": it_log.auto,
                'timeUnit': it_log.timeUnit,
                'running': it_log.running,
                'close': it_log.close,
                'violations': it_log.violations,
                'leaveroute': it_log.leaveroute,
                'canceled': it_log.canceled,
                'inittime': it_log.inittime
                if it_log.inittime != '0000-00-00 00:00:00' else None,
                'endtime': it_log.endtime
                if it_log.endtime != '0000-00-00 00:00:00' else None,
                'regtime': it_log.regtime
                if it_log.regtime != '0000-00-00 00:00:00' else None,
                'userCanceled': it_log.userCanceled,
                'comments': it_log.comments,
                'totalIngresos': it_log.totalIngresos,
                'totalSalidas': it_log.totalSalidas,
                'totalBloqueos': it_log.totalBloqueos,
                'ItPointsLog': [('create', list_ItPoints)]
                if hasattr(it_log.ItPointsLog, 'ItPointLog') else [],
                'quantity_points': quantity_points,
                'porcentage_points': porcentage_points,
                }
            if records:
                cls.write(records, values)

            else:
                vlist.append(values)
        records = cls.create(vlist)
        cls.save(records)
        logger.info("Reporte Importacion: {c}.".format(c=len(records)))
        logger.info("TIEMPO 9: %s.", datetime.today())
        return True

    @classmethod
    def id_from_regId(cls,regId):
        """Devuelve el id de un itinerario historico para el regId dado."""
        record = cls.search_read([('regId','=',regId)],fields_names=['id'])
        if record:
            return record[0]['id']
    
    @classmethod
    def ItPointsLog_to_list(cls,ItPointsLog,ItId,ignore_points=[]):
        """Convierte un ItPointsLog devuelto por la Api de SonarAvl en una lista de diccionarios."""
        
        Itinerary = Pool().get('datos_sonar.itinerary')
        Itinerary_point = Pool().get('datos_sonar.itinerary_point')
        data=[]
        for itpoint in ItPointsLog:
            if itpoint.p_index in ignore_points:
                continue
            else:
                ignore_points.append(itpoint.p_index)
            prototype = {
                'p_index': itpoint.p_index,
                'p_violation': itpoint.p_violation,
                "p_error": itpoint.p_error,
                "p_realtime": itpoint.p_realtime if itpoint.p_realtime != '0000-00-00 00:00:00' else None,
                "p_gpstime": itpoint.p_gpstime if itpoint.p_gpstime != '0000-00-00 00:00:00' else None,
                "p_expectedtime": itpoint.p_expectedtime if itpoint.p_expectedtime != '0000-00-00 00:00:00' else None,
                "p_difference": itpoint.p_difference,
                "p_total": itpoint.p_total,
                "p_ingresos": itpoint.p_ingresos,
                "p_salidas": itpoint.p_salidas,
                "p_bloqueos": itpoint.p_bloqueos,
                'geofence_name': Itinerary_point.geofence_name_from_index_and_itinerary(itpoint.p_index,
                                                                                        Itinerary.id_from_It_id(ItId))
                }
            data.append(prototype)
        return data

    @classmethod
    def subparam_for_constant_sync(cls,mId='',mobil_id=''):
        Itinerary = Pool().get('datos_sonar.itinerary')
        
        itineraries = Itinerary.get_all_ids(It_id=True)
        return ('ItId','Itinerary',itineraries)


class VirtualItineraryHistory(MixinItineraryHistory,ModelSQL,ModelView):
    """ItinerarioHistorico Virtual"""

    __name__ = "datos_sonar.virtual_itinerary_history"

    ItPointsLog = fields.One2Many('datos_sonar.virtual_itinerary_history_point','itinerary_history','ItPointsLog',
                                  add_remove=[('itinerary_history','=',Eval('id'))]
                                  )
    

    @classmethod
    def __setup__(cls):
        super(VirtualItineraryHistory, cls).__setup__()
        cls._order = [('inittime','DESC'),('mobil_id','ASC')]
#         table = cls.__table__()
#         cls._sql_constraints += [
#             ('unique_vItineraryHistory_regId', Unique(table, table.regId),
#                 'datos_sonar.vitinerary_history: solo debe haber un registro para un regId')
#         ]
    
    @classmethod
    def info_of_duplicates(cls,date_):
        """ Devuelve una lista de tuplas con los itinerarios duplicados
        cada tupla tiene la forma (mobil_id,inittime,main_id) donde el
        main_id es el identificador del itinerario que tiene mas puntos y mayor
        porcentage de cumplimiento
        """
        cursor = Transaction().connection.cursor()
        inittime, endtime = datos_sonar_lib.colombia_month2utctime(date_.year,date_.month)
        table = cls.__table__()
        
        columns = [table.mobil_id,
                   table.inittime,
                   Max(table.quantity_points + table.porcentage_points),
                   Count(table.id).as_('quantity')]
        group_by_ = [table.mobil_id,
                    table.inittime]
        where_ = ((table.inittime >=inittime) & (table.endtime <= endtime))
        grouped_select = table.select(*columns,where=where_,group_by=group_by_)
        k = tuple(grouped_select)
        text = k[0] % tuple(["'{}'".format(x) for x in k[1]])
        duplicates_select = f"SELECT * FROM ({text}) AS a where a.quantity > 1"
        cursor.execute(duplicates_select)
        return cursor.fetchall()


    @classmethod
    def delete_duplicates(cls,date_):
        """Elimina los itinerarios historicos virtuales repetidos
         para el dia (date_)
        1. Halla los itinerarios repetidos (self.info_of_duplicates)
        2. Por cada itinerario elimina todos excepto el culla suma de puntos y 
         porcentage conincida con el entregado por self.info_of_duplicates
        """
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        if isinstance(date_,str):
            date_ = datetime.fromisoformat(date_)
            
        duplicates = cls.info_of_duplicates(date_)
        for mobil_id,inittime,max_sum,_ in duplicates:
            columns = [table.id,
                       ]
            where_ = ((table.mobil_id == mobil_id) & (table.inittime == inittime) \
                      & (table.quantity_points + table.porcentage_points == Literal(max_sum)))
            not_delete = table.select(*columns,where = where_,limit=1)
            cursor.execute(*not_delete)
            not_delete = cursor.fetchall()
            if not_delete:
                cls.delete(cls.search([('inittime','=',inittime),
                                       ('mobil_id','=',mobil_id),
                                       ('id','not in',not_delete[0])]))

class MixinItineraryHistoryPoint(ModelSQL,ModelView):
    '''Mixin Punto historico de itinerario.
    Se estan creando itinerarios historicos basados en EventLocation
    para esto se necesita replicar el Modelo ItineraryHistoryPoint pero
    para no copiar y pegar y centralizar los cambios se decide crear
    este mixin que será heredado por ItineraryHistoryPoint y
    VirtualItineraryHistoryPoint
    '''
    
    p_index = fields.Integer("p_index")
    #Se almacena de forma statica este nombre por ser un registro historico y
    #que la referencia puede cambiar en el tiempo
    geofence_name = fields.Char('Geocerca')
    p_violation = fields.Char("p_violation",size=1)
    p_error = fields.Integer("p_error")
    p_realtime = fields.DateTime("p_realtime")
    p_gpstime = fields.DateTime("p_gpstime")
    p_expectedtime = fields.DateTime("p_expectedtime")
    p_difference = fields.Integer("p_difference")
    p_total = fields.Char("p_total",size=1)
    p_ingresos = fields.Integer("p_ingresos")
    p_salidas = fields.Integer("p_salidas")
    p_bloqueos = fields.Integer("p_bloqueos") 




class ItineraryHistoryPoint(MixinItineraryHistoryPoint,ModelSQL,ModelView):
    '''Punto historico de itinerario--.'''

    __name__ = "datos_sonar.itinerary_history_point"
    _rec_name = "id"

    itinerary_history = fields.Many2One("datos_sonar.itinerary_history",
                                        "itinerary_history",
                                        ondelete='CASCADE')
    

    @classmethod
    def __setup__(cls):
        super(ItineraryHistoryPoint, cls).__setup__()

        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_ItineraryHistoryPoint_p_index_and_itinerary_history', Unique(table, table.p_index,table.itinerary_history),
                'datos_sonar.itinerary_history_point: solo debe haber un registro para un p_index y itinerary_history')
        ]

    @classmethod
    def exist_point(cls,p_index,itinerary_history_id):
        """Devuelve Verdadero si existe un punto con el p_index y el itinerary_history dados, falso en cualquier otro caso."""
        record = cls.search([('p_index','=',p_index),('itinerary_history','=',itinerary_history_id)])
        return True if len(record) > 0 else False


class VirtualItineraryHistoryPoint(MixinItineraryHistoryPoint,ModelSQL,ModelView):
    '''Virtual Punto historico de itinerario Virtual.'''

    __name__ = "datos_sonar.virtual_itinerary_history_point"

    itinerary_history = fields.Many2One("datos_sonar.virtual_itinerary_history",
                                        "itinerary_history",
                                        ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super(VirtualItineraryHistoryPoint, cls).__setup__()

        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_ItineraryHistoryPoint_p_index_and_itinerary_history', Unique(table, table.p_index,table.itinerary_history),
                'datos_sonar.itinerary_history_point: solo debe haber un registro para un p_index y itinerary_history')
        ]


class ShowEvasionFilter(ModelView):
    """filtro para el asistente show_evasion"""
    __name__ = 'datos_sonar.itinerary_history.show_evasion.filter'
    
    itinerary = fields.Many2One("datos_sonar.itinerary","Itinerario",
                                required=True,
                                )
    points = fields.Many2Many('datos_sonar.itinerary_point',None,None, 'Puntos Evadidos',
                              required=True,
                              domain=[('itinerary','=',Eval('itinerary'))]
                              )
    init_date = fields.Date('Fecha de Inicio',
                            required=True
                            )
    end_date = fields.Date('Fecha Final',
                           required=True,
                           domain=[('end_date','>',Eval('init_date'))]
                           )


class ShowEvasion(Wizard):
    """Muestra itinerarios que han evadido los puntos seleccionados."""
    
    __name__ = 'datos_sonar.itinerary_history.show_evasion'
    start = StateView('datos_sonar.itinerary_history.show_evasion.filter',
                            '',[
                                Button('Cancelar', 'end', 'tryton-cancel'),
                                Button('Mostrar','open_', 'tryton-ok', default= True)
                                ]
                            )
    open_ = StateAction('datos_sonar.act_window_itinerary_history')
    
    def do_open_(self,action):
        cursor = Transaction().connection.cursor()
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        ItineraryPoint = Pool().get('datos_sonar.itinerary_point')
        ItineraryHistoryPoint = Pool().get('datos_sonar.itinerary_history_point')
        
        it_points = ItineraryPoint.browse(self.start.points)
        it_points_pindex = [x.point_index for x in it_points]
        
        init_date = self.start.init_date
        end_date = self.start.end_date
        
        init_time = datetime.fromordinal(init_date.toordinal()) -\
         timedelta(hours=configuration.diferencia_utc)#hora colombia
        end_time = datetime.fromordinal(end_date.toordinal()) +\
         timedelta(hours=24) - timedelta(hours=configuration.diferencia_utc)#hora colombia
        logger.info("inittime: {}".format(init_time))
        logger.info("endtime: {}".format(end_time))
        ith_table = ItineraryHistory.__table__()
        ith2_table = ItineraryHistory.__table__()
        itp_table = ItineraryHistoryPoint.__table__()
        
        join = ith2_table.join(itp_table)
        join.condition = join.right.itinerary_history == ith2_table.id
        join.type_ = 'LEFT'
        p_indexes = [x.point_index for x in self.start.points] 
        where1 = (ith2_table.canceled == 'N') & (ith2_table.running == 'N')\
            & (ith2_table.inittime >= init_time) & (ith2_table.inittime <= end_time)\
            & (join.right.p_index.in_(p_indexes)) & (ith2_table.itinerary == self.start.itinerary.id)
     
        subselect = join.select(ith2_table.id,
                                where=where1
                                )
        where = (ith_table.canceled == 'N') & (ith_table.running == 'N')\
            & (ith_table.inittime >= init_time) & (ith_table.endtime <= end_time)\
            & (NotIn(ith_table.id, subselect)) & (ith_table.itinerary == self.start.itinerary.id)
        
        select = ith_table.select(
            ith_table.id,
            where=where,
            )

        cursor.execute(*select)
        col_sql = [x.name for x in cursor.description]
        rows = cursor.fetchall()
        ids = [x[col_sql.index('id')] for x in rows]
        
        action['pyson_domain'] = [
            ('id','in',ids),
            ]
        action['name'] += " Evasion de Puntos"
        #ItineraryHistory.raise_user_error("ids:{}".format(ids))
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}

class ItineraryHistoryCsv(Report):
    __name__ = 'datos_sonar.itinerary_history.csv'
    
#     @classmethod
#     def _get_records(cls, ids, model, data):
#         pass

    @classmethod
    def execute(cls, ids, data):
        ActionReport = Pool().get('ir.action.report')

        action_report_ids = ActionReport.search([
            ('report_name', '=', cls.__name__)
            ])
        if not action_report_ids:
            raise Exception('Error', 'Report (%s) not find!' % cls.__name__)
        action_report = ActionReport(action_report_ids[0])
        csv = cls.get_csv_data(ids,data)
        return ('csv', fields.Binary.cast(csv,'utf8'), False, action_report.name)
    
    @classmethod
    def get_csv_data(cls,ids,data):
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        Configutation = Pool().get('datos_sonar.configurations')
        configuration = Configutation.get_singleton()
        fields_definition = {}
        values = []
        if data:
            if 'fields_names' in data.keys():
                values = ItineraryHistory.read(ids,data['fields_names'])
                fields_definition = ItineraryHistory.fields_get(['fields_names'])
        values = values or ItineraryHistory.read(ids)
        fields_definition = fields_definition or ItineraryHistory.fields_get()
        if values:
            fields = [x for x in values[0].keys()]
            header = [fields_definition[x]['string'] for x in fields ]
        else:
            header = []
        #ajust datetime object to preferences
        if configuration.diferencia_utc:
            #hallar campos datetime
            fields_datetime = set()
            for key, value in fields_definition.items():
                if value['type'] == 'datetime':
                    fields_datetime.add(key)
            #adicionar diferencia_utc a los campos hallados
            for x in range(0,len(values)):
                for field in fields_datetime:
                    if field in values[x].keys():
                        if values[x][field]:
                            values[x][field] += timedelta(hours=configuration.diferencia_utc)
        
        rows = [[x for x in y.values()] for y in values]
        str_csv = cls.list_to_csv_string(rows,header)
        return str_csv
    
    @classmethod
    def list_to_csv_string(cls,rows,header=None,dialect='excel'):
            """Convert list to csv string.
            
            @param rows: list with row data.
            @param header: list with header data.
            @param dialect: dialect valid for csv module, excel by default.
            return string in csv format.
            """
            string = StringIO()
            writer = csv.writer(string,dialect=dialect)
            if header:
                writer.writerow(header)        
            list(writer.writerow(x) for x in rows)
            str_csv = string.getvalue()
            string.close()
            return str_csv
             
class ItineraryHistoryFilter(ModelView):
    """ItineraryHisoty filter"""
    __name__ = 'datos_sonar.itinerary_history.filter'
    
    init_time = fields.DateTime('Fecha Inicial',
                            required=True,
                            )
    end_time = fields.DateTime('Fecha Final',
                           required=True,
                            domain=[('end_time','>',Eval('init_time'))]
                           )
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                help="Vehiculo al que hace referencia el reporte",
                                required=True,
                                )
    min_percentage = fields.Integer('Porcentaje mínimo',
                                  help="Porcentaje minimo para que el registro sea tenido en cuenta",
                                  required=True,
                                domain=[('min_percentage','>=',0),
                                       ('min_percentage','<=',100)]
                                  )
    fields_names = fields.Many2Many('ir.model.field',None,None,'Campos',
                                help="Campos que se desan exportar",
                                required=False,
                                domain=[('model.model','=','datos_sonar.itinerary_history')],
                                states={'invisible':True}
                                )
    by_day = fields.Boolean('Por dia',
                            states={
                                'invisible':Bool(Eval('invisible_by_day')),
                                'readonly':Bool(Eval('invisible_by_day'))},
                            help="Indica si el informe se totalizara dia a dia\n"
                            "o por el tiempo total"
                            )
    invisible_by_day = fields.Boolean('invisible_by_day',
                                      states={'invisible':True})
    
    @classmethod
    def default_min_percentage(cls):
        return 1.0

    
class ItineraryHistoryFilterCsv(Wizard):
    """Wizard to make report ItineraryHistoryCsv with filter."""
    
    __name__ = 'datos_sonar.itinerary_history.csv_filter'
    
    start = StateView('datos_sonar.itinerary_history.filter',
        'datos_sonar.itinerary_history_filter_view_form',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Imprimir', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateAction('datos_sonar.report_itinerary_history_ods')
    
    
    def default_start(self,fields):
        return {'invisible_by_day':True}
    
    def do_print_(self, action):
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        clause = [
            ('inittime','>=',self.start.init_time),
            ('inittime','<=',self.start.end_time),
            ('mobil_id','in',self.start.mobiles),
            ('porcentage_points','>=', Decimal(self.start.min_percentage/100))
            ]
        records = ItineraryHistory.search_read(clause,
                                          order=[('inittime','ASC'),('endtime','ASC')],
                                          fields_names=['id'])
        if not records:
            ItineraryHistory.raise_user_error('Ningun registro hallado')
        ids = []
        id = []
        if records:
            ids = [x['id'] for x in records]
            id = ids[0]
        fields_names = []
        if hasattr(self.start, 'fields_names'):
            fields_names = [x.name for x in self.start.fields_names]
            
        #replace for rec_name the relation fields
        fields_definition = ItineraryHistory.fields_get()
        relation_fields=[]
        for field in fields_names:
            if 'relation' in fields_definition[field].keys():
                if fields_definition[field]['relation']:
                    relation_fields.append(field)
                    
        for x in range(0,len(fields_names)): 
            if fields_names[x] in relation_fields:
                fields_names[x] = fields_names[x] + '.rec_name' 
        return action, {
            'id': id,
            'ids': ids,
            'fields_names':fields_names,
            'by_day':self.start.by_day
            }


class ItineraryHistoryCsvStadistic(Report):
    """este reporte no usa como ingreso los ids seleccionados
    obligatoriamenete requiere data para filtrar los resultados.
    
    data['filter'] = {'init_time':time_object,'end_time':time_object,'mobiles':[mobil_id,...],'min_percentage':None or Decimal 0.0-1.0,
                    'title':title| None,'type':'png' | 'svg' | None}
    """
    
    __name__ = 'datos_sonar.itinerary_history.stadistic_csv'

    @classmethod
    def execute(cls, ids, data):
        if data:
            if 'filter' not in data.keys():
                raise Exception('Error','Report (%s) required filter info, not direct execution' % cls.__name__)
        else:
            raise Exception('Error','Report (%s) required filter info, not direct execution' % cls.__name__)
        
        ActionReport = Pool().get('ir.action.report')

        action_report_ids = ActionReport.search([
            ('report_name', '=', cls.__name__)
            ])
        if not action_report_ids:
            raise Exception('Error', 'Report (%s) not find!' % cls.__name__)
        action_report = ActionReport(action_report_ids[0])
        if 'type'in data['filter'].keys():
            if data['filter']['type'] == 'png' or data['filter']['type'] ==  'pdf':
                file = cls.get_graph_data(ids,data)
                return (data['filter']['type'], fields.Binary.cast(file), False, action_report.name)
        file = cls.get_csv_data(ids,data)
        return ('csv', fields.Binary.cast(file,'utf8'), False, action_report.name)
    
    @classmethod
    def get_data(cls,ids,data):
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        if 'by_day' in data['filter'].keys():
            by_day = data['filter']['by_day']
        else:
            by_day = False
        
        t_mobiles = Mobiles.__table__()
        t_itinieraryh = ItineraryHistory.__table__()
        
        join = t_itinieraryh.join(t_mobiles)
        join.type_='LEFT'
        join.condition = join.right.id == t_itinieraryh.mobil_id
        
        if by_day:
            select = join.select(
                    t_mobiles.mDescription.as_('Vehiculo'),
                    t_itinieraryh.itinerary_name.as_('Itinerario'),
                    Count(t_itinieraryh.id).as_('Cantidad'),
                    t_itinieraryh.inittime.cast('date').as_('Fecha')
                    )
            select.group_by = [t_itinieraryh.inittime.cast('date'),t_mobiles.mDescription,t_itinieraryh.itinerary_name]
            select.order_by = [t_itinieraryh.inittime.cast('date'),t_mobiles.mDescription,t_itinieraryh.itinerary_name]
        else:
            select = join.select(
                    t_mobiles.mDescription.as_('Vehiculo'),
                    t_itinieraryh.itinerary_name.as_('Itinerario'),
                    Count(t_itinieraryh.id).as_('Cantidad'),
                    )
            select.group_by = [t_mobiles.mDescription,t_itinieraryh.itinerary_name]
            select.order_by = [t_mobiles.mDescription,t_itinieraryh.itinerary_name]
        
        select.where = t_itinieraryh.mobil_id.in_(data['filter']['mobiles'])
        select.where &= t_itinieraryh.inittime >= data['filter']['init_time']
        select.where &= t_itinieraryh.inittime <= data['filter']['end_time']
        
        if data['filter']['min_percentage']:
            select.where &= t_itinieraryh.porcentage_points > data['filter']['min_percentage']

        logger.error("Select:{s}".format(s=str(select)))
        
        cursor = Transaction().connection.cursor()
        cursor.execute(*select)
        col_sql = [x.name for x in cursor.description]
        rows = cursor.fetchall()
        if data['filter']['title']:
            title = data['filter']['title']
        else:
            title = ['Reporte Intinerarios ',
                     str(data['filter']['init_time'])+
                     ' a ' + str(data['filter']['end_time']),]
            if data['filter']['min_percentage']:
                title += [" > ", str(int(data['filter']['min_percentage'] * 100)), "%"]
        return rows,col_sql,title
    
    @classmethod
    def get_csv_data(cls,ids,data):
        rows,col_sql,title = cls.get_data(ids, data)
        
        str_csv = cls.list_to_csv_string(rows,col_sql,title)
        return str_csv
    
    @classmethod
    def list_to_csv_string(cls,rows,header=None,title=None,dialect='excel'):
            """Convert list to csv string.
            
            @param rows: list with row data.
            @param header: list with header data.
            @param dialect: dialect valid for csv module, excel by default.
            return string in csv format.
            """
            string = StringIO()
            writer = csv.writer(string,dialect=dialect)
            if title:
                writer.writerow(title)
            if header:
                writer.writerow(header)
            list(writer.writerow(x) for x in rows)
            str_csv = string.getvalue()
            string.close()
            return str_csv
    
    @classmethod
    def get_graph_data(cls,ids,data):
        rows,col_sql, title = cls.get_data(ids, data)
        
        mobiles = set(x[col_sql.index('Vehiculo')] for x in rows)
        mobiles = tuple(mobiles)
        itineraries = set(x[col_sql.index('Itinerario')] for x in rows)
        itineraries = tuple(itineraries)

        if 'Fecha' in col_sql:
            fechas = sorted(set(x[col_sql.index('Fecha')] for x in rows ))
            fechas = tuple(x.strftime('%Y-%m-%d') for x in fechas)
            means = {fecha:'' for fecha in fechas}
            for fecha in fechas:
                means[fecha] = {x:list(0 for y in range(0,len(mobiles))) for x in itineraries}
                 
            for row in rows:
                #means[fecha][itinerario][posicion_del_vehiculo]= cantidad
                means[row[col_sql.index('Fecha')].strftime('%Y-%m-%d')][row[col_sql.index('Itinerario')]][mobiles.index(row[col_sql.index('Vehiculo')])] = row[col_sql.index('Cantidad')] 
            del rows
            values = {}
            for fecha,reg in means.items():
                values[fecha] = [tuple([key,tuple(value)]) for key,value in reg.items()]
            del means
            logger.error('Values:{}'.format(values))
            file = multigrafico_archivo_columnas_apiladas(mobiles,values," ".join(title),
                                                 etiqueta_x='Vehiculos',etiqueta_y='Viajes',
                                                 formato=data['filter']['type'])
        else:
            means = {x:list(0 for y in range(0,len(mobiles))) for x in itineraries}
            for row in rows:
                means[row[col_sql.index('Itinerario')]][mobiles.index(row[col_sql.index('Vehiculo')])] = row[col_sql.index('Cantidad')]
            values = [tuple([key,tuple(value)]) for key,value in means.items()]
    
            file = grafico_archivo_columnas_apiladas(mobiles,values," ".join(title),
                                                 etiqueta_x='Vehiculos',etiqueta_y='Viajes',
                                                 formato=data['filter']['type'])
        return file
        
class ItineraryHistoryStadistic(Wizard):
    """Wizard to make csv stadistic from ItineraryHistory."""
    
    __name__ = 'datos_sonar.itinerary_history.stadistic'
    
    start = StateView('datos_sonar.itinerary_history.filter',
        'datos_sonar.itinerary_history_filter_view_form',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Csv', 'csv', 'tryton-ok', default=True),
            Button('Png', 'png', 'tryton-ok'),
            Button('Pdf', 'pdf', 'tryton-ok'),
            ])
    csv = StateAction('datos_sonar.report_itinerary_history_stadistic_csv')
    png = StateAction('datos_sonar.report_itinerary_history_stadistic_csv')
    pdf = StateAction('datos_sonar.report_itinerary_history_stadistic_csv')
    
    def do_csv(self, action):
        return action, {
            'id': None,
            'ids': None,
            'filter':{
                'init_time':self.start.init_time,
                'end_time':self.start.end_time,
                'mobiles':[x.id for x in self.start.mobiles],
                'min_percentage':Decimal(self.start.min_percentage/100),
                'title':None,
                'type':'csv',
                'by_day':self.start.by_day
                }
            }
    
    def do_png(self, action):
        return action, {
            'id': None,
            'ids': None,
            'filter':{
                'init_time':self.start.init_time,
                'end_time':self.start.end_time,
                'mobiles':[x.id for x in self.start.mobiles],
                'min_percentage':Decimal(self.start.min_percentage/100),
                'title':None,
                'type':'png',
                'by_day':self.start.by_day
                }
            }
        
    def do_pdf(self, action):
        return action, {
            'id': None,
            'ids': None,
            'filter':{
                'init_time':self.start.init_time,
                'end_time':self.start.end_time,
                'mobiles':[x.id for x in self.start.mobiles],
                'min_percentage':Decimal(self.start.min_percentage/100),
                'title':None,
                'type':'pdf',
                'by_day':self.start.by_day
                }
            }

class VirtualItineraryHistoryParams(ModelView):
    """vista para la creación de itinerarios historicos virtuales"""
    
    __name__ = 'datos_sonar.virtual_itinerary_history.create.params'
    
    init_time = fields.DateTime('Fecha inicial',
                            required=True)
    end_time = fields.DateTime('Fecha final',
                           required=True,
                           domain=[('end_time','>',Eval('init_time')),],
                           help="No debe ser superior a 8 días")
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                required=True,
                                )
    itinerary = fields.Many2One("datos_sonar.itinerary","Itinerario")
    ignored_geofences = fields.Many2Many('datos_sonar.itinerary_point',None,None,'Geocercas ignoradas',
                                         domain=[('itinerary','=',Eval('itinerary'))],
                                         help="Estas geocercas seran ignoradas si se encuentran en una posicion "
                                         "posterior en el itinerario")
    complety_ignored_geofences = fields.Many2Many('datos_sonar.itinerary_point',None,None,'Geocercas ignoradas completamente',
                                         domain=[('itinerary','=',Eval('itinerary'))],
                                         help="Estas geocercas no apareceran en los itinerarios historicos que se creen.\n"
                                         "Son usadas cuando estas geocercas se atraviezan en el itinerario en una posicion "
                                         "que no le corresponden")
    minim_points = fields.Integer('Puntos mínimos',
                                  domain=[('minim_points','>=',0)],
                                  help="Cantidad de puntos mínimos para "
                                  "considerar que es un itinerario\n"
                                  "Esta cantidad de puntos no tiene que ser "
                                  "consecutiva pero el punto 2 debe tener un "
                                  "indice (orden en itinerario) superior"
                                  )
    
    def pre_validate(self):
        super(VirtualItineraryHistoryParams, self).pre_validate()
        if (self.start.init_time - self.start.end_time).days > 8:
            self.raise_user_error("La cantidad de dias no debe ser superior a 8")
    

class CreateVirtualItineraryHistory(Wizard):
    """Asistente que crea itinerarios historicos basado en EventLocation"""
    
    __name__ = 'datos_sonar.virtual_itinerary_history.create'

    start = StateView('datos_sonar.virtual_itinerary_history.create.params',
        'datos_sonar.virtual_itinerary_history_params_view_form',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Crear', 'create_', 'tryton-launch', default=True),
            ])
    create_ = StateTransition()
   
    def transition_create_(self):
        EventLocation = Pool().get('datos_sonar.event_location')
        if (self.start.init_time - self.start.end_time).days > 8:
            self.raise_user_error("La cantidad de dias no debe ser superior a 8")
            return 'start'
        ignored_geofences = [x.geofence.g_name for x in self.start.ignored_geofences]
        complety_ignored_geofences = [x.geofence.g_name for x in self.start.complety_ignored_geofences]
        for mobil_id in self.start.mobiles:
            EventLocation.to_itineraries_history(self.start.init_time,
                                                  self.start.end_time,
                                                  mobil_id,
                                                  self.start.itinerary,
                                                  self.start.minim_points,
                                                  ignored_geofences,
                                                  complety_ignored_geofences
                                                  )
        return 'end'
    
class OpenEventsFromItinerarHistory(Wizard):
    """Abre eventos en el mismo tiempo del itinerario historico."""
    
    __name__ = 'datos_sonar.itnerary_history.open_events'
    
    start = StateAction('datos_sonar.act_event_location_without_filter_form')
    
    def do_start(self,action):
        #@todo eventDescription debe ser con los sinonimos
        ItineraryHistory = Pool().get(Transaction().context['active_model'])
        transaction = Transaction()
        
        itinerary_history = ItineraryHistory.browse([transaction.context['active_id']])[0]
        inittime = itinerary_history.inittime - timedelta(minutes=5)#@todo llevar a configuracion
        endtime = itinerary_history.endtime + timedelta(minutes=5)#@todo llevar a configuracion
        action['pyson_domain'] = [
            ('system_gmt','>=',inittime),
            ('system_gmt','<=',endtime),
            ('mobil_id','=',itinerary_history.mobil_id.id),
            ]
        action['name'] += " {mobil} ({date})".format(mobil=itinerary_history.mobil_id.rec_name,
                                                            date=inittime,
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}

class QunatityItineraryHistoryParams(ModelView):
    """Formulario para el asistente de Cantidad de Itinearios realizados
    por mobil"""
    
    __name__ = 'datos_sonar.itinerary_history.quantity_for_day.params'
    
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                required=True,
                                )
    date = fields.Date('Fecha')
    itinerary_ids = fields.Many2Many('datos_sonar.itinerary',None,None,'Itinerarios',
                                     help="si se omite se toman todos los itinerarios")
    minim_porcentage = fields.Numeric('% Cumplimiento mínimo',
                                digits=(1,2),
                                help="Porcentage de cumplimiento minimo"
                                )

class QuantityItineraryHistory(Wizard):
    """Reporta la cantidad de itinerarios historicos realizados por mobil
    por dia"""
    __name__ = 'datos_sonar.itinerary_history.quantity_for_day'
    
    start = StateView('datos_sonar.itinerary_history.quantity_for_day.params',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Reporte', 'report_', 'tryton-launch', default=True),
            ])
    report_ = StateReport('datos_sonar.itinerary_history.quantity')

    def do_report_(self,action):
        Itinerary = Pool().get('datos_sonar.itinerary')
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        VirtualItineraryHistory = Pool().get('datos_sonar.virtual_itinerary_history')
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        information = {}
        for mobil in self.start.mobiles:
            mobile = Mobiles.browse([mobil])[0]
            if mobile.rec_name not in information:
                information[mobile.rec_name] = {}
            information[mobile.rec_name]['itinerary_history'] = ItineraryHistory.get_quantity_for_day_for_mobil(self.start.date,
                                                                                                       mobil,
                                                                                                       self.start.itinerary_ids,
                                                                                                       self.start.minim_porcentage)
            information[mobile.rec_name]['virtual_itinerary_history'] = VirtualItineraryHistory.get_quantity_for_day_for_mobil(self.start.date,
                                                                                                       mobil,
                                                                                                       self.start.itinerary_ids,
                                                                                                       self.start.minim_porcentage)
        return action, {
            'date_':self.start.date,
            'itineraries':Itinerary.browse(self.start.itinerary_ids) if self.start.itinerary_ids else "Todos",
            'minim_porcentage': self.start.minim_porcentage if self.start.minim_porcentage else "0",
            'information': information#[a.id for a in evt],
            }

class VirtualItineraryHistoryDeleteDuplicatedParams(ModelView):
    """Modelo para definir los itinerarios historicos duplicados a eliminar"""
    
    __name__ = 'datos_sonar.virtual_itinerary_history.delete_duplicated.params'
    
    day = fields.Date('Fecha',
                      required=True)
    
    @staticmethod
    def default_day():
        return date.today() - timedelta(days=1)
    
class VirtualItineraryHistoryDeleteDuplicated(Wizard):
    """Asistente para eliminar itinerarios historicos virtuales repetidos"""
    
    __name__ = 'datos_sonar.virtual_itinerary_history.delete_duplicated'
    
    start = StateView('datos_sonar.virtual_itinerary_history.delete_duplicated.params',
                      '',[
                          Button('Cancelar','end', 'tryton-cancel'),
                          Button('Eliminar Duplicados','delete_duplicates','tryton-launch',default=True)
                          ])
    delete_duplicates = StateTransition()
    
    def transition_delete_duplicates(self):
        VItinerarHistory = Pool().get('datos_sonar.virtual_itinerary_history')
        VItinerarHistory.delete_duplicates(self.start.day)
        return 'end'
