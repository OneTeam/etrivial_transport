# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import json

from io import StringIO
from datetime import datetime, date, timedelta

from trytond.modules.datos_sonar.sonar import SonarConstantSync

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Equal, Not, Or, PYSONEncoder
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport

import logging
from decimal import Decimal

logger = logging.getLogger(__name__)


__all__ =["GeoPassengerLog", 'GeoPassengerLogMap']

map_type = 'Pasajeros'


class GeoPassengerLog(SonarConstantSync,ModelSQL,ModelView):
    "Historico Pasajeros Georeferenciados"
    __name__ = "datos_sonar.geo_passenger_log"
    _rec_name = "sonar_id"
    
    #Campos requeridos para SonarConstantSync
    _field_time = 'system_time'
    _sonar_constant_sync_request = 'GET_GeolocatedPassengerHistory'
    _sonar_constant_sync_target = 'geolocated_passenger'
    _sonar_constant_sync_array = 'GeolocatedPassengerLog'
    _sonar_constant_sync_extra_param = {}
    
    sonar_id = fields.BigInteger("Sonar Id")
    mId = fields.Char("mId")
    # blocking_count = fields.Integer('Conteo Bloqueos') 

    mobil_id = fields.Many2One("datos_sonar.mobiles", "Vehiculo")
    gps_time = fields.DateTime("Hora GPS")
    system_time = fields.DateTime("Hora Sistema")
    latitude = fields.Float("latitud")
    longitude = fields.Float("longitud")
    address = fields.Char("Direccion")
    door1_in = fields.Integer('Ingresos P1')
    door1_out = fields.Integer('Salidas P1')
    door1_block = fields.Integer('Bloqueos P1')
    door2_in = fields.Integer('Ingresos P2')
    door2_out = fields.Integer('Salidas P2')
    door2_block = fields.Integer('Bloqueos P2')
    count_in = fields.Integer('Ingresos')
    count_out = fields.Integer('Salidas')
    count_block = fields.Integer('Bloqueos')
    warning_other_door = fields.Boolean('Advertencia otra puerta',
                                        help="Advierte si algún registro indico "
                                        "conteo para una puerta diferente a 1 o 2")
    driver_id = fields.BigInteger("Conductor id")
    driver = fields.Many2One('datos_sonar.drivers','Conductor')
    driver_name = fields.Char('Conductor')
    driver_document = fields.Char('Conductor Documento')
        
    def get_link_position(self,field):
        """
        devuelve un link a openstreetmap con la latitud y longitud del Evento,
        la forma del link la toma de la configuracion 
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        link = configuration.link_map_format
        if link == '':
            link = 'http://www.openstreetmap.org/?mlat={0}&mlon={1}&zoom=12'
        return (link.format(self.latitude,self.longitude))
        
    @classmethod
    def __setup__(cls):
        super(GeoPassengerLog, cls).__setup__()
        cls._order = [('system_time','DESC'),('mobil_id','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('sonar_id_key', Unique(table, table.sonar_id),
                'geo_passenger_log ya existe el sonar_id')
        ]

    @classmethod
    def syncRespuestaAPI(cls,list_response):
        """
        sincroniza los geo_passenger_log que es el objeto devuelto
        por la API de la funcion GET_GeolocatedPassengerHistory()
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        data=[]
        sonar_ids_to_sync = [x.Id for x in list_response.GeolocatedPassengerLog]
        cls.delete(cls.search([('sonar_id', 'in', sonar_ids_to_sync)]))
        for geolocated_log in list_response.GeolocatedPassengerLog:
            prototype = {}
            prototype ['count_in'] = 0
            prototype ['count_out'] = 0
            prototype ['count_block'] = 0
            prototype ['door1_in'] = 0
            prototype ['door1_out'] = 0
            prototype ['door1_block'] = 0
            prototype ['door2_in'] = 0
            prototype ['door2_out'] = 0
            prototype ['door2_block'] = 0
            prototype ['warning_other_door'] = False
            for door in geolocated_log.doors['Door']:
                if door.NumDoor == 1:
                    prototype['door1_in'] = door.In
                    prototype['door1_out'] = door.Out
                    prototype['door1_block'] = door.Blocking
                elif door.NumDoor == 2:
                    prototype['door2_in'] = door.In
                    prototype['door2_out'] = door.Out
                    prototype['door2_block'] = door.Blocking
                else:
                    prototype['warning_other_door'] = True
                prototype['count_in'] += door.In
                prototype['count_out'] += door.Out
                prototype['count_block'] += door.Blocking

            prototype['sonar_id'] = geolocated_log.Id
            prototype['mId'] = geolocated_log.mId
            #prototype['blocking_count'] = geolocated_log.blocking_count
            prototype['mobil_id'] = Mobiles.idMobile_From_mId(geolocated_log.mId)
            prototype['gps_time'] = geolocated_log.gps_time
            prototype['system_time'] = geolocated_log.system_time
            prototype['latitude'] = geolocated_log.latitude
            prototype['longitude'] = geolocated_log.longitude
            prototype['address'] = geolocated_log.address
            prototype['driver_id'] = geolocated_log.driver_id
            prototype['driver'] = None#geolocated_log.driver #buscar como vincular conductor
            prototype['driver_name'] = geolocated_log.driver_name
            prototype['driver_document'] = geolocated_log.driver_document
            data.append(prototype)
        records = cls.create(data)
        cls.save(records)
        return True

    @classmethod
    def search_read_for_time(cls, init_time, end_time, mobil_id, extra_domain=[], order=[],fields_names=[]):
        """ Devuelve los registros del mobil_id entre la hora inicial y hora final
        """
        domain = [('system_time', '>=', init_time),
                  ('system_time', '<=', end_time),
                  ('mobil_id', '=', mobil_id)]
        if extra_domain:
            domain.extend(extra_domain)
        if not order:
            order = [('system_time', 'DESC')]
        return cls.search_read(domain, order=order, fields_names=fields_names)

    @classmethod
    def get_total_passenger_for_time(cls, init_time, end_time, mobil_id, extra_domain=[], order=[]):
        fields_names = ['count_in', 'count_out', 'count_block']
        records = cls.search_read_for_time(init_time, end_time, mobil_id, extra_domain, order,
                                           fields_names=fields_names.copy())
        r = [0 for x in fields_names]
        for record in records:
            for key, value in record.items():
                if key in fields_names:
                    r[fields_names.index(key)] += value or 0
        return tuple(r)
        
    @classmethod
    def get_totals_passenger_for_time(cls, init_time, end_time, mobil_id, extra_domain=[], order=[]):
        "Devuelve una lista de 3 listas ( puerta 1, puerta 2, general) con los totales ingresos, salidas, bloqueos"
        fields_names = ['door1_in',
                        'door1_out',
                        'door1_block',
                        'door2_in',
                        'door2_out',
                        'door2_block',
                        'count_in',
                        'count_out',
                        'count_block']
        records = cls.search_read_for_time(init_time, end_time, mobil_id, extra_domain, order,
                                           fields_names = fields_names.copy())
        r = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]
        for record in records:
            for field in fields_names:
                if field in record:
                    if record[field]:
                        if field == "door1_in":
                            r[0][0] += record[field]
                        elif field == "door1_out":
                            r[0][1] += record[field]
                        elif field == "door1_block":
                            r[0][2] += record[field]

                        elif field == "door2_in":
                            r[1][0] += record[field]
                        elif field == "door2_out":
                            r[1][1] += record[field]
                        elif field == "door2_block":
                            r[1][2] += record[field]

                        elif field == "count_in":
                            r[2][0] += record[field]
                        elif field == "count_out":
                            r[2][1] += record[field]
                        elif field == "count_block":
                            r[2][2] += record[field]
        return r

    @classmethod
    def get_data_for_map(cls, init_time, end_time, mobil_id, extra_domain=[], order=[]):
        records = cls.search_read_for_time(init_time, end_time, mobil_id, extra_domain, order)
        return [record.to_map_format() for record in records]
    
    @classmethod
    def search_read_data_for_map(cls,ids):
        records = cls.search([('id', 'in', ids)])
        return [record.to_map_format() for record in records]
    
    def to_map_format(self):
        return {'latitude': self.latitude,
                'longitude': self.longitude,
                'description': self.get_map_description(),
                'type': map_type,
                'metadata': self.get_map_metadata()
        }

    def get_map_metadata(self):
        fields = [('mobil_id','rec_name'), 'system_time', 'address' ]
        metadata = {}
        for field in fields:
            if isinstance(field,tuple):
                metadata[field[0]] = getattr(getattr(self, field[0], None), field[1], None)
                if isinstance (metadata[field[0]],datetime):
                    metadata[field[0]] = metadata[field[0]].isoformat()
            else:
                metadata[field] = getattr(self,field, None)
                if isinstance(metadata[field],datetime):
                    metadata[field] = metadata[field].isoformat()
        return metadata
                                       
    def get_map_description(self):
        r = f"Pasajeros {self.mobil_id.rec_name}. {self.system_time}.<br/>"
        r += f"Puerta 0: in={self.door1_in or 0} sal={self.door1_out or 0} bloq={self.door1_block or 0}</br>"
        r += f"Puerta 1: in={self.door2_in or 0} sal={self.door2_out or 0} bloq={self.door2_block or 0}</br>"
        r += f"Total: in={self.count_in or 0} sal={self.count_out or 0} bloq={self.count_block or 0}"
            
        return r


    @classmethod
    def get_overwrite_params(cls):
        """
        devuelve un diccionario que sobreescribe los parametros pasados a Sonar
        """
        return {
            'UTC_datetime_init': 'datetime_init',
            'UTC_datetime_end': 'datetime_end'
        }
    

class GeoPassengerLogMap(Wizard):
    "Reporte Mapa Pasajeros Geolocalizados"
    __name__ = 'datos_sonar.geo_passenger_log.map'
    
    start = StateReport('datos_sonar.geo_passenger_log')

    def do_start(self, action):
        GeoPassengerLog = Pool().get('datos_sonar.geo_passenger_log')
        map_data = GeoPassengerLog.search_read_data_for_map(Transaction().context['active_ids'])
        json_dump = StringIO()
        json.dump(map_data, json_dump)
        json_data = json_dump.getvalue()
        json_dump.close()
        return action, {
            'id': Transaction().context['active_id'],
            'ids': Transaction().context['active_ids'],
            'json_data': json_data
        }
