# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime, date, timedelta
import csv
from io import StringIO, BytesIO
from collections import defaultdict
import calendar
import logging
import pickle
from decimal import Decimal

from trytond.modules.datos_sonar.sonar import SonarConstantSync
from trytond.modules.datos_sonar import datos_sonar_lib
from .graficos import *

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool

from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval, Equal, Not, Or, PYSONEncoder
from trytond.wizard.wizard import StateAction
from trytond.transaction import Transaction

from sql import *
from sql.aggregate import *
from sql.conditionals import *
from sql import operators
from sql.functions import Function, Abs, Overlay, Trim, AtTimeZone, ToChar




logger = logging.getLogger(__name__)


__all__ =["EventLocation",'TotalDiarioEventLocation','ExportEventLocation',
        'EventLocationFiltro','ExportEventLocationCSV','ExportEventLocationCSVData',
        'StadisticEventLocation','StadisticLocationFiltro',
        'StadisticLocationSelectType','StadisticEventLocationCSV','SpeedMobileGraph',
        'SpeedMobile','EventLocationDateFilter','EventsForDay','MasterEvent', 'OpenEvents',
        'EventsForMonth','OpenDays','AnaliticMasterEvent','AnaliticCondition',
        'AnaliticEventsForDay', 'AnaliticEventsForMonth']

class EventLocation(SonarConstantSync,ModelSQL,ModelView):
    '''    Informacion extraida usando la funcion GET_EventsHistory de la API.
       
    '''
    __name__ = "datos_sonar.event_location"
    _rec_name = "eventID"
    
    #Campos requeridos para SonarConstantSync
    _field_time = 'system_gmt'
    _sonar_constant_sync_request = 'GET_EventsHistory'
    _sonar_constant_sync_target = 'evtList'
    _sonar_constant_sync_array = 'EventLocation'
    _sonar_constant_sync_extra_param = {'eventID':''}
    
    _error_messages = {
        "Error_Fatal": "error fatal: %s"
        }

    regId = fields.BigInteger("regId")
    mId = fields.Char("mId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    eventID = fields.Integer("eventID")
    eventDescription = fields.Char("eventDescription: Descripcion")
    address = fields.Char("address: Direccion descriptiva ")
    gps_GMT = fields.DateTime("gps_GMT:TiempoUTC")
    gps_tz = fields.Function(fields.DateTime("Tiempo GPS Colombia"),"obtener_gps_tz")
    system_tz = fields.Function(fields.DateTime("Tiempo System Colombia"),"obtener_system_tz")
    system_gmt = fields.DateTime("system_GMT:TiempoUTC", required=True)
    latitude = fields.Float("latitude")
    longitude = fields.Float("longitude")
    speed = fields.Integer("speed")
    precision = fields.Integer("precision")
    altitude = fields.Integer("altitude")
    position = fields.Function(fields.Char('Posicion'),'get_link_position')
    """
    Heading: es la dirección horizontal a la que apunta en cualquier instante el móvil sea este buque, aeronave, coche, etc,.
    """
    heading = fields.Integer("heading:angulo")
    age = fields.Integer("age:?")
    
    
    def obtener_gps_tz(self,field):
        gps_tz= self.gps_GMT - timedelta(hours=5)
        return gps_tz
    
    def obtener_system_tz(self,field):
        system_tz= self.system_gmt - timedelta(hours=5)
        return system_tz
    
    def get_link_position(self,field):
        """
        devuelve un link a openstreetmap con la latitud y longitud del Evento,
        la forma del link la toma de la configuracion 
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        link = configuration.link_map_format
        if link == '':
            link = 'http://www.openstreetmap.org/?mlat={0}&mlon={1}&zoom=12'
        return (link.format(self.latitude,self.longitude))
        
    @classmethod
    def __setup__(cls):
        super(EventLocation, cls).__setup__()
        cls._order = [('system_gmt','DESC'),('mobil_id','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('regid_key', Unique(table, table.regId),
                'El regId debe ser unico')
        ]
    
    @classmethod
    def syncRespuestaAPI(cls,event_list_response):
        """
        SincEventListResponse
        sincroniza los eventos de un ListResponse que es el objeto devuelto
        por la API de la funcion GET_EventsHistory()
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        data=[]
        fields_names=[
            "regId",
            "mId", #para relacionar el Mobile en el sistema
            "mobil_id",
            "eventID",
            "eventDescription",
            "address",
            "gps_GMT",
            "system_gmt",
            "latitude",
            "longitude",
            "speed",
            "precision",
            "altitude",
            "heading",
            "age",
            ]
        for event_location in event_list_response.EventLocation:
            if not cls.existe_regid(event_location.regId,event_location.system_GMT):
                prototype = [
                event_location.regId,
                event_location.mId,
                Mobiles.mDescriptionFromMid(event_location.mId),
                event_location.eventID,
                event_location.eventDescription,
                event_location.address,
                event_location.gps_GMT,
                event_location.system_GMT,
                event_location.latitude,
                event_location.longitude,
                event_location.speed,
                event_location.precision,
                event_location.altitude,
                event_location.heading,
                event_location.age,
                ]
                data.append(prototype)
        errores=cls.import_data(fields_names, data)
        logger.info("Reporte Importacion: %s.",errores)
        logger.info("TIEMPO 9: %s.",datetime.today())
        return True
    
    @classmethod
    def existe_regid(cls,regId,system_gmt=None):
        conditions = [('regId','=',regId)]
        if system_gmt:
            conditions.append(('system_gmt','=',system_gmt))
        return cls.search_count(conditions) > 0
    
    @classmethod
    def relacionarConMobile(cls):
        """Relaciona los datos importados con mobiles
        
        """
        logger.info("Inicio relacionarConMobile: %s.",datetime.today())
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        allIdsMidDict = Mobiles.allIdsMidDict()
        
        objetos = cls.search_read([('mobil_id','=',None)])
        for objeto in objetos:
            objeto['mobil_id']=allIdsMidDict[objeto['mId']]
            record = cls.search([('id','=',objeto['id'])])
            cls.write(record,objeto)
            cls.save(record)
        logger.info("Fin relacionarConMobile: %s.",datetime.today())
    
    @classmethod
    def lista_to_csv_line(cls,campos ,lista_de_lineas,dialect='excel'):
            """Convierte una lista en archivo csv.
            
            @param campos: lista con los nombres de las columnas (header)
            @param lista_de_lineas: lista de lista
            return string con formato csv
            """
            linea = StringIO()
            writer = csv.writer(linea,dialect=dialect)
            if campos:
                writer.writerow(campos)
            list(writer.writerow(x) for x in lista_de_lineas)
            str_csv = linea.getvalue()
            linea.close()
            return str_csv

    @classmethod
    def search_related_with_geofences_itinerary(cls,init_time,end_time,mobil_id,itinerary_id,ignored_geofences=[]):
        """Devuelve los eventos del vehiculo (mobil_id) que esten relacionados
        con el ingreso y salida de las geocercas del itinerario (itinerary_id)
        entre la hora de inicio (init_time) y la hora final (end_time)
        ordenados de forma ascendente por system_gmt
        """
        Itinerary = Pool().get('datos_sonar.itinerary')

        itinerary = Itinerary.browse([itinerary_id])[0]
        #points = itinerary.get_order_name_points()
        geofences_names = itinerary.get_events_description_for_geofences(ignored_geofences)
        return cls.search([('eventDescription','in',geofences_names),
                           ('mobil_id','=',mobil_id),
                           ('system_gmt','>=',init_time),
                           ('system_gmt','<=',end_time)],
                        order=[('system_gmt','ASC')])

    @classmethod
    def to_itineraries_history(cls,init_time,end_time,mobil_id,itinerary_id,minim_points=1,ignored_geofences=[],complety_ignored_geofences=[]):
        """Devuelve Itinerarios historicos basado en eventos de ingreso a geocercas
        Solo se considerará itinerario si tiene la cantidad minima de puntos (minim_points) 
        @param init_time: datetime or datetime_string in one of the formats emitted by datetime.isoformat()
        @param end_time: datetime or datetime_string in one of the formats emitted by datetime.isoformat()
        @param ignored_geofences: lista de nombres de las geocercas que serán ignoradas en caso de que se
         se encuentren "atravesadas" en el recorrido
        """
        Itinerary = Pool().get('datos_sonar.itinerary')
        Configuration = Pool().get('datos_sonar.configurations')
        VirtualItineraryHistory = Pool().get('datos_sonar.virtual_itinerary_history')

        if isinstance(init_time,str):
            init_time = datetime.fromisoformat(init_time)
        if isinstance(end_time,str):
            end_time = datetime.fromisoformat(end_time)

        def idx_from_idxs(pidx,nidx,idxs):
            """Devuelve un idx para multiples idxs
            basado en el idx anterior(pidx) y el siguiente (nidx)"""
            idxs = sorted(idxs)
            r = idxs[0]
            if pidx and nidx:
                for idx in idxs:
                    if pidx <= idx <= nidx:
                        r = idx
                        break
            elif pidx:
                for idx in idxs:
                    if pidx <= idx:
                        r = idx
                        break
            elif nidx:
                for idx in sorted(idxs,reverse=True):
                    if nidx >= idx:
                        r = idx
                        break
            return r



        configuration = Configuration.get_singleton()
        itinerary = Itinerary.browse([itinerary_id])[0]

        events = cls.search_related_with_geofences_itinerary(init_time,
                                                             end_time,
                                                             mobil_id,
                                                             itinerary_id,
                                                             complety_ignored_geofences)
        ignored_events = [configuration.evento_ingreso_geocerca.format(geocerca=geo) for geo in ignored_geofences ]
        #points = dict((y,x) for x,y in itinerary.get_order_name_points())
        points = dict((x,y) for x,y in itinerary.get_order_name_points())
        points_name_event = {k:configuration.evento_ingreso_geocerca.format(geocerca=v) for k,v in points.items()}

        group_points_name_event = {}
        for idx,name_event in points_name_event.items():
            if name_event not in group_points_name_event:
                group_points_name_event[name_event]=[idx]
            else:
                group_points_name_event[name_event].append(idx)

        #agrupar los eventos por itinerario
        grouped_events = []
        pidx = -1
        idxs = []
        group = []
        for x in range(0,len(events)):
            event = events[x]
            idxs = group_points_name_event[event.eventDescription]
            if len(idxs) == 1:
                idx = idxs[0]
            else:
                idx = idx_from_idxs(pidx if pidx != -1 else None,
                                   None,#@todo implementar idx del siguiente evento
                                   idxs)
            if idx > pidx:
                group.append((idx,event))
            #que ocurre si el evento se repite? es del mismo itinerario historico)
            #es de un itinerario historico diferente? se opta porque sea del mismo
            elif idx == pidx:
                continue
            else:
                if event.eventDescription in ignored_events:#en caso de que la diferencia sea por una geocerca que se deba ignorar
                    continue
                if len(group) >= minim_points:
                    grouped_events.append(group)
                group=[(idx,event)]
            pidx = idx
        if len(group) >= minim_points:
            grouped_events.append(group)

        #construyendo itinerari history
        v_itinerarios_historicos = []
        for x in range(0,len(grouped_events)):
            list_ItPoints = []
            for idx,event in grouped_events[x]:
                it_point = {
                'p_index' : idx,
                'p_violation' : 'N',
                'p_error' : 0, #logica de campo desconocidia
                'p_realtime' : event.system_gmt,
                'p_gpstime' : event.gps_GMT,
                'p_expectedtime' : event.system_gmt,
                'p_difference' : 0, #logica de campo desconocidia
                'p_total' : "N", #@todo logica de campo desconocida, supongo que es si totalizo pasajeros
                'p_ingresos' : 0, #@todo supongo que es ingreso pasajeros, implementar
                'p_salidas' : 0, #@todo supongo que es salida pasajeros, implementar
                'p_bloqueos' : 0, #@todo supongo que es bloqueos contadora pasajeros, implementar
                'geofence_name' : event.eventDescription.replace(configuration.evento_ingreso_geocerca.format(geocerca=''),''),
                }
                list_ItPoints.append(it_point)

            ith = {
                'itinerary' : itinerary_id,
                'itinerary_name' : itinerary.It_name,
                'mobil_id' : mobil_id,
                'auto' : 'N',
                'running' : 'N', #@todo implementar verificando hora actual
                'close' : 'N', #@todo implementar verificando los puntos
                'violations' : 'N', #@todo implementar verificando todos los puntos
                'leaveroute' : 'N', #@todo implementar verificando todos los puntos
                'canceled' : 'N', #@todo implementar verificando todos los puntos
                'inittime' : grouped_events[x][0][1].system_gmt,
                'endtime' : grouped_events[x][-1][1].system_gmt,
                'userCanceled' : 'N', #estos itinerarios no se pueden cancelar
                'ItPointsLog' : [('create',list_ItPoints)] if list_ItPoints else [], #@todo agregar itpoints basado en eventos
                'quantity_points' : len(list_ItPoints), #@todo implementar verificando todos los puntos
                'porcentage_points' : Decimal(len(list_ItPoints)/len(points)).quantize(Decimal('0.00')) if len(points) else Decimal(0), #@todo implementar verificando todos los puntos
                }
            v_itinerarios_historicos.append(ith)

        return VirtualItineraryHistory.create(v_itinerarios_historicos)
    
    @classmethod
    def get_velocity_limit_exceded(cls,init_time,end_time,mobil_id,limit=60,count=False):
        """Devuelve los eventos que superan el limite(limit) de velocidad del
        vehiculo(mobil_id) entre el tiempo inicial(init_time) y el tiempo final
        (end_time) si se indica count devuelve la cantidad de lo contrario una
        lista de eventos
        """
        domain_ = [('system_gmt','>=',init_time),
                    ('system_gmt','<=',end_time),
                    ('mobil_id','=',mobil_id),
                    ('speed','>',limit),
                    ]
        return cls.search(domain_,count=count)
    
    @classmethod
    def get_dispatch_info(cls,inittime,endtime,mobil_id,itinerary_id):
        """Devuelve la información Sonar usada por los despachos basada en la
        hora de inicio, hora final, vehiculo e itinerario dados.
        retorna una tupla (time_first_geofence,time_last_geofence,done_geofences,
        ignored_geofences,name_ignored_geofences,porcentage_points)
        """
        Itinerary = Pool().get('datos_sonar.itinerary')
        
        itinerary = Itinerary.browse([itinerary_id])[0]
        #obtener eventos para las geocercas basado en el itinerario
        events = cls.search_related_with_geofences_itinerary(inittime,
                                                             endtime,
                                                             mobil_id,
                                                             itinerary_id,
                                                             [])
        #obtener las geocercas del itinerario y convertirlas en el nombre de los eventos
        geofences_names = itinerary.get_events_description_for_geofences()
        total_geofences = len(geofences_names)
        if events:
            time_first_geofence = events[0].system_gmt
            time_last_geofence = events[-1].system_gmt
            for event in events:
                if event.eventDescription in geofences_names:
                    geofences_names.remove(event.eventDescription)

            ignored_geofences = len(geofences_names)
            done_geofences = total_geofences - ignored_geofences
            name_ignored_geofences = ", ".join(geofences_names)
            porcentage_points = Decimal(done_geofences / total_geofences).quantize(Decimal('0.00')) if total_geofences else Decimal(0)
        
        else:
            time_first_geofence = None
            time_last_geofence = None
            done_geofences = None
            ignored_geofences = None
            name_ignored_geofences = None
            porcentage_points = None
        
        return time_first_geofence, time_last_geofence, done_geofences, \
                ignored_geofences, name_ignored_geofences, porcentage_points


class MasterEvent(ModelSQL,ModelView):
    """Eventos Maestros formado a partir de los nombres de eventLocation.
    
    Usado para unificar los eventos entre los diferentes vehículos
    """
    
    __name__ = "datos_sonar.master_event"
    
    name = fields.Char("Nombre",
                       readonly=True,
                       required=True)
    description = fields.Char("Nombre alternativo",)
    to_check = fields.Boolean('Por revisar',
                              help="Indica si se debe revisar en busca de sinónimos")
    parent = fields.Many2One('datos_sonar.master_event','Evento Padre',
                             help="Usado para Eventos sinonimos,"
                             "Si usa este campo asegurese que el padre"
                             "tambien se tenga a si mismo.",
                             domain=['OR',[('id','=',Eval('id'))],[('parent','>',0)]]
                             )
    
    @fields.depends('parent')
    def on_change_parent(self):
        if not self.check_parent():
            self.parent = None
        else:
            return
    
    @classmethod
    def __setup__(cls):
        super(MasterEvent, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.name),
                'Solo debe haber un registro para un name')
        ]

    
    def pre_validate(self):
        super(MasterEvent, self).pre_validate()
        if not self.check_parent():
            self.raise_user_error("El evento padre tambien se debe tener a el mismo como padre")
    
    def check_parent(self):
        if self.parent:
            if self.parent.id == self.id:
                return True
            if not self.parent.parent:
                return False
            if self.parent.id != self.parent.parent.id:
                return False
        return True
    
    @classmethod
    def get_all_events_with_parent(cls):
        """Devuelve todos los eventos con su padre si tiene.
        
        devuelve 2 listas de tuplas: columnas y registros
         ['id', 'name', 'parent'] y
         [(1981, 'Ingreso a Cancha del Dorado', None), ...]
        """
        cursor = Transaction().connection.cursor()
        t_mevent = cls.__table__()
        t_mevent2 = cls.__table__()

        me_join = t_mevent.join(t_mevent2)
        me_join.condition = me_join.right.id == t_mevent.parent
        me_join.type_ = 'LEFT'
        me_select = me_join.select(
            t_mevent.id,
            t_mevent.name,
            t_mevent2.name.as_('parent')
            )
        cursor.execute(*me_select)
        me_col_sql = [x.name for x in cursor.description]
        me_rows = cursor.fetchall()
        return me_col_sql,me_rows
    
    @classmethod
    def get_all_synonym_for_name(cls,name):
        """Devuelve todos los sinonimos para un nombre de evento dado.
        @attention: no usar recursivamente por problemas de rendimiento.
        """
        records = cls.search(['OR',[('name','=',name)],[('parent.name','=',name)]])
        names = [x.name for x in records]
        return names
    
    @classmethod
    def get_all_synonym_for_id(cls,id):
        """Devuelve todos los sinonimos para un id de evento dado.
        @attention: no usar recursivamente por problemas de rendimiento.
        """
        records = cls.search(['OR',[('id','=',id)],[('parent','=',id)]])
        names = [x.name for x in records]
        logger.info("INICIA TOTALIZAR DIA {0}".format(names))
        logger.info("INICIA TOTALIZAR DIA {0}".format(records))
        return names

class EventsForDay(ModelSQL,ModelView):
    """Cantidad de eventos por dia."""
    
    __name__ = "datos_sonar.events_for_day"
    
    _rec_name = 'mobile_id'
    
    date = fields.Date('Fecha',
                       required=True)
    day = fields.Function(fields.Integer('Día'),'on_change_with_day')
    month = fields.Function(fields.Integer('Mes'),'on_change_with_month')
    year = fields.Function(fields.Integer('Año'),'on_change_with_year')
    event = fields.Many2One('datos_sonar.master_event','Evento',
                            help="Puede ser aproximado pues hay sinónimos",
                            required=True)
    mobil_id = fields.Many2One('datos_sonar.mobiles','Vehiculo',
                                help="Vehículo al que hace referencia este total",
                                required=True,
                                )
    quantity = fields.Integer('Cantidad',
                              help="Cantidad de eventos para este vehículo este día.",
                              required=True,
                              )
    parent = fields.Function(fields.Char('Sinónimo'),'on_change_with_parent')
    
    @classmethod
    def __setup__(cls):
        super(EventsForDay, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.date,table.mobil_id,table.event),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
        ]
        cls._order = [
            ('date', 'DESC'),
            ('mobil_id', 'ASC'),
            ('event','ASC')
            ]
    
    @fields.depends('event')
    def on_change_with_parent(self,name=None):
        if self.event:
            if self.event.parent:
                return self.event.parent.name
    
    @fields.depends('date')
    def on_change_with_day(self,name=None):
        if self.date:
            return self.date.day
    
    @fields.depends('date')
    def on_change_with_month(self,name=None):
        if self.date:
            return self.date.month
    
    @fields.depends('date')
    def on_change_with_year(self,name=None):
        if self.date:
            return self.date.year
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('mobil_id', ) + tuple(clause[1:]),
                ('event', ) + tuple(clause[1:]),
            ]
    
    @classmethod
    def do_yesterday(cls):
        """Genera el total diario del dia anterior."""
        date_ = date.today() - timedelta(days=1)
        cls.count_day(date_)
    

    @classmethod
    def count_day_on_period(cls,init_date,end_date):
        """ejecuta count_day() para cada dia dentro del rango."""
        
        if isinstance(init_date, str):
            init_date = datos_sonar_lib.str2date(init_date)
        if isinstance(end_date, str):
            end_date = datos_sonar_lib.str2date(end_date)
        days = (end_date - init_date).days
        for  x in range(0,days+1):
            cls.count_day(init_date + timedelta(days=x))
    
    @classmethod
    def count_day(cls,date_=None):
        """Obtiene y almacena la cantidad de eventos por dia para
        todos los vehiculos de event location.
        
        @param date_: datetime o str en formato %Y-%m-%d
        """
        
        if isinstance(date_, str):
            date_ = datos_sonar_lib.str2date(date_)
        EventLocation = Pool().get('datos_sonar.event_location')
        MasterEvent = Pool().get('datos_sonar.master_event')
        
        logger.info("INICIA TOTALIZAR DIA {0}".format(date_))
        logger.info("INICIA CONSULTA {0}".format(date_))
        me_col_sql, me_rows = MasterEvent.get_all_events_with_parent()
        
        #lista con el nombre de los eventos maestreos
        master_events = set([x[me_col_sql.index('name')] for x in me_rows])
        #diccionario de eventos maestros con sinonimos de la forma
        #{nombre del evento:nombre del evento padre,...}
        master_events_with_parent = {
            x[me_col_sql.index('name')]: x[me_col_sql.index('parent')] for x in me_rows if x[me_col_sql.index('parent')]
            }
        master_events_id = {x[me_col_sql.index('name')]:x[me_col_sql.index('id')] for x in me_rows}
        #consulta los totales de eventos por dia
        e_col_sql, e_rows = cls.query_total_for_event_for_day(date_)
        logger.info("FINALIZA CONSULTA {0}".format(date_))
        
        if not e_rows:
            logger.info("SIN EVENTOS A TOTALIZAR EL DIA {0}".format(date_))
            return
        
        #si existen registros sin evento maestro, o con eventos sinonimos 
        # se extraen
        e_rows_to_create_me = []
        e_rows_with_parent = []
        e_rows_swap = []

        me_to_create = set()
        for x in range(0, len(e_rows)):
            e_description = e_rows[x][e_col_sql.index('eventDescription')]
            if e_description in master_events:
                if e_description in master_events_with_parent.keys():
                    e_rows_with_parent.append(e_rows[x])
                else:
                    e_rows_swap.append(e_rows[x])
            else:
                e_rows_to_create_me.append(e_rows[x])
                me_to_create.add(e_description)
        
        e_rows = e_rows_swap
        del e_rows_swap
                
        #crear eventos maestros nuevos
        me_values = MasterEvent.create([{'name':x,'to_check': True} for x in me_to_create])
        for me_value in me_values: 
            master_events_id.update({me_value.name:me_value.id})
        
        #retotalizar con sinonimos
        if e_rows_with_parent:
            list_e_with_parent = cls.rebuild_total_day_with_parent(
                e_rows_with_parent,
                e_col_sql,
                master_events_with_parent
                )
            e_rows += list_e_with_parent
        
        #reemplazar los nombres de los eventos con el id del evento maestreo
        e_rows = cls.change_master_event_name_with_id(e_rows,e_col_sql,master_events_id)
        
        #si existe el día eliminarlo
        cls.delete(cls.search([('date','=',date_)]))
        #guardar totales de dias
        name_fields = ['mobil_id','event','date','quantity']
        cls.create([dict(zip(name_fields,x)) for x in e_rows])
        
        logger.info("FINALIZA TOTALIZAR DIA {0}".format(date_))
        return
    
    @classmethod
    def query_total_for_event_for_day(cls,date_):
        """Devuelve las columnas y registros de la consulta de totales por evento por dia"""
        Configuration = Pool().get('datos_sonar.configurations')
        EventLocation = Pool().get('datos_sonar.event_location')
        
        configuration = Configuration.get_singleton()
        cursor = Transaction().connection.cursor()
        t_event = EventLocation.__table__()
        init_time , end_time = datos_sonar_lib.colombia_date2utctime(date_)
        
        gps_GMT_col = AtTimeZone(
                        AtTimeZone(t_event.gps_GMT,'UTC'),
                        configuration.zona_horaria).cast('date').as_('date')
        
        columns = [
            t_event.mobil_id,
            t_event.eventDescription,
            gps_GMT_col,
            Count(t_event.id).as_('quantity'),
            ]
                        
        #consultar todos los eventos del dia totalizados por
        #vehiculo, dia, eventDescription
        e_select = t_event.select(
                *columns,
                where=((t_event.gps_GMT >= init_time) & (t_event.gps_GMT <= end_time)),
                order_by=(t_event.mobil_id),
                group_by=(
                    t_event.mobil_id,
                    t_event.eventDescription,
                    gps_GMT_col,
                    )
                )

        cursor.execute(*e_select)
        e_col_sql = [x.name for x in cursor.description]
        e_rows = cursor.fetchall()
        return e_col_sql, e_rows
    
    @classmethod
    def rebuild_total_day_with_parent(cls,e_rows_with_parent,e_col_sql,master_events_with_parent):
        """Reecalcula los totales de eventos por dia agrupando la suma por el evento padre.
        
        @attention: solo funciona cuando e_rows_with_parent tiene totales de un solo dia.
        """
        build_e_with_parent = dict()
        date__ = e_rows_with_parent[0][e_col_sql.index('date')]
        for row in e_rows_with_parent:
            description = row[e_col_sql.index('eventDescription')]
            mobil = row[e_col_sql.index('mobil_id')]
            me =  master_events_with_parent[description]
            quantity = row[e_col_sql.index('quantity')]
            if not mobil in build_e_with_parent.keys():
                build_e_with_parent[mobil] = {}
            if not me in build_e_with_parent[mobil].keys():
                build_e_with_parent[mobil][me] = 0
            build_e_with_parent[mobil][me] += quantity
        
        list_e_with_parent = []
        for mobil,events in build_e_with_parent.items():
            for event, total in events.items():
                list_e_with_parent.append([mobil,event,date__,total])
                
        return list_e_with_parent
    
    @classmethod
    def change_master_event_name_with_id(cls,e_rows,e_col_sql,master_events_id):
        """cambia el nombre del evento por el identificador en la lista pasada.
        
        @param e_rows: lista de campos
        @param e_col_sql: nombre de las columnas
        @param master_events_id: diccionario con el nombre del evento y su id.
        """
        for x in range(0,len(e_rows)):
            description = e_rows[x][e_col_sql.index('eventDescription')]
            e_rows[x] = list(e_rows[x])
            e_rows[x][e_col_sql.index('eventDescription')] = master_events_id[description]
        return e_rows

class EventsForMonth(ModelSQL,ModelView):
    """Cantidad de eventos por mes."""
    
    __name__ = "datos_sonar.events_for_month"
    
    _rec_name = 'mobile_id'
    
    date = fields.Function(fields.Date('Fecha',
                       help="Se usa el primer dia del mes,\n"
                       "ya que solo se usa el mes y el año."),
                        'on_change_with_date')
    month = fields.Integer('Mes')
    year = fields.Integer('Año')
    event = fields.Many2One('datos_sonar.master_event','Evento',
                            help="Puede ser aproximado pues hay sinónimos",
                            required=True)
    mobil_id = fields.Many2One('datos_sonar.mobiles','Vehiculo',
                                help="Vehículo al que hace referencia este total",
                                required=True,
                                )
    quantity = fields.Integer('Cantidad',
                              help="Cantidad de eventos para este vehículo este día.",
                              required=True,
                              )
    partial = fields.Boolean('Parcial',
                             help="Indica si el total es parcial (sin acabar el mes)"
                             )
    
    @fields.depends('year,month')
    def on_change_with_date(self,name=None):
        if self.year and self.month:
            return date(self.year,self.month,1)
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('mobil_id', ) + tuple(clause[1:]),
                ('event', ) + tuple(clause[1:]),
            ]
        
    @classmethod
    def __setup__(cls):
        super(EventsForMonth, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.month,table.year,table.mobil_id,table.event),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
            ]
        cls._order = [
            ('year', 'DESC'),
            ('month', 'DESC'),
            ('mobil_id', 'ASC'),
            ('event','ASC')
            ]
        

    @classmethod
    def do_current(cls):
        """Genera el total mensual del mes el actual y el anterior.
        
        El anterior solo se totaliza si no se ha realizado."""
        date_ = date.today()
        previous = datos_sonar_lib.previous_month(date_)
        count_previous = cls.search_count([
            ('year','=',previous.year),
            ('month','=',previous.month),
            ('partial','=',False)
            ])
        if not count_previous:
            cls.count_month(previous.replace(day=1))
        cls.count_month(date_.replace(day=1))
    
    @classmethod
    def count_month_on_period(cls,init_date,end_date):
        """ejecuta count_month() para cada dia dentro del rango.
        
        @param init_date: string in format %Y-%m-%d or date object
        @param end_date: string in format %Y-%m-%d or date object
        """
        if isinstance(init_date, str):
            init_date = datos_sonar_lib.str2date(init_date)
        if isinstance(end_date, str):
            end_date = datos_sonar_lib.str2date(end_date)

        months = datos_sonar_lib.date_month_in_range(init_date,end_date)
        for  month in months:
            cls.count_month(month)
    
    @classmethod
    def count_month(cls,date_):
        """Totaliza y almacena los eventos de un mes para todos los vehiculos"""
       
        if isinstance(date_, str):
            date_ = datos_sonar_lib.str2date(date_) 
        
        EventLocation = Pool().get('datos_sonar.event_location')
        MasterEvent = Pool().get('datos_sonar.master_event')
        
        logger.info("INICIA TOTALIZAR MES {0}".format(date_))
        logger.info("INICIA CONSULTA {0}".format(date_))
        
        me_col_sql, me_rows = MasterEvent.get_all_events_with_parent()
        
        #lista con el nombre de los eventos maestreos
        master_events = set([x[me_col_sql.index('name')] for x in me_rows])
        #diccionario de eventos maestros con sinonimos de la forma
        #{nombre del evento:nombre del evento padre,...}
        master_events_with_parent = {
            x[me_col_sql.index('name')]: x[me_col_sql.index('parent')] for x in me_rows if x[me_col_sql.index('parent')]
            }
        master_events_id = {x[me_col_sql.index('name')]:x[me_col_sql.index('id')] for x in me_rows}
        #consulta los totales de eventos por dia
        e_col_sql, e_rows = cls.query_total_for_event_for_month(date_)
        logger.info("FINALIZA CONSULTA {0}".format(date_))

        
        if not e_rows:
            logger.info("SIN EVENTOS A TOTALIZAR EL MES {0}".format(date_))
            return
        
        #si existen registros sin evento maestro, o con eventos sinonimos 
        # se extraen
        e_rows_to_create_me = []
        e_rows_with_parent = []
        e_rows_swap = []

        me_to_create = set()
        for x in range(0, len(e_rows)):
            e_description = e_rows[x][e_col_sql.index('eventDescription')]
            if e_description in master_events:
                if e_description in master_events_with_parent.keys():
                    e_rows_with_parent.append(e_rows[x])
                else:
                    e_rows_swap.append(e_rows[x])
            else:
                e_rows_to_create_me.append(e_rows[x])
                me_to_create.add(e_description)
        
        e_rows = e_rows_swap
        del e_rows_swap
                
        #crear eventos maestros nuevos
        me_values = MasterEvent.create([{'name':x,'to_check': True} for x in me_to_create])
        for me_value in me_values: 
            master_events_id.update({me_value.name:me_value.id})
        
        #retotalizar con sinonimos
        list_e_with_parent = cls.rebuild_total_month_with_parent(
            e_rows_with_parent,
            e_col_sql,
            master_events_with_parent
            )
        e_rows += list_e_with_parent
         
        #reemplazar los nombres de los eventos con el id del evento maestreo
        e_rows = cls.change_master_event_name_with_id(e_rows,e_col_sql,master_events_id)
        
        #si existe el mes eliminarlo
        cls.delete(cls.search([('year','=',date_.year),('month','=',date_.month)]))
        #guardar totales de dias
        field_names = list(e_col_sql)
        field_names[e_col_sql.index('eventDescription')] = 'event'
        values = [dict(zip(field_names,x)) for x in e_rows]
        #agrega parcial al total si es el mes actual.
        if not datos_sonar_lib.less_month_test(date_,date.today()):
            for value in values:
                value['partial'] = True
            
        cls.create(values)
        
        logger.info("FINALIZA TOTALIZAR MES {0}".format(date_))
        return
    
    @classmethod
    def query_total_for_event_for_month(cls,date_):
        """Devuelve las columnas y registros de la consulta de totales por evento por dia"""
        Configuration = Pool().get('datos_sonar.configurations')
        EventLocation = Pool().get('datos_sonar.event_location')
        
        configuration = Configuration.get_singleton()
        cursor = Transaction().connection.cursor()
        t_event = EventLocation.__table__()
        
        init_time, end_time = datos_sonar_lib.colombia_month2utctime(date_.year,date_.month)
        
        year_col = ToChar(
                AtTimeZone(
                    AtTimeZone(t_event.gps_GMT,'UTC'),
                    configuration.zona_horaria),
                'YYYY'
            )
        month_col = ToChar(
                AtTimeZone(
                    AtTimeZone(t_event.gps_GMT,'UTC'),
                    configuration.zona_horaria),
                'MM'
            )

        columns = [
            t_event.mobil_id,
            t_event.eventDescription,
            year_col.as_('year'),
            month_col.as_('month'),
            Count(t_event.id).as_('quantity'),
            ]
        
        #consultar todos los eventos del dia totalizados por
        #vehiculo, dia, eventDescription
        e_select = t_event.select(
                *columns,
                where=((t_event.gps_GMT >= init_time) & (t_event.gps_GMT <= end_time)),
                order_by=(t_event.mobil_id),
                group_by=(t_event.mobil_id,
                          t_event.eventDescription,
                          year_col,
                          month_col,
                          )
                )
        cursor.execute(*e_select)
        e_col_sql = [x.name for x in cursor.description]
        e_rows = cursor.fetchall()
        return e_col_sql, e_rows
    
    @classmethod
    def rebuild_total_month_with_parent(cls,e_rows_with_parent,e_col_sql,master_events_with_parent):
        """Reecalcula los totales de eventos por dia agrupando la suma por el evento padre.
        
        @attention: solo funciona cuando e_rows_with_parent tiene totales de un solo dia.
        """
        build_e_with_parent = dict()
        year = e_rows_with_parent[0][e_col_sql.index('year')]
        month = e_rows_with_parent[0][e_col_sql.index('month')]
        for row in e_rows_with_parent:
            description = row[e_col_sql.index('eventDescription')]
            mobil = row[e_col_sql.index('mobil_id')]
            me =  master_events_with_parent[description]
            quantity = row[e_col_sql.index('quantity')]
            if not mobil in build_e_with_parent.keys():
                build_e_with_parent[mobil] = {}
            if not me in build_e_with_parent[mobil].keys():
                build_e_with_parent[mobil][me] = 0
            build_e_with_parent[mobil][me] += quantity
        
        list_e_with_parent = []
        for mobil,events in build_e_with_parent.items():
            for event, total in events.items():
                list_e_with_parent.append([mobil,event,year,month,total])
                
        return list_e_with_parent
    
    @classmethod
    def change_master_event_name_with_id(cls,e_rows,e_col_sql,master_events_id):
        """cambia el nombre del evento por el identificador en la lista pasada.
        
        @param e_rows: lista de campos
        @param e_col_sql: nombre de las columnas
        @param master_events_id: diccionario con el nombre del evento y su id.
        """
        for x in range(0,len(e_rows)):
            description = e_rows[x][e_col_sql.index('eventDescription')]
            e_rows[x] = list(e_rows[x])
            e_rows[x][e_col_sql.index('eventDescription')] = master_events_id[description]
        return e_rows


class AnaliticMasterEvent(ModelSQL,ModelView):
    """Eventos Maestros Analíticos.
    
    Usado para crear totales diarios y mensuales que tienen condicionales.
    """
    
    __name__ = "datos_sonar.analitic_master_event"
    
    name = fields.Char("Nombre",
                       required=True)
    active = fields.Boolean('Activo',
                            help="Si no esta activo no se ignora en los totales automáticos"
                            )
    description = fields.Char("Descripción",)
    to_check = fields.Boolean('Por revisar',
                              help="Indica si se debe revisar en busca de sinónimos")
    conditions = fields.One2Many('datos_sonar.analitic_master_event.condition','master_event','Condiciones')
#     groupers = fields.One2Many('datos_sonar.analitic_master_event.groupers','master_event','Condiciones')
    notes = fields.Char('observacion')
    
    def select_sql_for_day(self,date_,mobil_id=None):
        """Devuelve el select (python-sql) con el total de eventos por día, por vehículo para esta fecha,con las condiciones dadas.
        
        Devuelve necesariamente las columnas ['date','quantity','mobil_id']
        a menos que se indique mobil que devolverá los identificadores de los eventos
        relacionados con este evento maestro para el mobil dado.
        """
        Configuration = Pool().get('datos_sonar.configurations')
        EventLocation = Pool().get('datos_sonar.event_location')
        
        configuration = Configuration.get_singleton()
        t_event = EventLocation.__table__()
        
        init_time , end_time = datos_sonar_lib.colombia_date2utctime(date_)
        
        if mobil_id:
            columns = [
                t_event.id
                ]
        else:
            gps_GMT_col = AtTimeZone(
                AtTimeZone(t_event.gps_GMT,'UTC'),
                configuration.zona_horaria
                ).cast('date').as_('date')
            columns = [
                t_event.mobil_id,
                gps_GMT_col,
                Count(t_event.id).as_('quantity'),
                ]
        
        where=((t_event.gps_GMT >= init_time) & (t_event.gps_GMT <= end_time))
        if self.conditions:
            for condition in self.conditions:
                if condition.operator == '=':
                    where &= operators.Equal(getattr(t_event, condition.field),condition.operand)
        
        if mobil_id:
            where &= t_event.mobil_id == mobil_id
            e_select = t_event.select(
                    *columns,
                    where=where,
                    order_by=(t_event.id),
                    )
        else:
            group_by = [
                t_event.mobil_id,
                gps_GMT_col,
                ]
            e_select = t_event.select(
                    *columns,
                    where=where,
                    order_by=(t_event.mobil_id),
                    group_by=group_by,
                    )
        return e_select
    
    def select_sql_for_month(self,date_,mobil_id=None):
        """Devuelve el select (python-sql) con el total de eventos por mes, por vehículo para esta fecha,con las condiciones dadas.
        
        Devuelve necesariamente las columnas ['month','year','quantity','mobil_id']
        a menos que se indique mobil que devolverá los identificadores de los eventos
        relacionados con este evento maestro para el mobil dado.
        """
        Configuration = Pool().get('datos_sonar.configurations')
        EventLocation = Pool().get('datos_sonar.event_location')
        
        configuration = Configuration.get_singleton()
        t_event = EventLocation.__table__()
    
        init_time, end_time = datos_sonar_lib.colombia_month2utctime(date_.year,date_.month)
        
        if mobil_id:
            columns = [
                t_event.id
                ]
        else:
            year_col = ToChar(
                    AtTimeZone(
                        AtTimeZone(t_event.gps_GMT,'UTC'),
                        configuration.zona_horaria),
                    'YYYY'
                )
            month_col = ToChar(
                    AtTimeZone(
                        AtTimeZone(t_event.gps_GMT,'UTC'),
                        configuration.zona_horaria),
                    'MM'
                )

            columns = [
                t_event.mobil_id,
                year_col.as_('year'),
                month_col.as_('month'),
                Count(t_event.id).as_('quantity'),
                ]
        
        where=((t_event.gps_GMT >= init_time) & (t_event.gps_GMT <= end_time))
        if self.conditions:
            for condition in self.conditions:
                if condition.operator == '=':
                    where &= operators.Equal(getattr(t_event, condition.field),condition.operand)
        
        if mobil_id:
            where &= t_event.mobil_id == mobil_id
            e_select = t_event.select(
                *columns,
                where=where,
                order_by=(t_event.id),
                )
        else:
            group_by = [
                t_event.mobil_id,
                year_col,
                month_col,
                ]
            e_select = t_event.select(
                    *columns,
                    where=where,
                    order_by=(t_event.mobil_id),
                    group_by=group_by
                    )
        
        return e_select
    

class AnaliticCondition(ModelSQL,ModelView):
    "condiciones para eventos maestros analiticos"
    
    __name__ = 'datos_sonar.analitic_master_event.condition'

    master_event = fields.Many2One('datos_sonar.analitic_master_event', 'Evento')
    field_name = fields.Many2One('ir.model.field','Campo',
                                help="Campo que se condicionara",
                                required=True,
                                domain=[('model.model','=','datos_sonar.event_location')],
                                states={'invisible':False}
                                )
    operator = fields.Selection([('=','='),
                                 #('!=','!='),
                                 #('<','<'),
                                 #('>','>')
                                 ],'Operador',
                                required=True
                                )
    operand = fields.Char('Operando',
                          required=True
                          )

    
# class AnaliticGrouper(ModelSQL,ModelView):
#     "Agrupadores para eventos maestros analiticos"
#     
#     __name__ = 'datos_sonar.analitic_master_event.condition'
# 
#     master_event = fields.Many2One('datos_sonar.analitic_master_event', 'Evento')
#     field_name = fields.Many2One('ir.model.field','Campo',
#                                 help="Campo que se condicionara",
#                                 required=True,
#                                 domain=[('model.model','=','datos_sonar.event_location')],
#                                 states={'invisible':False}
#                                 )
#     operator = fields.Selection([('=','Igual')],'Operador',
#                                 required=True
#                                 )
#     operand = fields.Char('Operando',
#                           required=True
#                           )


class AnaliticEventsForDay(ModelSQL,ModelView):
    """Cantidad de eventos con condiciones por dia."""
    
    __name__ = "datos_sonar.analitic_events_for_day"
    
    date = fields.Date('Fecha',
                       required=True)
    day = fields.Function(fields.Integer('Día'),'on_change_with_day')
    month = fields.Function(fields.Integer('Mes'),'on_change_with_month')
    year = fields.Function(fields.Integer('Año'),'on_change_with_year')
    event = fields.Many2One('datos_sonar.analitic_master_event','Evento',
                            required=True)
    mobil_id = fields.Many2One('datos_sonar.mobiles','Vehiculo',
                                help="Vehículo al que hace referencia este total",
                                required=True,
                                )
    quantity = fields.Integer('Cantidad',
                              help="Cantidad de eventos para este vehículo este día.",
                              required=True,
                              )
    have_usos_metro = fields.Boolean('Tiene Usos metro',
                                help="Indica si tiene usos metro.\n"
                                "Solo se usa para los eventos con sospecha de errores en la "
                                "Sincronización.",
                                )
    diff_with_sonar = fields.Boolean('Diferencias con Sonar',
                                     help="Indica si este total tiene diferencias con la plataforma Sonar.\n"
                                     "Usado para clasificar los tipos de errores en la cantidad de datos."
                                     )
    note = fields.Text('Observaciones')
    

    
    @classmethod
    def count_day_on_period(cls,init_date,end_date):
        """ejecuta count_day() para cada dia dentro del rango.
        
        @param init_date: string in format %Y-%m-%d or date object
        @param end_date: string in format %Y-%m-%d or date object
        """
        if isinstance(init_date, str):
            init_date = datos_sonar_lib.str2date(init_date)
        if isinstance(end_date, str):
            end_date = datos_sonar_lib.str2date(end_date)

        days = (end_date - init_date).days
        for  x in range(0,days+1):
            cls.count_day(init_date + timedelta(days=x))
    
    @classmethod
    def do_yesterday(cls):
        """Genera el total diario del dia anterior."""
        date_ = date.today() - timedelta(days=1)
        cls.count_day(date_)
    
    @classmethod
    def count_day(cls,date_):
        """Obtiene y almacena la cantidad de eventos por dia para
        todos los vehiculos.
        
        @param date_: datetime.date o string en el formato %Y-%m-%d
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        EventLocation = Pool().get('datos_sonar.event_location')
        AnaliticMasterEvent = Pool().get('datos_sonar.analitic_master_event')
        cursor = Transaction().connection.cursor()
        
        if isinstance(date_, str):
            date_ = datos_sonar_lib.str2date(date_)

        mobiles = set(Mobiles.allIds())
        logger.info("INICIA TOTALIZAR DIA EVENTOS ANALIZADOS{0}".format(date_))
        m_events = AnaliticMasterEvent.search([('active','=',True)])
        for m_event in m_events:
            logger.info("INICIA TOTALIZAR DIA {0} para el evento {1}".format(date_,m_event.name))
            logger.info("INICIA CONSULTA {0}".format(date_))
            select = m_event.select_sql_for_day(date_)
            cursor.execute(*select)
            e_col_sql = [x.name for x in cursor.
                         description]
            if not set(['date','quantity','mobil_id']).issubset(set(e_col_sql)):
                logger.error("Ignorando evento {ev} porque a la consulta le falta una columna".format(ev=m_event.name))
            e_rows = cursor.fetchall()
            logger.info("FIN CONSULTA {0}".format(date_))
            values = []
            for e_row in e_rows:
                values.append({
                    'date':e_row[e_col_sql.index('date')],
                    'quantity':e_row[e_col_sql.index('quantity')],
                    'mobil_id':e_row[e_col_sql.index('mobil_id')],
                    'event':m_event.id
                    })
            
            #crear con 0 eventos los vehiculos que no tienen evento
            mobiles_with_quantity = set([x[e_col_sql.index('mobil_id')] for x in e_rows])
            for x in mobiles - mobiles_with_quantity:
                values.append({
                    'date':date_,
                    'quantity':0,
                    'mobil_id':x,
                    'event':m_event.id
                    })
                
            #si existe el día eliminarlo
            cls.delete(cls.search([('date','=',date_),('event','=',m_event.id)]))
            
            cls.create(values)
            logger.info("FINALIZA TOTALIZAR DIA {} para el evento {}".format(date_,m_event.name))
        logger.info("FINALIZA TOTALIZAR DIA EVENTOS ANALIZADOS{0}".format(date_))
    
    
    @classmethod
    def __setup__(cls):
        super(AnaliticEventsForDay, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.date,table.mobil_id,table.event),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
        ]
        cls._order = [
            ('date', 'DESC'),
            ('mobil_id', 'ASC'),
            ]
    
    
    @fields.depends('date')
    def on_change_with_day(self,name=None):
        if self.date:
            return self.date.day
    
    @fields.depends('date')
    def on_change_with_month(self,name=None):
        if self.date:
            return self.date.month
    
    @fields.depends('date')
    def on_change_with_year(self,name=None):
        if self.date:
            return self.date.year
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('mobil_id', ) + tuple(clause[1:]),
                ('event', ) + tuple(clause[1:]),
            ]


class AnaliticEventsForMonth(ModelSQL,ModelView):
    """Cantidad de eventos condicionados por mes."""
    
    __name__ = "datos_sonar.analitic_events_for_month"
    
    date = fields.Function(fields.Date('Fecha',
                       help="Se usa el primer dia del mes,\n"
                       "ya que solo se usa el mes y el año."),
                        'on_change_with_date')
    month = fields.Integer('Mes',
                           required=True)
    year = fields.Integer('Año',
                          required=True)
    event = fields.Many2One('datos_sonar.analitic_master_event','Evento',
                            required=True)
    mobil_id = fields.Many2One('datos_sonar.mobiles','Vehiculo',
                                help="Vehículo al que hace referencia este total",
                                required=True,
                                )
    quantity = fields.Integer('Cantidad',
                              help="Cantidad de eventos para este vehículo este día.",
                              required=True,
                              )
    partial = fields.Boolean('Parcial',
                             help="Indica si el total es parcial (sin acabar el mes)"
                             )
    
    @fields.depends('year,month')
    def on_change_with_date(self,name=None):
        if self.year and self.month:
            return date(self.year,self.month,1)
    
    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('mobil_id', ) + tuple(clause[1:]),
                ('event', ) + tuple(clause[1:]),
            ]
        
    @classmethod
    def __setup__(cls):
        super(AnaliticEventsForMonth, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.month,table.year,table.mobil_id,table.event),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
            ]
        cls._order = [
            ('year', 'DESC'),
            ('month', 'DESC'),
            ('mobil_id', 'ASC'),
            ('event','ASC')
            ]

    @classmethod
    def do_current(cls):
        """Genera el total mensual del mes el actual y el anterior.
        
        El anterior solo se totaliza si no se ha realizado."""
        date_ = date.today()
        previous = datos_sonar_lib.previous_month(date_.year,date_.month)
        count_previous = cls.search_count([
            ('year','=',previous.year),
            ('month','=',previous.month),
            ('partial','=',False)
            ])
        if not count_previous:
            cls.count_month(previous.replace(day=1))
        cls.count_month(date_.replace(day=1))
    
    @classmethod
    def count_month_on_period(cls,init_date,end_date):
        """ejecuta count_month() para cada dia dentro del rango.
        
        @param init_date: string in format %Y-%m-%d or date object
        @param end_date: string in format %Y-%m-%d or date object
        """
        if isinstance(init_date, str):
            init_date = datos_sonar_lib.str2date(init_date)
        if isinstance(end_date, str):
            end_date = datos_sonar_lib.str2date(end_date)

        months = datos_sonar_lib.date_month_in_range(init_date,end_date)
        for  month in months:
            cls.count_month(month)
    
    @classmethod
    def count_month(cls,date_):
        """Totaliza y almacena los eventos de un mes para todos los vehiculos.
        
        @param date_: string in format %Y-%m-%d or date object
        """
        EventLocation = Pool().get('datos_sonar.event_location')
        AnaliticMasterEvent = Pool().get('datos_sonar.analitic_master_event')
        Mobiles = Pool().get('datos_sonar.mobiles')
        cursor = Transaction().connection.cursor()
        mobiles = set(Mobiles.allIds())
        
        if isinstance(date_, str):
            date_ = datos_sonar_lib.str2date(date_)
        
        logger.info("INICIA TOTALIZAR MES EVENTOS ANALIZADOS {0}".format(date_))
        m_events = AnaliticMasterEvent.search([('active','=',True)])
        for m_event in m_events:
            logger.info("INICIA TOTALIZAR DIA {0} para el evento {1}".format(date_,m_event.name))
            logger.info("INICIA CONSULTA {0}".format(date_))
            select = m_event.select_sql_for_month(date_)
            cursor.execute(*select)
            e_col_sql = [x.name for x in cursor.
                         description]
            if not set(['year','month','quantity','mobil_id']).issubset(set(e_col_sql)):
                logger.error("Ignorando evento {ev} porque a la consulta le falta una columna".format(ev=m_event.name))
            e_rows = cursor.fetchall()
            logger.info("FIN CONSULTA {0}".format(date_))
            values = []
            for e_row in e_rows:
                values.append({
                    'year':e_row[e_col_sql.index('year')],
                    'month':e_row[e_col_sql.index('month')],
                    'quantity':e_row[e_col_sql.index('quantity')],
                    'mobil_id':e_row[e_col_sql.index('mobil_id')],
                    'event':m_event.id
                    })
            #crear con 0 eventos los vehiculos que no tienen evento
            mobiles_with_quantity = set([x[e_col_sql.index('mobil_id')] for x in e_rows])
            for x in mobiles - mobiles_with_quantity:
                values.append({
                    'year':date_.year,
                    'month':date_.month,
                    'quantity':0,
                    'mobil_id':x,
                    'event':m_event.id
                    })
            #si existe el día eliminarlo
            cls.delete(cls.search([('month','=',date_.month),('year','=',date_.year),('event','=',m_event.id)]))

            #agrega parcial al total si es el mes actual.
            if not datos_sonar_lib.less_month_test(date_,date.today()):
                for value in values:
                    value['partial'] = True

            cls.create(values)
            logger.info("FINALIZA TOTALIZAR MES {} para el evento {}".format(date_,m_event.name))
        logger.info("FINALIZA TOTALIZAR MES EVENTOS ANALIZADOS{0}".format(date_))
    
class OpenEvents(Wizard):
    """Abre eventos con contexto."""
    
    __name__ = 'datos_sonar.events_for_day.open_events'
    start = StateTransition()
    select_model = StateTransition()
    for_day = StateAction('datos_sonar.act_event_location_without_filter_form')
    for_month = StateAction('datos_sonar.act_events_for_day_form')
    for_day_analitic = StateAction('datos_sonar.act_event_location_without_filter_form')
    for_month_analitic = StateAction('datos_sonar.act_analitic_events_for_day')
    
    def transition_start(self):
        model_ = Transaction().context['active_model']
        if model_ == 'datos_sonar.events_for_day':
            return 'for_day'
        elif model_ == 'datos_sonar.events_for_month':
            return 'for_month'
        elif model_ == 'datos_sonar.analitic_events_for_month':
            return 'for_month_analitic'
        elif model_ == 'datos_sonar.analitic_events_for_day':
            return 'for_day_analitic'
        else:
            return 'end'
        
    def do_for_day(self,action):
        #@todo eventDescription debe ser con los sinonimos
        EventsForDay = Pool().get('datos_sonar.events_for_day')
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        MasterEvent = Pool().get('datos_sonar.master_event')
        transaction = Transaction()
        
        total = EventsForDay.browse([transaction.context['active_id']])
        total = total[0]
        init_time , end_time = datos_sonar_lib.colombia_date2utctime(total.date)
        event_names = MasterEvent.get_all_synonym_for_id(total.event.id)
        action['pyson_domain'] = [
            ('gps_GMT','>=',init_time),
            ('gps_GMT','<=',end_time),
            ('mobil_id','=',total.mobil_id.id),
            ('eventDescription','in',event_names)
            ]
        action['name'] += " {mobil} ({date}): {event}".format(mobil=total.mobil_id.rec_name,
                                                            date=total.date,
                                                            event=total.event.name
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}
    
    def do_for_month(self,action):
        EventsForMonth = Pool().get('datos_sonar.events_for_month')
        MasterEvent = Pool().get('datos_sonar.master_event')
        
        total = EventsForMonth.browse([Transaction().context['active_id']])
        total = total[0]
        last_day = datos_sonar_lib.month_last_day(total.date.year, total.date.month)
        end_date = total.date.replace(day=last_day)

        event_names = MasterEvent.get_all_synonym_for_id(total.event.id)
        action['pyson_domain'] = [
            ('date','>=',total.date),
            ('date','<=',end_date),
            ('mobil_id','=',total.mobil_id.id),
            ('event.name','in',event_names)
            ]
        action['name'] += " {mobil} ({date}): {event}".format(mobil=total.mobil_id.rec_name,
                                                            date= "{}/{}".format(total.month,total.year),
                                                            event=total.event.name
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}
    
    def do_for_day_analitic(self,action):
        EventsForDayA = Pool().get('datos_sonar.analitic_events_for_day')
        transaction = Transaction()
        
        total = EventsForDayA.browse([transaction.context['active_id']])
        total = total[0]
        
        #Consulto los identificadores de los eventos que cumplen con las condiciones
        sql_ids = total.event.select_sql_for_day(total.date,mobil_id=total.mobil_id.id)
        cursor = transaction.connection.cursor()
        cursor.execute(*sql_ids)
        ids = cursor.fetchall() or []

        action['pyson_domain'] = [
            ('id','in',ids)
            ]
        action['name'] += " {mobil} ({date}): {event}".format(mobil=total.mobil_id.rec_name,
                                                            date=total.date,
                                                            event=total.event.name
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}
    
    def do_for_month_analitic(self,action):
        EventsForMonthA = Pool().get('datos_sonar.analitic_events_for_month')
        transaction = Transaction()
        
        total = EventsForMonthA.browse([transaction.context['active_id']])
        total = total[0]
        last_day = datos_sonar_lib.month_last_day(total.date.year, total.date.month)
        end_date = total.date.replace(day=last_day)

        action['pyson_domain'] = [
            ('date','>=',total.date),
            ('date','<=',end_date),
            ('mobil_id','=',total.mobil_id.id),
            ('event','=',total.event.id)
            ]
        action['name'] += " {mobil} ({date}): {event}".format(mobil=total.mobil_id.rec_name,
                                                            date= "{}/{}".format(total.month,total.year),
                                                            event=total.event.name
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}
    
class OpenDays(Wizard):
    """Abre eventos con contexto."""
    
    __name__ = 'datos_sonar.events_for_month.open_days'
    start_state = 'open_'
    open_ = StateAction('datos_sonar.act_events_for_day_form')
    
    
    def do_open_(self,action):
        EventsForDay = Pool().get('datos_sonar.events_for_day')
        EventsForMonth = Pool().get('datos_sonar.events_for_month')
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        MasterEvent = Pool().get('datos_sonar.master_event')
        
        total = EventsForMonth.browse([Transaction().context['active_id']])
        total = total[0]
        last_day = calendar.monthrange(int(total.date.year), int(total.date.month))[1]
        end_date = total.date.replace(day=last_day)
        
        event_names = MasterEvent.get_all_synonym_for_id(total.event.id)
        logger.info('event_name: {}'.format(event_names))
        action['pyson_domain'] = [
            ('date','>=',total.date),
            ('date','<=',end_date),
            ('mobil_id','=',total.mobil_id.id),
            ('event.name','in',event_names)
            ]
        action['name'] += " {mobil} ({date}): {event}".format(mobil=total.mobil_id.rec_name,
                                                            date= "{}/{}".format(total.month,total.year),
                                                            event=total.event.name
                                                            )
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        return action,{}
    

class TotalDiarioEventLocation(ModelSQL,ModelView):
    ''' @deprecated   Totales por dia por vehiculo de cada uno de los Master tipos de eventos para poder graficar 
    
    Estos totales son estaticos pero se podran regenerar a partir de los eventos importados
    '''
    
    __name__ = "datos_sonar.total_diario_event_location"
    _rec_name = "fecha"
    
    fecha = fields.Date('fecha',
                        help="fecha de la totalizacion del evento",
                        required = True)
    dia = fields.Function(
        fields.Integer(
            'Dia',
            help='Campo Virtual del dia numerico (ej: 5) de la fecha, esto para usar en graficos y reportes',
            readonly=True
        )
        ,'obtener_dia')
    mes = fields.Function(
        fields.Integer(
            'Mes',
            help='Campo Virtual del dia numerico del mes (ej: 4), esto para usar en graficos y reportes',
            readonly=True
        )
        ,'obtener_mes')
    ano = fields.Function(
        fields.Integer(
            'Ano',
            help='Campo Virtual del dia numerico del ano (ej: 2018), esto para usar en graficos y reportes',
            readonly=True
        )
        ,'obtener_ano')
    evento_maestro_id = fields.Many2One('datos_sonar.master_type_event_mobile',
                                    "Evento",
                                    help="Evento (evento maestro y no del vehiculo) que se totaliza",
                                    required=True,
                                    )
    mobil_id = fields. Many2One ('datos_sonar.mobiles',
                                'Vehiculo',
                                help="Vehiculo al que hace referencia este total",
                                required=True,
                                )
    total = fields.Integer('Total',
                        help='Total del evento para el vehiculo y en la fecha especificada',
                        required = True,
                        )
    registros = fields.Text(
        "registros",
        help="Son los registros contados el total"
        )
    
    @classmethod
    def __setup__(cls):
        super(TotalDiarioEventLocation, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.fecha,table.mobil_id,table.evento_maestro_id),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
        ]
    
    
    def obtener_dia(self,field):
        fecha=self.fecha
        return (int(fecha.strftime("%d")))
    
    def obtener_mes(self,field):
        fecha=self.fecha
        return (int(fecha.strftime("%m")))
    
    def obtener_ano(self,field):
        fecha=self.fecha
        return (int(fecha.strftime("%Y")))
    
    @classmethod
    def totalizar_dia_manual(cls,fecha_str):
        """Totaliza un dia pasando el dia en texto, es para poder hacer pruebas
        
        @param fecha_str: date en formato %Y-%m-%d 
        """
        fecha = date.fromordinal(datetime.strptime(fecha_str,"%Y-%m-%d").toordinal())
        cls.totalizar_dia(fecha)
    
    @classmethod
    def totalizar_mes_manual(cls,fecha_str):
        """Totaliza un mes pasando el mes en date, es para poder hacer pruebas
        
        @param fecha_str: date en formato %Y-%m 
        """
        logger.info("INICIA TOTALIZAR mes {0}".format(fecha_str))
        fecha = date.fromordinal(datetime.strptime(fecha_str,"%Y-%m").toordinal())
        m = fecha.strftime("%m")
        y = fecha.strftime("%Y")
        j = calendar.monthrange(int(y), int(m))[1]
        for d in range(1,j+1):            
            cls.totalizar_dia(date.fromordinal(datetime.strptime("{0}-{1}-{2}".format(y,m,d),"%Y-%m-%d").toordinal()))
            logger.info("{1}:Totalizando el dia{0}".format(date.fromordinal(datetime.strptime("{0}-{1}-{2}".format(y,m,d),"%Y-%m-%d").toordinal()),datetime.today()))
    
    @classmethod
    def totalizar_dia(cls,date):
        """Totaliza por vehiculo y por tipo de evento (maestro) los eventos de un dia y almacena el total basado en system_gmt
        
        @param date: objeto tipo date con el dia que se quiere sincronizar
        @attention: basado en el tiempo del campo system_gmt de los event_location
        
        borrador plan:
        A.consulta sql de la suma de todos los eventos del dia (para colombia) agrupados por vehiculo y por eventid
        se va a recorrer vehiculo por vehiculo:
         1. cargar todos los eventid del vehiculo registrados (TypeEventMobile)
         2.
         3. @todo trabajando
        
        """

    
    
class TotalMensualEventLocation(ModelSQL,ModelView):
    '''    Totales por mes por vehiculo de cada uno de los Master tipos de eventos para poder graficar. 
    
    Estos totales son estaticos pero se podran regenerar a partir de los totales_diarios
    @attention: aun en implementacion.
    '''
    
    __name__ = "datos_sonar.total_mensual_event_location"
    _rec_name = "fecha"
    
    fecha = fields.Date('fecha',
                        help="fecha de la totalizacion del evento, solo se usa el mes y el año",
                        required = True)
    
    mes = fields.Function(
        fields.Integer(
            'Mes',
            help='Campo Virtual del dia numerico del mes (ej: 4), esto para usar en graficos y reportes',
            readonly=True
        )
        ,'obtener_mes')
    ano = fields.Function(
        fields.Integer(
            'Ano',
            help='Campo Virtual del dia numerico del ano (ej: 2018), esto para usar en graficos y reportes',
            readonly=True
        )
        ,'obtener_ano')
    evento_maestro_id = fields.Many2One('datos_sonar.master_type_event_mobile',
                                    "Evento",
                                    help="Evento (evento maestro y no del vehiculo) que se totaliza",
                                    required=True,
                                    )
    mobil_id = fields.Many2One ('datos_sonar.mobiles',
                                'Vehiculo',
                                help="Vehiculo al que hace referencia este total",
                                required=True,
                                )
    #subtotales = feidls.One2Many('')
    
    total = fields.Integer('Total',
                        help='Total del evento para el vehiculo y en la fecha especificada',
                        required = True,
                        )
    
    @classmethod
    def __setup__(cls):
        super(TotalDiarioEventLocation, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.fecha,table.mobil_id,table.evento_maestro_id),
                'Solo debe haber un registro para una fecha, un mobil y un evento')
        ]
    

    def obtener_mes(self,field):
        fecha=self.fecha
        return (int(fecha.strftime("%m")))
    
    def obtener_ano(self,field):
        fecha=self.fecha
        return (int(fecha.strftime("%Y")))
    
    @classmethod
    def totalizar_mes_manual(cls,fecha_str):
        """Totaliza un mes pasando el mes en date, es para poder hacer pruebas
        
        @param fecha_str: date en formato %Y-%m 
        """
        logger.info("INICIA TOTALIZAR mes {0}".format(fecha_str))
        fecha = date.fromordinal(datetime.strptime(fecha_str,"%Y-%m").toordinal())
        m = fecha.strftime("%m")
        y = fecha.strftime("%Y")
        i,j = calendar.monthrange(int(y), int(m))
        del i
        for d in range(1,j+1):            
            cls.totalizar_dia(date.fromordinal(datetime.strptime("{0}-{1}-{2}".format(y,m,d),"%Y-%m-%d").toordinal()))
            logger.info("{1}:Totalizando el dia{0}".format(date.fromordinal(datetime.strptime("{0}-{1}-{2}".format(y,m,d),"%Y-%m-%d").toordinal()),datetime.today()))
    
    @classmethod
    def totalizar_mes(cls,fecha):
        """Totaliza por vehiculo y por tipo de evento (maestro) los eventos de un mes y almacena el total basado en la fecha del total_diario
        
        @param date: objeto tipo date con el dia que se quiere sincronizar solo se toma el mes y el ano
        @attention: basado en el tiempo del campo system_gmt de los event_location
        """
        logger.info("INICIA TOTALIZAR Mes {0}".format(date))
        TotalesDiarios = Pool().get('datos_sonar.total_diario_event_location')
        inicio_mes = fecha.replace(day=1)
        inicio_mes_siguiente = inicio_mes.replace
        
        totales_del_mes = TotalesDiarios.search_read(
            domain=[('fecha','>=',inicio_mes),('fecha','<',inicio_mes_siguiente)],
            )
        estadistica = defaultdict(list)
        list(map(lambda x:estadistica[x['evento_maestro_id']].append(x),totales_del_mes))
        
        
        values = []
        for mobil_id in estadistica:
            maestros_del_mobil = defaultdict(list)
            for registro in estadistica[mobil_id]:
                list(map(lambda x: maestros_del_mobil[x[0]].append(x[1]),registro.items()))
            list(map(
                lambda x: values.append({'fecha':date,'evento_maestro_id':x[0],'mobil_id':mobil_id,'total':len(x[1]),'registros':str(x[1])}),
                maestros_del_mobil.items()
                ))
        if cls.eliminar_dia_totalizado(date):
            list(map(cls.create_total_dia,values))

    @classmethod
    def create_total_dia(cls,total):
        values=[total]
        records = cls.create(values)
        cls.save(records)
    
    @classmethod
    def eliminar_dia_totalizado(cls,date):
        """Elimina los totales para un dia
        
        @param date: objeto tipo date
        """
        logger.info("Eliminando dias totalizados en la fecha{0}".format(date))
        totales = cls.search(domain = [('fecha',"=",date)])
        cls.delete(totales)
        return True

    @classmethod
    def total_vehiculo_maestro(cls,mobile_id,evento_maestro_id,fecha):
        cantidad=cls.search_count(domain=[('mobile_id','=',mobile_id),('evento_maestro_id','=',evento_maestro_id),('fecha','=',fecha)])
        return cantidad
    
class EventLocationFiltro(ModelView):
    'Filtra Reporte EventLocation'
    __name__ = 'datos_sonar.event_location.print_events.filtro'
    
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                help="Vehiculo al que hace referencia este total",
                                required=True,
                                )
    inicio = fields.DateTime('Fecha Inicio',
                        help="Desde cuando?",
                        required = True,
                        domain=[('inicio','<',Eval('fin'))],
                        depends=['fin']
                        )
    
    fin = fields.DateTime('fecha final',
                        help="Hasta cuando?",
                        required = True,
                        domain=[('fin','>',Eval('inicio'))],
                        depends=['inicio']
                        )
    descripcion = fields.Char("Descripcion del evento",
                            help="Solo implementado en CSV, Puede usar el comodin % ejemplo %erta% traera todos los eventos que la descripcion contenga erta"
                            )
    
class ExportEventLocation(Wizard):
    """PrintEventLocationReport wizard.
    
    """
    __name__ = 'datos_sonar.event_location.print_events'
    
    start = StateView('datos_sonar.event_location.print_events.filtro',
        'datos_sonar.print_events_filtro_form_view',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Expotar', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('datos_sonar.event_location')
    
    def do_print_(self,action):
        eventos = Pool().get('datos_sonar.event_location')
        clause = [
            ('mobil_id','in',list(map(lambda x: x.id,self.start.mobiles))),
            ]
        if self.start.inicio and self.start.fin:
            clause.append(('gps_GMT','>=',self.start.inicio))
            clause.append(('gps_GMT','<=',self.start.fin))
        evt = eventos.search(clause,order=[('gps_GMT','ASC'),('id','ASC')])
        return action, {
            'ids': [a.id for a in evt],
            }

    def transition_print_(self):
        return 'end'


class ExportEventLocationCSV(Wizard):
    """PrintEventLocationReport wizard.
    
    """
    __name__ = 'datos_sonar.event_location.print_events_csv'
    
    start = StateView('datos_sonar.event_location.print_events.filtro',
        'datos_sonar.print_events_filtro_form_view',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Obtener Datos', 'obtener_datos', 'tryton-go-next', default=True),
            ])
    obtener_datos = StateTransition()
    mostrar = StateView('datos_sonar.event_location.export_csv.data',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Volver', 'start', 'tryton-undo'),
            ])
    
    
    def transition_obtener_datos(self):
        eventos = Pool().get('datos_sonar.event_location')
        mobiles = Pool().get('datos_sonar.mobiles')
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        table = eventos.__table__()
        table_mobiles = mobiles.__table__()
        campos = {
            'id':table.id,
            'regId':table.regId,
            'gps_GMT':ToChar(
                AtTimeZone(
                    AtTimeZone(table.gps_GMT,'UTC'),#sea asigna UTC
                    configuration.zona_horaria#se convierte a Zona Horaria local, de configuracion
                    ),
                'YYYY-MM-DD HH24:MI:SS' #se muestra sin zona horaria
            ),
            #'gps_GMT':table.gps_GMT,
            'system_gmt':ToChar(
                AtTimeZone(
                    AtTimeZone(table.system_gmt,'UTC'),
                    configuration.zona_horaria
                    ),
                'YYYY-MM-DD HH24:MI:SS'
            ),
            #'system_gmt':table.system_gmt,
            'vehiculo':table_mobiles.mDescription,
            'mId':table.mId,
            'eventID':table.eventID,
            'eventDescription':table.eventDescription,
            'latitude':table.latitude,
            'longitude':table.longitude,
            'speed':table.speed,
            'heading':table.heading,
            'address':table.address,
            'age':table.age}
        columns = list(campos.values())
        where1 = table.mobil_id.in_(list(map(lambda x: x.id,self.start.mobiles)))
        #inicio = self.start.inicio - timedelta(days=configuration.diferencia_utc)
        #fin = self.start.fin - timedelta(days=configuration.diferencia_utc)
        where2 = table.gps_GMT >= self.start.inicio
        where3 = table.gps_GMT <= self.start.fin
        if self.start.descripcion:
            where4 = table.eventDescription.like(self.start.descripcion)
        logger.info("INICIO BUSQUEDA EVENTOS EXPORT CSV")
        
        if self.start.descripcion:
            cursor.execute(*table.join(table_mobiles,condition=(table.mobil_id == table_mobiles.id)
            ).select(*columns,
                                    where=(where1 & where2 & where3 & where4),
                                    order_by=(table.gps_GMT,table.mobil_id)
                                    )
                        )
        else:
            cursor.execute(*table.join(table_mobiles,condition=(table.mobil_id == table_mobiles.id)
                ).select(*columns,
                                        where=(where1 & where2 & where3),
                                        order_by=(table.gps_GMT,table.mobil_id)
                                        )
                            )
        logger.info("FIN BUSQUEDA EVENTOS EXPORT CSV")
        datos=cursor.fetchall()
        logger.info("INICIO EXPORT DATA EVENTOS EXPORT CSV")
        datos = eventos.lista_to_csv_line([x for x in campos.keys()], datos) if datos else datos 
        Transaction().set_context({'ExportEventLocationCSV':datos})
        return 'mostrar'

    def transition_mostrar(self):
        return 'end'

class ExportEventLocationCSVData(ModelView):
    ' Sincronizar Mobiles Start (para wizard).'
    
    __name__ = 'datos_sonar.event_location.export_csv.data'
    
    nota = fields.Char("Nota",readonly=True) 
    archivo = fields.Binary('Archivo',filename="EventLocation.csv")
    
    @classmethod
    def default_archivo(cls):
        archivo = Transaction().context['ExportEventLocationCSV']
        Transaction().context['ExportEventLocationCSV'] = ""
        return archivo
    
    @classmethod
    def default_nota(cls):
        return "Recuerde limpiar el archivo luego de guardarlo para que sea mas rapido el cerrar esta ventana"


class StadisticLocationFiltro(ModelView):
    'Filtra La estadistica para EventLocation'
    __name__ = 'datos_sonar.event_location.stadistic.filtro'
    
    mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
                                help="Vehiculo al que hace referencia la estadistica",
                                required=True,
                                )
    init = fields.DateTime('Fecha Inicio',
                        help="Desde cuando?",
                        required = True,
                        domain=[('init','<',Eval('end'))],
                        depends=['end']
                        )
    
    end = fields.DateTime('fecha final',
                        help="Hasta cuando?",
                        required = True,
                        domain=[('end','>',Eval('init'))],
                        depends=['init']
                        )
    #master_event = fields.Selection('get_master_events_selection','Evento Maestro')
    descripcion = fields.Char("Descripcion del evento",
                            help="Sin implementar en CSV, Puede usar el comodin % ejemplo %erta% traera todos los eventos que la descripcion contenga erta"
                            )
    speed_init = fields.Integer('Velocidad inicial',
                            help='Si se indica se filtraran los eventos que speed > velocidad_inicial'
                            )
    speed_end = fields.Integer('Velocidad final',
                            help='Si se indica se filtraran los eventos que speed < velocidad_inicial'
                            )
    group_by_day = fields.Boolean('Agrupado por dia',
                                help='Indica si el conteo se realizará día por día o completo para el rango de fechas seleccionado'
                                )
    
    #@staticmethod
    #def get_master_events_selection():
    #    master_events = Pool().get('datos_sonar.master_type_event_mobile')
    #    master_events_names = [x['name'] for x in master_events.search_read([('id','>',0)],fields_names=['name'])] 
    #    return list(zip(master_events_names,master_events_names))
    
class StadisticLocationSelectType(ModelView):
    'Selecciona el tipo de estadistica para EventLocation'
    __name__ = 'datos_sonar.event_location.stadistic.select_type'
    
    nota = fields.Char("Nota",readonly=True)
    sql = fields.Binary('sql',filename='prueba.dat',states={'invisible':True})
    
    @classmethod
    def default_nota(cls):
        return "Que tipo de estadistica desea?"
    

class StadisticEventLocationCSV(ModelView):
    ' Sincronizar Mobiles Start (para wizard).'
    
    __name__ = 'datos_sonar.event_location.stadistic.csv'
    
    nota = fields.Char("Nota",readonly=True) 
    archivo = fields.Binary('Archivo',filename="EventLocation.csv")
    
    @classmethod
    def default_nota(cls):
        return "Recuerde limpiar el archivo luego de guardarlo para que sea mas rapido el cerrar esta ventana"

class StadisticEventLocation(Wizard):
    """PrintEventLocationReport wizard.
    
    """
    __name__ = 'datos_sonar.event_location.stadistic'
    _sql = ''#consulta sql que se construye con el filtro en el evento start
    start = StateView('datos_sonar.event_location.stadistic.filtro',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Estadistica', 'stadistic_sql', 'tryton-list'),
            ])
    stadistic_sql = StateTransition()
    select_type = StateView('datos_sonar.event_location.stadistic.select_type',
                        '',[
                            Button('Cancelar', 'end', 'tryton-cancel'),
                            Button('Csv Filas', 'setup_csv', 'tryton-list'),
                            Button('Csv Matriz', 'setup_csv_matriz', 'tryton-list'),
                            Button('Volver a empezar', 'start', 'tryton-undo'),
                            ])
    setup_csv = StateTransition()
    to_csv = StateView('datos_sonar.event_location.stadistic.csv',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Volver a empezar', 'start', 'tryton-undo'),
            ])
    setup_csv_matriz = StateTransition()
    to_csv_matriz =  StateView('datos_sonar.event_location.stadistic.csv',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Volver a empezar', 'start', 'tryton-undo'),
            ])
    
    def transition_stadistic_sql(self):
        #transaction = Transaction()
        #cursor = transaction.connection.cursor()
        event_location = Pool().get('datos_sonar.event_location')
        mobiles = Pool().get('datos_sonar.mobiles')
        t_mobiles = mobiles.__table__()
        t_event = event_location.__table__()
        
        join = t_event.join(t_mobiles)
        join.type_='LEFT'
        join.condition = join.right.id == t_event.mobil_id
        
        if self.start.group_by_day:
            select = join.select(
                t_mobiles.mDescription,
                t_event.eventDescription,
                t_event.gps_GMT.cast('date').as_('Fecha'),
                Count(t_event.id).as_('Cantidad'),
                )
            group_by = [t_mobiles.mDescription,t_event.eventDescription,t_event.gps_GMT.cast('date')]
            
        else:
            select = join.select(
                t_mobiles.mDescription,
                t_event.eventDescription,
                Count(t_event.id).as_('Cantidad'),
                Min(t_event.gps_GMT).as_('Fecha inicial'),
                Max(t_event.gps_GMT).as_('Fecha Final'),
                )
            group_by = [t_mobiles.mDescription,t_event.eventDescription]
            
        select.where = t_event.mobil_id.in_(list(map(lambda x: x.id,self.start.mobiles)))
        select.where &= t_event.gps_GMT >= self.start.init
        select.where &= t_event.gps_GMT <= self.start.end
        if self.start.descripcion:
            select.where &= t_event.eventDescription.like(self.start.descripcion)
        if self.start.speed_init:
            select.where &= t_event.speed > self.start.speed_init
        if self.start.speed_end:
            select.where &= t_event.speed < self.start.speed_end        
        select.group_by = group_by
        select.order_by = [t_event.eventDescription]
        logger.info("Select:{s}".format(s=str(select)))
        Transaction().set_context({'datos_sonar_event_location_stadistic_sql':pickle.dumps(select)})#@todo no sera mejor otra forma y no con pickle?
        return 'select_type'
    
    def default_select_type(self,fields):
        sql = Transaction().context['datos_sonar_event_location_stadistic_sql']
        return {'sql':sql}

    def transition_setup_csv(self):
        event_location = Pool().get('datos_sonar.event_location')
        select = pickle.loads(self.select_type.sql)
        #ejecutar consulta
        cursor = Transaction().connection.cursor()
        cursor.execute(*select)
        col_sql = [x.name for x in cursor.description]
        datos = cursor.fetchall()
        datos = event_location.lista_to_csv_line(col_sql,datos)
        Transaction().set_context({'datos_sonar_event_location_stadistic_csv':datos})
        return 'to_csv'
    
    def default_to_csv(self,fields):
        return{
            'archivo':Transaction().context['datos_sonar_event_location_stadistic_csv'],
            }
    
    def transition_setup_csv_matriz(self):
        event_location = Pool().get('datos_sonar.event_location')
        select = pickle.loads(self.select_type.sql)
        #ejecutar consulta
        cursor = Transaction().connection.cursor()
        cursor.execute(*select)
        col_sql = [x.name for x in cursor.description]
        col_sql = {col_sql[x]:x for x in range(0,len(col_sql))}#diccionario que contiene los nombres de los campos con el index de la posicion de las columnas
        data = cursor.fetchall()
        
        def join_description_fecha(description,fecha):
            return ' '.join([description,str(fecha)])
        
        if self.start.group_by_day:
            _events = {x[col_sql['eventDescription']]:None for x in data }
            total_days = (self.start.end - self.start.init).days
            init = date.fromordinal(self.start.init.toordinal())
            events = {}
            for days in range (0,total_days+1):
                events.update({join_description_fecha(event, init + timedelta(days=days)):0 for event in _events})#esqueleto para cada linea para mantener el orden de las columnas y por defecto 0
        else:
            events = {x[col_sql['eventDescription']]:0 for x in data }#esqueleto para cada linea para mantener el orden de las columnas y por defecto 0
                    
        mobiles = {x[col_sql['mDescription']]:events.copy() for x in data}
        
        for row in data:
            if self.start.group_by_day:
                mobiles[row[col_sql['mDescription']]][join_description_fecha(row[col_sql['eventDescription']], row[col_sql['Fecha']])]=row[col_sql['Cantidad']]
            else:
                mobiles[row[col_sql['mDescription']]][row[col_sql['eventDescription']]]=row[col_sql['Cantidad']]
        
        head = ['mDescription',*events]

        content = [[mobil,*list([*event.values()])] for mobil,event in mobiles.items() ]

        data_csv = event_location.lista_to_csv_line(head,content)
        Transaction().set_context({'datos_sonar_event_location_stadistic_csv_matriz':data_csv})
        return 'to_csv_matriz'
    
    def default_to_csv_matriz(self,fields):
        return{
            'archivo':Transaction().context['datos_sonar_event_location_stadistic_csv_matriz'],
            }


class SpeedMobileGraph(Report):
    """ Devuelve security.xml que contiene politicas de seguridad por defecto para un modulo.
    
    """
    
    __name__ = 'datos_sonar.event_location.speed_mobile_graph'

    @classmethod
    def execute(cls, ids, data):
        EventLocation = Pool().get('datos_sonar.event_location')
        Mobiles = Pool().get('datos_sonar.mobiles')
        ActionReport = Pool().get('ir.action.report')
        
        action_report_ids = ActionReport.search([
            ('report_name', '=', cls.__name__)
            ])
        if not action_report_ids:
            raise Exception('Error', 'Report (%s) not find!' % cls.__name__)
        #action_report = ActionReport(action_report_ids[0])
        report_date = data['date'] if 'date' in data.keys() else date.today()
        #ajustando hora a colombia @todo usar campo de configuracion
        reporte_date_init = datetime.fromordinal(report_date.toordinal()) - timedelta(hours=5)
        reporte_date_end = datetime.fromordinal(report_date.toordinal()) + timedelta(hours=19)
        
        mobile = Mobiles.browse([ids[0]])
        if mobile:
            mobile = mobile[0]
        else:
            Mobiles.raise_user_error('Sin mobil seleccionado')
        speeds_y, date_time_x = cls.get_data(mobile.id,reporte_date_init,reporte_date_end) 
#         #EventLocation.search_read([('mobil_id','=',mobile.id),
#                                             ('gps_GMT','<=',reporte_date_end),
#                                             ('gps_GMT','<=',reporte_date_init),
#                                             ],
#                                            fields_names=['speed','gps_GMT'])
        #mobile.raise_user_error("error:{x},{y}".format(x=date_time_x,y=speeds_y))
        data = grafico_linea_unica_datetime(date_time_x,speeds_y,'png')
        file_name = "Velocidad_{date}_{mobil}".format(date=report_date,mobil=mobile.mDescription)
        return ('png', fields.Binary.cast(data), False, file_name)
    
    @classmethod
    def get_data(cls,mobil_id,datetime_init,datetime_end):
        cursor = Transaction().connection.cursor()
        sql = """SELECT speed,"gps_GMT"
        FROM datos_sonar_event_location
        where
         mobil_id={mobil_id} and "gps_GMT" >= '{date_init:%Y-%m-%d %H:%M:%S}'
        and
           "gps_GMT" <= '{date_end:%Y-%m-%d %H:%M:%S}'
        and
            speed IS NOT NULL
        order by "gps_GMT"
           ;
        """.format(mobil_id=mobil_id,
           date_init=datetime_init,
           date_end=datetime_end)
        cursor.execute(sql)
        rows = cursor.fetchall()
        logger.error("rows:{r}".format(r=mobil_id))
        logger.error("rows:{r}".format(r=datetime_end))
        logger.error("rows:{r}".format(r=datetime_init))
        logger.error("rows:{r}".format(r=sql))
        logger.error("rows:{r}".format(r=rows))
        y = [y[0] for y in rows ]
        x = [x[1] for x in rows ]
        return y,x

class SpeedMobile(Wizard):
    """Wizard to make csv stadistic from Passengers."""
    
    __name__ = 'datos_sonar.event_location.speed_mobile'
    
    start = StateView('datos_sonar.event_location.date_filter',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Png', 'png', 'tryton-ok'),
            ])
    png = StateAction('datos_sonar.report_speed_mobile_graph')
    
    def do_png(self, action):
        return action, {
            'id':Transaction().context['active_id'],
            'ids':[Transaction().context['active_id']],
            'date':self.start.date
            }

class EventLocationDateFilter(ModelView):
    """filtro mensual"""
    
    __name__ = 'datos_sonar.event_location.date_filter'
    
    year = fields.Selection('select_year','Año',sort=False)
    month = fields.Selection('select_month','Mes',sort=False)
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    init_date = fields.Function(fields.DateTime('Fecha inicial',
                                                depends=['year','month'],
                                                states={'invisible':True},
                                                ),'on_change_with_init_date',)
    end_date = fields.Function(fields.DateTime('Fecha final',
                                                depends=['year','month'],
                                                states={'invisible':True},
                                                ),'on_change_with_end_date',)
    
    @staticmethod
    def select_year():
        init_year = 2018
        current_year = date.today().year
        return [(str(x),str(x))for x in range(init_year,current_year + 1)]
    
    @staticmethod
    def select_month():
        return [(str(x),str(x))for x in range(1,12 + 1)]
    
    @staticmethod
    def default_year():
        return date.today().year
    
    @staticmethod
    def default_month():
        return date.today().month
    
    @fields.depends('year','month','init_date')
    def on_change_with_init_date(self):
        if self.year and self.month:
            return datetime(year=int(self.year),
                            month=int(self.month),
                            day=1) + timedelta(hours=5)
    
    @fields.depends('year','month','end_date')
    def on_change_with_end_date(self):
        if self.year and self.month:
            last_day = calendar.monthrange( int(self.year),int(self.month))[1]
            return datetime(year=int(self.year),
                            month=int(self.month),
                            day=last_day,
                            hour=23,
                            minute=59,
                            second=59) + timedelta(hours=5)
