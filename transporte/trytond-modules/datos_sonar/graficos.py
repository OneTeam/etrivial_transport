# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from io import BytesIO
from datetime import datetime, date, timedelta

try:
    import matplotlib
    matplotlib.use('Agg')
    from matplotlib import dates
    import matplotlib.pyplot as plt
    import numpy as np
    
except ModuleNotFoundError:
    raise Exception('Error','Modules required: (%s)' % "matplotlib,numpy")

__all__ = ['grafico_archivo_columnas_apiladas','multigrafico_archivo_columnas_apiladas',
           'grafico_archivo_columnas_multiples','grafico_linea_unica_datetime']


def grafico_archivo_columnas_apiladas(columnas,valores,titulo='Titulo',formato='png',
                                      alto_inicial=8,ancho_inicial=8,cantidad_columnas_para_ancho=12,
                                      ancho_de_barras=0.35,transparencia=0.8,etiqueta_x='X',etiqueta_y='Y'):
    """Devuelve un archivo con un grafico en columnas apiladas.
    
    @param columnas: tupla con los nombres de las columnas que se mostrarán,
     estos conforma el eje x.
    @param valores: lista de tuplas que contienen dos tuplas: ('grupo',tupla valores)
     ej: [('Dorado',(43,37,26),('Metro',12,0,1)]. El grupo es el que aparecera
     en la leyenda del grafico.
    @param titulo: str titulo del grafico
    @param formato: 'png' | 'svg' | 'pdf'
    
    @attention: len(columnas_x) == len(valores[0][1])
    """
    
    file = BytesIO()
    fig_width = ancho_inicial #ancho por defecto
    fig_height = alto_inicial #alto por defecto
    quantity_good_cols = cantidad_columnas_para_ancho
    n_groups = len(columnas)
    index = np.arange(1,n_groups+1)
    bar_width = ancho_de_barras
    opacity = transparencia
    
    if n_groups > quantity_good_cols:
        scalar = (n_groups ) / quantity_good_cols
        fig_width *= int(scalar)
    fig, ax = plt.subplots(figsize=(fig_width,fig_height))
    ax.grid(True,'major','y')
    ax.grid(True,'minor','y',linestyle=':')
    ax.set_axisbelow(True)
    ax.minorticks_on()

    bars = []
    acumulated_value = tuple(0 for x in range(0,n_groups))
    for label,values in valores:
        bars.append(plt.bar(index,values,bar_width,alpha=opacity,
                label=label,bottom=acumulated_value))
        acumulated_value = tuple(map(lambda x,y:x+y,acumulated_value,values))
        
    plt.xlabel(etiqueta_x)
    plt.ylabel(etiqueta_y)
    plt.title(titulo)
    plt.xticks(index , columnas)
    plt.legend()
    plt.tight_layout()
    plt.savefig(file,format=formato)
    plt.close()
    return file.getvalue()

def multigrafico_archivo_columnas_apiladas(columnas,valores,titulo='Titulo',formato='png',
                                      alto_inicial=8,ancho_inicial=8,cantidad_columnas_para_ancho=12,
                                      ancho_de_barras=0.35,transparencia=0.8,etiqueta_x='X',etiqueta_y='Y'):
    """Devuelve un archivo con un grafico con multiples graficos en columnas apiladas.
    
    @param columnas: tupla con los nombres de las columnas que se mostrarán,
     estos conforma el eje x.
    @param valores: diccionarion con lista de tuplas que contienen dos tuplas: ('grupo',tupla valores)
     cada elemento del diccionario sera un subgrafico y por lo tanto contiene una lista de tuplas con los datos
     ej: [('Dorado',(43,37,26),('Metro',12,0,1)]. El grupo es el que aparecera
     en la leyenda del grafico.
    @param titulo: str titulo del grafico
    @param formato: 'png' | 'svg' | 'pdf'
    
    @attention: len(columnas_x) == len(valores[0][1])
    """
    
    file = BytesIO()
    fig_width = ancho_inicial #ancho por defecto
    fig_height = alto_inicial * len(valores) #alto por defecto
    quantity_good_cols = cantidad_columnas_para_ancho
    n_groups = len(columnas)
    index = np.arange(1,n_groups+1)
    bar_width = ancho_de_barras
    opacity = transparencia
    cantidad_de_graficos = len(valores)
    
    if n_groups > quantity_good_cols:
        scalar = (n_groups ) / quantity_good_cols
        fig_width *= int(scalar)
    subgrap = 0
#     share_x = True
    
#     fig = {}
#     ax = {}
    fig,axes = plt.subplots(cantidad_de_graficos,1,figsize=(fig_width,fig_height),sharex=False,sharey=True)
    # Set the ticks and ticklabels for all axes
    plt.setp(axes, xticks=index, xticklabels=columnas)
    for key, value in valores.items():
        axes[subgrap].grid(True,'major','y')
        axes[subgrap].grid(True,'minor','y',linestyle=':')
#         axes[subgrap].set_axisbelow(True)
#         axes[subgrap].minorticks_on()
        acumulated_value = tuple(0 for x in range(0,n_groups))
        for label,values in value:
            axes[subgrap].bar(index,values,bar_width,alpha=opacity,
                    label=label,bottom=acumulated_value)
            acumulated_value = tuple(map(lambda x,y:x+y,acumulated_value,values))
        axes[subgrap].set_ylabel(key + "\n " + etiqueta_y)
        axes[subgrap].set_xlabel(etiqueta_x)
        axes[subgrap].set_title(titulo)
        axes[subgrap].set_xticklabels(columnas)
        axes[subgrap].legend()
        subgrap += 1
        
    plt.tight_layout()
    plt.savefig(file,format=formato)
    plt.close()
    return file.getvalue()

def grafico_archivo_columnas_multiples(columnas,valores,titulo='Titulo',formato='png',
                                      alto_inicial=8,ancho_inicial=8,cantidad_columnas_para_ancho=2,
                                      ancho_de_barras=0.35,transparencia=0.8,etiqueta_x='X',etiqueta_y='Y'):
    """Devuelve un archivo con un grafico en columnas apiladas.
    
    @param columnas: tupla con los nombres de las columnas que se mostrarán,
     estos conforma el eje x.
    @param valores: lista de tuplas que contienen dos tuplas: ('grupo',tupla valores)
     ej: [('Dorado',(43,37,26),('Metro',12,0,1)]. El grupo es el que aparecera
     en la leyenda del grafico.
    @param titulo: str titulo del grafico
    @param formato: 'png' | 'svg' | 'pdf'
    
    @attention: len(columnas_x) == len(valores[0][1])
    """
    
    file = BytesIO()
    fig_width = ancho_inicial #ancho por defecto
    fig_height = alto_inicial #alto por defecto
    quantity_good_cols = cantidad_columnas_para_ancho
    n_groups = len(columnas*len(valores))
    index = np.arange(len(valores),n_groups)
    bar_width = ancho_de_barras
    opacity = transparencia
    
    if n_groups > quantity_good_cols:
        scalar = (n_groups ) / quantity_good_cols
        fig_width *= int(scalar)
    fig, ax = plt.subplots(figsize=(fig_width,fig_height))
    ax.grid(True,'major','y')
    ax.grid(True,'minor','y',linestyle=':')
    ax.set_axisbelow(True)
    ax.minorticks_on()

    bars = []
    index_add = 0
    for label,values in valores:
        bars.append(plt.bar(index+index_add,values,bar_width,alpha=opacity,label=label))
        index_add += bar_width
        
    plt.xlabel(etiqueta_x)
    plt.ylabel(etiqueta_y)
    plt.title('cambia')
    plt.xticks(index , columnas,rotation='vertical')
    plt.legend()
    plt.tight_layout()
    plt.savefig(file,format=formato)
    plt.close()
    return file.getvalue()

def grafico_linea_unica_datetime(x,y,formato,ancho_hora=0.5,alto_inicial=8):
    """based on https://stackoverflow.com/questions/31700348/good-date-format-on-x-axis-matplotlib.
    
    """
    fig = plt.figure()
    ax = fig.add_axes([0.1,0.2,0.85,0.75])
    init_datetime = x[0]
    end_datetime = x[-1]
    xmin = datetime.fromordinal(date.fromordinal(init_datetime.toordinal()).toordinal())
    xmax = datetime.fromordinal(date.fromordinal(end_datetime.toordinal()).toordinal()) + timedelta(hours=24)
    hours = (xmax - xmin).total_seconds() / 3600
    fig_width = int(hours * ancho_hora)
    fig.set_size_inches(fig_width, alto_inicial)
    xtk_loc = [init_datetime + timedelta(hours=i) for i in np.arange(0,hours + 0.1,0.5) ]
    ax.set_xticks(xtk_loc)
    ax.tick_params(axis='both',direction='out',top='off',right='off')
    hfmt = dates.DateFormatter('%H:%M')
    ax.xaxis.set_major_formatter(hfmt)
    fig.autofmt_xdate(rotation=90,ha='center')
    ax.set_ylabel('Velocidad (km/h)',labelpad=10,fontsize=14)
    ax.set_xlabel('Hora',labelpad=20,fontsize=14)
    
    ax.plot(x, y, color='#004c99', label='Velocidad (km/h)')
    ax.fill_between(x,y,0,facecolor='#00a1e4', alpha=0.5, lw=0.5)
    ax.axis(xmin=init_datetime,xmax=end_datetime)
    file = BytesIO()
    plt.savefig(file,format=formato)
    plt.close()
    return file.getvalue()
#     fig_height = alto_inicial #alto por defecto
#     init_datetime = x[0]
#     end_datetime = x[-1]
#     xmin = datetime.fromordinal(date.fromordinal(init_datetime.toordinal()).toordinal())
#     xmax = datetime.fromordinal(date.fromordinal(end_datetime.toordinal()).toordinal()) + timedelta(hours=24)
#     hours = (xmax - xmin).total_seconds() / 3600
#     fig_width = int(hours * ancho_hora) 
#     plt.xlim(xmin, xmax)
#     #el ancho de la figura es de 1 pulgada por cada 
#     fig, ax = plt.subplots(figsize=(fig_width,fig_height))
#     file = BytesIO()
#     ax.plot(x,y)
#     plt.fill(x, y, '#00a1e4', alpha=0.8)
#     fig.autofmt_xdate()
#     plt.savefig(file,format=formato)
#     plt.close()
#     return file.getvalue()

def grafico_histo_unica_datetime(x,y,formato,ancho_hora=0.5,alto_inicial=8):
    fig_height = alto_inicial #alto por defecto
    if x and y:
        x = sorted(x)
        init_datetime = x[0]
        end_datetime = x[-1]
        xmin = datetime.fromordinal(date.fromordinal(init_datetime.toordinal()).toordinal())
        xmax = datetime.fromordinal(date.fromordinal(end_datetime.toordinal()).toordinal()) + timedelta(hours=24)
        hours = (xmax - xmin).total_seconds() / 3600
        fig_width = int(hours * ancho_hora) 
        plt.xlim(xmin, xmax)
        #el ancho de la figura es de 1 pulgada por cada 
        fig, ax = plt.subplots(figsize=(fig_width,fig_height))
        file = BytesIO()
        ax.bar(x,y)
        fig.autofmt_xdate()
        plt.savefig(file,format=formato)
        plt.close()
        return file.getvalue()
