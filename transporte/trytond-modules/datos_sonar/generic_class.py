# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import Model, ModelSQL, ModelView, fields, DictSchemaMixin, Unique
from trytond.pool import Pool
from datetime import datetime, date, timedelta
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval
from trytond.transaction import Transaction
import logging

logger = logging.getLogger(__name__)

__all__ = ['GenericStartView','WsdlConection']
_sonar_status_ok = 'OK'

class GenericStartView(ModelView):
    """ Clase vista Generica para usar en Wizards que envian una advertencia al inicio."""
    
    __name__ = 'datos_sonar.generic_class.generic_start_view'
    
    attention = fields.Char('Nota',readonly=True)
    
class WsdlConection(Model):
    """ Clase Generica para llamar metodos relacionados con la Api de Sonar.
    
    @todo: Oganizar si debe ser del tipo Model o MixIn.
    """
    __name__ = 'datos_sonar.generic_class.wsdlconection'
    
    @classmethod
    def GetWsdl(cls,request,name_object_content_response,params={}):
        """Realiza la solicitud (request) a la API de Sonar.
        
        Al ejecutar la solicitud verifica el exito de la consutla (status == ok) y que el objeto (name_object_content_response)
        contenga información.
        Devuelve el contenido de la respuesta (object_content_response) en caso de contener informacion o False en cualquier otro caso.
        El usuario, contraseña y url de la Api de Sonar son tomados de la configuracion.
        @param params: Diccionario que contiene el nombre del parametro como key y el valor como value los key usados son:
            'mId', 'UTC_datetime_init', 'UTC_datetime_end','eventID','FleetId':None,'CounterID':None,'Itinerary'
            Los demas keys se ignorarán
        @todo configurar para que todas las consultas a la api wsdl de Sonar usen este metodo.
        """
        request_param = {
            'GET_MobileGroups':['FleetId'],
            'GET_ItinerariesHistory':['mId','UTC_datetime_init','UTC_datetime_end','Itinerary'],
            'GET_Itineraries':[],
            'GET_GeofenceWithPoints':['GroupId'],
            'GET_GroupGeofences':['GroupId'],
            'GET_CounterHistory':['mId','CounterID','UTC_datetime_init','UTC_datetime_end'],
            'GET_PassengersCounter':['mId','UTC_datetime_init','UTC_datetime_end'],
            'GET_EventsHistory':['mId','eventID','UTC_datetime_init','UTC_datetime_end'],
            'GET_Drivers_v2':['FleetId','DrvId','DrvDoc'],
            'GET_DriversHistory' : ['mId','UTC_datetime_init','UTC_datetime_end'],
            }
        #verificar que los parametros obligatorios (en request_param) se hallan pasado
        true_params = False if len([False for x in request_param[request] if x not in params.keys()]) > 0 else True
        if not true_params:
            logger.error("Error en la solicitud {request} parametros requeridos:{request_param},parametros ingresados {params}. Faltan parametros".format(
                request=request,params=params,request_param=request_param[request]))
            return False
        
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        wsdl_url = configuration.wsdl_url
        params['User']= configuration.wsdl_user
        params['Password'] = configuration.wsdl_password

        try:
                from zeep import Client #seria ideal hacer un paquete aparte para tener las funciones especificas del Soap de Sonar extrayendo todos los comandos zeep
        except ModuleNotFoundError:
            logger.eror("Error: Module zeep NotFound")
            return "Error: Module zeep NotFound"
        client = Client(wsdl_url)
        Request = getattr(client.service, request)
        response = Request(**params)
        if response.status == 'OK':
            if hasattr(response,name_object_content_response):
                return getattr(response, name_object_content_response)
            else:
                logger.info("Sin datos en la Solicitud {request} con los parametros:{params}: {description}".format(request=request,params=params,description=response.description))
                return None
        else:
            logger.error('error en la conexion WSDL %s',response.description)
            logger.error('parametros:{p}'.format(p=params))
            return None
    
    @classmethod
    def connect_wsdl(cls):
        "Reliza la conexion con el webservice de Sonar y devuelve dicha coneccion"
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        wsdl_url = configuration.wsdl_url
        try:
                from zeep import Client #seria ideal hacer un paquete aparte para tener las funciones especificas del Soap de Sonar extrayendo todos los comandos zeep
        except ModuleNotFoundError:
            logger.eror("Error: Module zeep NotFound")
            raise Exception("Error: Module zeep NotFound")
        client = Client(wsdl_url)
        return client
    
    @classmethod
    def get_login(cls,id_sonar_user=None):
        """Devuelve un diccionario con el User y Password para la conexión de sonar
        
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        params = {'User':'','Password':''}
        if not id_sonar_user:
            params['User']= configuration.wsdl_user
            params['Password'] = configuration.wsdl_password
        #todo implementar para diferentes usuarios
        return params
    
    @classmethod
    def request(cls,client,request,params,id_sonar_user=None):
        params.update(cls.get_login(id_sonar_user))
        Request = getattr(client.service, request)
        response = Request(**params)
        return response
    
    @classmethod
    def SET_ItAssign_v2(cls,mId,Itinerary,DrvId,UTC_datetime=None,id_sonar_user=None,log=False):
        """Asigna un itinerario a un móvil y a un conductor
        
        UTC_datetime se da en el formato yyyy-mm-dd hh:mm:ss,
        si se deja en blanco toma la hora actual, pero se debe pasar asi sea en blanco
        """
        params = {'mId':mId,
                      'Itinerary':Itinerary,
                      'DrvId':DrvId,
                      'UTC_datetime': UTC_datetime or ''
                      }
        
        client = cls.connect_wsdl()
        response = cls.request(client,'SET_ItAssign_v2', params,id_sonar_user)
        if response.status == 'OK':
            if log:
                logger.info("response SET_ItAssign_v2 ({p}): {r}".format(p=params,r=response))
            return response.regId
        else:
            if log:
                logger.info("Error al asignar itinerario SET_ItAssign_v2 con los parametros {p}, el error fue: {e}".format(p=params,e=response.description))
            return None
        
    @classmethod
    def SET_ItCancel(cls,mId,regId,comments="",id_sonar_user=None,log=False):
        """cancela un itinerario de un vehiculo
        
        Devuelve True|False 
        """
        params={
            'mId':mId,
            'regId':regId,
            'comments':comments,
            }
        client = cls.connect_wsdl()
        response = cls.request(client,'SET_ItCancel',params,id_sonar_user)
        if response.status == _sonar_status_ok:
            if log:
                logger.info("response SET_ItCancel ({p}): {r}".format(p=params,r=response))
            return True
        else:
            if log:
                logger.info("Error response SET_ItCancel ({p}): {r}".format(p=params,r=response))
            return False
