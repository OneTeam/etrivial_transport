# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging

from trytond.modules.datos_sonar.sonar import SonarMixin, SonarAuthMixin

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.wizard.wizard import Wizard, Button, StateView, StateTransition,\
    StateAction
from trytond.transaction import Transaction
from trytond import backend


logger = logging.getLogger(__name__)

__all__ = ['Mobile','TypeEventMobile',
        'MasterTypeEventMobile','MobileSelectRevision','ShowMobilehistory',
        'SincronizarMobiles','SincronizarMobilesStart','SincronizarTypeEventStart',
        'SincronizarTypeEvent','TypeEventMobileName','SincronizarTypeEventName',
        'SincronizarTypeEventNameStart','ConstruirEventosMaestros',
        'ConstruirEventosMaestrosStart','VincularEventosMaestreosStart',
        'VincularEventosMaestros','SyncAllMobileGroup',
        ]

class Mobile(SonarAuthMixin,SonarMixin,ModelSQL,ModelView):
    """Informacion extraida usando la funcion GET_MobileList de la API.
    
    """
    __name__ = "datos_sonar.mobiles"
    _rec_name = "mDescription"

    mId = fields.Char("mId-id_interno")
    mIdFleet = fields.Integer("mIdFleet:ruta")#relacion a flota
    mDescription = fields.Char("mDescription:vehiculo")#debe ser unico (usar Constraint) porque la importacion de datos usa rec_name para relacion de campos
    mMake = fields.Char("mMake:desconocido")
    mPlaca = fields.Char("mPlaca",size=6) #mirar como poder revisar con nexpresion regular [a-zA-z]{3}[0-9]{3}
    mDevice = fields.Char("mDevice:Dispositivo")
    mDevice_version = fields.Char("mDevice_version:version del dispositivo")#"0.041"
    mImei = fields.Char("mImei",size=15)
    mPhoneNumber = fields.Char("mPhoneNumber",size=10)#"3117471767"
    mDriverId = fields.BigInteger("mDriverId:idConductor")
    mProfile = fields.Integer("mProfile:no se")
    mActivo = fields.Char("mActivo",size=1)#sera activo del todo o solo cuando no tiene conexion?
    mContactName = fields.Char("mContactName")
    mContactNit = fields.Char("mContactNit")
    mContactAddress = fields.Char("mContactAddress")
    mContactEmail = fields.Char("mContactEmail")
    mContactPhoneNumber = fields.Char("mContactPhoneNumber")
    mComments = fields.Text("mComments")
    mAddons = fields.One2Many("datos_sonar.mobileaddons","mobil_id","Addons")
    event_locations = fields.One2Many('datos_sonar.event_location','mobil_id',"EventLocations")
    passengers_logs = fields.One2Many('datos_sonar.passengers_log','mobil_id',"passengers_log")
    counter_logs = fields.One2Many('datos_sonar.counter_log','mobil_id',"counter_log")
    sync_tasks = fields.One2Many('datos_sonar.sync_task','mobile_id','Tareas de Sincronizacion')
    type_events = fields.One2Many('datos_sonar.type_event_mobile','mobil_id','Tipos de Eventos')
    #@todo debe ser One2Many un vehiculo pertenece a varios grupos en Sonar
    #aunque en la realidad no aplica por que los vehículos no aceptan las geocercas
    group = fields.Many2One('datos_sonar.mobile_group','Grupo')
    total_tasks = fields.Function(fields.Integer('Tareas'),'total_tareas')
    total_tasks_done = fields.Function(fields.Integer('Tareas ejecutadas'),'total_tareas_done')
    total_tasks_nodone = fields.Function(fields.Integer('Tareas no ejecutadas'),'total_tareas_nodone')
    
    @classmethod
    def __setup__(cls):
        super(Mobile, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_mPlaca', Unique(table, table.mPlaca),
                'la mPlaca debe ser unica'),
        ]
        #Activar history para este modelo
        cls._history = True
    
    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)

        super(Mobile, cls).__register__(module_name)

        # Migration from before agosto/27/2018: drop unique constraints mId an mDescription
        table.drop_constraint('unique_key')

    def total_tareas(self,field):
        Model = Pool().get('datos_sonar.sync_task')
        total_tareas = Model.search_count(
            (['mobile_id','=',self.id])
            )
        return (total_tareas)
    
    def total_tareas_done(self,field):
        Model = Pool().get('datos_sonar.sync_task')
        total_tareas = Model.search_count(domain=[('mobile_id','=',self.id),('done','=',True),])
        return (total_tareas)
    
    def total_tareas_nodone(self,field):
        Model = Pool().get('datos_sonar.sync_task')
        total_tareas = Model.search_count(domain=[('mobile_id','=',self.id),('done','=',False)],)
        return (total_tareas)
    
    
    @classmethod
    def sincronizarMobilesWsdl(cls):
        logger.info("Inicio sincronizacion de Mobiles")
        mobiles_sonar = cls.consultar_mobiles()
        if mobiles_sonar:
            for auth,mobiles in mobiles_sonar.items(): 
                for Mobile in mobiles.Mobile:
                    if not Mobile.mPlaca:
                        logger.info("REVISAR VEHICULO SIN PLACA: {} ".format(Mobile.mId))
                        continue
                    existente=cls.search(domain = [("mPlaca","=",Mobile.mPlaca)])
                    if existente :
                        cls.updateMobile(existente,Mobile,auth)
                    else:
                        cls.createMobile(Mobile,auth)
        else:
            logger.error('error en la conexion WSDL %s')
        logger.info("Fin sincronizacion de Mobiles")
    
    @classmethod
    def consultar_mobiles(cls):
        """Consulta en la API Sonar la información de los vehiculos
        
        Devuelve un diccionario donde la key es el auth.name y los
         valores el listado de mobiles de ese auth.
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        return cls.request_object_multi_auth(auth_field = configuration.wsdl_auth,
                                         target='mList',
                                         request='GET_MobileList',
                                         params={
                                              'FleetId':''
                                              },
                                         url=configuration.wsdl_url,
                                         log_=False)
    
    @classmethod
    def createMobile(self,Mobile,auth): 
        records = self.create([self.mobileToDict(Mobile,auth)])
        self.save(records)
    
    @classmethod
    def updateMobile(self,record,Mobile,auth):
        valores_sonar =self.mobileToDict(Mobile,auth)
        field_names = [x for x in valores_sonar.keys() ]
        valores_tryton={ }
        for field in field_names:
            valores_tryton[field] = getattr(record[0], field)
        valores_tryton['mAddons'] = None #si los addons cambia no influye en actualizar o no el campo
        if valores_sonar != valores_tryton:
            self.write(record,valores_sonar)
            self.save(record)
    
    @classmethod
    def mobileToDict(self,Mobile,auth):
        prototype = {
            "mId" : Mobile.mId,
            "mIdFleet" : Mobile.mIdFleet,
            "mDescription" : Mobile.mDescription,
            "mMake" : Mobile.mMake,
            "mPlaca" : Mobile.mPlaca,
            "mDevice" : Mobile.mDevice,
            "mDevice_version" : Mobile.mDevice_version,
            "mImei" : Mobile.mImei,
            "mPhoneNumber" : Mobile.mPhoneNumber,
            "mDriverId" : Mobile.mDriverId,
            "mProfile" : Mobile.mProfile,
            "mActivo" : Mobile.mActivo,
            "mContactName" : Mobile.mContactName,
            "mContactNit" : Mobile.mContactNit,
            "mContactAddress" : Mobile.mContactAddress,
            "mContactEmail" : Mobile.mContactEmail,
            "mContactPhoneNumber" : Mobile.mContactPhoneNumber,
            "mComments" : Mobile.mComments,
            "mAddons" : None, #por el momento quito los addons
            "sonar_auth":auth
            }
        return prototype

    
    @classmethod
    def idMobile_From_mId(self,mId):
        """
        @revisar se supone que el mId debe ser unico y por tanto al hacer una consulta normal deberia devolver solo 1
        Devuelve el id (de tryton) para un mId dado
        """
        record =self.search_read(domain=["mId","=",mId],fields_names=['id'])
        ide = record[0]
        return (ide['id'])
    
    @classmethod
    def mDescriptionFromMid(cls,mId):

        record = cls .search_read(domain=["mId","=",mId],fields_names=['mDescription'])
        mDescritpion = record[0]
        return mDescritpion['mDescription']
    
    @classmethod
    def mIdMobileFromId(cls,Id):
        """    Devuelve el mId para un id(de tryton) dado."""
        record = cls.search_read(domain=[('id','=',Id)],fields_names=['mId'])
        mId = record[0]['mId']
        return (mId)
    
    @classmethod
    def dic_itineary(cls,mobil_id):
        """Devuelve un diccionario con los itineraries(ids) del vehiculo (en realidad del grupo del vehiculo).
        
        keys = itinerary_id, values = It_id
        """
        record = cls.search([('id','=',mobil_id)])
        return {x.id:x.It_id for x in record[0].group.itineraries}
    
    @classmethod
    def allIds(cls):
        """Devuelve el identificador de todos los vehiculos (activos e inactivos)
        
        @attention: usado por taskManager en modulo sync_task
        @return: lista con los ids
        """
        records = cls.search_read(domain=[('id','>',0)],fields_names=['id'])
        ids=[]
        for record in records:
            ids.append(record['id'])
        return ids
    
    @classmethod
    def allmIds(cls):
        """Devuelve el mId (identificador de Sonar) de todos los vehiculos (activos e inactivos).
        
        @attention: usado por taskManager en modulo sync_task
        @return: lista con los mids
        """
        records = cls.search_read(domain=[('id','>',0)],fields_names=['mId'])
        mids=[]
        for record in records:
            mids.append(record['mId'])
        return mids
        
    @classmethod
    def allIdsMidDict(cls):
        """devuelve un diccionario con todos los id:mid y los mid:id.
        
        esto es para evitar hacer muchas consultas en la base de datos al momento de importacion
        grande de datos.
        """
        mobiles = {}
        records = cls.search_read(domain=[('id','>',0)],fields_names=['id','mId'])
        for record in records:
            mobiles[record['id']]=record['mId']
            mobiles[record['mId']]=record['id']
        return mobiles
    
    
    @classmethod
    def allIdsFrom_mDescription(cls):
        """Devuelve un Diccionario con mDescription como key y el id como valor para todos los vehiculos."""
        values = cls.search_read([('id','>',0)],fields_names=['id','mDescription'])
        return {x['mDescription']:x['id'] for x in values}
    
    @classmethod
    def all_id_mDescription_dict(cls):
        """Devuelve un diccionario keys = id, values = mDescription"""
        values = cls.search_read([('id','>',0)],fields_names=['id','mDescription'])
        return {x['id']:x['mDescription'] for x in values}
    
    @classmethod
    def all_id_mId_dict(cls):
        """Devuelve un diccionario keys = id, values = mId"""
        values = cls.search_read([('id','>',0)],fields_names=['id','mId'])
        return {x['id']:x['mId'] for x in values}
        
    @classmethod
    def sincronizar_mobiles_group(cls,log_=False):
        """ Sincroniza el grupo de todos los mobiles con la Api de SonarAvl.
        
        @attention: almacena solo un grupo, aunque la plataforma de Sonar
        acepta varios grupos pero al aplicarlos a los vehiculos sale errores
        con las geocercas.
        """
        groups = Pool().get('datos_sonar.mobile_group')
        groups_dict = groups.allIdsFrom_gId()
        mobiles_dict = cls.allIdsFrom_mDescription()
        
        if log_:
            logger.info("Inicio sincronizacion del Grupo para todos los mobiles")
        response_group_content = 'MobileGroup'
        attrib_mobiles = 'mList'
        attrib_id = 'gId'
        
        glists = groups.consultar_mobiles_group()
        if glists:
            values={}
            for gList in glists.values():
                for Group in gList[response_group_content]:
                    if Group[attrib_mobiles]:
                        for mobil in Group[attrib_mobiles]['Mobile']:
                            if mobil:
                                mobil_id = mobiles_dict[mobil['mDescription']]
                                values[mobil_id]=(mobil_id,groups_dict[Group[attrib_id]])
            
            records = cls.search_read(
                [('id','in',list(values.keys()))],
                fields_names=['id','group']
                )
            for record in records:
                cls.update_group(*values[record['id']])
            if log_:
                logger.info("Fin sincronizacion del Grupo para todos los mobiles")
            return True
        else:
            if log_:
                logger.info("Sin informacion\n"
                            "Fin sincronizacion del Grupo para todos los mobiles")
            return False    

    
    @classmethod
    def update_group(cls,mobil_id,group_id):
        """Actualiza el Grupo a un mobil"""
        record = cls.browse([mobil_id])
        if record:
            cls.write(record,{'group':group_id})
    
    @classmethod
    def It_id_from_mobil(cls,mobil_id):
        """Devuelve los itinerarios(It_id) del vehiculo"""
        record = cls.search([('id','=',mobil_id)])
        if record:
            return [x.It_id for x in record[0].group.itineraries]
        else:
            return list()
        
    @classmethod
    def It_id_from_mId(cls,mId):
        """Devuelve los itinerarios(It_id) del vehiculo"""
        record = cls.search([('mId','=',mId)])
        if record[0].group:
            return [x.It_id for x in record[0].group.itineraries]
        else:
            return list()
    
    @classmethod
    def get_ids_auth(cls,mobil_id='',mId=''):
        fields_names = ['id','mId','sonar_auth']
        return cls._get_mobile(mobil_id=mobil_id,mId=mId,fields_names=fields_names)
    

    @classmethod
    def _get_mobile(cls,mobil_id='',mId='',fields_names=[]):
        """Devuelve el record object de un mobil indicando
        su mobil_id o su mId, sino se indica devuelve
        todos los vehiculos.
        
        @param fields_names: si se indica solo consulta estos campos
           
        """
        if not(mobil_id or mId):
            if fields_names:
                mobiles = cls.search_read([('id','>',0)],fields_names=fields_names)
            else:
                mobiles = cls.search([('id','>',0)])
            return mobiles
        elif mId:
            if fields_names:
                mobile = cls.search_read([('mId','=',mId)],fields_names=fields_names)
            else:
                mobile = cls.search([('mId','=',mId)])
        elif mobil_id:
            if fields_names:
                mobile = cls.search_read([('id','=',mobil_id)],field_names=fields_names)
            else:
                mobile = cls.browse([mobil_id])
        if mobile:
            return mobile[0]
    
    @classmethod
    def delete_recent_duplicate_by_placa(cls):
        """elimina mobiles duplicados usando la placa como identificador y dejando el mas antiguo."""
        #@todo revisar si lo lento es por usar el record u otra causa.
        SyncTask = Pool().get('datos_sonar.sync_task')
        records = cls.search_read(['id','>',0],fields_names=['id','mPlaca','create_date','write_date'])
        mobiles = {}
        for record in records:
            time = record['write_date'] if record['write_date'] else record['create_date']
            if record['mPlaca'] in mobiles.keys():
                mobiles[record['mPlaca']].update({time:record['id']})
            else:
                mobiles[record['mPlaca']] = {time:record['id']}
        
        logger.info("mobiles: {}".format(mobiles))
        to_delete = []
        
        for placa, valores in mobiles.items():
            if not placa: #puede haber vehiculos sin placas
                logger.info("REVISAR HAY VEHICULOS SIN PLACAS")
                continue
            elif len(valores) > 1:
                keys_to_delete = sorted(valores.keys())[1:]
                for key in keys_to_delete:
                    to_delete.append(valores[key])
                    
        logger.info("Para eliminar: {}".format(to_delete))
        #las tareas de sincronizacion impide eliminar los mobiles
        SyncTask.delete(SyncTask.search([('mobile_id','in',to_delete)]))
        cls.delete(cls.browse(to_delete))
        return

        
class SincronizarMobilesStart(ModelView):
    ' Sincronizar Mobiles Start (para wizard).'
    
    __name__ = 'datos_sonar.mobiles.sincronizar.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea Sincronizar los vehiculos con SonarAVL?')
        
class SincronizarMobiles(Wizard):
    'Sincronizar Mobiles'
    __name__ = 'datos_sonar.mobiles.sincronizar'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar los vehiculos con SonarAVL?'}
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        Mobiles = Pool().get('datos_sonar.mobiles')
        Mobiles.sincronizarMobilesWsdl()
        return 'end'

class SyncAllMobileGroup(Wizard):
    """Wizard para la sincronizacion con Api Sonar El grupo de todos los vehiculos
    
    """
    __name__ = 'datos_sonar.mobiles.sincronizar_group'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar los Grupos de los vehiculos con SonarAVL? **Nota:** <b>Es<b/> recomendable sincronizar primero Los Grupos'}
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        Mobiles = Pool().get('datos_sonar.mobiles')
        Mobiles.sincronizar_mobiles_group()
        return 'end'


class TypeEventMobile(ModelSQL,ModelView):
    """
    Tipos de Eventos para un Mobil
    Cada vehiculo (Mobile) tiene unos tipos de eventos que varia en cada carro,
    pues en cada carro cada evento es configurable independiente desde la plataforma
    de SonarAvl; por esto para la construccion de estos objetos se requiere 2 etapas
    la una automatica que jala desde los EventLocation (en Tryton) los eventId para
    cad vehículo y la descripcion de este evento en Descripcion del evento. Se supone
    que los eventId:
     * Del 1-99 son eventos como velocidad, lugar, etc.
     * Del 100-199 son ingresos a geocercas
     * Del 200-299 son salidas de las gocercas
     Nota: al parecer el evento 101 y 201, 102 y 202 ,... hacen referencia a la 
           misma geocerca solo que uno de entrada y el otro de salida.
    La parte manual es que a traves de la API no se puede acceder a una informacion
    que contiene mas detalles de cada evento por ejemplo el evento de exeso de velocidad
    tiene un campo donde se almacena el limite de la velocidad (60 km/h)
    La idea es Almacenar esta información pero tanto la digitacion de estos
    campos como la actualizacion, por el momento, se debe hacer manual para
    cada vehiculo.      
    """
    __name__ = "datos_sonar.type_event_mobile"
    _rec_name = "name"
    
    eventID = fields.Integer("eventID")
    name = fields.Function(fields.Char("Nombre"),'obtener_nombre',)
    other_names = fields.One2Many('datos_sonar.type_event_mobile_name','type_event_mobile_id','Otros nombres',
                                order=[('ultima_vez_usado','DESC')],
                                help='Otros nombres que ha tenido este eventID para este vehiculo'
                                )
    eventDescription = fields.Char("Nombre alternativo")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    master_event = fields.Many2One("datos_sonar.master_type_event_mobile","Evento Maestro")
    inconsistent = fields.Boolean ("Inconsistente",
                                help="Indica si se ha detectado alguna inconsistencia como que halla otro nombre para este id de evento para el mismo vehiculo"
                                )
    
    def obtener_nombre(self,field=None):
        if self.other_names:
            return self.other_names[0].nombre
        else:
            return None
    
    @classmethod
    def __setup__(cls):
        super(TypeEventMobile, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_mobil_and_eventid', Unique(table, table.eventID,table.mobil_id),
                'datos_sonar.type_event_mobile: solo debe haber un registro para un eventID y mobile_id')
        ]
        
    @classmethod
    def sincronizar_basado_en_event_location(cls):
        """ Sincroniza los typo de eventos por vehiculo basado en los registros historicos de EventLocation
        @deprecated
        """
        EventLocations = Pool().get("datos_sonar.event_location")
        Mobile = Pool().get('datos_sonar.mobiles')
        MasterEvent = Pool().get('datos_sonar.master_type_event_mobile')
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        #=======================================================================
        #consulta sql que trae todos los typos de eventos por vehiculos basadoen la tabla event_location
        # SELECT
        #  DISTINCT ON (e."mobil_id", e."eventID")
        #  e."mobil_id", e."eventID", e."eventDescription",t.id AS type_id,m.id AS master_id
        # FROM
        #  datos_sonar_event_location AS e
        #  LEFT JOIN
        #   datos_sonar_type_event_mobile AS t
        #   ON t."mobil_id" = e."mobil_id" AND t."eventID" = e."eventID"
        #   lEFT JOIN
        #     datos_sonar_master_type_event_mobile AS m
        #     ON m.name=e."eventDescription"
        # ORDER BY
        #  e."mobil_id", e."eventID";
        #=======================================================================

        t_event = EventLocations.__table__()
        t_master = MasterEvent.__table__()
        t_type_event = cls.__table__()
        
        columnas = [
            t_event.mobil_id,
            t_event.eventID,
            t_event.eventDescription,
            t_type_event.id.as_("type_id"),
            t_master.id.as_("master_id"),
            ]
        distinct_on = [
            t_event.mobil_id,
            t_event.eventID,
            #t_event.eventDescription
            ]
        join1 = t_event.join(t_type_event)
        join1.type_='LEFT'
        join1.condition = (join1.right.mobil_id == t_event.mobil_id) & (join1.right.eventID == t_event.eventID)
        join2 = join1.join(t_master)
        join2.type_='LEFT'
        join2.condition = join2.right.name == t_event.eventDescription
        
        logger.info("INICIO CONSULTA TYPE EVENTS")
        cursor.execute(*join2.select(*columnas,distinct_on=distinct_on,order_by=(t_event.mobil_id,t_event.eventID)))
        logger.info("FIN CONSULTA TYPE EVENTS")
        col_sql = [x.name for x in cursor.description]
        col_sql = {col_sql[x]:x for x in range(0,len(col_sql))}#diccionario que contiene los nombres de los campos con el index de la posicion de las columnas
        #=======================================================================
        # Recorrer la consulta:
        # *si tiene type_id verificar el nombre en la base de datos si es igual dejarlo quieto si cambia cambiarlo y marcarlo como inconistente.
        # *si no tiene type_id crear (debe ponersele maestro_id si tiene y sino crearlo)
        # *si master_id esta vacio crearlo y asignarselo
        # *si master_id no esta vacio verificar que sea el mismo nombre, si el nombre es diferente dejarselo y marcar como inconsistente.        
        #=======================================================================
        for record in cursor:
            if record[col_sql['type_id']]:#si el type_event existe
                existente = cls.read([record[col_sql['type_id']]],fields_names=['name','master_event'])
                if record[col_sql['master_id']]:#si existe un master_event para este evento
                    if existente[0]['master_event'] != record[col_sql['master_id']]:#si no esta vinculado
                        cls.marcar_inconsistente(record[col_sql['type_id']])
                else:#si no existe un master_event
                    pass #hay una funcion que busca los que no tienen eventos maestros, los crea y los vincula
            else:
                event = {}
                event['eventID'] = record[col_sql['eventID']]
                event['eventDescription'] = record[col_sql['eventDescription']]
                event['mobil_id'] = record[col_sql['mobil_id']]
                if record[col_sql['master_id']]:
                    event['master_event'] = record[col_sql['master_id']]
                cls.create_type_event(event)
        logger.info("FIN PROCESAR LOS REGISTROS")

    @classmethod
    def marcar_inconsistente(cls,record_id):
        """ Marca como inconsistente el registro del id correspondiente."""
        record = cls.search(domain=[('id','=',record_id)])
        cls.write(record,{'inconsistent':True})
        cls.save(record)
        return True

    @classmethod
    def update_type_event(cls,record,evento):
        cls.write(record, evento)
        cls.save(record)
    
    @classmethod
    def create_type_event(cls,evento):
        records = cls.create([evento])
        cls.save(records)
    
    @classmethod
    def update_master_event(cls,record_id,master_event):
        record=cls.search(domain=[('id','=',record_id)])
        #values=cls.read([record_id])
        #values[0]['master_event']=master_event
        cls.write(record,{'master_event':master_event})
        cls.save(record)
        

    @classmethod
    def getMasterEvent(cls,eventID,mobil_id):
        """Devuelve el identificador del evento maestro para un eventID y un mobil_id determinado
        
        @param eventID: identificador del tipo de evento para el vehiculo
        @param mobil_id: identificador del mobil
        @return: int con el id del evento maestro
        """
        values = cls.search_read(domain=[('eventID','=',eventID),('mobil_id','=',mobil_id)],limit=1)
        if values:
            return (values[0]['master_event'])

class TypeEventMobileName(ModelSQL,ModelView):
    """Nombres de los TypeEventMobile.
    
    Los TypeEventMobile puede tener varios nombres a lo largo del tiempo,
    este modelo contedra dichos nombres.      
    """
    __name__ = "datos_sonar.type_event_mobile_name"
    _rec_name = "name"
    
    type_event_mobile_id = fields.Many2One("datos_sonar.type_event_mobile","TypeEvent",
                                        required=True,
                                        help="TypeEventMobile al que pertenece este nombre"
                                        )
    nombre = fields.Char("Nombre",
                        required=True,
                        help="Nombre",
                        )
    ultima_vez_usado = fields.DateTime("Ultima vez usado",
                                    help="Ultima vez que se uso este nombre, No se actualiza automaticamente",
                                    required=False
                                    )
    
    @classmethod
    def __setup__(cls):
        super(TypeEventMobileName, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_type_event_mobil_name_nombre_type_event', Unique(table, table.nombre,table.type_event_mobile_id),
                'datos_sonar.type_event_mobile_name: solo debe haber un registro para un nombre y type_event_mobile_id')
        ]
    @classmethod
    def actualizar_nombres(cls):
        """Actualiza (borra y crea) los nombres de los type_event_mobile_id basado en la tabla eventLocation.
        
        """
        logger.info("INICIO SINCRONIZAR NOMBRES TYPE EVENT")
        TypeEvent = Pool().get("datos_sonar.type_event_mobile")
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        #@todo usar el modulo python-sql para escribir la consulta
        sql="""
SELECT b.id,a."eventDescription",a.fecha,a.vehiculo_id FROM (
 SELECT
  a.mobil_id,a."eventID",a."eventDescription", max(a."gps_GMT") AS fecha, max(a.mobil_id) AS vehiculo_id 
 FROM
  datos_sonar_event_location AS a
 GROUP BY 
  a.mobil_id,a."eventID",a."eventDescription"
 ) AS a
LEFT JOIN
 datos_sonar_type_event_mobile AS b
 ON
 b."mobil_id" = a."mobil_id" AND b."eventID" = a."eventID"
ORDER BY
 b,id,a."eventDescription"
;"""
        logger.info("INICIO CONSULTA SINCRONIZAR NOMBRES TYPE EVENT")
        cursor.execute(sql)
        logger.info("FIN CONSULTA SINCRONIZAR NOMBRES TYPE EVENT")
        col_sql = [x.name for x in cursor.description]
        col_sql = {col_sql[x]:x for x in range(0,len(col_sql))}#diccionario que contiene los nombres de los campos con el index de la posicion de las columnas
        #eliminar todos los nombres existentes
        cls.delete(cls.search(domain = [('id',">",0)]))
        logger.info("INICIO PROCESAR REGISTROS SINCRONIZAR NOMBRES TYPE EVENT")
        values = []
        for record in cursor:
                value = {}
                value['nombre'] = record[col_sql['eventDescription']]
                value['ultima_vez_usado'] = record[col_sql['fecha']]
                if record[col_sql['id']]:#los reset de unidad no tiene type_event porque el id es null
                    value['type_event_mobile_id'] = record[col_sql['id']]
                else:
                    record=TypeEvent.search([('mobil_id','=',record[col_sql['vehiculo_id']]),('eventID','=',None)])
                    value['type_event_mobile_id'] = record[0].id
                values.append(value)
        logger.info("FIN PROCESAR REGISTROS SINCRONIZAR NOMBRES TYPE EVENT")
        logger.info("INICIO ALMACENAR REGISTROS SINCRONIZAR NOMBRES TYPE EVENT")
        records = cls.create(values)
        cls.save(records)
        logger.info("FIN ALMACENAR REGISTROS SINCRONIZAR NOMBRES TYPE EVENT")
        logger.info("FIN SINCRONIZAR NOMBRES TYPE EVENT")
    
class SincronizarTypeEventStart(ModelView):
    ' Sincronizar Mobiles Start (para wizard).'
    
    __name__ = 'datos_sonar.type_event_mobile.sincronizar.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea Sincronizar los typeEvent basado en EventLocation?')
        
class SincronizarTypeEvent(Wizard):
    'Sincronizar Mobiles'
    __name__ = 'datos_sonar.type_event_mobile.sincronizar'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.type_event_mobile.sincronizar.start',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    sincronizar_nombres = StateAction('datos_sonar.type_event_name_sincronizar_wizard')
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        TypeEvent = Pool().get('datos_sonar.type_event_mobile')
        TypeEvent.sincronizar_basado_en_event_location()
        return 'sincronizar_nombres'

class SincronizarTypeEventNameStart(ModelView):
    ' Sincronizar TypeEventName start (para wizard).'
    
    __name__ = 'datos_sonar.type_event_mobile_name.sincronizar.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea Sincronizar los Nombres de los TypeEvent basado en EventLocation?')
        
class SincronizarTypeEventName(Wizard):
    'Sincronizar TypeEventMobileName'
    __name__ = 'datos_sonar.type_event_mobile_name.sincronizar'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.type_event_mobile_name.sincronizar.start',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        TypeEventName = Pool().get('datos_sonar.type_event_mobile_name')
        TypeEventName.actualizar_nombres()
        return 'end'

class MasterTypeEventMobile(ModelSQL,ModelView):
    """Maestro de los tipos de eventos, para solventar el problema de diferentes id por vehiculo para los eventos.
    @deprecated: ya no se usa, se debe eliminar  
    """
    __name__ = "datos_sonar.master_type_event_mobile"
    _rec_name = "name"
    
    
    name = fields.Char("Nombre")
    eventDescription = fields.Char("Nombre alternativo")
    children_events = fields.One2Many('datos_sonar.type_event_mobile', 'master_event', 'Eventos Hijos',order=[('mobil_id','ASC')])
    
    @classmethod
    def __setup__(cls):
        super(MasterTypeEventMobile, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.name),
                'Solo debe haber un registro para un name')
        ]
    
    @classmethod
    def make_master_events(cls):
        """ Crea los eventos maestros basado en todos los eventos existentes eventLocation.
        
        """
        EventLocation = Pool().get("datos_sonar.event_location")
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        eventos_maestros = cls.search_read(domain=[('id','>','0')])
        event_location = EventLocation.__table__()
        
        #SELECT a."eventDescription" FROM datos_sonar_event_location AS a GROUP BY a."eventDescription";
        cursor.execute(*event_location.select(event_location.eventDescription,group_by=event_location.eventDescription)) 
        
        col_sql = [x.name for x in cursor.description]
        col_sql = {col_sql[x]:x for x in range(0,len(col_sql))}#diccionario que contiene los nombres de los campos con el index de la posicion de las columnas
        
        nombres_existentes = [x['name'] for x in eventos_maestros]
        maestros_por_crear = [x[col_sql['eventDescription']] for x in cursor if x[col_sql['eventDescription']] not in nombres_existentes]
        maestros = [{'name':x,'eventDescription':x} for x in maestros_por_crear]
        records = cls.create(maestros)
        cls.save(records)

    @classmethod
    def link_with_master_events(cls,all_type_events=False):
        """ Vincula los eventos de mobiles con el evento maestro si no tienen evento maestro vinculado
        
        Si all_type_events = True se volveran a vincular todos los eventos.
        @attention: el vinculo se basa en el ultimo nombre de evento para el eventID
        """
        TypeEventMobile = Pool().get("datos_sonar.type_event_mobile")
        
        
        domain = [('id','>',0)] if all_type_events else [('id','>',0),('master_event','=',None)]
        
        tipos_de_eventos_por_mobil = TypeEventMobile.search_read(domain=domain)
        tipos_de_eventos_maestros = cls.search_read(domain=[('id','>','0')])
        
        def obtener_idx (eventos,name):
            for idx, evento in enumerate(eventos):
                if evento['name'] == name: return idx
        entre = 0
        no_entre = 0
        for evento in tipos_de_eventos_por_mobil:
            filtro = list(filter(lambda x: x['name']==evento['name'],tipos_de_eventos_maestros))
            if len(filtro) > 0:
                entre +=1
                evento['master_event']=tipos_de_eventos_maestros[obtener_idx(tipos_de_eventos_maestros, evento['name'])]['id']
                TypeEventMobile.update_master_event(evento['id'],evento['master_event'])
            else:
                no_entre += 1
                logger.error("Falta sincronizar eventos maestros, el type_event_mobile %s no tiene maestro a relacionar con el nombre: %s",evento['id'],evento['name'])
        logger.error("ENTRE:{e},NO ENTRE:{n}".format(e=entre,n=no_entre))
class ConstruirEventosMaestrosStart(ModelView):
    """Construir eventso maestreos vista inicial"""
    __name__ = 'datos_sonar.master_type_event_mobile.construir.start'
    
    nota = fields.Char('Nota',readonly=True)
    
    @classmethod
    def default_nota(cls):
        return('Desea construir los eventos maestros basado en los nombres de eventos de EventLocation?')


class ConstruirEventosMaestros(Wizard):
    """ Wizard que construye los eventos maestros"""
    __name__ = 'datos_sonar.master_type_event_mobile.construir'
    
    start = StateView('datos_sonar.master_type_event_mobile.construir.start','',
                    [
                        Button('Cancelar','end','tryton-cancel'),
                        Button('Construir','construir','tryton-go-next')
                    ]
                    )
    construir = StateTransition()
    vincular = StateAction('datos_sonar.master_event_vincular_wizard')
    
    def transition_construir(self):
        Master = Pool().get('datos_sonar.master_type_event_mobile')
        Master.make_master_events()
        return 'vincular'

class VincularEventosMaestreosStart(ModelView):
    """Vincular eventos maestreos vista inicial"""
    __name__ = 'datos_sonar.master_type_event_mobile.vincular.start'
    
    nota = fields.Char('Nota',readonly=True)
    todos = fields.Boolean('Todos los typos de eventos',
                        help='Vuelve a crear todos los vinculos con eventos maestros inclusive los que ya tienen vinculo. tardar un poco mas de tiempo'
                        )
    @classmethod
    def default_nota(cls):
        return('Desea vincular los eventos maestros con los typeEvent que no tienen evento maestro asociado?')
    
    @classmethod
    def default_todos(cls):
        return False


class VincularEventosMaestros(Wizard):
    """ Wizard que construye los eventos maestros"""
    __name__ = 'datos_sonar.master_type_event_mobile.vincular'
    
    start = StateView('datos_sonar.master_type_event_mobile.vincular.start','',
                    [
                        Button('Cancelar','end','tryton-cancel'),
                        Button('Vincular','vincular','tryton-go-next')
                    ]
                    )
    vincular = StateTransition()
    
    def transition_vincular(self):
        Master = Pool().get('datos_sonar.master_type_event_mobile')
        if self.start.todos:
            Master.link_with_master_events(all_type_events=True)
        else:
            Master.link_with_master_events()
        return 'end'


class MobileSelect(ModelView):
    'Vista para seleccionar mobil, usado en wizards'
    __name__ = 'datos_sonar.mobiles.select'
    
    mobil = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    
class MobileSelectRevision(ModelView):
    'Seleccionar revision del Mobil'
    __name__ = 'datos_sonar.mobiles.show_mobile_history.select_revision'
    
    nota = fields.Char('Nota',readonly=True)
    mobil = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    revisiones = fields.Selection('on_change_with_revisiones','Revisiones',
                        help="Hasta cuando?",
                        required = True,
                        )
    #mobil_anterior = fields.Many2Many('datos_sonar.mobiles_history',None,None,'historicos')
    @classmethod
    def default_nota(cls):
        return('Wizard en desarrollo, por el momento solo se puede ver la lista de historicos para un vehiculo')
    
    @classmethod
    def default_mobil(cls): 
        return Transaction().context['active_id']
    
    #@classmethod
    @fields.depends('mobil')
    def on_change_with_revisiones(self,name="none"):
        Mobile = Pool().get('datos_sonar.mobiles')
        if hasattr(self.mobil, 'id'):
            revisiones = Mobile.history_revisions([self.mobil.id])
            #tabla_mobiles = Mobile.table_query( Mobile.__table_history__())
            #mobiles_viejos = Mobile.search_read(domain=[('id','=',self.mobil.id)])
            #logger.error("MOBILES VIEJOS:{m}".format(m=mobiles_viejos))
            return list(map(lambda x:(x[0],x[0]),revisiones))
        else:
            return []

class ShowMobilehistory(Wizard):
    __name__ = 'datos_sonar.mobiles.show_mobile_history'
    
    start = StateView('datos_sonar.mobiles.show_mobile_history.select_revision',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        #Button('Seleccionar', 'start','tryton-go-next')
                    ]
                    )
    
