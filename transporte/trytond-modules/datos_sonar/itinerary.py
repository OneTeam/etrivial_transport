# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging

from trytond.modules.datos_sonar.sonar import SonarMixin
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button



logger = logging.getLogger(__name__)

__all__ = ['Itinerary','ItineraryPoint','SyncItineraries',
           'SyncItinerariesPoints']

class Itinerary(SonarMixin,ModelSQL,ModelView):
    """No Point Informacion extraida usando la funcion GET_Itineraries de la API de SonarAvl.
    
    """
    __name__ = "datos_sonar.itinerary"
    _rec_name = "It_name"
    
    It_id = fields.Integer("It_id")
    It_name = fields.Char("It_name")
    It_IdFleet = fields.Integer("It_IdFleet")#@todo deberia ser una relacion a un modelo llamado flota
    #@todo se debe alamacenar It_Idgroup de sonar que es texto
    #y crear otro campo para almacenar la relacion con el grupo de tryton
    It_Idgroup = fields.Many2One("datos_sonar.mobile_group","It_Idgroup") 
    It_activo = fields.Char("It_activo")#@todo sacar listado de los tipos 
    It_auto = fields.Char("It_auto")
    It_timeUnit = fields.Char("It_timeUnit")
    It_timingP2P = fields.Char("It_timingP2P")
    ItPoints = fields.One2Many('datos_sonar.itinerary_point','itinerary','itinerary_points')
    
    @classmethod
    def __setup__(cls):
        super(Itinerary, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_Itinerary_It_id', Unique(table, table.It_id),
                'datos_sonar.itinerary: solo debe haber un registro para un It_id')
        ]
    
    @classmethod
    def quantity_points(cls,Itinerary_id):
        record = cls.browse([Itinerary_id])
        if record:
            return len(record[0].ItPoints)
    
    @classmethod
    def consultar_itineraries(cls):
        """Consulta los itinerarios para todos los grupos
        usando multiples cuentas de Sonar
        
        Devuelve ...
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        response = []
        responses = cls.request_object_multi_auth(auth_field = configuration.wsdl_auth,
                                         target='ItList.Itinerary',
                                         request='GET_Itineraries',
                                         params={},
                                         url=configuration.wsdl_url,
                                         log_=False)
        for l in responses.values():
            response.extend(l)
        
        return response
    
    @classmethod
    def SyncWsdl(cls,log_=False):
        """Sincroniza los itinerarios con SonarAvl usando la funcion GET_Itineraries.
        
        """
        if log_:
            logger.info("Inicio sincronizacion de los itinerarios")
        groups = Pool().get('datos_sonar.mobile_group')
        groups_dict = groups.allIdsFrom_gId()
        fields = ['It_id','It_name','It_IdFleet','It_Idgroup',
                  'It_activo','It_auto','It_timeUnit','It_timingP2P']
        #se ignora el campo ItPoints porque se syncronizan aparte
        
        values_create = []
        response = cls.consultar_itineraries()        
        if response:
            for itinerary in response:
                record = cls.search_read([
                    ('It_id','=',itinerary['It_id']),
                    ])
                if record:
                    values = {}
                    update = False
                    for field in fields:
                        if record[0][field] != getattr(itinerary, field):
                            if record[0][field] != getattr(itinerary, field):
                                update = True
                            values[field] = getattr(itinerary, field)
                    if update:
                        values['It_Idgroup'] = groups_dict [values['It_Idgroup']]
                        cls.update(record[0]['id'],values)
                    else:
                        values_create.append({x:getattr(itinerary, x) for x in fields })
                else:
                    values_create.append({x:getattr(itinerary, x) for x in fields })
        
        #cambiando groupID por el id en tryton de mobile_group
        for x in range(0,len(values_create)):
            values_create[x]['It_Idgroup'] = groups_dict[values_create[x]['It_Idgroup']]
        records = cls.create(values_create)
        cls.save(records)
        if log_:
            logger.info("Fin sincronizacion de los itinerarios")
        
        
    @classmethod
    def update(cls,itinerary_id,values):
        """Actualiza un Itinerario.
        
        @param itinerary_id: identificador del itinerario
        @param value: Diccionario con los campos a actualizar
        """
        records = cls.search([('id','=',itinerary_id)])
        cls.write(records, values)
        cls.save(records)
        
    @classmethod
    def id_It_id_dict(cls,invert=None):
        """Devuelve un diccionario donde los keys son los It_id y los values son los ids para todos los itinerarios.
        
        si se indica invert se intercambian los keys por los values
        """
        values = cls.search_read(['id','>',0],fields_names=['id','It_id'])
        if invert:
            return {x['id']:x['It_id'] for x in values}
        return {x['It_id']:x['id'] for x in values}
    
    @classmethod
    def id_from_It_id(cls,It_id):
        """Devuelve el id de un itineario para el It_id pasado."""
        itinerary = cls.search([('It_id','=',It_id)])
        return itinerary[0].id
    
    @classmethod
    def It_id_from_id(cls,itinerary_id):
        """Devuelve el It_id de un itinerario (id)."""
        itinerary = cls.search_read([('id','=',itinerary_id)],fields_names=['It_id'])
        if itinerary:
            return itinerary[0]['It_id']
        
    @classmethod
    def It_name_from_id(cls,itinerary_id):
        """Devuelve el It_name de un itinerario (id)."""
        itinerary = cls.search_read([('id','=',itinerary_id)],fields_names=['It_name'])
        if itinerary:
            return itinerary[0]['It_name']
        
    @classmethod
    def It_name_from_It_id(cls,It_id):
        """Devuelve el It_name de un itinerario (IT_id)."""
        itinerary = cls.search_read([('It_id','=',It_id)],fields_names=['It_name'])
        if itinerary:
            return itinerary[0]['It_name']
    
    @classmethod
    def get_all_ids(cls,It_id=False):
        """Devuelve una lista con todos los ids de los itinerarios o los ItId si se indica este parametro
        
        """
        field = 'It_id' if It_id else 'id'
        itineraries = cls.search_read(['id','>',0],fields_names=[field])
        return [element[field] for element in itineraries]


    def get_events_description_for_geofences(self,ignored_geofences=[]):
        """Devuelve el nombre de los eventos(event_description) que se reportan
        cuando el vehículo ingresa a las geocercas de este itinerario
        """
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()

        points = [(x.point_index,x.geofence_name) for x in self.ItPoints if x not in ignored_geofences]
        name_constructor = configuration.evento_ingreso_geocerca or "Ingreso a {geocerca}"
        return [name_constructor.format(geocerca=x[1]) for x in points]


    def get_order_name_points(self):
        """Devuelve una lista de tupla con la posicion(point_index) y el nombre de la geocerca(geofence_name)
        """
        r = []
        if self.ItPoints:
            r = [point.get_order_name() for point in self.ItPoints]
        return r
        
class SyncItineraries(Wizard):
    'Wizard para la Sincronizacion de Itinerarios.'
    
    __name__ = 'datos_sonar.itinerary.sync'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sync_itineraries','tryton-go-next')
                    ]
                    )
    sync_itineraries = StateTransition()
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar todos los Itinerarios con SonarAVL?'}
    
    def transition_sync_itineraries(self):
        Itinerary = Pool().get('datos_sonar.itinerary')
        Itinerary.SyncWsdl()
        return 'end'

class ItineraryPoint(ModelSQL,ModelView):
    '''    Point Informacion extraida usando la funcion GET_Itineraries de la API de SonarAvl.
       
    '''
    __name__ = "datos_sonar.itinerary_point"
    _rec_name = "geofence_name"
    
    point_index = fields.Integer("point_index")
    geofence = fields.Many2One('datos_sonar.geofence','geofence')
    geofence_index = fields.Function(fields.Integer("geofence_index"),'on_change_with_geofence_index')
    geofence_name = fields.Function(fields.Char("geofence_name"),'on_change_with_geofence_name',searcher='search_geofence_name')
    time_min = fields.Integer("time_min")
    time_max = fields.Integer("time_max") 
    itinerary = fields.Many2One("datos_sonar.itinerary","Itinerario")
    itinerary_name = fields.Function(fields.Char("N.Itinerario"),'on_change_with_itinerary_name')

    @fields.depends('geofence')
    def on_change_with_geofence_index(self,name=None):
        if self.geofence:
            return self.geofence.g_index
        
    @fields.depends('geofence')
    def on_change_with_geofence_name(self,name=None):
        if self.geofence:
            return self.geofence.g_name
        else:
            return self.id
    
    
    @classmethod
    def search_geofence_name(cls, name, clause):
        return ['OR',
                ('geofence.g_name', ) + tuple(clause[1:]),
            ]
        

    @fields.depends('itinerary')
    def on_change_with_itinerary_name(self,name=None):
        if self.itinerary:
            return self.itinerary.It_name
        
    @classmethod
    def __setup__(cls):
        super(ItineraryPoint, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_ItineraryPoint_point_index_itinerary', Unique(table, table.point_index,table.itinerary),
                'datos_sonar.itinerary_point: solo debe haber un registro para un itinerary y un point index')
        ]

    @classmethod
    def SyncWsdl(cls,log_=False):
        """Sincroniza Los Points para todos los itinerarios con SonarAvl usando la funcion GET_Itineraries.
        
        """
        Itinerary = Pool().get('datos_sonar.itinerary')
        if log_:
            logger.info("Inicio sincronizacion de los Puntos de los itinerarios")
        itinearies_dict = Itinerary.id_It_id_dict()
        fields = ['point_index','geofence_index','geofence_name','time_min','time_max']
        response = Itinerary.consultar_itineraries()
        if response:
            def Itpoint_to_dict(Itpoint,itinerary_id):
                return {
                    'point_index': Itpoint['point_index'],
                    'geofence': cls.geofence_from_index_itinerary(Itpoint['geofence_index'],itinerary_id),
                    'time_min': Itpoint['time_min'],
                    'time_max': Itpoint['time_max'],
                    'itinerary': itinerary_id,
                    }
            ItPointsDic = {}
            for itinerary in response:
                #obtener la lista de diccionarios con los values para cada Point del Itinerario
                It_id = itinearies_dict[itinerary.It_id]
                if hasattr(itinerary.ItPoints, 'ItPoint'):
                    ItPointsDic[It_id] = [Itpoint_to_dict(x,It_id) for x in itinerary.ItPoints.ItPoint] 
            for itinerary_id,points in ItPointsDic.items():
                #eliminar los points del itinerario a actualizar
                records = cls.search([('itinerary','=',itinerary_id)])
                if records:
                    cls.delete(records)
                #crear los nuevos points vinculados al itinerario
            logger.info(ItPointsDic)
            values = []
            for l in ItPointsDic.values():
                values.extend(l)
            records=cls.create(values)
            cls.save(records)
        else:
            if log_:
                logger.info("Error en la sincronizacion de ItineraryPoint, sin respuesta de la API o sin datos para procesar")
        if log_:
            logger.info("Fin sincronizacion de los Puntos de los itinerarios")
    @classmethod
    def geofence_from_index_itinerary(cls,geofence_index,itinerary_id):
        """devuelve el id de la geocerca para un geofence_index y un itineary.
        
        Nota: las geocercas pertenecen a un grupo al igual que el itinerary,
         osea que en realidad se devuelve la geocerca del grupo (al que pertenece el itinerario) que coincide con el geofence_index pasado.
        """
        Itinerary = Pool().get('datos_sonar.itinerary')
        group = Itinerary.search_read([('id','=',itinerary_id)],fields_names=['id','It_Idgroup'])
        if group:
            Geofence = Pool().get('datos_sonar.geofence')
            geofence = Geofence.search_read([('groupID','=',group[0]['It_Idgroup']),('g_index','=',geofence_index)],fields_names=['id'])
            if geofence:
                return geofence[0]['id']
        logger.info("Error al consultar la geocerca para el itinerary_id:{i} y el index:{gi}. Es posible que falte sincronizar Grupos o Geocercas".format(i=itinerary_id,gi=geofence_index))
    
    @classmethod
    def geofence_name_from_index_and_itinerary(cls,point_index,itinerary_id):
        """devuelve el nombre de una geocerca para el p_index y el itinerary_id dados."""
        record = cls.search([('point_index','=',point_index),('itinerary','=',itinerary_id)])
        if record:
            return record[0].geofence_name 

    def get_order_name(self):
        """Devuelve una tupla con la posicion(point_index) y el nombre de la geocerca(geofence_name)
        """
        return (self.point_index,self.geofence_name)


class SyncItinerariesPoints(Wizard):
    'Wizard para la Sincronizacion de los Puntos de los Itinerarios.'
    
    __name__ = 'datos_sonar.itinerary_point.sync'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sync_itineraries','tryton-go-next')
                    ]
                    )
    sync_itineraries = StateTransition()
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar los Puntos de todos los Itinerarios con SonarAVL?'}
    
    def transition_sync_itineraries(self):
        ItineraryPoint = Pool().get('datos_sonar.itinerary_point')
        ItineraryPoint.SyncWsdl()
        return 'end'
