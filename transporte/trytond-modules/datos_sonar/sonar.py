# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
from datetime import datetime,timedelta

from trytond.model import fields
from trytond.pool import Pool
from trytond.transaction import Transaction

logger = logging.getLogger(__name__)

__all__ = ['SonarMixin','_select_sonar_auth','SonarAuthMixin',
           'SonarConstantSync', 'sonar_correct_status']

_select_sonar_auth = [('Belén','Belén'),
                      ('Metropolitanos','Metropolitanos')
                      ]

sonar_correct_status = 'OK'

class SonarAuthMixin(object):
    sonar_auth = fields.Selection(_select_sonar_auth,'Credencial Sonar',
                                  help="Indica la credencial que usa de sonar "
                                  "Esta debe coincidir con alguno de los usuarios "
                                  "de sonar indicados en la configuracion"
                                  )

class SonarMixin(object):
    """Mixin para modelos de Sonar que tienen su contraparte en la
    Plataforma de Sonar
    
    Las operaciones que se accede esta definidas en:
    https://sonaravl.com/b2b/    
    """

    _sonar_request = ''
    _sonar_wsdl_url = ''
    _sonar_correct_status = 'OK'
    
    class NoneResponse(object):
        """Clase usada para indica que no hubo respuesta"""
        pass
    
    @classmethod
    def connect(cls,wsdl_url):
        """Reliza la conexion con el webservice de Sonar y devuelve dicha conexion
        
        """
        try:
            from zeep import Client #seria ideal hacer un paquete aparte para tener las funciones especificas del Soap de Sonar extrayendo todos los comandos zeep
        except ModuleNotFoundError:
            raise Exception("Error: Module Zeep NotFound")
        client = Client(wsdl_url)
        return client
    
    @classmethod
    def get_login(cls,user,password):
        """Devuelve un diccionario con el User y Password para la conexión de sonar
        
        """
        return {'User':user,
                'Password':password
                }
        
    @classmethod
    def login_from_field(cls,auth_field,name=""):
        """Devuelve un diccionario con el User y Password para la conexión
        con sonar extrañendolo de un objeto iterable.
        
        
        @param field: objeto iterable donde cada elemento de la iteración tiene
         los atributos: name, user y Password
        """
        for auth in auth_field:
            if not name:
                return {'User':auth.user,
                        'Password':auth.password
                        }
            if auth.name == name:
                return {'User':auth.user,
                        'Password':auth.password
                        }
    
    @classmethod
    def request(cls,auth_field,request='',params={},auth='',url=''):
        """Genera una operacion en el webservice de Sonar y devuelve la respuesta
        
        @param field: objeto iterable donde cada elemento de la iteración tiene
         los atributos: name, user y Password
        @param request: operacion que se realizará
        @param params: diccionario con parametros que se pasará,
         no incluye user ni password
        @param auth: string de identifiacion del usuario y clave a utilizar para
         esta operación
        @param url: url del WSDL para la conexión,
         si está definido el atributo _wsdl_url no se requiere 
        """
        request = request or cls._sonar_request
        client = cls.connect(url)
        params.update(cls.login_from_field(auth_field,auth))
        Request = getattr(client.service, request)
        response = Request(**params)
        return response
    
    @classmethod
    def correct_response(cls,auth_field,request='',params={},auth='',url='',log_=False):
        """
        Genera una operación en el webservice de Sonar y devuelve la respuesta
        si es correcta, de lo contrario devuelve NoneResponse object
        """
        if log_:
            logger.info("Entre correct_response")
        response = cls.request(auth_field, request, params, auth, url)
        #if log_:
        #    logger.info("Params: {}".format(params))
        #    logger.info("Response: {}".format(response))
        if response:
            if response.status == cls._sonar_correct_status:
                return response
        else:
            if log_:
                logger.info("Error en la solicitud al webservice de Sonar: {}".format(response))
            return cls.NoneResponse()
    
    @classmethod
    def request_object(cls,auth_field,target,request='',params={},auth='',url='',log_=False):
        """ Genera una operación en el WebService de Sonar y devuelve
        el objeto (target) de la respuesta
        
        @param target: nombre del objeto objetivo, se puede usar '.' para 
         indicar elementos del elemento objetivo
        @return: el objeto en caso de que la operación sea exitosa
         sino NoneResponse object, si la respuesta es exitosa y no se
         encuentra el objetivo (target) se envia una lista vacia []
        
        """
        if log_:
            logger.info("Entre request_object")
        empty_return = []
        response = cls.correct_response(auth_field, request, params, auth, url,log_=log_)
        if response:
            targets = target.split('.')
            if hasattr(response, targets[0]):
                response_t = getattr(response, targets[0])
            else:
                #cuando el target no se encuentra y hay una respuesta correcta
                #del WebService de Sonar lo mas problable es que sea vacia la
                #respuesta
                return empty_return
            if len(targets) > 1:
                for target_ in targets[1:]:
                    if hasattr(response_t, target_):
                        response_t = getattr(response_t, target_)
                    else:
                        return empty_return
            return response_t
        else:
            if log_:
                logger.info("Error en la solicitud al webservice de Sonar: {}".format(response))
            return cls.NoneResponse()
    
    @classmethod
    def is_noneresponse(cls,response):
        return True if isinstance(response, cls.NoneResponse) else False
        
    
    @classmethod
    def request_object_multi_auth(cls,auth_field,target,request='',params={},url='',log_=False):
        """Realiza una consulta en el web service de sonar y devuelve una lista con la respuesta
        para cada auth pasada en el auth_field"""
        responses = {}
        for auth in auth_field:
            response = cls.request_object(auth_field=auth_field,
                                          target=target,
                                          request=request,
                                          params=params,
                                          auth=auth.name,
                                          url=url,
                                          log_=log_)
            if cls.is_noneresponse(response):
                if log_:
                    logger.error("Sin datos o error en la consulta "
                                 "al WebService de Sonar: {}".format(auth.name))
                continue
            else:
                responses[auth.name] = response
        return responses

class SonarConstantSync(SonarMixin):
    """Mixin para modelos que ejecutan sincronizacion constante
    
    @attention: se debe definir en el modelo _field_time
    @attention: se debe definir en el modelo _sonar_constant_sync_request
    """
    
    @classmethod
    def time_for_constant_sync_mobil(cls,mId,max_days_for_sync=4,diff_time=1,other_domains=[],format_='%Y-%m-%d %H:%M:%S',log_=False):
        """Devuelve un init_time y end_time para un mobil
        
        @param diff-time: tiempo en segundos que se restará al tiempo del último evento
         usado para que no se omitan eventos en caso de que tengan el mismo tiempo.
        @return: 
         init_time = datetime del último evento registrado menos diff_time segundos o None
         end_time = el tiempo actual si la dieferencia con init time es menor a 4 dias,
          de lo contrario init_time + 4 dias o None
        """
        if log_:
            logger.info("Entre time_for_constant_sync_mobil")
        if other_domains:
            last = cls.last_time_mobil(mId=mId,other_domains=other_domains)
        else:
            last = cls.last_time_mobil(mId=mId,other_domains=other_domains)
        
        if last:
            rtime = timedelta(seconds=diff_time)
            datetime_init = last - rtime
            now = datetime.today()
            if datetime_init > now:
                if log_:
                    logger.error("Error calculando datetime_init: ultimo "
                    "registro en el futuro. Registro:{ev}".format(ev=last))
                return None,None
            if (now - datetime_init).days > max_days_for_sync:
                datetime_end = datetime_init + timedelta(days=max_days_for_sync)
                if log_:
                    logger.info("Diferencia de tiempo muy grande, ajustada. "
                                "init: {}, end:{}".format(datetime_init,datetime_end))
            else:
                datetime_end = now
                
            return datetime_init.strftime(format_),datetime_end.strftime(format_)
        else:
            if log_:
                    logger.info("Error calculando datetime_init: no existe "
                                "registro anterior para el modelo: {m}. "
                                "Deberia primero sincronizar por tareas".format(m=cls.__name__))
            return None,None
    
    @classmethod
    def last_time_mobil(cls,mId='',mobil_id='',other_domains=[]):
        """Devuelve la hora (datetime) del ultimo registro para un mobil"""
        Configuration = Pool().get("datos_sonar.configurations")
        configuration = Configuration.get_singleton()
        domain = []
        if mId:#@todo verificar que todos los modelos el campo mId sea mId y no mobil_id
            domain.append(('mId','=',mId))
        if mobil_id:
            domain.append(('mobil_id','=',mobil_id))
        if not domain:
            return None
        if other_domains:
            domain.extend(other_domains)
        #limitando el periodo de busqueda para mejorar rendimiento
        #en tablas particionadas
        back_days_for_find_sync_event = configuration.back_days_for_find_sync_event or 30 
        domain.append((cls._field_time,
                       '>',
                       datetime.today()- timedelta(days=back_days_for_find_sync_event)))
        record = cls.search_read(domain,
                   limit=1,
                   order=[(cls._field_time,'DESC')],
                   fields_names=[cls._field_time]
                   )
        if record:
            return record[0][cls._field_time]
        else:
            return None
    
        
    @classmethod
    def _constant_sync_for_mobil(cls,mId='',mobil_id='',auth='',subparam=set(),extra_param={},log_=False):
        """Verifica y completa los datos para ejecutar sincronizacion constante del mid
        
        
        @attention: se debe pasar mId o mobil_id de lo contrario falla
        @attention: se dejan opcinal los parametros para poder disminuir la cantidad de
                    consultas si quien llama este metodo consulta para todos los vehículos
        @param other_params: diccionario de la forma {field:(field,param_,value)}
        @return: diccionario: {
                        'registros_sincronizados': len(respuesta.EventLocation),
                        'hora_ejecucion':datetime.today(),
                        }
        o False si falla la ejecucion
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        Configuration = Pool().get("datos_sonar.configurations")
        
        if log_:
            logger.info("Entre _constant_sync_for_mobil")
            #logger.info("Other_params: {}".format(subparam))
        
        configuration = Configuration.get_singleton()
        if not configuration.wsdl_auth:
            if log_:
                logger.info("Error: Debe configurar credenciales de Sonar")
            return False
        if not(mId or mobil_id or auth):
            mobil = Mobiles.get_ids_auth(mobil_id=mobil_id,mId=mId)
        else:
            mobil = {'mId':mId,
                     'id':mobil_id,
                     'sonar_auth':auth
                     }
        other_domains = [(field,'=',value) for field,param_,value in subparam]

        UTC_date_time_init, UTC_date_time_end = cls.time_for_constant_sync_mobil(
            mId=mobil['mId'],
            max_days_for_sync=configuration.cantidad_dias_por_tarea,
            other_domains=other_domains,
            format_=configuration.wsdl_timeformat,
            log_=log_
            )

        if not (UTC_date_time_init or UTC_date_time_end):
            return False
        
        other_params= {param_:value for field,param_,value in subparam}
        if hasattr(cls, '_sonar_constant_sync_extra_param'):
            if cls._sonar_constant_sync_extra_param:
                other_params.update(cls._sonar_constant_sync_extra_param)
        return cls.execute_sync(mId=mobil['mId'],
                         auth=mobil['sonar_auth'],
                         auth_field=configuration.wsdl_auth,
                         url=configuration.wsdl_url,
                         UTC_date_time_init=UTC_date_time_init,
                         UTC_date_time_end=UTC_date_time_end,
                         other_params=other_params,
                         log_=log_)


    @classmethod
    def constant_sync_for_mobil(cls,mId='',mobil_id='',auth='',subparam=set(),log_=False):
        """
        
        @param sub_param: tupla con 3 valores: nombre del campo,nombre del parametro y un iterable con los
                          valores que se deberá ejecutar. Esto es porque hay modelos en los
                          cuales se debe ejecutar varias veces la syncronizacion. ej: el 
                          modelo counterlog debe ejecutar para un vehiculo 6 sincronizaciones,
                          una para cada contador en este caso la tupla debería ser:
                          ('counterId','counter_id',[0,1,2,3,4,5]) o ('conter_id',range(0,6))
        """
        status = False
        if log_:
            logger.info("Inicio syncronizacion constante para el mobil {}"
                        " del modelo: {}".format(mId or mobil_id,cls.__name__))
        if subparam:
            field,param_, values = subparam
            for value in values:
                subparam = [(field,param_,value)]
                status = cls._constant_sync_for_mobil(mId=mId,
                                             mobil_id=mobil_id,
                                             auth=auth,
                                             subparam=subparam,
                                             log_=log_)
        elif hasattr(cls,'_sonar_constant_sync_subparams_mobil'):
            if cls._sonar_constant_sync_subparams_mobil:
                field,param_,values = cls.subparam_for_constant_sync(mId=mId,mobil_id=mobil_id)
                for value in values:
                    subparam = [(field,param_,value)]
                    status = cls._constant_sync_for_mobil(mId=mId,
                                             mobil_id=mobil_id,
                                             auth=auth,
                                             subparam=subparam,
                                             log_=log_)
        else:
            status = cls._constant_sync_for_mobil(mId=mId,
                                         mobil_id=mobil_id,
                                         auth=auth,
                                         log_=log_)
        if log_:
            logger.info("Fin syncronizacion constante para el mobil {}"
                        " del modelo: {}".format(mId or mobil_id,cls.__name__))
        return status
    
    @classmethod
    def constant_sync_for_all_mobiles(cls,log_=False):
        """Ejecuta la sincronizacion constante para todos los vehiculos
        en el acutal modulo"""
        Mobiles = Pool().get('datos_sonar.mobiles')
        if log_:
            logger.info("Inicio syncronizacion constante para todos "
                        "los mobiles del modelo: {}".format(cls.__name__))
        mobiles = Mobiles.get_ids_auth()
        subparam = set()
        if hasattr(cls,'_sonar_constant_sync_subparams_model'):
            if cls._sonar_constant_sync_subparams_model:
                subparam = cls.subparam_for_constant_sync()
        counter = cls._contador(1)
        for mobil in mobiles:
            if log_:
                logger.info("model: {m}, vehiculo #{n} de {c}".format(m=cls.__name__,
                                                                      n=next(counter),
                                                                      c=len(mobiles)
                                                                      ))
            status = cls.constant_sync_for_mobil(mId=mobil['mId'],
                                        mobil_id=mobil['id'],
                                        auth=mobil['sonar_auth'],
                                        subparam=subparam,
                                        log_=log_)
            #se graba la información por vehiculo
            Transaction().commit()
        if log_:
            logger.info("Fin sincronizacion constante para todos "
                    "los mobiles del modelo: {}".format(cls.__name__))
        
        
    
    @staticmethod
    def _contador(inicial):
        while True:
            yield inicial
            inicial += 1
    
    
    @classmethod
    def execute_sync(cls,mId,auth_field,url,UTC_date_time_init,UTC_date_time_end,auth='',other_params={}, log_=False):
        """Ejecuta una sincronizacion al WSDL con los parametros pasados
        
        UTC_date_time_init y UTC_date_time_end deben ser pasados en el formato que pide el WSDL (%Y-%m-%d %H:%M:%S)
        @param other_params: diccionario con parametros adicionales ej: {'counterId':"5"} o {'Itinerary'=""}
        @param overwrite_params: diccionario con nombre de parametros a sobreescribir y el nuevo nombre del parametro
                                 como valor.
        Devuelve un diccionario: {
                        'registros_sincronizados': len(respuesta.infoWSDLModelo[model]['objeto_lista_de_array']),
                        'hora_ejecucion':datetime.today(),
                        }
        o False si falla la ejecucion
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        if not auth:
            auth = Mobiles.get_ids_auth(mId=mId)['sonar_auth']
            
        if log_:
            logger.info("Inicio sincronizacion para el modelo {modelo} "
                        "del  {dia_init} al {dia_end} para el mobil {mobil} "
                        "a las: {tiempo}".format(modelo=cls.__name__,
                                                 dia_init=UTC_date_time_init,
                                                 dia_end=UTC_date_time_end,
                                                 mobil=mId,
                                                 tiempo=datetime.today()
                                                 )
                        )

        params = {'mId':mId,
            'UTC_datetime_init':UTC_date_time_init,
            'UTC_datetime_end':UTC_date_time_end,
            }
        if other_params:
            params.update(other_params)
        
        if hasattr(cls, '_sonar_constant_sync_extra_param'):
            if cls._sonar_constant_sync_extra_param:
                params.update(cls._sonar_constant_sync_extra_param)

        if hasattr(cls, 'get_overwrite_params'):
            for before, after in cls.get_overwrite_params().items():
                if before in params:
                    params[after] = params[before]
                    del params[before]

        response = cls.request_object(auth_field=auth_field,
                                          target=cls._sonar_constant_sync_target,
                                          request=cls._sonar_constant_sync_request,
                                          params=params,
                                          auth=auth,
                                          url=url,
                                          log_=log_)
        if cls.is_noneresponse(response):
            if log_:
                logger.error("Error en la consulta "
                             "al WebService de Sonar. params {}, auth{}".format(params,auth))     
        elif not response:
            if log_:
                logger.info("Sin datos en la sincronizacion para el modelo {modelo} "
                            "del  {dia_init} al {dia_end} para el mobil "
                            "{mobil} a las: {tiempo}".format(modelo=cls.__name__,
                                                             dia_init=UTC_date_time_init,
                                                             dia_end=UTC_date_time_end,
                                                             mobil=mId,
                                                             tiempo=datetime.today()
                                                             )
                            )
            return {
                    'registros_sincronizados': 0,
                    'hora_ejecucion':datetime.today(),
                    }
        
        else:
            if cls.syncRespuestaAPI(response):
                if log_:
                    logger.info("Fin sincronizacion para el modelo {modelo} "
                                "del  {dia_init} al {dia_end} para el mobil {mobil} "
                                "a las: {tiempo}".format(modelo=cls.__name__,
                                                         dia_init=UTC_date_time_init,
                                                         dia_end=UTC_date_time_end,
                                                         mobil=mId,
                                                         tiempo=datetime.today()
                                                         )
                                )
                return {
                    'registros_sincronizados': len(getattr(response,cls._sonar_constant_sync_array)),
                    'hora_ejecucion':datetime.today(),
                    }
            else:
                if log_:
                    logger.error("Error sincronizacion para el modelo {modelo} "
                                 "del  {dia_init} al {dia_end} para el mobil {mobil} "
                                 "a las: {tiempo}".format(modelo=cls.__name__,
                                                          dia_init=UTC_date_time_init,
                                                          dia_end=UTC_date_time_end,
                                                          mobil=mId,
                                                          tiempo=datetime.today()
                                                          )
                                 )
                return False
