# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .mobiles import *
from .configuration import *
from .event_location import *
from .passengers_log import *
from .counter_log import *
from .sync_task import *
from .mobileaddons import *
from .mobile_group import *
from .generic_class import *
from .itinerary import *
from .geofence import *
from .itinerary_history import *
from .drivers import *
from .drivers_history import *
from .geo_passenger_log import *

def register():
    #registro de modelos en el framework
    Pool.register(
        Mobile,
        MobileAddon,
        Configuration,
        ConfigurationSonarAuth,
        EventLocation,
        EventLocationFiltro,
        PassengersLog,
        Drivers,
        DriversmIdValids,
        DriversHistory,
        DriverSelect,
        DriverSelectRevision,
        CounterLog,
        SyncTask,
        TypeEventMobile,
        MasterTypeEventMobile,
        TotalDiarioEventLocation,
        MobileSelectRevision,
        SincronizarMobilesStart,
        SincronizarMobileaddonsStart,
        CrearTareasParametrizar,
        EliminarTareasNoEjecutadasStart,
        ExportEventLocationCSVData,
        SincronizarTypeEventStart,
        TypeEventMobileName,
        SincronizarTypeEventNameStart,
        ConstruirEventosMaestrosStart,
        VincularEventosMaestreosStart,
        StadisticLocationFiltro,
        StadisticLocationSelectType,
        StadisticEventLocationCSV,
        MobileGroup,
        GenericStartView,
        WsdlConection,
        ItineraryPoint,
        Itinerary,
        Geofence,
        ItineraryHistoryPoint,
        ItineraryHistory,
        ItineraryHistoryFilter,
        EventLocationDateFilter,
        MasterEvent,
        EventsForDay,
        EventsForMonth,
        ShowEvasionFilter,
        AnaliticMasterEvent,
        AnaliticCondition,
        AnaliticEventsForDay,
        AnaliticEventsForMonth,
        VirtualItineraryHistoryPoint,
        VirtualItineraryHistory,
        VirtualItineraryHistoryParams,
        QunatityItineraryHistoryParams,
        VirtualItineraryHistoryDeleteDuplicatedParams,
        GeoPassengerLog,
        module="datos_sonar", type_="model"
        )
    #registro de wizards en el framework
    Pool.register(
        ExportEventLocation,
        ShowMobilehistory,
        SincronizarMobiles,
        SincronizarMobileaddons,
        CrearTareas,
        EliminarTareasNoEjecutadas,
        ExportEventLocationCSV,
        SincronizarTypeEvent,
        SincronizarTypeEventName,
        ConstruirEventosMaestros,
        VincularEventosMaestros,
        StadisticEventLocation,
        SincronizarMobileGroup,
        SyncAllMobileGroup,
        SyncGeofencesAllGroups,
        SyncItineraries,
        SyncItinerariesPoints,
        ItineraryHistoryFilterCsv,
        ItineraryHistoryStadistic,
        SpeedMobile,
        OpenEvents,
        OpenDays,
        ShowEvasion,
        ShowDriverhistory,
        SyncDrivers,
        CreateVirtualItineraryHistory,
        OpenEventsFromItinerarHistory,
        QuantityItineraryHistory,
        VirtualItineraryHistoryDeleteDuplicated,
        GeoPassengerLogMap,
        module='datos_sonar', type_='wizard'
        )
    #registro de Reportes en el framework
    Pool.register(
        ItineraryHistoryCsv,
        ItineraryHistoryCsvStadistic,
        SpeedMobileGraph,
        module='datos_sonar', type_='report')
    return True
