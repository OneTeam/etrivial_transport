# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime

from trytond.modules.datos_sonar.sonar import _select_sonar_auth

from trytond.model import ModelSQL, ModelView, fields, ModelSingleton, Unique



__all__ = ['Configuration','ConfigurationSonarAuth']

class Configuration(ModelSingleton,ModelSQL,ModelView):
    '''
    Configuraciones Necesarias
    '''
    __name__ = "datos_sonar.configurations"
    _rec_name = "id"
    
    wsdl_url = fields.Char("WSDL Url",help="Direccion Url del archivo")
    wsdl_auth = fields.One2Many('datos_sonar.configurations.sonar_auth','configuration','Identidades wsdl',
                                help="Identidades en Datos Sonar, "
                                "La idea es un usuario por cada plataforma separada de Sonar, "
                                "ejemplo: Belén y Metropolitanos"
                                )
    wsdl_user = fields.Char("WSDL USER @deprecated", help="Usuario para autenticarse en le WSDL")
    wsdl_password = fields.Char("WSDL PASSWORD @deprecated", help="Contrasena para autenticarse en le WSDL")
    wsdl_timeformat = fields.Char("WSDL Time Format",help="Formato de tiempo de Sonar compatible con datetime Ejemplo: %Y-%m-%d %H:%M:%S")
    
    inicio_rango_countersid = fields.Integer("Inicio de Rango para CountersId",help="Los CounterId al parecer van del 0-99 y funcionales del 0-5 aca se puede configurar cuando se entienda mejor el funcionamiento")
    fin_rango_countersid = fields.Integer("Fin de Rango para CountersId",help="Los CounterId al parecer van del 0-99 y funcionales del 0-5 aca se puede configurar cuando se entienda mejor el funcionamiento")
    
    fecha_inicio_sonar = fields.DateTime('Fecha de inicio de sonar',
                                        help='Fecha en que inicia a funcionar SonarAVL con la empresa, osea desde cuando hay datos.',
                                        required=True,
                                        )
    prioridad_tareas_pordefecto = fields.Integer('Prioridad para Tareas por defecto',
                                                help="Prioridad para Tareas por defecto"
                                                )
    link_map_format = fields.Char('Formato de enlace al mapa',
                                help='Formato de enlace al mapa usar {0} (latitud) y {1}(longitud) para la latitud y la longitud en este orden ej: http://www.openstreetmap.org/?mlat={0}&mlon={1}&zoom=12 ',
                                required=True,
                                )
    cantidad_de_tareas_ejecucion_cron = fields.Integer('Cantidad de Tareas ejecucion Cron',
                                                    help='Indica cuantas tareas se ejecutaran cada que el Cron ejecute la sincronizacion')
    cantidad_dias_por_tarea = fields.Integer('Cantidad de dias por tarea',
                                            help="Indica la cantidad de dias para los cual se ejecutara una tarea, para unas funciones hay limite de 5 dias"
                                            )
    zona_horaria = fields.Char('Zona Horaria',
                            help="No implementado. Zona Horaria Usada para la columna virtual para exportar gpsUTC y systemUTC en hora de colombia, por defecto 'America/Bogota'"
                            )
    diferencia_utc = fields.Integer('Diferencia en horas con UTC',
                                            help="La diferencia en Horas con Respecto a UTC, para colombia -5"
                                            )
    init_time_no_constant_sync = fields.TimeDelta('Inicio de intervalo de no sincronizacion constante',
                                                  help="Medido en tiempo a partir de las 00 horas del dia"
                                                  )
    period_no_constant_sync = fields.TimeDelta('Cantidad de tiempo que se pausara la sincronizacion',
                                                  help="Debe ser menor a 24 horas"
                                                  )
    back_days_for_find_sync_event = fields.Integer('Cantidad de días para buscar ultimo evento de sincronizacion',
                                                   help="Valor usando en la sincronizacion constante para "
                                                   "mejorar el rendimiento en tablas particionadas.\n"
                                                   "Si no se define o se usa 0 se usará 30")
    evento_ingreso_geocerca = fields.Char('Evento Ingreso Geocerca',
                                          required=True,
                                          help="{geocerca} indica el nombre de la"
                                               "geocerca. ej: Ingreso a {geocerca}"
                                               " \n Usado para construir los "
                                               "itinerarios historicos basado "
                                               "en eventos")
    limite_exceso_velocidad_itinerarios = fields.Integer('Limite velocidad Itinerarios Historicos',
                                                         help="Es usado para el reporte de itinerarios, "
                                                         "que cuenta los excesos de velocidad")

    @classmethod
    def default_evento_ingreso_geocerca(cls):
        return "Ingreso a {geocerca}"
    
    @classmethod
    def default_diferencia_utc(cls):
        return(-5)
    
    @classmethod
    def default_zona_horaria(cls):
        return('America/Bogota')
    
    @classmethod
    def default_cantidad_dias_por_tarea(cls):
        return (4)
    
    @classmethod
    def default_cantidad_de_tareas_ejecucion_cron(cls):
        return (4)
    
    @classmethod
    def default_link_map_format(cls):
        return ('http://www.openstreetmap.org/?mlat={0}&mlon={1}&zoom=12')
    
    @classmethod
    def default_prioridad_tareas_pordefecto(cls):
        return (10)
    
    @classmethod
    def default_fecha_inicio_sonar(cls):
        return (datetime.strptime("2018-01-01","%Y-%m-%d"))
    
    @classmethod
    def default_inicio_rango_countersid(cls):
        return (0)
    
    @classmethod
    def default_fin_rango_countersid(cls):
        return (5)
    
    @classmethod
    def default_wsdl_timeformat(cls):
        return ('%Y-%m-%d %H:%M:%S')
    
    @classmethod
    def default_wsdl_url(cls):
        return ('https://sonaravl.com/b2b/Service.asmx?WSDL')
    

class ConfigurationSonarAuth(ModelSQL,ModelView):
    """Autenticaciones en la plataforam Sonar"""
    
    __name__ = 'datos_sonar.configurations.sonar_auth'
    
    name = fields.Selection(_select_sonar_auth,'Nombre',
                       required=True,
                       )
    user = fields.Char("WSDL USER",
                       required = True,
                       help="Usuario para autenticarse en el WSDL de Sonar")
    password = fields.Char("WSDL PASSWORD",
                           required = True,
                           help="Contrasena para autenticarse en le WSDL")
    configuration = fields.Many2One('datos_sonar.configurations','Configuracion',
                                    required=True,
#                                     domain = [('id','=',1)]
                                    )
    
    @classmethod
    def default_configuration(cls):
        return 1
    
    @classmethod
    def __setup__(cls):
        super(ConfigurationSonarAuth, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('SonarAuth_unique_name', Unique(table, table.name),
                'SonarAuth el nombre debe ser unico'),
            ('SonarAuth_unique_user', Unique(table, table.name),
                'SonarAuth el usuario debe ser unico'),
            
        ]
    
