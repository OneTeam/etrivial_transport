# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, Unique
from datetime import datetime, timedelta
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard.wizard import Wizard, Button, StateView, StateTransition,\
    StateAction
import logging


logger = logging.getLogger(__name__)

__all__ = ['Drivers', 'DriverSelect', 'DriverSelectRevision', 'ShowDriverhistory', 'DriversmIdValids',
           'SyncDrivers',]





class Drivers(ModelSQL,ModelView):
    '''    Informacion extraida usando la funcion GET_Drivers2 de la API.
    
    '''
    #definir campos obligatorios pues ayuda cuando falle la sincronizacion
    __name__ = "datos_sonar.drivers"
    _rec_name_ = "dr_name"
    
    dr_Id = fields.BigInteger("dr_Id")
    dr_cedula = fields.Integer("dr_cedula")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    dr_IdFleet = fields.Integer("dr_IdFleet")
    dr_name = fields.Char("dr_name")
    dr_address = fields.Char("dr_address")
    dr_phone = fields.BigInteger("dr_phone")
    dr_cellphone = fields.BigInteger("dr_cellphone")
    dr_email = fields.Char("dr_email")
    dr_mId = fields.Char("dr_mId")
    driver_ID = fields.One2Many("datos_sonar.drivers.midvalids","drivers","Validos")#es necesario ID mayuscula, es de sonar

    @classmethod
    def __setup__(cls):
        super(Drivers, cls).__setup__()
        
        #Activar history para este modelo
        cls._history = True
    
        
    @classmethod
    def syncRespuestaAPI(cls,drList):
        """Sincroniza los  Drivers objeto devuelto de la api de Sonar Avl.
        
        Este metodo es llamado por el modulo syncTask para la sincronizacion
        """
        #verificar que hallas cambios significativos antes de actualizar
        logger.error("TIEMPO 4: %s.",datetime.today())
        Mobiles = Pool().get('datos_sonar.mobiles')
        DriversmIdValids = Pool().get('datos_sonar.drivers.midvalids')
        
        mIds = Mobiles.allIdsMidDict()
        vlist = []
        records = cls.records_from_dr_Id([drlist.dr_Id for drlist in drList.Driver])
        for drlist in drList.Driver:
            
            if hasattr(drlist, 'dr_mIdValids'):
                mIdValids = DriversmIdValids.mIdValids(drlist.dr_mIdValids['string'],mIds) if drlist.dr_mIdValids  else []
            
            mobil_id = mIds[drlist.dr_mId] if drlist.dr_mId in mIds.keys() else None

            values = {
                'mobil_id': mobil_id,
                "dr_Id":drlist.dr_Id,
                "dr_cedula":drlist.dr_cedula,
                "dr_IdFleet":drlist.dr_IdFleet,
                "dr_name":drlist.dr_name,
                "dr_address":drlist.dr_address,
                "dr_phone":drlist.dr_phone,
                "dr_cellphone":drlist.dr_cellphone,
                "dr_email":drlist.dr_email,
                "dr_mId":drlist.dr_mId,
                "driver_ID": [('create', mIdValids)],
            }
            
            if drlist.dr_Id in records.keys():
                record = records[drlist.dr_Id]
                if not cls.equal(values,record):
                    DriversmIdValids.delete(record.driver_ID)
                    cls.write([record],values)
                    cls.save([record])
                else:
                    continue
            else:
                vlist.append(values)

        if vlist:
            records=cls.create(vlist)
            cls.save(records)
        return True
    
    @classmethod
    def records_from_dr_Id(cls,dr_Ids):
        """Devuelve un diccionario con dr_Id como llave y record como value
        de cada uno de los elementos de la lista que tengan registro existente."""
        
        records = cls.search([('dr_Id','in',dr_Ids)])
        return {x.dr_Id:x for x in records}
    
    @classmethod
    def equal(cls,value,record):
        """Comapra un diccionario de valores (value) con un record."""
        field_names = [x for x in value.keys()]
        for field in field_names:
            if field == 'mobil_id' or field == 'dr_mId':
                if not cls.equal_mobil_id(value['mobil_id'],value['dr_mId'],record.mobil_id,record.dr_mId):
                    return False
            elif field == 'driver_ID':
                if not cls.equal_driver_ID(value[field],record.driver_ID):
                    return False
            elif str(value[field]) != str(getattr(record,field)):
                return False
        return True
    
    @classmethod
    def equal_mobil_id(cls,value_mobil,value_mId,record_mobil,record_mId):
        if str(value_mobil) == str(record_mobil):
            return True
        elif str(value_mId) == str(record_mId):
            return True
        else:
            return False
    
    
    @classmethod
    def equal_driver_ID(cls,dr_ID_value,dr_ID_record):
        if not dr_ID_value and not dr_ID_record:
            return True
        if not dr_ID_value[0][1] and not dr_ID_record:
            return True
        if len(dr_ID_value[0][1]) != len(dr_ID_record):
            return False
        
        values = []
        values_r = []
        for value in dr_ID_value[0][1]:
            mobil_id = value['mobil_id'] if 'mobil_id' in value.keys() else None
            invalid_mId = value['invalid_mId'] if 'invalid_mId' in value.keys() else None
            values.append( (str(mobil_id),str(invalid_mId)) )

        for valuer in dr_ID_record:
            if hasattr(valuer, 'mobil_id'):
                if valuer.mobil_id:
                    mobil_idr = valuer.mobil_id.id
                else:
                    mobil_idr = None
            else:
                mobil_idr = None

            invalid_mId_r = valuer.invalid_mId if hasattr(valuer, 'invalid_mId') else None
            values_r.append( (str(mobil_idr),str(invalid_mId_r)) )
        
        for x in range(0,len(values)):
            if (values[x][0] != values_r[x][0]) or (values[x][1] != values_r[x][1]): 
                return False
        
        return True
            

class DriversmIdValids(ModelSQL,ModelView):
    'Modelo de los Vehículos permitidos actuales Drivers_v2'
    
    __name__ = 'datos_sonar.drivers.midvalids'
    
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    drivers = fields.Many2One("datos_sonar.drivers","Driver",ondelete='CASCADE')
    invalid_mId = fields.Char("mId Invalido",
                              help="Es llenado cuando el vehiculo al que esta asignado el driver\n"
                              "no existe en la base de datos.\n"
                              "Puede ser que falte sincronizar mobiles."
                              )
    
    @classmethod
    def __setup__(cls):
        super(DriversmIdValids, cls).__setup__()
        
        #Activar history para este modelo
        cls._history = True

    
    @classmethod
    def mIdValids(cls,mIdValids,mIds=None):
        """Devuelve una lista  de diccionarios con los valores para la creación de DriversmIdValids,
        basado en los elementos de la lista mIdValids.
        
        Se puede pasar el diccionario mIds para no realizar consulta en la base de datos, esto ayuda
        a disminuir las consultas cuando se usa de forma recursiva este metodo""" 
        if not mIds:
            Mobile = Pool().get('datos_sonar.mobiles')
            mIds = mIds = Mobile.allIdsMidDict()
        values1 = []
        for dr_mIdValids in mIdValids:
            if dr_mIdValids:
                values = {
                    'mobil_id': mIds[dr_mIdValids] if dr_mIdValids in mIds.keys() else None,
                    }
                if not values['mobil_id']:
                    values['invalid_mId'] = dr_mIdValids
                values1.append(values)
        return(values1)           
                
    
     
    

class DriverSelect(ModelView):
    'Vista para seleccionar Driver, usado en wizards'
    __name__ = 'datos_sonar.drivers.select'
    
    driver = fields.Many2One("datos_sonar.drivers","Drivers")
    
class DriverSelectRevision(ModelView):
    'Seleccionar revision del Driver'
    __name__ = 'datos_sonar.drivers.show_driver_history.select_revision'
    
    nota = fields.Char('Nota',readonly=True)
    driver = fields.Many2One("datos_sonar.drivers","Driver")
    revisiones = fields.Selection('on_change_with_revisiones','Revisiones',
                        help="Hasta cuando?",
                        required = True,
                        )
    #mobil_anterior = fields.Many2Many('datos_sonar.mobiles_history',None,None,'historicos')
    @classmethod
    def default_nota(cls):
        return('Wizard en desarrollo, por el momento solo se puede ver la lista de historicos para un vehiculo')
    
    @classmethod
    def default_driver(cls): 
        return Transaction().context['active_id']
    
    #@classmethod
    @fields.depends('driver')
    def on_change_with_revisiones(self,name="none"):
        Driver = Pool().get('datos_sonar.drivers')
       
        if hasattr(self.driver, 'id'):
            
            revisiones = Driver.history_revisions([self.driver.id])
            return list(map(lambda x:(x[0],x[0]),revisiones))
        else:
            return []
 
    
class ShowDriverhistory(Wizard):
    __name__ = 'datos_sonar.drivers.show_driver_history'
    
    start = StateView('datos_sonar.drivers.show_driver_history.select_revision',
                    '',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        #Button('Seleccionar', 'start','tryton-go-next')
                    ]
                    )

class SyncDrivers(Wizard):
    'Sincronizar Conductores'
    
    __name__ = 'datos_sonar.drivers.sync'
    start_state = 'confirmar'
    confirmar = StateView('datos_sonar.generic_class.generic_start_view',
                    'datos_sonar.view_generic_class_start',
                    [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Sincronizar', 'sincronizar','tryton-go-next')
                    ]
                    )
    sincronizar = StateTransition()
    
    def default_confirmar(self,fields):
        return {'attention':'Desea Sincronizar todos los drivers (conductores) con SonarAVL?'}
    
    def transition_confirmar(self):
        return 'sincronizar'
    
    def transition_sincronizar(self):
        SyncTask = Pool().get('datos_sonar.sync_task')
        SyncTask.ejecutar_sincronizacion('datos_sonar.drivers','','','')
        return 'end'
