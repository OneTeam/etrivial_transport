'''
Created on 16/05/2019

@author: desarrollo
'''
from time import sleep
from datetime import datetime
import logging

logger = logging.getLogger(__name__)


class SyncTask:
    """
    Sincronización constante sonar
    """
    command = "sync_task"

    @staticmethod
    def parser_args(parser):
        pass

    @staticmethod
    def run(pool, options):
        Sync = pool.get('datos_sonar.sync_task')
        logger.info(f"{datetime.now()} inicio tarea")
        while True:
            try:
                Sync.constant_sync(log_=True)
            except Exception:
                logger.error("Ejecutando Sincronización Constante")
            logger.info(f"{datetime.now()} Esperando 60 segundos")
            sleep(60)

