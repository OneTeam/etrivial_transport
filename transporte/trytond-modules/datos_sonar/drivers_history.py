# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, Unique
from datetime import datetime, timedelta
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard.wizard import Wizard, Button, StateView, StateTransition,\
    StateAction
import logging


logger = logging.getLogger(__name__)

__all__ = ['DriversHistory']





class DriversHistory(ModelSQL,ModelView):
    '''    Informacion extraida usando la funcion GET_Driver_History de la API.
    
    '''
    
    __name__ = "datos_sonar.drivers_history"
    __rec_name__ = "dr_Id"    
    regId = fields.BigInteger("regId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    mId = fields.Char("mId")
    dr_Id = fields.BigInteger("dr_Id")
    drl_name = fields.Char("drl_name")
    drl_assign = fields.Char("drl_assign")
    drl_user = fields.Char("drl_user")
    drl_UTCdatetime = fields.DateTime("drl_UTCdatetime")



    
    @classmethod
    def __setup__(cls):
        super(DriversHistory, cls).__setup__()
        
        #Activar history para este modelo
        cls._history = True
    
        
    @classmethod
    def syncRespuestaAPI(cls,drList):
        """Sincroniza los  DriversHistory objeto devuelto de la api de Sonar Avl.
        
        Este metodo es llamado por el modulo syncTask para la sincronizacion
        """
        logger.error("TIEMPO 4: %s.",datetime.today())
        #itinerary_point = Pool().get('datos_sonar.itinerary_point')
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        mIds = Mobiles.allIdsMidDict()
        vlist = []
        print(drList)
        
        for drlist in drList.DriversLog:
            records = cls.search([('dr_Id','=',drlist.dr_Id)])
            
            #print(drlist.dr_mId)
            values = {
                "regId": drlist.regId,
                "mId": drlist.mId,
                "dr_Id": drlist.dr_Id,
                "drl_name": drlist.drl_name,
                "drl_assign": drlist.drl_assign,                
                "drl_user": drlist.drl_user,
                "drl_UTCdatetime": drlist.drl_UTCdatetime
                }

            for mId in mIds:
                
                #print(mId)
                if mId == drlist.mId:
                    values["mobil_id"] = mIds[drlist.mId]
                
            vlist = []
            
            if records:        
                
                field_names = [x for x in values.keys() ]
                valores_tryton={ }
                
                #print(records)
                for field in field_names:
                #    print("records[0]."+field)
                #    print(field)    
                    valores_tryton[field] = eval ("records[0]."+field)
                if values != valores_tryton:
                    cls.write(records,values)
                    cls.save(records)
            else:                
                vlist.append(values)
                records=cls.create(vlist)
                cls.save(records)
        return True                               


