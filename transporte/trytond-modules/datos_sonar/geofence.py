# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, DictSchemaMixin, Unique
from trytond.pool import Pool
from datetime import datetime, date, timedelta
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.report import Report
from trytond.pyson import Eval
from trytond.transaction import Transaction
import logging

logger = logging.getLogger(__name__)

__all__ = ['Geofence']

class Geofence(ModelSQL,ModelView):
    '''    Informacion extraida usando la funcion GET_Itineraries de la API de SonarAvl.
      
    '''
    __name__ = "datos_sonar.geofence"
    _rec_name = "g_name"
    
    groupID = fields.Many2One('datos_sonar.mobile_group','groupID')
    g_index = fields.Integer("g_index")
    g_name = fields.Char("g_name")
    g_active = fields.Char("g_active")
    
    @classmethod
    def __setup__(cls):
        super(Geofence, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_Goefence_groupID_and_g_index', Unique(table, table.groupID,table.g_index),
                'datos_sonar.geofence: solo debe haber un registro para un groupID y g_index')
        ]
    
    @classmethod
    def update(cls,geofence_id,value):
        """Actualiza una Geocerca.
        
        @param geofence_id: identificador del geofence
        @param values: Diccionario con los campos y valores a actualizar
        """
        records = cls.search([('id','=',geofence_id)])
        if records:
            cls.write(records, value)
            cls.save(records)
