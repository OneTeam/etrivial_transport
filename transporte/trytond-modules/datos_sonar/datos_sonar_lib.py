# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import calendar
from datetime import date, datetime, timedelta


__all__ = ['month_last_day','previous_month', 'colombiadate2utctime',
           'colombia_month2utctime','date_month_in_range']

def month_last_day(month,year):
    return calendar.monthrange(month, year)[1]


def previous_month(year,month):
    """Devuelve la fecha del ultimo dia del mes anterior a la fecha dada."""
    
    date_ = date(year,month,1) - timedelta(days=1)
    return date_

def colombia_date2utctime(date_):
    """Devuelve el tiempo inicial y final en utc para una fecha colombiana"""
    
    init_time = datetime.fromordinal(date_.toordinal()) + timedelta(hours=5)
    end_time = init_time + timedelta(hours=24) - timedelta(seconds=1)
    return init_time, end_time

def colombia_month2utctime(year,month):
    """Devuelve el tiempo inicial y final en utc para un mes en fecha colombiana"""
    
    last_day = calendar.monthrange(year, month)[1]
    init_date = date(year,month,1)
    end_date = init_date.replace(day=last_day)

    init_time = colombia_date2utctime(init_date)[0]
    end_time = colombia_date2utctime(end_date)[1]
    return init_time, end_time

def date_month_in_range(init,end):
    """Devuelve una lista de datetime.date con el primer día de cada mes en el rango de fechas dadas."""

    l = []
    for year in range(init.year, end.year + 1):
        minit = init.month if init.year == year else 1
        mend = end.month if end.year == year else 12
        for month in range(minit, mend +1):
            l.append(date(year,month,1))
    return l


#no usadas
def str2date(string,format_='%Y-%m-%d'):
    return date.fromordinal(datetime.strptime(string,format_).toordinal())

    
def str2date_list(list_):
    if list_:
        return_ = []
        for x in list_:
            return_.append(str2date(x))
        return return_
    else:
        return []

def less_month_test(date1,date2):
    """evalua si el mes de la fecha1 es menor que el mes de la fecha2."""
    months = 12
    if ((date1.year * months) + date1.month) < (((date2.year * months) + date2.month)):
        return True
    else:
        return False 
