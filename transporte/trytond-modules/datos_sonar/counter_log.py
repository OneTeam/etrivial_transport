# This file is part of datos_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime
import logging

from trytond.modules.datos_sonar.sonar import SonarConstantSync

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool

#from trytond.transaction import Transaction


logger = logging.getLogger(__name__)

__all__ = ['CounterLog']

class CounterLog(SonarConstantSync,ModelSQL,ModelView):
    """
    Informacion extraida usando la funcion GET_CounterHistory de la API.
    regId long, mId string, counterId int, value long, gps_UTC string, system_utc string, 
    """
    __name__ = 'datos_sonar.counter_log'
    _rec_name = 'regId'
    
    #Campos requeridos para SonarConstantSync
    _field_time = 'system_utc'
    _sonar_constant_sync_request = 'GET_CounterHistory'
    _sonar_constant_sync_target = 'counterlogList'
    _sonar_constant_sync_array = 'CounterLog'
    _sonar_constant_sync_subparams_model = True
    
    regId = fields.BigInteger("regId",help="identificador del registro")
    mId = fields.Char("mId")
    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")  
    counterId = fields.Integer("counterId",help="Valores validos del 0-99 en pruebas de humo pareciera que son usados del 0-5, posiblemente una relacion")
    type_counter = fields.Function(fields.Char('Type'),'get_type_counter') 
    value = fields.BigInteger('value',help="Valor: supongo que dependiendo del counterId podria ser km/h, Km")
    gps_UTC = fields.DateTime("gps_UTC",help="tiempo del GPS") 
    system_utc = fields.DateTime("system_UTC",help="tiempo del sistema Sonar", required=True)
    gps_tz = fields.Function(fields.DateTime("Tiempo GPS Colombia"),"obtener_gps_tz")
    system_tz = fields.Function(fields.DateTime("Tiempo System Colombia"),"obtener_system_tz")

    def obtener_gps_tz(self,field):
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        zona_horaria = configuration.zona_horaria if configuration.zona_horaria != None else 'America/Bogota' 
        try:
            from pytz import timezone
            return self.gps_UTC.astimezone(timezone(zona_horaria))
        except ModuleNotFoundError:
            logger.eror("Error: Module pytz NotFound")
            return None
    
    def obtener_system_tz(self,field):
        Configuration = Pool().get('datos_sonar.configurations')
        configuration = Configuration.get_singleton()
        zona_horaria = configuration.zona_horaria if configuration.zona_horaria != None else 'America/Bogota'
        try:
            from pytz import timezone
            return self.system_utc.astimezone(timezone(zona_horaria))
        except ModuleNotFoundError:
            logger.eror("Error: Module pytz NotFound")
            return None
    
    def get_type_counter(self,name=None):
        if self.counterId:
            types_counters =[
                'Distancia recorrida',
                'Tiempo Encendido',
                'Tiempo en Movimiento',
                'Tiempo de la Unidad Encendida',
                'Tiempo de la unidad online',
                'Tiempo con posición GPS válida',
                ]
            return types_counters[self.counterId]
        
    @classmethod
    def __setup__(cls):
        super(CounterLog, cls).__setup__()
        cls._order = [('system_utc','DESC')]
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('regid_key', Unique(table, table.regId),
                'El regId debe ser unico')
        ]

    @classmethod
    def syncRespuestaAPI(cls,CounterLogResponse):
        """Sincroniza un CounterLogResponse
        
        Este metodo es llamado por el modulo syncTask para la sincronizacion
        """
        logger.error("TIEMPO 4: %s.",datetime.today())
        Mobiles = Pool().get('datos_sonar.mobiles')
        data = []
        fields_names=[
            "regId",
            "mId",
            "mobil_id",
            "counterId",
            "value",
            "gps_UTC",
            "system_utc",
            ]
        
        for CounterLog in CounterLogResponse.CounterLog:
            if not cls.existe_regid(CounterLog.regId):
                prototype = [
                    CounterLog.regId,
                    CounterLog.mId,
                    Mobiles.mDescriptionFromMid(CounterLog.mId),
                    CounterLog.counterId,
                    CounterLog.value,
                    CounterLog.gps_UTC,
                    CounterLog.system_UTC,        
                    ]
                data.append(prototype)
        errores=cls.import_data(fields_names, data)
        logger.info("Reporte Importacion: %s.",errores)
        logger.info("TIEMPO 9: %s.",datetime.today())
        return True
    
    @classmethod
    def existe_regid(cls,regId):
        """Si existe un evento con este regID devuelve verdadero de lo contrario falso.
        
        """
        regids = cls.search([('regId','=',regId)])
        existe = True if (len(regids) > 0 ) else False
        return existe
    
    
    @classmethod
    def relacionarConMobile(cls):
        """Relaciona los datos importados con mobiles
        
        """
        logger.info("Inicio relacionarConMobile: %s.",datetime.today())
        Mobiles = Pool().get('datos_sonar.mobiles')
        
        allIdsMidDict = Mobiles.allIdsMidDict()
        
        objetos = cls.search_read([('mobil_id','=',None)])
        for objeto in objetos:
            objeto['mobil_id']=allIdsMidDict[objeto['mId']]
            record = cls.search([('id','=',objeto['id'])])
            cls.write(record,objeto)
            cls.save(record)
        logger.info("Fin relacionarConMobile: %s.",datetime.today())
    
    @classmethod
    def subparam_for_constant_sync(cls):
        Configuration = Pool().get("datos_sonar.configurations")
        configuration = Configuration.get_singleton()
        return ('counterId','CounterID',
            range(configuration.inicio_rango_countersid,configuration.fin_rango_countersid + 1))
