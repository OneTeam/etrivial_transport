# This file is part of datos_sonar_monitor_avl.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

class Mobile(metaclass=PoolMeta):
    'Mobile'
    __name__ = "datos_sonar.mobiles"


    @classmethod
    def order_last_sync(cls, tables):
        table, _ = tables[None]
        return [table.last_sync_cache]

    @classmethod
    def order_percent_lost_connection(cls, tables):
        table, _ = tables[None]
        return [table.percent_lost_connection_cache]

    def get_last_sync(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        MonitorAvlMinute = pool.get('datos_sonar_monitor_avl.minute')
        try:
            minute, = MonitorAvlMinute.search_read([('mobil_id', '=', self.id)],
                                                   limit=1,
                                                   order=[('id', 'DESC')],
                                                   fields_names=['last_system_gmt'])
            with Transaction().new_transaction(autocommit=True, readonly=False) as t:
                cursor = t.connection.cursor()
                table = self.__table__()
                cursor.execute(*table.update(
                    columns=[table.last_sync_cache],
                    values=[minute['last_system_gmt']],
                    where=table.id == self.id))
            return minute['last_system_gmt']
        except ValueError:
            pass
        return None


    def get_percent_lost_connection(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        MonitorAvlMinute = pool.get('datos_sonar_monitor_avl.minute')

        avl_minutes = MonitorAvlMinute.search_read([('mobil_id', '=', self.id)],
                                                   order=[('id', 'DESC')],
                                                   limit=120,
                                                   fields_names=['accTotal'])
        if not avl_minutes:
            return 100

        count_repeat = {}
        max_repeat = None
        for avl_minute in avl_minutes:
            key = avl_minute['accTotal']
            if key not in count_repeat:
                count_repeat[key] = 1
            else:
                count_repeat[key] += 1
            if max_repeat is None:
                max_repeat = key
            elif count_repeat[key] >= count_repeat[max_repeat]:
                max_repeat = key

        calc = int((count_repeat[max_repeat] / sum(count_repeat.values())) * 100)
        # MACHETE: getter de function corre en en solo lectura,
        # se abre nueva transaccion que permita escritura
        with Transaction().new_transaction(autocommit=True, readonly=False) as t:
            cursor = t.connection.cursor()
            table = self.__table__()
            cursor.execute(*table.update(
                columns=[table.percent_lost_connection_cache],
                values=[calc],
                where=table.id == self.id))
        return calc


    last_sync_cache = fields.DateTime('Last Sync AVL', readonly=True,
                                      states={'invisible': True})
                                      
    last_sync = fields.Function(fields.DateTime('Last Sync AVL'),
                                'get_last_sync')

    percent_lost_connection_cache = fields.Float('% Lost Connection AVL', readonly=True,
                                                  states={'invisible': True})
    percent_lost_connection = fields.Function(fields.Float('% Lost Connection AVL'),
                                              'get_percent_lost_connection')
    
