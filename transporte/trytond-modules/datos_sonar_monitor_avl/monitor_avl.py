# This file is part of datos_sonar_monitor_avl.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime, timedelta

from trytond.model import Model, ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.transaction import Transaction

class MonitorAvlMaintenance(Model):
    'MonitorAvl'

    __name__ = 'datos_sonar_monitor_al.maintenance'

    @classmethod
    def machete_pgpathman(cls):
        cursor = Transaction().connection.cursor()
        cursor.execute("SELECT tablename FROM pg_catalog.pg_tables WHERE  schemaname != 'pg_catalog' AND schemaname != 'information_schema' AND tablename like 'datos_sonar_event_location%';")
        tablenames = []
        for row in cursor.fetchall():
            tablename = row[0]
            qdtrigger = """
            DROP TRIGGER IF EXISTS {table}_avl_minute_check_insert
            ON {table};
            """.format(table=tablename)
            
            qtrigger = """
            CREATE TRIGGER {table}_avl_minute_check_insert
            AFTER INSERT ON {table}
            FOR EACH ROW
            EXECUTE PROCEDURE datos_sonar_monitor_avl_minute_insert();
            """.format(table=tablename)

            cursor.execute(qdtrigger)
            cursor.execute(qtrigger)
    

# intentamos una serie de tiempo
# aunque lo idea es usar una base de datos 'round-robin database'.
# se asume que la diferencias grandes de tiempo mayores a 1 minuto
# ahi error con el avl, @mono dice que puede haber
# días sin conexión y no es error.

# el procedimiento es por media aritmetica
# se almacenan los diferenciales, se promedia con media aritmetica
# y el que este por encima del promedio general

# De esta tabla se pueden elimina periodicamente registros
class MonitorAvlMinute(ModelSQL, ModelView):
    'MonitorAvlMinute'

    __name__ = 'datos_sonar_monitor_avl.minute'

    mobil_id = fields.Many2One("datos_sonar.mobiles","Vehiculo")
    rows = fields.BigInteger('Rows')
    gps_gt_system_seconds = fields.BigInteger('GPS GMT - System GMT')
    latitude_gt_longitude = fields.Float('Latitude - Longitude')
    dtEventDescription = fields.BigInteger('DT Event Description')
    accSpeed = fields.BigInteger('Acc Speed')
    accPrecision = fields.BigInteger('Acc Precision')
    accHeading = fields.BigInteger('Acc Heading')

    last_system_gmt = fields.DateTime('System GMT')

    #agrupa por minuto
    serie = fields.Date("Serie")
    hour = fields.Integer("Hour")
    minute = fields.Integer("Minute")
    
    eventDescription = fields.Char('Event - Description')

    accTotal = fields.Function(fields.Float('Acc Total'),
                               'get_accTotal')

    @fields.depends('accSpeed', 'accPrecision', 'accHeading')
    def get_accTotal(self, name):
        return round((self.latitude_gt_longitude or 0) + (self.accHeading or 0) + (self.accSpeed or 0), 6)

    @classmethod
    def __setup__(cls):
        super(MonitorAvlMinute, cls).__setup__()

        t = cls.__table__()
        cls._sql_constraints = [
            ('mobile_serie_unique', Unique(t, t.mobil_id, t.serie, t.hour, t.minute),
                "serie can be only once."),
        ]

    @classmethod
    def __register__(cls, module_name):
        transaction = Transaction()
        super(MonitorAvlMinute, cls).__register__(module_name)

        cursor = transaction.connection.cursor()

        #https://stackoverflow.com/questions/14020919/find-difference-between-timestamps-in-seconds-in-postgresql
        #https://www.the-art-of-web.com/sql/upsert/
        qfunction = """
        CREATE OR REPLACE FUNCTION datos_sonar_monitor_avl_minute_insert()
        RETURNS trigger AS
        $BODY$
        BEGIN
        WITH upsert AS (
          UPDATE datos_sonar_monitor_avl_minute SET gps_gt_system_seconds = LEAST(gps_gt_system_seconds + ABS(EXTRACT(EPOCH FROM (NEW."gps_GMT" - NEW.system_gmt))), 86400), latitude_gt_longitude = LEAST(latitude_gt_longitude + (COALESCE(NEW.latitude, 0) - COALESCE(NEW.longitude, 0)), 9999), write_date = now(), rows = LEAST(rows + 1, 9999),  last_system_gmt = NEW.system_gmt, "accSpeed" = LEAST("accSpeed" + COALESCE(New.speed, 0), 9999), "accPrecision" = LEAST("accPrecision" + COALESCE(New.precision, 0), 9999), "accHeading" = LEAST("accHeading" + COALESCE(New.heading, 0), 9999)
         WHERE serie = CAST(NEW.system_gmt AS DATE) AND minute = EXTRACT(MINUTE FROM New.system_gmt) AND hour = EXTRACT(HOUR FROM New.system_gmt) AND mobil_id = NEW.mobil_id RETURNING *)
         INSERT INTO datos_sonar_monitor_avl_minute(mobil_id, gps_gt_system_seconds, latitude_gt_longitude, serie, hour, minute, create_date, rows, "accSpeed", "accPrecision", "accHeading", last_system_gmt) SELECT 
          NEW.mobil_id,
          LEAST(ABS(EXTRACT(EPOCH FROM (NEW."gps_GMT" - NEW.system_gmt))), 999999999),
          LEAST(COALESCE(NEW.latitude, 0) - COALESCE(NEW.longitude, 0), 999999999),
          NEW.system_gmt,
          EXTRACT(HOUR FROM New.system_gmt),
          EXTRACT(MINUTE FROM New.system_gmt),
          now(),
          1,
          COALESCE(New.speed, 0),
          COALESCE(New.precision, 0),
          COALESCE(New.heading, 0),
          New.system_gmt
         WHERE NOT EXISTS (SELECT * FROM upsert);
        RETURN NEW;
        END;
        $BODY$
        LANGUAGE plpgsql;
        """

        qdtrigger = """
        DROP TRIGGER IF EXISTS datos_sonar_monitor_avl_minute_check_insert
        ON datos_sonar_event_location;
        """
        
        qtrigger = """
        CREATE TRIGGER datos_sonar_monitor_avl_minute_check_insert
        AFTER INSERT ON datos_sonar_event_location
        FOR EACH ROW
        EXECUTE PROCEDURE datos_sonar_monitor_avl_minute_insert();
        """
        
        cursor.execute(qfunction)
        cursor.execute(qdtrigger)
        cursor.execute(qtrigger)


    #metodo para cron
    @classmethod
    def purge_old(cls, days_keep=1):
        Date = Pool().get('ir.date')
        now = Date.today() - timedelta(days=days_keep)
        records = cls.search([('serie', '<=', now)])
        cls.delete(records)
