# monitor_avl

adiciona nuevos atributos a 'mobiles' de datos_sonar.

### cron mantenimiento pgpathman

se hace uso de pg_pathman para la tabla 'datos_sonar_event_location' el cual segmenta los registros
en tablas 'datos_sonar_event_location_<ano>_<month>', se debe correr esta tarea almenos una vez al mes
para reconstruir los triggers.

adicionar lanzador con los siguientes parametros:

  * **model** : datos_sonar_monitor_avl.maintenance
  * **function** : machete_pgpathman
  * **args** : []

### cron purgar datos

Es importante poder llamar este cron diariamente.

adicionar lanzador con los siguientes parametros:

  * **model** : datos_sonar_monitor_avl.minute
  * **function** : purge_old
  * **args** : [] adicionalmente se puede indicar el numero de días a mantener en base de datos

