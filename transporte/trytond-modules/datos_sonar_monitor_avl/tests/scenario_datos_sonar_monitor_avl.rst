================================
Datos Sonar Monitor Avl Scenario
================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install datos_sonar_monitor_avl::

    >>> config = activate_modules('datos_sonar_monitor_avl')
