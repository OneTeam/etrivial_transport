# This file is part of datos_sonar_monitor_avl.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


try:
    from trytond.modules.datos_sonar_monitor_avl.tests.test_datos_sonar_monitor_avl import suite
except ImportError:
    from .test_datos_sonar_monitor_avl import suite

__all__ = ['suite']
