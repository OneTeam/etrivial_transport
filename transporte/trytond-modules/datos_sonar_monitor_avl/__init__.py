# This file is part of datos_sonar_monitor_avl.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .monitor_avl import *
from .mobile import *
__all__ = ['register']



def register():
    Pool.register(
        Mobile,
        MonitorAvlMaintenance,
        MonitorAvlMinute,
        module='datos_sonar_monitor_avl', type_='model')
    Pool.register(
        module='datos_sonar_monitor_avl', type_='wizard')
    Pool.register(
        module='datos_sonar_monitor_avl', type_='report')
