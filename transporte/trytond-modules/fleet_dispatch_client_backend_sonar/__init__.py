# This file is part of fleet_dispatch_client_backend_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import routes

__all__ = ['register']


def register():
    Pool.register(
        module='fleet_dispatch_client_backend_sonar', type_='model')
    Pool.register(
        module='fleet_dispatch_client_backend_sonar', type_='wizard')
    Pool.register(
        module='fleet_dispatch_client_backend_sonar', type_='report')
