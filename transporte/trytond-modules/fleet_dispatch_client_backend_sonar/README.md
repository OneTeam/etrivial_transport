# fleet_dispatch_client_backend_sonar

Este backend exporta la url **/fleet_dispatch_client/webhook/sonar/dispatch/<state>** para notificar
despachos a sonar.

Actualmente responde al **<state>** **dispatched** que es cuando se ha realizado un despachos.
