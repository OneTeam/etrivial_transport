# This file is part of fleet_dispatch_client_backend_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import pickle

from trytond.transaction import Transaction
from trytond.wsgi import app
from werkzeug.exceptions import abort, Response

from trytond.protocols.wrappers import with_pool, with_transaction, user_application
client_application = user_application('fleet_dispatch_client')


# Se mantiene compatibilidad mientras se migran los clientes
@app.route('/<database_name>/fleet_dispatch_client/webhook/dispatch/<state>', methods=['POST'])
@with_pool
@with_transaction()
@client_application
def dispatch(request, pool, state):
    request_body = request.get_data(cache=False)
    payload = pickle.loads(request_body)
    if state == 'test':
        return Response(
            pickle.dumps({'sonar_id': 9999, '_pkey': payload['_pkey'], '_model_': payload['_model_']}),
            content_type='application/octet-stream')

    if state == 'dispatched':
        return Response(do_dispatch(pool, payload),
                        content_type='application/octet-stream')

    if state == 'canceled':
        return Response(do_cancel(pool, payload),
                        content_type='application/octet-stream')

    return Response(pickle.dumps({'_pkey': payload['_pkey'], '_model_': payload['_model_']}),
                    content_type='application/octet-stream')

@app.route('/<database_name>/fleet_dispatch_client/webhook/sonar/dispatch/<state>', methods=['POST'])
@with_pool
@with_transaction()
@client_application
def dispatch(request, pool, state):
    request_body = request.get_data(cache=False)
    payload = pickle.loads(request_body)
    if state == 'test':
        return Response(
            pickle.dumps({'sonar_id': 9999, '_pkey': payload['_pkey'], '_model_': payload['_model_']}),
            content_type='application/octet-stream')

    if state == 'dispatched':
        return Response(do_dispatch(pool, payload),
                        content_type='application/octet-stream')
    if state == 'canceled':
        return Response(do_cancel(pool, payload),
                        content_type='application/octet-stream')

    return Response(pickle.dumps({'_pkey': payload['_pkey'], '_model_': payload['_model_']}),
                    content_type='application/octet-stream')

def do_cancel(pool, payload):
    Dispatch = pool.get('public_transport.dispatch')
    Mobiles = pool.get('datos_sonar.mobiles')
    Configuration = pool.get("datos_sonar.configurations")
    configuration = Configuration.get_singleton()

    # se toma ejemplo de datos_sonar/generic_class.py#WsdlConection.Set_ITCancel
    params={
        'mId':payload['vehicle']['sonar_id'],
        'regId': payload['sonar_id'],
        'comments': ''
    }

    mobil = Mobiles.get_ids_auth(mId=payload['vehicle']['sonar_id'])
    response = Dispatch.request(configuration.wsdl_auth,
                       request='SET_ItCancel',
                       params=params,
                       auth=mobil['sonar_auth'],
                       url=configuration.wsdl_url)
    
    if response.status == 'OK':
        return pickle.dumps({'sonar_cancelled': True, '_pkey': payload['_pkey'], '_model_': payload['_model_']})
    else:
        return pickle.dumps({'sonar_cancelled': False, '_pkey': payload['_pkey'], '_model_': payload['_model_']})

def do_dispatch(pool, payload):
    Dispatch = pool.get('public_transport.dispatch')
    Mobiles = pool.get('datos_sonar.mobiles')
    Configuration = pool.get("datos_sonar.configurations")
    configuration = Configuration.get_singleton()

    driver_id_sonar = (payload['driver'] or {}).get('id_sonar', '')
    route_id_sonar = payload['route']['sonar_id']
    init_time = protocol.as_utc(payload['init_time']).replace(microsecond=0, tzinfo=None)
    params={
        'mId':payload['vehicle']['sonar_id'],
        'Itinerary': route_id_sonar,
        'DrvId': driver_id_sonar,
        'UTC_datetime': init_time,
    }
    mobil = Mobiles.get_ids_auth(mId=payload['vehicle']['sonar_id'])
    response = Dispatch.request(configuration.wsdl_auth,
                       request='SET_ItAssign_v2',
                       params=params,
                       auth=mobil['sonar_auth'],
                       url=configuration.wsdl_url)
    
    if response.status == 'OK':
        return pickle.dumps({'sonar_id': response.regId, '_pkey': payload['_pkey'], '_model_': payload['_model_']})
    else:
        return pickle.dumps({'_pkey': payload['_pkey'], '_model_': payload['_model_']})
