# This file is part of fleet_dispatch_client_backend_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker


class FleetDispatchClientBackendSonarTestCase(ModuleTestCase):
    'Test Fleet Dispatch Client Backend Sonar module'
    module = 'fleet_dispatch_client_backend_sonar'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            FleetDispatchClientBackendSonarTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_fleet_dispatch_client_backend_sonar.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
