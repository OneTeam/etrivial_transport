============================================
Fleet Dispatch Client Backend Sonar Scenario
============================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install fleet_dispatch_client_backend_sonar::

    >>> config = activate_modules('fleet_dispatch_client_backend_sonar')
