# This file is part of fleet_dispatch_client_backend_sonar.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


try:
    from trytond.modules.fleet_dispatch_client_backend_sonar.tests.test_fleet_dispatch_client_backend_sonar import suite
except ImportError:
    from .test_fleet_dispatch_client_backend_sonar import suite

__all__ = ['suite']
