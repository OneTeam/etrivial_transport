# This file is part of holidays_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Created on 12/12/2018

@author: francisco.garcia@santra.com.co
'''
import logging

from datetime import date,timedelta

from trytond.modules.holidays_co.festivos_colombia import festivos_colombia
from trytond.model import Model

__all__ = ['Holidays']

logger = logging.getLogger(__name__)

class Holidays(Model):
    """Festivos"""
    
    __name__ = 'holidays_co.holidays'
    
    @classmethod
    def is_holiday(cls,date_):
        if festivos_colombia.es_festivo(date_):
            return True;
        else:
            return None;
    
    @classmethod
    def how_many(cls,init_date,end_date):
        return festivos_colombia.cuantos(init_date,end_date)
    
    @classmethod
    def to_list_range(cls,init_date,end_date):
        return festivos_colombia.lista_rango(init_date,end_date)
    
    @classmethod
    def how_ordinaries(cls,init_date,end_date):
        return festivos_colombia.cuantos_dias_habiles(init_date,end_date)
    
    @classmethod
    def how_saturdays(cls,init_date,end_date):
        return festivos_colombia.cuantos_sabados(init_date,end_date)
    
    @classmethod
    def how_holidays_and_sundays(cls,init_date,end_date):
        return festivos_colombia.cuantos_domingos_y_festivos(init_date,end_date)
    
    
    @classmethod
    def test(cls):
        logger.info("holidays_co test")
        k = festivos_colombia()
        _date = date.today().replace(day=1).replace(month=1)
        logger.info("holidays current year ({})".format(_date.year))
        for x in range(0,365):
            if k.es_festivo(_date + timedelta(days=x)):
                logger.info((_date + timedelta(days=x)).strftime("%d/%m/%y"))
    
