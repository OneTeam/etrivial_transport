# This file is part of holidays_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""
Pruebas
@todo probar año viciesto
>>> festivos_colombia.es_festivo(date(2020,1,1))
True
>>> festivos_colombia.es_festivo(date(2019,1,1))
True
>>> festivos_colombia.es_festivo(date(2019,1,2))
False
>>> sorted(festivos_colombia.lista(2019,sin_domingos=True))
[datetime.date(2019, 1, 1), datetime.date(2019, 1, 7), datetime.date(2019, 3, 25), datetime.date(2019, 4, 18), datetime.date(2019, 4, 19), datetime.date(2019, 5, 1), datetime.date(2019, 6, 3), datetime.date(2019, 6, 24), datetime.date(2019, 7, 1), datetime.date(2019, 7, 20), datetime.date(2019, 8, 7), datetime.date(2019, 8, 19), datetime.date(2019, 10, 14), datetime.date(2019, 11, 4), datetime.date(2019, 11, 11), datetime.date(2019, 12, 25)]
>>> festivos_colombia.cuantos_domingos(date(2019,4,1), date(2019,4,30))
4
>>> festivos_colombia.cuantos(date(2019,4,1), date(2019,4,30))
2
>>> festivos_colombia.cuantos_domingos_y_festivos(date(2019,4,1), date(2019,4,30))
6
>>> festivos_colombia.cuantos_sabados(date(2019,4,1), date(2019,4,30))
4
>>> festivos_colombia.cuantos_sin_domingos(date(2019,4,1), date(2019,4,30))
2
>>> festivos_colombia.cuantos_dias_habiles(date(2019,4,1), date(2019,4,30))
20
>>> festivos_colombia.cuantos_dias_habiles_con_sabados(date(2019,4,1), date(2019,4,30))
24
>>> #pruebas dos meses mayo junio
>>> festivos_colombia.cuantos(date(2019,5,1), date(2019,6,30))
3
>>> festivos_colombia.cuantos_sin_domingos(date(2019,5,1), date(2019,6,30))
3
>>> festivos_colombia.cuantos_domingos_y_festivos(date(2019,5,1), date(2019,6,30))
12
>>> festivos_colombia.cuantos_sabados(date(2019,5,1), date(2019,6,30))
9
>>> festivos_colombia.cuantos_dias_habiles(date(2019,5,1), date(2019,6,30))
40
>>> festivos_colombia.cuantos_dias_habiles_con_sabados(date(2019,5,1), date(2019,6,30))
49
>>> festivos_colombia.cuantos_domingos_y_festivos(date(2019,5,1), date(2019,6,30))
12
>>> festivos_colombia.cuantos_sabados(date(2019,5,1), date(2019,6,30))
9
"""

from dateutil.easter import easter
from datetime import date, timedelta

import logging
logger = logging.getLogger(__name__)

def siguiente_lunes(date):
    dias_para_lunes = ['Monday','Sunday','Saturday','Friday','Thursday','Wednesday','Tuesday']
    return date + timedelta(days=dias_para_lunes.index(date.strftime("%A")))

def fecha_basada_en_pascua(ano,dias):
    return easter(ano) + timedelta(days=dias)

def limpiar_lista(lista):
    return [x for x in lista if x]

class Festivo(object):
    tipos = ['fijo','siguiente lunes','basado en pascua','siguiente lunes basado en pascua']
    
    def __init__(self,nombre,tipo,mes=None,dia=None,dias_de_pascua=None):
        if tipo not in self.tipos:
            raise Exception ('Tipo de festivo no valido')
        self.nombre = nombre
        self.tipo = tipo
        self.mes = mes
        self.dia = dia
        self.dias_de_pascua = dias_de_pascua
        self.validar_tipo()

    def validar_tipo(self):
        if self.tipo == 'fijo' or self.tipo == 'siguiente lunes' or self.tipo == 'siguiente lunes basado en pascua':
            if (not isinstance(self.dia,int)) or (not isinstance(self.mes,int)):
                raise Exception ('Error en dia, mes y tipo de festivo')
        elif self.tipo == 'basado en pascua' or self.tipo == 'siguiente lunes basado en pascua':
            if not isinstance(self.dias_de_pascua,int):
                raise Exception ('Error en dias de pascua y tipo de festivo')
    
    def fecha(self,ano):
        if self.tipo == 'fijo':
            return date(ano,self.mes,self.dia)
        elif self.tipo == 'siguiente lunes':
            return siguiente_lunes(date(ano,self.mes,self.dia))
        elif self.tipo == 'basado en pascua':
            return fecha_basada_en_pascua(ano, self.dias_de_pascua)
        elif self.tipo == 'siguiente lunes basado en pascua':
            return siguiente_lunes( fecha_basada_en_pascua(ano, self.dias_de_pascua) )


class FestivosColombia(object):
    def __init__(self,festivos):
        self.festivos=[]
        for festivo in festivos:
            if isinstance(festivo, Festivo):
                self.festivos.append(festivo)
            else:
                raise Exception('Festivo invalido: {}'.format(festivo))
    
    def lista(self,ano,sin_domingos=False):
        """devuelve la lista de festivos del año dado"""
        r = [x.fecha(ano) for x in self.festivos]
        if sin_domingos:
            for x in r:
                if int(x.strftime('%w')) == 0:
                    r.remove(x)
        return list(set(r))
    
    def lista_fiesta(self,ano,sin_domingos=False):
        """devuelve una lista de tuplas con la fecha del
        festivo y el nombre de la fiesta para el año dado
        """
        return [(festivo.fecha(ano),festivo.nombre) for festivo in self.festivos]
    
    def __str__(self):
        imprimir = ""
        for festivo in self.festivos:
            imprimir += festivo.nombre + " (" + festivo.tipo + ")\n"
        return imprimir
    
    def es_festivo(self,date_):
        """Indica si la fecha pasada es un festivo"""
        if date_ in self.lista(date_.year):
            return True
        else:
            return False
    
    def lista_rango(self,fecha_inicial,fecha_final,sin_domingos=False):
        """Devuelve las fechas de festivos entre 2 fechas dadas"""
        if fecha_inicial > fecha_final:
            fecha_inicial,fecha_final = fecha_final,fecha_inicial
        anos = [x for x in range(fecha_inicial.year,fecha_final.year+1)]
        festivos = []
        extraer = []
        for ano in anos:
            festivos.extend(self.lista(ano,sin_domingos=sin_domingos))
        for x in festivos:
            if not(fecha_inicial <= x <= fecha_final):
                extraer.append(x)
        return sorted(list(set(festivos) - set(extraer)))
        
    def cuantos(self,fecha_inicial,fecha_final):
        """Devuelve la cantidad de festivos entre 2 fechas"""
        return len(self.lista_rango(fecha_inicial, fecha_final))
    
    def festivos_sin_domingos(self,fecha_inicial,fecha_final):
        """Devuelve los festivos entre 2 fechas sin domingos"""
        festivos = self.lista_rango(fecha_inicial, fecha_final)
        for dx in range(0,len(festivos) - 1):
            if festivos[dx].strftime('%w') == 0:
                festivos[dx] = None
        festivos = limpiar_lista(festivos)
        return festivos
    
    def cuantos_sin_domingos(self,fecha_inicial,fecha_final):
        """Devuelve la cantidad de festivos entre 2 fechas sin contar domingos"""
        return len(self.festivos_sin_domingos(fecha_inicial,fecha_final))
    
    def dias(self,dia_de_la_semana,fecha_inicial,fecha_final):
        """Devuelve una lista con los dias en el rango de fechas dado.
        
        @param dia_de_la_semana: es un entero donde 0 es domingo y 6 sábado
        """
        if fecha_final == fecha_inicial:
            return []
        if fecha_inicial > fecha_final:
            fecha_inicial,fecha_final = fecha_final,fecha_inicial
        
        if int(fecha_inicial.strftime('%w')) != dia_de_la_semana:
            if dia_de_la_semana > int(fecha_inicial.strftime("%w")):
                fecha_inicial = fecha_inicial + timedelta( (dia_de_la_semana - int(fecha_inicial.strftime("%w"))))
            else:
                fecha_inicial = fecha_inicial + timedelta( 7 - (int(fecha_inicial.strftime("%w")) - dia_de_la_semana))        
        
        dias = (fecha_final - fecha_inicial).days
        dias_r = [fecha_inicial + timedelta(days=x) for x in range(0, dias + 1, 7)]
        return dias_r
    
    def domingos(self,fecha_inicial,fecha_final):
        """Devuelve una lista con los domingos en el rango de fechas dado"""
        return self.dias(0,fecha_inicial,fecha_final)
    
    def cuantos_domingos(self,fecha_inicial,fecha_final):    
        """Devuelve la cantiad de domingos en el rango de fechas dado"""
        return len(self.domingos(fecha_inicial, fecha_final))
    
    def sabados(self,fecha_inicial,fecha_final):
        return self.dias(6,fecha_inicial,fecha_final)
    
    def cuantos_sabados(self,fecha_inicial,fecha_final):
        return len(self.dias(6,fecha_inicial,fecha_final))
    
    def cuantos_dias_habiles(self,fecha_inicial,fecha_final):
        """devuelve la cantidad de dias hábiles entre 2 fechas, incluyendolas"""
        if fecha_inicial > fecha_final:
            fecha_inicial,fecha_final = fecha_final,fecha_inicial
        dias = (fecha_final - fecha_inicial).days + 1
        sabados = self.cuantos_sabados(fecha_inicial, fecha_final)
        domingos = self.cuantos_domingos(fecha_inicial, fecha_final)
        festivos = self.cuantos_sin_domingos(fecha_inicial, fecha_final)
        return dias - sabados - domingos - festivos
    
    def cuantos_dias_habiles_con_sabados(self,fecha_inicial,fecha_final):
        """devuelve la cantidad de dias hábiles, cuando incluyen el sábado,
        entre 2 fechas"""
        if fecha_inicial > fecha_final:
            fecha_inicial,fecha_final = fecha_final,fecha_inicial
        dias = (fecha_final - fecha_inicial).days + 1
        domingos = self.cuantos_domingos(fecha_inicial, fecha_final)
        festivos = self.cuantos_sin_domingos(fecha_inicial, fecha_final)
        return dias - domingos - festivos
    
    def cuantos_domingos_y_festivos(self,fecha_inicial,fecha_final):
        """Devuelve la cantidad de domingos y festivos entre 2 fechas"""
        domingos = self.cuantos_domingos(fecha_inicial, fecha_final)
        festivos = self.cuantos_sin_domingos(fecha_inicial, fecha_final)
        return domingos + festivos
    
#lista de tuplas (nombre,tipo,mes,dia,a_dias_de_pascua,)
#faltan domingo de ramos, domingo de pascua, 
dias_festivos_colombia = [
('Año nuevo','fijo',1,1,0),
('Día de los Reyes Magos','siguiente lunes',1,6,0),
('Día de San José','siguiente lunes',3,19,0),
('Jueves Santo','basado en pascua',0,0,-3),
('Viernes Santo','basado en pascua',0,0,-2),
('Día del Trabajo','fijo',5,1,0),
('Ascensión del Señor','siguiente lunes basado en pascua',0,0,43),
('Corphus Christi','siguiente lunes basado en pascua',0,0,64),
('Sagrado Corazón de Jesús','siguiente lunes basado en pascua',0,0,71),
('San Pedro y San Pablo','siguiente lunes',6,29,0),
('Día de la Independencia','fijo',7,20,0),
('Batalla de Boyacá','fijo',8,7,0),
('La Asunción de la Virgen','siguiente lunes',8,15,0),
('Día de la Raza','siguiente lunes',10,12,0),
('Todos los Santos','siguiente lunes',11,1,0),
('Independencia de Cartagena','siguiente lunes',11,11,0),
('Día de la Inmaculada Concepción','fijo',12,8,0),
('Día de Navidad','fijo',12,25,0),
]

festivos_colombia = FestivosColombia([Festivo(*x) for x in dias_festivos_colombia])   


if __name__ == "__main__":
    import doctest
    doctest.testmod()
