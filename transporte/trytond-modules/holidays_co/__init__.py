# This file is part of holidays_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .holidays import *
__all__ = ['register']


def register():
    Pool.register(
        Holidays,
        module='holidays_co', type_='model')
    Pool.register(
        module='holidays_co', type_='wizard')
    Pool.register(
        module='holidays_co', type_='report')
