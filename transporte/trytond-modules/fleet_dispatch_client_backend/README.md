# fleet_dispatch_client_backend

este modulo exporta un **user_application** para trytond, por lo tanto para hacer uso de la
API se requiere una cabezera *Authorization Bearer* con la *api_key* del usuario.

## PROVISIONAR

### parametros de configuracion en trytond.ini

Se puede personalizar parametros de configuracion atraves del archivo de configuracion de *trytond.ini*,
~~~ini
[fleet_dispatch_client_backend]
server_url = mi.servidor:8000
~~~

**server_url** url del servidor para los clientes, por defecto se usa 'web.listen'.

## API

### GET: /<database_name>/fleet_dispatch_client/client/pull/<model>

 * **<database_name>**: nombre de base de datos.
 * **<model>**: nombre del modelo a retornar.
 
#### Retorna

serializado con *pickle* una lista de *dict*, vease *fleet_dispatch_client.Db*.
~~~json
{
 '_pkey': ...,
 '_fkey': ...,
 '_model_': ...,
 'id': ...
}
~~~

### POST: /<database_name>/fleet_dispatch_client/client/push/<pkey>

 * **body**: serializado con *pickle* vease *fleet_dispatch_client.Db*.
 * **<database_name>**: nombre de base de datos
 * **<pkey>**: identificador **pkey** de registro.
 
#### Retorna

serializado con *pickle* el mismo registro enviado en **body**, pero actualizado
los campos **_dirty** y **_fkey**.

### POST: /<database_name>/fleet_dispatch_client/webhook/dispatch/<state>

 * **body** serializado con *pickle* un registro *{'_pkey': ..., '_model_': ...}*
 * **<database_name>**: nombre de base de datos
 * **<state>**: estado del despatcho *finalize*,*canceled*,*dispatched*, o *test* para pruebas.

#### Retorna

serializa con *pickle* un registro:
~~~json
{
 'sonar_id': ... ,
 '_pkey': ...,
 '_model_': ....
}
~~~

## DESARROLLO

Se puede indicar a los modelos a sincronizar se almacene el 'pkey' que es la llave primaria en el cliente,
y el 'routes.py(sync_record)' confirmaria la no existencia del registro, adicionando los atributos:

~~~python
clas ...(metaclass=PoolMeta):
...
    fleet_dispatch_client_pkey = fields.Char('Fleet Dispatch Client Pkey', states={'readonly': True})
    fleet_dispatch_client_machine = fields.Char('Fleet Dispatch Client Machine', states={'readonly': True})
~~~python
