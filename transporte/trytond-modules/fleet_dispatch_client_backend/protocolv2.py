# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import gzip as compress
import base64
import pickle
import datetime
import operator
import functools


class UrlHeartBeat:
    def __init__(self, base, machine):
        self.base = base
        self.machine = machine

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/heartbeat".format(base=self.base, machine=self.machine)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/heartbeat'


class UrlProvision:
    def __init__(self, base, machine, token):
        self.base = base
        self.machine = machine
        self.token = token

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/provision/{token}".format(base=self.base, machine=self.machine, token=self.token)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/provision/<token>'

class UrlPush:
    def __init__(self, base, machine, pkey=None):
        self.base = base
        self.pkey = pkey
        self.machine = machine
        
    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/push/{pkey}".format(machine=self.machine,base=self.base, pkey=self.pkey)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/push/<pkey>'


class UrlPushWithRelations:
    def __init__(self, base, machine):
        self.base = base
        self.machine = machine
        
    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/push_with_relations/".format(machine=self.machine,base=self.base)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/push_with_relations/'


class UrlPull:
    def __init__(self, base, machine, model=None, date=None):
        self.base = base
        self.model = model
        self.machine = machine
        self.date = date or ""

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/pull/v2/{model}?date={date}".format(machine=self.machine, base=self.base, model=self.model, date=self.date)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/pull/v2/<model>'


def encode(data):
    if isinstance(data, dict):
        val = pickle.dumps(data)
    else:
        val = data
    return base64.encodebytes(compress.compress(val))

def encode_binary(data):
    if isinstance(data, dict):
        val = pickle.dumps(data)
    else:
        val = data
    return compress.compress(val)
    
#TODO: no deberia importar si es 'dict' u otro tipo,
#pero 'getattr' no obtiene los valores de los descriptores
#de objetos de tryton.
def serialize(records, model, encoder=encode):
    vals = marshal(model, records)
    return encoder(pickle.dumps(vals))


def fields_names_for_model(model):
    fields, field_relation, _virtual = fields_for_model(model)

    for rel_name, rel_fields in field_relation.items():
        rfields = [rel_name]
        fields.append(rel_name)
        for rfield in rel_fields['fields']:
            if rfield != rel_fields['pkey_field']:
                fields.append(".".join([rel_name, rfield]))

    return fields


def marshal(main_model, records):
    vals = []
    if not isinstance(records, list):
        records = [records]

    fields, fields_relations, _virtual = fields_for_model(main_model)
    for record in records:
        val = {'_model_': main_model, '_fkey': record['id']}
        for field in fields:
            if field in record:
                val[field] = format_field(record[field])

        for rel_key, field_rel in fields_relations.items():
            if rel_key in record:
                val[rel_key] =  {}
                if record[rel_key]:
                    val[rel_key][field_rel['pkey_field']] = record[rel_key]
                    val[rel_key]['_fkey'] = record[rel_key]

                val[rel_key]['_model_'] = field_rel['__model__']
                for field in field_rel['fields']:
                    if field != field_rel['pkey_field']:
                        rkey = ".".join([rel_key, field])
                        if rkey in record and record[rkey]:
                            val[rel_key][field] =  record[rkey]
                    
        vals.append(val)
    return vals


# se debe mantener hasta estar todos actualizados
# a ultima version
def decode(data):
    if isinstance(data, bytes):
        return compress.decompress(base64.decodebytes(data))
    return compress.decompress(base64.decodebytes(str.encode(data)))

def decode_binary(data):
    if isinstance(data, bytes):
        return compress.decompress(data)
    return compress.decompress(str.encode(data))

def unserialize(data, decoder=decode):
    return pickle.loads(decoder(data))

def format_field(field):
    # HACK: es necesario asignar la zona para
    # que python realice cambios de horarios correctamente.
    # esto es suponiendo indica que el servidor envia la hora en UTC y 
    # `proteus`no hace conversion.
    if isinstance(field, datetime.datetime):
        if field.tzname() == None or field.tzname() == 'UTC':
            return field.replace(tzinfo=datetime.timezone.utc)
        else:
            return field.astimezone(datetime.timezone.utc)
    return field


def fields_for_model(model):
    mapping = {
        'fleet_dispatch_client_backend.gps_log': ([
            'id',
            'rec_name',
            'model',
            'model_id',
            'model_pkey',
            'latitude',
            'longitude',
            'message'
        ], {}, False),
        
        'fleet_dispatch_client_backend.trips_daily-reason_cancel': ([
            'id',
            'rec_name',
            'name'
        ], {}, False),

        'fleet_dispatch_client_backend.trips_daily-notes': ([
            'id',
            'rec_name',
            'name'
        ], {}, False),
        
        'fleet_dispatch_client_backend.trips_daily': ([
            'id',
            'rec_name',
            'init_counter',
            'end_counter',
            'date',
            'quantity_dispatches',
            'open_confirm_check',
            'open_confirm_check_note',
            'user_confirm_check',
            'user_confirm_check_note',
            'cancel_reason',
            'cancel_note',
        ], {
            'vehicle': {
                '__model__': 'fleet.vehicle',
                'pkey_field': 'id',
                'fields': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate']
            },
            'group_route': {
                '__model__': 'public_transport.group_route',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'code', 'name', 'active']},
        }, False),
        
        'public_transport.dispatch.quantity_for_day': ([
            'id',
            'rec_name',
            'init_counter',
            'end_counter',
            'quantity_dispatches',

            #QuantityForDayMixin
            'date',
        ], {
            'vehicle': {'__model__': 'fleet.vehicle',
                        'pkey_field': 'id',
                        'fields': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate']},
            'group_route': {'__model__': 'public_transport.group_route',
                            'pkey_field': 'id',
                            'fields': ['id', 'rec_name', 'code', 'name', 'active']},
        }, False),
        
        'public_transport.dispatch.annotation_dispatch': ([
            'id',
            ], {
                'dispatch': {'__model__': 'public_transport.dispatch',
                             'pkey_field': 'id',
                             'fields': ['id', 'rec_name']},
                'annotation': {'__model__': 'public_transport.dispatch.annotation',
                               'pkey_field': 'id',
                               'fields': ['id', 'rec_name']},
            }, False),
        'public_transport.dispatch.annotation': ([
            'id',
            'rec_name',
            'name',
            'code',
            'description',
            'to_dispatcher'
        ], {}, True),
        
        'fleet.vehicle.passenger_counter.issue': ([
            'id',
            'rec_name',
            'name',
            'time',
            'note',
            ], {
                'vehicle': {'__model__': 'fleet.vehicle',
                            'pkey_field': 'id',
                            'fields': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate']},
                'init_counter': {
                    '__model__': 'fleet.vehicle.passenger_counter',
                    'pkey_field': 'id',
                    'fields': ['id', 'counter', 'time']
                },
                'end_counter': {
                    '__model__': 'fleet.vehicle.passenger_counter',
                    'pkey_field': 'id',
                    'fields': ['id', 'counter', 'time']
                },
                'type': {
                    '__model__': 'fleet.vehicle.passenger_counter.issue.type',
                    'pkey_field': 'id',
                    'fields': ['id', 'rec_name', 'name']
                }
            }, False),
        
        'fleet.vehicle.passenger_counter.issue.type': ([
            'id',
            'rec_name',
            'name',
            'affects_passangers',
            ], {}, True),

        'fleet_dispatch_client_backend.profile': ([
            'id',
            'rec_name',
            'name',
            'code',
            'server_url',
        ], {
            'user_application': {
                '__model__': 'res.user.user_application',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'key', 'user.id']
                },
        }, True),
        
        'public_transport.group_route': ([
            'id',
            'rec_name',
            'name',
            'code',
            'active'
        ], {}, False),
        
        'public_transport.route': ([
            'id',
            'rec_name',
            'name',
            'code',
            'active',
            'sonar_id',
            'max_passangers_for_lap'
        ], {
            'group': {
                '__model__': 'public_transport.group_route',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'code', 'name', 'active']
            }
        }, False),

        'fleet.vehicle': ([
            'id',
            'rec_name',
            'sonar_id',
            'internal_code',
        ], {
            'routes': {
                '__model__': 'public_transport.group_route',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'code']
            }
        }, False),
        
        'fleet.vehicle.passenger_counter': ([
            'id',
            'counter',
            'time',
            'note'
        ], {
            'vehicle': {
                '__model__': 'fleet.vehicle',
                'pkey_field': 'id',
                'fields': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate']
            }
        }, False),
        
        'company.employee': ([
            'id',
            'is_driver',
            'rec_name',
            'id_sonar'
        ], {
            'party': {
                '__model__': 'party.party',
                'pkey_field': 'id',
                'fields': ['id', 'name', 'rec_name']
            }
        }, True),

        'public_transport.dispatch': ([
            'id',
            'turn',
            'init_time',
            'end_time',
            'sonar_id',
            'sonar_cancelled',
            'issue_register_note',
            'canceled_cause',
            'notes',
            'state',
            'dispatched_time',
            'done_time',
            'canceled_time',
        ], {
            'dispatched_uid': {
                '__model__': 'res.user',
                'pkey_field': 'id',
                'fields': ['id']
            },
            'done_uid': {
                '__model__': 'res.user',
                'pkey_field': 'id',
                'fields': ['id']
            },
            'canceled_uid': {
                '__model__': 'res.user',
                'pkey_field': 'id',
                'fields': ['id']
            },
            'group_route': {
                '__model__': 'public_transport.group_route',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'code', 'name', 'active']
            },
            'route': {
                '__model__': 'public_transport.route',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name', 'code', 'sonar_id', 'name', 'max_passangers_for_lap']
            },
            'vehicle': {
                '__model__': 'fleet.vehicle',
                'pkey_field': 'id',
                'fields': ['id', 'internal_code', 'sonar_id', 'licence_plate', 'rec_name']
            },
            'driver': {
                '__model__': 'company.company',
                'pkey_field': 'id',
                'fields': ['id', 'name']
            },
            'init_counter': {
                '__model__': 'fleet.vehicle.passenger_counter',
                'pkey_field': 'id',
                'fields': ['id', 'counter', 'time']
            },
            'end_counter': {
                '__model__': 'fleet.vehicle.passenger_counter',
                'pkey_field': 'id',
                'fields': ['id', 'counter', 'time']
            },
            'issue_counter': {
                '__model__': 'fleet.vehicle.passenger_counter',
                'pkey_field': 'id',
                'fields': ['id', 'counter', 'time']
            },
            'subtotal_from': {
                '__model__': 'public_transport.dispatch.quantity_for_day',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name']
            },
            'fleet_dispatch_client_trip_daily': {
                '__model__': 'fleet_dispatch_client_backend.trips_daily',
                'pkey_field': 'id',
                'fields': ['id', 'rec_name']
            }
        }, False),
    }
    
    return mapping[model]


def as_utc(field):
    if isinstance(field, datetime.datetime):
        if field.tzname() == None or field.tzname() == 'UTC':
            return field.replace(tzinfo=datetime.timezone.utc)
        else:
            return field.astimezone(datetime.timezone.utc)
    return field
