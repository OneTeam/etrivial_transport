# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.
from trytond.exceptions import UserError
from trytond.model import fields
from trytond.pool import PoolMeta

class Dispatch(metaclass=PoolMeta):
    __name__ = 'public_transport.dispatch'

    fleet_dispatch_client_pkey = fields.Char('Fleet Dispatch Client Pkey', states={'readonly': True})
    fleet_dispatch_client_machine = fields.Char('Fleet Dispatch Client Machine', states={'readonly': True})

    @classmethod
    def delete(cls, records):
        for record in records:
            if hasattr(record, 'fleet_dispatch_client_pkey') and record.fleet_dispatch_client_pkey:
                raise UserError('No se permite eliminar despachos vinculados a un cliente mobil')
        return super(Dispatch, cls).delete(records)

class VehiclePassengerCounter(metaclass=PoolMeta):
    __name__ = 'fleet.vehicle.passenger_counter'
    
    fleet_dispatch_client_pkey = fields.Char('Fleet Dispatch Client Pkey', states={'readonly': True})
    fleet_dispatch_client_machine = fields.Char('Fleet Dispatch Client Machine', states={'readonly': True})

    @classmethod
    def delete(cls, records):
        for record in records:
            if hasattr(record, 'fleet_dispatch_client_pkey') and record.fleet_dispatch_client_pkey:
                raise UserError('No se permite eliminar contadoras vinculadas a un cliente mobil')
        return super(VehiclePassengerCounter, cls).delete(records)

class VehiclePassengerCounterIssue(metaclass=PoolMeta):
    __name__ = 'fleet.vehicle.passenger_counter.issue'
    
    fleet_dispatch_client_pkey = fields.Char('Fleet Dispatch Client Pkey', states={'readonly': True})
    fleet_dispatch_client_machine = fields.Char('Fleet Dispatch Client Machine', states={'readonly': True})


    @classmethod
    def delete(cls, records):
        for record in records:
            if hasattr(record, 'fleet_dispatch_client_pkey') and record.fleet_dispatch_client_pkey:
                raise UserError('No se permite eliminar incidencias vinculadas a un cliente mobil')
        return super(VehiclePassengerCounter, cls).delete(records)

class DispatchesQuantityForDay(metaclass=PoolMeta):
    __name__ = 'public_transport.dispatch.quantity_for_day'
    
    user_confirm_check = fields.Boolean('User Confirm Check?', readonly=True, states={'invisible': True})
    user_confirm_check_note = fields.Text('User Confirm Note', readonly=True, states={'invisible': True})

    @classmethod
    def fleet_client_recalculate(cls, client_vals):
        dispatch_quantity_for_day = cls(client_vals['id'])
        # metodo de QuantityForDayMixin
        cls.write_calculation([dispatch_quantity_for_day])
        
