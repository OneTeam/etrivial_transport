# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.
import secrets

from trytond.model import DeactivableMixin, ModelSQL, ModelView, fields, Unique
from trytond.pyson import Eval
from trytond.config import config
from trytond.transaction import Transaction

class Device(ModelSQL, ModelView):
    'Device'

    __name__ = 'fleet_dispatch_client_backend.device'

    name = fields.Char('Alias', required=True)
    machineid = fields.Char('Machine ID', required=True)
    group_routes = fields.Many2Many('fleet_dispatch_client_backend.device-grouproute', 'device', 'group_route', 'Group Routes', required=True)
    profiles = fields.Many2Many('fleet_dispatch_client_backend.device-profile',  'device', 'profile', 'Profiles',
                                domain=[('user_application.user.groups_routes', 'in', Eval('group_routes', -1))],
                                depends=['group_route'])
    last_heartbeat = fields.DateTime('Last HeartBeat', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Device, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('machineid_key', Unique(table, table.machineid),
             'You can not have to devices with the same machineid')
        ]


class DeviceGroupRoute(ModelSQL, ModelView):
    'Device - GroupRoute'

    __name__ = 'fleet_dispatch_client_backend.device-grouproute'
    device = fields.Many2One('fleet_dispatch_client_backend.device', 'Device')
    group_route = fields.Many2One('public_transport.group_route', 'Group Route')
    

class DeviceProfile(ModelSQL, ModelView):
    'Device - Profile'

    __name__ = 'fleet_dispatch_client_backend.device-profile'
    device = fields.Many2One('fleet_dispatch_client_backend.device', 'Device')
    profile = fields.Many2One('fleet_dispatch_client_backend.profile', 'Profile')
    

class Profile(DeactivableMixin, ModelSQL, ModelView):
    'Profile'

    __name__ = 'fleet_dispatch_client_backend.profile'
    name = fields.Char('Alias', required=True)
    code = fields.Char('Code', required=True)
    server_url = fields.Char('Server Url', required=True)
    user_application = fields.Many2One('res.user.application', 'User Application',
                                       domain=[('state', '=', 'validated')],
                                       required=True)
    
    @classmethod
    def __setup__(cls):
        super(Profile, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('name_key', Unique(table, table.name),
             'You can not have profiles with the same name'),
            ('code_key', Unique(table, table.code),
             'You can not have to profiles with the same code')
        ]

    @staticmethod
    def default_code():
        code = secrets.token_hex(3)
        while Profile.search_count([('code', '=', code)]):
            code = secrets.token_hex(3)
        return code
            
    @staticmethod
    def default_server_url():
        listen = config.get('fleet_dispatch_client_backend', 'server_url') \
            or config.get('web', 'listen')

        return "api://{listen}/{database}".format(
            listen=listen,
            database=Transaction().database.name)
        
class DeviceProvision(ModelSQL, ModelView):
    'Device - Provision'

    __name__ = 'fleet_dispatch_client_backend.device-provision'

    device = fields.Many2One('fleet_dispatch_client_backend.device', 'Device', required=True)
    token = fields.Char('Token', required=True)
    last_sync = fields.DateTime('Last Sync', readonly=True)
    
    @classmethod
    def __setup__(cls):
        super(DeviceProvision, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('device_key', Unique(table, table.device),
             'You can not have more than one provision for device')
        ]

    @staticmethod
    def default_token():
        return secrets.token_hex(8)
