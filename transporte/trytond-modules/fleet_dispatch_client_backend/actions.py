# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

from trytond.model import DeactivableMixin, ModelSQL, ModelView, fields, sequence_ordered
from trytond.pyson import Eval

class DeviceAction(DeactivableMixin, sequence_ordered(), ModelSQL, ModelView):
    'Remote Action for Device'
    __name__ = 'fleet_dispatch_client_backend.device-action'

    device = fields.Many2One('fleet_dispatch_client_backend.device', 'Device',
                             required=True)
    action = fields.Selection([
        ('push_pull', 'Push/Pull'),
        ('push', 'Push'),
        ('pull', 'Solo Pull!!! ojo'),
    ], 'Action', required=True, help='!!Solo Pull!! elimina los datos del dispositivo')



    # notificacion del cliente al finalizar la tarea
    ack = fields.Boolean('ACK', states={'invisible': True})
    completed = fields.Boolean('Completed', states={'invisible': True})

    motive = fields.Char('Motive', states={
        'required': Eval('action').in_(['pull']),
    }, depends=['action'])

    @staticmethod
    def default_sequence():
        return 1

    def do_ack(self):
        self.ack = True
        self.active = False
        self.save()
