# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.exceptions import UserError

class DispatchTrips(metaclass=PoolMeta):
    __name__ = 'public_transport.dispatch'

    fleet_dispatch_client_trip_daily = fields.Many2One('fleet_dispatch_client_backend.trips_daily',
                                                       'Trip Daily',
                                                       states={'readonly': True})

class TripsDaily(ModelSQL, ModelView):
    'Trips - Daily'
    __name__ = 'fleet_dispatch_client_backend.trips_daily'

    group_route = fields.Many2One('public_transport.group_route', 'Ruta',
                                  readonly=True)
    date = fields.Date('Fecha',
                       readonly=True)

    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              ondelete='RESTRICT',
                              readonly=True)

    quantity_dispatches = fields.Integer('Viajes',
                                         help="Cantida de viajes",
                                         readonly=True)

    dispatches = fields.One2Many(
        'public_transport.dispatch', 'fleet_dispatch_client_trip_daily',
        'Despachos')

    init_counter = fields.Integer(
        'Registradora Inicial',
        help="La registradora anterior a la primera del día "
        "(Generalmente la del día anterior). Este valor es "
        "Estático y esta almacenado en base de datos",
        readonly=True)
    
    end_counter = fields.Integer(
        'Registradora Final',
        readonly=True,
        help=" Este valor es Estático y esta almacenado en base de datos")
    
    open_confirm_check = fields.Boolean('User Confirm Open?', readonly=True)
    open_confirm_check_note = fields.Text('User Open Note', readonly=True)
    
    user_confirm_check = fields.Boolean('User Confirm Check?', readonly=True)
    user_confirm_check_note = fields.Text('User Confirm Note', readonly=True)

    cancel_reason = fields.Char('Cancel Reason')
    cancel_note = fields.Text('Cancel Note')


class TripsDailyNotes(ModelSQL, ModelView):
    'TripsDaily - Notes'
    __name__ = 'fleet_dispatch_client_backend.trips_daily-notes'

    name = fields.Char('Name')

class TripsDailyReasonCancel(ModelSQL, ModelView):
    'TripsDaily - Notes'
    __name__ = 'fleet_dispatch_client_backend.trips_daily-reason_cancel'

    name = fields.Char('Name')
