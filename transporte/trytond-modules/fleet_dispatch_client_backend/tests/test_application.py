# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from contextlib import contextmanager

import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool

import urllib.request

import application


class ApplicationTestCase(ModuleTestCase):
    'Test fleet_dispatch_client_backend module'
    module = 'fleet_dispatch_client_backend'

    @with_transaction()
    def test_webhook_dispatch(self):
        'Webhook dispatch'
        pool = Pool()
        ret = application.do_dispatch(pool, {'driver': {'id_sonar': 9999},
                                             'route': {'sonar_id': 9999},
                                             'vehicle': {'sonar_id': 9999},
                                             'init_time': '2018-12-01 99:99'})
        self.assert_(False)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ApplicationTestCase))
    return suite

