# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.
from timeit import default_timer as timer
import json
import pickle
import base64
import bz2
from datetime import datetime, timedelta
import logging
logger = logging.getLogger(__name__)
import functools
import traceback

from trytond.transaction import Transaction
from trytond.wsgi import app
from werkzeug.exceptions import abort, Response

from trytond.protocols.wrappers import with_pool, with_transaction, user_application
client_application = user_application('fleet_dispatch_client')

from . import protocol
from . import protocolv2

__all__ = ['push', 'pull']


# DERIVADO DE: datos_sonar/datos_sonar_lib.py(colombia_date2utctime)
def colombia_datetime2utctime(date_):
    """Devuelve el tiempo inicial y final en utc para una fecha colombiana"""

    date_ = date_ - timedelta(hours=5) #America/Bogota
    init_time = datetime.fromordinal(date_.toordinal()) + timedelta(hours=5)
    end_time = init_time + timedelta(hours=24) - timedelta(seconds=1)
    return init_time, end_time

def get_model_from_pool(pool, model):
    return pool.get(model)

def _build_params(pool, record, machine):
    """
    Tomado de 'fleet_dispatch_client'
    """
    
    nrecord = {}
    for k, v in record.items():
        if isinstance(v, dict):
            if '_ondelete_' in v:
                if v['_ondelete_'] == 'SET NULL':
                    nrecord[k] = None
                    continue
                else:
                    raise AttributeError('unexpected value for _ondelete_')

            if 'id' not in v:
                continue
            if '_model_' not in v:
                raise AttributeError('not found key: _model_ for attribute %s and vals ' % (k))
            TModel = get_model_from_pool(pool, v['_model_'])
            try:
                (nrecord[k], ) = TModel.search([('id', '=', v['id'])], limit=1)
            except ValueError:
                raise RuntimeError(f"Se requiere registro de relacion para id {v['id']} de modelo {v['_model_']}")

        elif isinstance(v, list):
            to_add = []
            for rrecord in v:
                if 'id' in rrecord:
                    to_add.append(rrecord['id'])
            one2many_action = []
            if to_add:
                one2many_action.append(('add', to_add))
            if one2many_action:
                nrecord[k] = one2many_action

        else:
            nrecord[k] = v
    return nrecord

def model_allowed_write_pkey(TModel):
    return 'fleet_dispatch_client_pkey' in TModel._fields

def exists_record_for_model(TModel, machine, pkey):
    if not pkey:
        return None

    if not machine:
        return None

    if model_allowed_write_pkey(TModel):
        records = TModel.search_read([('fleet_dispatch_client_pkey', '=', pkey),
                                      ('fleet_dispatch_client_machine', '=', machine)],
                                     fields_names=['id'],
                                     limit=1)
        if records:
            return records[0]['id']
    return None

def call_actions(pool, vals):
    actions = vals.get('_actions_', [])
    for action in actions:
        model, func, args = tuple(action)
        Model = pool.get(model)
        getattr(Model, func)(vals, *args)

def sync_record(pool, machine, pkey, vals, acc={}):
    cvals = vals.copy()
    model = vals['_model_']
    TModel = get_model_from_pool(pool, model)


    if vals['_dirty'] and vals['_state'] == 'new':
        record_in_db = exists_record_for_model(TModel, machine, pkey)
        if record_in_db:
            logger.warning(f"Se ha intentado crear un registro existen de pkey {pkey} y modelo {model}, se actualiza")
            vals['id'] = record_in_db
            vals['_model_'] = model
            vals['_fkey'] = record_in_db
            vals['_dirty'] = True
            vals['_state'] = 'created'


    if vals['_dirty'] and vals['_state'] == 'new':
        instance = record_to_instance(pool, machine, pkey, vals)
        vals['id'] = instance.id
        vals['_model_'] = model
        vals['_fkey'] = instance.id
        vals['_dirty'] = False
        vals['_state'] = 'created'
        call_actions(pool, vals)
        return vals
            
    elif vals['_dirty'] and vals['_state'] == 'created' and vals['_fkey']:
        record = record_to_instance(pool, machine, pkey, vals, acc)
        vals['_dirty'] = False
        call_actions(pool, vals)
        return vals
                    
    elif vals['_dirty'] and vals['_state'] == 'deleted' and vals['_fkey']:
        record = record_to_instance(pool, machine, pkey, vals)
        vals['_state'] = 'deleted'
        call_actions(pool, vals)
        return vals

def record_to_instance(pool, machine, pkey, vals, acc={}):
    model = vals['_model_']
    TModel = get_model_from_pool(pool, model)

    if model not in acc:
        acc[model] = {}


    if vals['_dirty'] and vals['_state'] == 'new':
        params = _build_params(pool, vals, machine)
        vlist = {}
        if model_allowed_write_pkey(TModel):
            vlist['fleet_dispatch_client_pkey'] = pkey
            vlist['fleet_dispatch_client_machine'] = machine

        for k, v in params.items():
            if k in TModel.fields_get():
                vlist[k] = v
        instance, = TModel.create([vlist])
        assert instance.id > 0, f"Se requiere registro nuevo para model {model}"
        return instance
            
    elif vals['_dirty'] and vals['_state'] == 'created' and vals['_fkey']:
        if acc and model in acc and vals['_fkey'] in acc[model]:
            records = acc[model][vals['_fkey']]
        else:
            records = TModel.search([('id', '=', vals['_fkey'])], limit=1)
            acc[model][vals['_fkey']] = records

        assert len(records) == 1, f"Se requiere registro para fkey {vals['_fkey']} de model {model}"
        record = records[0]
        params = _build_params(pool, vals, machine)
        changed = False
        for k, v in params.items():
            if k in ['id']:
                continue

            #con timeit esto resulto ser los mas rapido
            if v.__class__ != list:
                try:
                    setattr(record, k, v)
                except AttributeError:
                    pass

        try:
            record.save()
        except Exception as e:
            logger.error("error al guardar registro para modelo %s de id %d" % (vals['_model_'], vals['_fkey']))
            traceback.print_exc()
            raise e

        return record
                    
    elif vals['_dirty'] and vals['_state'] == 'deleted' and vals['_fkey']:
        records = TModel.search([('id', '=', vals['_fkey'])])
        assert len(records) == 1, f"Se requiere registro para fkey {vals['_fkey']} de model {model}"
        record = records[0]
        record.delete()
        return record

def get_id(record):
    if record:
        return record.id
    return None

def current_user(pool):
    User = pool.get('res.user')
    return User(Transaction().user)

def find_group_route(pool):
    GroupRoute = pool.get('public_transport.group_route')
    return list(current_user(pool).groups_routes)

def first_group_route(pool):
    routes = find_group_route(pool)
    if len(routes) > 0:
        return routes[0]
    return None

def find_route(pool):
    Route = pool.get('public_transport.route')
    query = [('group', 'in', find_group_route(pool))]
    return Route.search(query)

def find_vehicles(pool):
    Vehicle = pool.get('fleet.vehicle')
    query = [('routes', 'in', find_group_route(pool))]
    return Vehicle.search(query)

def find_dispatch_annotation(pool, search_read=False):
    DispatchAnnotation = pool.get('public_transport.dispatch.annotation')
    if search_read:
        return DispatchAnnotation.search_read([('to_dispatcher', '=', True)])
    else:
        return DispatchAnnotation.search([('to_dispatcher', '=', True)])

def find_dispatch_rel_annotation(pool, date=None, search_read=False):
    AnnotationDispatch = pool.get('public_transport.dispatch.annotation_dispatch')
    records = find_dispatch(pool, date=date)
    dispatch_ids = map(int, records)
    annotations = []

    if search_read:
        return AnnotationDispatch.search_read([('annotation.to_dispatcher', '=', True),
                                               ('dispatch', 'in', dispatch_ids)],
                                              fields_names=protocolv2.fields_names_for_model('public_transport.dispatch.annotation_dispatch'))
    return AnnotationDispatch.search([('annotation.to_dispatcher', '=', True),
                                      ('dispatch', 'in', dispatch_ids)])

            

def find_passenger_counter(pool, search_read=False):
    PassengerCounter = pool.get('fleet.vehicle.passenger_counter')
    results = []
    query = [('vehicle.routes', 'in', find_group_route(pool))]

    for vehicle in find_vehicles(pool):
        cquery = query.copy()
        cquery.extend([('vehicle', '=', vehicle.id)])
        if search_read:
            counters = PassengerCounter.search_read(cquery,
                                                    limit=1,
                                                    order=[('time', 'DESC')],
                                                    fields_names=protocolv2.fields_names_for_model('fleet.vehicle.passenger_counter'))
        else:
            counters = PassengerCounter.search(cquery,
                                               limit=1,
                                               order=[('time', 'DESC')])
        results.extend(counters)
    return results

def find_dispatch(pool, date=None, search_read=False):
    Date = pool.get('ir.date')
    Dispatch = pool.get('public_transport.dispatch')
    query = [('group_route', 'in', find_group_route(pool))]

    if date:
        last_date = date
    else:
        last_date = Date.today()

    # machete a utc
    start_day = datetime.combine(date, datetime.min.time()) +  timedelta(hours=5)
    end_day = datetime.combine(date, datetime.max.time()) + timedelta(hours=5)

    vehicle_ids = [v.id for v in find_vehicles(pool)]
    vehicle_ids.append(None)
    dispatchs = []
    if vehicle_ids:
        cquery = query.copy()
        cquery.extend([('vehicle', 'in', vehicle_ids)])
        cquery.extend([('init_time', '>=', start_day - timedelta(days=1)),
                       ('init_time', '<=', end_day)])
        if search_read:
            dispatchs = Dispatch.search_read(cquery,
                                             order=[('id', 'DESC')],
                                             fields_names=protocolv2.fields_names_for_model('public_transport.dispatch'))
        else:
            dispatchs = Dispatch.search(cquery,
                                        order=[('id', 'DESC')])
                                   
    return dispatchs

def find_driver(pool):
    Employee = pool.get('company.employee')
    return Employee.search([('is_driver', '=', True)])

def find_passenger_counter_issue_types(pool):
    IssueTypes = pool.get('fleet.vehicle.passenger_counter.issue.type')
    return IssueTypes.search([])

def find_passenger_counter_issues(pool):
    Issues = pool.get('fleet.vehicle.passenger_counter.issue')
    results = []
    query = [('vehicle.routes', 'in', find_group_route(pool))]

    for vehicle in find_vehicles(pool):
        cquery = query.copy()
        cquery.extend([('vehicle', '=', vehicle.id)])
        counters = Issues.search(cquery,
                                 limit=1,
                                 order=[('time', 'DESC')])
        results.extend(counters)
    return results

def find_trips_daily(pool, date=None):
    Date = pool.get('ir.date')
    if date:
        today = date
    else:
        today = Date.today()

    yesterday = today - timedelta(days=1)

    TripsDaily = pool.get('fleet_dispatch_client_backend.trips_daily')
    trips = TripsDaily.search([('group_route', 'in', find_group_route(pool)),
                               ('vehicle', 'in', find_vehicles(pool)),
                               ('date', 'in', [today, yesterday])],
                              order=[('vehicle', 'ASC')])
    return trips

def find_dispatch_quantity_for_day(pool, date=None):
    Date = pool.get('ir.date')
    if date:
        today = date
        yesterday = today - timedelta(days=1)
    else:
        today = Date.today()
        yesterday = Date.today() - timedelta(days=1)

    DispatchQuantityForDay = pool.get('public_transport.dispatch.quantity_for_day')
    return DispatchQuantityForDay.search([('vehicle', 'in', find_vehicles(pool)),
                                          ('date', 'in', [today, yesterday])],
                                         order=[('vehicle', 'ASC')])
        
def find_trips_daily_notes(pool):
    Notes = pool.get('fleet_dispatch_client_backend.trips_daily-notes')
    return Notes.search([])

def find_trips_daily_reason(pool):
    Notes = pool.get('fleet_dispatch_client_backend.trips_daily-reason_cancel')
    return Notes.search([])

def logsync(pool, name, action, model=''):
    LogSync = pool.get('fleet_dispatch_client_backend.logsync')
    LogSync.create([{
        'name': name,
        'model': model,
        'action': action,
        'intime': datetime.now()
    }])

@app.route('/<database_name>/fleet_dispatch_client/client/pull/<model>', methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def pull_old(request, pool, model):
    return do_pull(request, pool, None, model)


@app.route(protocolv2.UrlPull.to_route(), methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def pullv2(request, pool, machine, model):
    model_search_read = [
        'public_transport.dispatch',
        'fleet.vehicle.passenger_counter',
        'public_transport.dispatch.annotation_dispatch'
    ]

    if model in model_search_read:
        return resp_bodyv2log(request, find_by_model(request, pool, machine, model, search_read=True), model)
    else:
        return do_pull(request, pool, machine, model)


@app.route(protocol.UrlPull.to_route(), methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def pull(request, pool, machine, model):
    return do_pull(request, pool, machine, model)


def find_by_model(request, pool, machine, model, search_read=False):
    logger.info(f"Peticion PULL {request.full_path} de usuario {current_user(pool).name}")
    mstart = timer()
    date = None
    if 'date' in request.args and request.args['date']:
        date = datetime.strptime(request.args['date'], '%Y-%m-%d')

    finds = {
        'public_transport.dispatch.quantity_for_day': functools.partial(find_dispatch_quantity_for_day, date=date),
        'public_transport.dispatch.annotation_dispatch': functools.partial(find_dispatch_rel_annotation, date=date, search_read=search_read),
        'public_transport.dispatch.annotation': functools.partial(find_dispatch_annotation, search_read=search_read),
        'fleet.vehicle.passenger_counter.issue': find_passenger_counter_issues,
        'fleet.vehicle.passenger_counter.issue.type': find_passenger_counter_issue_types,
        'fleet.vehicle': find_vehicles,
        'public_transport.dispatch': functools.partial(find_dispatch, date=date, search_read=search_read),
        'company.employee': find_driver,
        'public_transport.group_route': find_group_route,
        'public_transport.route': find_route,
        'fleet.vehicle.passenger_counter': functools.partial(find_passenger_counter, search_read=search_read),
        'fleet_dispatch_client_backend.trips_daily': functools.partial(find_trips_daily, date=date),
        'fleet_dispatch_client_backend.trips_daily-notes': find_trips_daily_notes,
        'fleet_dispatch_client_backend.trips_daily-reason_cancel': find_trips_daily_reason,
    }
    logsync(pool, machine, 'pull', model)
    mend = timer()
    records = finds[model](pool)
    logger.info(f"Peticion PULL {request.full_path} demoro {mend - mstart}")
    return records

def do_pull(request, pool, machine, model, search_read=False):
    """
    PETICION SINCRONICA.

    Retorna los registros condicionados segun el usuario del modelo 'model' en la estructura de datos de 'fleet_dispatch_client',
    esto implica que no trae todos, se sigen las reglas implementadas en 'fleet_dispatch_client.sync'.
    """
    return resp_bodylog(request, find_by_model(request, pool, machine, model))

@app.route('/<database_name>/fleet_dispatch_client/client/push/<pkey>', methods=['POST'])
@with_pool
@with_transaction()
@client_application
def push_old(request, pool, pkey):
    return do_push(request, pool, None, pkey)

@app.route(protocol.UrlPush.to_route(), methods=['POST'])
@with_pool
@with_transaction(readonly=False)
@client_application
def push(request, pool, machine, pkey):
    return do_push(request, pool, machine, pkey)

def do_push(request, pool, machine, pkey):
    """
    PETICION SINCRONICA.

    Sincroniza los datos del modelo recibido usando la estructura de datos de 'fleet_dispatch_client',
    y retorna nuevo estado, para el cliente, el cliente debe confirmar que el '_pkey' del modelo sea el enviado.

    TODO: implementar excepciones
    """
    logger.debug(f"Peticion PUSH {request.full_path} de usuario {current_user(pool).name}")
    body = req_body(request)
    if isinstance(body, list):
        data = body[0]
    else:
        data = body
        
    if '_model_' not in data:
        raise TypeError('need attribute _model_')

    vals = sync_record(pool, machine, pkey, data)
    logsync(pool, machine, 'push')
    return resp_body(request, vals)

@app.route(protocol.UrlPushWithRelations.to_route(), methods=['POST'])
@with_pool
@with_transaction(readonly=False)
@client_application
def push_with_relations(request, pool, machine):
    return do_push_with_relations(request, pool, machine)

def do_push_with_relations(request, pool, machine):
    """
    PETICION SINCRONICA.

    Sincroniza los datos del modelo recibido usando la estructura de datos de 'fleet_dispatch_client',
    y retorna nuevo estado, para el cliente, el cliente debe confirmar que el '_pkey' del modelo sea el enviado.

    TODO: implementar excepciones
    """
    logger.info(f"Peticion PUSH {request.full_path} de usuario {current_user(pool).name}")
    mstart = timer()

    body = req_body(request)
    if isinstance(body, list) and len(body) == 0:
        data = {}
    elif isinstance(body, list):
        data = body[0]
    else:
        data = body

    records = []
    def sync_relations(vals, acc):
        cvals = vals.copy()
        for k, v in vals.items():
            if isinstance(v, dict) and '_pkey' in v:
                rvals = sync_relations(v, acc).copy()
                if 'id' not in v:
                    try:
                        cvals[k] = sync_record(pool, machine, rvals['_pkey'], rvals, acc).copy()
                    except Exception as e:
                        raise RuntimeError(f"Excepcion en sync_record para {cvals['_model_']}.{cvals.get('_fkey')} en estado {cvals.get('_state')}. {str(cvals)} " + str(e))
                    
                records.insert(0, cvals[k])
        return cvals

    acc = {}
    for pkey, vals in data.items():
        cvals = sync_relations(vals, acc)
        try:
            nvals = sync_record(pool, machine, pkey, cvals, acc)
        except Exception as e:
            raise RuntimeError(f"Excepcion en sync_record para {cvals['_model_']}.{cvals.get('_fkey')} en estado {cvals.get('_state')}. {str(cvals)}. " + str(e))

        records.append(nvals)

    logsync(pool, machine, 'push_with_relations')
    mend = timer()
    logger.info(f"Peticion PUSH {request.full_path} demoro {mend - mstart}")
    return resp_body(request, records)

@app.route('/<database_name>/fleet_dispatch_client/heartbeat', methods=['GET'])
@with_pool
@with_transaction()
@client_application
def heartbeat(request, pool):
    return Response()

@app.route(protocol.UrlHeartBeat.to_route(), methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def heartbeat(request, pool, machine):
    Device = pool.get('fleet_dispatch_client_backend.device')
    try:
        device, = Device.search([('machineid', '=', machine)], limit=1)
        Device.__queue__.write([device], {'last_heartbeat': datetime.now()})
    except ValueError:
        pass
    return Response()


@app.route('/<database_name>/v2/fleet_dispatch_client/client/<machine>/provision/<token>', methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def provision(request, pool, machine, token):
    Device = pool.get('fleet_dispatch_client_backend.device')
    DeviceProvision = pool.get('fleet_dispatch_client_backend.device-provision')

    try:
        provision, = DeviceProvision.search([('token', '=', token),
                                             ('device.machineid', '=', machine)], limit=1)
        DeviceProvision.__queue__.write([provision], {'last_sync': datetime.now()})
        data = {
            'group_routes': protocol.marshal(list(provision.device.group_routes)),
            'profiles': protocol.marshal(list(provision.device.profiles)),
        }
        logsync(pool, machine, 'provision')
        if request.headers['Content-Type'] == 'application/octet-stream':
            return Response(protocol.encode_binary(data))
        else:
            return Response(protocol.encode(data))
    except ValueError:
        return Response(status=401)

    
@app.route(protocol.UrlRAction.to_route(), methods=['POST'])
@with_pool
@with_transaction(readonly=False)
@client_application
def PUT_raction(request, pool, machine):
    DeviceAction = pool.get('fleet_dispatch_client_backend.device-action')
    raction, = req_body(request)
    daction = DeviceAction(raction['id'])
    DeviceAction.__queue__.do_ack(daction)
    raction['_dirty'] = False
    return resp_body(request, raction)

@app.route(protocol.UrlRAction.to_route(), methods=['GET'])
@with_pool
@with_transaction(readonly=False)
@client_application
def GET_raction(request, pool, machine):
    DeviceAction = pool.get('fleet_dispatch_client_backend.device-action')
    actions = DeviceAction.search([('device.machineid', '=', machine),
                                   ('completed', '=', False)])
    return resp_body(request, actions)

# ejemplo de como se debe implementar la url
# la propuesta es que si otros modulos que exporten la url para este
# cliente, adicionen un bloque, mirar por ejemplo 'fleet_dispatch_client_backend_sonar'
#@app.route('/<database_name>/fleet_dispatch_client/webhook/dispatch/<state>', methods=['POST'])
#@with_pool
#@with_transaction()
#@client_application
#def dispatch(request, pool, state):
#    return Response()


def req_body(request):
    rbody = request.get_data(cache=False)
    if request.headers['Content-Type'] == 'application/octet-stream':
        body = protocol.unserialize(rbody, decoder=protocol.decode_binary)
    else:
        body = protocol.unserialize(rbody)
    return body


def resp_bodyv2log(request, data, model):
    mtimer = timer()
    if request.headers['Accept'] == 'application/octet-stream':
        dserialize = protocolv2.serialize(data, model, encoder=protocol.encode_binary)
    else:
        dserialize = protocolv2.serialize(data, model)
    logger.info(f"resp_bodyv2 {timer() - mtimer}")
    return Response(dserialize)

def resp_bodyv2(request, data, model):
    if request.headers['Accept'] == 'application/octet-stream':
        return Response(protocolv2.serialize(data, model, encoder=protocol.encode_binary))
    else:
        return Response(protocolv2.serialize(data, model))
    
def resp_body(request, data):
    if request.headers['Accept'] == 'application/octet-stream':
        return Response(protocol.serialize(data, encoder=protocol.encode_binary))
    else:
        return Response(protocol.serialize(data))

def resp_bodylog(request, data):
    mtimer = timer()
    if request.headers['Accept'] == 'application/octet-stream':
        dserialize = protocol.serialize(data, encoder=protocol.encode_binary)
    else:
        dserialize = protocol.serialize(data)
    logger.info(f"resp_body {timer() - mtimer}")
    return Response(dserialize)
