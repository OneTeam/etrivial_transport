# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import gzip as compress
import base64
import pickle
import datetime
import operator
import functools


class UrlRAction:
    def __init__(self, base, machine):
        self.base = base
        self.machine = machine

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/raction".format(base=self.base, machine=self.machine)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/raction'

class UrlHeartBeat:
    def __init__(self, base, machine):
        self.base = base
        self.machine = machine

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/heartbeat".format(base=self.base, machine=self.machine)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/heartbeat'


class UrlProvision:
    def __init__(self, base, machine, token):
        self.base = base
        self.machine = machine
        self.token = token

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/provision/{token}".format(base=self.base, machine=self.machine, token=self.token)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/provision/<token>'

class UrlPush:
    def __init__(self, base, machine, pkey=None):
        self.base = base
        self.pkey = pkey
        self.machine = machine
        
    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/push/{pkey}".format(machine=self.machine,base=self.base, pkey=self.pkey)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/push/<pkey>'


class UrlPushWithRelations:
    def __init__(self, base, machine):
        self.base = base
        self.machine = machine
        
    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/push_with_relations/".format(machine=self.machine,base=self.base)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/push_with_relations/'


class UrlPull:
    def __init__(self, base, machine, model=None, date=None):
        self.base = base
        self.model = model
        self.machine = machine
        self.date = date or ""

    def __str__(self):
        return "{base}/v2/fleet_dispatch_client/client/{machine}/pull/{model}?date={date}".format(machine=self.machine, base=self.base, model=self.model, date=self.date)

    @classmethod
    def to_route(cls):
        return '/<database_name>/v2/fleet_dispatch_client/client/<machine>/pull/<model>'



def encode(data):
    if isinstance(data, dict):
        val = pickle.dumps(data)
    else:
        val = data
    return base64.encodebytes(compress.compress(val))

def encode_binary(data):
    if isinstance(data, dict):
        val = pickle.dumps(data)
    else:
        val = data
    return compress.compress(val)
    
#TODO: no deberia importar si es 'dict' u otro tipo,
#pero 'getattr' no obtiene los valores de los descriptores
#de objetos de tryton.
def serialize(records, encoder=encode):
    vals = marshal(records)
    return encoder(pickle.dumps(vals))

def marshal(records):
    vals = []

    if not isinstance(records, list):
        records = [records]

    for record in records:
        if not record:
            continue

        val = {}
        if isinstance(record, dict):
            val = record.copy()
            vals.append(val)
            continue

        #si es instancia de tryton
        if hasattr(record, 'id'):
            val['_fkey'] = record.id
        if hasattr(record, '__name__'):
            val['_model_'] = record.__name__

        _hasattr = operator.contains if isinstance(record, dict) else hasattr
        _getattr = operator.getitem if isinstance(record, dict) else getattr

        fields, fields_relation, _virtual = fields_for_model(val['_model_'])
        for field in fields:
            if _hasattr(record, field):
                val[field] = format_field(_getattr(record, field))
        
        for field, relations in fields_relation.items():
            if hasattr(record, field):
                relation = None
                if hasattr(record, 'fields_get') and callable(record.fields_get):
                    relation = record.fields_get([field])[field]['relation']
                else:
                    relation = record._fields[field]['relation']
                val[field] = {
                    '_model_': relation
                }
                rel = getattr(record, field)
                for field_rel in relations:
                    subfields = field_rel.split(".", 1)
                    if rel:
                        lval = functools.reduce(lambda acc, val: getattr(acc, val), subfields, rel)
                        if lval:
                            val[field][field_rel] = format_field(lval)
        vals.append(val)
    return vals

# se debe mantener hasta estar todos actualizados
# a ultima version
def decode(data):
    if isinstance(data, bytes):
        return compress.decompress(base64.decodebytes(data))
    return compress.decompress(base64.decodebytes(str.encode(data)))

def decode_binary(data):
    if isinstance(data, bytes):
        return compress.decompress(data)
    return compress.decompress(str.encode(data))

def unserialize(data, decoder=decode):
    return pickle.loads(decoder(data))

def format_field(field):
    # HACK: es necesario asignar la zona para
    # que python realice cambios de horarios correctamente.
    # esto es suponiendo indica que el servidor envia la hora en UTC y 
    # `proteus`no hace conversion.
    if isinstance(field, datetime.datetime):
        if field.tzname() == None or field.tzname() == 'UTC':
            return field.replace(tzinfo=datetime.timezone.utc)
        else:
            return field.astimezone(datetime.timezone.utc)
    return field


def fields_for_model(model):
    mapping = {
        'fleet_dispatch_client_backend.device-action': ([
            'id',
            'sequence',
            'action',
            'completed',
            'ack',
            'active',
            'motive'
        ], {
            'device': ['id', 'machineid'],
        }, True),
                                                   
        'fleet_dispatch_client_backend.gps_log': ([
            'id',
            'rec_name',
            'model',
            'model_id',
            'model_pkey',
            'latitude',
            'longitude',
            'message'
        ], {}, False),
        
        'fleet_dispatch_client_backend.trips_daily-reason_cancel': ([
            'id',
            'rec_name',
            'name'
        ], {}, False),

        'fleet_dispatch_client_backend.trips_daily-notes': ([
            'id',
            'rec_name',
            'name'
        ], {}, False),
        
        'fleet_dispatch_client_backend.trips_daily': ([
            'id',
            'rec_name',
            'init_counter',
            'end_counter',
            'date',
            'quantity_dispatches',
            'open_confirm_check',
            'open_confirm_check_note',
            'user_confirm_check',
            'user_confirm_check_note',
            'cancel_reason',
            'cancel_note',
        ], {
            'vehicle': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate'],
            'group_route': ['id', 'rec_name', 'code', 'name', 'active', 'unique_turn_for_all_route'],
        }, False),
        
        'public_transport.dispatch.quantity_for_day': ([
            'id',
            'rec_name',
            'init_counter',
            'end_counter',
            'quantity_dispatches',

            #QuantityForDayMixin
            'date',
        ], {
            'vehicle': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate'],
            'group_route': ['id', 'rec_name', 'code', 'name', 'active', 'unique_turn_for_all_route'],
        }, False),
        
        'public_transport.dispatch.annotation_dispatch': ([
            'id',
            ], {
                'dispatch': ['id', 'rec_name'],
                'annotation': ['id', 'rec_name'],
            }, False),
        
        'public_transport.dispatch.annotation': ([
            'id',
            'rec_name',
            'name',
            'code',
            'description',
            'to_dispatcher'
        ], {}, True),
        
        'fleet.vehicle.passenger_counter.issue': ([
            'id',
            'rec_name',
            'name',
            'time',
            'note',
            ], {
                'vehicle': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate'],
                'init_counter': ['id', 'counter', 'time'],
                'end_counter': ['id', 'counter', 'time'],
                'type': ['id', 'rec_name', 'name']
            }, False),
        
        'fleet.vehicle.passenger_counter.issue.type': ([
            'id',
            'rec_name',
            'name',
            'affects_passangers',
            ], {}, True),

        'fleet_dispatch_client_backend.profile': ([
            'id',
            'rec_name',
            'name',
            'code',
            'server_url',
        ], {
            'user_application': ['id', 'rec_name', 'key', 'user.id'],
        }, True),
        
        'public_transport.group_route': ([
            'id',
            'rec_name',
            'name',
            'code',
            'active',
            'unique_turn_for_all_route',
        ], {}, False),
        
        'public_transport.route': ([
            'id',
            'rec_name',
            'name',
            'code',
            'active',
            'sonar_id',
            'max_passangers_for_lap'
        ], {
            'group': ['id', 'rec_name', 'code', 'name', 'active']
        }, False),

        'fleet.vehicle': ([
            'id',
            'rec_name',
            'sonar_id',
            'internal_code',
        ], {
            'routes': ['id', 'rec_name', 'code']
        }, False),
        
        'fleet.vehicle.passenger_counter': ([
            'id',
            'counter',
            'time',
            'note'
        ], {
            'vehicle': ['id', 'internal_code', 'rec_name', 'sonar_id',  'active', 'licence_plate']
        }, False),
        
        'company.employee': ([
            'id',
            'is_driver',
            'rec_name',
            'id_sonar'
        ], {
            'party': ['id', 'name', 'rec_name']
        }, True),
        'public_transport.dispatch': ([
            'id',
            'turn',
            'init_time',
            'end_time',
            'sonar_id',
            'sonar_cancelled',
            'issue_register_note',
            'canceled_cause',
            'notes',
            'state',
            'dispatched_time',
            'done_time',
            'canceled_time',
        ], {
            'dispatched_uid': ['id'],
            'done_uid': ['id'],
            'canceled_uid': ['id'],
            'group_route': ['id', 'rec_name', 'code', 'name', 'active', 'unique_turn_for_all_route'],
            'route': ['id', 'rec_name', 'code', 'sonar_id', 'name', 'max_passangers_for_lap'],
            'vehicle': ['id', 'internal_code', 'sonar_id', 'licence_plate', 'rec_name'],
            'driver': ['id', 'name'],
            'init_counter': ['id', 'counter', 'time'],
            'end_counter': ['id', 'counter', 'time'],
            'issue_counter': ['id', 'counter', 'time'],
            'subtotal_from': ['id', 'rec_name'],
            'fleet_dispatch_client_trip_daily': ['id', 'rec_name']
        }, False),
    }
    
    return mapping[model]


def as_utc(field):
    if isinstance(field, datetime.datetime):
        if field.tzname() == None or field.tzname() == 'UTC':
            return field.replace(tzinfo=datetime.timezone.utc)
        else:
            return field.astimezone(datetime.timezone.utc)
    return field
