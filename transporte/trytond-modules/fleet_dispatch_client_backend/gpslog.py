# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool

class GPSLog(ModelSQL, ModelView):
    'GPS - Log'
    __name__ = 'fleet_dispatch_client_backend.gps_log'

    machine = fields.Char('Machine')
    model = fields.Char('Model')
    model_id = fields.BigInteger('ID')
    model_pkey = fields.Char('Pkey')
    message = fields.Text('Message')
    latitude = fields.Float('Latitude')
    longitude = fields.Float('Longitude')

    @classmethod
    def try_link(cls, vals, model, pkey):
        TModel = Pool().get(model)

        #se vincula a modelos que tengan atribute fleet_dispatch_client_pkey
        #y que ya existan, los registros por crear quedaran en blanco
        #lo cual requiere un wizard para reenlazar
        if not 'fleet_dispatch_client_pkey' in TModel.fields_get():
            return

        try:
            record, = TModel.search([('fleet_dispatch_client_pkey', '=', pkey)], limit=1)
            lrecord, = cls.search([('model', '=', model), ('model_pkey', '=', pkey)], limit=1)
            lrecord.model_id = record.id
            lrecord.save()
        except ValueError:
            pass
