# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.
from trytond.pool import PoolMeta

__all__ = ['UserApplication']

class UserApplication(metaclass=PoolMeta):
    __name__ = 'res.user.application'
    
    @classmethod
    def __setup__(cls):
        super(UserApplication, cls).__setup__()
        cls.application.selection.append(('fleet_dispatch_client', 'Fleet Dispatch Client'))
