# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client_backend.
#
# fleet_dispatch_client_backend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client_backend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client_backend.  If not, see <https://www.gnu.org/licenses/>.

from trytond.pool import Pool, PoolMeta

from .user import UserApplication
from .dispatch import *
from .provision import *
from .logsync import *
from .trips_daily import *
from .gpslog import *
from .actions import *
from . import routes


__all__ = ['register']


def register():
    Pool.register(
        Device,
        Profile,
        DeviceGroupRoute,
        DeviceProfile,
        DeviceProvision,
        UserApplication,
        Dispatch,
        VehiclePassengerCounter,
        VehiclePassengerCounterIssue,
        DispatchesQuantityForDay,
        LogSync,
        DispatchTrips,
        TripsDaily,
        TripsDailyNotes,
        TripsDailyReasonCancel,
        GPSLog,
        DeviceAction,
        module='fleet_dispatch_client_backend', type_='model')
    

