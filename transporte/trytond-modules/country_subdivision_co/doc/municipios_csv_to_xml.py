"""
Convierte el archivo municipios_colombia.csv y departamentos_colombia.csv
en municipios_colombia.xml
"""

import csv
model = 'country.subdivision'
municipios = open('municipios_colombia.csv','r')
departamentos = open('departamentos_colombia.csv','r')

municipios_xml = open('municipios_colombia.xml','w')
#departamentos_xml = open('departamentos_colombia.xml','w')

municipios_reader = csv.reader(municipios)
departamentos_reader = csv.reader(departamentos)
departamentos_header = next(departamentos_reader)
d_fields = {departamentos_header[x]:x for x in range(0,len(departamentos_header))}

Departamentos = {}
for departamento in departamentos_reader:
	#iso_id = 'co-' + str(departamento[d_fields['ISO']]).lower
	dane_code = int(departamento[d_fields['DANE']])
	Departamentos [dane_code] = dict(zip(list(d_fields.keys()),departamento))
departamentos.close()

municipios_header = next(municipios_reader)
m_fields = {municipios_header[x]:x for x in range(0,len(municipios_header))}

municipios_xml.write("""<?xml version="1.0"?>
<tryton>
    <data>
""")
for row in municipios_reader:
	dane_code = row[m_fields['department']] + row[m_fields['code']]
	depto_id = "co-" + Departamentos[int(row[m_fields['department']])]['ISO'].lower()
	municipios_xml.write("""	<record model="{model}"  id="{id}">
		<field name="name">{name}</field>
		<field name="type">borough</field>
		<field name="dane_code">{dane_code}</field>
		<field name="country" ref="country.co"/>
		<field name="code">{code}</field>
		<field name="parent" ref="country.{depto_id}"/>
        </record>
""".format(
	model=model,
	id="co-"+str(dane_code),
	name=row[m_fields['name']],
	code="co-"+str(dane_code),
	dane_code=dane_code,
	depto_id=depto_id,	
	))
municipios_xml.write("""
    </data>
</tryton>
"""
)
municipios.close()
municipios_xml.close()
