# This file is part of country_subdivision_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .country import *
__all__ = ['register']


def register():
    Pool.register(
        Subdivision,
        module='country_subdivision_co', type_='model')
    Pool.register(
        module='country_subdivision_co', type_='wizard')
    Pool.register(
        module='country_subdivision_co', type_='report')
