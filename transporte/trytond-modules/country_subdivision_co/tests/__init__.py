# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.country_subdivision_co.tests.test_country_subdivision_co import suite
except ImportError:
    from .test_country_subdivision_co import suite

__all__ = ['suite']
