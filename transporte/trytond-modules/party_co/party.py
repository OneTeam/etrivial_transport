# This file is part of party_co.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView, Unique, sequence_ordered
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Equal
import logging

logger = logging.getLogger(__name__)

__all__ = ['PartyIdentifier','Party','ContactMechanism']


CO_IDENTIFIERS = [
    (None, ''),
    ('co_vat_11', 'Registro Civil'),
    ('co_vat_12', 'Tarjeta de Identidad'),
    ('co_vat_13', 'Cedula de Ciudadania'),
    ('co_vat_21', 'Tarjeta de Extranjeria'),
    ('co_vat_22', 'Cedula de Extranjeria'),
    ('co_vat_31', 'NIT'),
    ('co_vat_41', 'Pasaporte'),
    ('co_vat_42', 'Tipo de Documento Extrangero'),
    ('co_vat_43', 'Sin identificacion en del exterior')
]
CO_CONTACT_MECHANISM_TYPES = [
    ('sms','SMS')
]

class Party(ModelSQL,ModelView):
    "Party Colombian"
    
    __name__='party.party'
    __metaclass__ = PoolMeta
    
    first_name = fields.Char("Primer Nombre",states={
        'invisible': Not(Eval('type_person') == 'persona_natural'),}
    )
    second_name = fields.Char("Segundo Nombre",states={
        'invisible': Not(Eval('type_person') == 'persona_natural'),}
    )
    first_surname = fields.Char("Primer Apellido",states={
        'invisible': Not(Eval('type_person') == 'persona_natural'),}
    )
    second_surname = fields.Char("Segundo Apellido",states={
        'invisible': Not(Eval('type_person') == 'persona_natural'),}
    )
    birth_date = fields.Date('Fecha de nacimiento')
    regime_tax = fields.Selection([
            (None, ''),
            ('regimen_simplificado', 'Regimen Simplificado'),
            ('regimen_comun', 'Regimen Comun'),
            ('gran_contribuyente', 'Gran Contribuyente'),
            ('entidad_estatal', 'Entidad Estatal'),
            ('domiciliado_extranjero', 'Domiciliado en Extranjero'),
            ], 'Regimen de Impuestos')
    type_person = fields.Selection([
            (None, ''),
            ('persona_natural', 'Persona Natural'),
            ('persona_juridica', 'Persona Juridica'),
            ], 'Tipo de Persona')
    autoretenedor = fields.Boolean('Autoretenedor')
    declarante = fields.Boolean('Declarante')
    ciiu_code = fields.Char('CIIU Code', size=4)
    age = fields.Function(fields.Integer('Edad'),'on_change_with_age')
    type_tax_identifier = fields.Function(fields.Selection('get_types_identifier','Tipo de Identidad'),'on_change_with_type_tax_identifier')

    @staticmethod
    def default_type_person():
        return 'persona_natural'
    
    @fields.depends('birth_date')
    def on_change_with_age(self,name=None):
        if self.birth_date:
            Date = Pool().get('ir.date')
            today = Date.today()
            age = today.year - self.birth_date.year
            age -= (today.month, today.day) < (self.birth_date.month, self.birth_date.day)
            return  age
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_type_person(self):
        if self.type_person == 'persona_natural':
            self.on_change_name()
        if self.type_person == 'persona_juridica':
            pass
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_name(self):
        self.rebuild_name()
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_first_name(self):
        self.rebuild_name()
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_second_name(self):
        self.rebuild_name()
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_second_surname(self):
        self.rebuild_name()
    
    @fields.depends('name','first_name','second_name','first_surname',
                    'second_surname','type_person')
    def on_change_first_surname(self):
        self.rebuild_name()
    
    def rebuild_name(self):
        """reconstruye los campos relacionados con el nombre dependiendo de
        estos y del tipo de persona."""
        
        if self.type_person == 'persona_natural':
            if not (self.first_name or self.second_name \
                    or self.first_surname or self.second_surname):
                if self.name:
                    self.first_name, self.second_name, self.first_surname, \
                    self.second_surname = self.split_name(self.name)
                else:
                    return
            else:
                self.name = self.get_full_name()
        if self.type_person == 'persona_juridica':
            if self.name:
                self.clean_split_name()
            else:
                self.name = self.get_full_name()
 
    def clean_split_name(self):
        self.first_name = self.second_name = self.first_surname = self.second_surname = ''
    
    @staticmethod
    def split_name(name):
        """Divide un nombre completo en una lista de 4 elementos"""
        
        second_surname = None
        first_surname = None
        second_name = None
        first_name = None
        names = name.split(' ')
        if len(names) > 0:
            first_name = names[0]
            if len(names) == 2:
                second_surname = None
                first_surname = names[1]
            elif len(names) == 3:
                first_surname = names[1]
                second_surname = names[2]
            elif len(names) >= 4:
                second_name = names[1]
                first_surname = names[2]
                second_surname = " ".join(names[3:])
        return [first_name,second_name,first_surname, second_surname]
    
    @fields.depends('identifiers')
    def on_change_with_type_tax_identifier(self,name=None):
        types = self._tax_identifier_types()
        if self.identifiers:
            for identifier in self.identifiers:
                if identifier.type in types:
                    return identifier.type

    @fields.depends('identifiers')
    def on_change_with_tax_identifier(self,name=None):
        if self.identifiers:
            return self.get_tax_identifier(name)
    
    @classmethod
    def _tax_identifier_types(cls):
        types = super(Party, cls)._tax_identifier_types()
        types.extend(['co_vat_31','co_vat_13'])
        return types
        
    def get_full_name(self, name=None):
        if self.type_person == 'persona_natural':
            name_list = [self.first_name, self.second_name,
                         self.first_surname,self.second_surname]
            return " ".join([x for x in name_list if x])
        return self.name
    
    @staticmethod
    def get_types_identifier():
        PartyIdentifier = Pool().get('party.identifier')
        return PartyIdentifier.type.selection
    
    def pre_validate(self):
        super(Party, self).pre_validate()
        if not self.identifiers:
            self.raise_user_error("El tercero debe tener un documento de identidad")

class PartyIdentifier(ModelSQL, ModelView):
    "Party Identifier Colombian"
    
    __name__ = "party.identifier"
    
    expedition_city = fields.Many2One("country.subdivision",
        'Ciudad de Expedicion',
        help="Lugar donde fue expedido el documento de identidad",
        domain=[('parent', '!=', None)],
        states = {'invisible':Not(Equal(Eval('type'),'co_vat_13'))}
        )
    check_digit = fields.Function(fields.Integer('DV',
                                                        help='Digito de Verificacion colombiano',
                                                        states = {'invisible':Not(Equal(Eval('type'),'co_vat_31'))})
                                                        ,'on_change_with_check_digit')

    @classmethod
    def __setup__(cls):
        super(PartyIdentifier, cls).__setup__()
        cls.type.selection.extend(CO_IDENTIFIERS)
        table = cls.__table__()
        cls._sql_constraints += [
            ('UniqueIdentifier', Unique(table, table.code,table.type),
                'La identificacion ya existe')
        ]

    
    @fields.depends('type','code')
    def on_change_with_check_digit(self,name=None):
        try:
            import stdnum.co.nit #la version debe ser mayor a la 1.2 o 1.3
        except:
            self.raise_user_error('Se requiere modulo stdnum.co.nit')
        if self.type and self.code:
            if self.type in ('co_vat_31'):
                return int(stdnum.co.nit.calc_check_digit(self.code))
    
    @fields.depends('type', 'party', 'code')
    def check_code(self):
        super(PartyIdentifier, self).check_code()
        if self.type == 'co_vat_31':
            #generalizar multiples paises
            #puede ser por el country del party de la company actual
            try:
                import stdnum.co.nit #la version debe ser mayor a la 1.2 o 1.3
            except:
                self.raise_user_error('Se requiere modulo stdnum.co.nit')
            
            if not stdnum.co.nit.is_valid(self.code + str(self.check_digit)):
                if self.party and self.party.id > 0:
                    party = self.party.rec_name
                else:
                    party = ''
                self.raise_user_error('invalid_vat', {
                    'code': self.code,
                    'party': party,
                })
        if self.type in ['co_vat_12','co_vat_13','co_vat_31'] and not self.code.isdigit():
            if self.party and self.party.id > 0:
                party = self.party.rec_name
            else:
                party = ''
            self.raise_user_error('invalid_vat',{
                'code':self.code,
                'party':party
                })

    @staticmethod
    def default_type_document():
        return 'co_vat_13'
    
    def get_rec_name(self,name=None):
        if self.code:
            return self.code
        elif self.id:
            return self.id
        return ''

class ContactMechanism(sequence_ordered(), ModelSQL, ModelView):
    "Contact Mechanism"
    __name__ = 'party.contact_mechanism'
    __metaclass__ = PoolMeta

    @classmethod
    def __setup__(cls):
        super(ContactMechanism, cls).__setup__()
        cls.type.selection.extend(CO_CONTACT_MECHANISM_TYPES)
