# This file is part of public_transport_fare.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime
from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, fields


class FareResolution(ModelSQL, ModelView):
    'Fare - Resolution'
    __name__ = 'public_transport_fare.fare_resolution'

    name = fields.Char('Resolution')
    authority = fields.Many2One('public_transport.authority', 'Authority', required=True)
    valid_since = fields.Date('Valid Since', required=True)

    #resolucion auxiliar para consulta
    #se forma una lista enlazada
    before_resolution = fields.Many2One('public_transport_fare.fare_resolution', 'Before Resolution',
                                        states={'readonly': True, 'invisible': True})
    #fecha auxiliar para consulta
    before_since = fields.Date('Before Since', states={'readonly': True, 'invisible': True})

    def pre_validate(self):
        if self.before_since \
           and self.valid_since <= self.before_since:
            raise UserError('Invalid since %s before %s' % (str(self.valid_since), str(self.before_since)))
        
    @fields.depends('valid_since', 'authority')
    def on_change_valid_since(self):
        try:
            last_resolution, = self.search([('authority', '=', self.authority)],
                                           order=[('valid_since', 'DESC')],
                                           limit=1)
            self.before_resolution = last_resolution
            self.before_since = last_resolution.valid_since
        except ValueError:
            pass


price_digits = (6, 0)
class FareAttributes(ModelSQL, ModelView):
    'Fare Attributes'
    __name__ = 'public_transport_fare.fare_attributes'
    
    _rec_name = 'price'

    price = fields.Numeric('Price', digits=price_digits, required=True)
    resolution = fields.Many2One('public_transport_fare.fare_resolution', 'Resolution', required=True)
    

    
# reglas para tarifas, la idea es que el modelo se extienda
# y se adicionen atributos para filtrar las tarifas segun lo necesario.
# El modelo es pensado para estar lo mas desacoplado de otros modelos
# esto es no vincularlo directamente, en si es usar asi:
# FareRules.search(...)
class FareRules(ModelSQL, ModelView):
    'Fare Rules'
    __name__ = 'public_transport_fare.fare_rules'


    fare = fields.Many2One('public_transport_fare.fare_attributes', 'Fare', required=True)
    group_route = fields.Many2One('public_transport.group_route', 'Group Route', required=True)
    price = fields.Numeric('Price', digits=price_digits,
                           states={'readonly':True})

    type_mobile_tpp = fields.Many2One('fleet.vehicle.type_tpp', 'Tipo de vehiculo (TPP)',
                                      domain=[('to_vehicle','=',True)])


    @fields.depends('fare')
    def on_change_with_price(self):
        if self.fare:
            return self.fare.price
        return 0
