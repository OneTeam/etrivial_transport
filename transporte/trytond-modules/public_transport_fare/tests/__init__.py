# This file is part of public_transport_fare.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


try:
    from trytond.modules.public_transport_fare.tests.test_public_transport_fare import suite
except ImportError:
    from .test_public_transport_fare import suite

__all__ = ['suite']
