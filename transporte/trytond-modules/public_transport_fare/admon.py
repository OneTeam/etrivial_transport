# This file is part of public_transport_fare.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime
from trytond.exceptions import UserError
from trytond.model import ModelSQL, ModelView, fields
from trytond.wizard.wizard import Wizard, StateView, Button, StateTransition
from trytond.rpc import RPC
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction
import itertools

class VehicleProduction(ModelSQL, ModelView):
    'Vehicle - Production'

    __name__ = 'public_transport_fare.vehicle-production'
    _states = {'readonly': True}
    since = fields.Date('From (Inclusive)', states=_states)
    to  = fields.Date('To (Inclusive)', states=_states)
    group_route = fields.Many2One('public_transport.group_route', 'Group Route', states=_states)    
    vehicle_rel = fields.Many2One('fleet.vehicle', 'Vehicle', states=_states)
    vehicle_trips = fields.Integer('Trips', states=_states)
    vehicle_npassengers = fields.Integer('Passengers', states=_states)
    trips_by_route = fields.Integer('Trips By Route', states=_states)
    trips_out = fields.Integer('Trips Out', states=_states)
    route_fare = fields.Float('Route Fare', states=_states)
    route_nvehicles = fields.Integer('Vehicles in Route', states=_states)
    route_npassengers = fields.Integer('Passengers in Route', states=_states)
    route_production = fields.Float('Route Production', states=_states)
    route_production_prom = fields.Float('Route Production (Prom)', states=_states)
    vehicle_route_production = fields.Float('Vehicle Route Production', states=_states)
    vehicle_price_admon = fields.Float('Price Admon vehicle', states=_states)
    vehicle_price_admon_prom = fields.Float('Price Admon vehicle (Prom)', states=_states)
    route_price_trip = fields.Float('Price of Trip', states=_states)
    total = fields.Float('Total', states=_states)

class FareVehicleGroupRoute(ModelView):
    'Fare Vehicle - Group Route'
    __name__ = 'public_transport_fare.farevehicle-groute'

    vehicle = fields.Many2One('fleet.vehicle', 'Vehicle')
    type_mobile_tpp = fields.Many2One('fleet.vehicle.type_tpp', 'Tipo de Vehicule (TPP)')
    price_route = fields.Float('Price')

    
class GenerateVehicleProductionRoute(ModelView):
    'Generate - VehicleProduction Route'

    __name__ = 'public_transport_fare.generate.vehicle-production.form.route'

    group_route = fields.Many2One('public_transport.group_route', 'Group Route', required=True)
    type_mobile_tpp = fields.Many2One('fleet.vehicle.type_tpp', 'Tipo de vehiculo (TPP)',
                                      help='por defecto se toman todos los tipos de vehiculo, solo usar si se desea restringir al tipo indicado',
                                      domain=[('to_vehicle','=',True)],
                                      required=False)
    ntrips = fields.Integer('# Trips')
    percent_admon = fields.Float('Percent Admin')
    fares = fields.One2Many('public_transport_fare.fare_rules', None, 'Fares',
                            # se fuerza solo add/remove
                            add_remove=[()])


    @fields.depends('group_route', 'type_mobile_tpp')
    def on_change_with_ntrips(self):
        t = Transaction()
        since = t.context.get('since', None)
        to = t.context.get('to', None)

        dispatchs_for_day = GenerateVehicleProduction.search_read_quantity_for_days(self.group_route,
                                                                                    self.type_mobile_tpp,
                                                                                    since,
                                                                                    to)
        total_vehicles = len({r['vehicle'] for r in dispatchs_for_day})
        total_dispatches = sum([r['quantity_dispatches'] for r in dispatchs_for_day])
        if total_vehicles:
            return int(total_dispatches / total_vehicles)
        return None

    @fields.depends('group_route')
    def on_change_with_fares(self):
        pool = Pool()
        t = Transaction()
        FareRules = pool.get('public_transport_fare.fare_rules')
        since = t.context.get('since', None)
        to = t.context.get('to', None)
        if not since:
            return []

        try:
            where = [('group_route', '=', self.group_route),
                     ('fare.resolution.valid_since', '<=', since)]
            if self.type_mobile_tpp:
                where.append(('type_mobile_tpp', '=', self.type_mobile_tpp))

            return list(map(int, FareRules.search(where,
                                                  order=[('fare.resolution.valid_since', 'DESC')],
                                                  limit=1)))
        except ValueError:
            pass
        return []



    @classmethod
    def default_percent_admon(cls):
        return 0.07

class GenerateVehicleProductionForm(ModelView):
    'Generate - VehicleProduction Form'
    __name__ = 'public_transport_fare.generate.vehicle-production.form'

    since = fields.Date('From (inclusive)', required=True)
    to = fields.Date('To (inclusive', required=True)
    
    group_route_admon = fields.One2Many('public_transport_fare.generate.vehicle-production.form.route',
                                        None, 'Admon',
                                        help='Indique los grupos de ruta a generar valores',
                                        context={'since': Eval('since'), 'to': Eval('to')},
                                        depends=['since', 'to'])
    nvehicles = fields.Integer('# Vehicles', states={'readonly': True})
    omit_empty_values = fields.Boolean('Omity Empty Values')

    @classmethod
    def default_omit_empty_values(cls):
        return True

    @fields.depends('group_route_admon')
    def on_change_group_route_admon(self):
        pool = Pool()
        Vehicle = pool.get('fleet.vehicle')
        vids = []
        types_mobiles = [g.type_mobile_tpp for g in self.group_route_admon if g.type_mobile_tpp]
        groutes = [g.group_route for g in self.group_route_admon if g.group_route]
        where = [('routes', 'in', groutes)]
        if types_mobiles:
            where.append(('type_mobile_tpp', 'in', types_mobiles))
        self.nvehicles = len(Vehicle.search(where))


class GenerateVehicleProduction(Wizard):
    'Generate - VehicleProduction'
    __name__ = 'public_transport_fare.generate.vehicle-production'

    start = StateView('public_transport_fare.generate.vehicle-production.form',
                      '',
                      [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Generate', 'generate', 'tryton-ok')
                      ])
    generate = StateTransition()
    
    @classmethod
    def search_read_quantity_for_days(cls, groute,
                                      type_mobile_tpp,
                                      since, to):
        if type_mobile_tpp is not None and not isinstance(type_mobile_tpp, list):
            type_mobile_tpp = [type_mobile_tpp]

        pool = Pool()
        QuantityForDay = pool.get('public_transport.dispatch.quantity_for_day')
        Vehicle = pool.get('fleet.vehicle')

        where_vehicle = [('routes', '=', groute)]
        if type_mobile_tpp:
            where_vehicle.append(('type_mobile_tpp', 'in', type_mobile_tpp))

        vehicles = Vehicle.search(where_vehicle)
        quantity_for_days = QuantityForDay.search_read([
            ('group_route', '=', groute),
            ('vehicle', 'in', vehicles),
            ('date', '>=', since),
            ('date', '<=', to)
        ], fields_names=['id', 'vehicle', 'date', 'quantity_dispatches', 'passengers_by_dispatches'])
        return quantity_for_days

    def transition_generate(self):
        pool = Pool()
        QuantityForDay = pool.get('public_transport.dispatch.quantity_for_day')
        Vehicle = pool.get('fleet.vehicle')

        to_create = []
        for gradmon in self.start.group_route_admon:
            groute = gradmon.group_route
            fares = gradmon.fares
            fares_by_type_mobile_tpp = {f.type_mobile_tpp.id: f for f in fares if f.type_mobile_tpp}
            fares_by_group_route = {f.group_route.id: f for f in fares if f.group_route}
            fares_by_id = {f.id: f for f in fares}
            fares_type_mobile_tpp = [f.type_mobile_tpp for f in fares if f.type_mobile_tpp]

            
            quantity_for_days = self.search_read_quantity_for_days(groute,
                                                                   fares_type_mobile_tpp,
                                                                   self.start.since,
                                                                   self.start.to)
            sum_fields = ['quantity_dispatches', 'passengers_by_dispatches']
            
            #sumatoria agrupada por vehiculo
            quantity_for_vehicle = {}
            #sumataroia de ruta
            sums_total = {k: 0 for k in sum_fields}
            for qday in quantity_for_days:
                if not qday['vehicle'] in quantity_for_vehicle:
                    quantity_for_vehicle[qday['vehicle']] = {k: 0 for k in sum_fields}
                    
                for ksum in sum_fields:
                    if not qday[ksum]:
                        if self.start.omit_empty_values:
                            qday[ksum] = 0
                        else:
                            raise UserError('Despacho %d del %s el %s contiene "%s" en blanco' % (qday['id'], Vehicle(qday['vehicle']).rec_name, qday['date'], ksum))

                    quantity_for_vehicle[qday['vehicle']][ksum] += qday[ksum] or 0
                    sums_total[ksum] += qday[ksum]

            #se resuelve la tarifa por vehiculo
            price_by_vehicle = {}
            for vehicle, _ in quantity_for_vehicle.items():
                oVehicle = Vehicle(vehicle)
                if oVehicle.type_mobile_tpp.id in fares_by_type_mobile_tpp:
                    price_by_vehicle[vehicle] = fares_by_type_mobile_tpp[oVehicle.type_mobile_tpp.id].price
                elif oVehicle.routes.id in fares_by_group_route:
                    price_by_vehicle[vehicle] = fares_by_group_route[oVehicle.routes.id].price
                else:
                    raise UserError('No se pudo resolver tarifa para vehiculo %s' % (oVehicle.internal_code))

            #se totaliza toda la ruta y se adiciona 'admon_price' administracion promedio vehiculo
            route_admon_price = 0
            route_production = 0
            for vehicle, sums in quantity_for_vehicle.copy().items():
                agency_admon = float(price_by_vehicle[vehicle]) * float(gradmon.percent_admon)
                admon_price = round(sums['passengers_by_dispatches'] * agency_admon)
                quantity_for_vehicle[vehicle]['admon_price'] = admon_price
                quantity_for_vehicle[vehicle]['trips_out'] = max(gradmon.ntrips - sums['quantity_dispatches'], 0)
                quantity_for_vehicle[vehicle]['route_production'] = sums['passengers_by_dispatches'] * price_by_vehicle[vehicle]
                route_admon_price += admon_price
                # TODO esto no es necesario ya que este total
                # se puede obtener sumando todo la columna
                route_production += quantity_for_vehicle[vehicle]['route_production'] 

            
            route_admon_price_prom = round(route_admon_price / len(quantity_for_vehicle))
            trip_price = round(route_admon_price_prom / gradmon.ntrips)       
            for vehicle, sums in quantity_for_vehicle.copy().items():
                admin_price_prom = sums['trips_out'] * trip_price
                quantity_for_vehicle[vehicle]['admin_price_prom'] = admin_price_prom
                quantity_for_vehicle[vehicle]['admin_price_total'] = admin_price_prom + sums['admon_price']
                
                
            for vehicle, sums in quantity_for_vehicle.copy().items():
                to_create.append({
                    'since': self.start.since,
                    'to': self.start.to,
                    'group_route': groute.id,
                    'vehicle_rel': vehicle,
                    'vehicle_npassengers': sums['passengers_by_dispatches'],
                    'vehicle_trips': sums['quantity_dispatches'],
                    'trips_by_route': gradmon.ntrips,
                    'trips_out': sums['trips_out'],
                    'route_fare': price_by_vehicle[vehicle],
                    'route_price_trip': trip_price,
                    'route_production': route_production,
                    'route_production_prom': route_admon_price_prom,
                    'route_npassengers': sums_total['passengers_by_dispatches'],
                    'route_nvehicles': len(quantity_for_vehicle),
                    'vehicle_route_production': sums['route_production'],
                    'vehicle_price_admon': sums['admon_price'],
                    'vehicle_price_admon_prom': sums['admin_price_prom'],
                    'total': sums['admin_price_total']
                })
        VehicleProduction.create(to_create)
        return 'end'
