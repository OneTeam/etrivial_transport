# This file is part of public_transport_fare.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .fare import *
from .admon import *

__all__ = ['register']


def register():
    Pool.register(
        FareResolution,
        FareAttributes,
        FareRules,
        VehicleProduction,
        GenerateVehicleProductionRoute,
        GenerateVehicleProductionForm,
        FareVehicleGroupRoute,
        module='public_transport_fare', type_='model')
    Pool.register(
        GenerateVehicleProduction,
        module='public_transport_fare', type_='wizard')
    Pool.register(
        module='public_transport_fare', type_='report')
