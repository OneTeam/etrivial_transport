# metro_civica

modulo para importacion y procesamiento de usos metros desde fuente de hoja de calculo.

## cron create_total_dia

Importa totales de archivos xls y xlsx descargados del metro almacenados en la ruta. 

adicionar lanzador con los siguientes parametros:

  * **model** : metro_civica.metro_civica
  * **function** : create_total_dia
  * **args** : ['']


## Requerimientos

  * pyexcel
  * pyexcel-xls
