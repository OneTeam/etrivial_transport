# This file is part of metro_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, ModelSingleton
from trytond.transaction import Transaction
from datetime import datetime, date, time
from decimal import Decimal

import glob
import logging
import os.path
from decimal import Decimal
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition
from trytond.exceptions import UserWarning, UserError
from re import match
   
logger = logging.getLogger(__name__)

__all__ = ['Metro_Civica','ArchivosImportados','Configuracion','ImportarArchivos']

class Metro_Civica(ModelSQL, ModelView):
    "Metro Civica"
    
    #identificar unico del modelo, es necesario para referencias posteriores
    __name__ = "metro_civica.metro_civica"
    _history = True
    #definicion de attributos del modelo
    fecha = fields.Date("Fecha")
    empresa = fields.Char("Empresa")
    ruta = fields.Char("Ruta")
    subruta = fields.Char("Subruta")
    equipo = fields.Char("Equipo")
    tipo_trayecto = fields.Char("Tipo Trayecto")
    valor_liquidado = fields.Numeric("Valor Liquidado")
    alportador_cantidad = fields.Integer("Alportador Cantidad")
    alportador_valor = fields.Numeric("Alportador Valor")
    conductor_cantidad = fields.Integer("Conductor Cantidad")
    conductor_valor = fields.Numeric("Conductor Valor")
    estudiantealcaldia_cantidad = fields.Integer("Estudiantealcaldia Cantidad")
    estudiantealcaldia_valor = fields.Numeric("Estudiantealcaldia Valor")
    frecuentes_cantidad = fields.Integer("Frecuentes Cantidad")
    frecuentes_valor = fields.Numeric("Frecuentes Valor")
    metroestandar_cantidad = fields.Integer("Metroestandar Cantidad")
    metroestandar_valor = fields.Numeric("Metroestandar Valor")
    operacionesmetro_cantidad = fields.Integer("Operacionesmetro Cantidad")
    operacionesmetro_valor = fields.Numeric("Operacionesmetro Valor")
    pmr_cantidad = fields.Integer("Conductor Cantidad")
    pmr_valor = fields.Numeric("Conductor Valor")
    terceraedad_cantidad = fields.Integer("Terceraedad Cantidad")
    terceraedad_valor = fields.Numeric("Terceraedad Valor")
    total_cantidad = fields.Numeric("Total Cantidad")
    total_valor = fields.Numeric("Total Valor")
    servicios_transporte = fields.Float("Servicios Transporte")
    total = fields.Float("Total")        
    archivo = fields.Char('Archivo',
                          help="Indica la ruta del archivo de donde fue importado este registro.\n"
                          "Esto para efecto de Auditoria"
                          )
    actualizado = fields.Boolean('Actualizado',
                                 help="Indica si el campo ha sido sobreescrito")

    @staticmethod
    def default_pmr_valor():
        return Decimal(0)

    @staticmethod
    def default_pmr_cantidad():
        return 0

    @staticmethod
    def default_terceraedad_valor():
        return Decimal(0)

    @staticmethod
    def default_terceraedad_cantidad():
        return 0

    @staticmethod
    def default_operacionesmetro_valor():
        return Decimal(0)

    @staticmethod
    def default_operacionesmetro_cantidad():
        return 0
    
    @staticmethod
    def default_metroestandar_valor():
        return Decimal(0)
    
    @staticmethod
    def default_metroestandar_cantidad():
        return 0

    @staticmethod
    def default_frecuentes_valor():
        return Decimal(0)

    @staticmethod
    def default_frecuentes_cantidad():
        return 0

    @staticmethod
    def default_estudiantealcaldia_valor():
        return Decimal(0)

    @staticmethod
    def default_estudiantealcaldia_cantidad():
        return 0

    @staticmethod
    def default_conductor_valor():
        return Decimal(0)
    
    @staticmethod
    def default_conductor_cantidad():
        return 0

    @staticmethod
    def default_alportador_valor():
        return Decimal(0)

    @staticmethod
    def default_alportador_cantidad():
        return 0

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('fecha', ) + tuple(clause[1:]),
                ('empresa', ) + tuple(clause[1:]),
                ('ruta', ) + tuple(clause[1:]),
                ('subruta', ) + tuple(clause[1:]),
                ('equipo', ) + tuple(clause[1:]),
                ('tipo_trayecto' )  + tuple(clause[1:])
                #('valor_liquidado' ) : )  tuple(clause[1:])
            ]
            
            
    @classmethod
    def buscar_archivos(cls,ruta):
        return glob.glob(os.path.join(ruta, '**/*.xlsx'), recursive=True) \
            + glob.glob(os.path.join(ruta, '**/*.xls'), recursive=True)

    @classmethod
    def procesar_registros_importados(cls, registros):
        return []

    @classmethod
    def create_total_dia(cls,ruta=None):
        """Importa totales de archivos xls y xlsx descargados del metro almacenados en la ruta. 
        
        @param ruta: ruta donde se almacenan los archivos, si no se indica
         se usara la ruta de configuracion si existe.
        """
        ##Para convertir archivos
        ##
        ##  unoconv -f ods -i FilterOptions="44,34,76,1,1/5/2/1/3/1/4/1" 123.csv
        ##unoconv -f csv -o 123.csv -i FilterOptions="9,34,UNICODE,1" 123.od
        ArchivosImportados = Pool().get('metro_civica.archivos_importados')
        Configuracion = Pool().get('metro_civica.configuracion')
        configuracion = Configuracion.get_singleton()
        
        if not ruta:
            if hasattr(configuracion, 'ruta_archivos'):
                ruta = configuracion.ruta_archivos
            else:
                return
        logger.debug('Hora inicio:{h}'.format(h=datetime.today()))
        logger.debug('Hora inicio busqueda archivos:{h}'.format(h=datetime.today()))
        
        lugares = cls.buscar_archivos(ruta)
        archivos_importados = ArchivosImportados.archivos_importados()
        archivos_a_reimportar = ArchivosImportados.archivos_a_reimportar()
        
        lugares = [x for x in lugares if x not in archivos_importados]
        logger.debug('Hora fin busqueda archivos:{h}'.format(h=datetime.today()))
        registros_importacion = []
        for c in lugares:
            registros_importados = cls.importar_usos(c)
            if not registros_importados:
                continue
            registros_importacion.extend(registros_importados)

            if c not in archivos_a_reimportar:
                ArchivosImportados.crear(c)
            else:
                ArchivosImportados.desmarcar_reimportado(c)
        logger.debug('Hora fin:{h}'.format(h=datetime.today()))
        cls.procesar_registros_importados(registros_importacion)
        cls.save(registros_importacion)
 

    @classmethod   
    def importar_usos(cls, file_name):
        try:
            import pyexcel
        except:
            raise UserError("NO existe el modulo pyexcel")

        if not isinstance(file_name, str):
            raise ArgumentError("se requiere ruta")

        sheet = pyexcel.get_sheet(file_name=file_name)
        fusion_heads = {'AL PORTADOR' : 'alportador',
                       'CONDUCTOR' : 'conductor',
                       'ESTUDIANTE ALCALDIA' : 'estudiantealcaldia',
                       'FRECUENTES' : 'frecuentes',
                       'METRO ESTÁNDAR' : 'metroestandar',
                       'OPERACIONES METRO' : 'operacionesmetro',
                       'PMR' : 'pmr',
                       'TERCERA EDAD' : 'terceraedad'}
        single_heads = {'Fecha' : 'fecha',
                        'Empresa' : 'empresa',
                        'Ruta' : 'ruta',
                        'Subruta' : 'subruta',
                        'Equipo' : 'equipo',
                        'Tipo Trayecto' : 'tipo_trayecto',
                        'Valor Liquidado' : 'valor_liquidado',
                        'Total Cantidad' : 'total_cantidad',
                        'Total Valor' : 'total_valor',
                        'Servicios Transporte' : 'servicios_transporte',
                        'Total' : 'total'}

        pivot_fusion_head_row_index = 0
        pivot_fusion_head_row = None
        for row in sheet:
            nrow = [str(col).upper().strip() for col in row]
            if (set(fusion_heads.keys()) & set(nrow)) != set():
                pivot_fusion_head_row = nrow.copy()
                break
            pivot_fusion_head_row_index += 1
        
        if not pivot_fusion_head_row_index:
            raise RuntimeError('no se ubico fila de cabezeras')

        for check_head, name_attr in single_heads.items():
            if check_head not in sheet.row[pivot_fusion_head_row_index + 1]:
                raise RuntimeError('No se ubica Cabezera %s para %s' % (check_head, file_name))

        row_header = sheet.row[pivot_fusion_head_row_index + 1]
        for col_index, col in enumerate(row_header):
            try:
                row_header[col_index] = single_heads[col]
            except KeyError:
                pass

        for col_index, col in enumerate(pivot_fusion_head_row):
            try:
                model_attr = fusion_heads[col]
                row_header[col_index] = model_attr + "_cantidad"
                row_header[col_index + 1] = model_attr + "_valor"
            except KeyError:
                pass

        sheet.delete_rows(list(range(0, pivot_fusion_head_row_index + 1)))
        sheet.row[0] = row_header
        sheet.name_columns_by_row(0)

        records = sheet.to_records()
        obj_records = []
        for record in records:
            #fin de los registros
            if record['fecha'] == '':
                break

            try:
                current_record, = cls.search([('equipo', '=', record['equipo']),
                                              ('fecha', '=', record['fecha']),
                                              ('valor_liquidado', '=', record['valor_liquidado']),
                                              ('ruta', '=', record['ruta']),
                                              ('subruta', '=', record['subruta'])], limit=1)
                for key, val in record.items():
                    setattr(current_record, key, val)
                obj_records.append(current_record)
            except ValueError:
                default_record = {}
                for name, field in cls._fields.items():
                    if field._type in ['integer', 'numeric']:
                        if name in cls._defaults:
                            default_record[name] = cls._defaults[name]()
                default_record.update(record)
                nrecords = cls.create([default_record])
                obj_records.extend(nrecords)

        return obj_records

    @staticmethod
    def equipo_a_placa(equipo):
        """Convierte un equipo en una placa.
        
        La placa devuelta es en el formato AAA123.
        """

        if equipo[3] == ' ':
            placa = equipo[0:3]+equipo[4:7]
        else:
            placa = equipo[0:6]
        if match('^[A-Za-z]{3}[0-9]{3}$',placa):
            return placa
        else:
            raise Exception('el equipo:{e} no se pudo convertir en placa'.format(e=equipo)) 
    
    @staticmethod
    def clausula_equipo_de_placa(placa):
        """Devuelve la clausuala (domain) necesaria para consultar los equipos a partir de una placa.
        
        La placa en el formato AAA123, devuelta por equipo_a_placa
        """
        clause = ['OR',
                         [('equipo','ilike',placa + '%')],
                         [('equipo','ilike',placa[0:3] + ' ' + placa[3:7] + '%')]
                        ]
        return clause
    
class ArchivosImportados(ModelSQL, ModelView):
        """Archivos importados.
        
        Por el momento solo almacena la ruta al archivo para poder validar si
        se ha importado y no volverla a importar.
        @todo definier el manejo de estos archivos 
        """
        __name__ = "metro_civica.archivos_importados"
        _rec_name = 'ruta'
        ruta = fields.Char('Ruta', required=True, readonly=True)
        reimportar = fields.Boolean('Reimportar',
                                    help="Define si el archivo se debe volver a reimportar o no.\n"
                                    "los archivos no marcados a reimportar se ingnoran en el momento "
                                    "de la importación."
                                    )
        
        @classmethod
        def archivos_importados(self):
            """devuelve una lista de rutas de los archivos menos los marcados como reimportar."""
            rutas = self.search_read([('reimportar','!=',True)],fields_names=['ruta'])
            return [x['ruta'] for x in rutas]
        
        @classmethod
        def archivos_a_reimportar(self):
            """devuelve una lista de rutas de los archivos a reimportar."""
            rutas = self.search_read([('reimportar','=',True)],fields_names=['ruta'])
            return [x['ruta'] for x in rutas]
        
        @classmethod
        def desmarcar_reimportado(self,ruta):
            """Desmarca el campo reimportar del archivo que coincida con la ruta"""
            records = self.search([('ruta','=',ruta)])
            self.write(records, {'reimportar':False})
            self.save(records)
        
        @classmethod
        def crear(self,ruta):
            """Crea un documento con la ruta dada."""
            self.create([{'ruta':ruta}])


class Configuracion(ModelSingleton, ModelSQL, ModelView):
    'Metro_Civica Configuration'
    __name__ = 'metro_civica.configuracion'

    ruta_archivos = fields.Char('Ruta donde estan almacenados los archivos a importar')
    

class ImportarArchivos(Wizard):
    'Importar Archivos xls, xlsx obtenidos de la plataforma metro'
    __name__ = 'metro_civica.importar_archivos'

    start = StateTransition()

    def transition_start(self):
        Metro_Civica = Pool().get('metro_civica.metro_civica')
        Metro_Civica.create_total_dia()
        return 'end'
    
