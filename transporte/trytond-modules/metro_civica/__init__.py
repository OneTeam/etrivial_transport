# This file is part of metro_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .models import *

__all__ = ['register']

def register():
    #registramos los modelos al framework
    Pool.register(
        Metro_Civica,
        ArchivosImportados,
        Configuracion,
		#Documento,
		#PersonaDocumento,
        module="metro_civica", type_="model")
    Pool.register(
        ImportarArchivos,
        module="metro_civica", type_="wizard")
