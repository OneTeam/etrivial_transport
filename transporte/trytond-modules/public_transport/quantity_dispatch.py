# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
from datetime import datetime, timedelta, date, time
from decimal import Decimal

from trytond.model import ModelSQL, ModelView, fields, dualmethod, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Bool
from trytond.wizard.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from sql.aggregate import Count, Aggregate


logger = logging.getLogger(__name__)
__all__ = ['DispatchesQuantityForDay', 'CalculateDispatchesForDayParams',
           'CalculateDispatchesForDay', 'DispatchesQuantityDayGroute',
           'CalculateDispatchesDayGroute', 'DispatchesQuantityForMonth',
           'CalculateDispatchesForMonth']


def colombia_date2utctime(date_):
    """Devuelve el tiempo inicial y final en utc para una fecha colombiana"""
    init_time = datetime.fromordinal(date_.toordinal()) + timedelta(hours=5)
    end_time = init_time + timedelta(hours=24) - timedelta(seconds=1)
    return init_time, end_time


def last_day_for_month(year, month):
    month = 1 if month == 12 else month + 1
    return (date(year, month, 1) - timedelta(days=1)).day


def colombia_month2utctime(year, month):
    """Devuelve el tiempo inicial y final en utc, para un mes colombiano"""
    init_time = datetime.fromordinal(
        date(year, month, 1).toordinal()) + timedelta(hours=5)
    last = date(year, month, last_day_for_month(year, month))
    end_time = datetime.fromordinal(
        last.toordinal()) + timedelta(hours=(24+5))
    return init_time, end_time


def time2bogota(datetime_):
    """devuelve un datetime convertido a colombia"""
    return datetime_ - timedelta(hours=5)


class QuiantityForDayMixin(object):

    group_route = fields.Many2One('public_transport.group_route', 'Ruta',
                                  required=True,
                                  readonly=True)
    date = fields.Date('Fecha',
                       required=True,
                       readonly=True)
    init_time_co = fields.Function(fields.DateTime('hora inicial colombia',
                                                   depends=['date']),
                                   'time_co')
    end_time_co = fields.Function(fields.DateTime('hora inicial colombia',
                                                  depends=['date']),
                                  'time_co')
    calculate_datetime = fields.DateTime(
        'Fecha de cálculos',
        readonly=True,
        help="Como los datos pueden variar en el tiempo, "
        "Este campo indica la fecha en que se realizaron los cálculos")

    def time_co(self, name):
        init_time, end_time = colombia_date2utctime(self.date)
        if name == 'init_time_co':
            return init_time
        elif name == 'end_time_co':
            return end_time

    @dualmethod
    @ModelView.button
    def write_calculation(cls, instances):
        for instance in instances:
            for field, attribute in instance.fields_to_calculation:
                value = getattr(instance, field + "_computed")
                if attribute:
                    value = getattr(value, attribute, None)
                setattr(instance, field, value)
            instance.calculate_datetime = datetime.today()
        cls.save(instances)

    @classmethod
    def _create_from_day(cls, date_, rewrite=False):
        """
        Crea total diario básico con los datos de despachos para la fecha dada
        si se indica rewrite reescribe los totales que existan, de lo
        contrario,falla si encuentra algún registro para el dia indicado.
        """
        if isinstance(date_, str):
            date_ = date.fromisoformat(date_)

        # contar registros existentes para el dia
        exist_records = cls.search_count([('date', '=', date_)])
        if not rewrite and exist_records:
            cls.raise_user_error("Este día ya fue totalizado")
        if exist_records:
            cls._delete_day(date_)
        records = cls.create(cls._get_values_to_create(date_))
        cls.link_disptaches(records)
        cls.write_calculation(records)
        cls.save(records)


    @classmethod
    def _delete_day(cls, date_):
        """elimina todos los totales para la fecha indicada"""
        cls.delete(cls.search(['date', '=', date_]))


    @classmethod
    def create_range(cls, init_date, end_date, rewrite=True):
        """
        Crea totales diarios de despachos para los dias entre las fechas
        dadas incluyendolos
        """

        if isinstance(init_date, str):
            init_date = date.fromisoformat(init_date)
        if isinstance(end_date, str):
            end_date = date.fromisoformat(end_date)
        assert init_date <= end_date, "init_date upper that end_date"
        for x in range(0, (end_date - init_date).days + 1):
            cls._create_from_day(init_date + timedelta(days=x), rewrite)


class DispatchesQuantityForDay(QuiantityForDayMixin, ModelSQL, ModelView):
    """Cantidad de despachos por dia, estadistica diaria por vehiculo por gruta
    """
    # @todo se usan campos function, revisar si mejor se cambian a @property
    # para evitar sobrecargas en servidor
    __name__ = 'public_transport.dispatch.quantity_for_day'

    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              ondelete='RESTRICT',
                              readonly=True,
                              domain=[('tpp', '=', True)],
                              required=True,
                              )
    init_counter = fields.Integer(
        'Registradora Inicial',
        help="La registradora anterior a la primera del día "
        "(Generalmente la del día anterior). Este valor es "
        "Estático y esta almacenado en base de datos",
        readonly=True,)
    end_counter = fields.Integer(
        'Registradora Final',
        readonly=True,
        help=" Este valor es Estático y esta almacenado en base de datos")
    diff_counters = fields.Integer('Diferencia de contadoras',
                                   readonly=True)
    exceptional_quantity_counter = fields.Integer(
        'Excepcion en vueltas de contadora',
        help="Generalmente por revisión o accidente, son "
        "la cantidad de vueltas y no el numero de registradoras")
    passengers_by_dispatches = fields.Integer(
        'Pasajeros',
        readonly=True,
        help="Pasajeros segun información de pasajeros de Despachos")
    passengers_by_counters = fields.Integer(
        'Pasajeros segun registradoras',
        readonly=True,
        help="Pasajeros segun información de registradoras de Despachos, "
        "Registradora final - inicial +/- vueltas novedad registradoras")
    quantity_dispatches = fields.Integer('Viajes',
                                         help="Cantida de viajes",
                                         readonly=True)
    dispatches = fields.One2Many(
        'public_transport.dispatch', 'subtotal_from',
        'Despachos',
        domain=[('vehicle', '=', Eval('vehicle')),
                ('group_route', '=', Eval('group_route'))],
        readonly=True)
    average_passengers_by_counters = fields.Numeric(
        'Promedio Pasajeros contadoras',
        digits=(1, 2),
        readonly=True)
    average_passengers_by_dispatches = fields.Numeric(
        'Promedio Pasajeros despachos',
        digits=(1, 2),
        readonly=True)
    quantity_group_route = fields.Many2One(
        'public_transport.dispatch.quantity_for_day.groute', 'Total Ruta',
        ondelete='RESTRICT')
    subtotal_from = fields.Many2One(
        'public_transport.dispatch.quantity_for_month', 'Subtotal de',
        #ondelete='RESTRICT',
        domain=[('vehicle', '=', Eval('vehicle')),
                ('group_route', '=', Eval('group_route'))]
        )
    door1_in = fields.Integer('Ingresos P1')
    door1_out = fields.Integer('Salidas P1')
    door1_block = fields.Integer('Bloqueos P1')
    door2_in = fields.Integer('Ingresos P2')
    door2_out = fields.Integer('Salidas P2')
    door2_block = fields.Integer('Bloqueos P2')
    count_in = fields.Integer('Ingresos')
    count_out = fields.Integer('Salidas')
    count_block = fields.Integer('Bloqueos')

    @property
    def _exceptional_quantity_counter_computed(self):
        IssueCounter = Pool().get('fleet.vehicle.passenger_counter.issue')
        domain = [('time', '>=', self.init_time_co),
                  ('time', '<', self.end_time_co),
                  ('vehicle', '=', self.vehicle),
                  ('type.affects_passangers', '=', True)]
        issues = IssueCounter.search_read(domain, fields_names=['turns'])
        return sum([issue['turns'] or 0 for issue in issues])

    @property
    def _passengers_by_dispatches_computed(self):
        if self.dispatches:
            return sum(
                [dispatch.passangers or 0 for dispatch in self.dispatches])

    @property
    def _passengers_by_counters_computed(self):
            if isinstance(self.diff_counters, int) and \
               isinstance(self._exceptional_quantity_counter_computed, int):
                return self.diff_counters \
                    - self._exceptional_quantity_counter_computed

    @property
    def _average_passengers_by_counters_computed(self):
        if self.passengers_by_counters and \
           self.quantity_dispatches:
            return Decimal(
                self.passengers_by_counters /
                self.quantity_dispatches).quantize(Decimal('0.00'))

    @property
    def _average_passengers_by_dispatches_computed(self):
        if self.passengers_by_dispatches and \
           self.quantity_dispatches:
            return Decimal(
                self.passengers_by_dispatches /
                self.quantity_dispatches).quantize(Decimal('0.00'))
        else:
            return None

    @property
    def _fields_to_calculation(self):
        """Devuelve una lista de tuplas con los campos que se calculán y
        almacenan en la base de datos y si se toma un atributo de este.
        @attention: para todos estos campos debe existir otro campo igual
         con el sufijo '_computed'
        @attention: importa el orden
        """
        return ['exceptional_quantity_counter',
                'passengers_by_dispatches',
                'passengers_by_counters',
                'average_passengers_by_counters',
                'average_passengers_by_dispatches',
                ]

    def get_counters(self, name=None):
        """ Devuelve las contadoras del dia dado mas la anterior registradora en
        orden cronológico
        @attention usarse para ver registradoras relacionadas con el total
        """
        Counters = Pool().get('fleet.vehicle.passenger_counter')
        counters = Counters.search([('vehicle', '=', self.vehicle),
                                    ('time', '>', self.init_time_co),
                                    ('time', '<=', self.end_time_co)],
                                   order=[('time', 'DESC')],
                                   )
        if counters:
            counters.extend(Counters.search([('vehicle', '=', self.vehicle),
                                             ('time', '<', counters[-1].time)],
                                            limit=1)
                            )
        return [counter.id for counter in counters]

    @classmethod
    def __setup__(cls):
        super(DispatchesQuantityForDay, cls).__setup__()
        cls._order = [('date', 'DESC'), ('group_route', 'ASC'),
                      ('vehicle', 'ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('date_vehicle_group_route_key', Unique(table,
                                                    table.date,
                                                    table.group_route,
                                                    table.vehicle),
                "Solo debe haber un registro para la fecha, vehiculo y grupo "
                "de ruta dados")
        ]
        cls._buttons.update({'write_calculation':
                             {'invisible': Bool(Eval('calculate_datetime')),
                              'help': "Escribe los cálculos en la base de "
                              "datos",
                              'icon': 'tryton-launch'
                              }
                             })

    @property
    def domain_done_dispatches(self):
        """ Devuelve el domain de los despachos que se tendrán en cuenta en la
        finalizacion del vehiculo
        """
        return [('vehicle', '=', self.vehicle),
                ('init_time', '>', self.init_time_co),
                ('init_time', '<=', self.end_time_co),
                ('state', '=', 'done'),
                ('group_route', '=', self.group_route)
                ]

    @dualmethod
    @ModelView.button
    def write_calculation(cls, instances):
        for instance in instances:
            for field in instance._fields_to_calculation:
                setattr(
                    instance,
                    field,
                    getattr(instance, "_" + field + "_computed")
                    )
            instance.calculate_datetime = datetime.today()
        cls.save(instances)

    @classmethod
    def _create_from_day(cls, date_, rewrite=False):
        """
        Crea total diario básico con los datos de despachos para la fecha dada
        si se indica rewrite reescribe los totales que existan, de lo
        contrario,falla si encuentra algún registro para el dia indicado.
        """
        if isinstance(date_, str):
            date_ = date.fromisoformat(date_)

        # contar registros existentes para el dia
        exist_records = cls.search_count([('date', '=', date_)])
        if not rewrite and exist_records:
            cls.raise_user_error("Este día ya fue totalizado")
        if exist_records:
            cls._delete_day(date_)
        logger.info(f"inicia creacion {date_}")
        records = cls.create(cls._get_values_to_create(date_))
        logger.info(f"inicia link_dispatches {date_}")
        cls.set_counters(records)
        logger.info(f"inicia geo_passenger {date_}")
        cls.set_geo_passenger(records)
        logger.info(f"inicia write_claculation {date_}")
        cls.write_calculation(records)
        logger.info(f"inicia save {date_}")
        cls.save(records)
        logger.info(f"finaliza {date_}")

    def _set_geo_passenger(self):
        "consulta y almacena los pasajeros georeferenciados para el despacho"
        GeoPassengerLog = Pool().get('datos_sonar.geo_passenger_log')
        if getattr(self.vehicle,'sonar_mobile'):
            init_time = datetime.combine(self.date, time.min) + timedelta(hours=5) #@todo usar timezone
            end_time = datetime.combine(self.date, time.max) + timedelta(hours=5) #@todo usar timezone
            door1, door2, count = GeoPassengerLog.get_totals_passenger_for_time(init_time, end_time, self.vehicle.sonar_mobile.id)
            self.door1_in, self.door1_out, self.door1_block = door1
            self.door2_in, self.door2_out, self.door2_block = door2
            self.count_in, self.count_out, self.count_block = count


    @dualmethod
    def set_geo_passenger(cls, instances):
        """consulta y almacena los pasajeros georeferenciados para el despacho"""
        for instance in instances:
            instance._set_geo_passenger()
        cls.save(instances)

    @classmethod
    def _get_values_to_create(cls, date_):
        """Consulta los despachos y devuelve una lista de valores con los
        registros que se deben crear para la fecha indicada.
        select vehicle,group_route from public_transport_dispatch
        where state='done' and
        init_time > '2019-04-02 19:00:00' and end_time <= '2019-04-03 19:00:00'
        group by vehicle,group_route;
        """
        Dispatch = Pool().get('public_transport.dispatch')
        cursor = Transaction().connection.cursor()

        init_time, end_time = colombia_date2utctime(date_)
        t_dispatch = Dispatch.__table__()

        class ArrayAgg(Aggregate):
            __slots__ = ()
            _sql = 'ARRAY_AGG'

        columns = [t_dispatch.vehicle,
                   t_dispatch.group_route,
                   Count(t_dispatch.id).as_('quantity_dispatches'),
                   ArrayAgg(t_dispatch.id).as_('dispatches')]

        where = ((t_dispatch.init_time >= init_time) &
                 (t_dispatch.end_time <= end_time) &
                 (t_dispatch.state == 'done'))
        group_by = (t_dispatch.vehicle,
                    t_dispatch.group_route,
                    )
        order_by = (t_dispatch.group_route, t_dispatch.vehicle)
        d_select = t_dispatch.select(*columns,
                                     where=where,
                                     group_by=group_by,
                                     order_by=order_by)
        cursor.execute(*d_select)
        values = cls._fetch2list_dict(cursor.fetchall(), cursor.description)
        for value in values:
            value['date'] = date_
            value['dispatches'] = [('add', value['dispatches'])]
        return values

    @staticmethod
    def _fetch2list_dict(cursor_fetchall, cursor_description):
        headers = [x.name for x in cursor_description]
        values = []
        for row in cursor_fetchall:
            record = {}
            for header in headers:
                record[header] = row[headers.index(header)]
            values.append(record)
        return values

    @dualmethod
    def set_counters(cls, instances):
        for instance in instances:
            instance._set_counters()
        cls.save(instances)

    def _set_counters(self):
        """
        Calcula y almacena los campos relacionados con registradoras.
        @attention: no se diferencia por grupo de ruta
        """
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        cursor = Transaction().connection.cursor()
        t_counter = Counter.__table__()
        init_time, end_time = colombia_date2utctime(self.date)
        columns = [t_counter.counter]
        where = ((t_counter.time >= init_time) &
                 (t_counter.time <= end_time) &
                 (t_counter.vehicle == self.vehicle.id))
        order_by = (t_counter.time.desc)
        select_counter = t_counter.select(*columns,
                                          where=where,
                                          order_by=order_by,
                                          limit=1)
        cursor.execute(*select_counter)
        end_counter = self._fetch2list_dict(
            cursor.fetchall(), cursor.description)

        where_init = ((t_counter.time < init_time) &
                      (t_counter.vehicle == self.vehicle.id)
                      )
        select_counter_first = t_counter.select(
            *columns,
            where=where_init,
            order_by=order_by,
            limit=1)
        cursor.execute(*select_counter_first)
        init_counter = self._fetch2list_dict(cursor.fetchall(),
                                             cursor.description)
        if init_counter and end_counter:
            self.init_counter = init_counter[0]['counter']
            self.end_counter = end_counter[0]['counter']
            self.diff_counters = Counter._substract(self.init_counter, self.end_counter)

    @staticmethod
    def _counters2passengers(ordered_counters):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        r = []
        for idx in range(0, len(ordered_counters) - 1):
            r.append(Counter._substract(ordered_counters[idx],
                                        ordered_counters[idx + 1]))
        return sum(r)


class DispatchesQuantityDayGroute(QuiantityForDayMixin, ModelSQL, ModelView):
    """ Cantidad de despachos por dia, por grupo de ruta"""
    # @todo se usan campos function, revisar si mejor se cambian a @property
    # para evitar sobrecargas en servidor

    __name__ = 'public_transport.dispatch.quantity_for_day.groute'

    quantities = fields.One2Many('public_transport.dispatch.quantity_for_day',
                                 'quantity_group_route', 'Cantidad Ruta')
    quantity_dispatches = fields.Integer('Viajes',
                                         help="Cantida de viajes",
                                         readonly=True)
    quantity_dispatches_computed = fields.Function(fields.Integer(
        'Viajes computado',
        depends=['quantities']),
        'get_quantity')

    passengers_by_dispatches = fields.Integer('Pasajeros',
                                              readonly=True,
                                              help="Pasajeros segun "
                                              "información de pasajeros de "
                                              "Despachos")
    passengers_by_dispatches_computed = fields.Function(
        fields.Integer('Pasajeros computado', depends=['quantities']),
        'get_quantity'
        )
    passengers_by_counters = fields.Integer('Pasajeros segun registradoras',
                                            readonly=True,
                                            help="Pasajeros segun información "
                                            "de registradoras de Despachos, "
                                            "Registradora final - inicial +/- "
                                            "vueltas novedad registradoras")
    passengers_by_counters_computed = fields.Function(
        fields.Integer('Pasajeros segun registradoras computado',
                       depends=['quantities']),
        'get_quantity')
    average_passengers_by_counters = fields.Numeric(
        'Promedio Pasajeros contadoras',
        digits=(1, 2),
        readonly=True)
    average_passengers_by_counters_computed = fields.Function(
        fields.Numeric('Promedio Pasajeros segun contadoras calculado',
                       digits=(1, 2),
                       depends=['quantities',
                                'passengers_by_counters_computed',
                                'quantity_dispatches_computed']
                       ),
        'get_average_passengers_by_counters_computed')
    average_passengers_by_dispatches = fields.Numeric(
        'Promedio Pasajeros despachos',
        digits=(1, 2),
        readonly=True
        )
    average_passengers_by_dispatches_computed = fields.Function(
        fields.Numeric('Promedio Pasajeros segun despachos calculado',
                       digits=(1, 2),
                       depends=['quantities',
                                'passengers_by_dispatches_computed',
                                'quantity_dispatches_computed']),
        'get_average_passengers_by_dispatches_computed')

    def get_quantities_field_list(self, field):
        """devuelve una lista con los valores del campo para de cada cantidad
        """
        if self.quantities:
            return [getattr(d, field, None) for d in self.quantities
                    if getattr(d, field, None)]
        else:
            return []

    def get_quantity(self, name=None):
        suf = '_computed'
        field = name.replace(suf, '')
        lo = self.get_quantities_field_list(field)
        r = sum(lo)
        return r

    @staticmethod
    def _decimal_div(a, b, format_='0.00'):
        if b:
            return Decimal(a / b).quantize(Decimal(format_))
        else:
            return Decimal('0.00').quantize(Decimal(format_))

    def get_average_passengers_by_counters_computed(self, name=None):
        if self.quantity_dispatches_computed \
           and self.passengers_by_counters_computed:
            return self._decimal_div(self.passengers_by_counters_computed,
                                     self.quantity_dispatches_computed)

    def get_average_passengers_by_dispatches_computed(self, name=None):
        if self.quantity_dispatches_computed \
           and self.passengers_by_dispatches_computed:
            return self._decimal_div(self.passengers_by_dispatches_computed,
                                     self.quantity_dispatches_computed)

    @classmethod
    def __setup__(cls):
        super(DispatchesQuantityDayGroute, cls).__setup__()
        cls._order = [('date', 'DESC'), ('group_route', 'ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('date_vehicle_group_route_key', Unique(table, table.date,
                                                    table.group_route),
                "Solo debe haber un registro para la fecha, vehiculo y grupo "
                "de ruta dados")
        ]
        cls._buttons.update({'write_calculation':
                             {'invisible': Bool(Eval('calculate_datetime')),
                              'help': "Escribe los cálculos en la base de "
                              "datos",
                              'icon': 'tryton-launch'
                              }
                             })

    @property
    def fields_to_calculation(self):
        """Devuelve una lista de tuplas con los campos que se calculán y
        almacenan en la base de datos y si se toma un atributo de este.
        @attention: para todos estos campos debe existir otro campo igual
         con el sufijo '_computed'
        """
        return [('passengers_by_dispatches', None),
                ('passengers_by_counters', None),
                ('quantity_dispatches', None),
                ('average_passengers_by_counters', None),
                ('average_passengers_by_dispatches', None)
                ]

    @dualmethod
    @ModelView.button
    def link_disptaches(cls, instances):
        Quantity = Pool().get('public_transport.dispatch.quantity_for_day')
        for instance in instances:
            quantities = Quantity.search(
                [('date', '=', instance.date),
                 ('group_route', '=', instance.group_route)],
                order=[('date', 'DESC')])
            for quantity in quantities:
                quantity.quantity_group_route = instance.id
            Quantity.save(quantities)

    @dualmethod
    def unlink_disptaches(cls, instances):
        Quantity = Pool().get('public_transport.dispatch.quantity_for_day')
        for instance in instances:
            quantities = Quantity.search(
                [('date', '=', instance.date),
                 ('group_route', '=', instance.group_route)],
                order=[('date', 'DESC')])
            for quantity in quantities:
                quantity.quantity_group_route = None
            Quantity.save(quantities)

    @classmethod
    def _get_values_to_create(cls, date_):
        """Consulta los despachos y devuelve una lista de valores con los
        registros que se deben crear para la fecha indicada.
        select group_route from public_transport_dispatch
        where state='done' and
        init_time > '2019-04-02 19:00:00' and end_time <= '2019-04-03 19:00:00'
        group by group_route;
        """
        Dispatch = Pool().get('public_transport.dispatch')
        init_time, end_time = colombia_date2utctime(date_)
        cursor = Transaction().connection.cursor()
        t_dispatch = Dispatch.__table__()

        columns = [t_dispatch.group_route]

        where = ((t_dispatch.init_time >= init_time) &
                 (t_dispatch.end_time <= end_time) &
                 (t_dispatch.state == 'done'))
        group_by = (t_dispatch.group_route,)
        order_by = (t_dispatch.group_route,)
        d_select = t_dispatch.select(*columns,
                                     where=where,
                                     group_by=group_by,
                                     order_by=order_by)
        cursor.execute(*d_select)
        headers = [x.name for x in cursor.description]
        rows = cursor.fetchall()
        values = []
        for row in rows:
            record = {}
            for header in headers:
                record[header] = row[headers.index(header)]
            record['date'] = date_
            values.append(record)
        return values

    @classmethod
    def _delete_day(cls, date_):
        cls.unlink_disptaches(cls.search(['date', '=', date_]))
        super(DispatchesQuantityDayGroute, cls)._delete_day(date_)


class CalculateDispatchesMixin(object):

    start = StateView('public_transport.dispatch.quantity_for_day.params',
                      '', [
                        Button('Cancelar', 'end', 'tryton-cancel'),
                        Button('Calcular', 'calculate', 'tryton-launch',
                               default=True),
                        ])
    calculate = StateTransition()

    def end(self):
        return 'reload'


class CalculateDispatchesForDay(CalculateDispatchesMixin, Wizard):
    """ Calcula los totales de despachos por dias"""

    __name__ = 'public_transport.dispatch.quantity_for_day.calculate'

    def transition_calculate(self):
        Dispatchfd = Pool().get('public_transport.dispatch.quantity_for_day')
        Dispatchfd.create_range(self.start.init_date, self.start.end_date,
                                self.start.rewrite)
        return 'end'


class CalculateDispatchesDayGroute(CalculateDispatchesMixin, Wizard):
    """ Calcula los totales de despachos por dias por ruta"""

    __name__ = 'public_transport.dispatch.quantity_for_day.groute.calculate'

    def transition_calculate(self):
        Dispatchfd = Pool().get(
            'public_transport.dispatch.quantity_for_day.groute')
        Dispatchfd.create_range(self.start.init_date, self.start.end_date,
                                self.start.rewrite)
        return 'end'


class CalculateDispatchesForDayParams(ModelView):
    """Formulario para el asistente que calcula los totales de despachos por
    dias"""

    __name__ = 'public_transport.dispatch.quantity_for_day.params'

    init_date = fields.Date('Fecha inicial',
                            required=True)
    end_date = fields.Date('Fecha final',
                           domain=[('end_date', '>=', Eval('init_date'))],
                           required=True)
    rewrite = fields.Boolean('Sobreescribir',
                             help="Si se indica fallará en"
                             "caso de que el día ya este totalizado")

    @staticmethod
    def default_rewrite():
        return True


class QuiantityForMonthMixin(object):

    group_route = fields.Many2One('public_transport.group_route', 'Ruta',
                                  required=True,
                                  readonly=True)
    month = fields.Integer('Mes',
                           domain=[('month', '>=', 0),
                                   ('month', '<=', 12)],
                           required=True,
                           readonly=True)
    year = fields.Integer('Año',
                          domain=[('year', '>', 1970),
                                  ('year', '<', 2050)],
                          required=True,
                          readonly=True)
    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              ondelete='RESTRICT',
                              readonly=True,
                              domain=[('tpp', '=', True)],
                              required=True,
                              )
    init_date = fields.Function(fields.Date('Fecha inicial',
                                            depends=['date']),
                                'get_dates')
    end_date = fields.Function(fields.Date('Fecha final',
                                           depends=['date']),
                               'get_dates')
    calculate_datetime = fields.DateTime(
        'Fecha de cálculos',
        readonly=True,
        help="Como los datos pueden variar en el tiempo, "
        "Este campo indica la fecha en que se realizaron los cálculos")
    subtotals = fields.One2Many(
        'public_transport.dispatch.quantity_for_day', 'subtotal_from',
        'Subtotales',
        readonly=True,
        domain=[('vehicle', '=', Eval('vehicle')),
                ('group_route', '=', Eval('group_route'))],
        order=[('date', 'ASC')]
        )

    def get_dates(self, name):
        if self.year and self.month:
            if name == 'init_date':
                return date(self.year, self.month, 1)
            elif name == 'end_date':
                return date(self.year, self.month,
                            last_day_for_month(self.year, self.month))

    @fields.depends('year', 'month')
    def on_change_with_date(self, name=None):
        if self.year and self.month:
            return date(self.year, self.month, 1)

    @classmethod
    def _delete_month(cls, year, month):
        """elimina todos los totales del mes indicado"""
        cls.delete(cls.search([('year', '=', year),
                               'month', '=', month]))

    @classmethod
    def _create_month(cls, year, month, rewrite=False):
        pass

    @classmethod
    def create_range(cls, init_date, end_date, rewrite=True):
        """
        Crea totales diarios de despachos para los dias entre las fechas
        dadas incluyendolos
        """
        def months_from_range(init_date, end_date):
            """ devuelve una lista de meses (tupla año,mes) entre las fechas
            indicadas incluyéndolas """
            assert init_date < end_date
            r = []
            for y in range(init_date.year, end_date.year + 1):
                init_month = init_date.month if y == init_date.year else 1
                end_month = end_date.month if y == end_date.year else 12
                for m in range(init_month, end_month + 1):
                    r.append((y, m))
            return r
        if isinstance(init_date, str):
            init_date = date.fromisoformat(init_date)
        if isinstance(end_date, str):
            end_date = date.fromisoformat(end_date)
        assert init_date <= end_date, "init_date upper that end_date"
        for year, month in months_from_range(init_date, end_date):
            cls._create_month(year, month, rewrite)

    @staticmethod
    def _link_subtotals(instances, subtotal_model):
        Subtotal = Pool().get(subtotal_model)
        for instance in instances:
            subtotals = Subtotal.search(instance.domain_subtotals,
                                        order=[('date', 'DESC')])
            for subtotal in subtotals:
                subtotal.subtotal_from = instance.id
            Subtotal.save(subtotals)


class DispatchesQuantityForMonth(QuiantityForMonthMixin, ModelSQL, ModelView):
    """Cantidad de despachos por mes, estadística mensual por vehiculo por gruta
    """

    __name__ = 'public_transport.dispatch.quantity_for_month'

    init_counter = fields.Integer(
        'Registradora Inicial',
        help="Registradora inicial del primer registro del mes.",
        readonly=True)
    end_counter = fields.Integer(
        'Registradora Final',
        readonly=True)
    diff_counters = fields.Integer('Diferencia de contadoras')
    exceptional_quantity_counter = fields.Integer(
        'Excepcion en vueltas de contadora',
        help="Generalmente por revisión o accidente, son "
        "la cantidad de vueltas y no el numero de registradoras")
    passengers_by_dispatches = fields.Integer(
        'Pasajeros',
        readonly=True,
        help="Pasajeros segun información de pasajeros de Despachos")
    passengers_by_counters = fields.Integer(
        'Pasajeros segun registradoras',
        readonly=True,
        help="Pasajeros segun información de registradoras de Despachos, "
        "Registradora final - inicial +/- vueltas novedad registradoras")
    quantity_dispatches = fields.Integer('Viajes',
                                         help="Cantida de viajes",
                                         readonly=True)
    average_passengers_by_counters = fields.Numeric(
        'Promedio Pasajeros contadoras',
        digits=(1, 2),
        readonly=True)
    average_passengers_by_dispatches = fields.Numeric(
        'Promedio Pasajeros despachos',
        digits=(1, 2),
        readonly=True)
#     quantity_group_route = fields.Many2One(
#         'public_transport.dispatch.quantity_for_month.groute', 'Total Ruta',
#         ondelete='RESTRICT')

    def get_diff_counters(self, name=None):
        if isinstance(self.init_counter, int) \
           and isinstance(self.end_counter, int):
            return self.end_counter - self.init_counter

    @classmethod
    def __setup__(cls):
        super(DispatchesQuantityForMonth, cls).__setup__()
        cls._order = [('year', 'DESC'), ('month', 'DESC'),
                      ('group_route', 'ASC'),
                      ('vehicle', 'ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('date_vehicle_group_route_key', Unique(table,
                                                    table.year,
                                                    table.month,
                                                    table.group_route,
                                                    table.vehicle),
                "Solo debe haber un registro para el año, mes, vehiculo y "
                "grupo de ruta dados")
        ]
        cls._buttons.update({'calculate_data':
                             {'invisible': Bool(Eval('calculate_datetime')),
                              'help': "Totaliza subtotales",
                              'icon': 'tryton-launch'
                              }
                             })

    @property
    def domain_subtotals(self):
        return [('date', '>=', self.init_date),
                ('date', '<=', self.end_date),
                ('vehicle', '=', self.vehicle),
                ('group_route', '=', self.group_route)]

    @dualmethod
    @ModelView.button
    def link_subtotals(cls, instances):
        subtotal_model = 'public_transport.dispatch.quantity_for_day'
        cls._link_subtotals(instances, subtotal_model)

    @classmethod
    def _get_values_to_create(cls, year, month):
        """Consulta los totales diarios y devuelve una lista de valores con los
        registros que se deben crear para el mes indicado.
        """
        Subtotal = Pool().get('public_transport.dispatch.quantity_for_day')
        init_date = date(year, month, 1)
        end_date = date(year, month, last_day_for_month(year, month))
        cursor = Transaction().connection.cursor()
        t_subtotal = Subtotal.__table__()

        columns = [t_subtotal.vehicle,
                   t_subtotal.group_route]

        where = ((t_subtotal.date >= init_date) &
                 (t_subtotal.date <= end_date))
        group_by = (t_subtotal.vehicle,
                    t_subtotal.group_route,
                    )
        order_by = (t_subtotal.group_route, t_subtotal.vehicle)
        d_select = t_subtotal.select(*columns,
                                     where=where,
                                     group_by=group_by,
                                     order_by=order_by)
        cursor.execute(*d_select)
        headers = [x.name for x in cursor.description]
        rows = cursor.fetchall()
        values = []
        for row in rows:
            record = {}
            for header in headers:
                record[header] = row[headers.index(header)]
            record['year'] = year
            record['month'] = month
            values.append(record)
        return values

    @classmethod
    def fields_to_calculate(cls):
        return [('exceptional_quantity_counter',
                 'exceptional_quantity_counter'),
                ('passengers_by_dispatches', 'passengers_by_dispatches'),
                ('passengers_by_counters', 'passengers_by_counters'),
                ('quantity_dispatches', 'quantity_dispatches'),
                ('diff_counters', 'diff_counters'),
                ('average_passengers_by_counters',
                 'average_passengers_by_counters'),
                ('average_passengers_by_dispatches',
                 'average_passengers_by_dispatches')
                ]

    @dualmethod
    @ModelView.button
    def calculate_data(cls, instances):
        for instance in instances:
            if instance.subtotals:
                instance.init_counter = instance.calculate_init_counter
                instance.end_counter = instance.calculate_end_counter
                for field, subtotal_field in instance.fields_to_calculate():
                    setattr(instance, field,
                            instance.get_sum_subtotals_field(subtotal_field))
                instance.calculate_datetime = datetime.today()
        cls.save(instances)

    @property
    def calculate_init_counter(self):
        if self.subtotals:
            return self.subtotals[0].init_counter

    @property
    def calculate_end_counter(self):
        if self.subtotals:
            return self.subtotals[-1].end_counter

    def get_sum_subtotals_field(self, subtotal_field):
        return sum(self.get_subtotals_list_value_field(subtotal_field))

    def get_subtotals_list_value_field(self, subtotal_field):
        if self.subtotals:
            return [getattr(d, subtotal_field, None)
                    for d in self.subtotals
                    if getattr(d, subtotal_field, None)
                    ]
        else:
            return []

    @classmethod
    def _create_month(cls, year, month, rewrite=False):
        """
        Crea total diario básico con los datos de despachos para la fecha dada
        si se indica rewrite reescribe los totales que existan, de lo
        contrario,falla si encuentra algún registro para el dia indicado.
        """
        # contar registros existentes para el dia
        exist_records = cls.search_count([('year', '=', year),
                                          ('month', '=', month)])
        if not rewrite and exist_records:
            cls.raise_user_error("Este mes ya fue totalizado")
        if exist_records:
            cls._delete_month(year, month)
        records = cls.create(cls._get_values_to_create(year, month))
        cls.link_subtotals(records)
        cls.calculate_data(records)
        cls.save(records)


class CalculateDispatchesForMonth(CalculateDispatchesMixin, Wizard):
    """ Calcula los totales de despachos por Mes"""

    __name__ = 'public_transport.dispatch.quantity_for_month.calculate'

    def transition_calculate(self):
        Dispatchfm = Pool().get('public_transport.dispatch.quantity_for_month')
        Dispatchfm.create_range(self.start.init_date, self.start.end_date,
                                self.start.rewrite)
        return 'end'
