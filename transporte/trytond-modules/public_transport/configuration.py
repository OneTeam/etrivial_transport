# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval

__all__ = ['Configuration']

class Configuration(ModelSingleton,ModelSQL,ModelView):
    '''Configuracion'''
    
    __name__ = "public_transport.configuration"
    
    route_default_country = fields.Many2One('country.country', 'Country',
                            required=True)
    route_default_departament = fields.Many2One("country.subdivision",'Subdivision',
                                domain=[('country', '=', Eval('route_default_country', -1)),
                                    ('parent', '=', None),],
                                required=True
                                )
    route_default_city = fields.Many2One("country.subdivision",'Municipio',
                        domain=[
                            ('country', '=', Eval('route_default_country', -1)),
                            ('parent', '=', Eval('route_default_departament',-1)),
                            ],
                        required=True
                        )
