# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields, Unique

__all__ = ['Authority']

class Authority(ModelSQL,ModelView):
    '''Autoridades de control.'''
    
    __name__ = "public_transport.authority"
    
    name = fields.Char('Nombre',
                       required=True)
    description = fields.Char('Descripción')
    generates_operation_card = fields.Boolean('Genera tarjetas de operacion')
    generates_vehicle_registration = fields.Boolean('Genera Licencias de Transito')
    
    @classmethod
    def __setup__(cls):
        super(Authority, cls).__setup__()
        cls._order = [('name','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
                ('name_key', Unique(table, table.name),
                    'El nombre de la autoridad debe ser unico')
            ]
