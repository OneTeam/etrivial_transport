# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
import logging
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not

logger = logging.getLogger(__name__)

__all__ = ['Employee']

_categories_of_licenses = [
    ('A1','A1, antes 1'),
    ('A2','A2, antes 2'),
    ('B1','B1, antes 3'),
    ('A1','A1, antes 1'),
    ('A1','A1, antes 1'),
    ('A1','A1, antes 1'),
    ('A1','A1, antes 1'),
    ]

class Employee(ModelSQL, ModelView):
    'Employee'
    
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'
    
    is_driver = fields.Boolean('Es conductor')
    driving_licence = fields.Char('Licencia de Conduccion',
                                  states={'required':Bool(Eval('is_driver')),
                                         'invisible':Not(Bool(Eval('is_driver')))
                                         }
                                  )
    category_driving_licence = fields.Many2One('public_transport.category_driving_licence','Categoria del pase',
                                               states={'required':Bool(Eval('is_driver')),
                                                      'invisible':~Bool(Eval('is_driver')),
                                                      },
                                               ondelete='RESTRICT',
                                               domain=[]
                                               )
    date_end_licence = fields.Date('Fecha de vencimiento del pase',
                                   states={'required':Bool(Eval('is_driver')),
                                         'invisible':~Bool(Eval('is_driver'))
                                         }
                                   )
    is_supernumerary = fields.Boolean('Es supernumerario',
                                      help='Indica que el conductor puede manejar cualquier vehiculo del propietario',
                                      states={
                                         'invisible':~Bool(Eval('is_driver'))
                                         }
                                      )
    secundary_employer = fields.Many2One('party.party','Segundo Empleador',
                                         ondelete='RESTRICT',
                                         states={'required':Bool(Eval('is_driver')),
                                                'invisible':~Bool(Eval('is_driver'))
                                         }
                                         )
    valid_secundary_employer = fields.Function(fields.Boolean('Segundo empleador valido',
                                                              states={'invisible':~Bool(Eval('is_driver'))}),
                                               'on_change_with_valid_secundary_employer')
    vehicles = fields.One2Many('public_transport.proprietary_contract_driver','driver','Vehiculos',
                               help=("Vehiculos asignados al conductor,\n"
                               "en realidad son los contratos activos del segundo empleador"),
                               states={'required':Bool(Eval('is_driver')),
                                      'invisible':~Bool(Eval('is_driver'))
                                     },
                               )
    temporal_vehicles = fields.One2Many('public_transport.proprietary_contract_driver','driver','Vehiculos temporales',
                               help=("Vehiculos temporales asignados al conductor"),
                                states={'invisible':~Bool(Eval('is_driver'))},
                               )
    

    @fields.depends('secundary_employer')
    def on_change_with_valid_secundary_employer(self, name=None):
        if self.secundary_employer:
            Contract = Pool().get('public_transport.proprietary')
            contracts = Contract.search([('proprietary','=',self.secundary_employer),
                                         #('contract.end_date','=',None)#@todo ajustar a fechas de vigencia
                                         ])
            return True if contracts else False 
        else:
            return False
    
    def pre_validate(self):
        super(Employee, self).pre_validate()
        self.check_secundary_employer()
        self.check_vehicles()
        #if self.temporal_vehicles:
            #self.check_temporal_vehicles()
    
    def check_secundary_employer(self):
        if self.secundary_employer:
            Contract = Pool().get('public_transport.proprietary')
            contracts = Contract.search([('proprietary','=',self.secundary_employer),
                                         #('contract.end_date','=',None)
                                         ])
            if not contracts:
                self.raise_user_error('El segundo empleador no tiene contratos vigentes')

    def check_vehicles(self):
        for vehicle in self.vehicles:
            if vehicle.temp_link:
                self.raise_user_error("Los vehiculos no deben ser asignados de forma temporal")
            proprietaries_id = [proprietary.proprietary.id for proprietary in vehicle.contract.proprietaries]
            logger.info("second: {}, proprietaries: {}".format(self.secundary_employer.id,proprietaries_id))
            if self.secundary_employer.id not in proprietaries_id:
                self.raise_user_error("No puede asignar vehiculos de un propietario diferente al segundo empleador")
