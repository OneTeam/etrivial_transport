# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
la lista de vehiculos será asi:
[(vehiculo_subruta_1,vehiculo_subruta_2,...,vehiculo_subruta_n),
...,
]
donde el turno es el index de la lista + 1

en la Ruta ejemplo existen 3 tipos de rotacion:
* rotacion reversa: rotacion continua iniciando en la posición(idx) -n, donde n es la cantidad
  de dias del mismo tipo transcurridos desde el orden de vehiculos de referencias y el dia
  a generar. Aplica para dias ordinarios y sábados aunque con listas diferentes.
* rotacion continua iniciando en la posicion (idx) n, donde n es la cantidad
  de dias del mismo tipo transcurridos desde el orden de vehiculos de referencias y el dia
  a generar. Aplica para domingos y festivos.

La Ruta ejemplo también tiene una rotación de subruta, donde la tupla de cada turno se rota n veces
 donde n es la cantidad de dias tipo transcurridos 
La Ruta ejemplo también tiene 2 vehículos que no son contados para los domintos (b285 b280),
supongo que son vehículos inactivos y los domingos hay pocos carros y por esto la programacion
es mas precisa.

'''
import logging

from datetime import timedelta
from itertools import cycle
from math import ceil

__all__ = ['PlanedDispatchGenerator']

logger = logging.getLogger(__name__)

#no modificar
HOLIDAY, SATURDAY, ORDINARY = ['holiday','saturday','ordinary']

#tipos de rotacion en empresa ejemplo
#la lista de vehiculos se devuelve un puesto por día
ROTACION_INVERSA = 'rotacion_inversa' 
#la lista de vehiculos se toma desde una posición siguiente a la anterior rotacion
ROTACION_SIGUIENTE = 'rotacion_siguiente'  

TIPOS_ROTACION = {HOLIDAY:ROTACION_SIGUIENTE,
                  SATURDAY:ROTACION_INVERSA,
                  ORDINARY:ROTACION_INVERSA
                  }


def counter(limit,init=0,steps=1):
    """Contador desde 0 hasta el limit, reiniciandoce cada vez que llega al final"""
    x = init
    while True:
        yield x
        x = init if x > limit else x + steps
        x = x if x <= limit else init

def to_idx(idx,len_):
    """Devuelve un idx valido de un idx mayor al largo de la lista(len_)
    o menor que el negativo del largo de la lista (-len_)"""
    if 0 <= idx < len_:
        return idx
    elif idx > 0:
        return idx % len_
        pass
    elif idx < 0:
        r = (idx % len_) - len_
        return 0 if r == -len_ else r

def saltar_lista(list_,idx=0):
    """devuelve una lista con todos los elementos y el orden de la lista
    pero iniciando en idx"""
    idx = to_idx(idx, len(list_))
    if idx:
        if idx < 0:
            list_ = list_[idx:] + list_[:idx]
        else:
            end = idx if idx == 1 else -idx+1
            list_ = list_[idx:] + list_[:end]
    return list_
    
def loop_list(list_,idx=0):
    """Devuelve valores ciclicos de una lista, si se indica idx se iniciará en
    la posicion dada, la cual puede ser negativa.
    El idx se toma como en las listas los idx positivos empiezan en la posicion
    0 y los idx negativos en -1"""
    return cycle(saltar_lista(list_, idx))

def rotacion_reversa(lista,n,salto=0):
    """retorna n elementos desde la posicion (salto) haciendo rotacion inversa
    de la lista, el salto puede ser negativo"""
    idx = -salto
    k = loop_list(lista,idx)
    return [next(k) for _ in range(0,n)]

def rotacion_continua(lista,n,salto=0):
    """retorna n elementos desde la posicion (salto) haciendo rotacion continua
    de la lista, el salto puede ser negativo.
    >>> orden10 = [(141, 74), (181, 182), (174, 186), (192, 191), (220, 223), (71, 235), (236, 241), (242, 243), (250, 249), (294, 251), (258, 253), (246, 257), (268, 261), (270, 263), (272, 267), (274, 269), (276, 271), (286, 273), (298, 277), (289, 287), (290, 313), (291, 292), (293, 296), (295, 288), (297, 300), (299, 266), (301, 310), (225, 304), (307, 306), (311, 308), (303, 346), (315, 314), (316, 35), (41, 317), (53, 320), (59, 338), (309, 312), (73, 360), (103, 72)]
    >>> rotacion_continua(orden10,21,3)
[(297, 300),
 (299, 266),
 (301, 310),
 (225, 304),
 (307, 306),
 (311, 308),
 (303, 346),
 (315, 314),
 (316, 35),
 (41, 317),
 (53, 320),
 (59, 338),
 (309, 312),
 (73, 360),
 (103, 72),
 (141, 74),
 (181, 182),
 (174, 186),
 (192, 191),
 (220, 223),
 (71, 235)]
    >>> rotacion_continua(orden10,21,0)
[(141, 74),
 (181, 182),
 (174, 186),
 (192, 191),
 (220, 223),
 (71, 235),
 (236, 241),
 (242, 243),
 (250, 249),
 (294, 251),
 (258, 253),
 (246, 257),
 (268, 261),
 (270, 263),
 (272, 267),
 (274, 269),
 (276, 271),
 (286, 273),
 (298, 277),
 (289, 287),
 (290, 313)]    
    """
    idx = (salto * n) + 1 if salto else salto
    k = loop_list(lista,idx)
    return [next(k) for _ in range(0,n)]


def correr_sublistas(lista,salto=0):
    """corre las sublistas de la lista n cantidad de veces,
    donde n = salto"""
    r = []
    for item in lista:
        r.append(saltar_lista(item,salto))
    return r
    

def leer_por_bloques(lista,largo=1):
    x = 0 
    while True:
        if x > len(lista) - 1:
                x = 0
        r = lista[x:x+largo]
        falta = largo - len(r)
        if falta:
            r += [None for x in range(0,falta)]
        yield r
        x += largo


def separar_lista(lista,cantidad_grupo):
    """parte la lista en elementos agruapdos de a n (cantidad_grupo),
    si para el último elemento no alcanzan los elementos de la lista
    se completa con None
    >>>agrupar_lista([1,2,3,4,5,6,7,8,9],2)
    [[1,2],[3,4],[5,6],[7,8],[9,None]]
    """
    largo = ceil(len(lista)/cantidad_grupo)
    lector = leer_por_bloques(lista, cantidad_grupo)
    return [next(lector) for _ in range(0,largo)]

#@deprected
def correr_lista(lista,salto):
    negativo = False
    if salto < 0:
        salto = salto * -1
        negativo = True
    if salto > len(lista):
        salto = salto % len(lista)
    if negativo:
        salto = len(lista) - salto
    return lista[salto:] + lista[:salto]

#@deprected
def correr_lista_con_sublistas(lista,salto):
    r = []
    for sublista in lista:
        r.append(correr_lista(sublista, salto))
    return r


FUNCION_ROTACION = {HOLIDAY:rotacion_continua,
                    SATURDAY:rotacion_reversa,
                    ORDINARY:rotacion_reversa
                    }


class GrupoRuta(object):
    def __init__(self,id_,ids_rutas,vehiculos_ordenados,fechas_vehiculos_ordenados,
                 cantidad_de_turnos,rotar_ruta=False):
        self.id = id_
        self.ids_rutas = ids_rutas
        self.cantidad_rutas = len(ids_rutas)
        self.vehiculos_ordenados = vehiculos_ordenados
        self.fechas_vehiculos_ordenados = fechas_vehiculos_ordenados
        self.cantidad_de_turnos = cantidad_de_turnos
        self.rotar_ruta = rotar_ruta
        
    def obtener_vehiculos_ordenados(self,tipo):
        return self.vehiculos_ordenados[tipo]
    
    def obtener_fecha_vehiculos_ordenados(self,tipo):
        return self.fechas_vehiculos_ordenados[tipo]
    
    def obtener_cantidad_de_turnos(self,tipo):
        return self.cantidad_de_turnos[tipo]
        
class Dia(object):
    def __init__(self,fecha):
        self.fecha = fecha


class GeneradorDeDespachos(object):
    def __init__(self,funcion_festivos,
                 funcion_cuantos_dias_festivos,
                 funcion_cuantos_dias_ordinarios,
                 funcion_cuantos_dias_sabados,
                 funcion_obtener_horarios):
        self.funcion_festivos = funcion_festivos
        self.funcion_cuantos_dias_ordinarios = funcion_cuantos_dias_ordinarios
        self.funcion_cuantos_dias_sabados = funcion_cuantos_dias_sabados
        self.funcion_cuantos_dias_festivos = funcion_cuantos_dias_festivos
        self.funcion_obtener_horarios = funcion_obtener_horarios
    
    def establecer_grupo_de_ruta(self,id_,ids_rutas,vehiculos_ordenados,fechas_vehiculos_ordenados,
                 cantidad_de_turnos,rotar_ruta=False):
        self.grupo_ruta = GrupoRuta(id_,ids_rutas,vehiculos_ordenados,fechas_vehiculos_ordenados,
                 cantidad_de_turnos,rotar_ruta)
        
    def iniciar_dia(self,fecha):
        self.dia = Dia(fecha)
        self.dia.tipo = self.tipo_de_dia()
        self.dia.fecha_inicial = self.grupo_ruta.obtener_fecha_vehiculos_ordenados(self.dia.tipo)
        self.dia.dias_tipo_transcurridos = self.calcular_dias_tipos_transcurridos(self.dia.tipo,
                                                                                  self.dia.fecha_inicial,
                                                                                  self.dia.fecha)
        self.dia.rotacion_vehiculos = self.dia.dias_tipo_transcurridos
        #logger.info("Fecha inicial: {},fecha: {}".format(self.dia.fecha_inicial,fecha))
#         self.dia.rotacion_vehiculos = self.dia.dias_tipo_transcurridos
#         if self.dia.tipo == HOLIDAY:
#             self.dia.rotacion_vehiculos = self.dia.dias_tipo_transcurridos
#         elif self.dia.fecha == self.dia.fecha_inicial:
#             self.dia.rotacion_vehiculos = 0
#         else:
#         self.dia.rotacion_vehiculos = -self.dia.dias_tipo_transcurridos 
#             if self.dia.dias_tipo_transcurridos == 1: #exclutendo el dia 1n aun no entiendo porque no aplica
#                 self.dia.rotacion_vehiculos = -1
#             else:
#                 self.dia.rotacion_vehiculos = 1 - self.dia.dias_tipo_transcurridos
#         self.dia.tipo_rotacion = TIPOS_ROTACION[self.dia.tipo]
        self.dia.vehiculos_ordenados = self.grupo_ruta.obtener_vehiculos_ordenados(self.dia.tipo)
        self.dia.cantidad_turnos = self.grupo_ruta.obtener_cantidad_de_turnos(self.dia.tipo)
        self.dia.horarios = self.obtener_horarios_del_dia()

    
    def obtener_horarios_del_dia(self):
        r = {}
        for ruta in self.grupo_ruta.ids_rutas:
            r[ruta] = self.funcion_obtener_horarios(self.dia.tipo,self.dia.fecha,ruta)
        return r
        
        
    def borrar_dia(self):
        self.dia = None
        
    def tipo_de_dia(self):
        #domingo
        if int(self.dia.fecha.strftime('%w')) == 0:
            return HOLIDAY
        elif self.funcion_festivos(self.dia.fecha):
            return HOLIDAY
        elif int(self.dia.fecha.strftime('%w')) == 6:
            return SATURDAY
        else:
            return ORDINARY
    
    def calcular_dias_tipos_transcurridos(self,tipo,fecha_inicial,fecha_final):
        if tipo == HOLIDAY:
            qdays = self.funcion_cuantos_dias_festivos(fecha_inicial,fecha_final)
        elif tipo == ORDINARY:
            qdays = self.funcion_cuantos_dias_ordinarios(fecha_inicial,fecha_final)
        elif tipo == SATURDAY:
            qdays = self.funcion_cuantos_dias_sabados(fecha_inicial,fecha_final)
        qdays = qdays - 1 if qdays else 0 #se resta 1 porque la funcion de dias transcurridos devuelve 1 para el mismo dia
        return qdays
    
    def generar_contenido(self):
        self.dia.contenido = self.obtener_esquema_contenido(self.dia.cantidad_turnos,
                                                            self.grupo_ruta.ids_rutas)
        self.agregar_horarios_a_contenido()
        self.agregar_vehiculos_a_contenido()
    
    def obtener_esquema_contenido(self,turnos,rutas):
        r = {}
        for turno in range(1,turnos + 1):
            r[turno]={}
            for ruta in rutas:
                r[turno][ruta] = {'vehiculo':None,
                                'horarios':[]
                                }
        return r
    
    def agregar_horarios_a_contenido(self):
        for ruta, turnos in self.dia.horarios.items():
            for turno, horarios in turnos.items():
                self.dia.contenido[turno][ruta]['horarios'] = horarios
    
    def agregar_vehiculos_a_contenido(self):
        rutas = saltar_lista(self.grupo_ruta.ids_rutas, self.dia.rotacion_vehiculos)
        vehiculos = separar_lista(self.dia.vehiculos_ordenados, self.grupo_ruta.cantidad_rutas)
        logger.info("vehiculos originales: {}".format(vehiculos))
        f_vehiculos = FUNCION_ROTACION[self.dia.tipo]
        vehiculos = f_vehiculos(vehiculos,self.dia.cantidad_turnos,self.dia.dias_tipo_transcurridos)
#        if self.dia.tipo_rotacion == ROTACION_SIGUIENTE:
#            vehiculos = correr_lista(vehiculos,(self.dia.dias_tipo_transcurridos * self.dia.cantidad_turnos) -1 + 1)
#        else: #aplica para el resto, de momento a sabados y ordinarios
#            vehiculos = correr_lista(vehiculos,self.dia.rotacion_vehiculos)
        
        logger.info("vehiculos rotados ({}*{})-1 veces({} dias): {}".format(self.dia.dias_tipo_transcurridos,
                                                                        self.dia.cantidad_turnos,
                                                                        self.dia.rotacion_vehiculos,
                                                                        vehiculos))
            
        for idx in range(0,self.dia.cantidad_turnos - 1):
            turno = idx + 1
            for idx2 in range(0,self.grupo_ruta.cantidad_rutas):
                self.dia.contenido[turno][rutas[idx2]]['vehiculo'] = vehiculos[idx][idx2]
    
    def generar_values_tryton(self):
        r = []
        for turno,rutas in self.dia.contenido.items():
            for ruta,datos in rutas.items():
                r.extend(self.generar_despachos_por_horario(ruta,
                                                            datos['vehiculo'],
                                                            turno,
                                                            datos['horarios'],
                                                            self.grupo_ruta.rotar_ruta))
        #logger.info(self.dia.contenido)
        return r
        
    def generar_despachos_por_horario(self,ruta,vehiculo,turno,horarios,rotar_ruta=False):
        r = []
        ruta_inicial = ruta
        if rotar_ruta:
            rotador_ruta = loop_list(self.grupo_ruta.ids_rutas, self.grupo_ruta.ids_rutas.index(ruta))
        for horario in horarios:
            if rotar_ruta:
                ruta = next(rotador_ruta)
            r.append(self.generar_despacho(ruta,vehiculo,turno,ruta_inicial,horario))
        return r
    
    def generar_despacho(self,ruta,vehiculo,turno,ruta_turno,horario):
        return {
                'group_route':self.grupo_ruta.id,
                'route':ruta,
                'type':self.dia.tipo,
                'vehicle':vehiculo,
                'turn_route':ruta_turno,
                'time':horario,
                'turn':turno
                }
    
    def generar_rango(self,fecha_inicial,fecha_final):
        r = []
        diferencia = (fecha_final - fecha_inicial).days
        for x in range(0,diferencia + 1):
            r.extend(self.generar_dia(fecha_inicial + timedelta(days=x)))
        return r
    
    def generar_dia(self,fecha):
        self.iniciar_dia(fecha)
        self.generar_contenido()
        r = self.generar_values_tryton()
        self.dia = None
        return r
