# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
from datetime import datetime, date, timedelta

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pyson import Eval, Bool
from trytond.pool import Pool, PoolMeta

logger = logging.getLogger(__name__)


__all__ = ['Vehicle','VehicleTypeTpp','VehicleActionRation',
           'VehicleCorporateImage','VehicleOperationCard',
           'FleetConfiguration','VehiclePassengerCounter',
           'VehicleRegistration', 'IssuePassengerCounterType',
           'IssuePassengerCounter']

_depends = ['tpp']

class Vehicle(ModelSQL,ModelView):
    """Vehículo."""
    
    __metaclass__ = PoolMeta
    __name__ = 'fleet.vehicle'
    
    tpp = fields.Boolean('TPP',
                         help="Transporte Publico de Pasajerosn\n"
                         "indica si este vehiculo tiene esta homologación"
                         )
    type_mobile_tpp = fields.Many2One('fleet.vehicle.type_tpp', 'Tipo de vehiculo (TPP)',
                            states={'required':Bool(Eval('tpp')),'visible':Bool(Eval('tpp'))},
                            domain=[('to_vehicle','=',True)],
                            )
    action_ration = fields.Many2One('fleet.vehicle.action_ration','Radio de acción',
                                    help="Define el radio de acción definido en la"
                                    "tarjeta de operación",
                                    states={'required':Bool(Eval('tpp')),'visible':Bool(Eval('tpp'))},
                                    domain=[('active','=',True)]
                                    )
    pmr = fields.Boolean('Plataforma PMR',
                         help="Plataforma Pasajeros con Movilidad Reducida.\n"
                         "Si la tiene agrega un pasajero en la capacidad total"
                         )
    driver_door = fields.Boolean('Puerta de Conductor',
                                 help="Indica si el vehículo tiene puerta de conductor"
                                 )
    standing_passangers = fields.Integer('Pasajeros de pie',
                                         help="Cantidad de pasajeros parados"
                                         )
    seated_passangers = fields.Integer('Pasajeros Sentados',
                                       help="Cantidad de pasajeros sentados")
    max_passangers = fields.Function(fields.Integer('Total Pasajeros',
                                                    readonly=True,  
                                                    help="Pasajeros sentados + "
                                                    "pasajeros de pie + pasajeros"
                                                    "movilidad reducida",
                                                    ),'on_change_with_max_passangers')
    corporate_image = fields.Many2One('fleet.vehicle.corporate_image','Imagen Corporativa',
                                      help="Apariencia externa del vehículo",
                                      required=True,
                                      domain=[()]
                                      )
    routes = fields.Many2One('public_transport.group_route', 'Rutas',
                        help="Grupo de Rutas a la cual pertenece el vehiculo.\n"
                        "Debe ingresar primero el tipo de vehiculo,\n"
                        "cantidad de pasajeros y puertas",
                        ondelete='RESTRICT',
                        domain=[('action_ration','=',Eval('action_ration'))],
#                         domain=[('type_mobile_tpp.min_passangers','<=',Eval('max_passangers')),
#                                 ('type_mobile_tpp.max_passangers','>=',Eval('max_passangers')),
#                                 ('type_mobile_tpp.min_doors','<=',Eval('doors')),
#                                 ('type_mobile_tpp.max_doors','>=',Eval('doors')),
#                                 ],
                        required=False,
                        )
    internal_code = fields.Function(fields.Char('Código Interno',depends=_depends+['contract']),
                                    'get_internal_code',
                                    searcher='search_internal_code',
                                    )
    contract = fields.Function(fields.Many2One('public_transport.proprietary_contract','Contrato',
                                               help="Contrato vigente para este vehículo",
                                               depends=_depends + ['contracts']
                                               ),'get_contract',searcher='search_contract')
    contracts = fields.One2Many('public_transport.proprietary_contract','vehicle','Contratos',
                                order=[('valid_to','DESC')],
                                depends=_depends
                                )
    #drivers = fields.Many2Many('company.employee.vehicles','vehicle','driver','conductores')
    operation_cards = fields.One2Many('fleet.vehicle.operation_card','vehicle','Tarejetas de operacion',
                                      order=[('valid_to','DESC')],
                                      )
    operation_card = fields.Function(fields.Many2One('fleet.vehicle.operation_card','Tarjeta de operacion'),
                                     'get_operation_card')
    passangers_counter = fields.Function(fields.Integer('Contadora de pasajeros'),'last_passangers_counter')
    
    def last_passangers_counter(self,name=None):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        last = Counter.search([('vehicle','=',self.id)],
                       limit=1,
                       order=[('time','DESC')])
        if last:
            return last[0].counter
    
    def get_operation_card(self,name):
        if self.operation_cards:
            return self.operation_cards[0].id
        return None
        
    def get_contract(self,name):
        if self.contracts:
            end = 2 if len(self.contracts) > 2 else len(self.contracts)
            for x in range(0,end):
                if self.contracts[x].valid_from and self.contracts[x].valid_to:
                    if self.contracts[x].valid_from <= date.today() <= self.contracts[x].valid_to:
                        return self.contracts[x].id
        return None
            
    def get_internal_code(self,name):
        if self.contract:
            return self.contract.internal_code
        else:
            return None
    
    @classmethod
    def search_internal_code(cls, name, clause):
        """analiza si el texto pasado es numerico y si dicho valor
        numerico esta precedido o no por m|M|b|B
        
        
        ejemplos: b035, B35,35 
        @attention: de momento hay que ingresar el numero interno completo
        @attention: también busca en contratos viejos y no actuales
        """
        text = clause[2]
        text = text.strip('%')
 
        clause_first = text[0] if text[0] in "bBmM" else None
        if clause_first:
            clause_number = int(text[1:]) if str(text[1:]).isdigit() else None
        else:
            clause_number = int(text) if str(text).isdigit() else None
        
        if (not clause_first and not clause_number) or \
           (clause_first and not clause_number and len(text)> 1):
            return [('id','=',-1)]
        
        internal_code_domain = ['AND',]
        if clause_first:
            internal_code_domain.append(('contracts.internal_letter',clause[1],clause_first))
        if clause_number:
            internal_code_domain.append(('contracts.internal_number','=',clause_number))
        return internal_code_domain
    
    @classmethod
    def search_contract(cls, name, clause):
        return ['OR',
                ('contracts.code', ) + tuple(clause[1:]),
            ]
    
    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Vehicle, cls).search_rec_name(name=name,clause=clause)
        domain = ['OR',domain,
                ('internal_code',) + tuple(clause[1:]),
                ('routes',) + tuple(clause[1:]),
                ]
        return domain

    
    @staticmethod
    def default_tpp():
        return True
    
    def get_rec_name(self, name):
        rec = str(self.licence_plate)
        if self.internal_code:
            rec += "-" + self.internal_code
        return rec
    
    
    
    @fields.depends('seated_passangers','standing_passangers','pmr')
    def on_change_with_max_passangers(self,name=None):
        count = 0
        count += self.seated_passangers if self.seated_passangers else 0
        count += self.standing_passangers if self.standing_passangers else 0
        count += 1 if self.pmr else 0
        return count
    
    @classmethod
    def __setup__(cls):
        super(Vehicle, cls).__setup__()
        cls._order = [('tpp','ASC'),('active','ASC'),('type_mobile_tpp','ASC'),('licence_plate','ASC')]
        #migracion ALTER TABLE fleet_vehicle ALTER COLUMN type_mobile_tpp TYPE int USING type_mobile_tpp::integer;

    def pre_validate(self):
        super(Vehicle, self).pre_validate()
        if self.tpp:
            if not self.check_licence_plate_co(
                    self.licence_plate,
                    only_common_vehicle=True):
                self.raise_user_error("Placa de Vehicuos de TPP debe "
                                      "ser en el formato ABC123")
#         Comentado porque en la realidad no aplican estas cantidades
#         if not self.check_max_passangers_type_vehicle():
#             self.raise_user_error("El tipo de vehiculo {t} no es para {c} pasajeros: {p}".format(p=self.licence_plate,
#                                                                                                        t=self.type_mobile_tpp.name,
#                                                                                                        c=self.max_passangers
#                                                                                                        ))
        #Se deja aunque depronto toque quitar esta validación porque
        # en la realidad no aplican estos criterios
        if not self.check_doors_type_vehicle():
            self.raise_user_error("El tipo de vehiculo no es para esa cantidad de puertas")
            

    def check_max_passangers_type_vehicle(self):
        """Verifica que la cantidad de pasajeros coincidan con los limites
        del tipo de vehículo."""
        
        if self.tpp:
            if self.max_passangers <= 0:
                return False
            if self.type_mobile_tpp.min_passangers <= self.max_passangers \
             <= self.type_mobile_tpp.max_passangers:
                return True
            else:
                return False
        else:
            return True
    
    def check_doors_type_vehicle(self):
        """Verifica que la cantidad de puertas del vehiculo coincida
        con los limites del vehiculo"""
        
        if self.tpp:
            if self.doors and self.type_mobile_tpp.min_doors \
            and self.type_mobile_tpp.min_doors:
                if self.doors <= 0:
                    return False
                if self.type_mobile_tpp.min_doors <= self.doors <= \
                self.type_mobile_tpp.max_doors:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return True
        
    

class VehicleTppMixin(object):
    """Vehículo."""
    
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              ondelete='RESTRICT',
                              required=True,
                              domain=[('active','=',True),
                                      ('tpp','=',True)]
                              )

class FleetConfiguration(ModelSQL,ModelView):
    """Configuraciones de Flota"""
    
    __metaclass__ = PoolMeta
    __name__ = 'fleet.configuration'
    
    operation_card_years = fields.Integer('Años de las tarjetas de operacion')
    
    @staticmethod
    def default_operation_card_years():
        return 2
       
class VehicleTypeTpp(ModelSQL,ModelView):
    """Tipo de vehiculo de Transporte Publico de Pasajeros(tpp).
    
    @attention: es de configuración no se debería modificar por el usuario,
     solo desde archios xml 
    """
    
    __name__ = 'fleet.vehicle.type_tpp'
    
    name = fields.Char('Nombre',
                       required=True)
    code = fields.Char('Código',
                       help="Letra usada para construir el numero interno",
                       required=False)
    active = fields.Boolean('Activo')
    min_passangers = fields.Integer('Pasajeros mínimos',
                                    help="Capacidad de pasajeros",
                                    required=True)
    max_passangers = fields.Integer('Pasajeros máximos',
                                    help="Capacidad de pasajeros",
                                    required=True,
                                    domain=[('max_passangers','>=',Eval('min_passangers'))])
    min_doors = fields.Integer('Puertas minimas',
                               help="No se debe tener en cuenta la del conductor",
                               required=True,
                               )
    max_doors = fields.Integer('Puertas máximas',
                               help="No se debe tener en cuenta la del conductor",
                               required=True,
                               domain=[('max_doors','>=',Eval('min_doors'))]
                               )
    to_vehicle = fields.Boolean('Aplica para Vehiculos')
    to_route = fields.Boolean("Aplica para rutas")
    notes = fields.Text('Observaciones')
    
    @classmethod
    def __setup__(cls):
        super(VehicleTypeTpp, cls).__setup__()
        cls._order = [('name','ASC'),('code','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_state_name', Unique(table, table.name),
                'Vehiculo_state: El nombre de la estado debe ser unico'),
        ]
    
    def pre_validate(self):
        super(VehicleTypeTpp, self).pre_validate()
        if self.to_vehicle:
            if not self.code:
                self.raise_user_error("Los tipos de vehiculos para vehiculos deben tener codigo")
            elif self.code not in ('B','M'):
                self.raise_user_error("El código de tipos de vehiculos para vehiculos debe ser B o M")
    

class VehicleActionRation(ModelSQL,ModelView):
    """Radios de Acción de los vehiculos"""
    
    __name__ = 'fleet.vehicle.action_ration'
    
    name = fields.Char('Nombre',
                       required=True)
    code = fields.Char('Código',
                       required=False)
    active = fields.Boolean('Activo')
    one_city = fields.Boolean('Un solo municipio',
                                   help="define si este radio de acción"
                                   "solo incluye un municipio"
                                   )
    
    @staticmethod
    def default_active():
        return True
    
    @classmethod
    def __setup__(cls):
        super(VehicleActionRation, cls).__setup__()
        cls._order = [('active','ASC'),('name','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_action_ration_name', Unique(table, table.name),
                'Vehiculo_action_ration: El nombre del radio de accion debe ser unico'),
            ('vehicle_action_ration_code', Unique(table, table.code),
                'Vehiculo_action_ration: El codigo del radio de accion debe ser unico'),
        ]

class VehicleCorporateImage(ModelSQL,ModelView):
    """Imagenes Corporativas"""
    
    __name__ = 'fleet.vehicle.corporate_image'
    
    name = fields.Char('Nombre',
                       required=True)
    active = fields.Boolean('Activa')
    notes = fields.Text('Observaciones')
    
    @staticmethod
    def default_active():
        return True
    
    @classmethod
    def __setup__(cls):
        super(VehicleCorporateImage, cls).__setup__()
        cls._order = [('active','ASC'),('name','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('vehicle_corporate_image_name', Unique(table, table.name),
                'Vehiculo_corporate_image: El nombre de la imagen corporativa debe ser unico'),
        ]

class VehicleOperationCard(VehicleTppMixin,ModelSQL,ModelView):
    """Tarjetas de Operacion."""
    
    __name__ = 'fleet.vehicle.operation_card'
    
    _rec_name = 'code'
    
    code = fields.Char('Número',
                       required=True)
    authority = fields.Many2One('public_transport.authority','Autoridad',
                                required=True,
                                domain=[('generates_operation_card','=',True)]
                                )
    expedition_date = fields.Date('Fecha de expedicion',
                                  required=True,
                                  domain=[('expedition_date','<=',Eval('valid_from'))]
                                  )
    valid_from = fields.Date('Valida desde',
                             required=True,
                             domain=[('valid_from','<',Eval('valid_to')),
                                     ]
                             )
    valid_to = fields.Date('Valida hasta',
                           required = True,
                           domain=[('valid_to','>',Eval('valid_from')),
                                 ])
    note = fields.Char('Observacion')
    
    @fields.depends('valid_from')
    def on_change_with_end_date(self):
        Configuration = Pool().get('fleet.configuration')
        configuration = Configuration.get_singleton()
        if self.valid_from and hasattr(configuration, 'operation_card_years'):
            year = self.valid_from.year + configuration.operation_card_years
            return self.valid_from.replace(year=year)
        else:
            return None
    
    @classmethod
    def __setup__(cls):
        super(VehicleOperationCard, cls).__setup__()
        cls._order = [('valid_to','DESC'),('vehicle','ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('unique_key', Unique(table, table.code,table.authority),
                'VehicleOperationCard: El numero debe ser unico para cada autoridad'),
        ]

class VehiclePassengerCounter(VehicleTppMixin,ModelSQL,ModelView):
    """Registro de la contadora de pasajeros"""
    
    __name__ = 'fleet.vehicle.passenger_counter'
    
    time = fields.DateTime('Fecha',
                           required=True,
                           help="Fecha y hora del momento en que se registra")
    time_co = fields.Function(fields.DateTime('Fecha colombiana'),'get_time_co')
    counter = fields.Integer('Conteo',
                             required=True,
                             domain=[('counter','>=',0)])
    note = fields.Char('Observacion')
    
    def get_rec_name(self, name):
        #verificar si no se debe poner la fecha para evitar confusiones
        return str(self.counter)
    
    def get_time_co(self,name):
        if self.time:
            return self.time - timedelta(hours=5)
    
    @classmethod
    def __setup__(cls):
        super(VehiclePassengerCounter, cls).__setup__()
        cls._order = [('time','DESC')]
        table = cls.__table__()
        #comentado porque se requiere que el cliente de despacho
        #tenga en cuenta este constraint
#         cls._sql_constraints += [
#             ('Unique_time_vehicle', Unique(table, table.time,table.vehicle),
#                 "Solo debe haber una registradora para una hora y un vehiculo dado")
#         ]
        
    
    @classmethod
    def search_rec_name(cls, name, clause):
        text = clause[2].strip('%')
        counter = int(text) if str(text).isdigit() else None
        domain = ['OR',
                  ('vehicle',)+tuple(clause[1:]),
                  ]
        if counter:
            domain.append(('counter','=',counter))
        return domain

        
    
    @staticmethod
    def default_time():
        return datetime.today()
    
    def diff_counter(self,counter2):
        """Devuele la diferencia con respecto a otro contador del mismo vehiculo
        
        @param counter2: VehicleRegisterCounter object
        """
        assert self.vehicle == counter2.vehicle,"No se pueden operar contadoras de vehiculos diferentes"  
        
        #ordenando por fecha para comparar
        if self.time >= counter2.time:
            counter1 = self
        else:
            counter1 = counter2
            counter2 = self
        
        registers = self.search([('time','<=',counter1.time),
                                 ('time','>=',counter2.time),
                                 ('vehicle','=',self.vehicle.id)],
                                 order=[('time','ASC')])
        assert len(registers) > 0,"Error al calcular diferencias de contadores fleet.vehicle.passenger_counter"
        elements = []
        for x in range(0,len(registers)-1):
            elements.append(self._substract(registers[x].counter,registers[x+1].counter))
        return sum(elements)

    
            
        
    @staticmethod
    def _substract(init,end):
        """Devuelve la diferencia entre 2 valores
        
        Los valores tienen un limite desconocido y vuelvea iniciarse en 0
        porque son contadores mecanicos.
        Son desconocidos porque suponemos que pueden habe registradoras de
        5 digitos y otras de 6.
        Tambien suponemos que entre 2 registros no se puede haber recorrido
        la totalidad de los números porque no pódriamos saber la diferencia
        """
        assert int == type(init) == type(end),"los campos init y end debe ser enteros"
        if init <= end:
            return end - init
        else:
            max_register = (10 ** len(str(init)))
            return max_register - init + end
    
    @classmethod
    def get_last(cls,vehicle,time=None):
        """Devuelve la última registradora de un vehículo o None,
        si se indica time buscara la ultima registradora del vehiculo
        antes de esa hora indicada """
        domain = [('vehicle','=',vehicle)]
        if time:
            domain.append([('time','<',time)])
        last_counter = cls.search(domain,order=[('time','DESC')],limit=1)
        if last_counter:
            return last_counter[0]
        else:
            return None
    
    def get_previous(self):
        """devuelve la registradora anterior"""
        if self.time:
            return self.get_last(self.vehicle, self.time)
    
        
class IssuePassengerCounter(ModelSQL,ModelView):
    """Novedades de registradoras"""
    
    __name__ = 'fleet.vehicle.passenger_counter.issue'
    
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              ondelete='RESTRICT',
                              domain=[('tpp','=',True),
                                      ('active','=',True),
                                      ],
                              required=True
                              )
    init_counter = fields.Many2One('fleet.vehicle.passenger_counter','Registradora Inicial',
                                   domain=[('vehicle','=',Eval('vehicle',-1))],
                                   required=True
                                   )
    previous_turns = fields.Function(fields.Integer('Vueltas anteriores',
                                                               help="indica la cantidad de vueltas de la "
                                                               "registradora entre la registradora inicial "
                                                               "y la anterior registradora"),
                                                    'on_change_with_previous_turns')
    end_counter = fields.Many2One('fleet.vehicle.passenger_counter','Registradora Final',
                                   domain=[('vehicle','=',Eval('vehicle',-1))],
                                   required=True
                                   )
    time = fields.DateTime('Fecha',
                          help="Fecha y hora en que se registra la novedad, "
                          "Esta fecha será en la que se afectará el conteo de "
                          "pasajeros del vehículo")
    turns = fields.Function(fields.Integer('Vueltas'),'on_change_with_turns')
    type = fields.Many2One('fleet.vehicle.passenger_counter.issue.type','Tipo',
                           ondelete='RESTRICT',
                           required=True
                           )
    note = fields.Text('Observaciones')
    
    
    @fields.depends('init_counter','end_counter')
    def on_change_with_turns(self,name=None):
        if self.init_counter and self.end_counter:
            return self.init_counter.diff_counter(self.end_counter)
        else:
            return 0
            
    @fields.depends('init_counter','vehicle')
    def on_change_with_previous_turns(self,name=None):
        if self.init_counter:
            return self.init_counter.diff_counter(self.init_counter.get_previous())
    
    @fields.depends('init_counter','end_counter','previous_turns','vehicle','time')
    def on_change_vehicle(self):
        Counters = Pool().get('fleet.vehicle.passenger_counter')
        if self.vehicle:
            last_counter = Counters.get_last(self.vehicle)
            
            self.init_counter = last_counter.id if last_counter else None
            self.previous_turns = self.init_counter.diff_counter(self.init_counter.get_previous()) if self.init_counter else None
            self.end_counter = None
            if not self.time:
                self.time = self.init_counter.time if self.init_counter else None
        else:
            self.init_counter = None
            self.previous_turns = None
            self.end_counter = None
    
    @classmethod
    def __setup__(cls):
        super(IssuePassengerCounter, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('Unique_init_counter_vehicle', Unique(table, table.init_counter,table.vehicle),
                "No puede haber varias incidencias de registradoras para el"
                " mismo vehiculo y la misma registradora inicial"),
            ('Unique_end_counter_vehicle', Unique(table, table.end_counter,table.vehicle),
                "No puede haber varias incidencias de registradoras para el"
                " mismo vehiculo y la misma registradora final")
        ]
    
        
    def pre_validate(self):
        super(IssuePassengerCounter, self).pre_validate()
        if self.init_counter.time >= self.end_counter.time:
            self.raise_user_error("La registradora final debe tener una hora "
                                 "posterior a la registradora inicial, así sea 1 segundo")
    

class IssuePassengerCounterType(ModelSQL,ModelView):
    """tipos de novedades de registradoras"""
    
    __name__ = 'fleet.vehicle.passenger_counter.issue.type'
    
    name = fields.Char('Nombre',
                       required=True)
    affects_passangers = fields.Boolean('Afecta Pasajeros',
                                        help="Indica si este tipo de insidencia afecta los pasajeros")
    
    @staticmethod
    def default_affects_passangers():
        return True
    
    @classmethod
    def __setup__(cls):
        super(IssuePassengerCounterType, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('Unique_name_autority', Unique(table, table.name),
                'El nombre del tipo de novedad de registradora debe ser unico')
        ]
    
    
class VehicleRegistration(ModelSQL,ModelView):
    """Matricula de transito de vehiculos"""
    
    __metaclass__ = PoolMeta
    __name__= 'fleet.vehicle.registration'

    authority = fields.Many2One('public_transport.authority','Autoridad',
                                required=True,
                                domain=[('generates_vehicle_registration','=',True)]
                                )
    
    
    @classmethod
    def __setup__(cls):
        super(VehicleRegistration, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('Unique_code_autority', Unique(table, table.code,table.authority),
                'El codigo de la matricula debe ser unico para cada autoridad')
        ]
