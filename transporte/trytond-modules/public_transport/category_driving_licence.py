# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
import logging
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, Button
from io import StringIO
import csv

logger = logging.getLogger(__name__)

__all__ = ['CategoryDrivingLicenceImport','SelectCsvImportCategoryDrivingLicence',
           'CategoryDrivingLicence']


class CategoryDrivingLicence(ModelSQL,ModelView):
    """Categoria de Pases de Conduccion."""
    
    __name__ = 'public_transport.category_driving_licence'
    _order = [('active','ASC'),('name','ASC')]
    
    active = fields.Boolean('Activa')
    name = fields.Char('Nombre')
    alternative_name = fields.Char('Nombre alternativo',
                        help="Pude contener otros nombres con el que se conoce esta categoria\n"
                        "por ejemplo los nombres anteriores de las categorias"
                        )
    notes = fields.Text('Observaciones')


class SelectCsvImportCategoryDrivingLicence(ModelView):
    'Seleccionar archivo para importar'
    __name__ = 'public_transport.category_driving_licence.import.select_csv'
    
    categories = fields.Binary('Categorias de Pase a importar',
                               help="En la carpeta de documentacion del modulo\n"
                               "se encuentra el archivo csv a importar",
                               required=True,
                                )
    encoding = fields.Selection([('utf-8','utf-8')],'Codificacion',
                             required=True,
                             )
    
class CategoryDrivingLicenceImport(Wizard):
    """Importar categorias de pases para colombia que vienen con el modulo."""
    __name__ = 'public_transport.category_driving_licence.import'
    
    start = StateView('public_transport.category_driving_licence.import.select_csv',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel',default=True),
            Button('Importar', 'import_', 'tryton-executable'),
            ])
    import_ = StateTransition()
    
    
    def transition_import_(self):
        CategoryDrivingLicence = Pool().get('public_transport.category_driving_licence')
        file = StringIO(self.start.categories.decode(self.start.encoding))
        csv_data = csv.reader(file)
        csv_header = next(csv_data)
        data = list(csv_data)
        file.close()
        len_import = CategoryDrivingLicence.import_data(csv_header,data)
        logger.info("Total CategoryDrivingLicence import:{l}".format(l=len_import))
        return 'end'
