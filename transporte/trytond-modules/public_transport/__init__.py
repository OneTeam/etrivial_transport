# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .configuration import *
from .employee import *
from .proprietary import *
from .route import *
from .category_driving_licence import *
from .count_passengers import *
from .vehicle import *
from .dispatch import *
from .authority import *
from .user import *
from .quantity_dispatch import *


def register():
    # registro de modelos en el framework
    Pool.register(
        Configuration,
        Proprietary,
        ProprietaryContract,
        ProprietaryContractDriver,
        Route,
        SelectCsvImportCategoryDrivingLicence,
        CategoryDrivingLicence,
        Employee,
        PassangersCountStadisticFilter,
        GroupRoute,
        VehicleTypeTpp,
        Vehicle,
        VehicleActionRation,
        VehicleCorporateImage,
        Authority,
        VehicleOperationCard,
        FleetConfiguration,
        Dispatch,
        FormExecDispatch,
        FormProgramedDispatch,
        FormFinalizeDispatch,
        FormCancelDispatch,
        RouteItinerary,
        Itinerary,
        VehiclePassengerCounter,
        VehicleRegistration,
        DispatchContextFilter,
        User,
        GroupRouteUser,
        GroupRouteVehicleOrder,
        GroupRouteSchedule,
        GroupRoutePlanedDisaptch,
        GroupRouteParamsTypeDay,
        FormGeneratePlanedDispatch,
        FormImportOrderedVehicles,
        PlanedDisaptchDispatch,
        FormImportSchedules,
        DispatchesQuantityForDay,
        IssuePassengerCounter,
        IssuePassengerCounterType,
        CalculateDispatchesForDayParams,
        DispatchesQuantityDayGroute,
        DispatchesQuantityForMonth,
        ChangeDispatchForm,
        SelectChangeDispatchForm,
        DispatchAnnotation,
        AnnotationDispatch,
        DispatchDurationRule,
        module="public_transport", type_="model"
        )
    # registro de wizards en el framework
    Pool.register(
        CategoryDrivingLicenceImport,
        PassengersCountStadistic,
        CreateVehiclesFromMetro,
        ExecDispatch,
        ExecProgramedDispatch,
        ExecFinalizeDispatch,
        ExecCancelDispatch,
        GeneratePlanedDispatch,
        ImportOrderedVehicles,
        OpenEventsFromDispatch,
        ImportSchedules,
        CalculateDispatchesForDay,
        CalculateDispatchesDayGroute,
        CalculateDispatchesForMonth,
        ChangeDispatch,
        OpenGeoPassengerFromDispatch,
        DispatchShowMap,
        module='public_transport', type_='wizard'
        )
    # registro de Reportes en el framework
    Pool.register(
        PassangersCountStadisticCsv,
        GroupRouteOrderVehiclesReport,
        module='public_transport', type_='report')
