"""
Convierte el archivo aseguradoras.csv en insurances_data.xml

Esto para importar las aseguradoras colombianas en el presente modulo
"""

import csv
model = 'public_transport.category_driving_licence'
aseguradoras = open('categorias_pase_co.csv','r')
insureances_data = open('category_driving_license_co_data.xml','w')

csvReader = csv.reader(aseguradoras)
header = next(csvReader)
name = header.index('name')
alternative_name = header.index('alternative_name')
active = header.index('active')
notes = header.index('notes')


insureances_data.write("""<?xml version="1.0"?>
<tryton>
    <data>
""")
for row in csvReader:
    insureances_data.write( """
        <record model="{model}"  id="{id}">
            <field name="name">{name}</field>
            <field name="alternative_name">{alternative_name}</field>
            <field name="active">{active}</field>
            <field name="notes">{notes}</field>
        </record>
""".format(model=model,
           id='category_driving_' + row[name].strip().replace(' ','_').replace('.','_').lower(),
           name=row[name].strip(),
           alternative_name=row[alternative_name].strip(),
           active=row[active].strip(),
           notes=row[notes].strip(),
           )
        )
insureances_data.write("""
    </data>
</tryton>
"""
)
aseguradoras.close()
insureances_data.close()
