PublicTransport Module
######################


Dispatch
~~~~~~~~

Aún tengo dudas sobre el modelado de los despachos pues tengo 2 ideas que no
logro resolver aún:
1. Modelar Planillas (ruta, despachador, fecha inicio, fecha fin, ...) la cual
   contiene despachos (Dispatchs). En este esquema se podría controlar que un
   despachador solo realice despachos en la planilla asignada, además no se
   pude crear planillas solapadas en tiempo para una misma ruta.
2. Modela despachos directamente: de este modo no se agrupan los despachos
   explicitamente sino que estando en la misma tabla si se quiere consultar los
   despachos para una ruta un día determinado se usa filtros. Para el control
   de quien puede o no despachar recorridos en una ruta se podría crear una
   tabla de programación de despachadores y usar dicha tabla como filtro para
   validar al momento de seleccionar la ruta cual se puede seleccionar.
De momento estoy modelando pensando en la opción 2 pero que más adelante se
puede ajustar, si se requiere a la opcion 1.
