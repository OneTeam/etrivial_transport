# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['User']

class User(metaclass=PoolMeta):
    """Agrega campo de usuario requeridos por public_transport
    
    """
    __name__ = 'res.user'
    
    group_route = fields.Many2One('public_transport.group_route', 'Grupo de Ruta asignado',
                                  domain=[('id','in',Eval('groups_routes',[]))],
                                  help="Requiere que primero se seleccione y "
                                  "guarde los Grupos de Rutas asignados",
                                  depends=['groups_routes'])
    
    #groups_routes_id = fields.Function(fields.)
    #TODO solo lectura
    groups_routes = fields.Many2Many('public_transport.group_route.user', 'user','group_route', 'Grupos de Rutas asignados')
    
    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._context_fields += [
            'group_route',
            'groups_routes'
            ]
