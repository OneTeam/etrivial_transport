# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging

from datetime import timedelta, datetime, time
from math import ceil
from itertools import cycle
import pytz
import csv

from io import StringIO

from trytond.modules.public_transport.planed_dispatch_generator import GeneradorDeDespachos
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Get, Bool, Not
from trytond.report.report import Report
from trytond.wizard.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.model.descriptors import dualmethod


#from public_transport import  planed_dispatch_generator
#import  .planed_dispatch_generator as pd


logger = logging.getLogger(__name__)

__all__ = ['Route','GroupRoute','RouteItinerary', 'Itinerary',
           'GroupRouteUser', 'GroupRouteVehicleOrder',
           'GroupRouteParamsTypeDay', 'GroupRouteSchedule',
           'GroupRouteOrderVehiclesReport','GeneratePlanedDispatch',
           'FormGeneratePlanedDispatch','GroupRoutePlanedDisaptch',
           'FormImportOrderedVehicles','ImportOrderedVehicles',
           'PlanedDisaptchDispatch','ImportSchedules',
           'FormImportSchedules','DispatchDurationRule']

           
_type = [('B','Bus'),('M','Microbus'),('A','AMBOS')]

#no modificar
HOLIDAY, SATURDAY, ORDINARY = ['holiday','saturday','ordinary']
#no modificar
_route_programation_types = [(ORDINARY,'Ordinario'),
         (SATURDAY,'Sábados'),
         (HOLIDAY,'Domingos y Festivos')]
#no modificar
ROTACION_CONTINUA = 'continua'
ROTACION_INVERSA = 'inversa'
ROTACION_INVERSA_PARTIDA = 'inversa_partida'
rotation_types = [(ROTACION_CONTINUA, 'Continua'),
                  (ROTACION_INVERSA, 'Inversa'),
                  (ROTACION_INVERSA_PARTIDA, 'Inversa partida')]

def to_idx(idx,len_):
    """Devuelve un idx valido de un idx mayor al largo de la lista(len_)
    o menor que el negativo del largo de la lista (-len_)"""
    if 0 <= idx < len_:
        return idx
    elif idx > 0:
        return idx % len_
        pass
    elif idx < 0:
        r = (idx % len_) - len_
        return 0 if r == -len_ else r

def saltar_lista(list_,idx=0):
    """devuelve una lista con todos los elementos y el orden de la lista
    pero iniciando en idx"""
    idx = to_idx(idx, len(list_))
    if idx:
        if idx < 0:
            list_ = list_[idx:] + list_[:idx]
        else:
            end = idx if idx == 1 else -idx+1
            list_ = list_[idx:] + list_[:end]
    return list_

def loop_list(list_,idx=0):
    """Devuelve valores ciclicos de una lista, si se indica idx se iniciará en
    la posicion dada, la cual puede ser negativa.
    El idx se toma como en las listas los idx positivos empiezan en la posicion
    0 y los idx negativos en -1"""
    return cycle(saltar_lista(list_, idx))

def correr_sublistas(lista,salto=0):
    """corre las sublistas de la lista n cantidad de veces,
    donde n = salto"""
    r = []
    for item in lista:
        r.append(saltar_lista(item,salto))
    return r

def reverse_rotation_list(list_, quantity, jump):
    idx = -jump
    k = loop_list(list_,idx)
    return [next(k) for _ in range(0,quantity)]

def splite_list(list_, splite_position):
    return [list_[:splite_position], list_[splite_position:]]

class DaysCalc(object):
    """Calculo de dias"""
    
    
    @classmethod
    def tipo_de_dia(cls,day):
        Holidays = Pool().get('holidays_co.holidays')
        #domingo
        if int(day.strftime('%w')) == 0:
            return HOLIDAY
        elif Holidays.is_holiday(day):
            return HOLIDAY
        elif int(day.strftime('%w')) == 6:
            return SATURDAY
        else:
            return ORDINARY
    
    @classmethod
    def calcular_dias_tipos_transcurridos(cls,tipo,fecha_inicial,fecha_final):
        Holidays = Pool().get('holidays_co.holidays')
        if tipo == HOLIDAY:
            qdays = Holidays.how_holidays_and_sundays(fecha_inicial,fecha_final)
        elif tipo == ORDINARY:
            qdays = Holidays.how_ordinaries(fecha_inicial,fecha_final)
        elif tipo == SATURDAY:
            qdays = Holidays.how_saturdays(fecha_inicial,fecha_final)
        qdays = qdays - 1 if qdays else 0
        #se resta 1 porque la funcion de dias transcurridos devuelve 1 para el mismo dia
        return qdays

class Route(ModelSQL,ModelView):
    '''Rutas.'''
    
    __name__ = "public_transport.route"
    
    name = fields.Char('Nombre',
                    help='Nombre de la Ruta',
                    required=True
                    )
    code = fields.Char('Código',
                    help='Codigo de la ruta',
                    required=True
                    )
    active = fields.Boolean('Activa')
    description = fields.Char('Descripción')
#     country = fields.Many2One('country.country', 'Pais',
#                             required=True)
#     departament = fields.Many2One("country.subdivision",'Departamento',
#                                 domain=[('country', '=', Eval('country', -1)),
#                                     ('parent', '=', None),],
#                                 required=True
#                                 )
#     city = fields.Many2One("country.subdivision",'Municipio',
#                         domain=[
#                             ('country', '=', Eval('country', -1)),
#                             ('parent', '=', Eval('departament',-1)),
#                             ],
#                         required=True
#                         )

    gps_track = fields.Binary('Ruta Gps',
                            filename='gps_track_file_name',
                            help="Ruta Gps preferiblemente en formato gpx"
                            )
    group = fields.Many2One('public_transport.group_route','Grupo',
                            domain=[()],
                            help="Grupo de rutas a la que pertenece la ruta",
                            required=True)
    sonar_itinerary = fields.One2One('public_transport.route_itinerary','route','itinerary',"Itinerario Sonar",
                                     help="Itinerario en la plataforma de Sonar para esta ruta"
                                     )
    sonar_id = fields.Function(fields.Integer('Sonar id'),'on_change_with_sonar_id')
    max_passangers_for_lap = fields.Integer('máximo pasajeros por recorrido',
                                            required=True,
                                            help="Limita la diferencia de registradora "
                                            "inicial y final en el despacho, para evitar "
                                            "errores de digitacion")
    
    @fields.depends('sonar_itinerary')
    def on_change_with_sonar_id(self,name=None):
        if self.sonar_itinerary:
            return self.sonar_itinerary.It_id
    
    @staticmethod
    def default_max_passangers_for_lap():
        return 150
    
    @staticmethod
    def default_country():
        Configuration = Pool().get('public_transport.configuration')
        configuration = Configuration.get_singleton()
        if configuration:
            if configuration.route_default_country:
                return configuration.route_default_country.id
    
    @staticmethod
    def default_departament():
        Configuration = Pool().get('public_transport.configuration')
        configuration = Configuration.get_singleton()
        if configuration:
            if configuration.route_default_departament:
                return configuration.route_default_departament.id

#     #comentado para requerir  que el usuario busque la ciudad
#     @staticmethod
#     def default_city():
#         Configuration = Pool().get('public_transport.configuration')
#         configuration = Configuration.get_singleton()
#         if configuration:
#             if configuration.route_default_city:
#                 return configuration.route_default_city

    @classmethod
    def __setup__(cls):
        super(Route, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('Route_code_key', Unique(table, table.code),
                'Route: el codigo debe ser unico'),
            ('Route_name_key', Unique(table, table.name),
                'Route: el nombre debe ser unico'),
        ]
        cls._order = [
            ('name', 'ASC'),
            ('code', 'ASC'),
            ]
    
class GroupRoute(ModelSQL,ModelView):
    """Grupo de Rutas."""
    
    __name__ = "public_transport.group_route"
    
    name = fields.Char('Nombre',
                    help='Nombre del Grupo de rutas',
                    required=True
                    )
    code = fields.Char('Código',
                    help='Codigo del Grupo de la ruta',
                    required=True
                    )
    active = fields.Boolean('Activo')
    description = fields.Char('Descripción')
    type_mobile_tpp = fields.Many2One('fleet.vehicle.type_tpp', 'Tipo de vehiculo',
                            required=True,
                            domain=[('to_route','=',True)],
                            )
    metro_feeder = fields.Boolean('Alimentador Metro',
                                help="Indica si el grupo es alimentador del sistema Metro"
                                )
    vehicles = fields.One2Many('fleet.vehicle','routes','Vehiculos',
                               help="Vehiculos de estas rutas",
                               )
    routes = fields.One2Many('public_transport.route','group','Rutas',
                        help="Rutas que pertenecen a este grupo"
                        )
    action_ration = fields.Many2One('fleet.vehicle.action_ration','Radio de acción',
                                    help="Solo los vehículos en este mismo radio, \n"
                                    "podrán ser vinculados a estas rutas.",
                                    domain=[('active','=',True)],
                                    required = True,
                                    )
    limit_vehicles = fields.Integer('Capacidad Transportadora',
                                    required=True,
                                    help="Limite de vehiculos para esta ruta")
    orders_vehicles = fields.One2Many('public_transport.group_route.vehicle_order',
                                      'group_route','orden de vehiculos')
    params_type_days = fields.One2Many('public_transport.group_route.params_type_day',
                                     'group_route','Parámetros tipos de dias')
    schedules = fields.One2Many('public_transport.group_route.schedule',
                                      'group_route','Horarios de la ruta')
    unique_turn_for_all_route = fields.Boolean("Turno Unico",
                                               help="Indica si el grupo de ruta"
                                               " tiene un turno por subruta o uno"
                                               " solo para todo el Grupo de Ruta")
    planed_dispatch_with_out_route = fields.Boolean('Despachos planeados sin ruta',
                                                help="al generar los despachos planeados no se inidca ruta")
    change_schedules = fields.Selection(_route_programation_types+ [('','')],'Cambiar Horarios',
                         help="Define el tipo de horarios que serán usados para todos los despachos planeados "
                         "usado por ejemplo en semana santa que los dias ordinarios cambian los horarios por "
                         "los horarios de los sábados"
                         )
    

    @classmethod
    def __setup__(cls):
        super(GroupRoute, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('GroupRoute_code_key', Unique(table, table.code),
                'GroupRoute: el codigo debe ser unico'),
            ('Route_name_key', Unique(table, table.name),
                'GroupRoute: el nombre debe ser unico'),
        ]
        cls._order = [
            ('name', 'ASC'),
            ('code', 'ASC'),
            ]
        #eliminar columnas city,country,departament,type_mobile migracion v0.0.1
        
#     def quote_of_group(self,name=None):
#         if self.routes:
#             return 0
#             return sum(x.quota for x in self.routes)
#         else:
#             return 0

    def get_date_order_vehicles(self,type_):
        for date_order in self.params_type_days:
            if date_order.type == type_:
                return date_order.date
        return None
    
    def get_dic_date_order_vehicles(self):
        r = {}
        for type_ in [HOLIDAY,SATURDAY,ORDINARY]:
            r[type_] = self.get_date_order_vehicles(type_)
        return r
    
    def get_jump_route_for_dispatch(self,type_):
        for date_order in self.params_type_days:
            if date_order.type == type_:
                return date_order.jump_route_for_dispatch
        return None
    
    def get_jumping_route_by_day(self,type_):
        """indica si se intercambian la subruta de un día a otro para los
        vehiculos, esto sucede en las rutas de Belén"""
        for date_order in self.params_type_days:
            if date_order.type == type_:
                return date_order.jump_route_by_day
        return None
    
    def get_order_vehicles(self,type_):
        VehicleOrder = Pool().get('public_transport.group_route.vehicle_order')
        VehicleOrder.search([('group_route','=',self.id),
                             ('type','=',type_)])
        for date_order in self.order_vehicles:
            if date_order.type == type_:
                return date_order.date
        return None
    
    def get_dic_order_vehicles(self):
        r = {}
        for type_ in [HOLIDAY,SATURDAY,ORDINARY]:
            r[type_] = self.get_date_order_vehicles(type_)
        return r
    
    def get_quantity_turns(self,type_):
        for quantity_turn in self.params_type_days:
            if quantity_turn.type == type_:
                return quantity_turn.turn_quantity
        return None
    
    def get_dic_quantity_turns(self):
        r = {}
        for type_ in [HOLIDAY,SATURDAY,ORDINARY]:
            r[type_] = self.get_quantity_turns(type_)
        return r
    
    def get_schedules(self,type_,day,subroute=None):
        """Devuelve un diccionarion con los horarios para un type_ (ordinario, festivos, sabados)
        el diccionario es de la forma {turn:[datetime,datetime,...]},
        si se indica ruta trae solo los de la ruta indicada
        """
        Schedules = Pool().get('public_transport.group_route.schedule')
        if self.change_schedules:
            #usado para cambiar horarios en condicones especiales
            #como semana santa
            type_ = self.change_schedules
        conditions = [('type','=',type_),
                      ('group_route','=',self.id),
                      ]
        if subroute:
            conditions.append([('route','=',subroute)])
        schedules = Schedules.search(conditions)
        r = {}
        for schedule in schedules:
            if schedule.turn not in r.keys():
                r[schedule.turn] = []
            r[schedule.turn].append(schedule.get_time(day))
        return r
    
    def get_schedules_for_all_routes(self,type_,day):
        """Devuelve un diccionario donde las llaves son el id de la ruta y el
        contenido es un diccionario con los horarios para la subruta en el dia
        indicado"""
        r = {}
        for ruta in [x.id for x in self.routes]:
            r[ruta] = self.get_schedules(type_,day,ruta)
        return r
    
    
    def get_method_for_type(self,type_):
        for date_order in self.params_type_days:
            if date_order.type == type_:
                return (date_order.model_rotation,date_order.method_rotation)
        return None

    def get_splite_position(self, type_):
        for params in self.params_type_days:
            if params.type == type_:
                return params.splite_position
        return None
    
        
        
class RouteItinerary(ModelSQL,ModelView):
    """Vinculo entre datos_sonar.Itinerary y public_transport.route"""
    
    __name__ = 'public_transport.route_itinerary'
    
    route = fields.Many2One('public_transport.route','Ruta')
    itinerary = fields.Many2One('datos_sonar.itinerary','Itinerario Sonar')
    
    @classmethod
    def __setup__(cls):
        super(RouteItinerary, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('route_key', Unique(table, table.route),
                'RouteItinerary: la ruta debe ser unica'),
            ('vehicle_key', Unique(table, table.itinerary),
                'RouteItinerary: el itinerario debe ser unico'),
        ]
        
class Itinerary(ModelSQL,ModelView):
    '''    No Point Informacion extraida usando la funcion GET_Itineraries de la API de SonarAvl.
    
    '''
    __name__ = "datos_sonar.itinerary"
    __metaclass__ = PoolMeta
    
    public_transport_route = fields.One2One('public_transport.route_itinerary','itinerary','route',"Ruta",
                                     help="Ruta que corresponde con este Itinerario"
                                     )
    
class GroupRouteUser(ModelSQL,ModelView):
    """Relaciona Grupos de Rutas y usuarios
    
    @todo verificar si se requiere limitar por fechas
    @todo crear reglas de acceso a los registros basados en este modelo
    """
    __name__ = 'public_transport.group_route.user'
    group_route = fields.Many2One('public_transport.group_route','Grupo de rutas',
                                  required=True)
    user = fields.Many2One('res.user','Usuario',
                           required=True)
    
    @classmethod
    def __setup__(cls):
        super(GroupRouteUser, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('user_group_route', Unique(table, table.user,table.group_route),
                'No se debe repetir el mismo usuario con el mismo grupo de ruta')
        ]

class GroupRouteLimitMixin(object):
    """Mixin para clases que requieren grupos de rutas,
    limite de vehiculos y tipo (ordinario,sabado,domingo y festivos)"""
    group_route = fields.Many2One('public_transport.group_route','Grupo de rutas',
                                  required=True,
                                  )
    limit_vehicles = fields.Function(fields.Integer('limite',
                                            depends=['group_route'],
                                            help="limite máximo de vehículos "
                                            "para este grupo de rutas",
                                            readonly=True),'on_change_with_limit_vehicles',
                                     )
    type = fields.Selection(_route_programation_types,'Tipo',
                         required=True)
    
    @fields.depends('group_route')
    def on_change_with_limit_vehicles(self,name=None):
        if self.group_route:
            return getattr(self.group_route, 'limit_vehicles',-1)
    
class GroupRouteVehicleOrder(GroupRouteLimitMixin,ModelSQL,ModelView):
    """Orden de los vehículos en el grupo de Ruta"""
    
    __name__ = 'public_transport.group_route.vehicle_order'
    

    order = fields.Integer('Posicion',
                           domain=[('group_route.limit_vehicles','>=',Eval('order')),
                                   ('order','>',0)],
                           depends=['limit_vehicles','group_route'],
                           required=True)
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              required=False,
                              ondelete='RESTRICT',
                              domain=[('tpp','=',True),
                                      ('active','=',True),
                                      ('routes','=',Eval('group_route',-1)),
                                      ]
                              )
    
    @classmethod
    def __setup__(cls):
        super(GroupRouteVehicleOrder, cls).__setup__()
        table = cls.__table__()
        #Cuando se migre a tryton5 ajustar partial constraint para que
        #no se pueda repetir vehículo para un orden ordinario o sabado o
        # festivo pero que si pueda repetirse registros con el vehículo vacio 
        cls._sql_constraints += [
            ('unique_order_by', Unique(table, table.group_route,table.order,table.type),
                'El orden no se debe repetir para un mismo tipo'),
        ]
        cls._order = [
            ('group_route', 'ASC'),
            ('type','ASC'),
            ('order', 'ASC'),
            ]
    
    #todo validar al escribir pues pre_validate solo es a nivel de vista
    def pre_validate(self):
        super(GroupRouteVehicleOrder, self).pre_validate()
        self.check_repeat_order()
    
    def check_repeat_order(self):
        """verifica que el orden no este repetido para el tipo en el
        grupo de ruta"""
        repeat_order = self.search([('group_route','=',self.group_route),
                                    ('order','=',self.order),
                                    ('type','=',self.type),
                                    ('id','!=',self.id)])
        if repeat_order:
            self.raise_user_error("El orden {o}, del tipo {t} ya esta asignado "
                                  "para la ruta {r}".format(o=self.order,
                                                        t=self.type,
                                                        r=self.group_route))
    
    @classmethod
    def get_order_list(cls,group_route_id,type_):
        """Devuelve una lista de vehículos ordenada (por order) para la ruta y
         el tipo dado.
        @attention: se usa el maximo order como limite
        """
        GRoute = Pool().get('public_transport.group_route')
        groute = GRoute.browse([group_route_id])
        groute = groute[0]
        orders = cls.search([('group_route','=',group_route_id),
                                ('type','=', type_)],
                             )
        orders_dic = {x.order:x.vehicle.id for x in orders}
        limit = sorted(orders_dic.keys(),reverse=True)[0]
        r = []
        for x in range(1,limit+1):
            if x in orders_dic.keys():
                r.append(orders_dic[x])
            else:
                r.append(None)
        return r
    
    @classmethod
    def get_dic_order_list(cls,group_route_id):
        r = {}
        for type_ in [HOLIDAY,SATURDAY,ORDINARY]:
            r[type_] = cls.get_order_list(group_route_id, type_)
        return r
    
    @classmethod
    def get_grouped_order_list(cls,group_route_id,type_):
        """Devuelve la lista de vehiculos del grupo de ruta agrupandola por subrutas
        Ejemplo Belén tiene 2 subrutas y 80 vehiculos devolverá una lista así:
        [(vehiculo1,vehiculo2),(vehiculo3,vehiculo4),...(vehiculo79,vehiculo80)] 
        """
        GRoute = Pool().get('public_transport.group_route')
        groute = GRoute.browse([group_route_id])[0]
        def leer_por_bloques(lista,largo=1):
            x = 0 
            while True:
                if x > len(lista) - 1:
                        x = 0
                r = lista[x:x+largo]
                falta = largo - len(r)
                if falta:
                    r += [None for x in range(0,falta)]
                yield r
                x += largo
    
        def separar_lista(lista,cantidad_grupo):
            """parte la lista en elementos agruapdos de a n (cantidad_grupo),
            si para el último elemento no alcanzan los elementos de la lista
            se completa con None
            >>>agrupar_lista([1,2,3,4,5,6,7,8,9],2)
            [[1,2],[3,4],[5,6],[7,8],[9,None]]
            """
            largo = ceil(len(lista)/cantidad_grupo)
            lector = leer_por_bloques(lista, cantidad_grupo)
            return [next(lector) for _ in range(0,largo)]
        
        list_ = cls.get_order_list(group_route_id,type_)
        if groute.unique_turn_for_all_route:
            return list_
        else:
            return separar_lista(list_, len(groute.routes))
    
    @classmethod
    def get_dic_grouped_order_list(cls,group_route_id,type_):
        """Devuelve un diccionario donde las llaves son los tipos de dias y el
        contenido una lista agrupada ordenada de vehiculos"""
        r = {}
        for type_ in [HOLIDAY,SATURDAY,ORDINARY]:
            r[type_] = cls.get_grouped_order_list(group_route_id, type_)
        return r
    
    @classmethod
    def get_reverse_rotation_order_list(cls,group_route_id,type_,quantity,jump, splite_position=0):
        """devuelve una lista de quantity elementos agrupada ordenada de
        vehiculos haciendole una rotacion inversa e iniciando desde la posición
        indicada por jump
        """
        list_ = cls.get_grouped_order_list(group_route_id,type_)
        return reverse_rotation_list(list_, quantity, jump)
    
    @classmethod
    def get_continue_rotation_order_list(cls,group_route_id,type_,quantity,jump, splite_position=0):
        """devuelve una lista de quantity elementos agrupada ordenada de
        vehiculos haciendole una rotacion continua e iniciando desde la posición
        indicada por jump
        """
        list_ = cls.get_grouped_order_list(group_route_id,type_)
        idx = (jump * quantity) if jump else jump
        k = loop_list(list_, idx)
        return [next(k) for _ in range(0, quantity)]

    @classmethod
    def get_splite_reverse_rotation_order_list(cls, group_route_id, type_,
                                                     quantity, jump, splite_position=0):
        """devuelve una lista de quantity elementos ordenada de vehiculos
        separando la lista en la posición splite_pos y haciendo una rotacion continua
        a cada lista e iniciando desde la posición indicada por jump
        """
        r = []
        if splite_position:
            splite_position -= 1 # los ordenes de lista inician en 0
        list_vehicles = cls.get_grouped_order_list(group_route_id, type_)
        lists = splite_list(list_vehicles, splite_position)
        assert len(lists[0]) + len(lists[1]) == quantity, "Error en cantidad de vehiculos"
        for l in lists:
            r.extend(reverse_rotation_list(l, len(l), jump))
        return r
    
    @classmethod
    def get_order_list_for(cls,group_route_id,day):
        """Devuelve la lista ordenada para el grupo de ruta y el dia indicado
        """
        GRoute = Pool().get('public_transport.group_route')
        groute = GRoute.browse([group_route_id])[0]

        type_ = DaysCalc.tipo_de_dia(day)
        date_order_vehicles = groute.get_date_order_vehicles(type_)
        jump = DaysCalc.calcular_dias_tipos_transcurridos(type_,
                                                 date_order_vehicles,
                                                 day)
        method_,function_ = groute.get_method_for_type(type_)
        Model_ = Pool().get(method_)
        function_ = getattr(Model_, function_)
        quantity = groute.get_quantity_turns(type_)
        splite_position = groute.get_splite_position(type_)
        r = function_(group_route_id,
                      type_,
                      quantity,
                      jump,
                      splite_position or 0)
        if groute.get_jumping_route_by_day(type_):
            r = correr_sublistas(r, jump)
        return r
            

class GroupRouteSchedule(GroupRouteLimitMixin,ModelSQL,ModelView):
    """Horario de los Grupos de Ruta"""
    #@todo modelar remplazos que por ejemplo en belén el ficho 20 de los sábados es reemplazo
    __name__ = 'public_transport.group_route.schedule'
    
    turn_limit = fields.Function(fields.Integer('Turno límite'),'on_change_with_turn_limit')
    turn = fields.Integer('Turno',
                           domain=[('turn','>',0)],
                           depends=['group_route','turn_limit','type'],
                           required=True,
                           help="No debe ser superior al limite")
    route = fields.Many2One('public_transport.route', 'Ruta',
                            domain=[('group','=',Eval('group_route'))],
                            states={'required':~Bool(Eval('unique_turn_for_all_route')),#@todo si solo aplica para la vista ingresar este comportamiento en write
                                    'invisible':Bool(Eval('unique_turn_for_all_route')),
                                    'readonly':Bool(Eval('unique_turn_for_all_route'))},
                            depends=['unique_turn_for_all_route'],
                            help="Obligatorio si el grupo de ruta tiene un "
                            "turno para cada subruta"
                        )
    hour = fields.Integer('Hora',
                          domain=[('hour','>=',0),
                                  ('hour','<',24)],
                          required=True,
                          help="En formato militar: de las 0 a las 23")
    minute = fields.Integer('Minuto',
                          domain=[('minute','>=',0),
                                  ('minute','<',60)],
                          required=True,
                          help="De 0 a 59")
    duration = fields.Integer('Duracion',
                              domain=[('duration','>',0)],
                              required=True,
                              help="Duración en minutos")
    unique_turn_for_all_route = fields.Function(fields.Boolean("Turno Unico",
                                               depends=['group_route'],
                                               help="Indica si el grupo de ruta"
                                               " tiene un turno por subruta o uno"
                                               " solo para todo el Grupo de Ruta"),'on_change_with_unique_turn_for_all_route')


    
    @fields.depends('group_route')
    def on_change_with_unique_turn_for_all_route(self,name=""):
        if self.group_route:
            return self.group_route.unique_turn_for_all_route
    
    @fields.depends('group_route','type')
    def on_change_with_turn_limit(self,name=None):
        if self.group_route and self.type:
            if self.group_route.params_type_days:
                for q in self.group_route.params_type_days:
                    if q.type == self.type:
                        return q.turn_quantity
        return None
    
    def get_time(self,day):
        """devuelve un datetime para el turno"""
        tz = pytz.timezone("America/Bogota")
        return tz.localize(datetime(day.year,
                        day.month,
                        day.day,
                        self.hour,
                        self.minute))
    
    @classmethod
    def __setup__(cls):
        super(GroupRouteSchedule, cls).__setup__()
        table = cls.__table__() 
        cls._sql_constraints += [
            ('unique_schedule_by_groute', Unique(table, table.group_route, table.route, table.type,table.turn,table.hour,table.minute),
                'GroupRouteschedule: ya existe este horario'),
        ]
        cls._order = [
            ('group_route', 'ASC'),
            ('type','ASC'),
            ('hour','ASC'),
            ('minute','ASC'),
            ]
    
    #validar en write porque este metodo solo valida en la vista del cliente
    def pre_validate(self):
        super(GroupRouteSchedule, self).pre_validate()
        if self.turn > self.turn_limit:
            self.raise_user_error("El turno ({t}) no debe ser superior al "
                                  " límite({l})".format(t=self.turn,l=self.turn_limit))



class GroupRouteParamsTypeDay(GroupRouteLimitMixin,ModelSQL,ModelView):
    """Parametros para los tipos de dias del Grupo de Rutas"""

    __name__ = 'public_transport.group_route.params_type_day'
    
    date = fields.Date('Fecha orden vehiculos',
                       required=True)#@todo cambiar nombre de campo para hacer mas especifico
    turn_quantity = fields.Integer('Cantidad de turnos',
                           domain=[('group_route.limit_vehicles','>=',Eval('turn_quantity')),
                                   ('turn_quantity','>',0)],
                           depends=['limit_vehicles','group_route'],
                           required=True)
    rotation_type = fields.Selection(rotation_types,'Tipo de rotación',
                                     required=True)
    model_rotation = fields.Function(fields.Char('Modelo para la rotacion'),'get_method_rotation')
    method_rotation = fields.Function(fields.Char('Metodo para la rotacion'),'get_method_rotation')
    jump_route_by_day = fields.Boolean("Salta la ruta por día",
                                       help="Indica si la ruta va cambiando cada día")
    jump_route_for_dispatch = fields.Boolean("Salta la ruta por despacho",
                                             help="Indica si para un vehículo "
                                             "salta la ruta entre despachos")
    jump_route_for_schedule_time = fields.Boolean("Salta la ruta por tiempo de despacho",
                                                  help="Independiente del "
                                                  "turno quien define la ruta "
                                                  "es el orden cronologico del"
                                                  " despacho")#@todo excluye jump_route_for_dispatch jump_route_by_day
    splite_position = fields.Integer("Posición partir lista",
                                     states={
                                         'required':Eval('rotation_type', '').in_([ROTACION_INVERSA_PARTIDA])
                                     },
                                     help="Indica la posición donde se "
                                     "debe partir la lista")
    

    def get_method_rotation(self,name):
        if self.rotation_type == ROTACION_CONTINUA:
            if name == "model_rotation":
                return 'public_transport.group_route.vehicle_order'
            elif name == "method_rotation":
                return 'get_continue_rotation_order_list'
        elif self.rotation_type == ROTACION_INVERSA:
            if name == "model_rotation":
                return 'public_transport.group_route.vehicle_order'
            elif name == "method_rotation":
                return 'get_reverse_rotation_order_list'
        elif self.rotation_type == ROTACION_INVERSA_PARTIDA:
            if name == "model_rotation":
                return 'public_transport.group_route.vehicle_order'
            elif name == "method_rotation":
                return 'get_splite_reverse_rotation_order_list'
                
    @classmethod
    def __setup__(cls):
        super(GroupRouteParamsTypeDay, cls).__setup__()
        table = cls.__table__()
        #Cuando se migre a tryton5 ajustar partial constraint para que
        #no se pueda repetir vehículo para un orden ordinario o sabado o
        # festivo pero que si pueda repetirse registros con el vehículo vacio 
        cls._sql_constraints += [
            ('unique_type_by_groute', Unique(table, table.group_route,table.type),
                'Este tipo ya tiene fecha'),
        ]
        cls._order = [
            ('group_route', 'ASC'),
            ('type','ASC'),
            ('date', 'ASC'),
            ]
        
class GroupRouteOrderVehiclesReport(Report):
    
    __name__ = 'public_transport.group_route.order_vehicles_report'

    @classmethod
    def execute(cls, ids, data):
        GroupRoute = Pool().get('public_transport.group_route')
        ActionReport = Pool().get('ir.action.report')
        cls.check_access()
        
        action_id = data.get('action_id')
        if action_id is None:
            action_reports = ActionReport.search([
                    ('report_name', '=', cls.__name__)
                    ])
            assert action_reports, '%s not found' % cls
            action_report = action_reports[0]
        else:
            action_report = ActionReport(action_id)
        
        records = []
        content = []
        dict_content = {}
        model = action_report.model or data.get('model')
        if model:
            records = cls._get_records(ids, model, data)
        for record in records:
            limits = cls.get_limits(record)
            vehicles_orders = cls.get_vehicles_orders(record)
            #completar los ordenes no definidos
            content.append('Ruta '+record.name)
            for t_type in limits:
                content.append(t_type)
                for x in range(1,limits[t_type]+1):
                    if x not in vehicles_orders[t_type].keys():
                        content.append(str(x))
                    else:
                        content.append(','.join([str(x),vehicles_orders[t_type][x]]))
        content = ",\n".join(content)
        content = fields.Binary.cast(content,'utf8')
        return ('csv',content, action_report.direct_print, action_report.name)
    
    @classmethod
    def get_limits(cls,record):
            return {l.type:l.turn_quantity for l in record.params_type_days}
        
    @classmethod
    def get_vehicles_orders(cls,record):
        r = {}
        for vehicle_order in record.orders_vehicles:
            if vehicle_order.type not in r.keys():
                r[vehicle_order.type] = {}
            r[vehicle_order.type][vehicle_order.order]=vehicle_order.vehicle.rec_name
        return r


class GroupRoutePlanedDisaptch(ModelSQL, ModelView):
    """Despachos Planeados"""
    
    __name__ = 'public_transport.group_route.planed_dispatch'
    
    _readonly_state = {'readonly':Eval('dispatch',0) > 0}#@todo analizar si mejor se implementa estados

    group_route = fields.Many2One('public_transport.group_route', 'Grupo de rutas',
                                  required=True,
                                  states=_readonly_state
                                  )
    route = fields.Many2One('public_transport.route', 'Ruta',
                            domain=[('group', '=', Eval('group_route'))],
                                  states=_readonly_state)
    type = fields.Selection(_route_programation_types, 'Tipo',
                         required=True,
                                  states=_readonly_state)
    time = fields.DateTime('Fecha',
                       required=True,
                                  states=_readonly_state)
    turn = fields.Integer('Turno',
                           domain=[('turn', '>', 0)],
                           depends=['group_route', 'turn_limit', 'type'],
                           required=True,
                                  states=_readonly_state,
                           help="No debe ser superior al limite")
    turn_route = fields.Many2One('public_transport.route','Ruta de Turno',
                            domain=[('active','=',True),
                                    ('group','=',Eval('group_route',-1))],
                            help="Indica la ruta inicial del Turno de este despacho",
                                  states=_readonly_state
                            )
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              required=False,
                              ondelete='RESTRICT',
                              domain=[('tpp','=',True),
                                      ('active','=',True),
                                      ('routes','=',Eval('group_route',-1)),
                                      ],
                                  states=_readonly_state
                              )
    dispatch = fields.One2One('public_transport.group_route.planed_dispatch_dispatch',
                              'planed_dispatch','dispatch','Despacho',
                                  states=_readonly_state)
    
    @classmethod
    def __setup__(cls):
        super(GroupRoutePlanedDisaptch, cls).__setup__()
        table = cls.__table__()
        # Cuando se migre a tryton5 ajustar partial constraint para que
        # no se pueda repetir vehículo en una misma hora a menos que sea
        # None
        cls._sql_constraints += [
            ('unique_PlanedDispatchVehicleTime', Unique(table, table.vehicle,
                                             table.time,table.route),
                'Este vehiculo ya tiene despacho para esta hora'),
        ]
        cls._order = [
            ('group_route', 'ASC'),
            ('time', 'ASC'),
            ]
        cls._buttons.update({
                'to_dispatch': {
                    'invisible': Eval('dispatch',0) > 0,
                    'help': "Crea un despacho en estado Programado con esta "
                    "planeacion",
                    'icon': 'tryton-forward'
                    },
                })
        
    @classmethod
    def generate_by_day(cls,group_route_id,day):
        """
        Genera despachos planeados para la ruta y el dia indicados
        """
        GroupRoute = Pool().get('public_transport.group_route')
        OrderVehicles = Pool().get('public_transport.group_route.vehicle_order')
         
        groute = GroupRoute.browse([group_route_id])[0]
        order_vehicles = OrderVehicles.get_order_list_for(groute.id,day) 
        type_ = DaysCalc.tipo_de_dia(day)
        routes = [x.id for x in groute.routes]
        jump_for_dispatch = groute.get_jump_route_for_dispatch(type_)
        
        def get_values_with_schedules_route(order_vehicles,routes,all_schedules,groute,type_,jump_for_dispatch):
            #esquema del diccionario
            content = {}

            for turn in range(1,len(order_vehicles) + 1):
                    content[turn]={}
                    for route in routes:
                        content[turn][route] = {'vehiculo':None,
                                        'horarios':[]
                                        }
            #agregando horarios
            for route, turns in all_schedules.items():
                for turn, schedules in turns.items():
                    content[turn][route]['horarios'] = schedules
            #agregando vehiculos
            for idx in range(0,len(order_vehicles)):
                turn = idx + 1
                for idx2 in range(0,len(routes)):
                    content[turn][routes[idx2]]['vehiculo'] = order_vehicles[idx][idx2]
    
            #generar values tryton
            values=[]
            for turn,routes_ in content.items():
                    for route,data in routes_.items():
                        values.extend(cls.get_prototype_for_schedules(groute.id,
                                                                        route if not groute.planed_dispatch_with_out_route else None,
                                                                        type_,
                                                                        data['vehiculo'],
                                                                        turn,
                                                                        data['horarios'],
                                                                        rotate_route=jump_for_dispatch,
                                                                        routes=routes,
                                                                        ))
            return values
        
        def get_values_with_out_schedules_route(order_vehicles,routes,all_schedules,groute,type_,jump_for_dispatch):
            """usado para rutas donde la ruta no es indicada en los turnos, estas rutas los vehiculos ordenados son
            una lista y no una lista de listas.
            """
            #esquema del diccionario
            #{vehiculo:{turno:1},{horario}}
            def get_route(sorted_schedules,schedule,routes):
                return routes[to_idx(sorted_schedules.index(schedule), len(routes))]
            
            content = {}
            for turn in range(1,len(order_vehicles) + 1):
                    content[turn]={'vehiculo':None,
                                        'horarios_rutas':[]#lista de tuplas[(datetime,route_id)]
                                        }
            #horarios ordenados ascendentemente
            def get_sorted_schedules(all_schedules):
                r = []
                for turn,schedules in all_schedules.items():
                    for schedule in schedules:
                        r.append(schedule)
                return sorted(r)
            sorted_schedules = get_sorted_schedules(all_schedules)
            #@todo agregar condición de intercalar ruta entodos los despachos del día en modelo de grupo de ruta.
            #agregando horarios
            for turn, schedules in all_schedules.items():
                for schedule in schedules:
                    idx = sorted_schedules.index(schedule)
                    route_ = get_route(sorted_schedules,schedule,routes)
                    content[turn]['horarios_rutas'].append((schedule,route_))
                    sorted_schedules[idx] = None
            #agregando vehiculos
            for idx in range(0,len(order_vehicles)):
                turn = idx + 1
                content[turn]['vehiculo'] = order_vehicles[idx]
    
            #generar values tryton
            values=[]
            for turn,data in content.items():
                vehicle = data['vehiculo']
                for schedule_route in data['horarios_rutas']:
                    values.append(cls.get_prototype(groute.id,
                                                    schedule_route[1] if not groute.planed_dispatch_with_out_route else None,
                                                    type_,
                                                    vehicle,
                                                    schedule_route[1],
                                                    schedule_route[0],
                                                    turn,
                                                    ))
            return values
        if groute.unique_turn_for_all_route:
            all_schedules = groute.get_schedules(type_,day)
            values = get_values_with_out_schedules_route(order_vehicles,routes,all_schedules,groute,type_,jump_for_dispatch)
        else:
            all_schedules = groute.get_schedules_for_all_routes(type_,day)
            values = get_values_with_schedules_route(order_vehicles,routes,all_schedules,groute,type_,jump_for_dispatch)
        records = cls.create(values)
        cls.save(records)

    @classmethod
    def generate_by_range(cls,group_route_id,init_date,end_date):
        """
        Genera despachos planeados para los dias en el rango, incluyendo la
        fecha inicial y final
        """
        days = (end_date - init_date).days
        for x in range(0,days + 1):
            cls.generate_by_day(group_route_id,init_date + timedelta(days=x))
    
    @classmethod
    def get_prototype_for_schedules(cls,group_route,route,type_,vehicle,turn,
                                    schedules,rotate_route=False,routes=[]):
        r = []
        initial_route = route
        if rotate_route:
            rotator_route = loop_list(routes,routes.index(route))
        for schedule in schedules:
            if rotate_route:
                route = next(rotator_route)
            r.append(cls.get_prototype(group_route, route, type_, vehicle, initial_route,schedule,turn))
        return r
           
    @classmethod
    def get_prototype(cls,group_route=None,route=None,type_=None,vehicle=None,
                      turn_route=None,time=None,turn=None):
        """Devuelve un diccionario con los nombres de los campos como llaves,
        los valores serán vacios a menos que se indiquen como parametros"""
        return {
            'group_route': group_route,
            'route': route,
            'type': type_,
            'vehicle': vehicle,
            'turn_route': turn_route,
            'time': time,
            'turn': turn
            }
        
    @dualmethod
    @ModelView.button
    def to_dispatch(self,planed_dispatches):
        Dispatch = Pool().get('public_transport.dispatch')
        values = []
        for planed_dispatch in planed_dispatches:
            values.append({'group_route': planed_dispatch.group_route,
                           'route': planed_dispatch.route,
                           'vehicle':planed_dispatch.vehicle,
                           'turn':planed_dispatch.turn,
                           'turn_route':planed_dispatch.turn_route,
                           'init_time':planed_dispatch.time,
                           'planed_dispatch':planed_dispatch.id,
                })
        Dispatch.save(Dispatch.create(values))

class PlanedDisaptchDispatch(ModelSQL, ModelView):
    """Relaciona los despachos planeados y los despachos"""
    
    __name__ = 'public_transport.group_route.planed_dispatch_dispatch'
    
    planed_dispatch = fields.Many2One('public_transport.group_route.planed_dispatch','Despacho Planeado',
                                      ondelete='RESTRICT')
    dispatch = fields.Many2One('public_transport.dispatch','Despacho',
                               ondelete='RESTRICT')
    
    @classmethod
    def __setup__(cls):
        super(PlanedDisaptchDispatch, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints +=[
            ('planed_dispatch_key',Unique(table,table.planed_dispatch),
             "El despacho planeado no se puede repetir"),
            ('dispatch_key',Unique(table,table.dispatch),
             "El despacho no se puede repetir"),
            ]
        
class FormGeneratePlanedDispatch(ModelView):
    """Formulario para generar plan de despachos"""
    
    __name__ = 'public_transport.group_route.generate_planed_dispatch.form'
    
    group_route = fields.Many2One('public_transport.group_route','Grupo de Rutas',
                            domain=[()],
                            required=True)
    init_date = fields.Date('Fecha inicial',
                            required=True)
    end_date = fields.Date('Fecha Final',
                           domain=[('end_date','>=',Eval('init_date'))],
                           required=True)
    

class GeneratePlanedDispatch(Wizard):
    """Genera Despachos planeados"""

    __name__ = 'public_transport.group_route.generate_planed_dispatch'
    
    start = StateView('public_transport.group_route.generate_planed_dispatch.form',
              '',[Button('Salir', 'end', 'tryton-cancel'),
                  Button('Generar', 'generate', 'tryton-go-next'),
                  ]
              )
    #@todo mostrar previsualizacion antes de generar
    generate = StateTransition()
    
    def default_start(self,fields): 
        return {
            'group_route':Transaction().context['active_id']
            }
    
    def transition_generate(self):
        GRoute = Pool().get('public_transport.group_route')
        PlanedDispatch = Pool().get('public_transport.group_route.planed_dispatch')
        
        groute = GRoute.browse([self.start.group_route])[0]        
        PlanedDispatch.generate_by_range(groute.id,self.start.init_date, self.start.end_date)
        return 'end'
    
class FormImportOrderedVehicles(ModelView):
    """Formulario para generar plan de despachos"""
    
    __name__ = 'public_transport.group_route.vehicle_order.import.form'
    
    group_route = fields.Many2One('public_transport.group_route','Grupo de Rutas',
                            domain=[()],
                            required=True)
    type = fields.Selection(_route_programation_types,'Tipo de dia',
                         required=True,
                         help="tipo de dia para los que aplica este orden")#@todo definit tipo automaticamente con la seleccion del dia
    order_date = fields.Date('Fecha del orden',
                            required=True,
                            help="Fecha en la que estos vehículos fueron "
                            "despachados en este orden")
    header = fields.Boolean("Cabecera",
                            help="indica si contiene cabeceras")
    content = fields.Selection([('licence_plate','Placas'),('internal_code','Codigo Interno')],'Contenido')
    dialect = fields.Selection([(x,x) for x in  csv.list_dialects()],'Dialecto CSV',
                               required=True,
                               help="https://docs.python.org/3/library/csv.html#csv.Dialect")
    vehicles = fields.Text('Vehiculos',
                           required=True,
                           help="En formato CSV, puede ser una o muchas "
                           "columnas, la lectura se hace de derecha a "
                           "izquierda y de arriba hacia abajo."
                           "\nSi se quiere saltar un vehiculo se debe usar la "
                           "palabra None"
                           "\n Se eliminaran las O"
                           "\nEl vehiculo no puede estar repetido")
    
    @staticmethod
    def default_content():
        return 'internal_code'
    
    @staticmethod
    def default_dialect():
        return csv.list_dialects()[0]
    
class ImportOrderedVehicles(Wizard):
    """Importa Vehiculos ordenados"""

    __name__ = 'public_transport.group_route.vehicle_order.import'
    
    start = StateView('public_transport.group_route.vehicle_order.import.form',
              '',[Button('Salir', 'end', 'tryton-cancel'),
                  Button('Importar', 'import_', 'tryton-go-next'),
                  ]
              )
    #@todo mostrar previsualizacion antes de generar
    import_ = StateTransition()
    
    
    def transition_import_(self):
        Vehicle = Pool().get('fleet.vehicle')
        VehicleOrder = Pool().get('public_transport.group_route.vehicle_order')
        ParamsTypeDay = Pool().get('public_transport.group_route.params_type_day')
        
        def get_vehicle_id(string,type_,group_route):
            conditions = [(type_,'=',string),
                          ('routes','=',group_route)]
            record = Vehicle.search(conditions)
            if record:
                return record[0].id
            else:
                return None
        
        date_order = ParamsTypeDay.search([('group_route','=',self.start.group_route),
                                       ('type','=',self.start.type)])

        if len(date_order) != 1:
            VehicleOrder.raise_user_error("Primero se debe definir los "
                                          "parámetros para este tipo de día{t} "
                                          "en el Grupo de Ruta{gr}".format(
                                              t=self.start.type,
                                              gr=self.start.group_route
                                              ))
        vehicles = []
        #se elimina la "O" y se extrae la lista
        with StringIO(self.start.vehicles.replace("O",'')) as f:
            reader = csv.reader(f, self.start.dialect)
            if self.start.header:
                next(reader)
            for row in reader:
                vehicles.extend(row)
        #verificar que la cantidad de vehiculos no exceda el limite del grupo
        #de ruta
        if len(vehicles) > self.start.group_route.limit_vehicles:
            VehicleOrder.raise_user_error("Ingreso {lv} vehiculos y solo se "
                                          "permiten {tq} para el grupo de ruta "
                                          "{gr}".format(
                                              lv=len(vehicles),
                                              tq=self.start.group_route.limit_vehicles,
                                              gr=self.start.group_route
                                              ))
        #convertir "None" a None
        for x in range(0,len(vehicles)):
            if vehicles[x] == "None":
                vehicles[x] = None
        #convertir a diccionario
        ordered_vehicles = {x+1:get_vehicle_id(vehicles[x],self.start.content,self.start.group_route) \
                             for x in range(0,len(vehicles))}
        #creando values
        values = []
        for k,v in ordered_vehicles.items():
            if v:
                values.append({'group_route':self.start.group_route.id,
                               'type':self.start.type,
                               'vehicle':v,
                               'order':k,
                    })
        #reemplazando orden anterior
        VehicleOrder.delete(VehicleOrder.search([('group_route','=',self.start.group_route),
                                                 ('type','=',self.start.type)]))
        records = VehicleOrder.create(values)
        VehicleOrder.save(records)
        #cambiando fecha
        date_order[0].date = self.start.order_date
        ParamsTypeDay.save(date_order)
        
        return 'end'
    
class FormImportSchedules(ModelView):
    """Formulario para importar horarios de grupos de rutas
    """
    
    __name__ = 'public_transport.group_route.schedule.import.form'
    
    group_route = fields.Many2One('public_transport.group_route','Grupo de Rutas',
                            domain=[()],
                            required=True)
    duplicate_for_routes = fields.Boolean('Duplicar para cada ruta',
                                          help="Indica si los horarios "
                                          "pasados se debe repetir para cada "
                                          "ruta del grupo de rutas Ejemplo en "
                                          "Belén los horarios de las 315 y "
                                          "las 316 son iguales, por eso se "
                                          "puede pasar los horarios de una "
                                          "sola ruta y activar esta opcion "
                                          "para crear el doble")
    route = fields.Many2One('public_transport.route','Ruta',
                            domain=[('group','=',Eval('group_route'))],
                            depends=['group_route','route_required'],
                            states={'required':Bool(Eval('route_required',True)),
                                    'readonly':Bool(Eval('duplicate_for_routes')),
                                    'invisible':Bool(Eval('duplicate_for_routes')),
                                })
    route_required = fields.Function(fields.Boolean('Requiere ruta'),'on_change_with_route_required')
    dialect = fields.Selection([(x,x) for x in  csv.list_dialects()],'Dialecto CSV',
                               required=True,
                               help="https://docs.python.org/3/library/csv.html#csv.Dialect")
    header_first_col = fields.Char("Encabezado primera columna",
                                   required=True,
                                   help="Normalmente HO o HOR")
    type = fields.Selection(_route_programation_types,'Tipo de dia',
                         required=True,
                         help="tipo de dia para los que aplica estos horarios")#@todo definit tipo automaticamente con la seleccion del dia

    
    schedules = fields.Text('Horarios',
                           required=True,
                           help="En formato CSV, ver "
                            "ejemplo para entender estructura")
    
    example = fields.Text('Ejemplo',
                          readonly=True,
                          help="Ejemplo Belén 2019 ordinarios para una ruta "
                          "que se duplica")
    
    @staticmethod
    def default_header_first_col():
        return 'HO'
    
    @staticmethod
    def default_example():
                return """HO,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22
1,30,40,,0,36,D,D,27,,9,45,,45,,,,,,
2,,36,57,,33,,33,D,D,6,42,,42,,42,,,,
3,,,12,48,,36,,18,D,51,,36,,36,,15,,,
4,,24,48,,24,,24,D,57,,33,,33,,48,,,,
5,,,,,,57,,51,,33,,9,,9,54,,21,,10
6,,,,33,,9,54,,27,D,D,57,,57,,36,,10,
7,,,42,,18,,18,,0,D,D,18,,18,57,,28,,
8,,,3,39,,27,,9,D,42,,27,,27,,6,,,
9,35,44,,3,39,D,D,30,,12,48,,48,,,,,,
10,,,,,,15,,0,33,,9,,9,,9,48,,24,
11,,,,15,51,,36,,9,D,D,21,,21,,0,35,,
12,,,6,42,,30,,12,D,45,,30,,30,,9,,,
13,,32,54,,30,,30,D,D,3,39,,39,,39,,,,
14,40,48,,6,42,D,D,33,,15,51,,51,,,,,,
15,,,,,,18,,3,36,,12,,12,,12,51,,31,
16,,,,18,54,,39,,12,D,D,24,,24,,3,42,,
17,,,9,45,,33,,15,D,48,,33, ,33,,12,,,
18,,28,51,,27,,27,D,D,0,36,,36,,36,,,,
19,45,52,,9,45,D,D,36,,18,54,,54,,,,,,
20,,,,,,21,,6,39,,15,,15,,15,54,,38,
21,,,,21,57,,42,,15,D,D,45,,45,,24,49, ,
22,,,15,51,,39,,21,D,54,,39,,39,,18,,,
23,,20,45,,21,,21,D,54,,30, ,30,,45,,,,
24,,0,21,57,,45,D,D,6,39,,15,,15,,,,,
25,,,,,,48,,42,,24,,0,,0,30,,0,45,
26,,,,24,,0,45,,18,D,D,48,,48,,27,56,,
27,,,18,54,,42,,24,D,57,,42,,42,,21,,,
28,50,56,,12,48,D,D,39,,21,57,,57,,,,,,
29,,8,27,,3,,3,D,45,,21,,21,,21,,,,
30,,,,,,51,,45,,27,,3,,3,33,,7,52,
31,,,,27,,3,48,,21,D,D,51,,51,,30,,,
32,,,36,,12,,12,54,D,D,0,,0,,0,39,,,
33,,12,30,,6,,6,D,48,,24,,24,,24,,,,
34,,4,24,,0,,0,D,42,,18,,18,,18,,,,
35,,,,,,54,,48,,30,,6,,6,51,,14,,0
36,,,,30,,6,51,,24,D,D,54,,54,,33,,3,
37,,,39,,15,,15,57,D,D,3,,3,,3,42, ,,
38,55,,0,36,,24,D,D,3,36,,12,,12,,,,,
39,,16,33,,9,,9,D,51,,27,,27,,27,,,,
40,,,,,,12,57,,30,,6,,6,,6,45,,17,"""
    
    @staticmethod
    def default_dialect():
        return csv.list_dialects()[0]
    
    @fields.depends('group_route','duplicate_for_routes')
    def on_change_with_route_required(self,names=""):
        if self.group_route:
            if self.duplicate_for_routes:
                return False
            elif self.group_route.unique_turn_for_all_route:
                return False
        return True
    
class ImportSchedules(Wizard):
    """Importa Horarios para la realización de despachos planeados"""

    __name__ = 'public_transport.group_route.schedule.import'
    
    start = StateView('public_transport.group_route.schedule.import.form',
              '',[Button('Salir', 'end', 'tryton-cancel'),
                  Button('Importar', 'import_', 'tryton-go-next'),
                  ]
              )
    import_ = StateTransition()
    

    def transition_import_(self):
        Schedule = Pool().get('public_transport.group_route.schedule')
        DurationRule = Pool().get('public_transport.route.dispatch_duration_rule')
        # values to ignore in minute field on schedules csv data
        NOT_MINUTE_VALUES = ['','D',' ']

        def csv2list(text,dialect,header=False):
            r = []
            with StringIO(text) as f:
                reader = csv.reader(f, dialect)
                if header:
                    next(reader)
                for row in reader:
                    r.append(row)
            return r
        
        def csv2list_dict(text,dialect):
            r = []
            with StringIO(text) as f:
                reader = csv.DictReader(f)
                for row in reader:
                    r.append(row)
            return r
        
        def values_from_csv(list_order_dic, type_,
                            group_route, header_first_col,
                            routes, without_route=False):
            r = []
            for row in list_order_dic:
                turn = row[header_first_col]
                for hour, minute in row.items():
                    if hour == header_first_col or minute in NOT_MINUTE_VALUES \
                    or not minute.isdigit():
                        continue
                    schedule = {
                        'turn': int(turn),
                        'hour': int(hour),
                        'minute': int(minute),
                        'type': type_,
                        'group_route': group_route,
                        }
                    if without_route:
                        schedule['duration'] = DurationRule.get_duration_for(
                            time(int(hour), int(minute)),
                            routes[0],
                            type_)
                        r.append(schedule)
                    else:
                        for route in routes:
                            schedule_route = schedule.copy()
                            schedule_route['route'] = route
                            schedule_route['duration'] = DurationRule.get_duration_for(
                                time(int(hour), int(minute)),
                                route,
                                type_)
                            r.append(schedule_route)
            return r
        
        def get_turn_quantity(group_route, type_):
            if group_route.params_type_days:
                for q in group_route.params_type_days:
                    if q.type == type_:
                        return q.turn_quantity
        
        if not get_turn_quantity(self.start.group_route, self.start.type):
            self.raise_user_error("El grupo de Ruta no tiene definido la "
                                  "cantidad de turnos para el tipo de día")
        
        content = csv2list_dict(self.start.schedules,dialect=self.start.dialect)
        if self.start.route:
            routes = [self.start.route.id]
        else:
            routes = [x.id for x in self.start.group_route.routes]
        values = values_from_csv(content, self.start.type,
                                 self.start.group_route.id,
                                 self.start.header_first_col,
                                 routes,
                                 self.start.group_route.unique_turn_for_all_route)
        records = Schedule.create(values)
        Schedule.save(records)
        return 'end'


class DispatchDurationRule(ModelSQL, ModelView):
    "Dispatch Duration Rule"
    __name__ = 'public_transport.route.dispatch_duration_rule'
    _order = [('route', 'ASC'),('init_time', 'ASC')]
    
    route = fields.Many2One('public_transport.route', 'Ruta',
                            domain=[],
                            required=True)
    init_time = fields.Time('Hora inicial', required=True,
                            domain=[('init_time', '<',
                                     Eval('end_time'))],
    )
    end_time = fields.Time('Hora Final', required=True,
                           domain=[('init_time', '<',
                                    Eval('end_time'))],
    )
    duration = fields.Integer('Duración', required=True,
                              help="Duración en minutos")
    day_type = fields.Selection(_route_programation_types,
                                'Tipo de día',
                                required=True)
    
    
    def pre_validate(self):
        super(DispatchDurationRule, self).pre_validate()
        records_in_interval = self.search(
            [('route', '=', self.route),
             ('day_type', '=', self.day_type),
             ('id', '!=', self.id),
             ['OR',
              [('init_time', '>=', self.init_time),
               ('init_time', '<=', self.end_time)
              ],
              [('end_time', '>=', self.init_time),
               ('end_time', '<=', self.end_time)
              ]
             ],
            ]
        )
        if records_in_interval:
            self.raise_user_error("Existen otras reglas en el intervalo "
                                  "de tiempo indicado")

    @classmethod
    def get_duration_for(cls, _time, route, day_type):
        """ get duration for _time, route and type_day,
        if not duration rule return 0
        """
        rule = cls.search([('route', '=', route),
                           ('day_type', '=', day_type),
                           ('init_time', '<=', _time),
                           ('end_time', '>=', _time)])
        assert len(rule) <= 1 , "Many rules for time"
        if rule:
            return rule[0].duration
        else:
            return 0
