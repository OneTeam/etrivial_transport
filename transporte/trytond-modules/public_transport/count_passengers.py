# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport, StateAction
from decimal import Decimal
import logging
import csv
from io import StringIO,BytesIO
from trytond.report.report import Report
from .graficos import *
 
__all__ = ['PassangersCountStadisticFilter','PassangersCountStadisticCsv',
           'PassengersCountStadistic','CreateVehiclesFromMetro']


 
# class PassangersForDay(ModelSQL,ModelView):
#     """Conteo de pasajeros"""
#     
#     __name__ = 'public_transport.passangers_for_day'
#     
#     date = fields.date('fecha',
#                        required=True)
#     vehicle = fields.Many2One('fleet.vehicle','Vehiculo')
#     count_metro_civica = fields.Function(fields.Integer('Cantidad Metro Civica'),'calc_count_metro_civica')
#     count_ingresos_sonar = fields.Function(fields.Integer('Cantidad ingresos sonar'),'calc_count_entradas_sonar')
#     count_salidas_sonar = fields.Function(fields.Integer('Cantidad salidas sonar'),'calc_count_salidas_sonar')
#     count_bloqueos_sonar = fields.Function(fields.Integer('Cantidad bloqueos sonar'),'calc_count_bloqueos_sonar')
#     
#     @classmethod
#     def calc_calc_count_metro_civica(cls):
#         if cls.vehicle and cls.date:
#             Metro_Civica = Pool().get('metro_civica.metro_civica')
#             #t_metro_civica = Metro_Civica.__table__()@usar consulta sql
#             cursor = Transaction().connection.cursor()
#             sql = """select sum(a.total_cantidad)
#             from metro_civica_metro_civica AS a where
#             a.fleet_vehicle = {vehicle_id} and 
#             a.fecha = {fecha};
#             """.format(vehicle_id=cls.vehicle.id,
#                        fecha=str(cls.date)
#                        )
#             cursor.execute(sql)
#             quantity = cursor.fetchall()
#             if quantity[0][0]:
#                 return quantity[0][0]
#             else:
#                 return 0
#         else:
#             return 0
#
class PassangersCountStadisticFilter(ModelView):
    """ItineraryHisoty filter"""
    __name__ = 'public_transport.passengers_count.filter'
    
    date = fields.Date('Fecha',
                            required=True,
                            )
#     end_time = fields.DateTime('Fecha Final',
#                            required=True,
#                             domain=[('end_time','>',Eval('init_time'))]
#                            )
#     mobiles = fields.Many2Many('datos_sonar.mobiles',None,None,'Vehiculos',
#                                 help="Vehiculo al que hace referencia el reporte",
#                                 required=True,
#                                 )

class PassangersCountStadisticCsv(Report):
    """este reporte no usa como ingreso los ids seleccionados
    obligatoriamenete requiere data para filtrar los resultados.
    
    data['filter'] = {'init_time':time_object,'end_time':time_object,'mobiles':[mobil_id,...],'min_percentage':None or Decimal 0.0-1.0,
                    'title':title| None,'type':'png' | 'svg' | None}
    """
    
    __name__ = 'public_transport.passengers_count.stadistic_csv'

    @classmethod
    def execute(cls, ids, data):
        if data:
            if 'filter' not in data.keys():
                raise Exception('Error','Report (%s) required filter info, not direct execution' % cls.__name__)
        else:
            raise Exception('Error','Report (%s) required filter info, not direct execution' % cls.__name__)
        
        ActionReport = Pool().get('ir.action.report')

        action_report_ids = ActionReport.search([
            ('report_name', '=', cls.__name__)
            ])
        if not action_report_ids:
            raise Exception('Error', 'Report (%s) not find!' % cls.__name__)
        action_report = ActionReport(action_report_ids[0])
        if 'type'in data['filter'].keys():
            if data['filter']['type'] == 'png':
                data = cls.get_png_data(ids,data)
                return ('png', fields.Binary.cast(data), False, action_report.name)
        data = cls.get_csv_data(ids,data)
        return ('csv', fields.Binary.cast(data,'utf8'), False, action_report.name)
    
    @classmethod
    def get_data(cls,ids,data):
        cursor = Transaction().connection.cursor()
        date = data['filter']['date']
        date_init = date.replace(day=date.day-1) if date.day > 1 else date
        date_end = date
           
        sql = """
SELECT
 a.licence_plate AS placa,
 b.cantidad AS metro_cantidad,
 c.sonar_ingresos AS sonar_ingresos,
 c.sonar_salidas AS sonar_salidas,
 c.sonar_bloqueos AS sonar_bloqueos
FROM fleet_vehicle AS a
LEFT JOIN
(
SELECT
 a.fleet_vehicle AS fleet_vehicle,
 sum(a.total_cantidad) AS cantidad
FROM metro_civica_metro_civica AS a
WHERE
 a.fecha = '{date:%Y-%m-%d}'
GROUP BY
 a.fleet_vehicle
)
as b on b.fleet_vehicle = a.id
LEFT JOIN
(
SELECT
 a.vehicle AS fleet_vehicle,
 b.sonar_ingresos,
 b.sonar_salidas,
 b.sonar_bloqueos
from datos_sonar_fleet_mobile_vehicle AS a
LEFT JOIN
(
SELECT
 a.mobil_id AS mobile_id,
 sum(a.p_ingresos) AS sonar_ingresos,
 sum(a.p_salidas) as sonar_salidas,
 sum(a.p_bloqueos) AS sonar_bloqueos
FROM datos_sonar_passengers_log AS a
WHERE
 a.p_total = 'Y'
 and a."gps_UTC" >= '{date_init:%Y-%m-%d} 19:00:00'
 and a."gps_UTC" <= '{date_end:%Y-%m-%d} 19:00:00'
group by a.mobil_id
) as b
 ON a.mobile = b.mobile_id
) as c on c.fleet_vehicle = a.id;
""".format(date=date,
           date_init=date_init,
           date_end=date_end)
        cursor.execute(sql)
        col_sql = [x[0] for x in cursor.description]
        rows = cursor.fetchall()
        if data['filter']['title']:
            title = data['filter']['title']
        else:
            title = 'Reporte'
        return rows,col_sql,title
    
    @classmethod
    def get_csv_data(cls,ids,data):
        rows,col_sql,title = cls.get_data(ids, data)
        
        str_csv = cls.list_to_csv_string(rows,col_sql,title)
        return str_csv
    
    @classmethod
    def list_to_csv_string(cls,rows,header=None,title=None,dialect='excel'):
            """Convert list to csv string.
            
            @param rows: list with row data.
            @param header: list with header data.
            @param dialect: dialect valid for csv module, excel by default.
            return string in csv format.
            """
            string = StringIO()
            writer = csv.writer(string,dialect=dialect)
            if title:
                if isinstance(title, str):
                    writer.writerow([title])
                else:
                    writer.writerow(title)
            if header:
                writer.writerow(header)
            list(writer.writerow(x) for x in rows)
            str_csv = string.getvalue()
            string.close()
            return str_csv
    
    @classmethod
    def get_png_data(cls,ids,data):
        rows,col_sql, title = cls.get_data(ids, data)

        placas = set(x[col_sql.index('placa')] for x in rows)
        placas = sorted(tuple(placas))
        pasajeros = tuple(['metro_cantidad','sonar_ingresos','sonar_salidas','sonar_bloqueos'])
        means = {x:list([0] * len(placas)) for x in pasajeros}
        for row in rows:
            for x in range(0,len(pasajeros)):
                means[pasajeros[x]][placas.index(row[col_sql.index('placa')])] = ( row[col_sql.index(pasajeros[x])] or 0)
        values = [tuple([key,tuple(int(x) for x in value)]) for key,value in means.items()]
        title = " ".join(title) if isinstance(title, list) else title 
        file = grafico_archivo_columnas_multiples(placas,values,
                                                 etiqueta_x='Vehiculos',etiqueta_y='Pasajeros',
                                                 formato=data['filter']['type'])
        return file
        
class PassengersCountStadistic(Wizard):
    """Wizard to make csv stadistic from Passengers."""
    
    __name__ = 'public_transport.passengers_count.stadistic'
    
    start = StateView('public_transport.passengers_count.filter',
        '',[
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Csv', 'csv', 'tryton-ok', default=True),
            Button('Png', 'png', 'tryton-ok'),
            ])
    csv = StateAction('public_transport.report_count_passengers_stadistic_csv')
    png = StateAction('public_transport.report_count_passengers_stadistic_csv')
    
    def do_csv(self, action):
        return action, {
            'id': None,
            'ids': None,
            'filter':{
                'date':self.start.date,
#                 'end_time':self.start.end_time,
#                 'mobiles':[x.id for x in self.start.mobiles],
                'title':None,
                'type':'csv'
                }
            }
    def do_png(self, action):
        return action, {
            'id': None,
            'ids': None,
            'filter':{
                'date':self.start.date,
#                 'end_time':self.start.end_time,
#                 'mobiles':[x.id for x in self.start.mobiles],
                'title':None,
                'type':'png'
                }
            }

class CreateVehiclesFromMetro(Wizard):
    """Wizard to make csv stadistic from Passengers. @todo pasar a vehicles dentro de este modulo"""
    
    __name__ = 'public_transport.passengers_count.create_vehicles_from_metro'
    
    start = StateTransition()
    
    def transition_start(self):
        #Metro_Civica = Pool().get('metro_civica.metro_civica')#@todo transcribir consulta sql
        Vehicle = Pool().get('fleet.vehicle')
        cursor = Transaction().connection.cursor()
        sql = """
select a.equipo from metro_civica_metro_civica as a where fleet_vehicle IS NULL group by a.equipo;
"""
        cursor.execute(sql)
        rows = cursor.fetchall()
        #buscando las placas
        licence_plates = set()
        for row in rows:
            licence_plate = row[0].strip().replace(' ','')[0:6].upper()
            licence_plates.add(licence_plate[0:3] + licence_plate[3:6])
        
        records = Vehicle.search(['licence_plate','in',licence_plates])
        #eliminando las placas que existe
        if records:
            for record in records:
                licence_plates.discard(record.licence_plate)
        
        values = [{
            'licence_plate':x,
            'chassis_brand':1,
            'bodywork_brand':1,
            'licence_plate_city':4848,
            'licence_plate_country':50,
            'licence_plate_departament':580,
            'chassis_serial':x,
            'motor_serial':x,
            'european_standard':1,
            'doors':2,
            'model':1,
            'model_year':2010,
            'seated_passangers':31,
            'active':True,
            'tpp':True,
            'type_mobile_tpp':1,
            'corporate_image':1,
            'action_ration':1,} for x in licence_plates]
        instances = Vehicle.create(values)
        Vehicle.save(instances)
        return 'end'
