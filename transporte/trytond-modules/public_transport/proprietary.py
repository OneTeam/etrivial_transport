# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Administradoras (y Aseguradoras) del sistema de Seguridad Social

'''
from datetime import timedelta
import logging


from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pyson import Eval, Bool, Not
from trytond import backend


logger = logging.getLogger(__name__)

__all__ = ['Proprietary','ProprietaryContract','ProprietaryContractDriver']

_type = [
    ('poseedor','Poseedor'),
    ('tenedor','Tenedor'),
    ('inscrito','Inscrito'),
    ('locatario','Locatario'),
    ]
_causes_of_end_contract =[('',''),
                          ('traspaso','Traspaso'),
                         ('vencimiento tarjeta operacion','Vencimiento de la Tarjeta de Operación'),
                         ('cancelacion de contrato','Cancelación de Contrato'),
                         ('mutuo acuerdo','Mutuo Acuerdo')]

_type_contracts = [('traspaso','Traspaso'),
                 ('renovacion tarjeta de operacion','Renovación Tarjeta de Operación')]

class Proprietary(ModelSQL,ModelView):
    '''Propietarios (personas - party) relacionados en los contratos.'''
    
    __name__ = "public_transport.proprietary"
    
    proprietary = fields.Many2One('party.party', 'Propietario',
                        required=True,
                        ondelete='RESTRICT',
                        domain=[('active','=',True)],
                        help="Tercero.")
    main = fields.Boolean('Propietario Facturación',
                          help="Para fines Contables y administrativos, pero\n"
                          "no quita responsabilidad a los otros propietarios.\n"
                          "Debe haber uno, y solo un, propietario principal por\n"
                          "contrato."
                          )
    administrator = fields.Many2One('party.party', 'Administrador',
                        required=False,
                        ondelete='RESTRICT',
                        domain=[('active','=',True),['id','!=',Eval('proprietary',[])]],
                        help="Administrador para este propietario en este contrato")
    type = fields.Selection(_type,'Tipo',
                            required=True,
                            help="Indica el tipo de propietario que representa"
                            )
    contract = fields.Many2One('public_transport.proprietary_contract','Contrato',
                               required=True,
                               ondelete='CASCADE',
                               )
    internal_code = fields.Function(fields.Char('Número Interno',size=4),
                                    'internal_code_get')
    note = fields.Text('Observaciones')
    vehicle = fields.Function(fields.Many2One('fleet.vehicle','Vehiculo'),'vehicle_get')
    
    @classmethod
    def __setup__(cls):
        super(Proprietary, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('proprietary_contract_key', Unique(table, table.proprietary,table.contract),
                'Propietarios: El contrato ya tiene el propietario vinculado'),
        ]
        #Activar history para este modelo
        cls._history = True

    
    def vehicle_get(self,name=None):
        return self.contract.vehicle.id
    
    def get_rec_name(self, name):
        if self.proprietary.name:
            return self.proprietary.name
        else:
            return self.proprietary.id
    
    def internal_code_get(self,name=None):
        if self.contract:
            return self.contract.internal_code_get()
        else:
            return ''


class ProprietaryContract(ModelSQL,ModelView):
    """Contrato de propietarios"""
    
    __name__ = 'public_transport.proprietary_contract'
    #_order = [('code','DESC')]
    _rec_name = 'code'
    
    type = fields.Selection(_type_contracts,'Tipo',
                             required=True,
                             )
    code = fields.Char('Codigo',
                       required=True,
                       help="Codigo del contrato"
                       )
    proprietaries = fields.One2Many('public_transport.proprietary','contract','Propietarios',
                                    required=True,
                                    help='propietarios vinculados a este contrato')
    main_proprietary = fields.Function(fields.Many2One('public_transport.proprietary','Propietario de Facturacion',
                                    help="Propietario vinculado en los "
                                    "procesos del vehículo como lo contable"
                                    ),'on_change_with_main_proprietary')
    contract_date = fields.Date('Fecha de Contrato',required=True)
    valid_from = fields.Date('Vigente Desde',
                            help="Tomada con base a la tarjeta de operacion o "
                            "la fecha de la Licencia de Transito si es "
                            "traspaso",
                            #readonly=True,
                           depends=['operation_card','transit_registration',
                                    'type'],
                           )
    valid_to = fields.Date('Vigente hasta',
                           help="Tomada con base a la tarjeta de operación o "
                           "un día antes de la nueva Licencia de Transito si "
                           "es Traspaso",
                           #readonly=False,
                           depends=['operation_card','transit_registration',
                                    'type','cause_of_end',
                                    'next_transit_registration'],
                           states={'readonly':Eval('cause_of_end','').in_([None,'','traspaso','vencimiento tarjeta operacion'])
                               }
                           )
    internal_letter = fields.Selection([('B','B'),('M','M'),('','')],'letra',
                                    help="Letra que junto con el número forman el numero interno.\n"
                                    "Debe coincidir con el tipo de vehículo del vehículo seleccionado.",
                                    required=True,
                                    readonly=False,
                                    )
    internal_number = fields.Integer('Numero',
                                     help="Este mismo número interno no debe tener otros contratos vigentes",
                                     required=True,
                                     domain=[('internal_number','>=',0),
                                             ('internal_number','<=',1999)]
                                     )
    internal_code = fields.Function(fields.Char('Número Interno',size=4),
                                    'internal_code_get')
    vehicle = fields.Many2One('fleet.vehicle','Vehiculo',
                              required=True,
                              ondelete='RESTRICT',
                              help="Vehciulo homologado para el servicio de Transporte Publico de Pasajeros",
                              domain=[('tpp','=',True),('active','=',True)],
                              depends=['operation_card','transit_registration']
                                      )
    operation_card = fields.Many2One('fleet.vehicle.operation_card','Tarjeta de Operación',
                                     required=False,
                                     ondelete='RESTRICT',
                                     help="Si el contrato es por Renovacion de "
                                     "T.O no debe haber un contrato anterior "
                                     "Con la misma tarjeta de operacion y si "
                                     "es por Traspaso debe ser la misma del "
                                     "Último Contrato",
                                     domain=[('vehicle','=',Eval('vehicle'))],
#                                      states={#'invisible':Bool(Eval('in_operation_card_tramit')),
#                                              'required':~Bool(Eval('in_operation_card_tramit')),
#                                              #'readonly':~Bool(Eval('vehicle',False))
#                                              },
#                                      depends=['in_operation_card_tramit','vehicle']
                                        )
    in_operation_card_tramit = fields.Boolean('En tramite de tarjeta de operación',
                                              depends=['operation_card','valid_to'
                                                       'valid_from'])
    transit_registration = fields.Many2One('fleet.vehicle.registration','Licencia de transito',
                                     required=True,
                                     ondelete='RESTRICT',
                                     help="Si el contrato es por Traspaso no "
                                     "debe haber un contrato anterior con la "
                                     "misma licencia y si es por renovacion "
                                     "de la T.O debe ser la misma del "
                                     "Último Contrato",
                                     domain=[('vehicle','=',Eval('vehicle'))],
                                     states={'readonly':~Bool(Eval('vehicle',False))}
                                     )
    date_transit_registration = fields.Function(fields.Date('date_transit_registration',
                                                            depends=['transit_registration']),
                                                'on_change_with_date_transit_registration')
    cause_of_end = fields.Selection(_causes_of_end_contract,'Causa de Finalizacion',
                    help="Motivo de Finalización del Contrato",
                    readonly=False,
                    )
    next_transit_registration = fields.Many2One('fleet.vehicle.registration','Nueva Licencia de transito',
                                     required=False,
                                     ondelete='RESTRICT',
                                     help="Licencia de Transito que causo la "
                                     "finlización del contrato",
                                     domain=[('vehicle','=',Eval('vehicle')),
                                             ('expedition_date','>',Eval('date_transit_registration')),
                                         ],
                                     states={'invisible':~Bool(Eval('cause_of_end','').in_(['traspaso'])),
                                             'required':Eval('cause_of_end','').in_(['traspaso']),
                                             },
                                     depends=['date_transit_registration','cause_of_end']
                                     )
    
    note = fields.Text('Observaciones')
    
    @fields.depends('transit_registration')
    def on_change_with_date_transit_registration(self,name=None):
        """Campo funcion para acceder a la fecha de la licencia de transito"""
        if self.transit_registration:
            return self.transit_registration.expedition_date
        else:
            return None

    @fields.depends('vehicle','operation_card','transit_registration',
                    'type','in_operation_card_tramit')
    def on_change_with_valid_from(self):
        if self.in_operation_card_tramit:
                return None
        elif self.type:
            if self.type == 'traspaso':
                if self.transit_registration:
                    return self.transit_registration.expedition_date
            elif self.type == 'renovacion tarjeta de operacion':
                if self.operation_card:
                    return self.operation_card.valid_from
        return None
            
            
    
    @fields.depends('vehicle','operation_card','transit_registration',
                    'type','cause_of_end','next_transit_registration',
                    'in_operation_card_tramit')
    def on_change_with_valid_to(self):
        if self.in_operation_card_tramit:
                return None
        elif self.cause_of_end:
            if self.cause_of_end == 'traspaso':
                if self.next_transit_registration:
                    return self.next_transit_registration.expedition_date - timedelta(days=1)
            elif self.cause_of_end == 'vencimiento tarjeta operacion':
                if self.operation_card:
                    return self.operation_card.valid_to
            else:
                return None
        elif self.operation_card:
                return self.operation_card.valid_to
        return None
    
    @fields.depends('vehicle')
    def on_change_with_transit_registration(self):
        return None
    
    @fields.depends('vehicle')
    def on_change_with_operation_card(self):
        return None
    
    
#     @fields.depends('operation_card_tramit','transit_registration')
#     def on_change_vehicle(self):
#         if not self.vehicle:
#             if self.operation_card:
#                 self.operation_card = None
#             if self.transit_registration:    
#                 self.transit_registration = None
#         if self.operation_card:
#             if self.vehicle != self.operation_card.vehicle:
#                 self.operation_card = None
#         if self.transit_registration:
#             if self.vehicle != self.transit_registration.vehicle:
#                 self.transit_registration = None
            
#     @fields.depends('operation_card','vigent_to','vigent_from')
#     def on_change_in_operation_card_tramit(self):
#         if self.in_operation_card_tramit:
#             self.operation_card = None
#             self.valid_from = None
#             self.valid_to = None
    
        
    @fields.depends('vehicle')
    def on_change_with_internal_letter(self):
        if self.vehicle:
            if self.vehicle.type_mobile_tpp:
                return self.vehicle.type_mobile_tpp.code
    
    def internal_code_get(self,name=None):
        if self.internal_letter and self.internal_number:
            return self.internal_letter + str(self.internal_number).zfill(3)
        else:
            return ''
    
    @fields.depends('proprietaries')
    def on_change_with_main_proprietary(self,name=None):
        if self.proprietaries:
            for proprietary in self.proprietaries:
                if proprietary.main:
                    return proprietary.id
        return
    
    @classmethod
    def __setup__(cls):
        super(ProprietaryContract, cls).__setup__()
        
        table = cls.__table__()
        cls._sql_constraints += [
            ('contract_code_type_mobile_key', Unique(table, table.code),
                'Contrato: El codigo debe ser unico')
        ]
        #toca eliminar la columna cupo migracion de la 0.0.1
        #Activar history para este modelo
        cls._history = True
    
    @classmethod
    def __regegister__(cls,module_name):
        TableHandler = backend.get('TableHandler')    
        super(ProprietaryContract, cls).__register__(module_name)
        
        table = TableHandler(cls, module_name)
        # Migration from XXX: drop cupo
        if table.column_exist('cupo'):
            logger.warning(
                'The column "cupo" on table "%s" must be dropped manually',
                cls._table)
    
    def pre_validate(self):
        super(ProprietaryContract, self).pre_validate()
        self.check_operation_card()
        self.check_transit_registration()
        self.check_type_vehicle()
        self.check_proprietaries()
        self.check_valid_to()
        self.check_valid_from()
        self.check_overlap_contract()
    
    def check_operation_card(self):
        self.check_in_operation_card_tramit()
        if self.type== 'traspaso':
            if self.operation_card.valid_from > self.transit_registration.expedition_date:
                self.raise_user_error("Para traspasos la fecha de la "
                                      "Licencia debe ser posterior al inicio "
                                      "de la vigencia de la tarjeta de "
                                      "operación")
        elif self.type == 'renovacion tarjeta de operacion':
            if not self.in_operation_card_tramit:
                if self.operation_card.valid_from < self.transit_registration.expedition_date:
                    self.raise_user_error("Para renovaciones de T.O la fecha de "
                                          "la Licencia debe ser anterior al "
                                          "inicio de la vigencia de la tarjeta de "
                                          "operación")
                if self._previous_contracts_with_operation_card():
                    self.raise_user_error("contratos anteriores no deben tener "
                                          "esta tarjeta de operacion por que el "
                                          "contrato es por renovacion")
    
    def check_transit_registration(self):
        if self.type == 'traspaso':
            if self._previous_contracts_with_transit_registration():
                self.raise_user_error("contratos anteriores no deben tener "
                                      "esta licencia de transito por que el "
                                      "contrato es por Traspaso")
    
    def check_type_vehicle(self):
        "Valida que el tipo de vehiculo coincida con el codigo interno"
        if self.vehicle.type_mobile_tpp.code != self.internal_letter:
            self.raise_user_error("El codigo interno no es correcto para el vehiculo seleccionado")
            
    def check_proprietaries(self):
        """Valida los propietrios: propietario principal y propietario de matricula"""
        self.check_main_propietary()
        if self.transit_registration:
            if self.transit_registration.party not in \
               [proprietary.proprietary for proprietary in self.proprietaries]:
                self.raise_user_error("La persona de la licencia de Transito "
                                      "debe aparecer como propietaria en el "
                                      "contrato")
    
    def check_valid_to(self):
        if self.cause_of_end == 'traspaso':
            if self.valid_to != self.next_transit_registration.expedition_date - timedelta(days=1):
                self.raise_user_error("Los contratos finalizados por traspaso "
                                      "son validos hasta la expedición de la "
                                      "siguiente licencia de Transito menos "
                                      "un día")
        elif self.cause_of_end == 'vencimiento tarjeta operacion':
            if self.valid_to != self.operation_card.valid_to:
                self.raise_user_error("Los contratos finalizados por "
                                      "vencimiento de T.O son validos hasta "
                                      "la misma fecha que la T.O")
        if self.valid_to and self.valid_from:
            if self.valid_to < self.valid_from:
                self.raise_user_error('La fecha de Finalizacion debe ser mayor que la fecha inicial')
            if self.valid_to <= self.contract_date:
                self.raise_user_error("El fin de la vigencia del contrato "
                "debe ser superior a la fecha del contrato")
                
    def check_valid_from(self):
        if not self.in_operation_card_tramit:
            if self.type == 'traspaso':
                if self.transit_registration.expedition_date != self.valid_from:
                    self.raise_user_error("Para traspasos la vigencia desde "
                                          "debe ser igual a la de la "
                                          "expedición de la lic. de Transito")
            elif self.type == 'renovacion tarjeta de operacion':
                if self.operation_card.valid_from != self.valid_from:
                    self.raise_user_error("Para renovación de T.O la vigencia "
                                          "desde debe ser igual a la de la "
                                          "T.O")

                
    
    def check_overlap_contract(self):
        """Verifica si existe contrato sobrelapados
        
        verifica las fechas valid_from y valid_to
        """
        if self.valid_to and self.valid_from:
            overlap_contracts = self.search(['AND',
                ['OR',
                    [('valid_to','>=',self.valid_from),('valid_to','<=',self.valid_to)],
                    [('valid_from','>=',self.valid_from),('valid_from','<=',self.valid_to)],
                    [('valid_to','<=',self.valid_to),('valid_from','>=',self.valid_from)]
                ],
                ['OR',
                 [('internal_letter','=',self.internal_letter),('internal_number','=',self.internal_number)],
                 [('vehicle','=',self.vehicle)]
                ],
                [('id','!=',self.id),]
                ]
            )
            if len(overlap_contracts) != 0:
                self.raise_user_error("Existe otros contratos entre la fecha "
                                      "inicial y final") 
    
    def _previous_contracts_with_transit_registration(self):
        """Devuelve los contratos anteriores con la misma tarjeta de operación"""
        
        return self.search([
            ('transit_registration','=',self.transit_registration),
            ('id','!=',self.id),
            ('valid_to','<=',self.valid_from)
            ])
    
    
    
    def _previous_contracts_with_operation_card(self):
        """Devuelve los contratos anteriores con la misma tarjeta de operación"""
        
        return self.search(
            [('operation_card','=',self.operation_card),
             ('id','!=',self.id),
             ('valid_to','<=',self.valid_from),
             ])
    
    def check_in_operation_card_tramit(self):
        if self.in_operation_card_tramit:
            if self.valid_to or self.valid_from:
                self.raise_user_error("Un contrato en tramite de tarjeta de "
                                  "operacion no debe tener vigencia definida")
            if self.operation_card:
                self.raise_user_error("Un contrato en tramite de tarjeta de "
                                  "operacion no debe tener tarjeta de operacoin")
        else:
            if not (self.valid_to or self.valid_from):
                self.raise_user_error("Debe tener vigencia definida pues no "
                                      "esta en tramite de tajeta de operacion")
            if not self.operation_card:
                self.raise_user_error("Debe tener tarjeta de operacion pues no "
                                      "esta en tramite de tajeta de operacion")
        
    
    def check_main_propietary(self):
        """Valida que halla un, y solo un, propietario principal"""
        main_proprietaries = 0
        for proprietary in self.proprietaries:
            if proprietary.main:
                main_proprietaries += 1
        if main_proprietaries != 1:
            self.raise_user_error("Debe haber un, y solo un, propietario principal")
    
    def exist_vigent_contract(self):
        """@deprecated Verifica si el vehiculo y el numero interno tiene contratos vigentes
        
        @deprecated: porque pueden haber 2 contratos para un vehículo y lo que
         no se pueden superponer es las fechas de vigencia
        """
        vigent_contract = self.search([
            ('end_date','=',None),
            ('id','!=',self.id),
            ['OR',
             [
                 ('internal_letter','=',self.internal_letter),
                 ('internal_number','=',self.internal_number),
             ],
             ('vehicle','=',self.vehicle)  
             ]
            ]
        )
        if len(vigent_contract) != 0:
            return True
        else:
            return False
    
        
    
    
class ProprietaryContractDriver(ModelSQL,ModelView):
    """Relaciona los Conductores (empleados tipo conductores) de los contratos de propietarios"""
    
    __name__ = 'public_transport.proprietary_contract_driver'
    _order = [('code','DESC')]
    
    contract = fields.Many2One('public_transport.proprietary_contract','Contrato',
                       required=True,
                       help="Contrato de Vehiculo al que esta vinculado el conductor",
                       #domain=[('end_date','=',None)],
                       ondelete='RESTRICT'
                       )
    driver = fields.Many2One('company.employee','Conductor',
                                    required=True,
#                                     domain=[('is_driver','=',True)],
                                    help='Conductor vinculado')
    temp_link = fields.Boolean('Vinculo temporal')
    init_date = fields.Date('Fecha de inicio',
                            states={'required':Bool(Eval('temp_link')),
                                   'invisible':Not(Bool(Eval('temp_link')))
                                   },
                            help="Solo cuando se trata de conductores temporales"
                            )
    end_date = fields.Date('Fecha de finalizacion',
                            states={'required':Bool(Eval('temp_link')),
                                   'invisible':Not(Bool(Eval('temp_link')))
                                   },
                            help="Solo cuando se trata de conductores temporales"
                            )
    note = fields.Char('Observaciones')
    
