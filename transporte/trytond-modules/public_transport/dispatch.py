# This file is part of fleet.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
from datetime import datetime, timedelta, date
import pytz
from decimal import Decimal
import json

from trytond.modules.datos_sonar.sonar import SonarMixin

from trytond.model import ModelSQL, ModelView, fields, Workflow, dualmethod, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Not, Bool, Or, In, Get, PYSONEncoder, And, If
from trytond.wizard.wizard import Wizard, StateView, Button, StateAction, \
    StateTransition, StateReport
from trytond.rpc import RPC
from trytond.modules.datos_sonar import datos_sonar_lib
from trytond.transaction import Transaction

# from trytond.wizard import Wizard, StateView, Button, StateTransition
# from trytond.transaction import Transaction

logger = logging.getLogger(__name__)
__all__ = ['Dispatch', 'FormExecDispatch', 'FormProgramedDispatch',
           'FormFinalizeDispatch', 'ExecDispatch', 'ExecProgramedDispatch',
           'ExecFinalizeDispatch', 'DispatchContextFilter',
           'ExecCancelDispatch', 'FormCancelDispatch',
           'OpenEventsFromDispatch', 'ChangeDispatchForm',
           'ChangeDispatch', 'SelectChangeDispatchForm', 'DispatchAnnotation',
           'AnnotationDispatch', 'OpenGeoPassengerFromDispatch',
           'DispatchShowMap']

STATES = [
        ('draft', 'Borrador'),
        ('dispatched', 'Despachado'),
        ('done', 'Finalizado'),
        ('canceled', 'Cancelado'),
        ]
_STATES_DRAFT = {
    'readonly': Eval('state') != 'draft',
    }


def colombia_date2utctime(date_):
    """Devuelve el tiempo inicial y final en utc para una fecha colombiana"""
    init_time = datetime.fromordinal(date_.toordinal()) + timedelta(hours=5)
    end_time = init_time + timedelta(hours=24) - timedelta(seconds=1)
    return init_time, end_time


def time2bogota(datetime_):
    """devuelve un datetime convertido a colombia"""
    # tz = pytz.timezone("America/Bogota")
    # utc_tz = pytz.timezone("UTC")
    return datetime_ - timedelta(hours=5)


class Dispatch(SonarMixin, Workflow, ModelSQL, ModelView):
    """Despachos: recorrido de vehiculos en las rutas
    requiere datos_sonar y el modulo zeep
    """

    __name__ = 'public_transport.dispatch'

    group_route = fields.Many2One(
        'public_transport.group_route', 'Ruta',
        domain=[()],
        states=_STATES_DRAFT,
        help="Grupo de rutas a la que pertenece la ruta",
        required=True)
    route = fields.Many2One('public_transport.route', 'SubRuta',
                            domain=[('active', '=', True),
                                    ('group', '=', Eval('group_route', -1))],
                            states=_STATES_DRAFT,
                            required=False)
    turn = fields.Integer('Turno_Ficho',
                          states=_STATES_DRAFT,)
    turn_route = fields.Many2One(
        'public_transport.route', 'Ruta de Turno',
        domain=[('active', '=', True),
                ('group', '=', Eval('group_route', -1))],
        help="Indica la ruta inicial del Turno de este despacho",
                    )
    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              ondelete='RESTRICT',
                              states={
                                    'readonly': Eval('state') != 'draft',
                                    'required': Eval('state') != 'draft'
                                    },
                              domain=[('tpp', '=', True),
                                      ('active', '=', True),
                                      ('routes', '=', Eval('group_route', -1)),
                                      # ('sonar_mobile.group','=',Eval('route.sonar_itinerary.It_Idgroup',-1))
                                      ]
                              )

    driver = fields.Many2One(
        'company.employee', 'Conductor',
        states=_STATES_DRAFT,
        domain=[('is_driver', '=', True), ('end_date', '=', None)]
        )  # @todo se debe organizar para poder buscar por cedula o nombre
    init_time = fields.DateTime('Hora de inicio',
                                states=_STATES_DRAFT,)
    init_time_co = fields.Function(fields.DateTime('Hora de Inicio colombia'),
                                   'time_co')
    end_time = fields.DateTime(
        "Hora de finalizacion",
        domain=['OR',
                ('end_time', '>=', Eval('init_time')),
                ('end_time', '=', None)
                ],
        states={
            'readonly': Eval('state').in_(['draft', 'canceled', 'done']),
            'invisible': Eval('state').in_(['draft']),
            'required': Bool(Eval('end_counter'))
            }
                       )
    end_time_co = fields.Function(fields.DateTime('Hora de fin colombia'),
                                  'time_co')
    init_counter = fields.Many2One(
        'fleet.vehicle.passenger_counter',
        'Registradora Inicial',
        domain=[('vehicle', '=', Eval('vehicle', -1))],
        states=_STATES_DRAFT
        )
    end_counter = fields.Many2One(
        'fleet.vehicle.passenger_counter', 'Registradora Final',
        domain=[('vehicle', '=', Eval('vehicle', -1)), ],
        states={
          # verificar si se requiere modificar registradora final
          # de un despacho cancelado
          'readonly': Eval('state').in_(['draft', 'done', 'canceled']),
          'invisible': Eval('state') == 'draft',
          }
                      )
    passangers = fields.Function(
        fields.Integer('Pasajeros',
                       states={
                            'invisible': Bool(~Eval('end_counter')),
                                            }), 'on_change_with_passangers')
    sonar_id = fields.BigInteger(
        'Sonar id',
        help="Identificador del itineraryHistory de Sonar.\n"
        "Indica que se ha (pre)despachado en Sonar.",
        readonly=True)
    sonar_cancelled = fields.Boolean(
        'Cancelado en Sonar',
        readonly=True,
        help="Indica si se canceló en sonar. \n"
        "Si un despacho tiene Sonar id, está en "
        "estado cancelado y no tiene este campo "
        "activo indica que no se pudo cancelar en "
        "Sonar.",
        states={'invisible': Not(Bool(Eval('sonar_id', -1)))})
    itinerary_history = fields.Function(
        fields.Many2One('datos_sonar.itinerary_history', 'ItinerarioHistorico',
                        states={'invisible': Not(Bool(Eval('sonar_id', -1)))}),
        'on_change_with_itinerary_history'
           )
    issue_counter = fields.Many2One(
        'fleet.vehicle.passenger_counter', 'Novedad en la registradora',
        domain=[('vehicle', '=', Eval('vehicle', -1))],
        states={'readonly': Eval('state').in_(['canceled', 'done'])}
       )
    issue_register_note = fields.Char(
        'Motivo Novedad registradora',
        states={'readonly': Eval('state').in_(['canceled', 'done'])}
                              )
    canceled_cause = fields.Char(
        'Motivo de cancelacion',
        states={'invisible': Eval('state') == 'draft',
                'readonly': Eval('state').in_(['canceled', 'done'])
                },
                         )
    notes = fields.Text('Observaciones')
    state = fields.Selection(STATES, 'Estado',
                             readonly=True)

    sonar_time_first_geofence = fields.DateTime('Hora primera Geocerca')
    sonar_time_first_geofence_co = fields.Function(
        fields.DateTime('hora primera geocerca colombia'), 'time_co')
    sonar_time_last_geofence = fields.DateTime('Hora ultima Geocerca')
    sonar_time_last_geofence_co = fields.Function(
        fields.DateTime('hora ultima geocerca colombia'), 'time_co')
    sonar_done_geofences = fields.Integer('Geocercas Realizadas')  # @todo usar fields.Dict
    sonar_ignored_geofences = fields.Integer('Geocercas no realizadas')  # @todo usar fields function count(sonar_name_ignored_geofences
    sonar_name_ignored_geofences = fields.Text('Nombre geocercas no realizadas')  # ŧodo usar fields.Dict
    sonar_porcentage_points = fields.Numeric(
        '% Cumplimiento',
        digits=(1, 2),
        help="Porcentage de cumplimiento segun geocercas sonar"
        )
    sonar_speed_excesses = fields.Integer('Excesos de velocidad')
    sonar_speed_limit = fields.Integer('Limite velocidad km/h')
    sonar_need_reload_data = fields.Boolean(
        'Se requiere recargar informacion Sonar')
    sonar_passangers_in = fields.Integer("Ingresos Pasajeros")
    sonar_passangers_out = fields.Integer("Salidas Pasajeros")
    sonar_passangers_block = fields.Integer("Bloqueos Pasajeros")
    planed_dispatch = fields.One2One(
        'public_transport.group_route.planed_dispatch_dispatch',
        'dispatch', 'planed_dispatch', 'Despacho Planeado')
    dispatched_time = fields.DateTime(
        'Hora Despachado',
        readonly=True,
        help="Hora en que se cambio de estado a despachado")
    dispatched_uid = fields.Many2One('res.user', 'Despachó', readonly=True)
    done_time = fields.DateTime(
        'Hora Cancelado',
        readonly=True,
        help="Hora en que se cambio de estado a finalizado")
    done_uid = fields.Many2One('res.user', 'finalizó', readonly=True)
    canceled_time = fields.DateTime(
        'Hora Cancelado',
        readonly=True,
        help="Hora en que se cambio de estado a cancelado")
    canceled_uid = fields.Many2One('res.user', 'Canceló', readonly=True)
    subtotal_from = fields.Many2One(
        'public_transport.dispatch.quantity_for_day', 'Subtotal de',
        domain=[('vehicle', '=', Eval('vehicle')),
                ('group_route', '=', Eval('group_route'))]  # todo add restriction datetime date
        )
    annotations = fields.Many2Many(
        'public_transport.dispatch.annotation_dispatch',
        'dispatch', 'annotation', 'Anotaciones')

    @staticmethod
    def default_sonar_need_reload_data():
        return True

    @fields.depends('group_route', 'vehicle')
    def on_change_route(self):
        if self.vehicle:
            self.vehicle = None
            self.on_change_vehicle()
#         if self.route:
#             if self.route.sonar_itinerary:
#                 if self.route.It_Idgroup:
#                      self.raise_user_error(
#                          "route: {}".format(
#                              dir(self.route.sonar_itinerary.It_Idgroup.id)))
#         if hasattr(self.route, 'sonar_itinerary'):
#             self.raise_user_error(
#                 "route_sonar_itinerary: {}".format(
#                     dir(self.route.sonar_itinerary)))
#         if hasattr(self.route.sonar_itinerary, 'It_Idgroup'):
#             self.raise_user_error(
#                 "group route: {}".format(dir(
#                     self.route.sonar_itinerary.It_Idgroup))
#                             )

    @fields.depends('vehicle', 'route')
    def on_change_group_route(self):
        if self.route:
            self.route = None
        if self.vehicle:
            self.vehicle = None
            self.on_change_vehicle()

    @fields.depends('init_counter', 'end_counter', 'issue_counter', 'driver')
    def on_change_vehicle(self):
        if self.init_counter:
            if self.vehicle:
                self.init_counter = self.on_change_with_init_counter()
            else:
                self.init_counter = None
        if self.end_counter:
            self.end_counter = None
        if self.driver:
            self.driver = None
        if self.issue_counter:
            self.issue_counter = None

    @staticmethod
    def default_state():
        return('draft')

    @fields.depends('sonar_id')
    def on_change_with_itinerary_history(self, name=None):
        ItineraryHistory = Pool().get('datos_sonar.itinerary_history')
        if not self.sonar_id:
            return
        rec = ItineraryHistory.search([('regId', '=', self.sonar_id)])
        if rec:
            return rec[0].id
        else:
            return

    def time_co(self, name=None):
        """Devuelve el tiempo en hora colombiana, el tiempo es tomada del
        nombre del campo (name) quitandole _co
        """
        name = name.replace("_co", "")
        if getattr(self, name, None):
            return time2bogota(getattr(self, name))
        else:
            return None

    def pre_validate(self):
        super(Dispatch, self).pre_validate()
        self.check_end_counter()

    def check_end_counter(self):
        if self.end_counter:
            if not self.end_time:
                self.raise_user_error(
                    "Debe ingresar la hora de finalizacion del viaje, "
                    "puesto que ingresó la registradora final.")
            if self.init_counter:
                if (self.init_counter != self.end_counter) and \
                   (self.end_counter.time <= self.init_counter.time):
                    self.raise_user_error(
                        "Error: La contadora final tiene una "
                        "fecha anterior a la contadora inicial")
            if self.init_counter.diff_counter(self.end_counter) > \
               self.route.max_passangers_for_lap:
                self.raise_user_error(
                    "Error: demasiados pasajeros ({}) para un viaje "
                    "en esta ruta".format(self.init_counter.diff_counter(
                        self.end_counter)))

    @classmethod
    def __setup__(cls):
        super(Dispatch, cls).__setup__()
        cls._order = [('init_time', 'DESC'), ('end_time', 'DESC')]
        cls._buttons.update({
                'despachar': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'help': "No se tiene en cuenta la hora inicial indicada.\n"
                            "Intenta despachar en Sonar",
                    'icon': 'tryton-forward'
                    },
                'cancelar': {
                    'invisible': ~Eval('state').in_(['dispatched']),
                    'help': "También intenta cancelarlo en Sonar.\n"
                            "Requiere hora de finalizacion y motivo de cancelación",
                    },
                'finalizar': {
                    'invisible': ~Eval('state').in_(['dispatched']),
                    'help': "Requiere registradora final y hora de finalizacion",
                    'icon': 'tryton-go-previous',
                    },
                'exec_programed_dispatch': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'help': "Realiza despacho programado",
                    'icon': 'tryton-forward'
                    },
                'exec_finalize_dispatch': {
                    'invisible': ~Eval('state').in_(['dispatched']),
                    'help': "Realiza finaliza un despacho en recorrido",
                    'icon': 'tryton-forward'
                    },
                'exec_cancel_dispatch': {
                    'invisible': ~Eval('state').in_(['dispatched']),
                    'help': "Cancela un despacho en recorrido",
                    'icon': 'tryton-forward'
                    },
                'fill_sonar_data': {
                    'invisible': Not(And(Eval('state').in_(['done']),
                                         Bool(Eval('sonar_need_reload_data')) == True)),
                    'help': "Busca y almacena información Sonar del Despacho",
                    'icon': 'tryton-sonar'
                    },
                })
        cls._transitions |= set((
                ('draft', 'dispatched'),
                ('dispatched', 'done'),
                ('dispatched', 'canceled'),
                ))

    @classmethod
    @ModelView.button
    @Workflow.transition('dispatched')
    def despachar(cls, dispatches):
        for dispatch in dispatches:
            dispatch.check_for_despachar()
            if not dispatch.init_counter:
                cls.raise_user_error(
                    "La registradora inical es obligatoria "
                    "para despachar (id: {d})".format(d=dispatch.id))
            try:
                regid = dispatch.despachar_sonar()  # @todo insertar try para que no falle por sonar
                if regid:
                    cls.write([dispatch], {'sonar_id': regid})
            except Exception:
                logger.info(
                    "Fallo despachar en sonar el despacho id: {}".format(
                        dispatch.id))
            if not dispatch.dispatched_time:
                dispatch.dispatched_time = datetime.today()
            if not dispatch.dispatched_uid:
                dispatch.dispatched_uid = Transaction().user

    def set_sonar_id(self, regid):
        """almacena el sonar_id"""
        if regid and (self.sonar_id != regid):
            self.write([self], {'sonar_id': regid})

    def set_init_today(self):
        """pone el init_time a datetime.today()"""
        self.write([self], {'init_time': datetime.today()})

    def despachar_sonar(self, log_=True):
        """ejecuta despacho en Sonar y devuelve el regId
        Si ocurre algún error devuelve false.
        requiere datos_sonar y el modulo zeep.
        """
        Mobiles = Pool().get('datos_sonar.mobiles')
        Configuration = Pool().get("datos_sonar.configurations")

        configuration = Configuration.get_singleton()
        if not self.init_time:
            self.set_init_today()
        if self.vehicle.sonar_id and self.route.sonar_id:
            params = {
                'mId': self.vehicle.sonar_id,
                'Itinerary': self.route.sonar_id,
                'DrvId': self.driver.id_sonar if
                hasattr(self.driver, 'id_sonar') else '',
                'UTC_datetime': self.init_time or ''
            }
            mobil = Mobiles.get_ids_auth(mId=self.vehicle.sonar_id)
            response = self.request(configuration.wsdl_auth,
                                    request='SET_ItAssign_v2',
                                    params=params,
                                    auth=mobil['sonar_auth'],
                                    url=configuration.wsdl_url)
            if response.status == 'OK':
                if log_:
                    logger.info("response SET_ItAssign_v2 ({p}): {r}".format(
                        p=params, r=response))
                return response.regId
            else:
                if log_:
                    logger.info(
                        "Error al asignar itinerario SET_ItAssign_v2 "
                        "con los parametros {p}, el error fue: {e}".format(
                            p=params, e=response.description))
                return None
        else:
            if log_:
                logger.info('Faltan datos para poder realizar el despacho')
            return False

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def finalizar(cls, dispatches):
        for dispatch in dispatches:
            if not dispatch.end_counter:
                cls.raise_user_error("Error: Para finalizar se requiere "
                                     "registradora final (id: {d})".format(
                                         d=dispatch.id))
            dispatch.check_end_counter()
            if not dispatch.done_time:
                dispatch.done_time = datetime.today()
            if not dispatch.done_uid:
                dispatch.done_uid = Transaction().user

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancelar(cls, dispatches):
        for dispatch in dispatches:
            if not dispatch.canceled_cause:
                cls.raise_user_error(
                    "Debe ingresar el motivo de la cancelación "
                    "del despacho antes de cancelarlo")
            if dispatch.sonar_id:
                try:
                    if dispatch.cancelar_sonar(log=True):
                        cls.write([dispatch], {'sonar_cancelled': True})
                except Exception:
                    logger.info(
                        "Fallo cancelar en sonar el despacho id: {}".format(
                            dispatch.id))

            if not dispatch.canceled_time:
                dispatch.canceled_time = datetime.today()
            if not dispatch.canceled_uid:
                dispatch.canceled_uid = Transaction().user

    def cancelar_sonar(self, log=False):
        """cancela un despacho en la plataforam de Sonar"""
        Mobiles = Pool().get('datos_sonar.mobiles')
        Configuration = Pool().get("datos_sonar.configurations")

        configuration = Configuration.get_singleton()

        if not (self.sonar_id and self.vehicle.sonar_id and
                self.route.sonar_id):
            if log:
                logger.info("error: no se puede cancelar el itinerario datos"
                            "insuficientes o errones: {}".format(self))
            return False

        mobil = Mobiles.get_ids_auth(mId=self.vehicle.sonar_id)
        params = {
            'mId': self.vehicle.sonar_id,
            'regId': self.sonar_id,
            'comments': "Cancelado User: T",
            }
        response = self.request(configuration.wsdl_auth,
                                request='SET_ItCancel',
                                params=params,
                                auth=mobil['sonar_auth'],
                                url=configuration.wsdl_url)
        if response.status == self._sonar_correct_status:
            if log:
                logger.info("response SET_ItCancel ({p}): {r}".format(
                    p=params, r=response))
            return True
        else:
            if log:
                logger.info("Error response SET_ItCancel ({p}): {r}".format(
                    p=params, r=response))
            return False

    def check_for_despachar(self):
        if not self.route:
            self.raise_user_error("Error: Para despachar se requiere "
                                  "ruta (id: {d})".format(d=self.id))
        if not self.vehicle:
            self.raise_user_error("Error: Para despachar se requiere "
                                  "vehiculo (id: {d})".format(d=self.id))
        if not self.init_time:
            self.raise_user_error("Error: Para despachar se requiere "
                                  "Hora inicial (id: {d})".format(d=self.id))

    def check_for_cancelar(self):
        pass

    def check_for_finalizar(self):
        pass

    @fields.depends('end_counter', 'init_counter')
    def on_change_with_passangers(self, name=None):
        if self.init_counter and self.end_counter:
            return self.init_counter.diff_counter(self.end_counter)
        else:
            return 0

    @staticmethod
    def default_init_time():
        return datetime.today()

    @fields.depends('vehicle', 'issue_counter')
    def on_change_with_init_counter(self, name=None):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        if self.vehicle:
            last = Counter.search([('vehicle', '=', self.vehicle.id)],
                                  limit=1,
                                  order=[('time', 'DESC')])
            if last:
                return last[0].id

    @fields.depends('vehicle')
    def on_change_with_issue_counter(self):
        return

    @classmethod
    @ModelView.button_action('public_transport.execute_programed_dispatch_wizard')
    def exec_programed_dispatch(cls, dispatches):
        pass

    @classmethod
    @ModelView.button_action('public_transport.execute_finalize_dispatch_wizard')
    def exec_finalize_dispatch(cls, dispatches):
        pass

    @classmethod
    @ModelView.button_action('public_transport.execute_cancel_dispatch_wizard')
    def exec_cancel_dispatch(cls, dispatches):
        pass

    @classmethod
    def get_quantity_for_day_for_vehicle(cls, date_, vehicle_id, route_ids=[]):
        """Devuelve la cantidad de despachos finalizados por dia por vehiculo
        """
        init_time, end_time = datos_sonar_lib.colombia_date2utctime(date_)
        domain_ = [('vehicle', '=', vehicle_id),
                   ('init_time', '>=', init_time),
                   ('end_time', '<=', end_time),
                   ('state', '=', 'done')
                   ]
        if route_ids:
            domain_.append([('route', 'in', route_ids)])
        return cls.search(domain_, count=True)

    @dualmethod
    @ModelView.button
    def fill_sonar_data(cls, dispatches):
        """Consulta y llena información del despacho relacionada con Sonar"""
        EventLocation = Pool().get('datos_sonar.event_location')
        GeoPassengerLog = Pool().get('datos_sonar.geo_passenger_log')
        for dispatch in dispatches:
            if dispatch.route.sonar_itinerary \
               and getattr(dispatch.vehicle, 'sonar_mobile', None) \
               and dispatch.init_time and dispatch.end_time:
                itinerary_id = dispatch.route.sonar_itinerary.id
                # itinerary = Itinerary.browse()
                dispatch.sonar_time_first_geofence, \
                    dispatch.sonar_time_last_geofence, \
                    dispatch.sonar_done_geofences, \
                    dispatch.sonar_ignored_geofences, \
                    dispatch.sonar_name_ignored_geofences, \
                    dispatch.sonar_porcentage_points = \
                    EventLocation.get_dispatch_info(
                        dispatch.init_time,
                        dispatch.end_time,
                        dispatch.vehicle.sonar_mobile.id,
                        itinerary_id)
                dispatch.sonar_need_reload_data = False
                dispatch.sonar_speed_limit = 60  # @todo meter en configuracion
                dispatch.sonar_speed_excesses = \
                    EventLocation.get_velocity_limit_exceded(
                        dispatch.init_time,
                        dispatch.end_time,
                        dispatch.vehicle.sonar_mobile.id,
                        dispatch.sonar_speed_limit,
                        True)
                dispatch.sonar_passangers_in, dispatch.sonar_passangers_out, \
                    dispatch.sonar_passangers_block = \
                    GeoPassengerLog.get_total_passenger_for_time(
                        dispatch.init_time,
                        dispatch.end_time,
                        dispatch.vehicle.sonar_mobile.id,
                        )
        cls.save(dispatches)


class FormExecDispatch(ModelView):
    """Vista para realizar un despacho no programado
    
    """
    __name__ = 'public_transport.dispatch.exec.form'
    
    with_context = fields.Boolean('Con contexto',
                                  readonly=True,
                                  states={'invisible':True}
                                  )
    group_route = fields.Many2One('public_transport.group_route', 'Ruta',
                            domain=[('active', '=', True),
                                    ('id', '=', Get(Eval('context', {}), 'group_route'))],
                            readonly=True,
                            # states = {'readonly':Bool(Eval('with_context'))},
                            help="Grupo de rutas a la que pertenece la ruta",
                            required=True)
    route = fields.Many2One('public_transport.route', 'SubRuta',
                            domain=[('active', '=', True),
                                    ('group', '=', Eval('group_route', -1))],
                            required=True)
    init_time = fields.DateTime('hora')
    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              required=True,
                              domain=[('tpp', '=', True),
                                      ('active', '=', True),
                                      ('routes', '=', Eval('group_route', -1)),
                                      ]
                              )
    init_counter = fields.Many2One('fleet.vehicle.passenger_counter', 'Registradora Inicial',
                                   domain=[('vehicle', '=', Eval('dispatch.vehicle', -1))],
                                   readonly=True,
                                   required=True,
                                   
                                   )
    turn = fields.Integer('Turno_Ficho',
                          )
    driver = fields.Many2One('company.employee', 'Conductor',
                         domain=[('is_driver', '=', True), ('end_date', '=', None)]
                         )  # @todo se debe organizar para poder buscar por cedula o nombre
    
    @staticmethod
    def default_init_time():
        return datetime.today()
    
    @fields.depends('init_counter', 'vehicle')
    def on_change_with_init_counter(self):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        if self.vehicle:
            return Counter.get_last(self.vehicle).id


class ExecDispatch(Wizard):
    """Realiza un despacho sin estar programado
    
    1.Pide el vehículo sugiriendo por defecto el vehiculo seleccionado
    2.Evalua en el contexto la ruta sino la solicita
    3.Pide los valores necearios como registradora inicial
    4.Crea el registro si no se selecciona un borrador
    5.pasa el registro de borrador a despachado
    """
    
    __name__ = 'public_transport.dispatch.exec'
    
    start_state = 'set_info'
    
    set_info = StateView('public_transport.dispatch.exec.form',
        '', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Despachar', 'dispatch', 'tryton-forward'),
            ])
    dispatch = StateTransition()

    @classmethod
    def __setup__(cls):
        super(ExecDispatch, cls).__setup__()
        cls.__rpc__.update({
            'rpc_dispatch': RPC(readonly=False)
        })
    
    def default_set_info(self, fields): 
        return {
            'with_context':False,  # @todo establecer la ruta de acuerdo a la configuracion del usuario
            'group_route':Transaction().context['group_route']
            }
    
    def transition_dispatch(self):
        self.rpc_dispatch(self.set_info)
        return('end')

    @classmethod
    def rpc_dispatch(cls, params):
        """
        Exportamos metodo para el acceso remoto, ya que `transition_`, no permite
        retornar valores.
        """
        _fields = ['group_route', 'route', 'init_time', 'init_time', 'vehicle',
                   'turn', 'driver', 'init_counter']
        dic = {field:getattr(params, field, None) for field in _fields}
        record = Dispatch.create([dic])[0]
        record.despachar([record])
        Dispatch.save([record])
        return record.id


class FormProgramedDispatch(ModelView):
    """Vista para solicitar vehiculo, ruta, subruta
    
    """
    __name__ = 'public_transport.dispatch.exec_programed.form'
    
    dispatch = fields.Many2One('public_transport.dispatch', 'Despacho Programado',
                               readonly=True
                               )
    init_time = fields.DateTime('Hora')
    driver = fields.Many2One('company.employee', 'Conductor',
                         domain=[('is_driver', '=', True), ('end_date', '=', None)]
                         )  # @todo se debe organizar para poder buscar por cedula o nombre
    init_counter = fields.Many2One('fleet.vehicle.passenger_counter', 'Registradora Inicial',
                                   domain=[('vehicle', '=', Eval('dispatch.vehicle', -1))],
                                   readonly=True
                                   )
    group_route = fields.Function(fields.Integer('Grupo de Ruta', states={'invisible':True}), 'get_g_route')
    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              required=True,
                              domain=[('tpp', '=', True),
                                      ('active', '=', True),
                                      ('routes', '=', Eval('group_route', -1)),
                                      ],
                              depends=['group_route']
                              )
    
    @fields.depends('vehicle')
    def on_change_with_init_counter(self, name=None):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        if self.vehicle:
            return Counter.get_last(self.vehicle).id
        else:
            return None


class ExecProgramedDispatch(Wizard):
    """Realiza un despacho que estaba programado
    
    1.Pide los valores necesarios como registradora inicial
      y hora de salida opcional
    3.pasa el registro de borrador a despachado
    """
    __name__ = 'public_transport.dispatch.exec_programed'
    
    start = StateTransition() 
    set_info = StateView('public_transport.dispatch.exec_programed.form',
              '', [Button('Cancelar', 'end', 'tryton-cancel'),
                  Button('Despachar', 'dispatch', 'tryton-forward'),
                  ]
              )
    dispatch = StateTransition();
    
    def transition_start(self):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        if dispatch.state == 'draft':
            return 'set_info'
        else:
            Dispatch.raise_user_error("El despacho no se puede "
                                      "despachar por que esta en el "
                                      "estado {}".format(dispatch.state))
            return 'end'
        
    def default_set_info(self, fields):
        Dispatch = Pool().get('public_transport.dispatch')
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0] 
        return {
            'dispatch':getattr(dispatch, 'id'),
            'init_time':getattr(dispatch, 'init_time', datetime.today()),
            'driver':getattr(dispatch.driver, 'id', None),
            'init_counter': Counter.get_last(dispatch.vehicle).id if dispatch.vehicle else None,
            'vehicle':getattr(getattr(dispatch, 'vehicle', None), 'id', None),
            'group_route':getattr(getattr(dispatch, 'group_route', None), 'id', None),
            }
    
    def transition_dispatch(self):
        dispatch = self.set_info.dispatch
        dispatch.init_time = getattr(self.set_info, 'init_time', dispatch.init_time)
        dispatch.driver = getattr(self.set_info, 'driver', dispatch.driver)
        dispatch.vehicle = getattr(self.set_info, 'vehicle', None)
        dispatch.despachar([dispatch])
        return('end')


class FormFinalizeDispatch(ModelView):
    """Vista para finalizar un despacho
    
    """
    __name__ = 'public_transport.dispatch.exec_finalize.form'
    
    dispatch = fields.Many2One('public_transport.dispatch', 'Despacho Programado',
                               readonly=True
                               )
    end_time = fields.DateTime('Hora',
                               readonly=True,
                               required=True)
    end_counter = fields.Integer('Contadora Final',
                                 required=True)
    passengers = fields.Function(fields.Integer('Pasajeros'), 'on_change_with_passengers')
    
    @fields.depends('end_counter', 'dispatch')
    def on_change_with_passengers(self, name=None):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        if self.dispatch:
            if self.dispatch.init_counter and self.end_counter:
                return Counter._substract(self.dispatch.init_counter.counter, self.end_counter)
        return None
    
    def pre_validate(self):
        super(FormFinalizeDispatch, self).pre_validate()
        self.check_passangers()
        self.check_end_time()
        
    def check_passangers(self):
        if not isinstance(self.passangers, int):
            self.raise_user_error("Error en la cantidad de pasajeros: "
                                  "Revisar las registradoras")
        if self.passengers > self.dispatch.route.max_passangers_for_lap:
            self.raise_user_error("La cantidad de pasajeros supera el limite "
                                  "de la ruta: Revisar las registradoras")
        if not self.passengers < 0 :
            self.raise_user_error("La cantida de pasajeros no puede ser "
                                  "negativa: Revisar las registradoras")
            
    def check_end_time(self):
        if self.end_time < self.dispatch.init_time:
            self.raise_user_error("La fecha de finalización no debe ser "
                                  "anterior a la fecha de inicio del despacho")

    
class ExecFinalizeDispatch(Wizard):
    """Finaliza un despacho
    
    1.Pide sleccionar el despacho de los despachos iniciados del día.
    2.Pide los valores necearios como registradora final
    3.almacena los datos necesarios como fecha final que es tomada de
      la hora de finalización de sonar si existe sino la hora en que 
      se ejecuta este asistente.
    5.pasa el registro de despachado a finalizado
    """
    
    __name__ = 'public_transport.dispatch.exec_finalize'
    
    start = StateTransition() 
    set_info = StateView('public_transport.dispatch.exec_finalize.form',
              '', [Button('Cancelar', 'end', 'tryton-cancel'),
                  Button('Finalizar', 'finalize', 'tryton-forward'),
                  ]
              )
    finalize = StateTransition();
    
    def transition_start(self):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        if dispatch.state == 'dispatched':
            return 'set_info'
        else:
            Dispatch.raise_user_error("El despacho no se puede "
                                      "finalizar por que está en el "
                                      "estado {}".format(dispatch.state))
            return 'end'
        
    def default_set_info(self, fields):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        if getattr(dispatch.itinerary_history, 'endtime', None):
            end_time = dispatch.itinerary_history.endtime
        else:
            end_time = datetime.today()
        return {
            'dispatch':Transaction().context['active_id'],
            'end_time':end_time
            }
    
    def transition_finalize(self):
        Counter = Pool().get('fleet.vehicle.passenger_counter')
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([self.set_info.dispatch.id])[0]
        dispatch.end_time = self.set_info.end_time
        if dispatch.init_counter.counter == self.set_info.end_counter:
            dispatch.end_counter = dispatch.init_counter
        else:
            counter = Counter.create([{
                'time':datetime.today(),
                'vehicle':self.set_info.dispatch.vehicle,
                'counter':self.set_info.end_counter,
                'note': 'registradora final de despacho'
                }])
            dispatch.end_counter = counter[0]
        Dispatch.finalizar([dispatch])
        Dispatch.save([dispatch])
        return('end')


class FormCancelDispatch(ModelView):
    """Vista para cancelar un despacho
    
    """
    __name__ = 'public_transport.dispatch.exec_cancel.form'
    
    dispatch = fields.Many2One('public_transport.dispatch', 'Despacho',
                               readonly=True,
                               required=True,
                               )
    canceled_cause = fields.Char('Motivo de cancelacion',
                                 required=True
                                 )

    
class ExecCancelDispatch(Wizard):
    """Cancela un despacho

    """

    __name__ = 'public_transport.dispatch.exec_cancel'
    
    start = StateTransition() 
    set_info = StateView('public_transport.dispatch.exec_cancel.form',
              '', [Button('Salir', 'end', 'tryton-cancel'),
                  Button('Cancelar Despacho', 'cancel', 'tryton-forward'),
                  ]
              )
    cancel = StateTransition()
    def transition_start(self):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        if dispatch.state == 'dispatched':
            return 'set_info'
        else:
            Dispatch.raise_user_error("El despacho no se puede "
                                      "cancelar por que está en el "
                                      "estado {}".format(dispatch.state))
            return 'end'
            
    def default_set_info(self, fields):
        return {
            'dispatch':Transaction().context['active_id'],
            }
    
    def transition_cancel(self):
        dispatch = Dispatch.browse([self.set_info.dispatch.id])[0]
        dispatch.canceled_cause = self.set_info.canceled_cause
        Dispatch.cancelar([dispatch])
        Dispatch.save([dispatch])
        return('end')

    
class DispatchContextFilter(ModelView):
    """filtro mensual"""
    
    __name__ = 'public_transport.dispatch.context_filter'
    
    context_date = fields.Date('Día',
                               required=True)
    init_date = fields.Function(fields.DateTime('Hora inicial',
                                                depends=['context_date'],
                                                states={'invisible':True},
                                                ), 'on_change_with_init_date',)
    end_date = fields.Function(fields.DateTime('Hora final',
                                                depends=['context_date'],
                                                states={'invisible':True},
                                                ), 'on_change_with_end_date',)
    group_route = fields.Many2One('public_transport.group_route', 'Grupo de Ruta',
                            domain=[('id', 'in', Eval('context', {}).get('groups_routes', -1))],
                            required=False,
                            )
    
    @staticmethod
    def default_context_date():
        return date.today()
    
    @staticmethod
    def default_group_route():
        transaction = getattr(Transaction(), 'context', {})
        if  'group_route' in transaction: 
            return transaction['group_route']
        else:
            None 
    
    @fields.depends('context_date', 'init_date')
    def on_change_with_init_date(self):
        if self.context_date:
            d = self.context_date
            return datetime(year=d.year,
                            month=d.month,
                            day=d.day) + timedelta(hours=5)  # @todo verificar si se necesitan las 5 horas 
    
    @fields.depends('context_date', 'end_date')
    def on_change_with_end_date(self):
        if self.context_date:
            d = self.context_date
            return datetime(year=d.year,
                            month=d.month,
                            day=d.day,
                            hour=23,
                            minute=59,
                            second=59) + timedelta(hours=5)  # @todo verificar si se necesitan las 5 horas

class DispatchShowMap(Wizard):
    """Muestra el mapa de un despacho"""
    __name__ = 'public_transport.dispatch.show_map'

    start = StateReport('public_transport.dispatch.to_map')

    def do_start(self, action):
        Model = Pool().get(Transaction().context['active_model'])
        Events = Pool().get('datos_sonar.event_location')
        time_adjust = timedelta(minutes=5)
        record = Model.browse([Transaction().context['active_id']])[0]
        if not (record.init_time and record.end_time and record.vehicle
                and record.route):
            Model.raise_user_error("Faltan datos para realizar el mapa")
        
        if record.route.gps_track:
            gpx_content = record.route.gps_track.decode()
        else:
            gpx_content = ''
        event_domain = [
            ('system_gmt', '>=', record.init_time - time_adjust),
            ('system_gmt', '<=', record.end_time + time_adjust),
            ('mobil_id', '=', record.vehicle.sonar_mobile.id),
        ]
        event_fields_names = [
            'latitude',
            'longitude',
            'mobil_id.rec_name',
            'system_tz',
            'speed',
            'eventDescription'
        ]
        translate_fields = {
            'latitude': 'latitud',
            'longitude': 'longitud'
        }
        event_delete_fields = ['mobil_id.rec_name',
                               'system_tz',
                               'eventDescription',
                               'speed']
        event_add = [
            {'type': 'Eventos'}
        ]
        events = Events.search_read(event_domain,
                                    fields_names=event_fields_names,
                                    order=[('system_gmt','ASC')])
        for event in events:
            #agregando datos necesarios
            for add in event_add:
                event.update(add)
            ldescripcion = []
            ldescripcion.append(event['mobil_id.rec_name'])
            ldescripcion.append(event['system_tz'].strftime('%Y-%m-%d %H:%M:%S'))
            ldescripcion.append(event['eventDescription'] + '.')
            if event['speed']:
                ldescripcion.append(str(event['speed']) + "km/h")
            descripcion = ' '.join(ldescripcion)
            for field in event_delete_fields:
                del event[field]
            event.update({'description':descripcion})
            #todo borrar campos innecesarios
            for sfield, tfield in translate_fields.items():
                if sfield in event.keys():
                    event[tfield] = event[sfield]
                    del event[sfield]
                
            
        ruta_gpx = json.dumps(gpx_content)
        eventos = json.dumps(events)
        return action, {
            'id': Transaction().context['active_id'],
            'ruta_gpx': ruta_gpx,
            'eventos': eventos
        }


class OpenEventsFromDispatch(Wizard):
    """Abre eventos en el mismo tiempo del despacho finalizado."""
    
    __name__ = 'public_transport.dispatch.open_events'
    
    start = StateAction('datos_sonar.act_event_location_without_filter_form')
    
    def do_start(self, action):
        # @todo eventDescription debe ser con los sinonimos
        Model = Pool().get(Transaction().context['active_model'])
        transaction = Transaction()
        
        record = Model.browse([transaction.context['active_id']])[0]
        if record.init_time and record.end_time and record.vehicle:
            inittime = record.init_time - timedelta(minutes=5)  # @todo llevar a configuracion
            endtime = record.end_time + timedelta(minutes=5)  # @todo llevar a configuracion
            action['pyson_domain'] = [
                ('system_gmt', '>=', inittime),
                ('system_gmt', '<=', endtime),
                ('mobil_id', '=', record.vehicle.sonar_mobile.id),
                ]
            action['name'] += " {mobil} ({date})".format(mobil=record.vehicle.sonar_mobile.rec_name,
                                                                date=inittime,
                                                                )
            action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
            return action, {}
        else:
            Model.raise_user_error("Faltan datos para poder ver los eventos:\n"
                                   "Vehiculo: {v}, inicio: {i} utc, fin:{e} utc".format(v=record.vehicle.rec_name or None,
                                                                                i=record.init_time or None,
                                                                                e=record.end_time or None))
            return 'end'


class SelectChangeDispatchForm(ModelView):
    """Formulario asistente seleccion tipo de cambio despacho, el campo
    type debe ser el nombre del campo del despacho que se quiere modificar
    """

    __name__ = 'public_transport.dispatch.change.type_form'

    type = fields.Selection([('init_counter', 'registradora_inicial'),
                             ('end_counter', 'registradora_final'),
                             ('init_time', 'hora inicio'),
                             ('end_time', 'hora finalización')
                             ], 'Tipo de cambio',
                            required=True)


class ChangeDispatchForm(ModelView):
    """
    Formulario asistente cambio registradora inicial de despacho, el campo
    type debe ser el nombre del campo del despacho que se quiere modificar,
    y el campo en este formulario se debe llamar igual al del despacho
    """

    __name__ = 'public_transport.dispatch.change_init_counter.form'

    type = fields.Selection([('init_counter', 'registradora_inicial'),
                             ('end_counter', 'registradora_final'),
                             ('init_time', 'hora inicio'),
                             ('end_time', 'hora finalización')],
                            'Tipo de cambio',
                            readonly=True,
                            required=True)
    dispatch = fields.Many2One(
        'public_transport.dispatch', 'Despacho',
        readonly=True,
        required=True)
    init_counter = fields.Many2One(
        'fleet.vehicle.passenger_counter',
        'Registradora Inicial',
        domain=[('vehicle', '=', Eval('vehicle')),
                ('time', '<=', Eval('limit_time'))
                ],
        states={'required': Bool(Eval('type').in_(['init_counter'])),
                'invisible': Not(Eval('type').in_(['init_counter'])),
                'readonly': Not(Eval('type').in_(['init_counter']))
                }
        )
    end_counter = fields.Many2One(
        'fleet.vehicle.passenger_counter',
        'Registradora Final',
        domain=[('vehicle', '=', Eval('vehicle')),
                ('time', '>=', Eval('limit_time'))
                ],
        states={'required': Bool(Eval('type').in_(['end_counter'])),
                'invisible': Not(Eval('type').in_(['end_counter'])),
                'readonly': Not(Eval('type').in_(['end_counter']))
                }
        )
    init_time = fields.DateTime(
        'Hora inicial',
        domain=[('init_time',
                 If(Eval('type') == 'init_time', '<', '!='),
                 Eval('limit_time'))],
        states={'required': Bool(Eval('type').in_(['init_time'])),
                'invisible': Not(Eval('type').in_(['init_time'])),
                'readonly': Not(Eval('type').in_(['init_time']))
                })
    end_time = fields.DateTime(
        'Hora final',
        domain=[('end_time',
                 If(Eval('type') == 'end_time', '>', '!='),
                 Eval('limit_time'))],
        states={'required': Bool(Eval('type').in_(['end_time'])),
                'invisible': Not(Eval('type').in_(['end_time'])),
                'readonly': Not(Eval('type').in_(['end_time']))
                })
    vehicle = fields.Many2One('fleet.vehicle', 'Vehiculo',
                              readonly=True)
    limit_time = fields.DateTime(
        'hora limite',
        readonly=True,
        help="El valor depende del tipo de cambio que se va a realizar:\n"
        "Registradora inicial: hora de la contadora final.\n"
        "Registradora Final: hora de la contadora inicial.\n"
        "Hora inicial: hora final del despacho"
        "Hora Final: hora inicial del despacho")

    def pre_validate(self):
        super(ChangeDispatchForm, self).pre_validate()
        if self.type == 'init_counter':
            if (self.init_counter.time == self.dispatch.end_counter.time) and \
               (self.init_counter.id != self.dispatch.end_counter.id):
                self.raise_user_error("si la registradora ingresada no es "
                                      "igual a la registradora final del "
                                      "despacho la hora debe ser menor")
        if self.type == 'end_counter':
            if (self.end_counter.time == self.dispatch.end_counter.time) and \
               (self.init_counter.id != self.dispatch.end_counter.id):
                self.raise_user_error("si la registradora ingresada no es "
                                      "igual a la registradora inicial del "
                                      "despacho la hora debe ser mayor")


class ChangeDispatch(Wizard):
    """Asistente para cambio de campos al despacho"""

    __name__ = 'public_transport.dispatch.change'

    start = StateView(
        'public_transport.dispatch.change.type_form',
        '',
        [Button('Salir', 'end', 'tryton-cancel'),
         Button('Seleccionar', 'set_info', 'tryton-forward'),
         ]
       )
    set_info = StateView(
        'public_transport.dispatch.change_init_counter.form',
        '',
        [Button('Salir', 'end', 'tryton-cancel'),
         Button('Cambiar', 'change', 'tryton-forward'),
         ]
       )
    change = StateTransition()

    def default_start(self, fields):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        if dispatch.state == 'done':
            return {}
        else:
            Dispatch.raise_user_error("el Despacho debe estar finalizado")

    def default_set_info(self, fields):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = Dispatch.browse([Transaction().context['active_id']])[0]
        defaults = {'dispatch': dispatch.id,
                    'vehicle': dispatch.vehicle.id,
                    'type': self.start.type}
        if self.start.type == 'init_counter':
            defaults.update({'limit_time': dispatch.end_counter.time})
        elif self.start.type == 'end_counter':
            defaults.update({'limit_time': dispatch.init_counter.time})
        elif self.start.type == 'init_time':
            defaults.update({'limit_time': dispatch.end_time})
        elif self.start.type == 'end_time':
            defaults.update({'limit_time': dispatch.init_time})
        return defaults

    def transition_change(self):
        Dispatch = Pool().get('public_transport.dispatch')
        dispatch = self.set_info.dispatch
        field = getattr(self.set_info, 'type')
        if getattr(self.set_info, field) != getattr(dispatch, field):
            setattr(dispatch, field, getattr(self.set_info, field))
            Dispatch.save([dispatch])
        return('end')


class AnnotationDispatch(ModelSQL, ModelView):
    """Relaciona Anotaciones y despachos"""
    __name__ = 'public_transport.dispatch.annotation_dispatch'

    dispatch = fields.Many2One('public_transport.dispatch',
                               'Despacho',
                               ondelete='RESTRICT',
                               required=True
                               )
    annotation = fields.Many2One('public_transport.dispatch.annotation',
                                 'Anotación',
                                 ondelete='RESTRICT',
                                 required=True)


class DispatchAnnotation(ModelSQL, ModelView):
    """Anotaciones para despachos"""
    __name__ = 'public_transport.dispatch.annotation'

    name = fields.Char('Nombre',
                       required=True)
    code = fields.Char('Código',
                       size=4,
                       required=True)
    description = fields.Text('Descripción')
    to_dispatcher = fields.Boolean(
        'Para despachadores',
        help="Indica si esta anotación será ingresada por los despachadores")
    to_analyse = fields.Boolean(
        'Para Analistas',
        help="Indica si esta anotación será ingresada por los analistas")

    @classmethod
    def __setup__(cls):
        super(DispatchAnnotation, cls).__setup__()
        cls._order = [('name', 'ASC')]
        table = cls.__table__()
        cls._sql_constraints += [
            ('name_key', Unique(table, table.name),
                'El nombre debe ser unico'),
            ('code_key', Unique(table, table.name),
                'El codigo debe ser unico'),
        ]


class OpenGeoPassengerFromDispatch(Wizard):
    """Abre pasajeros geolocalizados en el mismo tiempo del despacho finalizado."""
    
    __name__ = 'public_transport.dispatch.open_geo_passenger'
    
    start = StateAction('datos_sonar.act_geo_passenger_log')
    
    def do_start(self, action):
        Model = Pool().get(Transaction().context['active_model'])
        transaction = Transaction()
        
        record = Model.browse([transaction.context['active_id']])[0]
        if record.init_time and record.end_time and record.vehicle:
            action['pyson_domain'] = [
                ('system_time', '>=', record.init_time),
                ('system_time', '<=', record.end_time),
                ('mobil_id', '=', record.vehicle.sonar_mobile.id),
                ]
            action['name'] += f" {record.vehicle.sonar_mobile.rec_name}"
            action['name'] += f"({str(record.init_time)} - {str(record.end_time)} UTC)"
            action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
            return action, {}
        else:
            Model.raise_user_error("Faltan datos para poder ver los eventos:\n"
                                   f"Vehiculo: {record.vehicle.rec_name}, inicio: {record.init_time or None} utc, fin:{record.end_time or None} utc"
            )
            return 'end'
