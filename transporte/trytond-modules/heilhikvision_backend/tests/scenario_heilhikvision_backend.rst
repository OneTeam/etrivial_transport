==============================
Heilhikvision Backend Scenario
==============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install heilhikvision_backend::

    >>> config = activate_modules('heilhikvision_backend')
