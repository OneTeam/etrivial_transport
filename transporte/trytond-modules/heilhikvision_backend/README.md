# heilhikvision_backend

Interfaz a tryton

## ELIMINACION DE VIDEOS

la eliminacion se realiza en 2 etapas, la 1 es marcar los videos proximos a eliminar y la 2 eliminarlos
del sistema de archivo y base de datos, para la primera esta **heilhikvision_backend.video_download_log.update_to_delete** y para la segundo **heilhikvision_backend.video_download_log.do_delete**, la eliminacion de un video
se puede suspender solo si se marca como persistente antes de la etapa 2.
**heilhikvision_backend.video_download_log.clean_old** marca los videos para eliminacion usando
como reglas el tamaño en servidor de videos

## CRON

### actualizar tamano de ficheros

se puede programar un lanzador para actualizar tamano de ficheros

adicionar lanzador con los siguientes parametros:

* **model** : heilhikvision_backend.video_download_log
* **function** : update_server_filesize
* **args** : []

### recuperar espacio de disco

se puede programar un lanzador para marcar videos a eliminar por falta de espacio
en **Configuration** del modulo ahi 2 atributos **Persistent Size** que es el tamaño
limite del servidor de videos y **Persistent Minimum Garbage Size** que es el tamaño
a recuperar.

adicionar lanzador con los siguientes parametros:

* **model** : heilhikvision_backend.video_download_log
* **function** : clean_old
* **args** : []

### eliminar videos marcados

se puede programar un lanzador para eliminar los videos marcados

adicionar lanzador con los siguientes parametros:

* **model** : heilhikvision_backend.video_download_log
* **function** : do_delete
* **args** : []

### marcar para eliminarr por dias atras

se puede programar un lanzador para actualizar si un video se debe eliminar.

adicionar lanzador con los siguientes parametros:

* **model** : heilhikvision_backend.video_download_log
* **function** : update_to_delete
* **args** : [3] # cantidad de dias hacia atras a marcar para eliminaor


### confirmacion de descarga en servidor

se puede programar un lanzador para actualizar existencia de archivo en servidor.

adicionar lanzador con los siguientes parametros:

* **model** : heilhikvision_backend.video_download_log
* **function** : update_in_server
* **args** : ['']

## API

### POST /heilhikvision/videolog/<machine>

Se inserta un registro en la bitacora, **machine** es el nombre del servidor
que envia la informacion.

~~~POST DATA
dvrname=
starttime= //en 8601
stopttime= //en 8601
video_path= //ruta del archivo en machine
duration= //duracion en segundos del video
~~~

### GET /heilhikvision/dispatchs?date=xxxx-xx-xx

Por defecto retorna despachos de dia en curso o se puede especificar el dia
con el parametro 'date=YYYY-MM-DD'.

~~~json
[
{
 'vehicle_code': 'xxx', //rec_name en tryton
 'init_time': '', //ISO 8601,
 'end_time': '', //ISO 8601,
},
...]
~~~
