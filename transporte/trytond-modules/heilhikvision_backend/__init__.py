# This file is part of heilhikvision_backend.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


from trytond.pool import Pool

__all__ = ['register']
from .user import *
from .log import *
from .configuration import *


from trytond.report import report
# MACHETE: esto se requiere ya que sino trytond intentaria
# usar soffice para hacer conversion de formato
report.MIMETYPES['m3u'] = 'application/x-mpegURL'


def register():
    Pool.register(
        UserApplication,
        VideoDownloadLog,
        Configuration,
        ActionReportM3U,
        VideoDownloadLogHistoryConfirm,
        module='heilhikvision_backend', type_='model')
    Pool.register(
        VideoDownloadLogM3U,
        VideoDownloadLogHistory,
        VideoDownloadLogCheckRemote,
        module='heilhikvision_backend', type_='wizard')
    Pool.register(
        module='heilhikvision_backend', type_='report')

from . import routes
