# This file is part of heilhikvision_backend.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import json
from datetime import datetime, timedelta
import dateutil.parser
import logging
logger = logging.getLogger(__name__)

from trytond.wsgi import app
from werkzeug.wrappers import Response

from trytond.protocols.wrappers import with_pool, with_transaction, user_application
client_application = user_application('heilhikvision')

@app.route('/<database_name>/heilhikvision/videolog/<machine>/inserver/<page>', methods=['GET'])
@with_pool
@with_transaction()
@client_application
def videolog_inserver(request, pool, machine, page):
    offsettime = datetime.now() - timedelta(days=5)
    limit=500
    VideoDownloadLog = pool.get('heilhikvision_backend.video_download_log')
    videologs = VideoDownloadLog.search_read([('state', '=', 'in server'),
                                              ('starttime', '<=', offsettime),
                                              ('machine', '=', machine)],
                                             offset = int(page) * limit,
                                             limit = limit,
                                             fields_names = ['machine', 'remote_path'])
    data = {
        'total': len(videologs),
        'page': page,
        'records': videologs,
    }
    return Response(json.dumps(data), content_type='application/json')


@app.route('/<database_name>/heilhikvision/videolog/<machine>', methods=['POST'])
@with_pool
@with_transaction()
@client_application
def videolog(request, pool, machine):
    VideoDownloadLog = pool.get('heilhikvision_backend.video_download_log')
    VideoDownloadLog.create([{
        'machine': machine,
        'dvrname': request.form['dvrname'],
        'starttime': dateutil.parser.parse(request.form['starttime']),
        'stoptime': dateutil.parser.parse(request.form['stoptime']),
        'remote_path': request.form['video_path'],
        'duration': int(request.form['duration']),
        'ip': request.form['ip'],
        'filesize': int(request.form['filesize'] or 0),
    }])
    return Response('ok')


@app.route('/<database_name>/heilhikvision/dispatchs', methods=['GET'])
@with_pool
@with_transaction()
@client_application
def dispatchs(request, pool):
    Dispatch = pool.get('public_transport.dispatch')
    where = []
    filter_date = request.args.get('date', datetime.today())

    if isinstance(filter_date, str):
        filter_date = datetime.strptime(filter_date, '%Y-%m-%d')

    where = [('init_time', '>=', datetime.combine(filter_date, datetime.min.time())),
             ('init_time', '<=', datetime.combine(filter_date, datetime.max.time())),
             ('state', '=', 'done')]
    resp = [{
        'vehicle_id': d.vehicle.id,
        'vehicle_code': d.vehicle.internal_code or d.vehicle.licence_plate,
        'init_time': d.init_time.isoformat(),
        'end_time': d.end_time.isoformat(),
    } for d in Dispatch.search(where) if d.vehicle and d.end_time]
    return Response(json.dumps(resp), content_type='application/json')
