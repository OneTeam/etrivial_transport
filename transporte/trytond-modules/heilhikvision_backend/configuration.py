from datetime import datetime
from trytond.model import ModelSQL, ModelView, ModelSingleton, fields
from trytond.transaction import Transaction
import urllib.parse


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Configuration'

    __name__ = 'heilhikvision_backend.configuration'

    @fields.depends('url')
    def get_url_example(self, name):
        if name == 'url_persistent':
            return self.formatted_url('prueba.ogv', self.url_persistent)
        else:
            return self.formatted_url('prueba.ogv')

    url_do_check = fields.Char('URL CHECK SERVER', help='url to do check')

    url_do_persistent = fields.Char('URL DO PERSISTENT SERVER', help='url to do video persistent')
    # se envia a esta url
    # {'videologs': [{'year': ..., 'month': ..., 'day': ..., 'dvrname': ..., 'name':...}...]
    url_do_remove = fields.Char('URL DO REMOVE', help='url to delete video')

    url = fields.Char('URL SERVER VIDEO', help='Use {videoname} for templating\n'
                      'Use {year}, {month}, {day}\n'
                      'User {dvrname}')
    url_example = fields.Function(fields.Char('URL Example'),
                                  'get_url_example')


    url_persistent = fields.Char('URL SERVER HISTORY VIDEO', help='Use {videoname} for templating\n'
                      'Use {year}, {month}, {day}\n'
                      'User {dvrname}')
    url_persistent_example = fields.Function(fields.Char('URL History Example'),
                                  'get_url_example')
    
    persistent_size = fields.Integer('Persistent Size (MB)')
    persistent_minimum_garbage_size = fields.Integer('Persistent Minimum Garbage Size (MB)')

    @staticmethod
    def default_persistent_minimum_garbage_size():
        return 300

    @staticmethod
    def default_persistent_size():
        return 1000

    @staticmethod
    def default_url_do_remove():
        return 'http://localhost:8000/rmvideo.py'

    @staticmethod
    def default_url_do_persistent():
        return 'http://localhost:8000/mvideo.py'


    @staticmethod
    def default_url():
        t = Transaction()
        return 'http://localhost:8000/%s/{videoname}' % (t.database.name)

    @staticmethod
    def default_url_persistent():
        t = Transaction()
        return 'http://localhost:8000/%s/{videoname}' % (t.database.name)

    def formatted_url(self, videoname, date=None, dvrname="", url=None):
        if not date:
            date = datetime.now().date()

        if not url:
            url = self.url
        return url.format(videoname=urllib.parse.quote(videoname),
                          year=date.year,
                          month=date.month,
                          day=date.day,
                          dvrname=urllib.parse.quote(dvrname))
