import os.path
import urllib.request, urllib.error
import json
import pytz
from datetime import datetime, timedelta
import re
import logging
logger = logging.getLogger(__name__)

from trytond.wizard import Wizard, StateView, Button, StateTransition, StateReport
from trytond.model import ModelSQL, ModelView, fields, dualmethod
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from sql.aggregate import Sum

def datetime_in_timezone(dtime, timezone=None):
    # TOMADO company/company.py#Date
    pool = Pool()
    Company = pool.get('company.company')
    company_id = Transaction().context.get('company')
    if timezone is None and company_id:
        company = Company(company_id)
        if company.timezone:
            timezone = pytz.timezone(company.timezone)
            return dtime.astimezone(timezone)
    return dtime

class ActionReportM3U(metaclass=PoolMeta):
    __name__ = 'ir.action.report'
    
    @classmethod
    def __setup__(cls):
        super(ActionReportM3U, cls).__setup__()
        cls.extension.selection.append(('m3u', 'Player List'))
    

class VideoDownloadLog(ModelSQL, ModelView):
    'Bitacora de videos'
    __name__ = 'heilhikvision_backend.video_download_log'

    _states = {'readonly': True}

    @fields.depends('dvrname', 'starttime')
    def get_videoname(self, name):
        return "%s %s" % (self.dvrname, datetime_in_timezone(self.starttime).isoformat())

    @fields.depends('state', 'remote_path', 'starttime', 'dvrname')
    def get_url(self, name):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)
        if self.state == 'persistent':
            return configuration.formatted_url(os.path.basename(self.remote_path),
                                               date=datetime_in_timezone(self.starttime),
                                               dvrname=self.dvrname,
                                               url=configuration.url_persistent)
            
        else:
            return configuration.formatted_url(os.path.basename(self.remote_path),
                                               date=datetime_in_timezone(self.starttime),
                                               dvrname=self.dvrname)

    state = fields.Selection([
        ('in remote', 'In Remote'),
        ('in server', 'In Server'),
        ('to delete', 'To Delete'),
        ('persistent', 'Persistent')
    ], 'State', states=_states)

    machine = fields.Char('Machine', states=_states)
    dvrname = fields.Char('DVRName', states=_states)
    starttime = fields.DateTime('Start Time', states=_states)
    stoptime = fields.DateTime('Stop Time', states=_states)
    remote_path = fields.Char('Remote Path', states=_states)
    duration = fields.Integer('Duration', states=_states)
    url = fields.Function(fields.Char('URL', readonly=True),
                          'get_url')
    ip = fields.Char('IP', states=_states)
    filesize = fields.Integer('File Size', states=_states)
    server_filesize = fields.Integer('File Size (Server)', states=_states)

    videoname = fields.Function(fields.Char('Video Name', readonly=True),
                                'get_videoname')

    @classmethod
    def __setup__(cls):
        super(VideoDownloadLog, cls).__setup__()
        cls._buttons.update({
            'check_remote_file': {
                'icon': 'tryton-launch',
            },
        })

    @classmethod
    def default_state(cls):
        return 'in remote'

    @classmethod
    def default_in_server(cls):
        return False

    @classmethod
    def clean_old(cls):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)
        cur = Transaction().connection.cursor()
        t_video_download_log = cls.__table__()
        cur.execute(*t_video_download_log.select(
            Sum(t_video_download_log.server_filesize).as_('size')))
        try:
            size, = cur.fetchone()
            if size > 0:
                size /= 1024*1024
        except ValueError:
            size = -1
            
        if size >= configuration.persistent_size:
            to_delete = []
            delete_size = 0

            for log in cls.search([('state', '=', 'in server')],
                                  order=[('starttime', 'ASC')],
                                  limit=3000):
                server_filesize = log.server_filesize
                if server_filesize > 0:
                    server_filesize /= 1024*1024
                delete_size += server_filesize

                to_delete.append(log)
                if delete_size >= configuration.persistent_minimum_garbage_size:
                    break
            cls.write(to_delete, {'state': 'to delete'})


    @classmethod
    def update_server_filesize(cls):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)
        for log in cls.search([('state', '=', 'in server')],
                              limit=1000):
            url = configuration.formatted_url(os.path.basename(log.remote_path),
                                              datetime_in_timezone(log.starttime),
                                              log.dvrname,
                                              configuration.url_do_check)
            try:
                req = urllib.request.Request(url=url, method='GET')
                res = urllib.request.urlopen(req)
                if res.status == 200:
                    filesize = int(re.sub(r"[^\d]+", "", str(res.read())))
                    log.server_filesize = filesize
                    log.save()
            except Exception:
                logger.warn("Fallo al verificar VideoDownloadLog %d url %s" % (log.id, url), exc_info=True)
                continue

    @dualmethod
    def check_remote_file(cls, records):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)
        for log in records:
            url = configuration.formatted_url(os.path.basename(log.remote_path),
                                              datetime_in_timezone(log.starttime),
                                              log.dvrname,
                                              configuration.url_do_check)
            try:
                req = urllib.request.Request(url=url, method='GET')
                res = urllib.request.urlopen(req)
                if res.status == 200:
                    try:
                        filesize = int(re.sub(r"[^\d]+", "", str(res.read())))
                    except:
                        filesize = 0
                    log.server_filesize = filesize
                    log.state = 'in server'
                    log.save()
            except Exception:
                logger.warn("Fallo al verificar VideoDownloadLog %d url %s" % (log.id, url), exc_info=True)
                continue

    @classmethod
    def update_in_server(cls):
        records = cls.search([('state', '=', 'in remote')],
                             limit=1000)
        return cls.check_remote_file(records)

    @classmethod
    def update_to_delete(cls, backdays=3):
        now = datetime.now() - timedelta(days=backdays)
        cls.write(cls.search([('starttime', '<=', now)]),
                  {'state': 'to delete'})
            

    @classmethod
    def do_delete(cls):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)
        if not configuration.url_do_remove:
            return

        data = []
        for videolog in cls.search([('state', '=', 'to delete')],
                                   limit=1000):
            data.append({
                'year': videolog.starttime.year,
                'month': videolog.starttime.month,
                'day': videolog.starttime.day,
                'dvrname': videolog.dvrname,
                'name': os.path.basename(videolog.remote_path),
                'id': videolog.id,
            })
        message = {
            'videologs': data
        }
        json_request = json.dumps(message).encode('utf-8')

        req = urllib.request.Request(configuration.url_do_remove)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        req.add_header('Content-Length', len(json_request))
        res = urllib.request.urlopen(req, json_request)

        if res.status == 200:
            videologs_ok = json.load(res)
            to_delete = []
            for videolog in videologs_ok:
                to_delete.append(cls(videolog['id']))
                cls.delete(to_delete)


class VideoDownloadLogHistoryConfirm(ModelView):
    'VideoDownloadLog - HistoryConfirm'
    __name__ = 'heilhikvision_backend.video_history_confirm'

    videologs = fields.One2Many('heilhikvision_backend.video_download_log', None,
                                'Video Logs',
                                readonly=True,
                                required=True)
    accept = fields.Boolean('Accept')
    

class VideoDownloadLogHistory(Wizard):
    'VideoDownloadLog - History'
    __name__ = 'heilhikvision_backend.video_history'
    start = StateView('heilhikvision_backend.video_history_confirm',
                      '',
                      [Button('Cancel', 'end', 'tryton-cancel'),
                       Button('Confirm', 'history', 'tryton-ok')])
    history = StateTransition()

    def default_start(self, names):
        VideoDownloadLog = Pool().get('heilhikvision_backend.video_download_log')
        videologs = VideoDownloadLog.search([('id', 'in', Transaction().context['active_ids']),
                                             ('state', 'in', ['in server', 'to delete'])])
        return {
            'videologs': list(map(int, videologs))
        }

    def transition_history(self):
        configuration = Pool().get('heilhikvision_backend.configuration')(1)

        if not self.start.accept:
            raise UserError('Debe aceptar')

        if not self.start.videologs:
            raise UserError('se requiere registros')

        data = []
        for videolog in self.start.videologs:
            data.append({
                'year': videolog.starttime.year,
                'month': videolog.starttime.month,
                'day': videolog.starttime.day,
                'dvrname': videolog.dvrname,
                'name': os.path.basename(videolog.remote_path),
                'id': videolog.id,
            })
        message = {
            'videologs': data
        }
        json_request = json.dumps(message).encode('utf-8')
        
        req = urllib.request.Request(configuration.url_do_persistent)
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        req.add_header('Content-Length', len(json_request))
        res = urllib.request.urlopen(req, json_request)
        if res.status == 200:
            videologs_ok = json.load(res)
            to_write = []
            for videolog in videologs_ok:
                to_write.append(VideoDownloadLog(videolog['id']))
            VideoDownloadLog.write(to_write, {'state': 'persistent'})
            return 'end'
        else:
            raise RuntimeError(res)
   

class VideoDownloadLogCheckRemote(Wizard):
    'VideoDownloadLog - CheckRemote'
    __name__ = 'heilhikvision_backend.check_remote'
    start = StateTransition()
    def transition_start(self):
        logs = VideoDownloadLog.search([('id', 'in', Transaction().context['active_ids'])])
        VideoDownloadLog.check_remote_file(logs)
        return 'end'


class VideoDownloadLogM3U(Wizard):
    'VideoDownloadLog - M3U'
    __name__ = 'heilhikvision_backend.m3u'
    start = StateReport('heilhikvision_backend.m3u.report')

    def do_start(self, action):
        videologs = VideoDownloadLog.search([('id', 'in', Transaction().context['active_ids']),
                                             ('state', 'in', ['in server', 'persistent'])])
        playlist = []
        for videolog in videologs:
            playlist.append({
                'dvrname': videolog.dvrname,
                'name': videolog.videoname,
                'duration': videolog.duration,
                'url': videolog.url,
            })
        return action, {'playlist': playlist}
