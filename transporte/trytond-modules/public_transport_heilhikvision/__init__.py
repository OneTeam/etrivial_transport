# This file is part of public_transport_heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .dispatch import *

__all__ = ['register']


def register():
    Pool.register(
        Dispatch,
        module='public_transport_heilhikvision', type_='model')
    Pool.register(
        DispatchVideoDownloadLogM3U,
        module='public_transport_heilhikvision', type_='wizard')
    Pool.register(
        module='public_transport_heilhikvision', type_='report')
