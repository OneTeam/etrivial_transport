# This file is part of public_transport_heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import timedelta

from trytond.model import ModelView, fields

from trytond.wizard import Wizard, StateView, Button, StateTransition, StateReport
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

OFFSET_MINUTES = 10

class Dispatch(metaclass=PoolMeta):
    __name__ = 'public_transport.dispatch'

    @fields.depends('state', 'vehicle', 'init_time', 'end_time')
    def get_videologs(self, name):
        if self.state not in ['done']:
            return []

        VideoDownloadLog = Pool().get('heilhikvision_backend.video_download_log')
        vehicle_key = [self.vehicle.internal_code, self.vehicle.licence_plate]
        logs = VideoDownloadLog.search_read([('starttime', '>=', self.init_time - timedelta(minutes=OFFSET_MINUTES)),
                                             ('starttime', '<=', self.end_time + timedelta(minutes=OFFSET_MINUTES)),
                                             ('dvrname', 'in', vehicle_key)],
                                            order=[('starttime', 'ASC')],
                                            fields_names=['id'])
        return [r['id'] for r in logs]

    videologs = fields.Function(fields.One2Many('heilhikvision_backend.video_download_log', None, 'Video Logs'),
                                'get_videologs')
    


class DispatchVideoDownloadLogM3U(Wizard):
    'Dispatch - VideoDownloadLog M3U'
    __name__ = 'public_transport.dispatch-video_download_log_m3u'

    start = StateReport('public_transport.dispatch-video_download_log_m3u.report')
    def do_start(self, action):
        Dispatch = Pool().get('public_transport.dispatch')
        playlist = []
        for dispatch in Dispatch.search([('id', 'in', Transaction().context['active_ids'])]):
            for videolog in dispatch.videologs:
                if videolog.state != 'in server':
                    continue
                playlist.append({
                    'name': videolog.videoname,
                    'dvrname': videolog.dvrname,
                    'duration': videolog.duration,
                    'url': videolog.url
                })
        return action, {'playlist': playlist}
