=======================================
Public Transport Heilhikvision Scenario
=======================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install public_transport_heilhikvision::

    >>> config = activate_modules('public_transport_heilhikvision')
