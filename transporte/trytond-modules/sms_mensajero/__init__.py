# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .sms import *
from .configuration import *

__all__ = ['register']


def register():
    Pool.register(
        Sms,
        Configuration,
        TestView,
        SmsLog,
        SmsGrantRules,
        SendSmsView,
        module='sms_mensajero', type_='model')
    Pool.register(
        TestSms,
        CheckSents,
        ShowState,
        SendSms,
        module='sms_mensajero', type_='wizard')
    Pool.register(
        module='sms_mensajero', type_='report')
