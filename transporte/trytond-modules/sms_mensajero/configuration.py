# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

'''
Created on 10/09/2018

@author: francisco.garcia@santra.com.co
'''

from trytond.model import ModelSQL, ModelView, fields, ModelSingleton
from trytond.pool import Pool

__all__ = ['Configuration']

class Configuration(ModelSingleton,ModelSQL,ModelView):
    """configuraciones sms_mensajero."""
    
    __name__ = "sms_mensajero.configurations"
    
    url = fields.Char('Url',
                      help="url,ip o dominio del servidor a través del cual"
                      "salen los mensajes de textos",
                      )
    user = fields.Char('usuario',
                       help="Usuario para la autenticación en el servidor",
                       )
    password = fields.Char('Clave',
                       help="Clave para la autenticación en el servidor",
                       )
    type = fields.Selection('type_sms_service','Tipo',
                            help="Indica el tipo de servicio que se usará para"
                            "el envio de los sms"
                            )
    
    @staticmethod
    def type_sms_service():
        """Devuelve una lista de tuplas con los tipos de servicios que se
        puede enviar sms.
        
        Este lista es tomada de sms.py para mantener unicidad en los tipos"""
        Sms = Pool().get('sms_mensajero.sms')
        return Sms.types_service()
