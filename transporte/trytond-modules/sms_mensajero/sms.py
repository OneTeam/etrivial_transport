# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
import traceback
import itertools

from datetime import date, datetime, timedelta

from trytond.model import Model, ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from trytond.tools import resolve
from trytond.pyson import Eval, Or, Equal
from trytond.config import config

logger = logging.getLogger(__name__)


__all__ = ['SmsGrantRules', 'Sms', 'TestView', 'TestSms', 'SmsLog', 'CheckSents', 'ShowState', 'SendSms', 'SendSmsView']


def str2date(string, format_='%Y-%m-%d'):
    return date.fromordinal(datetime.strptime(string, format_).toordinal())

def company_timezone(time, timezone=None):
        pool = Pool()
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company')
        try:
            import pytz
            if timezone is None and company_id:
                company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)
            return time.astimezone(timezone)
        except ImportError:
            return time


class Sms(Model):
    """Envia, consulta y verifica envio de sms."""
    # @todo implementar log
    __name__ = 'sms_mensajero.sms'

    @classmethod
    def __setup__(cls):
        super(Sms, cls).__setup__()
        cls._error_messages.update({
            'send_message': ("Mensaje enviado:\n"
                             "%(mobile)s : %(text)s. id: %(id)s."),
            'message_state': ("Estado del mensaje:\n"
                              "%(state)s.")})

    @classmethod
    def send_enqueue(cls, messages, **attrs):
        SmsLog = Pool().get('sms_mensajero.sms.log')
        attrs_list = []
        for key, val in attrs.items():
            attrs_list.append((key, val))
            
        logs = list(map(dict, [[('to', to),
                                ('message', message),
                                ('last_check', datetime.now()),
                                ('state', 'in process')] + attrs_list for (to, message) in messages]))

        smslogs = SmsLog.create(logs)
        for smslog in smslogs:
            SmsLog.__queue__.send(smslog)
        return smslogs

    @classmethod
    def send(cls, messages, **attrs):
        """Envia los mensajes usando la configuracion.

        @param messages: lista de tuplas con mobil y sms.
         Ej: [('3007668787','mensaje a enviar),...]
        """
        # TODO: se mantiene compatibilidad, una vez
        # se decida los workers se reemplazaria send por send_enqueue
        if config.getboolean('queue', 'worker'):
            return cls.send_enqueue(messages, **attrs)

        records  = []
        SmsLog = Pool().get('sms_mensajero.sms.log')
        if not messages:
            return
        mensajero = cls.get_mensajero()
        log = mensajero.send(messages, **attrs)
        logger.info('Log mensajes enviados: {}'.format(log))
        if log:
            records = SmsLog.create(log)
            SmsLog.__queue__.process(records)
            logger.info("Records post logs: {}".format(records))
        return records

    @classmethod
    def message_state(cls, message_id, to_text=False):
        """Consulta el estado de un mensaje dado."""
        mensajero = cls.get_mensajero()
        return mensajero.message_state(message_id, to_text)

    @classmethod
    def issent(cls, message_id):
        """Consulta si el mensaje esta enviado."""
        mensajero = cls.get_mensajero()
        return mensajero.issent(message_id)

    @classmethod
    def modem_state(cls, to_text=False):
        """Consulta el estado del modem."""
        mensajero = cls.get_mensajero()
        return mensajero.modem_state(to_text)

    @classmethod
    def types_service(cls):
        """Devuelve una lista de tuplas con los tipos de servicios que se
        puede enviar sms.

        Este lista es usada tambien por configuracion.py para mantener unicidad
        en los tipos"""
        return [('SmsJson', 'Json'), ('SmsTest', 'Test')]

    @classmethod
    def get_mensajero(cls):
        """devuelve un mensajero (objeto del tipo SmsGeneric) de acuerdo al tipo configurado.

        """
        Configuration = Pool().get('sms_mensajero.configurations')
        configuration = Configuration.get_singleton()

        if not configuration:
            Configuration.raise_user_error('Error en la configuracion Sms.')

        klass = resolve(
            "trytond.modules.sms_mensajero.client_sms.{}".format(configuration.type))
        mensajero = klass(configuration.url,
                          configuration.user, configuration.password)
        return mensajero


class SmsLog(ModelSQL, ModelView):
    """Registro de mensajes enviados y fallados."""

    __name__ = 'sms_mensajero.sms.log'

    _states = {
        'readonly': True
    }

    state = fields.Selection([
        ('in process', 'In Process'),
        ('ok', 'OK'),
        ('fail', 'Fail')
    ], 'State', readonly=True)

    to = fields.Char('Para',
                     help="# de celular", states=_states)
    message = fields.Text('Mensaje', states=_states)
    id_message = fields.Char('id del mensaje', states=_states,
                             help="Asignado por el prestador del servicio")

    tag = fields.Char('Etiqueta interna', states=_states,
                      help="Sirve para identificar el origen del mensaje.\n"
                      "Se suele usar el nombre del modelo que generó el mensaje")
    send_at = fields.DateTime('Send At', states=_states)
    retries_to_fail = fields.Integer('Retries To Fail', states=_states)
    last_check = fields.DateTime('Last Check', states=_states)
    interval_check = fields.Integer('Interval (Secs) to check', states=_states)
    forwarded = fields.Boolean('Forwarded', states=_states)
    
    @classmethod
    def __setup__(cls):
        super(SmsLog, cls).__setup__()
        cls._buttons.update({
            'resent': {
                'readonly': Eval('state').in_(['in process']),
                'depends': 'state'
            }
        })

    @staticmethod
    def default_forwarded():
        return False

    @staticmethod
    def default_send_at():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'in process'

    @staticmethod
    def default_retries_to_fail():
        return 3

    @staticmethod
    def default_interval_check():
        return 90

    @staticmethod
    def default_last_check():
        return datetime.now()

    @classmethod
    def upcheck_sent(cls, record):
        if Sms.issent(record.id_message):
            record.state = 'ok'
        elif record.retries_to_fail <= 0:
            record.state = 'fail'
        else:
            record.retries_to_fail -= 1
        record.save()

    def process(records):
        """
        Metodo para ser llamado como SmsLog.__queue__.process(..)
        """
        if records is None:
            return

        if isinstance(records, list):
            for record in records:
                SmsLog.do_process(record)
        else:
            SmsLog.do_process(records)
    
    @classmethod
    def do_process(cls, record):
        if record.state in ['ok', 'fail']:
            return

        if record.retries_to_fail <= 0:
            record.state = 'fail'
            record.save()
            return

        wait_delta = (datetime.now() - record.last_check).total_seconds() + 1
        if wait_delta < record.interval_check:
            with Transaction().set_context(
                    queue_name='sms_log_check',
                    queue_scheduled_at = timedelta(seconds=(record.interval_check - wait_delta))
                    ):
                SmsLog.__queue__.process(record)
            return
        SmsLog.upcheck_sent(record)
        record.last_check = datetime.now()
        record.save()
        SmsLog.__queue__.process(record)

    @classmethod
    def check_sents(cls, ids=[], limit_date=None, back_days=None):
        """Verifica los mensajes no enviados (que tienen id_message) a ver si ya fueron enviados.

        @param ids: lista de identificadores de registros del log. Si se indica
         solo verifica los registros inidicados

        @param limit_date: date object o str () si se indica solo se tendrá en cuenta
         registros posteriores a esta fecha.

        @param back_days: int si se indica solo los registros posteriores
         a la fecha actual menos los dias indicados se verificarán.

        En caso de pasarse los 2 parametros se tendrá en cuenta el primero.
        """
        Sms = Pool().get('sms_mensajero.sms')
        domain = [('state', '=', 'in process'), ('id_message', '!=', '')]

        if ids:
            domain.append(('id', 'in', ids))

        if limit_date or back_days:
            if limit_date:
                if isinstance(limit_date, str):
                    limit_date = str2date(limit_date)
                domain.append(('create_date', '>', limit_date))
            else:
                domain.append(
                    ('create_date', '>', date.today() - timedelta(days=back_days)))


        records = cls.search(domain)
        for record in records:
            cls.upcheck_sent(record)

    def send(self):
        try:
            self._send()
        except Exception as e:
            traceback.print_exc()
            self.state = 'fail'
            self.save()

    def _send(self):
        now = datetime.now()
        if SmsGrantRules.allowed(now):
            mensajero = Sms.get_mensajero()
            log, = mensajero.send([(self.to, self.message)], tag=self.tag)
            self.id_message = log['id_message']
            self.state = log['state']
            self.last_check = log['last_check']
            self.send_at = now
            self.save()
            with Transaction().set_context(
                    queue_name='sms_log_send',
                    queue_scheduled_at=timedelta(minutes=1)):
                SmsLog.__queue__.process(self)
        else:
            send_at = SmsGrantRules.send_at()
            wait_time = send_at - datetime.now()

            with Transaction().set_context(
                    queue_name='sms_log_send',
                    queue_scheduled_at=wait_time):
                self.send_at = send_at
                self.save()
                SmsLog.__queue__.send(self)
        

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()

        default.setdefault('last_check', cls.default_last_check())
        default.setdefault('interval_check', cls.default_interval_check())
        default.setdefault('state', cls.default_state())
        default.setdefault('send_at', cls.default_send_at())
        default.setdefault('retries_to_fail', cls.default_retries_to_fail())
        default.setdefault('forwarded', True)
        crecords = super(SmsLog, cls).copy(records, default=default)
        for record in crecords:
            record.message = "REENVIADO>{msg}".format(msg=record.message)
        cls.save(crecords)
        return crecords

    @classmethod
    @ModelView.button
    def resent(cls, records):
        crecords = cls.copy(records)
        SmsLog.__queue__.process(crecords)


class SmsGrantRules(ModelSQL, ModelView):
    'Sms - GrantRules'

    __name__ = 'sms_mensajero.sms.grant-rules'

    @classmethod
    def get_wday(cls):
        return [
            ('0', 'Monday'),
            ('1', 'Tuesday'),
            ('2', 'Wednesday'),
            ('3', 'Thursday'),
            ('4', 'Friday'),
            ('5', 'Saturday'),
            ('6', 'Sunday')
        ]

    wday = fields.Selection('get_wday', 'Week Day', help='Zona horaria de la empresa')
    init_at = fields.Time('Init At', help='Zona horaria de la empresa')
    end_at = fields.Time('End At', help='Zona horaria de la empresa')
    comment = fields.Char('Comment')

    @classmethod
    def check_rule(cls, rule, dtime):
        if dtime.weekday() == int(rule.wday) \
           and rule.init_at <= dtime.time() <= rule.end_at:
            return True
        return False

    @classmethod
    def allowed(cls, dtime):
        rules = cls.search([])
        for rule in rules:
            if cls.check_rule(rule, company_timezone(dtime)):
                return True
        return False

    @classmethod
    def send_at(cls):
        """
        calcula proxima fecha y hora de envio valida
        """
        offset_rule = None
        dtime = company_timezone(datetime.now())
        # TODO: simplificar
        # se divide la lista en 2 segun el dia de la semana
        # al estar ordenados la lista de la izquierda son los dias anteriores
        # al en curso y a la derecha los posteriores
        rules = sorted(list(cls.search([], order=[('wday', 'ASC')])), key=lambda k: k.wday)
        rules_left = []
        rules_same = []
        rules_right = []
        for rule in rules:
            if int(rule.wday) < dtime.weekday():
                rules_left.append(rule)
            elif int(rule.wday) > dtime.weekday():
                rules_right.append(rule)
            else:
                rules_same.append(rule)

        for rule in rules_same:
            if cls.check_rule(rule, dtime):
                offset_rule = rule
                break

        if not offset_rule:
            offset_rule = next(itertools.chain(rules_right, rules_left))

        #en caso de no haber reglas se envia inmediatamente
        if not offset_rule:
            return datetime.now()

        displace_time = datetime.now().replace(hour=offset_rule.init_at.hour,
                                               minute=offset_rule.init_at.minute,
                                               second=offset_rule.init_at.second)
        while True:
            if displace_time.weekday() == int(offset_rule.wday):
                return displace_time
            displace_time += timedelta(days=1)


class TestView(ModelView):
    """Vista de pruebas Sms."""

    __name__ = 'sms_mensajero.sms.test_send.test_view'

    mobile = fields.Char('Numero de Celular')
    message = fields.Text('Mensaje')
    modem_state = fields.Text('Estado del Modem', readonly=True)
    id_message = fields.Char('identificador del mensaje',
                             help='Solo si se quiere consultar el estado de un mensaje')


class TestSms(Wizard):
    """Asistente para la prueba de los mensajes de texto.

    """
    __name__ = 'sms_mensajero.sms.test_sms'

    start = StateView('sms_mensajero.sms.test_send.test_view',
                      '', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Enviar mensaje', 'send',
                                 'tryton-sms', default=True),
                          Button('Estado del mensaje', 'state', 'tryton-sms'),
                          Button('¿Enviado?', 'issent', 'tryton-sms'),
                      ])
    send = StateTransition()
    issent = StateTransition()
    state = StateTransition()

    def default_start(self, fields):
        Sms = Pool().get('sms_mensajero.sms')
        modem_state = Sms.modem_state(to_text=True)
        return {'modem_state': modem_state}

    def transition_send(self):
        Sms = Pool().get('sms_mensajero.sms')
        Sms.send([(self.start.mobile, self.start.message)], tag=self.__name__)
        return 'start'

    def transition_state(self):
        Sms = Pool().get('sms_mensajero.sms')
        if not self.start.id_message:
            Sms.raise_user_error('Debe ingresar un identificador de mensaje.')
        message_state = Sms.message_state(self.start.id_message, to_text=True)
        Sms.raise_user_warning('message_state_{}'.format(self.start.id_message),
                               'message_state',
                               {'state': message_state}
                               )
        return 'start'

    def transition_issent(self):
        Sms = Pool().get('sms_mensajero.sms')
        if not self.start.id_message:
            Sms.raise_user_error('Debe ingresar un identificador de mensaje.')
        issent = Sms.issent(self.start.id_message)
        logger.info("enviado: {}".format(issent))
        Sms.raise_user_warning('message_issent_{}'.format(self.start.id_message),
                               'Mensaje{}Enviado'.format(' ' if issent else ' no '))
        return 'start'


class CheckSents(Wizard):
    """Verifica si el mensaje o los mensajes fueron enviados."""

    __name__ = 'sms_mensajero.sms.log.check_sents'

    start = StateTransition()

    def transition_start(self):
        SmsLog = Pool().get('sms_mensajero.sms.log')
        ids = Transaction().context.get('active_ids')
        SmsLog.check_sents(ids)
        return 'end'


class ShowState(Wizard):
    """Muestra el estado de un mensaje."""

    __name__ = 'sms_mensajero.sms.log.show_state'

    start = StateTransition()

    def transition_start(self):
        SmsLog = Pool().get('sms_mensajero.sms.log')
        Sms = Pool().get('sms_mensajero.sms')
        id_ = Transaction().context.get('active_id')
        record = SmsLog.browse([id_])[0]
        state = Sms.message_state(record.id_message, to_text=True)
        SmsLog.raise_user_warning('message_state_{}'.format(id_),
                                  '{}'.format(state))
        return 'end'




class SendSmsView(ModelView):
    """Vista de pruebas Sms."""

    __name__ = 'sms_mensajero.sms.send_sms.send_view'

    mobile = fields.Char('Numero(s) de Celular', help="Number(s) separate by comma,")
    contacts = fields.Many2Many('party.contact_mechanism', None, None, 'Contacts',
                                domain=['OR', [
                                    # party_co de santra
                                    # aunque esta implementacion no es correcta segun tryton
                                    ('type', '=', 'sms'),
                                    # esta seria la correcta pero no esta implementado
                                    #('sms', '=', True)
                                ]])
    message = fields.Text('Mensaje', required=True)
    omit_invalid_numbers = fields.Boolean('Omit Invalid Numbers')
    modem_state = fields.Char('Estado del Modem', readonly=True)

class SendSms(Wizard):
    """Asistente para envio de mensajes manualmente
    """
    __name__ = 'sms_mensajero.sms.send_sms'

    start = StateView('sms_mensajero.sms.send_sms.send_view',
                      '', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Enviar mensaje', 'send',
                                 'tryton-ok', default=True),
                      ])
    send = StateTransition()
    issent = StateTransition()
    state = StateTransition()

    @classmethod
    def __setup__(cls):
        super(SendSms, cls).__setup__()
        cls._error_messages.update({
            'invalid_numbers': ("Please verify numbers %s")
        })

    def _validate_mobile_colombia(self, number):
        return len(number) == 10
    
    def transition_send(self):
        Sms = Pool().get('sms_mensajero.sms')
        bulk = []
        mobiles = self.start.mobile.split(",") + [contact.value for contact in self.start.contacts]
        invalid_numbers = []
        for number in mobiles:
            if self.start.omit_invalid_numbers and number.isdigit():
                bulk.append((number, self.start.message))
            elif not self.start.omit_invalid_numbers and self._validate_mobile_colombia(number):
                bulk.append((number, self.start.message))
            else:
                invalid_numbers.append(number)

        if invalid_numbers:
            self.raise_user_error('invalid_numbers', ",".join(invalid_numbers))

        Sms.send(bulk, tag=self.__name__)
        return 'start'

