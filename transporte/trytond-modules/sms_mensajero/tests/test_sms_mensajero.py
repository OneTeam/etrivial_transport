# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class SmsMensajeroTestCase(ModuleTestCase):
    'Test Sms Mensajero module'
    module = 'sms_mensajero'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            SmsMensajeroTestCase))
    return suite
