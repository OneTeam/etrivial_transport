# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


try:
    from trytond.modules.sms_mensajero.tests.test_sms_mensajero import suite
except ImportError:
    from .test_sms_mensajero import suite

__all__ = ['suite']
