# This file is part of sms_mensajero.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import time
import pprint

from datetime import datetime
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


__all__ = ['SmsSoap', 'SmsJson', 'build_sms_log']


_gammu_message_sent_ok = {
    'SendingOK': True,
    'SendingOKNoReport': True,
    'SendingError': False,
    'DeliveryOK': True,
    'DeliveryFailed': False,
    'DeliveryPending': False,
    'DeliveryUnknown': True,
    'Error': False,
    'Reserved': False,
    'False': False  # mensaje que no hace parte del core de gammu
}


def build_sms_log(mobil, message, id_message, last_check, **attrs):
    msg = {
        'to': mobil,
        'message': message,
        'id_message': str(id_message),
        'state': 'in process',
        'last_check': datetime.now()
    }
    msg.update(attrs)
    return msg

def dict2str(dict_):
    return pprint.pformat(dict_)


class SmsGeneric(object):
    def __init__(self, url, user, password):
        self.url = url
        self.user = user
        self.password = password

    def send(self, messages, **attrs):
        raise NotImplementedError

    def modem_state(self, to_text=False):
        raise NotImplementedError

    def message_state(self, message_id, to_text=False):
        raise NotImplementedError


class SmsTest(SmsGeneric):
    """Envia mensaje a logger"""


    def send(self, messages, **attrs):
        log = []
        for mobil, message in messages:
            params = {'username': self.user,
                      'password': self.password,
                      'celular': mobil,
                      'texto': message}
            logger.info("""
            Enviando {}
            """.format(str(params)))

            id_message = int(time.mktime(datetime.now().timetuple()))
    
            data = build_sms_log(mobil, message, id_message, datetime.now())
            data.update(attrs)
            log.append(data)
        return log

    def message_state(self, message_id, to_text=False):
        return "OK"

    def modem_state(self, to_text=True):
        return "OK"


    def issent(self, message_id):
        return True

# DEPRECATED: en favor de SmsJson
class SmsSoap(SmsGeneric):
    """Envia sms usando soap."""

    def send(self, messages, tag=''):
        """
        @param messages: [('233232', 'mensajes')..]
        @param tag: 'custom tag'
        """

        request = 'enviar_mensaje'
        log = []
        for mobil, message in messages:
            params = {'username': self.user,
                      'password': self.password,
                      'celular': mobil,
                      'texto': message}

            id_message = self.id_from_response(self.proxy_soap(request,
                                                               params=params,
                                                               wsse=True))
            log.append(build_sms_log(mobil, message,
                                     self.response2id_message(id_message) or '',
                                     datetime.now()))
        return log

    def response2id_message(self, response_):
        response_ = response_[0]
        if hasattr(response_, 'id_envio_mensaje'):
            return str(response_.id_envio_mensaje)
        else:
            return None

    def modem_state(self, to_text=False):
        request = 'estado_modem'
        params = {'username': self.user,
                  'password': self.password}

        modem_state = {}
        try:
            response = self.proxy_soap(request, params, wsse=True)
            modem_state = self.soap_object2dic(response[0])
        except IndexError:
            pass

        if to_text:
            return self.dict2str(modem_state)
        return modem_state

    def message_state(self, message_id, to_text=False):
        request = 'estado_mensaje'
        params = {'username': self.user,
                  'password': self.password,
                  'id': message_id}

        message_state = {}
        try:
            response = self.proxy_soap(request, params, wsse=True)
            message_state = self.soap_object2dict(response[0])
        except IndexError:
            pass

        if to_text:
            return dict2str(message_state)
        return message_state

    def issent(self, message_id):
        """Consulta si el mensaje esta enviado."""
        try:
            message_state = self.message_state(message_id)
            return _gammu_message_sent_ok[message_state['Status']]
        except KeyError:
            return False

    def proxy_soap(self, request, params={}, wsse=False):
        try:
            from zeep import Client
            if wsse:
                from zeep.wsse.username import UsernameToken
        except ModuleNotFoundError:
            raise ModuleNotFoundError

        if wsse and 'username' in params.keys() and 'password' in params.keys():
            wsse_p = UsernameToken(params['username'], params['password'])
            del params['username'], params['password']
            client = Client(self.url, wsse=wsse_p)
        else:
            client = Client(self.url)
            Request = getattr(client.service, request)
        if params:
            response = Request(**params)
        else:
            response = Request()
        return response

    def soap_object2dict(self, soap_object):
        d = {}
        for k in soap_object:
            d[k] = getattr(soap_object, k)
        return d

    @classmethod
    def id_from_response(cls, response):
        return response


class SmsJson(SmsGeneric):
    """Envia, consulta y verifica envio de sms."""

    def send(self, messages, **attrs):
        method = 'enviar_mensaje'
        log = []
        for mobil, message in messages:
            params = {'celular': mobil,
                      'texto': message}

            response = self.proxy_json(method, params)
            id_message = self.response2id_message(response)
            data = build_sms_log(mobil, message, id_message, datetime.now(), **attrs)
            log.append(data)

        return log

    def response2id_message(self, send_response):
        """
        Extrae el id_envio_mensaje de la respuesta de servicio y lo devuelve,
        si no hay id_envio_mensaje devuelve None
        """
        try:
            return send_response['Arreglos_enviar_mensaje'][0]['id_envio_mensaje']
        except Exception:
            return None

    def modem_state(self, to_text=False):
        method = 'estado_modem'
        params = {
        }
        response = self.proxy_json(method, params)
        if to_text:
            return self.modem_state2text(response)
        return response

    def modem_state2text(self, modem_state):
        """convierte un response de modem_state en texto."""
        try:
            return dict2str(modem_state['Arreglos_estado_modem'][0])
        except IndexError:
            return str(modem_state)
        except KeyError:
            return str(modem_state)

    def message_state(self, message_id, to_text=False):
        method = 'estado_mensaje'
        params = {'username': self.user,
                  'password': self.password,
                  'id': message_id}

        message_state = self.proxy_json(method, params)
        message_state = self.message_state2dict(message_state)

        if to_text:
            return dict2str(message_state)
        return message_state

    def message_state2dict(self, message_state):
        try:
            return message_state['Arreglos_estado_mensaje'][0]
        except IndexError:
            return {}
        except KeyError:
            return {}

    def issent(self, message_id):
        """Consulta si el mensaje esta enviado."""
        try:
            message_state = self.message_state(message_id)
            return _gammu_message_sent_ok[message_state['Status']]
        except KeyError:
            return False

    def proxy_json(self, method, params={}):
        try:
            import requests
        except ModuleNotFoundError:
            raise ModuleNotFoundError
        response = requests.get(url=self.url + '/' + method, params=params)
        return response.json()
