# trytond_sms_mensajero

The sms_mensajero module of the Tryton application platform.

## reglas

Para poder realizar los envios es necesario indicar las reglas de los horarios permitidos *Sms/Rules*.

Todo mensaje enviado fuera de estas reglas se programara su envio para el proximo horario disponible.

## como extender?

se invita que al extender el modulo, se adicione un nuevo 'page' en 'sms_mensajero.sms_log_form' para visualizar
los nuevos atributos

