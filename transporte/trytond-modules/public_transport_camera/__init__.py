# This file is part of public_transport_camera.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

__all__ = ['register']


def register():
    Pool.register(
        module='public_transport_camera', type_='model')
    Pool.register(
        module='public_transport_camera', type_='wizard')
    Pool.register(
        module='public_transport_camera', type_='report')
