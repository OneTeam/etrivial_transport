# This file is part of public_transport_camera.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker


class PublicTransportCameraTestCase(ModuleTestCase):
    'Test Public Transport Camera module'
    module = 'public_transport_camera'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PublicTransportCameraTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_public_transport_camera.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
