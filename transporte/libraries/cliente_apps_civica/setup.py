# This file is part of cliente_apps_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from setuptools import setup

setup(
    name="cliente_apps_civica",
    version="0.0.0",
    description="Cliente para apps.civica.com.co",
    author='Jovany Leandro G.C',
    author_email='bit4bit@riseup.net',
    url='http://www.etrivial.net',
    license="GPL3+",
    packages=["cliente_apps_civica"],
    install_requires=["bs4", "requests"],
    test_suite="tests",
    entry_points={
        "console_scripts": [
            "reporte_apps_civica=cliente_apps_civica.__main__:main"
        ]
    }
)
