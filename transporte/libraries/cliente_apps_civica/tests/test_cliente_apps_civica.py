# This file is part of cliente_apps_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest
import warnings
import os
import io
from datetime import datetime, timedelta

import cliente_apps_civica as apcv

class TestClienteAppsCivica(unittest.TestCase):

    def _accesos(self):
        usuario = os.environ.get('APPS_CIVICA_USUARIO')
        clave = os.environ.get('APPS_CIVICA_CLAVE')
        if usuario is None or clave is None:
            warnings.warn('se omite prueba de logea porfavor indica usuario y clave '
                          'en variables de entorno APPS_CIVICA_USUARIO y APPS_CIVICA_CLAVE')
            return None, None
        return usuario, clave

    def test_ingreso_invalido(self):
        ctx = apcv.ingresar('test', 'test')
        self.assertEqual(ctx, None)

    def test_ingreso(self):
        usuario, clave = self._accesos()
        if usuario is None or clave is None:
            return

        ctx = apcv.ingresar(usuario, clave)
        self.assertIsInstance(ctx, dict, 'no se creo contexto')
        apcv.salir(ctx)

    def test_reporteLiquidacionUsosTransporte(self):
        usuario, clave = self._accesos()
        if usuario is None or clave is None:
            return

        ctx = apcv.ingresar(usuario, clave)
        now = datetime.now()
        inicial = now.replace(hour=0,second=0,microsecond=0)
        final = inicial + timedelta(days=1)
        body = apcv.reporteLiquidacionUsosTransporte(ctx, inicial, final)
        length = 0
        for data in body:
            length += len(data)
        self.assertTrue(length >= 0, 'se espera archivo')
        apcv.salir(ctx)

if __name__ == '__main__':
    unittest.main()
