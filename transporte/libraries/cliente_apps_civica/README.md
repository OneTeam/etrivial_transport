# CLIENTE APPS CIVICA

Actualmente se implementa la obtencion del reporte 'Liquidacion Usos Transporte'.

Existen 2 formas de hacer uso de este paquete:
 - al ser instalado se puede llamar el comando 'reporte_apps_civica' o 'python -m cliente_apps_civica'.
 - como libreria importando y llamando las funciones publicas.
