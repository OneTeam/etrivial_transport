# This file is part of cliente_apps_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime, timedelta
from contextlib import contextmanager

from bs4 import BeautifulSoup as BS
import requests

_CONNECTION_TIMEOUT=60

__all__ = ['ingresar',
           'sesion',
           'set_timeout',
           'salir',
           'reporteLiquidacionUsosTransporte',
           'cambio_clave_operador',
           'error']

class PageChangedError(Exception):
    """
    Excepcion cuando ahi un cambio en la pagina
    """
    pass

class LoginError(Exception):
    """
    Fallo al iniciar sesion
    """
    pass

def _get(ctx: dict, url: str, stream=False):
    return ctx['session'].get(url, stream=stream,timeout=ctx['timeout'])

def _post(ctx: dict, url: str, data: dict, stream=False):
    return ctx['session'].post(url, data=data, timeout=ctx['timeout'], stream=stream)

def _new_context():
    return {
        'session': requests.Session(),
        'timeout': _CONNECTION_TIMEOUT,
        'last_error': ''
    }

def _aspx_session_from_url(ctx: dict(), url: str):
    page = _get(ctx, url)
    soup = BS(page.content, "lxml")

    data = {}
    data['__EVENTTARGET'] = ''
    data['__EVENTARGUMENT'] = ''

    for field in ['__VIEWSTATE', '__VIEWSTATEGENERATOR', '__EVENTVALIDATION']:
        data[field] = soup.select_one(f'#{field}').get('value', '')
    return data, soup

def ingresar(usuario: str, clave: str):
    ctx = _new_context()
    params, _ = _aspx_session_from_url(ctx, 'https://apps.civica.com.co/Civica/Index.aspx')
    params['txtLogin'] = usuario
    params['txtSena'] = clave
    params['btnOk'] = 'OK'
    
    page = _post(ctx, 'https://apps.civica.com.co/Civica/Index.aspx', params)
    if page.status_code != 200:
        ctx['session'].close()
        raise LoginError()

    if "txtInfLoginTit" not in page.text:
        ctx['session'].close()
        raise LoginError()
    
    return ctx

def set_timeout(ctx, timeout):
    ctx['timeout'] = timeout
    return ctx

@contextmanager
def sesion(usuario, clave):
    ctx = ingresar(usuario, clave)
    if ctx:
        try:
            yield ctx
        finally:
            salir(ctx)
            
def reporteLiquidacionUsosTransporte(ctx, fecha_hora_inicial: datetime,
                                     fecha_hora_final: datetime) -> iter or None:

    params, soup = _aspx_session_from_url(ctx, 'https://apps.civica.com.co/Civica/Compensacion/Consultas/frmReporteLiquidacionUsosTransporte.aspx')
    if any([
            soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstCuenca"}) is None,
            soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstEmpresas"}) is None,
            soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstRuta"}) is None
    ]):
        raise PageChangedError()
    
    options = soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstCuenca"}).find_all('option')
    lstCuenca = [str(o['value']) for o in options]

    options = soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstEmpresas"}).find_all('option')
    lstEmpresa = [str(o['value']) for o in options]

    options = soup.find('select',{'name': "ctl00$ContentPlaceHolder1$lstRuta"}).find_all('option')
    lstRuta = [str(o['value']) for o in options]
    params["ctl00$ContentPlaceHolder1$lstCuenca"] = lstCuenca
    params["ctl00$ContentPlaceHolder1$lstEmpresas"] = lstEmpresa
    params["ctl00$ContentPlaceHolder1$lstRuta"] = lstRuta
    params["ctl00$ContentPlaceHolder1$txtFechaInicial"] = fecha_hora_inicial.strftime("%d/%m/%Y")
    params["ctl00$ContentPlaceHolder1$txtHoraIni"] = fecha_hora_inicial.strftime("%H:%M:%S")
    params["ctl00$ContentPlaceHolder1$mkeHoraIni_ClientState"] = ""
    params["ctl00$ContentPlaceHolder1$txtFechaFinal"] = fecha_hora_final.strftime("%d/%m/%Y")
    params["ctl00$ContentPlaceHolder1$txtHoraFin"] = fecha_hora_final.strftime("%H:%M:%S")
    params["ctl00$ContentPlaceHolder1$rbBusquedaPor"] = "2"
    params["ctl00$ContentPlaceHolder1$rbTipo"] = "2"
    params["ctl00$ContentPlaceHolder1$rbNivel"] = "3"
    params["ctl00$ContentPlaceHolder1$btnVisualizar"] = "Generar+Reporte"

    urlReporte = "https://apps.civica.com.co/Civica/Compensacion/Consultas/frmReporteLiquidacionUsosTransporte.aspx"
    page = _post(ctx, urlReporte, params, stream=True)
    content_type = page.headers['content-type']

    if page.status_code == 200 \
       and content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        return page.iter_content()
    return None

def cambio_clave_operador(ctx: dict, clave_anterior: str, clave_nueva: str):
    params, _ = _aspx_session_from_url(ctx, 'https://apps.civica.com.co/Civica/Seguridad/CambiarClave/frmCambiarClave.aspx')
    params['ctl00$ContentPlaceHolder1$txtSenha'] = clave_anterior
    params['ctl00$ContentPlaceHolder1$txtNueva'] = clave_nueva
    params['ctl00$ContentPlaceHolder1$txtNuevaConf'] = clave_nueva
    params['ctl00$ContentPlaceHolder1$cmdAlteraSena'] = 'Confirmar'
    _post(ctx, 'https://apps.civica.com.co/Civica/Seguridad/CambiarClave/frmCambiarClave.aspx', params)
    page = _get(ctx, 'https://apps.civica.com.co/Civica/Includes/frmMensagem.aspx')
    soup = BS(page.content, "lxml")

    lbl = soup.select_one("#lblMensagem").text
    if "efectuada con" in lbl:
        return True
    
    ctx['last_error'] = lbl
    return False

def salir(ctx: dict):
    params, _ = _aspx_session_from_url(ctx, 'https://apps.civica.com.co/Civica/Index.aspx')
    params['__EVENTTARGET'] = "ctl00$Header1$lnkLogoff"
    params['__EVENTARGUMENT'] = ''
    _post(ctx, 'https://apps.civica.com.co/Civica/Default.aspx', params)
    ctx['session'].close()

def error(ctx: dict):
    return ctx['last_error']
