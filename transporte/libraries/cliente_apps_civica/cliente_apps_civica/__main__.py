# This file is part of cliente_apps_civica.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime, timedelta
import sys
import os
import cliente_apps_civica as apcv

def _must_usuario_clave():
    usuario = os.environ.get('APPS_CIVICA_USUARIO')
    clave = os.environ.get('APPS_CIVICA_CLAVE')
    if usuario is None or clave is None:
        raise RuntimeError('se requiere usuario y clave por variables de entorno'
                           ' APPS_CIVICA_USUARIO y APPS_CIVICA_CLAVE')
    return usuario, clave

def usage():
    print("""
    reporte_apps_civica  [<reporte> <archivo salida> <fecha inicial> [fecha final] | cambio_clave_operador <clave actual> <nueva clave>]

    archivo salida: ruta donde se almacena reporte.
    reporte: Puede ser 'LiquidacionUsosTransporte'.
    fecha inicial: en formato 2019-12-31T23:59:59.
    fecha final: en formato 2019-12-31T23:59:59
    """)


def cambio_clave_operador(argv):
    if len(argv) != 2:
        usage()
        exit(-1)

    try:
        clave_actual = argv.pop(0)
    except IndexError:
        raise RuntimeError('Se requiere clave actual')

    try:
        clave_nueva = argv.pop(0)
    except IndexError:
        raise RuntimeError('Se requiere nueva clave')

    usuario, clave = _must_usuario_clave()
    with apcv.sesion(usuario, clave) as ctx:
        result = apcv.cambio_clave_operador(ctx, clave_actual, clave_nueva)
        if not result:
            print(apcv.error(ctx))
            exit(1)
        
def LiquidacionUsosTransporte(argv):
    if len(argv) != 2:
        usage()
        exit(-1)
            
    try:
        archivo_salida = argv.pop(0)
    except IndexError:
        raise RuntimeError('se require ruta de archivo de salida')
    
    try:
        fecha_hora_inicial = datetime.strptime(argv.pop(0),
                                               '%Y-%m-%dT%H:%M:%S')
    except IndexError:
        raise ValueError('se requiere fecha y hora inicial en formato 2019-12-30T23:59:59')
        
    try:
        fecha_hora_final = datetime.strptime(argv.pop(0),
                                             '%Y-%m-%dT%H:%M:%S')
    except IndexError:
        fecha_hora_final = (fecha_hora_inicial + timedelta(days=1))\
            .replace(hour=23,minute=59,second=59,microsecond=0)

        
    usuario, clave = _must_usuario_clave()
    with apcv.sesion(usuario, clave) as ctx:
        apcv.set_timeout(ctx, 120)
        body = apcv.reporteLiquidacionUsosTransporte(
            ctx, fecha_hora_inicial, fecha_hora_final
        )
        with open(archivo_salida, 'wb') as f:
            for data in body:
                f.write(data)
                    
    
def main():
    argv = sys.argv.copy()
    program_name = argv.pop(0)

    try:
        reporte = argv.pop(0)
    except IndexError:
        print("!!Debe indicar reporte")
        usage()
        exit(-1)

    if reporte == 'LiquidacionUsosTransporte':
        LiquidacionUsosTransporte(argv)
    elif reporte == 'cambio_clave_operador':
        cambio_clave_operador(argv)
    else:
        print(f"!!Reporte desconocido {reporte}")
        usage()
        exit(-1)
        
if __name__ == '__main__':
    main()
