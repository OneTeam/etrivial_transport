#!/usr/bin/python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

#cgi script para consultar tamano del video
import cgitb
cgitb.enable()

import os.path
import cgi
import urllib.parse

DOWNLOAD_DIR="/camaras/grabaciones"
if not os.access(DOWNLOAD_DIR, os.R_OK):
    raise RuntimeError("director %s not readable" % (DOWNLOAD_DIR))


form = cgi.FieldStorage()
year = int(form.getfirst("year", ""))
month = int(form.getfirst("month", ""))
day = int(form.getfirst("day", ""))
video = urllib.parse.unquote(form.getfirst("video", ""))
dvrname = urllib.parse.unquote(form.getfirst("dvrname", ""))

def get_video_path(download_dir, year, month, day, dvrname, video):
    # convencion usado actualmente year/month/day/dvrname/video
    return os.path.join(download_dir,
                        str(year),
                        str(month),
                        str(day),
                        dvrname,
                        video)

video_path = get_video_path(DOWNLOAD_DIR,
                            year, month, day, dvrname, video)

videolen = os.path.getsize(video_path)
print("Content-Type: text/plain")
if os.path.exists(video_path):
    print("Status: 200 OK")
    print()
    print(os.path.getsize(video_path))
else:
    print("Status: 404 Not Found")
    print()
