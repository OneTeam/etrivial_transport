#!/usr/bin/python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

# cgi script para mover videos a ruta persistente
import cgitb
cgitb.enable()


import sys
import os
import json
import cgi


# los archivos de este directorio deben tener
# permisos de escritura
DOWNLOAD_DIR="/camaras/grabaciones"
# los archivos de este directorio deben tener
# permisos de escritura
PERSISTENT_DIR="/camaras/persistente"

if not os.access(DOWNLOAD_DIR, os.W_OK):
    raise RuntimeError("directory %s not writable" % (DOWNLOAD_DIR))


if not os.access(DOWNLOAD_DIR, os.R_OK):
    raise RuntimeError("director %s not readable" % (DOWNLOAD_DIR))

try:
    os.makedirs(PERSISTENT_DIR)
except:
    pass

if not os.access(PERSISTENT_DIR, os.W_OK):
    raise RuntimeError("directory %s not writable" % (PERSISTENT_DIR))


print("Content-Type: application/json\n")
message = json.loads(sys.stdin.read(int(os.environ['CONTENT_LENGTH'])))
videologs_ok = []

for videolog in message['videologs']:
    src_file = os.path.join(DOWNLOAD_DIR,
                            str(videolog['year']),
                            str(videolog['month']),
                            str(videolog['day']),
                            videolog['dvrname'],
                            videolog['name'])

    if not os.path.exists(src_file):
        continue

    dst_file = os.path.join(PERSISTENT_DIR,
                            str(videolog['year']),
                            str(videolog['month']),
                            str(videolog['day']),
                            videolog['dvrname'],
                            videolog['name'])
    try:
        os.renames(src_file, dst_file)
        videologs_ok.append(videolog)
    except:
        continue

print(json.dumps(videologs_ok), end='')
    
