#!/usr/bin/env python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import sys
from datetime import datetime, timedelta, timezone
import os
import time
import traceback
import json

os.environ['TZ'] = 'UTC'
if hasattr(time, 'tzset'):
    time.tzset()

def usage():
    print("%s <videos dir>" % (sys.argv[0]))

try:
    VIDEOS_DIR = os.path.abspath(sys.argv[1].strip())
except:
    usage()
    exit(-1)

assert os.access(VIDEOS_DIR, os.W_OK), \
    "directorio no tiene permisos de escritura"


APIKEY = os.environ.get('APIKEY', '')
MACHINE = os.environ.get('MACHINE', '')
SERVER_URL = os.environ.get('SERVER_URL', 'http://localhost:8000/demo/heilhikvision/videolog/')

if not APIKEY:
    raise RuntimeError('Need ENVIROMENT APIKEY')

def get_records(page, headers={}):
    #https://stackoverflow.com/questions/11322430/how-to-send-post-request
    from urllib.parse import urlencode
    from urllib.request import Request, urlopen

    url = SERVER_URL + MACHINE + '/inserver/' + str(page)
    request = Request(url, headers=headers)
    data = urlopen(request).read()
    if data:
        return json.loads(data)
    return {}


def loop(page):
    headers={'Authorization': 'Bearer %s' % (APIKEY)}
    bulk = get_records(page, headers=headers)

    for record in bulk['records']:
        if record['remote_path'].startswith(VIDEOS_DIR):
            if os.path.exists(record['remote_path']):
                os.unlink(record['remote_path'])
                print("ELIMINADO %s" % (record['remote_path']))
        else:
            print("NO SE UBICA %s" % (record['remote_path']))
    
    return bulk['total']


page = 0
while True:
    try:
        total = loop(page)
        print("Total:", total, " Page:", page)
        if total:
            page += 1
        else:
            page = 0
    except Exception as e:
        print(traceback.format_exc())
        time.sleep(60 * 5)
    time.sleep(60)
