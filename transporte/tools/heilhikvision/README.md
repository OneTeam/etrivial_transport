# heilhikvision

herramientas para administración de camaras usando [pyehik](https://pypi.org/project/pyehik/).


## Proceso de descarga de videos segun scripts

Lo primero es crear el archivo de configuracion **cdownload.json** por cada camara, este
archivo tiene un atributo **schedules** que se le puede indicar la ruta a un archivo que
contiene las programaciones ejemplo para vehiculo **GNU999**:

*~/GNU999-cdownload.json*
~~~json
{
 "sdk": {
     "channel": 1,
     "port": 8000,
     "username": "admin",
     "password": "admin"
 },
    "prefix": "",
    "download_dir": "/tmp",
 "schedules": '/programaciones/GNU999.json'
}
~~~

**cdispatchpull.py** al correr indicando el directorio **/programaciones** crearia el archivo **/programaciones/GNU999.json** (se usa numero interno o de contrario placa), el cual al ejecutar **cdownload.py GNU999-cdownload.json** tomaria las programaciones y descargaria segun el caso.

**csync.py** tomaria el directorio de descargas buscaria los **cdownload.log** y sincronizaria con el servidor la bitacora de descargas,
es tener presente el archivo **cache.db** que se crea en el directorio de descargas -este contiene la informacion para no repetir datos en
el servidor es importante copiar periodicamente este archivo-.


## flash.py

este script dumpea datos y los vuelve a escribir, este podria usarse para clonar configuraciones.

esta instruccion dumpea a FILE.
~~~
$ python flash.py dump  --file=<FILE> --ip=<IP> --user=<USER> --password=<PASSWORD>
~~~

esta instruccion escribe usando FILE.
~~~
$ python flash.py flash --file=<FILE> --ip=<IP> --user=<USER> --password=<PASSWORD>
~~~

## cdownload.py

este script permite la descarga continuada de los videos.

Requiere en el directorio del script:
  * libhcnetsdk.so
  * libHCCore.so
o puede indicar con variable de entorno **HCNETSDK** la ruta del archivo "libhcnetsdk.so".

para correr con depuracion:
~~~
$ DEBUG=1 python cdownload.py <IP> <CONFIG.json>
~~~

sin depuracion:

~~~
$ python cdownload.py <IP> <CONFIG.json>
~~~

archivo de configuracion de ejemplo seria **miarchivo.json**:
~~~
{
 "sdk": {
     "channel": 1,
     "port": 8000,
     "username": "admin",
     "password": "admin"
 },
 "prefix": "general",
 "download_dir": "/tmp",
 "schedules": [
  {"start-hour": 12, "start-minute": 30,
   "stop-hour": 12, "stop-minute": 40}
 ]
}
~~~

en "schedules" adicionar los horarios de descargas, **cdownload.py** descargara
los videos en los horarios indicados almacenandolos en **download_dir/YYYY/MM/DD/DVRNAME/** y en 
caso de haber cortes los continuara en el ultimo segundo, adicionalmente se crea un archivo llamado **cdownload.log**
que es la bitacora de los archivos descargados.

el atribute "prefix" puede ser usado como agrupador -ejemplo codigo interno del vehiculo-

## csync.py

Script que toma los **cdownload.log** y los sincroniza con **heilhikvision_backend**.

~~~
$APIKEY=xxx MACHINE=mimaquina SERVER_URL=hhhh python csync.py <DOWNLOAD_DIR>
~~~

## cdispatchpull.py

Script que consulta las programaciones de despacho y crea archivo de programacion para el atributo **schedules** de **cdownload.py**.

*!Consulta solo las programaciones del dia en curso*

~~~
$APIKEY=xxx SERVER_URL=hhhh python cdispatchpull.py <PULL_DIR>
~~~
