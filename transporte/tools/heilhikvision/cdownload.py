# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

"""
Script para descarga de grabaciones de camaras hikvision

Que hace? en particular descargar y remotar ultima descarga 
-en caso de fallo o corte- con el fin de completar la programacion de descargas.

se debe indicar un archivo de estructura json con la configuracion.
~~~
{
 'sdk': {
  'channel': 1
 },
 'prefix': '', //prefijo
 'dvrname': '', //nombre del dvr, dejar en blanco para uso automatico de dispositvo o como argumento
 'download_dir': '/tmp', //directorio donde ir almacenado
 'schedules': [
  {'start-hour': 12, 'start-minute': 30,
   'stop-hour': 12, 'stop-minute': 40} //ejemplo de descarga de 10 minutos
 ]
 //o schedules puede ser la ruta a un archivo con 
 // las programaciones en formato json o puede ser la palabra 'today' para descargar
 // dia completo o una variable de entorno
}
~~~
"""
from collections import namedtuple
from datetime import datetime, timedelta, date, timezone
from datetime import time as dttime

import argparse
import sys
import logging
import os
import os.path
import json
import subprocess
import time
import csv
import warnings
import time
import pickle

import pyehik

#codigos de retorno
error_codes = {
    'fail_login': 200
}

os.environ['TZ'] = 'UTC'
if hasattr(time, 'tzset'):
    time.tzset()

#zona horaria local
#-5 colombia
LOCALTZ = timezone(timedelta(hours=-5))

if time.tzname[0] != 'UTC':
    warnings.warn('Zona horaria detectada %s se recomienda UTC' % time.tzname[0])

    
def usage():
    print(sys.argv[0], "<IP> [CONFIG_PATH] [DVRNAME]")
    print("IP ip del equipo")
    print("CONFIG_PATH archivo de configuracion json")

logger = logging.getLogger('cdownload')
if os.environ.get('DEBUG', None):
    logging.basicConfig(level=logging.DEBUG)

SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'

IP = sys.argv[1]
if not IP:
    usage()
    raise RuntimeError('Se requiere IP')

try:
    CONFIG_PATH=sys.argv[2]
except IndexError:
    CONFIG_PATH=os.environ.get("HEILH_CONFIG", "/etc/heildownload.json")
    #warnings.warn('No se indico ruta de configuracion se usa %s' % (CONFIG_PATH))
    
if not CONFIG_PATH:
    usage()
    raise RuntimeError('Invalido archivo configuracion')
try:
    DVRNAME = sys.argv[3]
except IndexError:
    DVRNAME = ''
    #warnings.warn('Se omite argumento de DVRNAME se espera de archivo de configuracion')

config = {'sdk': {'channel': 1,
                  'username': '',
                  'password': '',
                  'port': 8000},
          'prefix': '',
          'dvrname': DVRNAME}
config.update(json.load(open(CONFIG_PATH, "rb")))

## SDK
def get_error_msg(sdk):
    return sdk.NET_DVR_GetErrorMsg(None)

def get_dvr_name(sdk):
    bytesReturned = sdk.DWORD()
    cfg = sdk.NET_DVR_DEVICECFG_V40()
    res = sdk.NET_DVR_GetDVRConfig(userid,
                                   sdk.NET_DVR_GET_DEVICECFG_V40,
                                   config['sdk']['channel'],
                                   sdk.REF(cfg),
                                   sdk.SIZEOF(cfg),
                                   sdk.REF(bytesReturned))
    if not res:
        return ''
    return cfg.sDVRName.decode('utf-8')

# TODO: usar la modulo utils genera fallo de segmentacion porque?
# se copia tal cual aca y no genera.
# ahi una pregunta con este, y es como se comporta cuando
# el video es por eventos?
def get_file_by_time_blocking(sdk, userid, channel, starttime, stoptime, outfile):
    itime = sdk.NET_DVR_TIME.from_datetime(starttime)
    otime = sdk.NET_DVR_TIME.from_datetime(stoptime)
    fhandle = sdk.NET_DVR_GetFileByTime(userid, channel,
                                        sdk.REF(itime), sdk.REF(otime), outfile)
    if fhandle < 0:
        raise RuntimeError(get_error_msg(sdk))

    if not sdk.NET_DVR_PlayBackControl(fhandle, sdk.NET_DVR_PLAYSTART, 0, None):
        raise RuntimeError(get_error_msg(sdk))

    while True:
        pos = sdk.NET_DVR_GetDownloadPos(fhandle)
        if pos >= 100 or pos < 0:
            break
        time.sleep(.5)
        lastError = sdk.NET_DVR_GetLastError()
        if lastError != 0:
            logger.debug('downloading get a invalid return %d' % (lastError))
            return False
    return True

def get_file_by_src(sdk, userid, srcfile, destfile):
    handle = sdk.NET_DVR_GetFileByName(userid, srcfile, destfile)
    while True:
        pos = sdk.NET_DVR_GetDownloadPos(handle)
        if pos >= 100 or pos < 0:
            break
        time.sleep(.5)
    sdk.NET_DVR_StopGetFile(handle)
    return True

def get_filenames(sdk: object, userid: int, channel: int, starttime: datetime, stoptime: datetime):
    conf = sdk.NET_DVR_FILECOND()
    conf.lChannel = channel
    conf.dwFileType = 0xff
    conf.dwIsLocked = 0xff
    conf.struStartTime = sdk.NET_DVR_TIME.from_datetime(starttime)
    conf.struStopTime = sdk.NET_DVR_TIME.from_datetime(stoptime)

    lhandle = sdk.NET_DVR_FindFile_V30(userid, sdk.REF(conf))
    if lhandle < 0:
        raise RuntimeError(get_error_msg(sdk))

    files = []
    while True:
        data = sdk.NET_DVR_FINDDATA_V30()
        res = sdk.NET_DVR_FindNextFile_V30(lhandle, sdk.REF(data))
        if res == sdk.NET_DVR_ISFINDING:
            time.sleep(.1)
            continue

        if res != sdk.NET_DVR_FILE_SUCCESS:
            break
        files.append((data.sFileName,
                      data.dwFileSize,
                      data.struStartTime.to_datetime(),
                      data.struStopTime.to_datetime()))
    return files

def login(sdk, ip, port, username, password):
    info = sdk.NET_DVR_DEVICEINFO_V30()
    return sdk.NET_DVR_Login_V30(ip, port, username, password, sdk.REF(info))

## GENERAL
class Duration(namedtuple('Duration', 'hour minute seconds')):
    def to_seconds(self):
        return self.hour * 3600 + self.minute * 60 + self.seconds

def file_transaction_path():
    return os.path.join(config['download_dir'],
                        '.' + config['dvrname'] + '.trx')

def file_transaction_clean():
    if os.path.exists(file_transaction_path()):
        with open(file_transaction_path(), 'r') as f:
            try:
                os.unlink(f.read())
            except FileNotFoundError:
                pass
        try:
            os.unlink(file_transaction_path())
        except FileNotFoundError:
            pass

def file_begin(video_path):
    with open(file_transaction_path(), 'w+') as f:
        f.write(video_path)

def file_commit(video_path):
    try:
        os.unlink(file_transaction_path())
    except FileNotFoundError:
        pass

def append_log(dvrname: str, starttime: datetime, stoptime: datetime, video_path: str, duration: int):
    """ adiciona registro a bitacora del video """

    logpath = os.path.join(os.path.dirname(video_path), 'cdownload.log')
    logger.debug('appending data to %s' % (logpath))

    with open(logpath, 'a', newline='') as f:
        writer = csv.writer(f, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow([dvrname,
                         starttime.isoformat(),
                         stoptime.isoformat(),
                         video_path,
                         duration,
                         IP,
                         os.path.getsize(video_path)])

def get_video_duration(filepath: str) -> Duration:
    cmd = ["ffprobe", "-v", "error", "-show_entries", "format=duration",
           "-of", "default=noprint_wrappers=1:nokey=1", "-sexagesimal", filepath]
    
    res = subprocess.run(cmd, capture_output=True)
    if res.returncode != 0:
        if os.path.exists(filepath):
            #no se pudo obtener se elimina archivo
            os.remove(filepath)
        logger.debug("get_video_duration for %s failed %s, !!file removed" % (filepath, str(res)))
        return None

    hour, minute, seconds = res.stdout.decode('utf-8').strip().split(":")
    return Duration(int(hour), int(minute), int(float(seconds)))

def generate_video_name(year, month, day, hour, minute, second, ip, suffix='', prefix=''):
    return '{prefix}{ip}-{year}-{month}-{day}-{hour}-{minute}-{second}{suffix}.mp4'.format(ip=ip,
                                                                                           year=year,
                                                                                           month=month,
                                                                                           day=day,
                                                                                           hour=hour,
                                                                                           minute=minute,
                                                                                           second=second,
                                                                                           prefix=prefix,
                                                                                           suffix=suffix)

def suffix_path(filepath: str) -> int:
    for idx in range(1, 1000):
        if not os.path.exists(filepath + ".%d" % (idx)):
            return "%s.%d" % (filepath, idx)
    return 1


def dvr_dir_path(time: datetime) -> str:
    return os.path.join(config['download_dir'],
                        str(time.year),
                        str(time.month),
                        str(time.day),
                        config['dvrname'])

def mkdir_base_dir(time: datetime) -> str:
    dir_path = dvr_dir_path(time)
    os.makedirs(dir_path, exist_ok=True)
    return dir_path

def dvr_config_path() -> str:
    return os.path.join(config['download_dir'],
                        config['dvrname'] + '-config.pickle')

def dvr_config_load() -> dict:
    configpath = dvr_config_path()
    try:
        return pickle.load(open(configpath, 'rb'))
    except FileNotFoundError:
        return {'now': datetime.now()}
    
def dvr_config_save(data):
    configpath = dvr_config_path()
    try:
        pickle.dump(data, open(configpath, 'wb'))
    except FileNotFoundError:
        return False

    return True

def calculate_video_path(video_path: str, start_time: datetime, duration_expected_secs: int) -> str or None:
    duration = get_video_duration(video_path)

    if not duration:
        raise RuntimeError("can't get video duration for %s" % (video_path))

    if duration_expected_secs \
       and duration.to_seconds() >= duration_expected_secs:
        return start_time, None

    if duration.to_seconds() == 0:
        return start_time, video_path

    logger.debug('calculate_video_path video_path=%s,start_time=%s,duration_expected_secs=%d,duration=%d' % (video_path, start_time, duration_expected_secs, duration.to_seconds()))

    start_time = start_time + timedelta(seconds=duration.to_seconds())
    video_name = generate_video_name(start_time.year,
                                     start_time.month,
                                     start_time.day,
                                     start_time.hour,
                                     start_time.minute,
                                     start_time.second,
                                     IP,
                                     prefix=config['prefix'])
    video_path = os.path.join(mkdir_base_dir(start_time), video_name)
    if os.path.exists(video_path):
        return calculate_video_path(video_path,
                                    start_time,
                                    duration_expected_secs - duration.to_seconds())
    return (start_time, video_path)

# retorna True si descargado todo el SCHEDULE del TODAY
# indicado
def download_by_fragments(schedule, today=datetime.now()):
    start_date = date.fromisoformat(schedule['start-date'])
    start_time = datetime.combine(start_date,
                                  dttime(schedule['start-hour'], schedule['start-minute']))
        
    stop_date = date.fromisoformat(schedule['stop-date'])
    # TODO los despachos tienen ultimo minuto como 60
    schedule['stop-minute'] = min(59, schedule['stop-minute'])
    schedule['stop-minute'] = max(0, schedule['stop-minute'])    
    stop_time = datetime.combine(start_date,
                                 dttime(schedule['stop-hour'], schedule['stop-minute']))

    files_in_server = get_filenames(SDK, userid,
                                    config['sdk']['channel'],
                                    start_time, stop_time)
    files_in_server.sort(key = lambda vals: vals[2])
    logger.debug("Files in server: %d by schedule %s" % (len(files_in_server), str(schedule)))

    downloaded_all = []
    for (srcfile, srcsize, src_starttime, src_stoptime) in files_in_server:
        start_time = max(src_starttime, start_time)
        src_stop_time = min(src_stoptime, stop_time)
        if start_time == stop_time:
            break


        video_name = generate_video_name(start_time.year,
                                         start_time.month,
                                         start_time.day,
                                         start_time.hour,
                                         start_time.minute,
                                         start_time.second,
                                         IP,
                                         prefix=config['prefix'])
        video_path = os.path.join(mkdir_base_dir(start_time), video_name)
        file_begin(video_path)
        duration_expected_secs = (src_stop_time - start_time).total_seconds()

        #MACHETE porque src_stop_time >= start_time algunas veces?
        src_stop_time = max(src_stop_time, start_time)
        start_time = min(src_stop_time, start_time)
        
        logger.debug("trying download from %s to %s" % (start_time, src_stop_time))

        if os.path.exists(video_path):
            logger.debug("%s exists generating new path" % (video_path))
            duration_file = get_video_duration(video_path)

            if not duration_file:
                logger.warn("%s can't get duration please verify file" % (video_path))
                file_commit(video_path)
                continue

            if duration_file.to_seconds() >= duration_expected_secs:
                continue
            start_time, new_video_path = calculate_video_path(video_path, start_time, duration_expected_secs)
            if not new_video_path:
                downloaded_all.append(True)
                file_commit(video_path)
                continue
            else:
                downloaded_all.append(False)
        else:
            new_video_path = video_path

        logger.debug('downloading to %s from %s to %s' % (new_video_path, start_time, src_stop_time))
        try:
            get_file_by_time_blocking(SDK, userid,
                                      config['sdk']['channel'],
                                      start_time, src_stop_time,
                                      new_video_path)
        finally:
            duration_downloaded = get_video_duration(new_video_path)
            if duration_downloaded:
                append_log(config['dvrname'], start_time, src_stop_time, new_video_path, duration_downloaded.to_seconds())
            file_commit(video_path)
        
        start_time = src_stop_time

    if not downloaded_all:
        return False
    return all(downloaded_all)
# main


userid = login(SDK, IP, config['sdk']['port'],
               config['sdk']['username'],
               config['sdk']['password'])
if userid < 0:
    exit(error_codes['fail_login'])


assert os.access(config['download_dir'], os.W_OK), \
    "directorio de descarga no tiene permisos de escritura"

if not config['prefix']:
    config['prefix'] = ("%s-" % (get_dvr_name(SDK))).strip().upper()

if not config['dvrname']:
    config['dvrname'] = get_dvr_name(SDK).strip().upper() or DVRNAME

schedules = config['schedules']
if isinstance(schedules, str):
    if schedules == 'dvrname':
        schedules = json.load(open(os.path.join(config['download_dir'],
                                                config['dvrname'] + ".json")
                                   , "rb"))
    elif schedules == 'today':
        schedules = []
        for hour in range(3, datetime.now().hour):
            schedules.append({
                'start-date': datetime.now().date(), 'start-hour': hour, 'start-minute': 0,
                'stop-date': datetime.now().date(), 'stop-hour': (hour+1) % 24, 'stop-minute': 0
            })
    elif os.path.exists(schedules):
        schedules = json.load(open(schedules, "rb"))
    else:
        schedules = json.load(open(os.environ.get(schedules, None), "rb"))

    assert isinstance(schedules, list), "schedules debe ser una lista"

configdvr = dvr_config_load()
# cuando se cierra el programa bruscamente es pierde el ultimo video descargado
file_transaction_clean()

# en caso de no haber tomado dia
# de arranque se toma en curso
# y se continua desde este
if 'now' not in configdvr:
    configdvr['now'] = datetime.now()

logger.debug("Descargando para IP:%s DVRNAME:%s" % (IP, get_dvr_name(SDK)))
downloaded = [download_by_fragments(schedule, configdvr['now']) for schedule in schedules]
if all(map(lambda val: val == True, downloaded)):
    #configdvr['now'] += timedelta(days=1)
    configdvr['now'] = datetime.now()
    dvr_config_save(configdvr)    
    logger.debug('configdvr se actualiza a %s' % (configdvr['now']))


SDK.NET_DVR_Cleanup()
