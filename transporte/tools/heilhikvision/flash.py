#!/usr/bin/env python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

# este script dumpea datos usando el SDK y los
# vuelve a escribir
import urllib.request
import urllib.error
import sys
import os
import pickle
import struct
import argparse
import ctypes
import re
from datetime import datetime
import pyehik


parser = argparse.ArgumentParser(description='Flash dump/flash/rewrite using pyehik')
subparsers = parser.add_subparsers()
parser_dump = subparsers.add_parser('dump')
parser_dump.add_argument('--file', type=str, required=True)
parser_dump.add_argument('--ip', type=str, required=True)
parser_dump.add_argument('--user', type=str, required=True)
parser_dump.add_argument('--password', type=str, required=True)
parser_dump.set_defaults(action='dump')

parser_flash = subparsers.add_parser('flash')
parser_flash.add_argument('--file', type=str, required=True)
parser_flash.add_argument('--ip', type=str, required=True)
parser_flash.add_argument('--user', type=str, required=True)
parser_flash.add_argument('--password', type=str, required=True)
parser_flash.set_defaults(action='flash')


parser_rewrite = subparsers.add_parser('rewrite')
parser_rewrite.add_argument('--file', type=str, required=True)
dvrname_group = parser_rewrite.add_mutually_exclusive_group()
dvrname_group.add_argument('--set-dvrname', type=str, dest='setdvrname')
dvrname_group.add_argument('--get-dvrname', dest='getdvrname', action='store_true',
                           help='get dvr name')
wifissid_group = parser_rewrite.add_mutually_exclusive_group()
wifissid_group.add_argument('--set-wifi-essid', type=str, dest='setwifiessid')
wifissid_group.add_argument('--get-wifi-essid', dest='getwifiessid', action='store_true')

wifiip_group = parser_rewrite.add_mutually_exclusive_group()
wifiip_group.add_argument('--set-wifi-ip', type=str, dest='setwifiip')
wifiip_group.add_argument('--get-wifi-ip', dest='getwifiip', action='store_true')

wifinetmask_group = parser_rewrite.add_mutually_exclusive_group()
wifinetmask_group.add_argument('--set-wifi-ip-mask', type=str, dest='setwifiipmask')
wifinetmask_group.add_argument('--get-wifi-ip-mask', dest='getwifiipmask', action='store_true')

wifiipgateway_group = parser_rewrite.add_mutually_exclusive_group()
wifiipgateway_group.add_argument('--set-wifi-ip-gateway', type=str, dest='setwifiipgateway')
wifiipgateway_group.add_argument('--get-wifi-ip-gateway', dest='getwifiipgateway', action='store_true')
parser_rewrite.set_defaults(action='rewrite')

parser_time = subparsers.add_parser('time')
parser_time.add_argument('--ip', type=str, required=True)
parser_time.add_argument('--user', type=str, required=True)
parser_time.add_argument('--password', type=str, required=True)

parser_timex = parser_time.add_mutually_exclusive_group()
parser_timex.add_argument('--get-time', dest='gettime', action='store_true')
parser_timex.add_argument('--set-time', type=str, dest='settime', help="formato YYYY-MM-DDTHH:mm")
parser_time.set_defaults(action='time')

parser_activate = subparsers.add_parser('activate')
parser_activate.add_argument('--ip', type=str, required=True)
parser_activate.add_argument('--port', type=int, required=True)
parser_activate.add_argument('--password', type=str, required=True)
parser_activate.set_defaults(action='activate')

parser_formatdisk = subparsers.add_parser('format-disk')
parser_formatdisk.add_argument('--ip', type=str, required=True)
parser_formatdisk.add_argument('--port', type=int, required=True)
parser_formatdisk.add_argument('--user', type=str, required=True)
parser_formatdisk.add_argument('--password', type=str, required=True)
parser_formatdisk.add_argument('--ndisk', type=int, required=True)

parser_formatdisk.set_defaults(action='format-disk')

args = parser.parse_args()
try:
    ACTION = args.action
except AttributeError:
    parser.print_help()
    exit(-1)
    
if ACTION in ['dump', 'flash', 'rewrite']:
    FILE = args.file

if ACTION in ['dump', 'flash', 'time', 'format-disk']:
    IP = args.ip
    USER = args.user
    PASSWORD = args.password

CHANNEL = int(os.environ.get('CHANNEL', '1'))
PORT = int(os.environ.get('PORT', '8000'))


NET_DVR_GET_TRACK_CFG = 161
NET_DVR_SET_TRACK_CFG = 160
NET_DVR_GET_TRACK_PARAMCFG = 197
NET_DVR_SET_TRACK_PARAMCFG = 196

NET_VCA_GET_FACEDETECT_RULECFG_V41 = 5017
NET_VCA_SET_FACEDETECT_RULECFG_V41 = 5018

NET_DVR_GET_RULECFG_V42 = 5049

NET_DVR_GET_JPEG_CAPTURE_CFG = 1280
NET_DVR_SET_JPEG_CAPTURE_CFG = 1281

NET_DVR_GET_RECORD_CHANNEL_INFO = 6013
NET_DVR_SET_RECORD_CHANNEL_INFO= 6014
NET_DVR_GET_SCHED_CAPTURECFG = 1282
NET_DVR_SET_SCHED_CAPTURECFG = 1283
NET_DVR_GET_MOTION_HOLIDAY_HANDLE = 1242
NET_DVR_SET_MOTION_HOLIDAY_HANDLE = 1243
NET_DVR_GET_MOTION_TRACK_CFG = 3228
NET_DVR_SET_MOTION_TRACK_CFG = 3229
NET_DVR_GET_VCA_CTRLCFG = 165
NET_DVR_SET_VCA_CTRLCFG = 164

NET_DVR_GET_VCA_MASK_REGION = 167
NET_DVR_SET_VCA_MASK_REGION = 166
NET_DVR_GET_VCA_ENTER_REGION = 169
NET_DVR_SET_VCA_ENTER_REGION = 168
NET_DVR_GET_AUX_AREA = 5029
NET_DVR_SET_AUX_AREA = 5030
NET_DVR_GET_SWITCH_LAMP_CFG = 412
NET_DVR_GET_DEVICECFG_V40 = 1100
NET_DVR_SET_DEVICECFG_V40 = 1101
NET_DVR_GET_ALARMINCFG_V40 = 6181
NET_DVR_SET_ALARMINCFG_V40 = 6182
NET_DVR_GET_ALARMINCFG_V30 = 1024
NET_DVR_SET_ALARMINCFG_V30 = 1025
NET_DVR_GET_ALARMOUTCFG_V30 = 1026
NET_DVR_SET_ALARMOUTCFG_V30 = 1027
NET_DVR_GET_ALARMIN_HOLIDAY_HANDLE = 1248
NET_DVR_GET_ALARMOUT_HOLIDAY_HANDLE = 1250

NET_DVR_GET_WIFI_CFG = 307
NET_DVR_SET_WIFI_CFG = 306
NET_DVR_SET_WIFI_WORKMODE = 308
NET_DVR_GET_WIFI_WORKMODE = 309


NET_DVR_GET_EXCEPTIONCFG_V30 = 1034
NET_DVR_SET_EXCEPTIONCFG_V30 = 1035
NET_DVR_GET_RECORDCFG_V30 = 1004
NET_DVR_SET_RECORDCFG_V30 = 1005
NET_DVR_GET_CCDPARAMCFG = 1067
NET_DVR_SET_CCDPARAMCFG = 1068
NET_DVR_GET_CCDPARAMCFG_EX = 3368
NET_DVR_SET_CCDPARAMCFG_EX = 3369
NET_DVR_GET_VIDEOOUTCFG_V30 = 1028
NET_DVR_SET_VIDEOOUTCFG_V30 = 1029
NET_DVR_GET_PICCFG_V30 = 1002
NET_DVR_SET_PICCFG_V30 = 1003
NET_DVR_GET_PIC_MODEL_CFG = 6196
NET_DVR_SET_PIC_MODEL_CFG = 6197
NET_DVR_GET_COMPRESSCFG_V30 = 1040
NET_DVR_SET_COMPRESSCFG_V30 = 1041
NET_DVR_GET_RECORD_PACK = 6301
NET_DVR_SET_RECORD_PACK = 6302

PARAMETER_GET = {
    "WIFI_CFG": (NET_DVR_GET_WIFI_CFG, None, CHANNEL),
    "WIFI_WORKMODE": (NET_DVR_GET_WIFI_WORKMODE, None, CHANNEL),
    "DEVICECFG_V40": (NET_DVR_GET_DEVICECFG_V40, None, CHANNEL),
    "ALARMINCFG_V40": (NET_DVR_GET_ALARMINCFG_V30, None, 0),
    "ALARMOUTCFG_V30": (NET_DVR_GET_ALARMOUTCFG_V30, None, 0),
    "EXCEPTIONCFG_V30": (NET_DVR_GET_EXCEPTIONCFG_V30, None, CHANNEL),
    "RECORDCFG_V30": (NET_DVR_GET_RECORDCFG_V30, 508, CHANNEL),
    "CCDPARAMCFG": (NET_DVR_GET_CCDPARAMCFG, None, 0),
    "CCDPARAMCFG_EX": (NET_DVR_GET_CCDPARAMCFG_EX, 492, 0xFFFFFFFF),
    "VIDEOOUTCFG_V30": (NET_DVR_GET_VIDEOOUTCFG_V30, None, 0),
    "PICCFG_V30": (NET_DVR_GET_PICCFG_V30, 7752, CHANNEL),
    "PIC_MODEL_CFG": (NET_DVR_GET_PIC_MODEL_CFG, None, 0),
    "VCA_MASK_REGION": (NET_DVR_GET_VCA_MASK_REGION, 360,  CHANNEL),
    "VCA_ENTER_REGION": (NET_DVR_GET_VCA_ENTER_REGION, 108, CHANNEL),
    "AUX_AREA": (NET_DVR_GET_AUX_AREA, None, CHANNEL),
    "VCA_CTRLCFG": (NET_DVR_GET_VCA_CTRLCFG, 148, 0xFFFFFFFF),
    "MOTION_HOLIDAY_HANDLE": (NET_DVR_GET_MOTION_HOLIDAY_HANDLE, None, CHANNEL),
    "SCHED_CAPTURECFG": (NET_DVR_GET_SCHED_CAPTURECFG, None, CHANNEL),
    "RECORD_CHANNEL_INFO": (NET_DVR_GET_RECORD_CHANNEL_INFO, 1028, CHANNEL),
    "JPEG_CAPTURE_CFG": (NET_DVR_GET_JPEG_CAPTURE_CFG, None, CHANNEL),
    "FACEDETECT_RULECFG_V41": (NET_VCA_GET_FACEDETECT_RULECFG_V41, 596, CHANNEL),
    "TRACK_CFG": (NET_DVR_GET_TRACK_CFG, None, CHANNEL),
    "TRACK_PARAMCFG": (NET_DVR_GET_TRACK_PARAMCFG, None, CHANNEL),
    "COMPRESSCFG_V30": (NET_DVR_GET_COMPRESSCFG_V30, None, CHANNEL),
    "RECORD_PACK": (NET_DVR_GET_RECORD_PACK, None, 0xFFFFFFFF)
}


PARAMETER_SET = {
    "WIFI_CFG": (NET_DVR_SET_WIFI_CFG, 0),
    "WIFI_WORKMODE": (NET_DVR_SET_WIFI_WORKMODE, 0),
    "DEVICECFG_V40": (NET_DVR_SET_DEVICECFG_V40, 0xffffffff),
    "ALARMINCFG_V40": (NET_DVR_SET_ALARMINCFG_V30, 0),
    "ALARMOUTCFG_V30": (NET_DVR_SET_ALARMOUTCFG_V30, 0),
    "EXCEPTIONCFG_V30": (NET_DVR_SET_EXCEPTIONCFG_V30, 0xffffffff),
    "RECORDCFG_V30": (NET_DVR_SET_RECORDCFG_V30, CHANNEL),
    "CCDPARAMCFG": (NET_DVR_SET_CCDPARAMCFG, 0),
    "CCDPARAMCFG_EX": (NET_DVR_SET_CCDPARAMCFG_EX, 0),
    "VIDEOOUTCFG_V30": (NET_DVR_SET_VIDEOOUTCFG_V30, 0),
    "PICCFG_V30": (NET_DVR_SET_PICCFG_V30, CHANNEL),
    "PIC_MODEL_CFG": (NET_DVR_GET_PIC_MODEL_CFG, 0),
    "VCA_MASK_REGION": (NET_DVR_SET_VCA_MASK_REGION,  CHANNEL),
    "VCA_ENTER_REGION": (NET_DVR_SET_VCA_ENTER_REGION, CHANNEL),
    "AUX_AREA": (NET_DVR_SET_AUX_AREA, CHANNEL),
    "VCA_CTRLCFG": (NET_DVR_SET_VCA_CTRLCFG, 0xFFFFFFFF),
    "MOTION_HOLIDAY_HANDLE": (NET_DVR_SET_MOTION_HOLIDAY_HANDLE, CHANNEL),
    "SCHED_CAPTURECFG": (NET_DVR_SET_SCHED_CAPTURECFG, CHANNEL),
    "RECORD_CHANNEL_INFO": (NET_DVR_SET_RECORD_CHANNEL_INFO, CHANNEL),
    "JPEG_CAPTURE_CFG": (NET_DVR_SET_JPEG_CAPTURE_CFG, CHANNEL),
    "FACEDETECT_RULECFG_V41": (NET_VCA_GET_FACEDETECT_RULECFG_V41, CHANNEL),
    "TRACK_CFG": (NET_DVR_SET_TRACK_CFG, CHANNEL),
    "TRACK_PARAMCFG": (NET_DVR_SET_TRACK_PARAMCFG, CHANNEL),
    "COMPRESSCFG_V30": (NET_DVR_SET_COMPRESSCFG_V30, CHANNEL),
    "RECORD_PACK": (NET_DVR_SET_RECORD_PACK, 0xFFFFFFFF)
}

ISAPI_URLS = {
    '/ISAPI/Event/triggers': (),
    '/ISAPI/Event/schedules/inputs': (),
    '/ISAPI/Smart/FieldDetection': (),
    '/ISAPI/Smart/LineDetection': (),
    #'/ISAPI/Smart/DefocusDetection': (),
    #'/ISAPI/Smart/SceneChangeDetection': (),
    '/ISAPI/Smart/regionEntrance': (),
    '/ISAPI/Event/schedules/sceneChangeDetections': (),
    #'/ISAPI/Event/schedules/motionDetection': (),
    '/ISAPI/Event/schedules/fieldDetections': (),
    '/ISAPI/Event/schedules/lineDetections': (),
    #'/ISAPI/Event/schedules/PIR': (),

}

NET_DVR_GET_STREAM_RECORD_INFO = 6019
NET_DVR_GET_STREAM_RECORD_STATUS = 6021
NET_DVR_GET_FACE_DETECT = 3352
NET_DVR_GET_SCENECHANGE_DETECTIONCFG = 3356
NET_DVR_GET_ROI_DETECT = 3350
NET_DVR_GET_FIELD_DETECTION = 3362
NET_DVR_GET_VCA_DETION_CFG = 5041
NET_DVR_SET_VCA_DETION_CFG = 5040
NET_DVR_GET_JPEG_CAPTURE_CFG_V40 = 6190
NET_DVR_GET_FIELD_DETECTION = 3362



def do_time():
    SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
    assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'

    info = SDK.NET_DVR_DEVICEINFO_V30()
    userid = SDK.NET_DVR_Login_V30(IP, PORT, USER, PASSWORD, SDK.REF(info))
    
    if userid < 0:
        raise RuntimeError("Verificar usuario y clave o conexion al equipo")

    if args.gettime:
        time = SDK.NET_DVR_TIME()
        bytesReturned = SDK.DWORD()
        res = SDK.NET_DVR_GetDVRConfig(userid,
                                       SDK.NET_DVR_GET_TIMECFG,
                                       CHANNEL,
                                       SDK.REF(time),
                                       SDK.SIZEOF(time),
                                       SDK.REF(bytesReturned))
        if not res:
            raise RuntimeError("Fallo al obtener tiempo")
        else:
            print(time.to_datetime())
            return

    if args.settime:
        time = SDK.NET_DVR_TIME.from_datetime(datetime.strptime(args.settime,
                                                                "%Y-%m-%dT%H:%M:%S"))
        res = SDK.NET_DVR_SetDVRConfig(userid,
                                       SDK.NET_DVR_SET_TIMECFG,
                                       CHANNEL,
                                       SDK.REF(time),
                                       SDK.SIZEOF(time))

        if not res:
            raise RuntimeError("Fallo al obtener tiempo")
        return



def do_dump():
    SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
    assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'


    info = SDK.NET_DVR_DEVICEINFO_V30()
    userid = SDK.NET_DVR_Login_V30(IP, PORT, USER, PASSWORD, SDK.REF(info))
    
    if userid < 0:
        raise RuntimeError("Verificar usuario y clave o conexion al equipo")

    flashdata = {}
    for name, (param, sizedata, channel) in PARAMETER_GET.items():
        defaultsize = 1024*10
        if sizedata:
            defaultsize = sizedata
        cfg = SDK.CREATE_STRING_BUFFER(defaultsize)
        bytesReturned = SDK.DWORD()
        res = SDK.NET_DVR_GetDVRConfig(userid,
                                       param,
                                       channel,
                                       cfg,
                                       SDK.SIZEOF(cfg),
                                       SDK.REF(bytesReturned))
        if not res:
            print("Fallo al obtener parametro", name)
            print("\t", SDK.NET_DVR_GetErrorMsg(None))
            continue
        else:
            print("Obtenido ", name)

            size = bytesReturned.value
            flashdata[name] = {'raw': cfg.raw[0:size], 'size': size}
    


    default_group = (SDK.NET_DVR_CHANNEL_GROUP * 1)()
    default_group[0].dwChannel = 1
    default_group[0].dwGroup = 0
    default_group[0].dwSize = SDK.SIZEOF(default_group)

    #default_group.dwSize = SDK.SIZEOF(default_group)
    class NET_VCA_RECT(ctypes.Structure):
        _fields_ = [
            ("fX", SDK.FLOAT),
            ("fY", SDK.FLOAT),
            ("fWidth", SDK.FLOAT),
            ("fHeight", SDK.FLOAT)
        ]

    class ROI_DETECT_UNION(ctypes.Union):
        class NET_DVR_ROI_TRACK_RECT_CFG(ctypes.Structure):
            _fields_ = [
                ("byEnableTrackRoi", SDK.BYTE),
                ("byImageQualityLevel", SDK.BYTE),
                ("byModeType", SDK.BYTE),
                ("byRes", SDK.BYTE * 509)
            ]
            
        class NET_DVR_ROI_FIX_RECT_CFG(ctypes.Structure):
            _fields_ = [
                ("byEnableFixRoi", SDK.BYTE),
                ("byImageQualityLevel", SDK.BYTE),
                ("byRes", SDK.BYTE * 2),
                ("szFixRoiName", SDK.BYTE * SDK.NAME_LEN),
                ("struRoiRect", NET_VCA_RECT)
            ]

        _fields_ = [
            ("uLen", SDK.DWORD * 128),
            ("strRoiFixRectCfg", NET_DVR_ROI_FIX_RECT_CFG),
            ("strRoiTrackRectCfg", NET_DVR_ROI_TRACK_RECT_CFG)
        ]


    class ROI_DETECT_CFG(ctypes.Structure):
        _fields_ = [
            ("dwSize", SDK.DWORD),
            ("dwStreamType", SDK.DWORD),
            ("byRoidDetectType", SDK.BYTE),
            ("byRes", SDK.BYTE * 3),
            ("uRoiDetectInfo", ROI_DETECT_UNION),
            ("byRes1", SDK.BYTE * 36)
        ]
        

    class NET_DVR_STREAM_INFO(ctypes.Structure):
        _fields_ = [
            ("dwSize", SDK.DWORD),
            ("byID", SDK.BYTE * 32),
            ("dwChannel", SDK.DWORD),
            ("byRes", SDK.BYTE * 32)
        ]
        
    class NET_DVR_MULTI_STREAM_COMPRESSIONCFG_COND(ctypes.Structure):
        _fields_ = [
            ("dwSize", SDK.DWORD),
            ("struStreamInfo", NET_DVR_STREAM_INFO),
            ("dwStreamType", SDK.DWORD),
            ("byRes", SDK.BYTE * 32)
        ]
        
    class ROI_DETECT_COND(ctypes.Structure):
        _fields_ = [
            ("dwSize", SDK.DWORD),
            ("dwRoiID", SDK.DWORD),
            ("struMultiStreamCfg", NET_DVR_MULTI_STREAM_COMPRESSIONCFG_COND),
            ("byRoiDetectType", SDK.BYTE),
            ("byRoiDetectTrackType", SDK.BYTE),
            ("byRes", SDK.BYTE)
        ]

    streainfo = NET_DVR_STREAM_INFO()
    streainfo.dwChannel = 1
    streainfo.dwSize = SDK.SIZEOF(streainfo)

    streacond = NET_DVR_MULTI_STREAM_COMPRESSIONCFG_COND()
    streacond.struStreamInfo = streainfo
    streacond.dwSize = SDK.SIZEOF(streacond)
    roicond = (ROI_DETECT_COND * 1)()
    roicond[0].dwRoiID = 0
    roicond[0].byRoiDetectTrackType = 2
    roicond[0].dwSize = SDK.SIZEOF(roicond[0])

    DEVICECONFIG_GET = {
        "VCA_DETION_CFG": (NET_DVR_GET_VCA_DETION_CFG, 1468, default_group),
        "FIELD_DETECTION": (NET_DVR_GET_FIELD_DETECTION, 1696, default_group),
        "FACE_DETECT": (NET_DVR_GET_FACE_DETECT, 892, default_group),
        "SCENECHANGE_DETECTIONCFG": (NET_DVR_GET_SCENECHANGE_DETECTIONCFG, 956, default_group),
        "JPEG_CAPTURE_CFG_V40": (NET_DVR_GET_JPEG_CAPTURE_CFG_V40, 100148, default_group),
        "STREAM_RECORD_INFO": (NET_DVR_GET_STREAM_RECORD_INFO, 512, streainfo),
        "STREAM_RECORD_STATUS": (NET_DVR_GET_STREAM_RECORD_STATUS, 20, streainfo)
    }
    dwCount = 1
    statusListType = SDK.DWORD * 1
    for name, (param, sizebuf, arg) in DEVICECONFIG_GET.items():
        statusList = statusListType()
        statusList[0] = 0
        cfg = SDK.CREATE_STRING_BUFFER(sizebuf)
        res = SDK.NET_DVR_GetDeviceConfig(userid,
                                          param,
                                          dwCount,
                                          SDK.REF(arg),
                                          SDK.SIZEOF(arg),
                                          SDK.REF(statusList),
                                          SDK.REF(cfg),
                                          SDK.SIZEOF(cfg))
        if not res:
            print("Fallo al obtener parametro", name)
            print("\t", SDK.NET_DVR_GetErrorMsg(None))
            continue
        else:
            print("Obtenido ", name)
            size = bytesReturned.value
            flashdata[name] = {
                'param': param,
                'count': dwCount,
                'in_raw': bytes(SDK.CAST(arg, SDK.BYTEP)[:SDK.SIZEOF(arg)]),
                'in_size': SDK.SIZEOF(arg),
                'raw': cfg.raw[0:size],
                'size': size}


    with open(FILE, 'wb') as f:
        pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)


def do_flash():
    SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
    assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'


    info = SDK.NET_DVR_DEVICEINFO_V30()
    userid = SDK.NET_DVR_Login_V30(IP, PORT, USER, PASSWORD, SDK.REF(info))
    
    if userid < 0:
        raise RuntimeError("Verificar usuario y clave o conexion al equipo")

    with open(FILE, 'rb') as f:
        flashdata = pickle.load(f)
        for name, data in flashdata.items():
            print("Escribiendo ", name)
            if name in PARAMETER_SET:
                cmd, channel = PARAMETER_SET[name]
                assert len(data['raw']) == data['size'], 'incosistencia en tamanos %d != %d' % (len(data['raw']),
                                                                                                data['size'])
                buff = SDK.CREATE_STRING_BUFFER(data['raw'], data['size'])
                res = SDK.NET_DVR_SetDVRConfig(userid,
                                               cmd,
                                               channel,
                                               buff,
                                               data['size'])
                if not res:
                    print("Fallo al escribir", name)
                    print("\t", SDK.NET_DVR_GetErrorMsg(None))
                    continue
                else:
                    print("Escrito correctamente: ", name)

            DEVICECONFIG_SET = {
                "JPEG_CAPTURE_CFG_V40": (NET_DVR_GET_JPEG_CAPTURE_CFG_V40, None),
                "SCENECHANGE_DETECTIONCFG": (NET_DVR_GET_SCENECHANGE_DETECTIONCFG, None),
            }

            statusListType = SDK.DWORD * 1
            statusList = statusListType()
            statusList[0] = 0
            if name in DEVICECONFIG_SET:
                cmd, _arg = DEVICECONFIG_SET[name]
                assert len(data['raw']) == data['size'], 'incosistencia en tamanos %d != %d' % (len(data['raw']), data['size'])
                
                buff = SDK.CREATE_STRING_BUFFER(data['raw'], data['size'])
                res = SDK.NET_DVR_SetDeviceConfig(userid,
                                                  data['param'],
                                                  data['count'],
                                                  SDK.CREATE_STRING_BUFFER(data['in_raw'], data['in_size']),
                                                  data['in_size'],
                                                  SDK.REF(statusList),
                                                  SDK.CREATE_STRING_BUFFER(data['raw'], data['size']),
                                                  data['size'])
                if not res:
                    print("Fallo al escribir", name)
                    print("\t", SDK.NET_DVR_GetErrorMsg(None))
                    continue
                else:
                    print("Escrito Correctamente: ", name)

            if name in ISAPI_URLS:
                app_url = name
                # ISAPI
                #https://docs.python.org/3.1/howto/urllib2.html#id6
                base_url = "http://%(ip)s:80/" % {"ip": IP}
                pmgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
                pmgr.add_password(None, base_url, USER, PASSWORD)
                handler = urllib.request.HTTPDigestAuthHandler(pmgr)
                opener = urllib.request.build_opener(handler)
                urllib.request.install_opener(opener)
                url = base_url + app_url
                print("Escribiendo", app_url)
                try:

                    if app_url not in flashdata:
                        print("Omitido ", app_url)
                    else:
                        r = urllib.request.Request(url,
                                                   data=flashdata[app_url],
                                                   method='PUT')
                        res = urllib.request.urlopen(r).read()
                        print("\tEscrito correctamente")
                except urllib.error.HTTPError as e:
                    print("\t", e)
                    print("\tFallo al escribir")
                    
def do_rewrite():
    with open(FILE, 'rb') as f:
        flashdata = pickle.load(f)

    if args.getdvrname:
        if not 'DEVICECFG_V40' in flashdata:
            raise RuntimeError('not found DEVICECFG_V40 in FILE')
        dvrname, = struct.unpack_from('32s', flashdata['DEVICECFG_V40']['raw'], 4)
        print(dvrname.decode('utf-8'))
        return

    if args.setdvrname:
        assert len(args.setdvrname) <= 32, 'superado tamano maximo de nombre <= 32'
        with open(FILE, 'wb') as f:
            buff = ctypes.create_string_buffer(flashdata['DEVICECFG_V40']['raw'],
                                               flashdata['DEVICECFG_V40']['size'])
            # se saltan los primeros 4bytes de DWORD dwSize
            struct.pack_into('32s', buff, 4, args.setdvrname.strip().encode('utf-8'))
            flashdata['DEVICECFG_V40']['raw'] = buff.raw
            pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)

    if args.getwifiessid:
        if not 'WIFI_CFG' in flashdata:
            raise RuntimeError('not found WIFI_CFG in FILE')
        essid, = struct.unpack_from('32s', flashdata['WIFI_CFG']['raw'], 108)
        print(essid.decode('utf-8'))
        return


    if args.setwifiessid:
        assert len(args.setdvrname) <= 32, 'superado tamano maximo de essid <= 32'
        with open(FILE, 'wb') as f:
            buff = ctypes.create_string_buffer(flashdata['WIFI_CFG']['raw'],
                                               flashdata['WIFI_CFG']['size'])
            # se saltan los primeros 4bytes de DWORD dwSize
            struct.pack_into('32s', buff, 108, args.setwifiessid.strip().encode('utf-8'))
            flashdata['WIFI_CFG']['raw'] = buff.raw
            pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)

    if args.getwifiip:
        if not 'WIFI_CFG' in flashdata:
            raise RuntimeError('not found WIFI_CFG in FILE')
        ip, = struct.unpack_from('16s', flashdata['WIFI_CFG']['raw'], 4)
        print(ip.decode('utf-8'))
        return

    if args.setwifiip:
        if not re.search("\d+\.\d+\.\d+.\d+", args.setwifiip):
            raise RuntimeError("invalid IP format")
        assert len(args.setwifiip) <= 16, 'superado tamano maximo de ip <= 16'

        with open(FILE, 'wb') as f:
            buff = ctypes.create_string_buffer(flashdata['WIFI_CFG']['raw'],
                                               flashdata['WIFI_CFG']['size'])
            # se saltan los primeros 4bytes de DWORD dwSize
            struct.pack_into('16s', buff, 4, args.setwifiip.strip().encode('utf-8'))
            flashdata['WIFI_CFG']['raw'] = buff.raw
            pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)

    if args.getwifiipmask:
        if not 'WIFI_CFG' in flashdata:
            raise RuntimeError('not found WIFI_CFG in FILE')
        ip, = struct.unpack_from('16s', flashdata['WIFI_CFG']['raw'], 4 + 16)
        print(ip.decode('utf-8'))
        return

    if args.setwifiipmask:
        if not re.search("\d+\.\d+\.\d+.\d+", args.setwifiipmask):
            raise RuntimeError("invalid IP format")
        assert len(args.setwifiip) <= 16, 'superado tamano maximo de ip <= 16'
        with open(FILE, 'wb') as f:
            buff = ctypes.create_string_buffer(flashdata['WIFI_CFG']['raw'],
                                               flashdata['WIFI_CFG']['size'])
            # se saltan los primeros 4bytes de DWORD dwSize
            struct.pack_into('16s', buff, 4 + 16, args.setwifiipmask.strip().encode('utf-8'))
            flashdata['WIFI_CFG']['raw'] = buff.raw
            pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)

    if args.getwifiipgateway:
        if not 'WIFI_CFG' in flashdata:
            raise RuntimeError('not found WIFI_CFG in FILE')
        ip, = struct.unpack_from('16s', flashdata['WIFI_CFG']['raw'], 81)
        print(ip.decode('utf-8'))
        return

    if args.setwifiipgateway:
        if not re.search("\d+\.\d+\.\d+.\d+", args.setwifiipgateway):
            raise RuntimeError("invalid IP format")
        assert len(args.setwifiip) <= 16, 'superado tamano maximo de ip <= 16'
        with open(FILE, 'wb') as f:
            buff = ctypes.create_string_buffer(flashdata['WIFI_CFG']['raw'],
                                               flashdata['WIFI_CFG']['size'])
            # se saltan los primeros 4bytes de DWORD dwSize
            struct.pack_into('16s', buff, 81, args.setwifiipgateway.strip().encode('utf-8'))
            flashdata['WIFI_CFG']['raw'] = buff.raw
            pickle.dump(flashdata, f, pickle.HIGHEST_PROTOCOL)


def do_activate():
    SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
    assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'

    cfg = SDK.NET_DVR_ACTIVATECFG() 
    for idx, c in enumerate(args.password.encode('utf-8')):
        cfg.sPassword[idx] = c
    cfg.dwSize = SDK.SIZEOF(cfg)

    res = SDK.NET_DVR_ActivateDevice(args.ip.encode('utf-8'), args.port, SDK.REF(cfg))
    if not res:
        raise RuntimeError(SDK.NET_DVR_GetErrorMsg(None))


def do_format_disk():
    SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
    assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'

    info = SDK.NET_DVR_DEVICEINFO_V30()
    userid = SDK.NET_DVR_Login_V30(IP, PORT, USER, PASSWORD, SDK.REF(info))
    res = SDK.NET_DVR_FormatDisk(userid, args.ndisk)
    if res < 0:
        raise RuntimeError(SDK.NET_DVR_GetErrorMsg(None))
    SDK.NET_DVR_Logout(userid)

{
    'flash': do_flash,
    'dump': do_dump,
    'rewrite': do_rewrite,
    'time': do_time,
    'activate': do_activate,
    'format-disk': do_format_disk
}[ACTION]()

