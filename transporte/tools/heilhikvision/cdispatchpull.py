#!/usr/bin/env python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import sys
from datetime import datetime, timedelta, timezone
import os
import time
import traceback
import json

os.environ['TZ'] = 'UTC'
if hasattr(time, 'tzset'):
    time.tzset()

#minutes adicionales al intervalo
OFFSET_MINUTES=int(os.environ.get('OFFSET_MINUTES', 10))
BACKDAYS=int(os.environ.get('BACKDAYS', 1))

#zona horaria local
#-5 colombia
LOCALTZ = timezone(timedelta(hours=-5))

def usage():
    print("%s <pull dir>" % (sys.argv[0]))
    print("<pull dir>: directorio donde almacenar programaciones para cdownload.py")

try:
    PULL_DIR = os.path.abspath(sys.argv[1].strip())
except:
    usage()
    exit(-1)

assert os.access(PULL_DIR, os.W_OK), \
    "directorio no tiene permisos de escritura"

APIKEY = os.environ.get('APIKEY', '')
SERVER_URL = os.environ.get('SERVER_URL', 'http://localhost:8000/demo/heilhikvision/dispatchs')

if not APIKEY:
    raise RuntimeError('Need ENVIROMENT APIKEY')

def get_json(url, headers={}):
    #https://stackoverflow.com/questions/11322430/how-to-send-post-request
    from urllib.parse import urlencode
    from urllib.request import Request, urlopen

    request = Request(url, headers=headers)
    data = urlopen(request).read()
    if data:
        return json.loads(data)
    return []
    

def decode_record(data):
    data = data.copy()
    data['init_time'] = datetime.fromisoformat(data['init_time']).replace(tzinfo=timezone.utc)\
                                                                 .astimezone(LOCALTZ)
    data['end_time'] = datetime.fromisoformat(data['end_time']).replace(tzinfo=timezone.utc)\
                                                               .astimezone(LOCALTZ)
    return data

def loop():
    headers={'Authorization': 'Bearer %s' % (APIKEY)}
    records = []
    today = datetime.now().date()
    for day in range(0, BACKDAYS):
        qdate = today - timedelta(days=day + 1)
        records.extend(get_json(SERVER_URL +"?date=" + qdate.isoformat(),
                                 headers=headers))
    records.extend(get_json(SERVER_URL, headers=headers))
    schedules = {}
    for record in records:
        if not record['end_time']:
            continue
        record = decode_record(record)
        record['init_time'] -= timedelta(minutes=OFFSET_MINUTES)
        record['end_time'] += timedelta(minutes=OFFSET_MINUTES)
        schedule = {
            'start-date': record['init_time'].date().isoformat(),
            'start-hour': record['init_time'].hour,
            'start-minute': record['init_time'].minute,
            'stop-date': record['end_time'].date().isoformat(),
            'stop-hour': record['end_time'].hour,
            'stop-minute': record['end_time'].minute
        }

        if record['vehicle_code'] not in schedules:
            schedules[record['vehicle_code']] = []
        schedules[record['vehicle_code']].append(schedule)

    for code, schedules in schedules.items():
        schedule_path = os.path.join(PULL_DIR, '%s.json' % (code))
        with open(schedule_path, 'w') as f:
            json.dump(schedules, f)

while True:
    try:
        loop()
    except Exception as e:
        print(traceback.format_exc())
        time.sleep(60 * 5)
    time.sleep(60)
