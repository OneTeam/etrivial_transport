# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import sys
import os
import os.path
from datetime import datetime
import time

import pyehik

IP=sys.argv[1]
PORT=8000
USERNAME=os.environ.get('HIK_USERNAME', 'admin')
PASSWORD=os.environ.get('HIK_PASSWORD', 'admin')
CHANNEL=int(os.environ.get('HIK_CHANNEL', '1'))

def get_error_msg(sdk):
    return sdk.NET_DVR_GetErrorMsg(None)

def login(sdk, ip, port, username, password):
    info = sdk.NET_DVR_DEVICEINFO_V30()
    userid = sdk.NET_DVR_Login_V30(ip, port, username, password, sdk.REF(info))
    return (userid, info)

#### MAIN

sdk = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
assert sdk.NET_DVR_Init() == True, 'Fallo iniciar SDK'
userid, info = login(sdk, IP, PORT, USERNAME, PASSWORD)
if userid < 0:
    raise RuntimeError('fallo logeo: %s' % (get_error_msg(sdk)))

print("# INFO DEVICE %s" % (IP))

print("#DEVICECFG")
bytesReturned = sdk.DWORD()
cfg = sdk.NET_DVR_DEVICECFG_V40()
res = sdk.NET_DVR_GetDVRConfig(userid,
                               sdk.NET_DVR_GET_DEVICECFG_V40,
                               CHANNEL,
                               sdk.REF(cfg),
                               sdk.SIZEOF(cfg),
                               sdk.REF(bytesReturned))
if not res:
    raise RuntimeError(get_error_msg(sdk))

print("\t DVRNAME:", cfg.sDVRName)
print("\t SERIAL NUMBER:", cfg.sSerialNumber)

print("##NETWORK WIFI")
bytesReturned = sdk.DWORD()
cfg = sdk.NET_DVR_WIFI_CFG()
res = sdk.NET_DVR_GetDVRConfig(userid,
                               sdk.NET_DVR_GET_WIFI_CFG,
                               CHANNEL,
                               sdk.REF(cfg),
                               sdk.SIZEOF(cfg),
                               sdk.REF(bytesReturned))
if not res:
    raise RuntimeError(get_error_msg(sdk))

print("IP:", cfg.struWifiCfg.struEtherNet.sIpAddress)
print("ESSID:", cfg.struWifiCfg.sEssid)


bytesReturned = sdk.DWORD()
cfg = sdk.NET_DVR_AP_INFO_LIST()
res = sdk.NET_DVR_GetDVRConfig(userid,
                               sdk.NET_DVR_GET_AP_INFO_LIST,
                               0,
                               sdk.REF(cfg),
                               sdk.SIZEOF(cfg),
                               sdk.REF(bytesReturned))
if not res:
    raise RuntimeError(get_error_msg(sdk))

#print("DETECTANDO WIFIS:", cfg.dwCount)
#for idx, apInfo in enumerate(cfg.struApInfo):
#    if idx == cfg.dwCount:
#        break
#    print("\t", apInfo.sSsid)


print("HEALT DISK")

stdconfig = sdk.NET_DVR_STD_CONFIG()
health = sdk.NET_DVR_STORAGE_DETECTION()
stdconfig.lpInBuffer = sdk.NULL
stdconfig.lpOutBuffer = sdk.CAST(sdk.REF(health), sdk.VOIDP)
stdconfig.dwOutSize = sdk.SIZEOF(health)
res = sdk.NET_DVR_GetSTDConfig(userid,
                               sdk.NET_DVR_GET_STORAGEDETECTION_STATE,
                               sdk.REF(stdconfig))
healt_state = {
    0: 'Good',
    1: 'Not Good',
    2: 'Damaged',
    3: 'Unknown'
}
print("\tHEALTH STATE:", healt_state[health.byHealthState])
print("\tBAD BLOCKS:", health.wBadBlocks)
print("\tREMANINING LIFE:", health.byRemainingLife)
if not res:
    raise RuntimeError(get_error_msg(sdk))


confile = "%s.cfg" % (IP)

print("INTENTANDO DESCARGAR CONFIGURACION DEL DISPOSITIVO con NET_DVR_GetConfigFile")
print("SaveConfig")
if not sdk.NET_DVR_SaveConfig(userid):
    print("\t", get_error_msg(sdk))
res = sdk.NET_DVR_GetConfigFile(userid, confile)
if not res:
    print("\t", get_error_msg(sdk))
else:
    print("DESCARGADO ARCHIVO CONFIGURACION:", confile)

print("INTENTANDO DESCARGAR CONFIGURACION DEL DISPOSITIVO con NET_DVR_GetConfigFile_V30")
confile = "%s.cfg" % (IP)
confbuf = sdk.CREATE_STRING_BUFFER(6000 * 60)
retsize = sdk.DWORD()
res = sdk.NET_DVR_GetConfigFile_V30(userid,
                                confbuf,
                                sdk.SIZEOF(confbuf),
                                sdk.REF(retsize))
if not res:
    print("\t", get_error_msg(sdk))
else:
    print("DESCARGADO ARCHIVO CONFIGURACION:", confile)
    with open(confile, "wb") as f:
        f.write(confbuf.raw)

print("INTENTANDO DESCARGAR CONFIGURACION DEL DISPOSITIVO con NET_DVR_GetConfigFile_EX")
confile = "%s.cfg" % (IP)
confbuf = sdk.CREATE_STRING_BUFFER(6000 * 60)
retsize = sdk.DWORD()
res = sdk.NET_DVR_GetConfigFile_EX(userid,
                                confbuf,
                                sdk.SIZEOF(confbuf))
if not res:
    print("\t", get_error_msg(sdk))
else:
    print("DESCARGADO ARCHIVO CONFIGURACION:", confile)
    with open(confile, "wb") as f:
        f.write(confbuf.raw)


print("CAPABILITIES DEVICE")
caps = sdk.CREATE_STRING_BUFFER(6000)
res = sdk.NET_DVR_GetDeviceAbility(userid,
                                   sdk.DEVICE_SOFTHARDWARE_ABILITY,
                                   sdk.NULL,
                                   0,
                                   caps,
                                   sdk.SIZEOF(caps))
if not res:
    raise RuntimeError(get_error_msg(sdk))
else:
    filename = "%s.caps" % (IP)
    with open(filename, "wb") as f:
        f.write(caps.raw)
    print("\t Ver archivo: ", filename)
                                   

print("# CONSULTA EVENTOS")
starttime = datetime.now()
stoptime = datetime.now()
lh = sdk.NET_DVR_FindDVRLog_V30(userid,
                                0,
                                sdk.MAJOR_ALARM,
                                sdk.MINOR_ALARM_IN,
                                sdk.NET_DVR_TIME.from_datetime(starttime),
                                sdk.NET_DVR_TIME.from_datetime(stoptime),
                                False)
if lh < 0:
    print("\t", get_error_msg(sdk))
else:
    logdata = sdk.NET_DVR_LOG_V30()
    res = sdk.NET_DVR_FindNextLog_V30(lh, sdk.REF(logdata))
    if res != 1000:
        print("\tno se encontraron eventos", res)
    else:
        print("\t strLogTime:", logdata.strLogTime.to_datetime())
        print("\t dwAlarmInPort:", logdata.dwAlarmInPort)
        print("\t dwAlarmOutPort:", logdata.dwAlarmOutPort)
        print("\t dwInfoLen:", logdata.dwInfoLen)
        print("\t sInfo:", logdata.sInfo)

    sdk.NET_DVR_FindLogClose_V30(lh)


print("# PROGRAMACION DE CAPTURA")
cfg = sdk.NET_DVR_SCHED_CAPTURECFG()
res = sdk.NET_DVR_GetDVRConfig(userid,
                               sdk.NET_DVR_GET_SCHED_CAPTURECFG,
                               CHANNEL,
                               sdk.REF(cfg),
                               sdk.SIZEOF(cfg),
                               sdk.REF(bytesReturned))
if not res:
    print("\t", get_error_msg(sdk))
else:
    print("\t ENABLED:", cfg.byEnable)
    print("\t RECORDER DURATION:", cfg.dwRecorderDuration)
    for idxday, cday in enumerate(cfg.struCaptureDay):
        print("\t IDX DAY:", idxday)
        print("\t ALLDAY CAPTURE:", cday.byAllDayCapture)
        print("\t CAPTURE TYPE:", cday.byCaptureType)


print("# LISTA DE VIDEOS")
conf = sdk.NET_DVR_FILECOND()
conf.lChannel = CHANNEL
conf.dwFileType = 0xff
conf.dwIsLocked = 0xff
conf.struStartTime = sdk.NET_DVR_TIME.from_datetime(datetime.combine(datetime.now(), datetime.min.time()))
conf.struStopTime = sdk.NET_DVR_TIME.from_datetime(datetime.combine(datetime.now(), datetime.max.time()))

lhandle = sdk.NET_DVR_FindFile_V30(userid, sdk.REF(conf))
if lhandle < 0:
    raise RuntimeError(get_error_msg(sdk))

videodata = []
maxtests = 100
while True:
    data = sdk.NET_DVR_FINDDATA_V30()
    # se extraer pero aun es recolectado
    #refData = sdk.REF(data)
    res = sdk.NET_DVR_FindNextFile_V30(lhandle, sdk.REF(data))
    if res == 1002:
        if maxtests > 0:
            maxtests -= 1
        else:
            break
        print("\t BUSCANDO")
        time.sleep(.5)
        continue

    if res != sdk.NET_DVR_FILE_SUCCESS:
        break
    print("\t ##DATA FOUND\n")
    print("\t FILENAME: ", data.sFileName)
    print("\t FILESIZE: ", data.dwFileSize)
    print("\t START TIME: ", data.struStartTime.to_datetime())
    print("\t STOP TIME: ", data.struStopTime.to_datetime())
    print("\t FILE TYPE: ", data.byFileType)
    videodata.append(data)

def get_file_by_time_blocking(sdk, userid, channel, starttime, stoptime, outfile):
    itime = sdk.NET_DVR_TIME.from_datetime(starttime)
    otime = sdk.NET_DVR_TIME.from_datetime(stoptime)
    fhandle = sdk.NET_DVR_GetFileByTime(userid, channel,
                                        sdk.REF(itime), sdk.REF(otime), outfile)
    if fhandle < 0:
        raise RuntimeError(get_error_msg(sdk))

    if not sdk.NET_DVR_PlayBackControl(fhandle, sdk.NET_DVR_PLAYSTART, 0, None):
        raise RuntimeError(get_error_msg(sdk))

    while True:
        pos = sdk.NET_DVR_GetDownloadPos(fhandle)
        if pos >= 100 or pos < 0:
            break
        time.sleep(.5)
    return True
