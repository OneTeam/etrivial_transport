# -*- coding: utf-8 -*-

"""Top-level package for pyehik."""

__author__ = """Jovany Leandro G.C"""
__email__ = 'bit4bit@riseup.net'
__version__ = '0.1.2'
import ctypes
import os.path

def load_sdk(pathname):
    HC = ctypes.cdll.LoadLibrary(pathname)
    
    from . import sdk
    sdk.HC = HC
    libdirpath = os.path.dirname(pathname)
    sLocalsdkpath = sdk.NET_DVR_LOCAL_SDK_PATH()
    if len(libdirpath) > sdk.NET_SDK_MAX_FILE_PATH:
        raise RuntimeError("path too long")
    sLocalsdkpath.sPath = libdirpath.encode('utf-8')
    sdk.NET_DVR_SetSDKInitCfg(sdk.NET_SDK_INIT_CFG_SDK_PATH,
                              sdk.REF(sLocalsdkpath))
    return sdk

