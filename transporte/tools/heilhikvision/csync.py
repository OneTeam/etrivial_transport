#!/usr/bin/env python
# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

# demonio para sincronizar datos con heilhikvision_backend
from datetime import datetime, timezone, timedelta
import time
import sys
import glob
import os
import os.path
import pickle
import traceback
import sqlite3

from contextlib import contextmanager

os.environ['TZ'] = 'UTC'
if hasattr(time, 'tzset'):
    time.tzset()

#zona horaria local
#-5 colombia
LOCALTZ = timezone(timedelta(hours=-5))

def usage():
    print("%s <download dir>" % (sys.argv[0]))
try:
    DOWNLOAD_DIR=sys.argv[1].strip()
except:
    usage()
    exit(-1)

APIKEY = os.environ.get('APIKEY', '')
MACHINE = os.environ.get('MACHINE', '')
SERVER_URL = os.environ.get('SERVER_URL', 'http://localhost:8000/demo/heilhikvision/videolog/' + MACHINE)

assert os.access(DOWNLOAD_DIR, os.W_OK), \
    "directorio de descarga no tiene permisos de escritura"

if not MACHINE:
    raise RuntimeError('Need ENVIRONMENT VAR MACHINE')
if not APIKEY:
    raise RuntimeError('Need ENVIRONMENT VAR APIKEY')

if not DOWNLOAD_DIR or not os.path.exists(DOWNLOAD_DIR):
    raise RuntimeError('Invalid DOWNLOAD_DIR')


CACHEFILE = os.path.join(DOWNLOAD_DIR, 'cache.db')
CONN = sqlite3.connect(CACHEFILE)

c = CONN.cursor()
# TODO solo se permite enteros
# se uso BLOB con pickle pero generaba correcion al cerrase mal el programa
c.execute("CREATE TABLE IF NOT EXISTS dict(key text PRIMARY KEY, val INTEGER)");


def get_dict_from_db():
    c = CONN.cursor()
    res = {}
    for row in c.execute("SELECT key, val FROM dict"):
        res[row[0]] = row[1]
    return res

def update_dict_to_db(data: dict):
    c = CONN.cursor()
    to_insert = [(k, v) for k,v in data.items()]
    c.executemany("INSERT INTO dict (key , val) VALUES (?, ?) ON CONFLICT(key) DO UPDATE SET val=excluded.val", to_insert);
    CONN.commit()
        
CACHE = get_dict_from_db()
STATUS = {'logfile': '', 'fields': '', 'seek': 0}

@contextmanager
def transaction(logfile):
    def flush(seekpos):
        CACHE[logfile] = seekpos
        update_dict_to_db(CACHE)
    try:
        if logfile not in CACHE:
            CACHE[logfile] = {}
        yield (CACHE[logfile], flush)
    finally:
        pass


def post(url, post_fields, headers = {}):
    #https://stackoverflow.com/questions/11322430/how-to-send-post-request
    from urllib.parse import urlencode
    from urllib.request import Request, urlopen

    request = Request(url, urlencode(post_fields).encode())
    for k,v in headers.items():
        request.add_header(k, v)
    return urlopen(request)


def loop():
    #consulta segun estructura /YYYY/MM/DD/DVRNAME/cdownload.log
    query = os.path.abspath(DOWNLOAD_DIR) + '/*/*/*/*/cdownload.log'
    logs = glob.glob(query)
    headers = ['dvrname',
               'starttime',
               'stoptime',
               'video_path',
               'duration',
               'ip',
               'filesize']

    for logfile in logs:

        with open(logfile, 'r') as f:
            with transaction(logfile) as params:
                cacheseek, flush = params
                seekpos = cacheseek or 0
                f.seek(seekpos)
                while True:
                    line = f.readline()
                    if line == '':
                        break
                    nfields = len(line.split(','))
                    #se mantiene compatiblidad con archivo
                    for _ in range(0, len(headers) - nfields):
                        line += ","
                    fields = dict(zip(headers,
                                      map(lambda v: v.strip('|').strip(), line.split(','))))
                    STATUS['fields'] = fields.copy()
                    STATUS['logfile'] = logfile
                    STATUS['seek'] = seekpos

                    #se asume en zona LOCALTZ y se lleva a UTC
                    starttime = datetime.fromisoformat(fields['starttime']).replace(tzinfo=LOCALTZ)\
                                                                           .astimezone(timezone.utc)
                    stoptime = datetime.fromisoformat(fields['stoptime']).replace(tzinfo=LOCALTZ)\
                                                                         .astimezone(timezone.utc)
                    fields['starttime'] = starttime.isoformat()
                    fields['stoptime'] = stoptime.isoformat()
                    if not fields['filesize']:
                        if os.path.exists(fields['video_path']):
                            fields['filesize'] = os.path.getsize(fields['video_path']);

                    resp = post(SERVER_URL, fields, headers={'Authorization': 'Bearer %s' % (APIKEY)})
                    if resp.status != 200:
                        raise RuntimeError('fallo al registrar en servidor')
                    flush(f.tell())

# MAIN


while True:
    try:
        loop()
    except Exception as e:
        print("LOGFILE %s SEEK %d DATA %s" % (STATUS['logfile'],
                                              STATUS['seek'],
                                              STATUS['fields']))
        print(traceback.format_exc())
        time.sleep(60 * 5)
    finally:
        CONN.rollback()
        CONN.close()
    time.sleep(60)
