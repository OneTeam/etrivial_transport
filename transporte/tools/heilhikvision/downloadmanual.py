# This file is part of heilhikvision.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import os
import argparse
import logging
import re
from datetime import datetime
import time

logger = logging.getLogger('downloadmanual')
if os.environ.get('DEBUG', None):
    logging.basicConfig(level=logging.DEBUG)


import pyehik

PORT = int(os.environ.get('PORT', '8000'))
USERNAME = os.environ.get('USERNAME', 'admin')
PASSWORD= os.environ.get('PASSWORD', 'admin')
CHANNEL= int(os.environ.get('CHANNEL', '1'))


parser = argparse.ArgumentParser(description='Download manual')
parser.add_argument('--ip', type=str, required=True)
parser.add_argument('--since', type=str, required=True,
                    help='yyyy-mm-ddTHH:MM')
parser.add_argument('--to', type=str, required=True,
                    help='yyyy-mm-ddTHH:MM')
parser.add_argument('--file', type=str, required=True)

args = parser.parse_args()

if not re.search("\d+\.\d+\.\d+.\d+", args.ip):
    raise ValueError("invalid IP format")

if not re.search("\d+-\d+-\d+T\d+:\d+:\d+", args.since):
    raise ValueError('Invalid SINCE format')

if not re.search("\d+-\d+-\d+T\d+:\d+:\d+", args.to):
    raise ValueError('Invalid TO format')

SDK = pyehik.load_sdk(os.environ.get('HCNETSDK', './libhcnetsdk.so'))
assert SDK.NET_DVR_Init() == True, 'Fallo iniciar SDK'

def login(sdk, ip, port, username, password):
    info = sdk.NET_DVR_DEVICEINFO_V30()
    return sdk.NET_DVR_Login_V30(ip, port, username, password, sdk.REF(info))



userid = login(SDK, args.ip, PORT,
               USERNAME, PASSWORD)

if userid < 0:
    raise RuntimeError("Verificar usuario y clave o conexion al equipo")

time_at = datetime.strptime(args.since, "%Y-%m-%dT%H:%M:%S")
time_to = datetime.strptime(args.to, "%Y-%m-%dT%H:%M:%S")


# TODO: usar la modulo utils genera fallo de segmentacion porque?
# se copia tal cual aca y no genera.
# ahi una pregunta con este, y es como se comporta cuando
# el video es por eventos?
def get_file_by_time_blocking(sdk, userid, channel, starttime, stoptime, outfile):
    itime = sdk.NET_DVR_TIME.from_datetime(starttime)
    otime = sdk.NET_DVR_TIME.from_datetime(stoptime)
    fhandle = sdk.NET_DVR_GetFileByTime(userid, channel,
                                        sdk.REF(itime), sdk.REF(otime), outfile)
    if fhandle < 0:
        raise RuntimeError(get_error_msg(sdk))

    if not sdk.NET_DVR_PlayBackControl(fhandle, sdk.NET_DVR_PLAYSTART, 0, None):
        raise RuntimeError(get_error_msg(sdk))

    while True:
        pos = sdk.NET_DVR_GetDownloadPos(fhandle)
        if pos >= 100 or pos < 0:
            break
        time.sleep(.5)
    return True

get_file_by_time_blocking(SDK, userid, CHANNEL,
                          time_at, time_to, args.file)
