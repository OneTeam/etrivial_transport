# This file is part of webservis_gammu.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from flask import Flask
from flask_spyne import Spyne
from flask_httpauth import HTTPBasicAuth
from spyne.protocol.http import HttpRpc
from spyne.protocol.soap import Soap11
from spyne.protocol.json import JsonDocument
from spyne.model.primitive import Unicode, Integer, DateTime
from spyne.model.complex import Iterable, ComplexModel, Array
import records
import psycopg2
import pymysql
import gammu
import logging



app = Flask(__name__)

spyne = Spyne(app)

mysql = "mysql+pymysql"
postgres = "postgres"
usuario_db = "smsd"
clave_db = "G$MmU@))&*"
url_db = "localhost"
nombre_db = "smsd"

id_buscar = 37588 


            


class Arreglos_enviar_mensaje(ComplexModel):
    id_envio_mensaje = Integer

class Arreglos_estado_mensaje(ComplexModel):    
    Tabla = Unicode
    UpdatedInDB = DateTime
    InsertIntoDB = DateTime
    SendingDateTime = DateTime
    Text = Unicode
    DestinationNumber = Integer
    Coding = Unicode
    Class   = Integer
    TextDecoded = Unicode
    ID = Integer
    SenderID = Integer
    Status = Unicode
    CreatorID = Unicode
    StatusCode = Unicode
    
class Arreglos_estado_modem(ComplexModel):
    Received = Integer
    BatterPercent = Integer
    NetworkSignal = Integer
    Failed = Integer
    Client = Unicode
    IMEI = Integer
    PhoneID = Integer
    Sent = Integer
    



class Respuesta_enviar_mensaje(ComplexModel):
    Arreglos_enviar_mensaje = Arreglos_enviar_mensaje.customize(max_occurs='unbounded')
class Respuesta_estado_modem(ComplexModel):    
    Arreglos_estado_modem = Arreglos_estado_modem.customize(max_occurs='unbounded')
class Respuesta_estado_mensaje(ComplexModel):    
    Arreglos_estado_mensaje = Arreglos_estado_mensaje.customize(max_occurs='unbounded')    
    

    
class Respuesta(ComplexModel):
    __namespace__ = 'tns'
    dummy_str = Unicode(min_occurs=0, max_occurs=1, nillable=False)
    dummy_num = Integer(min_occurs=0, max_occurs=1, nillable=True)
    codigo_mensaje = Integer(min_occurs=0, max_occurs=1, nillable=True)     

class JsonService(spyne.Service):
    __service_url_path__ = '/json'
    __in_protocol__ = HttpRpc(validator='soft')
    __out_protocol__ = JsonDocument(ignore_wrappers=True)

    @spyne.srpc(Unicode, Unicode, _returns=Respuesta_enviar_mensaje)
    def enviar_mensaje(celular,texto):
        SMS_1 = {
            'Number': celular,
            'SMSC': {'Location': 1},
            'Text': texto,
            }
        conectar_modem = gammu.SMSD('/etc/gammu-smsdrc')
        enviar_mensajes = conectar_modem.InjectSMS([SMS_1])
        id_enviado = [
            Arreglos_enviar_mensaje(id_envio_mensaje =enviar_mensajes)
            ]                
        return Respuesta_enviar_mensaje(Arreglos_enviar_mensaje = id_enviado)
    
    @spyne.srpc(_returns=Respuesta_estado_modem)
    def estado_modem():
        conectar_modem = gammu.SMSD('/etc/gammu-smsdrc')
        estados_modem = conectar_modem.GetStatus()
        arreglo_estado_modem = [
            Arreglos_estado_modem(Received = estados_modem['Received'],
                     BatterPercent = estados_modem['BatterPercent'],
                     NetworkSignal = estados_modem['NetworkSignal'],
                     Failed = estados_modem['Failed'],
                     Client = estados_modem['Client'],
                     IMEI = estados_modem['IMEI'],
                     PhoneID = estados_modem['PhoneID'],
                     Sent = estados_modem['Sent'])
            ]
        
        return Respuesta_estado_modem(Arreglos_estado_modem = arreglo_estado_modem)
    
    @spyne.srpc(Integer, _returns=Respuesta_estado_mensaje)
    def estado_mensaje(id):
        conexion_db = records.Database(mysql+'://'+usuario_db+':'+clave_db+'@'+url_db+'/'+nombre_db)
        consulta_db_conteo = conexion_db.query('SELECT COUNT(*) FROM outbox;')
        seguir = False
        tabla = False
        
        for conteo in consulta_db_conteo:
            if conteo["COUNT(*)"] > 0:
                seguir = True

        if seguir == True:
            consulta_db_outbox = conexion_db.query("select * from outbox where id= '%d'" %id)
            for outbox in consulta_db_outbox:
                #print(recorrido['ID'])
                if outbox['ID'] == id:
                    estados = outbox
                    tabla = 'outbox'
        else:
            consulta_db_sentitems = conexion_db.query("select * from sentitems where id= '%d'" %id)
            for sentitems in consulta_db_sentitems:
                if sentitems['ID'] == id:
                    estados = sentitems
                    tabla = 'sentitems'
        
        if tabla != False:
		    arreglo_estado_mensaje = [
		        Arreglos_estado_mensaje(Tabla = tabla,
		                 UpdatedInDB = estados['UpdatedInDB'],
		                 InsertIntoDB = estados['InsertIntoDB'],
		                 SendingDateTime = estados['SendingDateTime'],
		                 Text = estados['Text'],
		                 DestinationNumber = estados['DestinationNumber'],
		                 Coding = estados['Coding'],
		                 Class   = estados['Class'],
		                 TextDecoded = estados['TextDecoded'],
		                 ID = estados['ID'],
		                 SenderID = estados['SenderID'],
		                 Status = estados['Status'],
		                 CreatorID = estados['CreatorID'])]
        else:
            arreglo_estado_mensaje = [
                Arreglos_estado_mensaje(Status = tabla)]            

        return Respuesta_estado_mensaje(Arreglos_estado_mensaje = arreglo_estado_mensaje)
    
class SoapService(spyne.Service):
    __service_url_path__ = '/soap'
    __in_protocol__ = Soap11(validator='lxml')
    __out_protocol__ = Soap11()
    __wsse_conf__ = {
        'username': 'admin',
        'password': '123'  # never store passwords directly in sources!
    }

    @spyne.srpc(Unicode, Unicode, _returns=Respuesta_enviar_mensaje)
    def enviar_mensaje(celular,texto):
        SMS_1 = {
            'Number': celular,
            'SMSC': {'Location': 1},
            'Text': texto,
            }
        conectar_modem = gammu.SMSD('/etc/gammu-smsdrc')
        enviar_mensajes = conectar_modem.InjectSMS([SMS_1])
        id_enviado = [
            Arreglos_enviar_mensaje(id_envio_mensaje =enviar_mensajes)
            ]                
        return Respuesta_enviar_mensaje(Arreglos_enviar_mensaje = id_enviado)
    
    @spyne.srpc(_returns=Respuesta_estado_modem)
    def estado_modem():
        conectar_modem = gammu.SMSD('/etc/gammu-smsdrc')
        estados_modem = conectar_modem.GetStatus()
        arreglo_estado_modem = [
            Arreglos_estado_modem(Received = estados_modem['Received'],
                     BatterPercent = estados_modem['BatterPercent'],
                     NetworkSignal = estados_modem['NetworkSignal'],
                     Failed = estados_modem['Failed'],
                     Client = estados_modem['Client'],
                     IMEI = estados_modem['IMEI'],
                     PhoneID = estados_modem['PhoneID'],
                     Sent = estados_modem['Sent'])
            ]
        
        return Respuesta_estado_modem(Arreglos_estado_modem = arreglo_estado_modem)
    
    @spyne.srpc(Integer, _returns=Respuesta_estado_mensaje)
    def estado_mensaje(id):
        conexion_db = records.Database(mysql+'://'+usuario_db+':'+clave_db+'@'+url_db+'/'+nombre_db)
        consulta_db_conteo = conexion_db.query('SELECT COUNT(*) FROM outbox;')
        seguir = False
        for conteo in consulta_db_conteo:
            if conteo["COUNT(*)"] > 0:
                seguir = True

        if seguir == True:
            consulta_db_outbox = conexion_db.query("select * from outbox where id= '%d'" %id)
            for outbox in consulta_db_outbox:
                #print(recorrido['ID'])
                if outbox['ID'] == id:
                    estados = outbox
                    tabla = 'outbox'
        else:
            consulta_db_sentitems = conexion_db.query("select * from sentitems where id= '%d'" %id)
            for sentitems in consulta_db_sentitems:
                if sentitems['ID'] == id:
                    estados = sentitems
                    tabla = 'sentitems'

        if tabla != False:
            arreglo_estado_mensaje = [
                Arreglos_estado_mensaje(Tabla = tabla,
                         UpdatedInDB = estados['UpdatedInDB'],
                         InsertIntoDB = estados['InsertIntoDB'],
                         SendingDateTime = estados['SendingDateTime'],
                         Text = estados['Text'],
                         DestinationNumber = estados['DestinationNumber'],
                         Coding = estados['Coding'],
                         Class   = estados['Class'],
                         TextDecoded = estados['TextDecoded'],
                         ID = estados['ID'],
                         SenderID = estados['SenderID'],
                         Status = estados['Status'],
                         CreatorID = estados['CreatorID'])]
        else:
            arreglo_estado_mensaje = [
                Arreglos_estado_mensaje(Status = tabla)]
            
        return Respuesta_estado_mensaje(Arreglos_estado_mensaje = arreglo_estado_mensaje)


         
if __name__ == '__main__':
    app.run(host='0.0.0.0')
