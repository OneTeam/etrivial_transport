#!/usr/bin/bash
# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

#sincroniza bitacora a servidor tryton

. /opt/heilhikvision-scripts/variables.sh
export SERVER_URL=${SERVER_BASE}/heilhikvision/videolog/${MACHINE}

exec -a "csync" python ${HEILHIKVISION}/csync.py ${DOWNLOAD_DIR}
