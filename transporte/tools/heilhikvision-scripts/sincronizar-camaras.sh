#!/usr/bin/bash
# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

. /opt/heilhikvision-scripts/variables.sh

function job_download {
    ip=${1}
    time=$(date)
    logfile=${LOGDIR}/${ip}.log
    lockfile=/tmp/.${ip}-lock
    
    echo "${time} job_download: descargando para ${ip}" >> ${logfile}
    timeout 60m ${HOME_SCRIPTS}/cdownload.sh ${ip} /opt/heilhikvision-scripts/schedule-por-dvrname.json &> ${logfile}
    time=$(date)
    echo "${time} job_download: finalizado papa ${ip}" >> ${logfile}

    #se adiciona cambio de hora si se puede
    bash ${HOME_SCRIPTS}/fixtime.sh ${ip} ${HIKUSER} ${HIKPASS}

    #MACHETE cdownload.sh se deberia encargar de esto que pasa?
    rm -f ${lockfile}
}

function encontrar_ips {
    while true; do
	nmap -iL ${LISTA_IPS} -sP | grep -Eo "[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+"
    done
}

function dequeue {
    echo $1
}

export -f job_download
export -f dequeue

while true; do
    encontrar_ips | parallel -j ${PULL_JOBS} dequeue {} | parallel --env HOME_SCRIPTS --env HIKUSER --env HIKPASS --env LOGDIR -j ${NJOBS_DOWNLOAD} job_download {}
    sleep 10s
done
