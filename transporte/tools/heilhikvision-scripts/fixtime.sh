#!/usr/bin/bash
# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

#cambia la hora de la camara


if [[ $# != 3 ]]; then
    echo "$0: <ip> <hikuser> <hikpass>"
    exit 1
fi


IP=$1
HIKUSER=$2
HIKPASS=$3
#segundos de diferencia
#para corregir tiempo
DIFFSECONDS=60
TOTZ="UTC"

. /opt/heilhikvision-scripts/variables.sh
export SERVER_URL=${SERVER_BASE}/heilhikvision/videolog/

#se requiere !!no quitar
set -eu
pushd $HEILHIKVISION
timecamera=`python ${HEILHIKVISION}/flash.py time --get-time --ip $IP --user $HIKUSER --password $HIKPASS`
timeserver=`TZ="$TOTZ" date "+%Y-%m-%dT%H:%M:%S"`
echo "HORA CAMARA: $timecamera"
echo "HORA SERVIDOR: $timeserver"
cat <<EOF | python -
from datetime import datetime
tc=datetime.strptime('$timecamera', '%Y-%m-%d %H:%M:%S')
ts=datetime.strptime('$timeserver', '%Y-%m-%dT%H:%M:%S')
sd=(max(tc,ts) - min(ts,tc)).seconds
print("Segundos de diferencia:", sd)
if sd > $DIFFSECONDS:
  exit(0)
exit(1)
EOF

echo "Se debe cambiar hora"
python ${HEILHIKVISION}/flash.py time --set-time "$timeserver" --ip $IP --user $HIKUSER --password $HIKPASS
