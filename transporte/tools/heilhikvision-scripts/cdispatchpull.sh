#!/usr/bin/bash
# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

. /opt/heilhikvision-scripts/variables.sh
export SERVER_URL=${SERVER_BASE}/heilhikvision/dispatchs
exec -a "cdispatchpull" python ${HEILHIKVISION}/cdispatchpull.py ${DOWNLOAD_DIR}
