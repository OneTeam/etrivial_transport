#!/usr/bin/bash
# remueve archivos en los recolectores
# requiere: parallel,md5sum,ssh

# adicionar recolectores por espacio en blanco
DESTINATIONS="heilhikvision@10.10.10.10:22#/grabaciones#/opt/idforsync"
RECORDS=/camaras/grabaciones
RECORDS_TOP=/camaras
BACKDAYS=5

function remote_delete() {
    linechk=$1
    videos=$2
    sshcmd=$3
    filepath=$(echo ${linechk} | cut -d":" -f 1)
    status=$(echo ${linechk} | cut -d":" -f 2)

    if [[ $status =~ "OK" ]]; then
	rfilepath=$(echo ${filepath} | sed "s;${RECORDS};${videos};")
        echo "ELIMINADO ${rfilepath}"
	bash -c "${sshcmd} 'rm -f ${rfilepath}'"
    i
}
export -f remote_delete
export RECORDS

for destination in ${DESTINATIONS}; do
    sshhost=$(echo ${destination} | cut -d"#" -f1)
    videos=$(echo ${destination} | cut -d"#" -f2)
    sshidentity=$(echo ${destination} | cut -d"#" -f3)
    host=$(echo ${sshhost} | cut -d":" -f1)
    port=$(echo ${sshhost} | cut -d":" -f2)
    sshcmd="'ssh -i ${sshidentity} -p ${port} ${host}'"
    
    remotesumfile=/tmp/"${host}#$(date +%Y-%m-%dT%H:%M).sum"
    localsumfile=/tmp/"localhost#$(date +%Y-%m-%dT%H:%M).sum"
    
    echo "Obteniendo MD5SUM de ${host} a ${remotesumfile}"
    ssh -i ${sshidentity} -p ${port} ${host} "find ${videos}  -name '*.mp4' -type f -mtime +${BACKDAYS} -exec md5sum {} \;" > ${remotesumfile}

    echo "Obteniendo MD5SUM de archivos remotos en servidor local ${localsumfile} y comparando"
    cat ${remotesumfile}  | sed "s;${videos};${RECORDS};" | md5sum -c | parallel --env RECORDS -j 5 remote_delete "{} ${videos} ${sshcmd}"
done
