#!/usr/bin/bash
# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

. /opt/heilhikvision-scripts/variables.sh

PYTHONBIN=/usr/bin/python
PYSCRIPT=${HEILHIKVISION}/cdownload.py
LOCKFILE=/tmp/.$1-lock

# por defecto se descarga dia
export CONFIG_PATH=${HOME_SCRIPTS}/schedules-fijo.json
export DEBUG=1
export HCNETSDK=${HEILHIKVISION}/libhcnetsdk.so
cd ${HEILHIKVISION}

function unlock {
    rm -f ${LOCKFILE}
}

function lock {
    echo $1 > ${LOCKFILE}
}

if [ -e ${LOCKFILE} ]; then
       exit 0
fi

lock
trap unlock SIGKILL SIGINT SIGTERM EXIT
${PYTHONBIN} ${PYSCRIPT} "$@"
