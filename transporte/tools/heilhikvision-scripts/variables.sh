# This file is part of heilhikvision-scripts.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

export HEILHIKVISION=/opt/heilhikvision/
export HOME_SCRIPTS=/opt/heilhikvision-scripts/

#ADICIONAR APIKEY DE USUARIO heilhikvision de tryton
export APIKEY=xxxxx
export SERVER_BASE=http://localhost:8000/tryton5
export DOWNLOAD_DIR=/grabaciones
export MACHINE=$(cat /etc/hostname)
export LOGDIR=/tmp
export LISTA_IPS=${HOME_SCRIPTS}/ips-vehiculos.csv

export NJOBS_DOWNLOAD=12
export PULL_JOBS=$(( NJOBS_DOWNLOAD * 2))

#usuario y clave de la camara
#para cambio de tiempo
export HIKUSER=admin
export HIKPASS=admin
