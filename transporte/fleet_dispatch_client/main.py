# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

__version__ = '0.6.203'

from contextlib import contextmanager
import json
import os
import glob
import datetime
import threading
import traceback
import secrets
import time
import functools
import uuid
import tarfile
import warnings

os.environ['TZ'] = 'UTC'
if hasattr(time, 'tzset'):
    time.tzset()


from dateutil import tz

from kivy.app import App
from kivy.properties import ObjectProperty, NumericProperty, BooleanProperty

from kivy.uix.settings import SettingsPanel, SettingItem
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.modalview import ModalView

from kivy.clock import Clock
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.logger import Logger
from kivy.utils import platform
from kivy.base import EventLoop

import db
import ui
import utils
import form
import operator_logic
import sync
import i18n
import provision
import db_helper
import raction

from webhook import Webhook, WebhookFleetDispatchClientBackendAdapter

# SOLO HABILITAR PARA LA COMPILACION DE DEMO
DEMO=False
DEMO_TODAY='2019-12-14'
DEMO_YESTERDAY='2019-12-13'

class LoginException(Exception):

    def __init__(self, message):
        super(LoginException, self).__init__()
        self.message = message


class LoginPanel(BoxLayout):
    """
    Autenticacion
    """
    code = ObjectProperty(None)

    def __init__(self, on_ok=None):
        super(LoginPanel, self).__init__()
        if not callable(on_ok):
            raise ValueError(i18n._('expected callback'))
        self.on_ok = on_ok

    def login(self):
        app = App.get_running_app()

        for name, profile in app.profiles.items():
            if self.code.text == profile['code']:
                app.main_layout.remove_widget(self)
                app.profile = name
                self.on_ok(app.main_layout)
                return

        raise LoginException(i18n._('invalid code'))


class PullPushAppLogic:

    def __init__(self, app):
        self.app = app
        self.in_process = False

    def pull_from_clock_kivy(self, dt=None):
        self.pull()
        
    def push_from_clock_kivy(self, omit_popup, dt=None):
        self.push(omit_popup=omit_popup)

    def pull(self, cb=None):
        if cb:
            self.push(False, cb)
        else:
            self.push(self.pull_on_push)

    def pull_on_push(self, failed=False, cb=None):
        if not failed:
            if self.in_process:
                popup = ui.Popup(title=i18n._("Recibiendo..."),
                                 content=Label(text=i18n._("Aun sincronizando, espere porfavor..\n"
                                                           "Conexión al servidor un poco lenta :(")))
                popup.open()
                return
            self.in_process = True
            view = ScrollView()
            label_log = Label(text=i18n._("Recibiendo..."))
            view.add_widget(label_log)
            def progress(model_name):
                label_log.text += "\n Model:" + model_name  
              
            popup = Popup(title=i18n._("Recibiendo"), content=view, auto_dismiss=False)
            job = threading.Thread(target=self.async_pull, args=(popup, label_log, progress, cb))
            job.start()

    def async_pull(self, popup, label_log, progress, cb):
        failed=True
        try:
            popup.open()
            self.app.db_synchronizer.pull(cb_progress=progress, date=self.app.pull_date)
            popup.dismiss(force=True)
            config = self.app.db.get('local.config', 1)
            config['pull_at'] = utils.now()
            self.app.db.save('local.config', 1, config)
            #se regenera dia de trabajo en caso de vehiculos nuevos
            self.app.operator_ui.populate()
            self.app.run_once(self.app.generate_trips)
            self.app.refresh_status_bar()
            failed=False
        except db.BackendError as e:
            Logger.exception('BackendError')
            label_log.text += "\n\n!!Interrupcion en red al sincronizar, verifique e intente de nuevo porfavor\n"
            label_log.text += str(e)
            Clock.schedule_once(lambda dt: popup.dismiss(), 5)
        except Exception as e:
            Logger.exception('Exception')
            print(traceback.format_exc())
            label_log.text = traceback.format_exc()
            Clock.schedule_once(lambda dt: popup.dismiss(), 5)
        finally:
            popup.auto_dismiss = True
            self.in_process = False

        if cb:
            cb(failed)

    def push(self, cb=None, omit_popup=False):
        if self.in_process:
            if  omit_popup:
                popup = ui.Popup(title=i18n._("Enviando..."),
                                 content=Label(text=i18n._("Intentando sincronizar, espere porfavor..\n"
                                                           "Conexión al servidor un poco lenta :(")))
                popup.open()
            return
        self.in_process = True
        
        label_log = Label(text=i18n._("Enviando..."))
        def progress(wiz_name, model_name=None, pkey=None):
            if model_name:
                label_log.text += i18n._("\n Wizard: {wizard}, Model: {model}").format(wizard=wiz_name, model=model_name)
            else:
                label_log.text += "\n Model:" + wiz_name

        if omit_popup:
            popup = ui.PopupNull()
        else:
            popup = ui.Popup(title=i18n._("Enviando"), content=label_log, auto_dismiss=False)
        job = threading.Thread(target=self.async_push, args=(popup, label_log, progress, cb))
        job.start()

    def async_push(self, popup, label_log, progress, cb):
        failed = True
        try:
            popup.open()
            self.app.db_synchronizer.push(cb_progress=progress, cb_progress_dispatch=progress)
            popup.dismiss(force=True)
            config = self.app.db.get('local.config', 1)
            config['push_at'] = utils.now(self.app.timezone)
            self.app.db.save('local.config', 1, config)
            self.app.refresh_status_bar()
            failed =False
        except db.BackendError as e:
            Logger.exception('BackendError')
            label_log.text += "\n\n!!Interrupcion en red al sincronizar, verifique e intente de nuevo porfavor\n\n"
            label_log.text += str(e)
        except Exception as e:
            Logger.exception('Exception')
            print(traceback.format_exc())
            label_log.text = traceback.format_exc()
        finally:
            popup.auto_dismiss = True
            self.in_process = False
            
        if cb:
            cb(failed)

class MainApp(ui.MainView, App):
    version = __version__

    icon = 'data/icon.png'

    # TODO: al finalizar app detener `db_adapter`.
    timezone = None
    db_adapter = None
    db_synchronizer = None

    db = None
    db_helper = None
    auto_push_service_tick_now = utils.now()

    # tag: mensaje
    # se muestra en barra de estado
    status = {}
    
    # filtros que se aplican en general
    filters = {}

    main_layout = ObjectProperty(None)
    userlist = ObjectProperty(None)
    
    operator_ui = ObjectProperty(None)
    login_panel = ObjectProperty(None)

    enable_authentication = False
    profile = None
    profiles = {}

    event_thread_stop = threading.Event()
    webhook = Webhook(WebhookFleetDispatchClientBackendAdapter())
    observer = utils.Observer()
    ractions = None

    loc_latitude = NumericProperty(0)
    loc_longitude = NumericProperty(0)

    @property
    def allow_dispatch_finalize_time(self):
        try:
            if app.config.get('Machete', 'today'):
                return True
        except:
            pass

        try:
            return self.config.getboolean('Machete', 'allow_dispatch_finalize_time')
        except:
            return False

    @property
    def machine_id(self):
        return self.config.get('Machine', 'machine_id')

    @property
    def pull_date(self):
        return utils.today(self.timezone)

    @property
    def server_url(self):
        url = self.config.get('Server', 'url')
        if self.profile:
            url = self.get_profile(self.profile)['server_url']
        if url.startswith('api://'):
            return 'http://' + url.lstrip('api://')
        return url

    @property
    def system_server_url(self):
        url = self.config.get('Server', 'url')
        if url.startswith('api://'):
            return 'http://' + url.lstrip('api://')
        return url

    @property
    def system_api_key(self):
        url = self.config.get('Server', 'api_key')
        if url.startswith('api://'):
            return 'http://' + url.lstrip('api://')
        return url

    @property
    def api_key(self):
        if self.profile:
            return self.get_profile(self.profile)['api_key']
        return self.config.get('Server', 'api_key')

    @property
    def api_user_id(self):
        if self.profile:
            return self.get_profile(self.profile).get('user_id', None)
        return None

    @property
    def required_trip(self):
        return self.config.get('Server', 'required_trip') or False

    @property
    def day_work_omit_issue_type_counter(self):
        try:
            name = self.config.get('DayWork', 'omit_issue_type_counter')
            return name.split('&')
        except:
            return []

    @property
    def location(self):
        return [self.loc_latitude, self.loc_longitude]

    @property
    def demo(self):
        if DEMO:
            return True

        try:
            return self.config.getboolean('Server', 'demo')
        except:
            return False

    def build(self):
        self.ractions = raction.RAction(self.event_thread_stop)

        EventLoop.window.softinput_mode = 'below_target'
        self.timezone = tz.gettz(self.config.get('Server', 'timezone'))

        if self.demo:
            db_adapter_bk = db.DirtyAdapterBackend(dir_store=utils.get_data_demo_path())
        else:
            db_adapter_bk = db.DirtyAdapterBackend()

        db_adapter_bk.scheme_partitions  = {
            'public_transport.dispatch': {'state': None},
            'fleet.vehicle.passenger_counter': {'vehicle': lambda v: v['id']},
            'fleet_dispatch_client_backend.trips_daily': {'date': None}
        }
        db_adapter_bk.init_partition()
        self.db_adapter = db.DirtyAdapter(db_adapter_bk)

        self.db = db.DBStats(self.db_adapter)
        self.db_helper = db_helper.DBHelper(self)
        self.build_profiles()
        self.pull_push_logic = PullPushAppLogic(self)
        self.main_layout = super(MainApp, self).build()
        self.filter_clear()
        if time.tzname[0] != 'UTC':
            warnings.warn('Zona horaria detectada %s se recomienda UTC' % time.tzname[0])
        if self.config.getboolean('Authentication', 'enable') is True:
            self.enable_authentication = True
            self.build_purge(self.main_layout, 
                             lambda playout: self.build_login(playout))
        else:
            self.build_purge(self.main_layout, self.build_operator_panel)

        self.add_observers()
        if not self.db.count('local.config'):
            self.db.create('local.config', 1, {
                'push_at': datetime.datetime.min,
                'pull_at': datetime.datetime.min,
            }, False)
        return self.main_layout

    # deprecated
    def build_bad_timezone(self, parent_layout):
        content = Button(on_release=lambda b: self.quit(), text='Zona horaria detectada %s debe ser UTC' % time.tzname[0])
        popup = Popup(content=content, title='Bad Timezone')
        popup.open()

    def build_purge(self, parent_layout, cb=None):
        label = Label(text=i18n._("Inicializando...\n"))
        view = Popup(content=label,
                     title=i18n._("Cargando Despachon"),
                     size_hint=(None, None),
                     size=EventLoop.window.size)

        def purge_and_continue(app, popup, label, parent_layout):
            try:
                app.purge_old(label)
            except Exception as e:
                traceback.print_exc()
            popup.dismiss()
            if callable(cb):
                cb(parent_layout)
        view.bind(on_open=lambda x: purge_and_continue(self, view, label, parent_layout))        
        view.open()

    def build_login(self, parent_layout):
        self.build_profiles()
        self.login_panel = LoginPanel(on_ok=self.build_operator_panel)
        parent_layout.add_widget(self.login_panel)
        
    def build_operator_panel(self, parent_layout):
        self.init_synchronizer()
        self.start_service()
        self.operator_ui = operator_logic.OperatorUI()
        parent_layout.add_widget(self.operator_ui)
        self.operator_ui.populate()
        self.operator_ui.populate_trips()
        self.perfil_status()
        self.refresh_status()
        
    def init_synchronizer(self):
        self.event_thread_stop.clear()
        api_key = self.api_key
        server_url = self.server_url

        try:
            if server_url.startswith('http://'):
                synchronizer = db.BackendSynchronizer(self.db_adapter, server_url,
                                                      self.machine_id,
                                                      request_timeout=self.config.getint('Server', 'request_timeout'),
                                                      key=api_key)
            else:
                raise RuntimeError('No se encontro URL de sincronizacion')

        except Exception as e:
            synchronizer = db.NullSynchronizer(self.db_adapter, server_url, alert=self.alert_without_synchronizer)
            traceback.print_exc()

        self.db_synchronizer = sync.DispatchSyncronizer(self.db, synchronizer)
        if not isinstance(synchronizer, db.NullSynchronizer):
            if self.config.getboolean('Server', 'enable_auto_push'):
                Clock.schedule_once(self.auto_push_service, 60)
            
    def alert_without_synchronizer(self):
        raise form.FormError(i18n._("Porfavor verificar configuracion de sincronizacion"))

    def build_config(self, config):
        "valores por defecto a parametros de configuracion"
        config.setdefaults('Server',
                          {
                              'url': 'api://localhost:8000/demo/',
                              'timezone': 'UTC',
                              'api_key': None,
                              'enable_auto_push': False,
                              'auto_push_delay': 10,
                              'auto_push_changes': 27,
                              'request_timeout': 30,
                              'end_counter_equal_to_init_counter': True,
                              'required_trip': True,
                              'gps_enabled': False,
                              'gps_mintime': 600000,
                              'demo': False,
                              'pull_v2': True
                          })
        
        config.setdefaults('Authentication',
                           {
                               'code': secrets.token_urlsafe(5),
                               'enable': False
                           })
        config.setdefaults('Profiles',
                           {
                               'demo': '{"code": "abc", "api_key": "", "server_url": "api://localhost:8000/demo/"}'
                           })
        config.setdefaults('Webhook:Dispatch',
                           {
                               'finalize': '',
                               'canceled': '',
                               'dispatched': '',
                           })

        if DEMO or self.demo:
            config.setdefaults('Machete',
                               {
                                   'today': DEMO_TODAY,
                                   'yesterday': DEMO_YESTERDAY,
                                   'allow_dispatch_finalize_time': True,
                               })
            
        if platform == "linux":
            machine_id = open("/etc/hostname").read().strip()
            config.setdefaults('Machine', {
                'machine_id': machine_id
            })
        else:
            from plyer import uniqueid
            config.setdefaults('Machine', {
                'machine_id': str(uniqueid.id)
            })
        

                
    def build_settings(self, settings):
        "parametros menu de configuracion"
        
        params = []
        params.append({
            "type": "options",
            "title": i18n._("TimeZone"),
            "section": "Server",
            "options": ["UTC", "America/Bogota"],
            "key": "timezone"
        })
        params.append({
            "type": "string",
            "title": i18n._("URL"),
            "desc": i18n._("Internal Server URL, ejemplo: api://localhost:8000/midatabase/"),
            "section": "Server",
            "key": "url"
        })
        params.append({
            "type": "string",
            "title": i18n._("API KEY"),
            "desc": i18n._("API Key -user_application-"),
            "section": "Server",
            "key": "api_key"
        })
        params.append({
            "type": "bool",
            "title": i18n._("Authentication Enable"),
            "desc": i18n._("Enable authentication on device"),
            "section": "Authentication",
            "key": "enable"
        })
        params.append({
            "type": "bool",
            "title": i18n._("Enable Service Auto Push"),
            "desc": i18n._("Only notification"),
            "section": "Server",
            "key": "enable_auto_push"
        })
        params.append({
            "type": "numeric",
            "title": i18n._("Auto Push Delay (Minutes)"),
            "section": "Server",
            "key": "auto_push_delay"
        })
        params.append({
            "type": "numeric",
            "title": i18n._("Auto Push #Changes"),
            "section": "Server",
            "key": "auto_push_changes"
        })
        params.append({
            "type": "bool",
            "title": i18n._("Forzar Registradora Finalizada a inicial"),
            "section": "Server",
            "key": "end_counter_equal_to_init_counter"
        })
        params.append({
            "type": "bool",
            "title": i18n._("Forzar uso de viajes para despachos"),
            "section": "Server",
            "key": "required_trip"
        })
        params.append({
            "type": "bool",
            "title": i18n._('Activar GPS?'),
            "section": "Server",
            "key": "gps_enabled"
        })
        params.append({
            "type": "numeric",
            "title": i18n._("GPS Tiempo para refresco (ms)"),
            "section": "Server",
            "key": "gps_mintime",
        })
        params.append({
            "type": "bool",
            "title": i18n._('Demo?'),
            "section": "Server",
            "key": "demo",
        })

        params_webhook_dispatch = [
            {
                "type": "string",
                "title": i18n._("State Dispatched"),
                "desc": i18n._("url user_application"),
                "section": "Webhook:Dispatch",
                "key": "dispatched",
            },
            {
                "type": "string",
                "title": i18n._("State Canceled"),
                "desc": i18n._("url user_application"),
                "section": "Webhook:Dispatch",
                "key": "canceled",
            },
            {
                "type": "string",
                "title": i18n._("State Finalize"),
                "desc": i18n._("url user_application"),
                "section": "Webhook:Dispatch",
                "key": "finalize",
            }
        ]

        params_machine = [
            {
                "type": "string",
                "title": i18n._("Maquina ID"),
                "section": "Machine",
                "key": "machine_id"
            }
        ]
        settings.add_json_panel('Server', self.config, data=json.dumps(params))
        settings.add_json_panel('Webhook:Dispatch', self.config, data=json.dumps(params_webhook_dispatch))
        panelProvision = SettingsPanel(title=i18n._("Provision"))
        token_input = TextInput()
        sub_item = SettingItem(panel=panelProvision,
                               title=i18n._('Indique el token de sincronizacion'))
        btn = Button(text="Sincronizar")
        btn.bind(on_release=functools.partial(self.open_provisioner, token_input))
        sub_item.add_widget(token_input)
        sub_item.add_widget(btn)
        panelProvision.add_widget(sub_item)
        settings.interface.add_panel(panelProvision, 'Provision', panelProvision.uid)

    def add_observers(self):
        # TODO: esto es correcto? estamos creando una relacion no muy clara
        app = App.get_running_app()
        app.observer.add_observer(form.FormDispatchQuantityForDayAction, 'morboso')

    def pull(self):
        Clock.schedule_once(self.pull_push_logic.pull_from_clock_kivy)

    def push(self, omit_popup=False):
        Clock.schedule_once(
            functools.partial(self.pull_push_logic.push_from_clock_kivy, omit_popup)
        )

    def new_vehicle_register(self):
        content = form.FormNewVehicleRegister(close=self.close_new_dispatch)
        content.populate()
        self._dispatch_popup = ui.Popup(title=i18n._("Nuevo Despacho"), content=content)
        self._dispatch_popup.open()

    def new_vehicle_issue(self):
        content = form.FormNewIssuePassengerCounter(close=self.close_new_dispatch)
        content.populate()
        self._dispatch_popup = ui.Popup(title=i18n._("Novedad Registradora"), content=content)
        self._dispatch_popup.open()

    def new_dispatch(self):
        content = form.FormNewDispatch(close=self.close_new_dispatch)
        content.set_defaults()
        content.populate()
        self._dispatch_popup = ui.Popup(title=i18n._("Nuevo Despacho"), content=content)
        self._dispatch_popup.open()
        
    def close_new_dispatch(self):
        self._dispatch_popup.dismiss()

    def open_machine_info(self):
        box = BoxLayout(orientation='vertical')
        def add_item(title, val):
            hbox = BoxLayout(orientation='horizontal')
            hbox.add_widget(Label(text=title))
            hbox.add_widget(Label(text=val))
            box.add_widget(hbox)
        add_item(i18n._("Version"), __version__)
        add_item(i18n._("Maquina ID"), self.machine_id)
        if self.profile:
            add_item(i18n._("Perfil Activo"), self.profile)

        hbox = BoxLayout(orientation='horizontal')
        token_input = TextInput(password=True)
        hbox.add_widget(token_input)
        btn_provision = Button(text=i18n._('Provisionar'))
        btn_provision.bind(on_release=functools.partial(self.open_provisioner, token_input))
        hbox.add_widget(btn_provision)
        box.add_widget(hbox)
        popup = ui.Popup(title=i18n._("Maquina"), content=box)
        popup.open()
        
    # DEPRECATED: filtros en actionbar
    def open_filters(self):
        content = form.FormFilter(close=self.close_new_dispatch)
        content.populate()
        self._dispatch_popup = ui.Popup(title=i18n._("Nuevo Despacho"), content=content)
        self._dispatch_popup.open()

    def open_provisioner(self, tinput, btn):
        token = tinput.text
        tinput.text = ''
        provision.provision(self.db,
                            self.machine_id,
                            self.system_server_url,
                            self.system_api_key,
                            token)
    
    def set_old_filter(self):
        self.filters['records'] = 'old'
        self.operator_ui.populate()
        
    def set_current_filter(self):
        self.filters['records'] = 'current'
        self.operator_ui.populate()

    def set_toggle_in_time_filter(self, value):
        if value:
            self.set_old_filter()
        else:
            self.set_current_filter()

    def set_group_route_filter(self, group_route):
        self.filters['group_route'] = group_route
        self.operator_ui.populate()

    def set_vehicle_filter(self, vehicle):
        self.filters['vehicle'] = vehicle
        self.operator_ui.populate()

    def set_date_filter(self, text):
        self.filters['date'] = text
        self.operator_ui.populate()

    def filter_clear(self):
        self.filters = {
            'date': str(utils.today(self.timezone)),
            'vehicle': None,
        }

    def astimezone(self, date):
        if hasattr(date, 'astimezone') is True:
            return date.astimezone(self.timezone)
        return date

    def get_profile(self, name):
        return self.profiles[name]
    
    def build_profiles(self):
        self.profiles = {}

        dbprofiles = self.db.search('device-profiles', [])

        for dbprofile in dbprofiles:
            self.profiles[dbprofile['name']] = {
                'code': dbprofile['code'],
                'server_url': dbprofile.get('server_url', self.config.get('Server', 'url')),
                'api_key': dbprofile.get('user_application', {}).get('key', 'x'*100),
                'user_id': dbprofile.get('user_application', {}).get('user.id', None)
            }

        for name, data in self.config.items('Profiles'):
            self.profiles[name] = json.loads(data)

    def get_application_config(self):
        return utils.join_data_path('dispatch_fleet.ini')

    def try_ractions(self, dt=None):
        self.ractions.perform(self.db,
                              self.machine_id,
                              self.system_server_url,
                              self.system_api_key)


    def start_service(self):
        Clock.schedule_interval(self.try_ractions, 30)
        Clock.schedule_interval(self.refresh_status, 60)
        if platform == "android":
            Clock.schedule_interval(self._android_battery_status, 80)
            from plyer import gps
            if self.config.get('Server', 'gps_enabled'):
                gps.configure(self._android_on_location)
                gps.start(self.config.getint('Server', 'gps_mintime'))

    def _android_on_location(self, **kw):
        try:
            self.loc_latitude = kw['lat']
            self.loc_longitude = kw['lon']
        except Exception as e:
            print(traceback.format_exc())

    def _android_battery_status(self, dt):
        from plyer import battery
        status = battery.get_state()

        self.set_status('battery', round(status['percentage'], 1))
        if status.get('isCharging', False):
            self.set_status(i18n._('charging'), 'yes')
            
    def on_stop(self):
        self.event_thread_stop.set()

    def auto_push_service(self, dt=None):
        if not self.event_thread_stop.is_set():
            self.do_auto_push_service()
            Clock.schedule_once(self.auto_push_service, 60)

    def do_auto_push_service(self):
        do_push = False
        now = self.auto_push_service_tick_now

        if (utils.now() - now).seconds > self.config.getint('Server', 'auto_push_delay') * 60:
            do_push = True
            self.auto_push_service_tick_now = utils.now()

        if do_push:
            do_push = False
            self.push(omit_popup=True)

    def refresh_status(self, dt = None):
        items = []
        for k,v in self.status.items():
            items.append(f"{k}: {v}")
        self.operator_ui.status.text = ' '.join(items)

    def set_status(self, tag, msg):
        self.status[tag] = str(msg)
        
    def perfil_status(self):
        name = 'system'
        if self.profile:
            name = self.profile
        self.set_status('perfil', name)

    def refresh_status_bar(self):
        config = self.db.get('local.config', 1)
        self.operator_ui.sync_status.text = 'push_at ' + config['push_at'].strftime('%d %H:%M')

        
    def purge_old(self, label):
        try:
            config = self.db.get('local.config', 1)
        except KeyError:
            return

        days_to_keep = 3
        today = datetime.datetime.combine(utils.now(), datetime.datetime.max.time()).replace(tzinfo=self.timezone)
        old_date = (today - datetime.timedelta(days=days_to_keep));
        
        if config['push_at'] and config['push_at'].year > 1 and self.astimezone(config['push_at']) < (today - datetime.timedelta(days=days_to_keep)):
            return
        models_to_delete = [
            {'model': 'public_transport.dispatch',
             'field': 'init_time',
             'format': lambda x: x,
             'rel_delete': [
                 {'model': 'public_transport.dispatch.annotation_dispatch',
                  'fkey': 'dispatch._pkey'},
             ]
            },
            {'model': 'fleet_dispatch_client_backend.trips_daily',
             'field': 'date',
             'format': lambda x: x.date()},
            {'model': 'fleet_dispatch_client_backend.gps_log',
             'field': ''},
        ]
        records_to_delete = []
        for cond in models_to_delete:
            if cond['field']:
                query = [(cond['field'], '<=', cond['format'](self.astimezone(old_date))),
                         ('_dirty', '=', False)]
            else:
                query = []

            records = self.db.search(cond['model'], query)
            if 'rel_delete' in cond:
                records_ids = [r['_pkey'] for r in records]
                for relcond in cond['rel_delete']:
                    records_to_delete.extend(self.db.search(relcond['model'],
                                                            [(relcond['fkey'], 'in', records_ids)]))

            records_to_delete.extend(records)

        if records_to_delete:
            self._tar_stores()

        model_to_sync = ['public_transport.dispatch',
                         'fleet_dispatch_client_backend.trips_daily',
                         'public_transport.dispatch.annotation_dispatch',
                         'fleet.vehicle.passenger_counter',
                         'fleet.vehicle.passenger_counter.issue',
                         'fleet_dispatch_client_backend.gps_log']

        for record in records_to_delete:
            Logger.info("Purge {model}={pkey}".format(model=record['_model_'], pkey=record['_pkey']))
            self.db_adapter._store_delete(record['_model_'], record['_pkey'])

        for model in model_to_sync:
            label.text += "Limpiado para " + model + "\n"

            self.db_adapter.sync(model)

        self.db.clear('ractions')

    def _tar_stores(self):
        bkname ='bk-{date}.tar.bz2.'.format(date=utils.now().strftime("%Y-%m-%d"))
        filename = None
        for bkidx in range(0, 1000):
            name = bkname + str(bkidx)
            filename = utils.join_data_path(name)
            if not os.path.exists(filename):
                break
        tar = tarfile.open(filename, 'x')
        for storefile in glob.glob(utils.join_data_path('*.store')):
            Logger.debug("Backup {filename}".format(filename=storefile))
            tar.add(storefile)
        tar.close()
        
    def is_yesterday(self):
        return self.astimezone(utils.date_from_isoformat(self.filters['date'])) \
            <= self.astimezone(utils.yesterday(self.timezone))

    def is_today(self):
        return self.astimezone(utils.date_from_isoformat(self.filters['date'])) \
            == self.astimezone(utils.today(self.timezone))
    
    def quit(self):
        popup = Popup(title=i18n._('Cerrando'), content=Label(text=i18n._('Cerrando porfavor espere')))
        popup.open()
        def _quit(*args):
            if self.enable_authentication:
                for child in self.main_layout.children:
                    self.main_layout.remove_widget(child)
                self.event_thread_stop.set()
                popup.dismiss()
                self.build_login(self.main_layout)
            else:
                self.event_thread_stop.set()
                self.stop()
                time.sleep(6)
                popup.dismiss()
                exit(0)
            return True
        popup.bind(on_open= _quit)

    def append_gps_log(self, model, pkey, message):
        if not self.config.get('Server', 'gps_enabled'):
            return

        try:
            record, = self.db.search(model, [('_pkey', '=', pkey)], limit=1)
            fid = record.get('id', None) or record.get('_fkey', None)
            data = {
                'machine': self.machine_id,
                'model': model,
                'message': message,
                'latitude': self.loc_latitude,
                'longitude': self.loc_longitude,
                '_actions_': [['fleet_dispatch_client_backend.gps_log', 'try_link', [model, pkey]]]
            }

            if fid:
                data['model_id'] = fid
            else:
                data['model_pkey'] = pkey
            self.db.create('fleet_dispatch_client_backend.gps_log', None, data)
        except ValueError:
            pass

    def generate_trips(self):
        today = utils.today(self.timezone)
        for vehicle in self.db.search('fleet.vehicle', [], order=[('internal_code', 'ASC')]):
            try:
                trip_daily, = self.db.search('fleet_dispatch_client_backend.trips_daily',
                                            [('date', '=', today),
                                             ('vehicle.id', '=', vehicle['id'])],
                                             order=[('init_counter', 'DESC'), ('date', 'DESC')],
                                             limit=1)
            except ValueError:
                drecords = self.db_helper.find_dipatchs_by_vehicle_id_and_date(vehicle['id'], today)
                pkey = self.db.create('fleet_dispatch_client_backend.trips_daily', None, {
                    'date': today,
                    'vehicle': vehicle.copy(),
                    'group_route': vehicle['routes'].copy(),
                    'init_counter': 0,
                    'dispatches': drecords,
                    'quantity_dispatches': len(drecords),
                    'end_counter': 0,
                })
        self.run_once(self.operator_ui.populate_trips)

    def run_once(self, cb, *args, **kwargs):
        Clock.schedule_once(lambda dt: cb(*args, **kwargs), 0)

    def on_resume(self):
        if self.operator_ui:
            self.operator_ui.populate()
            self.operator_ui.populate_trips()

class FormExceptionHandler(ExceptionHandler):

    def handle_exception(self, ins):
        if isinstance(ins, form.FormError):
            popup = ui.Popup(title="Form Error", content=Label(text=ins.message))
            popup.open()
            return ExceptionManager.PASS
        if isinstance(ins, LoginException):
            popup = ui.Popup(title="Form Error", content=Label(text=ins.message))
            popup.open()
            return ExceptionManager.PASS
        return ins

@contextmanager
def pidfile():
    file_name = "/tmp/.kivy.pid"
    if platform == "linux":
        pid_file = file_name
        try:
            kpid = open(pid_file, "r").read()
            if os.path.isdir('/proc/{}'.format(kpid)):
                warnings.warn(i18n._('Detected kivy running, please delete {} otherwise'.format(pid_file)))
                exit(-1)
            else:
                open(pid_file, "w").write(str(os.getpid()))
        except FileNotFoundError:
            open(pid_file, "w").write(str(os.getpid()))
            
        yield
        os.unlink(file_name)
    else:
        yield


def main():
    with pidfile():    
        from kivy.config import Config as KVConfig
        KVConfig.set('kivy', 'exit_on_escape', '0')
        KVConfig.write()
    
        ExceptionManager.add_handler(FormExceptionHandler())
        if platform == "android" and not DEMO:
            from android.permissions import request_permissions, Permission
            request_permissions([Permission.WRITE_EXTERNAL_STORAGE,
                                 Permission.ACCESS_COARSE_LOCATION,
                                 "android.permission.ACCESS_SURFACE_FLINGER"])

        app = MainApp()
        app.run()
    
if __name__ == '__main__':
    main()
