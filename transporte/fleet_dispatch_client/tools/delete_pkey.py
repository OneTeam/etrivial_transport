import pickle
import sys


if __name__ == '__main__':
    assert len(sys.argv) == 3, "usage:%s <file .store> <pkey>" % (sys.argv[0])
    delete_pkey = sys.argv[2]
    
    dbstore = pickle.load(open(sys.argv[1], 'rb'))

    del dbstore[delete_pkey]
    pickle.dump(dbstore, open(sys.argv[1] + '.new', 'wb'))
