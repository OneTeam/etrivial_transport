import pickle
import sys
import os
import os.path
from glob import glob

def find_by_pkey(records, pkey):
    if pkey in records:
        return records[pkey]
    return None

def find_by_fkey(records, fkey):
    for _, record in records.items():
        if 'id' in record and record['id'] == fkey:
            return record

        if '_fkey' in record and record['_fkey'] == fkey:
            return record
    return None


def fix_relations(models, model_name, fkey, rel_model):
    if model_name not in models:
        return models

    cmodels = models.copy()
    records = cmodels[model_name].copy()
    for pkey, record in records.items():
        if fkey not in record:
            continue
      
        #relaciones a group route
        groute_fkey = record[fkey].get('_fkey', None)
        if not groute_fkey:
            groute_fkey = record[fkey].get('id', None)
        groute = find_by_fkey(cmodels[rel_model], groute_fkey)
        if not groute:
            try:
                groute_pkey = record[fkey]['_pkey']
                groute = find_by_pkey(cmodels[rel_model], groute_pkey)
            except KeyError:
                continue

        assert groute, "no se ubico relacion %s para /%s/ en %s" % (rel_model,
                                                                  fkey,
                                                                  record[fkey].get('rec_name', repr(record)))

        models[model_name][pkey][fkey] = groute.copy()

    return models

if __name__ == '__main__':
    models = {}
    
    assert len(sys.argv) == 2, "usage:%s <dir stores>" % (sys.argv[0])
    dirstore = sys.argv.pop()
    
    stores = glob(os.path.join(dirstore, '*.store'))
    for path in stores:
        models[os.path.basename(path)] = pickle.load(open(path.strip(), 'rb'))

    models = fix_relations(models, 'public_transport.route.store', 'group',
                           'public_transport.group_route.store')
    models = fix_relations(models, 'fleet.vehicle.store', 'group_route',
                           'public_transport.group_route.store')
    models = fix_relations(models, 'public_transport.dispatch.store', 'group_route',
                           'public_transport.group_route.store')
    models = fix_relations(models, 'public_transport.dispatch.store', 'route',
                           'public_transport.route.store')
    models = fix_relations(models, 'public_transport.dispatch.store', 'vehicle',
                           'fleet.vehicle.store')
    models = fix_relations(models, 'fleet_dispatch_client_backend.trips_daily.store', 'group_route',
                           'public_transport.group_route.store')
    models = fix_relations(models, 'public_transport.dispatch.quantity_for_day.store', 'group_route',
                           'public_transport.group_route.store')

    
    for model_name, record in models.items():
        path_store = model_name
        if not os.getenv('INPLACE'):
            path_store += '.new'
        pickle.dump(record, open(os.path.join(dirstore, path_store), 'wb'))
