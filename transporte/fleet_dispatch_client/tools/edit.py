import pickle
import sys
import os
import re
from functools import reduce

program_name = sys.argv[0]

def get_value_dot(record, key):
    subfields = key.split('.')
    return reduce(lambda acc, val: acc.get(val, None), subfields, record)

def set_value_dot(record, key, val):
    subfields = key.split('.')
    crecord = record
    for field in subfields:
        if field in record:
            if isinstance(record[field], dict):
                crecord = crecord[field]
            else:
                if '/' in val:
                    old, new = val.split('/', 1)
                    fval = get_value_dot(record, key)
                    rfval = fval.replace(old, new)
                    crecord[field] = rfval
                else:
                    crecord[field] = val

if __name__ == '__main__':
    #variable de entorno INPLACE actualiza archivo
    #<cast>:<val>
    #<cast>:<pattern>/<substitute>
    assert len(sys.argv) > 2, 'usage: %s <file .store> <set key=val,key=val> <where key=int:val..>' % (program_name)
    path_store = sys.argv[1]

    where = [field.split('=') for field in sys.argv[2].split(',')]
    try:
        setvals = [field.split('=') for field in sys.argv[3].split(',')]
    except IndexError:
        setvals = where
        where = []

    dbstore = pickle.load(open(path_store, 'rb'))
    found = []

    # TODO implementar otras operaciones
    find_operators = {
        '=': lambda a, b: a == b
    }
    
    for pkey, record in dbstore.items():
        filters = []
        for (key, val) in where:
            lval = get_value_dot(record, key)
            filters.append(find_operators['='](str(lval), val))

        if all(filters):
            found.append((pkey, record))

    print("# Registros encontrados:", len(found))
    for (pkey, record) in found:
        
        print("Registro ", pkey, "se actualiza ")
        for (nkey, nval) in setvals:
            type_field, new_val = nval.split(':', 1)
            func = eval(type_field)
            before_val = get_value_dot(record, nkey)
            set_value_dot(record, nkey, func(new_val))
            next_val = get_value_dot(record, nkey)
            print('\t', nkey, '=', before_val, '->', next_val, ' ', end='')
        dbstore[pkey] = record.copy()

    path_store_out = path_store
    if not os.getenv('INPLACE'):
        path_store_out += ".new"

    pickle.dump(dbstore, open(path_store_out, 'wb'))
