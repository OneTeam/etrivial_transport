# cliente despacho


## Requerimientos

  * python3 (sistema para Jaula)
  * base-devel o build-essential (sistema para compilar)
  * cython 0.29.2
  * python-dateutil
  * kivy 1.10.1
  * proteus (solo en versiones iniciales)
  * trytond (solo en versiones iniciales)

# Android

Parametros por defecto para android:

  * teclado se hace 'dock'
  * se inicia servicio para mostrar bateria
  
Para que apliquen los cambios se requiere despues de la primera instalacion,
iniciar y volver a iniciar el programa.

### Habilitar depuracion

como root desde **termux**
~~~
# sed -i 's|log_level = info|log_level = debug|' /data/user/0/co.com.despachon.fleet/files/app/.kivy/config.ini
~~~

### ZONA HORARIA

desde la version 0.6.80 se requiere dispositivo en zonahoraria UTC,
~~~
$ setprop persist.sys.timezone "UTC"
$ settings put global time_zone "UTC"
$ settings put global auto_time 0
$ settings put global auto_time_zone 0
~~~

# Versionado

Para actualizar la version del aplicativo utilizar:
~~~
$ git describe --tags
~~~

# Uso

## Machine

Se requiere identificar de forma unica al cliente entre todos los clientes -se intenta dar una identificacion automatica-:

~~~
[Machine]
machine_id = Mi computadora unica
~~~

## Almacenamiento

### Android

Si se desea almacenar los .store y .ini, se deben dar permisos al aplicativo de acceso al almacenamiento, el intentara solicitarlos al usuario al iniciar por primera vez el aplicativo, de lo contrario se puede dar por medio de la terminal:

~~~
$ pm grant co.com.despachon.fleet android.permissions.WRITE_EXTERNAL_STORAGE
~~~

## Enturnamiento

Para enturnar se requiere la sequencia de rutas, !!solo usar para despachos con mas de 3 rutas!!:

~~~
[Enturn]
sequence = 315,316,314
~~~

## fleet_dispatch_client_backend

El cliente puede conectarse a la [applicacion de usuario/user_application](http://docs.tryton.org/projects/server/en/latest/topics/user_application.html?highlight=user_application)  exportada por el módulo de **fleet_dispatch_client_backend** , para esto usar la url **api://localhost:8000/<database>** donde **<database>** es el nombre de la base de datos, adicionalmente se requiere la **api_key** creada y validada por el usuario.

## Novedades de Registradora


~~~ini
[DayWork]
omit_issue_type_counter = namee1&nameee2
~~~

## Viajes requeridos

pordefecto se requiere antes de cualquier despacho que el vehiculo haya inicializado el viaje,
se puede deshabilitar con la opcion **required_trip**.

~~~ini
[Server]
required_trip = False
~~~

## Timeout

Se puede parametrizar el tiempo de espera para peticiones de red, editando el *.ini* del aplicativa, ejemplo:

~~~ini
[Server]
request_timeout=30
~~~~

## Machete

Se adiciona la sección Machete para configurar parametros del aplicativo, ejemplo.
~~~ini
[Machete]
today = 2019-01-01
yesterday = 2019-01-01
allow_dispatch_finalize_time = True #si se permite indicar hora final de despacho
~~~

## Webhook

Se implementa **webhook** para despachos, estos se pueden configurar adicionando al archivo de configuracion la seccion **Webhook:Dispatch**, ejemplo:

~~~ini
[Webhook:Dispatch]
dispatched = http://localhost:8000/demo/fleet_dispatch_client/webhook/dispatch/dispatched
canceled = http://localhost:8000/demo/fleet_dispatch_client/webhook/dispatch/canceled
~~~

Se realizara un **POST** a la direccion indicada enviando como contenido **PICKLE** los datos del despacho.

**TODO** ejemplo..

## Autenticación/Perfiles

Para gestion de multiples perfiles adicionar en el archivo de configuracion, la seccion **Profiles**, ejemplo:

~~~ini
[Profiles]
demo = {"code": "abc", "api_key": "abc", "server_url": "demo:trytond_demo.ini", "user_id": 2}
~~~

Donde **demo** es el perfil y se debe indicar en formato **JSON**, los atributos serian:
  *  *code* de ingreso
  *  **api_key** (opcional) 
  *  *server_url* para conexion de proteus o *fleet_dispatch_client_backend*.
  *  *user_id* para indicar el user, esto es especifico: para dispatched_uid,canceled_uid,donde_uid
  
Si **server_url** usa el recurso **api://** (ejemplo: api://localhost:8000/mibase) entonces es necesario el **api_key** con la llave generada por el usuario en el servidor de tryton.

## Android (en proceso)

NOTA: al actualizar se elimina los *.store locales, entonces asegurarse que este sincronizando.

[GUIA](https://kivy.org/doc/stable/guide/packaging-android.html?highlight=android).

  * Hyperbola GNU/Linux, ahi error de dependencias para *multilib*.
  
### Android on Parabola GNU/Linux 64-bit

En */etc/pacman.conf* activar repositorios **multilib**.

```bash
# pacman -S --noconfirm base-devel ccache wget zip unzip jdk8-openjdk python python-pip python-virtualenv python-kivy lib32-mesa-libgl lib32-sdl2 lib32-sdl2_image lib32-sdl2_mixer sdl2_ttf gcc-multilib gcc-libs-multilib ccache git cython
# pip install --upgrade buildozer
```

Con el ejecutable **android** instalar una y otra vez **platform-tools** hasta que en la lista aparezca para instalar **la version 24** , este era un error que estaba comentiendo.

Inicial el proyecto con **buildozer init**, a continuacion actualizar los siguientos parametros en **buildozer.spec**:
ver [NDK-SDK](https://github.com/kivy/kivy/wiki/Android-SDK-NDK-Information).

```
android.permissions = WAKE_LOCK, INTERNET
android.api = 24
android.minapi = 21
android.sdk = 20
android.ndk = 14b
android.ndk_api = 24
android.skip_update = True
android.wakelock = True
p4a.branch = master
```

### fallos al compilar

#### Aidl not found

en [esta pagina estan los enlaces](https://androidsdkoffline.blogspot.com/p/android-sdk-build-tools.html), pero el usado es [descargar](https://dl.google.com/android/repository/build-tools_r24-linux.zip), una descargado se descomprime y se saca el ejecutable **aidl** se copia a *~/.buildozer/android/platform/android-sdk-24/build-tools/0/aidl*.

tambien se puede descargar usando el comando **android**.

#### platforms not found

~~~
$ /root/.buildozer/android/platform/android-sdk-24/tools/android update sdk --no-ui
~~~

es necesario descargar la **API 23** se supone que el buildozer lo hace, pero en mi caso no.

~~~
$ /root/.buildozer/android/platform/android-sdk-24/tools/android list sdk
~~~
  
y ubicar el numero de la **API 23**, e instalar .

~~~
$ /root/.buildozer/android/platform/android-sdk-24/tools/android update sdk -t <numero> -u
~~~

##### errores compilar jnius ndk 14/16b

no compila, **libncurses5.so** not found, no se encuentra en ultima version se parabola se [usa](https://aur.archlinux.org/packages/ncurses5-compat-libs).

##### errores compilar jnius ndk18b

clang: error: unknown argument: '-mandroid'
clang: error: unknown argument: '-mandroid'


### CrystaxNdk Parabola GNU/Linux (64bit)

Descargar [crystax ndk](https://www.crystax.net/), descomprimir e indicar en parametro **android.ndk_path** en **buildozer.spec**.

Require **python 3.6**.

en [esta pagina estan los enlaces](https://androidsdkoffline.blogspot.com/p/android-sdk-build-tools.html), pero el usado es [descargar](https://dl.google.com/android/repository/build-tools_r20-linux.zip), una descargado se descomprime y se saca el ejecutable **aidl** se copia a *~/.buildozer/android/platform/android-sdk-20/build-tools/0/aidl*.

Compila correctamente a excepcion de:

*/root/.buildozer/android/platform/android-sdk-20/tools/ant/build.xml:659: /root/fleet_dispatch_client/.buildozer/android/platform/build/dists/myapp/tmp-src does not exist.*

#### buildozer.spec

```
android.permissions = WAKE_LOCK, INTERNET
android.skip_update = True
android.wakelock = True
android.ndk_path = /root/crystax
p4a.branch = master
```
