# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.
from kivy.network.urlrequest import UrlRequest
from kivy.app import App
from kivy.clock import Clock

import functools
from threading import Thread
import queue

import protocol

class RAction(Thread):
    'Acciones remotas'

    def __init__(self, event_quit):
        Thread.__init__(self)
        self.queue = queue.Queue()
        self.event_quit = event_quit
        self.start()

    def perform(self, db, machine, base_url, api_key):
        self.queue.put((db, machine, base_url, api_key))

    def run(self):
        while not self.event_quit.is_set():
            try:
                args = self.queue.get(timeout=5)
                self.get_actions(*args)
                self.process_actions(*args)
                self.put_actions(*args)
            except queue.Empty:
                pass

    def get_actions(self, db, machine, base_url, api_key):
        headers = {'Content-Type': 'application/octet-stream', 'Accept': 'application/octet-stream'}
        headers['Authorization'] = 'Bearer {key}'.format(key=api_key)
        
        UrlRequest(str(protocol.UrlRAction(base_url, machine)),
                   on_success = functools.partial(self._on_success_get_actions, db),
                   on_failure = self._on_failure,
                   decode=False,
                   debug=True,
                   req_headers=headers)

    def _on_failure(self, req, res):
        print(res)

    def _on_success_get_actions(self, db, req, res):
        ractions = protocol.unserialize(res, decoder=protocol.decode_binary)
        for raction in ractions:
            try:
                db.get('ractions', raction['id'])
            except KeyError:
                db.create('ractions', raction['id'], raction)

    def process_actions(self, db, machine, base_url, api_key):
        app = App.get_running_app()
        try:
            raction, = db.search('ractions',
                                 [('completed', '=', False)],
                                 order=[('sequence', 'DESC')])


            if raction['action'] == 'push_pull':
                Clock.schedule_once(lambda dt: app.pull())
            elif raction['action'] == 'pull':
                #se fuera a un pull esto borra local
                Clock.schedule_once(lambda dt: app.pull_push_logic.pull_on_push())
            elif raction['action'] == 'push':
                Clock.schedule_once(lambda dt: app.push())

            #!!se marca como completada pero no se tiene
            #estado de finalizado correctamente
            raction['completed'] = True
            raction['active'] = False
            db.save('ractions', raction['id'], raction)
            db.sync('ractions')
        except ValueError:
            pass

    def _on_success_put_actions(self, db, raction, req, res):
        raction['ack'] = True
        db.save('ractions', raction['id'], raction)
        
    def put_actions(self, db, machine, base_url, api_key):
        headers = {'Content-Type': 'application/octet-stream', 'Accept': 'application/octet-stream'}
        headers['Authorization'] = 'Bearer {key}'.format(key=api_key)

        for raction in db.search('ractions', [('ack', '=', False),
                                              ('completed', '=', True)]):
            UrlRequest(str(protocol.UrlRAction(base_url, machine)),
                       req_body=protocol.serialize(raction, encoder=protocol.encode_binary),
                       on_success = functools.partial(self._on_success_put_actions, db, raction),
                       on_failure = self._on_failure,
                       decode=False,
                       debug=True,
                       req_headers=headers)
  
                
