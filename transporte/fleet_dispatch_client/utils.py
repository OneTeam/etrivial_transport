# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import os
import threading
import queue

import datetime as pydatetime
from dateutil import tz
import itertools
from collections import OrderedDict
import threading
from kivy.app import App
from kivy.utils import platform


#tomado de: `fleet.vehicle.passenger_counter` metodo `_substract`
#TODO: no entiendo la ayuda
def passengers_counter_diff(init,end):
    """Devuelve la diferencia entre 2 valores
        
        Los valores tienen un limite desconocido y vuelvea iniciarse en 0
        porque son contadores mecanicos.
        Son desconocidos porque suponemos que pueden habe registradoras de
        5 digitos y otras de 6.
        Tambien suponemos que entre 2 registros no se puede haber recorrido
        la totalidad de los números porque no pódriamos saber la diferencia
    """
    if not init or not end:
        return 0

    return (end - init) % (10 ** max(len(str(init)), len(str(end))))


class Observer():
    "Implementacion de patron 'Observer'"

    def __init__(self):
        self.observers = []

    def run(self):
        while not self.main_stop_event.is_set():
            try:
                item = self.queue.get(timeout=15)
                if item:
                    cb = item.pop(0)
                    cb(*item)
            except queue.Empty:
                pass

    def add_observer(self, observer, func = 'update'):
        self.observers.append((observer, func))

    def notify(self, tag, vals):
        for observer, func in self.observers:
            getattr(observer, func)(tag, vals)

def get_data_demo_path():
    if platform == 'android':
        return os.path.join(os.getenv('ANDROID_APP_PATH'), 'demo')
    return 'demo/'

def get_data_path():
    if platform == 'android':
        from android.permissions import check_permission, Permission
        if check_permission(Permission.WRITE_EXTERNAL_STORAGE):
            from plyer import storagepath
            return storagepath.get_external_storage_dir()
    return ''

def join_data_path(*args):
    return os.path.join(get_data_path(), *args)

def str_format_datetime(val):
    if val:
        return val.strftime("%d/%m/%y %H:%M")
    return ""

def str_format_hourmin(val):
    if val:
        return val.strftime("%H:%M")
    return ""

def get_pid_file_path():
    return join_data_path('main_auto_push.pid')

def get_pid():
    pidfile = get_pid_file_path()
    if not os.path.exists(pidfile):
        return 0
    with open(pidfile, 'r') as f:
        try:
            return int(f.readline())
        except ValueError:
            return 0

def set_pid(pid=0):
    open(get_pid_file_path(), "w").write(str(pid))



def _enturn_sequence_routes(initial_route, dispatchs):
    app = App.get_running_app()
    try:
        #se puede indicar manualmente desde
        #el archivo de configuracion al sequencia para el enturnado
        routes = app.config.get('Enturn', 'sequence').split(',')
    except:
        # TODO: solo es funcional para 2 rutas, para mas no se garantiza el orden.
        # TODO: si por algun motivo no funciona, ahi que revisar
        # que si se mantenga el orden entre 'list' y 'set'
        routes = [initial_route]
        for d in dispatchs:
            if d['route']['id'] not in routes:
                routes.append(d['route']['id'])

        assert len(routes) == 2, 'please configure section Enturn.sequence now detected %d dispatchs %d' % (len(routes), len(dispatchs))
    return routes

def _do_enturn(initial_route, dispatchs):
    routes = _enturn_sequence_routes(initial_route, dispatchs)
    grouped = OrderedDict()
    for dispatch in dispatchs:
        # TODO: se usa como llave implicita 'init_time'
        # ya que actualmente no ahi un campo que relacione
        # las rutas entre si.
        key = dispatch['init_time']
        subkey = dispatch['route']['id']
        grouped.setdefault(key, {})
        grouped[key][subkey] = dispatch

    edispatchs = []
    croutes = itertools.cycle(routes)
    for key, groutes in grouped.items():
        next_route = next(croutes)
        if next_route in grouped[key]:
            edispatchs.append(grouped[key][next_route])
        else:
            raise RuntimeError("not found route %s at time %s" %(next_route, str(key)))
        
    return edispatchs

def enturn(pkey, turn, partial=True, extra_where=[], unique_enturn=False):
    assert partial == True, 'untested complete'
    
    # Se utiliza el algoritmo de 'tests/enturn_test.py'
    app = App.get_running_app()

    where = [
        ('state', '=', 'draft'),
        ('turn', '=', turn),
    ]
    where.extend(extra_where)
    dispatchs = app.db.search('public_transport.dispatch', where, order=[('init_time', 'ASC')])

    head = []
    tail = []
    
    for dispatch in dispatchs:
        if app.astimezone(today()) != app.astimezone(dispatch['init_time']).date():
            continue

        if dispatch['_pkey'] != pkey and not tail:
            head.append(dispatch)
        else:
            if not tail:
                head.append(dispatch)
            tail.append(dispatch)

    if not tail:
        return []

    if unique_enturn:
        return tail

    initial_route = None
    for dispatch in dispatchs:
        if dispatch['_pkey'] == pkey:
            if not 'id' in dispatch['route']:
                raise RuntimeError('error: despacho con ruta en blanco, verifique grupo de ruta: %s' % (dispatch['group_route']['rec_name']))
            initial_route = dispatch['route']['id']
            break

    assert initial_route != None

    tail_enturn = _do_enturn(initial_route, tail)
    # TODO: se requiere list() en reversed() ya que de lo contrario
    # no mantiene el orden
    if partial:
        return tail_enturn
    else:
        head_enturn = _do_enturn(initial_route, list(reversed(head)))
        return list(itertools.chain(reversed(head_enturn), tail_enturn))

def date_from_isoformat(val):
    return pydatetime.date.fromisoformat(val)

def now(tzinfo=None,tz=None):
    tzinfo = tzinfo or tz
    tnow = pydatetime.datetime.combine(today(tzinfo), pydatetime.datetime.now(tzinfo).time()).replace(tzinfo=tzinfo)
    if not tzinfo:
        tnow = tnow.replace(tzinfo=pydatetime.timezone.utc)
    return tnow
    
def today(tzinfo=None):
    try:
        app = App.get_running_app()
        stoday = app.config.get('Machete', 'today')
        return pydatetime.date.fromisoformat(stoday)
    except:
        return pydatetime.datetime.now(tzinfo).date()

def utc_today():
    return today(pydatetime.timezone.utc)

def yesterday(tzinfo=None):
    app = App.get_running_app()
    try:
        syesterday = app.config.get('Machete', 'yesterday')
        return pydatetime.date.fromisoformat(syesterday)
    except:
        return today(tzinfo) - pydatetime.timedelta(days=1)

def as_utc(dt):
    if dt.tzname() == None or dt.tzname() == 'UTC':
        return dt.replace(tzinfo=pydatetime.timezone.utc)
    else:
        return dt.astimezone(pydatetime.timezone.utc)

def str_name_vehicle(name):
    names = name.split('-')
    return "\n".join(names)




class _wlock:

    def __init__(self, rlock):
        self._event = threading.Event()
        self._event.set()
        self._rlock = rlock

    def __enter__(self):
        self._rlock._event.wait()
        self._event.clear()

    def __exit__(self, *args):
        self._event.set()

class RWLock:

    def __init__(self):
        self._event = threading.Event()
        self._event.set()
        self.write = _wlock(self)
        self._readers = 0
        
    def __enter__(self):
        self.write._event.wait()
        self._event.clear()
        self._readers += 1
        
    def __exit__(self, *args):
        self._readers -= 1
        if self._readers == 0:
            self._event.set()


