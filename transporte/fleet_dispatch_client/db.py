# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

#DB implementa el sistema de sincronizacion
#y exporta API para gestion de datos internos

import os
from collections import defaultdict
from functools import reduce, partial
import queue
from queue import Queue
import uuid
import datetime
import socket

#unknown encoding: idna
import encodings.idna

#https://docs.python.org/3/library/socket.html?highlight=socket#socket.socket.settimeout
socket.setdefaulttimeout(30)

from kivy.clock import Clock
from kivy.storage.dictstore import DictStore
from kivy.logger import Logger
from network.urlrequest import UrlRequest

import utils
import protocol

class BackendError(Exception):
    pass

class PersistentFIFOQueue(object):
    """
    Queue persistente -No bloqueante- usando almacenamiento de kivy.
    
    """
    def __init__(self, name):
        self._lock = utils.RLock()
        self.store = DictStore(utils.join_data_path(name))
        if not self.store.exists('queue'):
            self.store.put('queue', counter=0,queue=[])

    def _next_counter(self):
        return self.store.get('queue')['counter'] + 1

    def putleft(self, val):
        with self._lock:
            counter = self._next_counter()
            queue = self.store.get('queue')
            records = self._queue_sorted(queue['queue'])
            records.insert(0, (counter, val))
            records = [(idx, records[idx][1]) for idx in range(0, len(records))]
            queue.update({'counter': len(records), 'queue': records})
            self.store.put('queue', **queue)

    def put(self, val):
        with self._lock:
            counter = self._next_counter()
            queue = self.store.get('queue')
            records = self._queue_sorted(queue['queue'])
            records.append((counter, val))
            queue.update({'counter': counter, 'queue': records})
            self.store.put('queue', **queue)

    def get(self):
        with self._lock:
            fifo = self.store.get('queue')
            records = self._queue_sorted(fifo['queue'])
            try:
                head = records.pop(0)[1]
            except IndexError:
                raise queue.Empty

            fifo.update({'queue': records})
            self.store.put('queue', **fifo)
            return head

    def __iter__(self):
        while True:
            try:
                yield self.get()
            except queue.Empty:
                break
            
    def _queue_sorted(self, records):
        if not records:
            return []
        return sorted(records, key=lambda record: record[0])


class DBAdapter(object):
    """"
    Adaptador para base de datos.

    `fkey` es la llave en el servidor de tryton.
    Si `fkey` no es `None` indica que se debe actualizar
    en el servidor, si `fkey` es False el registro es virtual,
    y no se realizan actualizaciones en el servidor.
    """

    def count(self, model):
        raise NotImplementedError
    
    def create(self, model, pkey, fkey, vals):
        raise NotImplementedError
    
    def save(self, model, pkey, fkey, vals, **kw):
        raise NotImplementedError

    def delete(self, model, pkey, fkey):
        raise NotImplementedError

    def get(self, model, pkey, fkey):
        raise NotImplementedError

    def clear_cache(self, model):
        return

    def clear(self, model):
        raise NotImplementedError

    def search(self, model, filters, order=[],limit=None):
        """
        permite la busqueda de registros por `model`.

        :param model: nombre del modelo.
        :param filters: lista de condicionales para la busquedad [('campo', '<>=in', 'campo'|valor)...], adicionalmente
        permite buscar valores en `dict` anidados utilizando `.`.
        :param order: ordena usando ('clave', 'DESC' | 'ASC')
        """
        raise NotImplementedError
    
    def sync(self, model):
        raise NotImplementedError
    
class DB(object):
    """
    `Db` para la gestion de la data.
    """
    
    def __init__(self, adapter):
        assert isinstance(adapter, DBAdapter)
        
        super(DB, self).__init__()
        self.reqs = Queue()
        self.adapter = adapter

    def create(self, model, pkey, vals, fkey=None):
        """
        :param model: nombre del modelo, usar el de tryton.
        :param pkey: llave primaria del modelo.
        :param fkey: llave primaria del modelo en tryton asigna False si es un registro virtual.
        :param vals: valores al modelo.
        """
        
        return self.adapter.create(model, pkey, fkey, vals)
        
    def save(self, model, pkey, vals, fkey=None, **kw):
        return self.adapter.save(model, pkey, fkey, vals, **kw)
        
    def delete(self, model, pkey, fkey=None):
        return self.adapter.delete(model, pkey, fkey)
        
    def get(self, model, pkey, fkey=None):
        return self.adapter.get(model, pkey, fkey)

    def search(self, model, filters, order=[], limit=None):
        return self.adapter.search(model, filters, order=order, limit=limit)

    def count(self, model):
        return self.adapter.count(model)

    def clear(self, model):
        return self.adapter.clear(model)

    def uuid4(self):
        return str(uuid.uuid4())

    def sync(self, model):
        return self.adapter.sync(model)

class ModelSynchronizer:
    """
    `ModelSynchronizer` interfaz para sincronizadores.
    """
    
    def pull(self, model, filter_=[], date=None):
        raise NotImplementedError

    def push(self, model):
        raise NotImplementedError

class DirtyAdapter(ModelSynchronizer, DBAdapter):

    def __init__(self, backend):
        self.backend = backend
        self._lock = backend._lock

    def _store(self, name):
        return self.backend._store(name)


    def create(self, *args, **kwargs):
        return self.backend.create(*args, **kwargs)

    def save(self, *args, **kw):
        return self.backend.save(*args, **kw)
    
    def get(self, *args, **kw):
        return self.backend.get(*args, **kw)

    def delete(self, *args, **kw):
        return self.backend.delete(*args, **kw)

    def clear(self, *args, **kw):
        return self.backend.clear(*args, **kw)

    def clear_cache(self, *args, **kw):
        return self.backend.clear_cache(*args, **kw)

    def search(self, *args, **kw):
        return self.backend.search(*args, **kw)

    def count(self, *args, **kw):
        return self.backend.count(*args, **kw)

    def records_for_sync(self, *args, **kw):
        return self.backend.records_for_sync(*args, **kw)

    def _create(self, *args, **kw):
        return self._call_backend('_create', *args, **kw)

    def _update(self, *args, **kw):
        return self._call_backend('_update', *args, **kw)

    def _put(self, *args, **kw):
        return self._call_backend('_put', *args, **kw)

    def _delete(self, *args, **kw):
        return self._call_backend('_delete', *args, **kw)

    def _store_delete(self, *args, **kw):
        return self._call_backend('_store_delete', *args, **kw)

    def _find(self, *args, **kw):
        return self._call_backend('_find', *args, **kw)

    def sync(self, *args, **kw):
        return self._call_backend('sync', *args, **kw)

    def _call_backend(self, func, *args, **kw):
        if hasattr(self.backend, func):
            return getattr(self.backend, func)(*args, **kw)
        else:
            warnings.warn('try calling %s not in backend' % (func))
            return None

class DirtyAdapterBackend:
    """
    `DirtyAdapter` es un adaptador, que lleva el estado
    actual de los objetos.
    
    Un objeto es un `dict` que contiene las claves de estado:
    
    - `_state`: estado del objeto puede ser *new*, *created*, *deleted*
    - `_dirty`: Verdadero en caso de que se haya hecho algun cambio al objeto.
    - `_pkey`: Llave primaria en el cliente.
    - `_fkey`: Llave primaria en el servidor.
    - `_virtual`: El objeto, no notifica cambios al servidor.
    - `_ondelete_`: 'SET NULL', un campo relación se hace None.
    - `_actions_`: lista de acciones a ejecutar en el servidor, [['Modelo', 'metodo de clase', ['argss'..]]], el servidor debe implementar el metodo como de clase y el primer argumento es 'client_vals' que son los valores del cliente
    """

    def __init__(self, dir_store=None):
        self._lock = utils.RWLock()
        self.stores = {}
        #el particionado funciona por el hecho de
        #que las consultas solo pueden ser and
        #{'model': ['field1', 'field2']}
        self.scheme_partitions = {}
        self.partitions = defaultdict(lambda: defaultdict(lambda :{}))
        self.dir_store = utils.get_data_path()
        if dir_store:
            self.dir_store = dir_store

    def _store(self, name):
        name = "%s.store" % (name)
        if name not in self.stores:
            self.stores[name] = DictStore(os.path.join(self.dir_store, name))
        return self.stores[name]
    
    def _sanitaze_vals(self, vals):
        cvals = vals.copy()
        cvals.pop('_model_', None)
        cvals.pop('_pkey', None)
        cvals.pop('_fkey', None)
        cvals.pop('_state', None)
        cvals.pop('_dirty', None)
        cvals.pop('_virtual', None)
        return cvals

    def init_partition(self):
        for model, fields in self.scheme_partitions.items():
            self._partition_clear(model)
            for record in self.search(model, []):
                self._partition_put(model, record['_pkey'], record)

    def _partition_key(self, model, field):
        return "%s.%s" % (model, field)

    def _partition_put(self, model, pkey, vals):
        fields = self.scheme_partitions.get(model, [])
        if not fields:
            return
        partition_key = []
        for name, func in fields.items():
            if callable(func):
                fname = func(vals[name])
            else:
                fname = name
            partition_key.append(str(fname))
            
        key = self._partition_key(model, ".".join(partition_key))
        self.partitions[model][key][pkey] = (pkey, vals)

    def _partition_find(self, model, query):
        if not query:
            return []

        fields = self.scheme_partitions.get(model, {})
        if not fields:
            return []

        partition_key = []
        for (field, op, val) in query:
            if field in fields:
                func = fields[field]
                if callable(func):
                    fname = func(val)
                else:
                    fname = field
                partition_key.append(str(field))
                
        key = self._partition_key(model, ".".join(partition_key))
        if key in self.partitions[model]:
            return self.partitions[model][key].values()

        return []

    def _partition_clear(self, model, pkey=None):
        if model in self.partitions:
            fields = self.scheme_partitions.get(model, [])
            for field in fields:
                try:
                    key = self._partition_key(model, field)
                    del self.partitions[model][key][pkey]
                except KeyError:
                    pass

        if not pkey:
            self.partitions[model].clear()

    def clear_cache(self, model):
        self._partition_clear(model)

    def create(self, model, pkey, fkey, vals):
        assert isinstance(vals, dict) == True
        with self._lock.write:

            if pkey is None:
                pkey = str(uuid.uuid4())

            vals['_model_'] = model
            vals['_pkey'] = pkey
            vals['_fkey'] = fkey
            vals['_state'] = 'new'
            vals['_dirty'] = True
            vals['_virtual'] = False
            if fkey == False:
                vals['_virtual'] = True

            self._store(model).put(pkey, **vals)
            self._partition_put(model, pkey, vals)
            return pkey

    def save(self, model, pkey, fkey, vals, cb=None):
        assert isinstance(vals, dict) == True
        with self._lock.write:

            cvals = self._store(model).get(pkey)
            cvals.update(self._sanitaze_vals(vals))

            if fkey:
                cvals['_fkey'] = fkey
            cvals.update({
                '_dirty': True
            })

            if cb:
                self._store(model).async_put(cb, pkey, **cvals)
            else:
                self._store(model).put(pkey, **cvals)

            self._partition_put(model, pkey, cvals)
            return cvals

        
    def get(self, model, pkey, fkey=None):
        with self._lock:
            vals = self._store(model).get(pkey)
            vals['_pkey'] = pkey
            return vals

    def delete(self, model, pkey, fkey):
        with self._lock.write:

            vals = self._store(model).get(pkey)
            vals.update({
                '_state': 'deleted',
                '_dirty': True
            })

            if fkey:
                vals['_fkey'] = fkey

            #se elimina registro, si no logro sincronizacion
            if not vals['_fkey']:
                self._store(model).delete(pkey)
            else:
                self._store(model).put(pkey, **vals)
            self._partition_clear(model, pkey)
            return vals

    def search(self, model, filters, order=[], limit=None):
        """
        consulta registros.

        """
        with self._lock:


            rel = {
                '=': lambda a,b: a == b,
                '!=': lambda a,b: a!= b,
                '>': lambda a,b: a > b,
                '>=': lambda a,b: a >= b,
                '<': lambda a,b: a < b,
                '<=': lambda a,b: a <= b,
                'in': lambda a,b: a in b,
            }
            finds = []
            records = self._partition_find(model, filters)
            if not records:
                records = self._store(model).find()

            for pkey, record in records:
                if record['_state'] == 'deleted':
                    continue


                qfilters = []
                for (field, op, val) in filters:
                    subfields = field.split('.')

                    lval = reduce(lambda acc, val: acc.get(val, None), subfields, record)
                    if isinstance(val, str) and hasattr(record, val):
                        rval = record[val]
                    else:
                        rval = val

                    qfilters.append(rel[op](lval, rval))

                if qfilters and not all(qfilters):
                    continue

                # caso especial solo se busca un registro en particular
                if limit == 1 and order == []:
                    return [record]

                finds.append(record)

            for key, direction in order:
                reverse = True
                
                def key_order(r):
                    subfields = key.split('.')
                    lval = reduce(lambda acc, val: acc.get(val, None), subfields, r)
                    if isinstance(lval, datetime.datetime):
                        return lval.timestamp()
                    #https://stackoverflow.com/questions/48234072/how-to-sort-a-list-and-handle-none-values-properly
                    return (lval is None, lval)
                if direction == 'ASC':
                    reverse =  False
                finds.sort(key=key_order, reverse=reverse)

            # se requiere tomar todos los registros
            # para poder ordernarlos y tomar el limite indicado
            if limit:
                return finds[:limit]
            return finds

    def count(self, model):
        with self._lock:
            count = self._store(model).count()
            return count

    def clear(self, model):
        with self._lock:
            self._store(model).clear()
            self._partition_clear(model)

    def records_for_sync(self, model):
        return self._store(model).find(_virtual=False, _dirty=True)

    def _create(self, model, pkey, vals):
        self._store(model).put(pkey, **vals)
        self._partition_put(model, pkey, vals)

    def _update(self, model, pkey, vals):
        cvals = self._store(model).get(pkey)
        cvals.update(vals)
        self._store(model).store_put(pkey, cvals)
        self._partition_put(model, pkey, cvals)

    def _put(self, model, pkey, vals):
        self._store(model).store_put(pkey, vals)
        self._partition_put(model, pkey, vals)

    def _delete(self, model, pkey):
        self._store(model).delete(pkey)
        self._partition_clear(model, pkey)

    def _store_delete(self, model, pkey):
        self._store(model).store_delete(pkey)
        self._partition_clear(model, pkey)

    def _find(self, model, **filters):
        return list(self._store(model).find(**filters))

    def sync(self, model):
        with self._lock:
            self._store(model).store_sync()


class BackendSynchronizer(ModelSynchronizer):
    
    def __init__(self, adapter, base_url, machine, key=None, **kwargs):
        self.adapter = adapter
        self.base_url = base_url
        self.request_timeout = kwargs.pop('request_timeout', 30)
        self.kwargs = kwargs
        self.machine = machine
        self._req_headers = {'Content-Type': 'application/octet-stream', 'Accept': 'application/octet-stream'}

        if key:
            self._req_headers['Authorization'] = 'Bearer {key}'.format(key=key)

    def do_heartbeat(self):
        try:
            UrlRequest(str(protocol.UrlHeartBeat(self.base_url, self.machine)),
                       on_success=lambda req,res: True,
                       on_failure=lambda req,res: True,
                       on_error=lambda req,res: True,
                       timeout=5,
                       req_headers=self._req_headers)
        except:
            pass


    def pull(self, model, fields, fields_relation={}, virtual=False, filter_=[],
             find_= None, date = None, klass_url_pull=None):
        self.do_heartbeat()
        if klass_url_pull:
            klass_url_pull = klass_url_pull
        else:
            klass_url_pull = protocol.UrlPull

        with self.adapter._lock:
            ctx = {'error': None, 'failure': None}
            req = UrlRequest(str(klass_url_pull(self.base_url, self.machine, model, date=date)),
                             on_success=partial(self.on_success_pull, model, virtual),
                             on_error=partial(self.on_error, ctx),
                             on_failure=partial(self.on_failure, ctx),
                             decode=False,
                             debug=True,
                             timeout=self.request_timeout,
                             req_headers=self._req_headers)
            req.wait()
            if ctx['error']:
                raise BackendError(ctx['error'])
            if ctx['failure']:
                raise BackendError(ctx['failure'])

            
    def on_success_pull(self, model, virtual, req, res):
        self._populate_pull(model, virtual, res)

    def push(self, model):
        self.do_heartbeat()

        with self.adapter._lock:
            data_with_relations = {}
            # se asume que toda relacion debe existir en su propio store
            def sync_relations(vals):
                cvals = vals.copy()
                for k, v in vals.items():
                    if isinstance(v, dict) and '_pkey' in v:
                        rvals = sync_relations(v)
                        cvals[k] = self.adapter.get(v['_model_'], v['_pkey']).copy()
                return cvals

            for pkey, vals in self.adapter.records_for_sync(model):
                data_with_relations[pkey] = sync_relations(vals)
            self.push_record_with_relations(data_with_relations)
            self.adapter.sync(model)

    def push_record_with_relations(self, vals):
        ctx = {'error': None, 'failure': None}
        req = UrlRequest(str(protocol.UrlPushWithRelations(self.base_url, self.machine)),
                         req_body=protocol.serialize(vals, encoder=protocol.encode_binary),
                         on_success=partial(self.on_success_push, None),
                         on_failure=partial(self.on_failure, ctx),
                         on_error=partial(self.on_error, ctx),
                         decode=False,
                         timeout=self.request_timeout,
                         req_headers=self._req_headers)
        
        req.wait()
        if ctx['error']:
            raise BackendError(ctx['error'])
        elif ctx['failure']:
            raise BackendError(ctx['failure'])

    def _populate_pull(self, model, virtual, res):

        if not virtual:
            to_delete = []
            for pkey, v in self.adapter._find(model, _state='created', _virtual=False):
                to_delete.append(pkey)
                    
            for pkey in to_delete:
                self.adapter._delete(model, pkey)
                
        records = protocol.unserialize(res, decoder=protocol.decode_binary)
        models_to_sync = {model}
        for val in records:
            val['_pkey'] = str(uuid.uuid4())
            val['_dirty'] = False
            val['_state'] = 'created'
            val['_virtual'] = virtual
            if virtual:
                try:
                    results = list(self.adapter._find(model, _virtual=True, id=val['id']))
                    if results:
                        continue
                except KeyError:
                    pass

            models_to_sync.add(val['_model_'])
            self.adapter._put(val['_model_'], val['_pkey'], val.copy())


        for model_to_sync in models_to_sync:
            self.adapter.sync(model_to_sync)
                

    def on_success_push(self, model, req, res):
        vals = protocol.unserialize(res, decoder=protocol.decode_binary)
        models_to_sync = set()
        for record in vals:
            models_to_sync.add(record['_model_'])
            self.adapter._update(record['_model_'], record['_pkey'], record.copy())
        for model_to_sync in models_to_sync:
            self.adapter.sync(model_to_sync)

    def raise_backend_error(self, error):
        raise BackendError(error)

    def on_error(self, ctx, req, error):
        ctx['error'] = error
        #hack, sin esto kivy queda en bucle
        req._resp_status = 600

    def on_failure(self, ctx, req, result):
        ctx['failure'] = result

class NullSynchronizer(ModelSynchronizer):

    def __init__(self, adapter, url, alert=None, **kwargs):
        self.adapter = adapter
        self.url = url
        self.kwargs = kwargs
        self.alert = alert
        
    def pull(self, model, fields, fields_relation={}, virtual=False, filter_=[], find_ = None, date = None):
        if self.alert:
            self.alert()

    def push(self, model):
        if self.alert:
            self.alert()


class DBStats(DB):
    """
    Clase que decora `DB` y agrega contador de llamadas de metodos.
    """

    def __init__(self, adapter):
        super(DBStats, self).__init__(adapter)
        self.stats = defaultdict(int)
        
    def create(self, *args, **kwargs):
        self.stats['create'] += 1
        return super(DBStats, self).create(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.stats['save'] += 1
        return super(DBStats, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.stats['delete'] += 1
        return super(DBStats, self).delete(*args, **kwargs)

    def sync(self, *args, **kwargs):
        return super(DBStats, self).sync(*args, **kwargs)
