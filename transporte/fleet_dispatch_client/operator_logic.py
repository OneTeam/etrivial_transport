# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import os
import datetime
import functools
from dateutil import tz

from kivy.app import App
from kivy.properties import (
    ObjectProperty, StringProperty, NumericProperty, DictProperty,
    BooleanProperty, ListProperty)

from kivy.uix.listview import ListView, ListItemButton
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from kivy.uix.recycleview import RecycleView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.pagelayout import PageLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
import kivy.uix.recycleview
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.clock import Clock

import db
import utils
import form
import ui
import i18n

class TripOperatorDomainListItem(RecycleDataViewBehavior, BoxLayout):
    index = None
    _header = BooleanProperty(False)
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    vehicle = StringProperty()
    route = StringProperty()
    date = StringProperty()
    quantity_dispatches = StringProperty()
    passengers = StringProperty()
    init_counter = StringProperty()
    end_counter = StringProperty()
    finalized = StringProperty()

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        self.pkey = data['_pkey']
        return super(TripOperatorDomainListItem, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
        if super(TripOperatorDomainListItem, self).on_touch_down(touch):
            return True

        if self.collide_point(*touch.pos) and self.selectable and touch.is_double_tap:
            app = App.get_running_app()
            if not self.pkey:
                return True

            record = app.db.get('fleet_dispatch_client_backend.trips_daily', self.pkey)
            content = form.FormDispatchQuantityForDayAction(
                pkey = self.pkey,
                record = record,
                close = self.dismiss_popup
            )
            content.populate()
            self._popup = ui.Popup(title='Viaje', content = content)
            self._popup.open()
            return False

        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)
        
    def dismiss_popup(self):
        self._popup.dismiss()
        self._popup = None

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected


class TripOperatorDomain(RecycleView):

    def __init__(self, *args, **kw):
        super(TripOperatorDomain, self).__init__(*args, **kw)
        self.rel_id = {}
      
class TripOperatorPanelItem(TabbedPanelItem):

    def populate_trips(self, *arg):
        app = App.get_running_app()
        app.operator_ui.panel.populate_trips()


class IssuePassengerCounterOperatorDomainListItem(RecycleDataViewBehavior, BoxLayout):
    _header = BooleanProperty(False)
    selected = BooleanProperty(False)

    vehicle = StringProperty()
    init_counter = StringProperty()
    end_counter = StringProperty()
    time = StringProperty()
    note = StringProperty()
    issue_type = StringProperty()

class VehicleRegisterOperatorDomainListItem(RecycleDataViewBehavior, BoxLayout):
    _header = BooleanProperty(False)
    selected = BooleanProperty(False)
    vehicle = StringProperty()
    time = StringProperty()
    counter = StringProperty()
    note = StringProperty()
    
class OperatorDomainListItem(RecycleDataViewBehavior, BoxLayout):
    """
    Elemento de la lista
    """
    index = None
    
    _header = BooleanProperty(False)
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)

    pkey = None
    _bgcolor = (0, 0, 0, 1)
    turn = StringProperty()
    route = StringProperty()
    state = StringProperty()
    vehicle = StringProperty()
    init_time = StringProperty()
    end_time = StringProperty()
    _passangers = StringProperty()
    #TODO: ser numerico
    init_counter = StringProperty()
    end_counter = StringProperty()
    turner = ObjectProperty(None)
    layout = ObjectProperty()

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        # `data` es populado desde 'populate_domain'


        self.index = index
        self.pkey = data['_pkey']
        if self.turner:
            self.layout.remove_widget(self.turner)

        if data['state'] == 'draft':
            if not data['_header'] and data['_turn']:
                self.turner = Button(text='Enturnar', size_hint_x=None)
                self.turner.bind(on_release=functools.partial(self.open_turner, self.pkey))
                self.layout.add_widget(self.turner)
        return super(OperatorDomainListItem, self).refresh_view_attrs(rv, index, data)
        
    def on_touch_down(self, touch):
        if super(OperatorDomainListItem, self).on_touch_down(touch):
            return True

        if self.collide_point(*touch.pos) and self.selectable and touch.is_double_tap:
            app = App.get_running_app()
            record = app.db.get('public_transport.dispatch', self.pkey)
            title = None
            content  = None
            
            if record['state'] == 'draft':
                title = "Despacho"
                klassForm = form.FormDispatch
            elif record['state'] == 'dispatched':
                title = "Wizard"
                klassForm = form.FormFinalizeOrCancel
            else:
                title = "Despacho"
                klassForm = form.FormReadonly

            content = klassForm(
                pkey = self.pkey,
                record = record,
                close=self.dismiss_popup
            )
            
            if isinstance(content, form.DispatchProperties):
                content.set_defaults()
            
            if isinstance(content, form.FormPopulate):
                content.populate()
            
            self._popup = ui.Popup(title=title, content=content)
            self._popup.open()
            
            return False

        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)
        
    def dismiss_popup(self):
        self._popup.dismiss()
        self._popup = None

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if self.turner:
            if is_selected:
                self.turner.disabled = False
            else:
                self.turner.disabled = True

    def open_turner(self, pkey, btn):
        app = App.get_running_app()
        record = app.db.get('public_transport.dispatch', pkey)

        title = "Enturnar"
        content = form.FormEnturn(
            pkey = pkey,
            record = record,
            close=self.dismiss_popup
        )

        self._popup = ui.Popup(title=title, content=content)
        self._popup.open()


class IssuePassengerCounterOperatorDomain(RecycleView):
    """
    Vista donde se visualiza la lista de novedades de registradors.
    """


class VehicleRegisterOperatorDomain(RecycleView):
    """
    Vista donde se visualiza la lista de registradors.
    """

class OperatorDomain(RecycleView):
    """
    Vista donde se visualiza la lista de despachos `OperatorDomainListItem`.
    """


class OperatorUI(PageLayout):
    sync_status = ObjectProperty(None)
    status = ObjectProperty(None)
    panel = ObjectProperty(None)
    vehicle_register_panel = ObjectProperty(None)
    vehicle_panel = ObjectProperty(None)
    trip_panel = ObjectProperty(None)
    vehicle_filter = ObjectProperty(None)
    group_route_filter = ObjectProperty(None)

    def populate(self):
        self._schedule(self._populate)

    def populate_trips(self):
        app = App.get_running_app()
        app.operator_ui.panel.populate_trips()

    def _populate(self, dt=None):
        self.panel.populate()
        self.group_route_filter.populate()
        self.vehicle_register_panel.populate()
        self.vehicle_filter.populate()
        
    def _schedule(self, cb, **kwargs):
        Clock.schedule_once(lambda dt: cb(**kwargs), 0)

   
class IssuePassengerCounterOperatorPanel(TabbedPanel):

    tab_all = ObjectProperty(None)
    domain_all = ObjectProperty(None)

    def populate(self):
        app = App.get_running_app()
        data = [{
            '_header': True,

            'vehicle': 'Vehiculo',
            'time': 'F.H',
            'init_counter': 'Reg. Inicial',
            'end_counter': 'Reg. Final',
            'issue_type': 'Tipo'
        }]

        order = [('time', 'ASC'), ('vehicle.internal_code', 'ASC')]
        for record in app.db.search('fleet.vehicle.passenger_counter.issue', [], order=order):
            # TODO: se debe filtrar la registradoras?
            # actualmente no ahi relacion en el modelo
            #filter_group_route = app.filters.get('group_route', {})
            filter_vehicle = app.filters.get('vehicle', {})
            filter_date = app.filters.get('date', str(utils.today(app.timezone)))
            
            if filter_vehicle:
                if record['vehicle'].get('id', -1) != filter_vehicle['id']:
                    continue

            if 'time' in record and not app.astimezone(record['time']).date() == utils.date_from_isoformat(filter_date):
                continue

            data.append({
                '_header': False,
                'vehicle': record['vehicle'].get('rec_name',''),
                'time': utils.str_format_datetime(app.astimezone(record['time'])),
                'init_counter': str(record['init_counter'].get('counter', None)),
                'end_counter': str(record['end_counter'].get('counter', None)),
                'issue_type': str(record['type'].get('name', None)),
                'note': str(record['note'])
            })

        self.tab_all.text = "All (%d)" % (len(data) - 1)
        self.domain_all.data = data
  
class VehicleRegisterOperatorPanel(TabbedPanel):

    tab_all = ObjectProperty(None)
    domain_all = ObjectProperty(None)

    def populate(self):
        app = App.get_running_app()
        data = [{
            '_header': True,
            'vehicle': 'Vehiculo',
            'time': 'F.H',
            'counter': '#Registradora',
            'note': 'Nota'
        }]

        order = [('time', 'ASC'), ('vehicle.internal_code', 'ASC')]
        for record in app.db.search('fleet.vehicle.passenger_counter', [], order=order):
            # TODO: se debe filtrar la registradoras?
            # actualmente no ahi relacion en el modelo
            #filter_group_route = app.filters.get('group_route', {})
            filter_vehicle = app.filters.get('vehicle', {})
            filter_date = app.filters.get('date', str(utils.today(app.timezone)))
            
            if filter_vehicle:
                if record['vehicle'].get('id', -1) != filter_vehicle['id']:
                    continue

            if 'time' in record and not app.astimezone(record['time']).date() == utils.date_from_isoformat(filter_date):
                continue

            data.append({
                '_header': False,
                'vehicle': record['vehicle'].get('rec_name',''),
                'time': utils.str_format_datetime(app.astimezone(record['time'])),
                'counter': str(record['counter']),
                'note': str(record['note'])
            })
        self.tab_all.text = "All (%d)" % (len(data) - 1)
        self.domain_all.data = data

class OperatorPanel(TabbedPanel):
    """
    Panel para mostrar por pestanas.
    """
    tab_scheduled = ObjectProperty(None)
    tab_dispatched = ObjectProperty(None)
    tab_done = ObjectProperty(None)
    tab_canceled = ObjectProperty(None)
    tab_trips = ObjectProperty(None)

    domain_scheduled = ObjectProperty(None)
    domain_dispatched = ObjectProperty(None)
    domain_done = ObjectProperty(None)
    domain_canceled = ObjectProperty(None)
    domain_trips = ObjectProperty(None)

    def populate_trips(self):
        self.domain_trips.data.clear()
        app = App.get_running_app()
        data = [{
            '_header': True,
            '_pkey': None,

            'vehicle': 'Vehicle',
            'route': 'Ruta',
            'quantity_dispatches': '# Despachos',
            'date': 'Fecha',
            'init_counter': '#Reg. Inicial',
            'end_counter': '#Reg. Final',
            'passengers': '# Pasajeros',
            'finalized': 'Finalizado?'
        }]
        cdata = 0
        self.domain_trips.rel_id = {}
        
        filters = {
            'vehicle': app.filters.get('vehicle', {})
        }

        old_trips = app.filters.get('records', None) == 'old'
        
        order = [('date', 'DESC'), ('vehicle.internal_code', 'ASC')]
        today = utils.today(tzinfo=app.timezone)
        filter_date = utils.date_from_isoformat(app.filters.get('date', str(today)))
        filter_group_route = app.filters.get('group_route', None)
        trip_daily_by_vehicle = {}
        for trip_daily in app.db.search('fleet_dispatch_client_backend.trips_daily',
                                            [('date', '=', filter_date)],
                                            order=[('init_counter', 'DESC'), ('date', 'DESC')]):
            key = trip_daily['vehicle']['id']
            if key not in trip_daily_by_vehicle:
                trip_daily_by_vehicle[key] = trip_daily
        
        for vehicle in app.db.search('fleet.vehicle', [], order=[('internal_code', 'ASC')]):
            cdata += 1
            trip_daily = None
            if vehicle['id'] in trip_daily_by_vehicle:
                trip_daily = trip_daily_by_vehicle[vehicle['id']]

            if not trip_daily:
                continue

            if filters['vehicle'] and vehicle.get('id', -1) != filters['vehicle']['id']:
                continue

            add_to_rows = True
            if filter_group_route:
                add_to_rows = vehicle['routes']['id'] == filter_group_route['id']

                
            row = {'_header': False, '_pkey': None}
            finalizedp = trip_daily.get('init_counter', 0) > 0  and trip_daily.get('end_counter', 0) > 0
            if finalizedp:
                finalized = "Si"
            else:
                finalized = "No"
                
            if old_trips:
                if not finalizedp:
                    add_to_rows = False
            else:
                if finalizedp:
                    add_to_rows = False

            row.update({
                '_pkey': trip_daily['_pkey'],
                'vehicle': vehicle.get('rec_name', ''),
                'quantity_dispatches': str(trip_daily.get('quantity_dispatches', 0)),
                'route': vehicle['routes'].get('rec_name', ''),
                'date': str(trip_daily.get('date', '')),
                'init_counter': str(trip_daily.get('init_counter', 0)),
                'end_counter': str(trip_daily.get('end_counter', 0)),
                'passengers': str(utils.passengers_counter_diff(trip_daily.get('init_counter', 0), trip_daily.get('end_counter', 0))),
                'finalized': finalized
            })

            if add_to_rows:
                self.domain_trips.rel_id[trip_daily['_pkey']] = cdata
                data.append(row)

        data.sort(key=lambda x: x['end_counter'])
        self.tab_trips.text = "Dia Trabajo (%d)" % (len(data) - 1)
        self.domain_trips.data = data

    def populate(self):
        app = App.get_running_app()
        order=[('route.id', 'ASC'), ('init_time', 'ASC')]
        states = {
            'draft': {
                'rv': self.domain_scheduled,
                'label': self.tab_scheduled,
                'format': 'Programados (%d)',
                'nrecords': 0,
                'data': []
            },
            'dispatched': {
                'rv': self.domain_dispatched,
                'label': self.tab_dispatched,
                'format': 'En Recorrido (%d)',
                'nrecords': 0,
                'data': []
            },
            'done': {
                'rv': self.domain_done,
                'label': self.tab_done,
                'format': 'Finalizados (%d)',
                'nrecords': 0,
                'data': []
            },
            'canceled': {
                'rv': self.domain_canceled,
                'label': self.tab_canceled,
                'format': 'Cancelados (%d)',
                'nrecords': 0,
                'data': []
            }
        }

        cache = {
            'today': utils.today(tzinfo=app.timezone),
            'now': utils.now(tzinfo=app.timezone),
            'current_time': utils.now(tzinfo=app.timezone) - datetime.timedelta(hours=1)
        }
        filters = {
            'group_route': app.filters.get('group_route', {}),
            'vehicle': app.filters.get('vehicle', {}),
            'date': app.filters.get('date', str(cache['today'])),
            'records': app.filters.get('records', 'current'),
            'date_isoformat': utils.date_from_isoformat(app.filters.get('date', str(cache['today']))),
        }

        for record in reversed(app.db.search('public_transport.dispatch', [], order=order)):
            for state, params in states.items():
                states[state]['nrecords'] += self._populate_operator_domain(record, states[state]['data'], app, state, order=order, cache=cache, filters=filters)

        for state, params in states.items():
            params['data'].insert(0, {
                '_header': True,
                'selectable': False,
                '_pkey': None,
                '_turn': None,
                '_bgcolor': (0/255., 101/255., 173/255., 1),
                '_init_time': None,

                'state': 'Estado',
                'turn': 'Turno',
                'route': 'Ruta',
                'vehicle': 'Vehiculo',
                'init_time': 'H:M Inicio',
                'end_time': 'H:M Final',
                'init_counter': 'Reg. Inicial',
                'end_counter': 'Reg. Final',
                '_passangers': '#Pasajeros',
            })

            params['rv'].data = params['data']
            params['rv'].refresh_from_data()
            params['label'].text = params['format'] % (params['nrecords'])


    def _populate_operator_domain(self, record, data, app, state, order=[], cache=None, filters={}):
        # TODO: al cambiar el color del registro, no se refresca en pantalla
        current_time = cache['current_time']

        if record['state'] != state:
            return 0

        if filters['vehicle']:
            if record['vehicle'].get('id', -1) != filters['vehicle']['id']:
                return 0

        if filters['group_route']:
            if record['group_route'].get('id', -1) != filters['group_route']['id']:
                return 0

        if record['init_time'] and not app.astimezone(record['init_time']).date() == filters['date_isoformat']:
            return 0
        
        if state == 'draft' and filters['records'] == 'current':
            if app.astimezone(record['init_time']) < current_time:
                return 0

        if state == 'draft' and filters['records'] == 'old':
            if app.astimezone(record['init_time']) > current_time:
                return 0

        init_counter = (record['init_counter'] or {}).get('counter', 0)
        end_counter = (record['end_counter'] or {}).get('counter', 0)
        passangers = utils.passengers_counter_diff(init_counter, end_counter)

        record.setdefault('turn', '')
        row = {
            '_header': False,
            'selectable': True,
            '_pkey': record['_pkey'],
            '_turn': record['turn'],
            '_bgcolor': (0, 0, 0, 1),
            '_init_time': record['init_time'],
            'state': record['state'],
            'turn': str(record['turn']),
            'route': str(record['route'].get('code', record['route'].get('name'))),
            'vehicle': str((record['vehicle'] or {}).get('rec_name', '')),
            'init_time': utils.str_format_hourmin(app.astimezone(record['init_time'])),
            'end_time': utils.str_format_hourmin(app.astimezone(record['end_time'])),
            'init_counter': str(record['init_counter'].get('counter', None)),
            'end_counter': str((record['end_counter'] or {}).get('counter', None)),
            '_passangers': str(passangers)
        }
        data.insert(0, row)
        return 1
