# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import sys
import shutil
import glob
import os

sys.path.append('../')

import proteus
import trytond.exceptions

import db as _db



class ProteusTest(unittest.TestCase):

    @classmethod
    def setUp(self):
        shutil.copyfile("test.db", "test.sqlite")
        [os.remove(f) for f in glob.glob("*.store")]

    @classmethod
    def tearDown(self):
        pass

    
    def test_rpc(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)
        synchronizer.pull('res.user', ['login'])

        wiz = proteus.Wizard('public_transport.dispatch.exec')
        with self.assertRaises(trytond.exceptions.UserError):
            wiz._proxy.rpc_dispatch([], wiz._config.context)

if __name__ == '__main__':
    unittest.main()
