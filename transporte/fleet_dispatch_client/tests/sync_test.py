# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import sys
sys.path.append('../')
import os
import glob
import unittest
import queue as pyqueue

import sync
import db

class WizPersistentFIFOQueueTest(unittest.TestCase):

    @classmethod
    def setUp(self):
        [os.remove(f) for f in glob.glob("*.store")]

    def test_queue_empty(self):
        queue = db.PersistentFIFOQueue('test.store')
        for i in queue:
            pass
        
        with self.assertRaises(pyqueue.Empty):
            queue.get()

    def test_queue_putleft(self):
        queue = db.PersistentFIFOQueue('test.store')
        queue.put({'val': 1})
        queue.putleft({'val': 2})
        self.assertEqual(queue.get(), {'val': 2})

    def test_requeue(self):
        queues = sync.WizPersistentFIFOQueue()

        fixtures = [
            ('modelo', 'test1', {'val': 1}),
            ('modelo', 'test1', {'val': 2}),
            ('modelo', 'test2', {'val': 11}),
            ('modelo', 'test2', {'val': 22})
        ]
        
        for (model, name, data) in fixtures:
            queues.queue(model, name).put(data)

        

if __name__ == '__main__':
    unittest.main()
