# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from functools import reduce
from itertools import cycle, takewhile
from collections import namedtuple, OrderedDict
from datetime import datetime

Despacho = namedtuple('Despacho', ['id', 'turn', 'vehicle', 'route', 'time'])
Time = namedtuple('Time', ['hour', 'min'])

class EnturnTest(unittest.TestCase):

    def enturnar(self, idx, vehicle, despachos, secuencia_rutas):
        ndespachos = []
        a_enturnar = despachos[idx:]
        timeroute = self._agrupar_por_timeroute(a_enturnar)
        crutas = cycle(secuencia_rutas)

        for time, rutas in timeroute.items():
            ruta = next(crutas)
            for ruta_grupo, despacho in rutas.items():
                if ruta_grupo == ruta:
                    ndespachos.append(despacho._replace(vehicle=vehicle))
                else:
                    ndespachos.append(despacho)

        return ndespachos

    def _agrupar_por_timeroute(self, despachos):
        ordenados = sorted(despachos, key=lambda d: d.time.hour)
        grupos = OrderedDict()
        for despacho in ordenados:
            grupos.setdefault(despacho.time, dict())
            grupos[despacho.time][despacho.route] = despacho
        return grupos

    def test_enturnar(self):
        despachos = [
            Despacho(37, 23, 'B445', 315, Time(6, 28)),
            Despacho(45, 23, 'B401', 316, Time(6, 28)),
            Despacho(52, 23, 'B325', 316, Time(8, 28)),
            Despacho(13, 23, 'B401', 315, Time(8, 28)),
            Despacho(23, 23, '', 315, Time(10, 28)),
            Despacho(73, 23, 'B401', 316, Time(10, 28))
        ]
        esperados_despachos = [
            Despacho(37, 23, 'B142', 315, Time(6, 28)),
            Despacho(45, 23, 'B401', 316, Time(6, 28)),
            Despacho(52, 23, 'B142', 316, Time(8, 28)),
            Despacho(13, 23, 'B401', 315, Time(8, 28)),
            Despacho(23, 23, 'B142', 315, Time(10, 28)),
            Despacho(73, 23, 'B401', 316, Time(10, 28))
        ]

        despachos_enturnados = self.enturnar(0, 'B142', despachos, [315, 316])
        self.assertEqual(despachos_enturnados, esperados_despachos)
                                             
if __name__ == '__main__':
    unittest.main()
