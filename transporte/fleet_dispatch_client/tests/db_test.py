# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import sys
sys.path.append('../')
import unittest
import uuid
import shutil
import time
import os
import glob

import kivy
kivy.require('1.10.1')

import db as _db

class DBProteusTest(unittest.TestCase):

    @classmethod
    def setUp(self):
        shutil.copyfile("test.db", "test.sqlite")
        [os.remove(f) for f in glob.glob("*.store")]

    @classmethod
    def tearDown(self):
        pass
        
        
    def test_res_user(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)
        
        uid = db.create('res.user', None, {
            'login': 'test1',
            'password': 'localhost'
        })
        user = db.get('res.user', uid)
        self.assertEqual(user['login'], 'test1')

        db.save('res.user', uid, {
            'login': 'test2'
        })
        user = db.get('res.user', uid)
        synchronizer.push('res.user')
        self.assertEqual(user['login'], 'test2')

        #se fuerza sincronizacion desde el servidor
        synchronizer.pull('res.user', ['login'])


    def test_db_search(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)

        fixtures = [
            {
                'model': 'res.user',
                'login': 'prueba',
                'group': 'test',
            },
            {
                'model': 'res.user',
                'login': 'prueba2',
                'group': 'test',
            },
            {
                'model': 'res.user',
                'login': 'rueba',
                'group': 'test1'
            }
        ]
        for fixture in fixtures:
            model = fixture.pop('model')
            db.create(model, None, fixture)
            
        records = db.search('res.user', [('group', 'in', ['test'])])
        self.assertEqual(len(records), 2)
        records = db.search('res.user', [('group', 'in', ['test1'])])
        self.assertEqual(len(records), 1)
        self.assertEqual(records[0]['login'], 'rueba')

        records = db.search('res.user', [('group', '=', 'test')])
        self.assertEqual(len(records), 2)


    def test_db_pull_relations(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)

        synchronizer.pull('res.user', ['login'], {'menu': ['id', 'name']})
        users = db.search('res.user', [])
        self.assertEqual(users[0]['menu']['name'], 'Menu')

    def test_db_virtual(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)
        
        uid = db.create('res.user', None, {
            'login': 'test1',
            'password': 'localhost'
        }, fkey=False)
        self.assertEqual(len(list(adapter.records_for_sync('res.user'))), 0)

    def test_persisten_fifo_queue(self):
        queue = _db.PersistentFIFOQueue('test.store')
        queue.put('a')
        queue.put('b')
        self.assertEqual(queue.get(), 'a')
        self.assertEqual(queue.get(), 'b')

    def test_db_search_deep(self):
        adapter = _db.DirtyAdapter()
        synchronizer = _db.DirtyProteusManualSynchronizer(adapter, 'test', config_file='trytond_test.ini')
        db = _db.DB(adapter)

        synchronizer.pull('ir.model.access', ['id', 'perm_read'], {'model': ['id', 'name', 'model']})
        access = db.search('ir.model.access', [('model.model', '=', 'public_transport.authority')])
        model = list(set([r['model']['model'] for r in access]))[0]
        self.assertEqual(model, 'public_transport.authority')

    def test_db_search_order(self):
        adapter = _db.DirtyAdapter()
        db = _db.DB(adapter)

        db.create('demo', None, {
            'id': 1
        })
        db.create('demo', None, {
            'id': 2
        })

        ids = [r['id'] for r in db.search('demo', [])]
        self.assertEqual(ids, [1,2])
        ids = [r['id'] for r in db.search('demo', [], order=[('id', 'DESC')])]
        self.assertEqual(ids, [2,1])
        
if __name__ == '__main__':
    unittest.main()
    
