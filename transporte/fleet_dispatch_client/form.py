# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

from kivy.properties import ObjectProperty, StringProperty, NumericProperty, DictProperty, BooleanProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.app import App
from kivy.factory import Factory

import functools
from datetime import date, datetime, timedelta
import utils
import copy

import ui
import i18n

# tope maximo en caso de no obtener desde servidor
# TODO pasar como parametro de configuracion
_MAX_PASSANGERS_FOR_LAP =  150

_GLOBAL_LOCK_NONBLOCK = False
def transaction_nonblock(func):
    def wrapper(*args, **kwargs):
        global _GLOBAL_LOCK_NONBLOCK

        if _GLOBAL_LOCK_NONBLOCK:
            return None
        _GLOBAL_LOCK_NONBLOCK = True
        try:
            result = func(*args, **kwargs)
        finally:
            _GLOBAL_LOCK_NONBLOCK = False
        return result
    return wrapper

class FormError(Exception):

    def __init__(self, message):
        super(FormError, self).__init__()
        self.message = message
        
class FormPopulate:
    def populate(self, **kwargs):
        pass

class DispatchProperties:
    "Propiedades Kivy generales para formularios de despacho"
    
    pkey = StringProperty()
    vehicle = StringProperty()
    state = StringProperty()
    init_time = StringProperty()
    init_counter = StringProperty()
    end_time = StringProperty()
    end_counter = StringProperty()

    dispatched_time = StringProperty()
    canceled_time = StringProperty()
    done_time = StringProperty()

    def set_defaults(self):
        app = App.get_running_app()
        if hasattr(self, '_record'):
            self.vehicle = str((self._record['vehicle'] or {}).get('rec_name'))
            self.init_time = str(app.astimezone(self._record['init_time']))
            self.init_counter = str(self._record['init_counter'].get('counter', None))
            self.end_time = str(app.astimezone(self._record['end_time']))
            self.end_counter = str((self._record['end_counter'] or {}).get('counter', ""))
            self.dispatched_time = str(app.astimezone(self._record.get('dispatched_time')))
            self.canceled_time = str(app.astimezone(self._record.get('canceled_time')))
            self.done_time = str(app.astimezone(self._record.get('done_time')))
            self.state=self._record['state']
        


class FormReadonly(DispatchProperties, FloatLayout):

    def __init__(self, pkey, record, close=None):
        super(FormReadonly, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close



class FormNewDispatch(DispatchProperties, FormPopulate, FloatLayout):
    """
    Formulario para un nuevo despacho.
    """
    general_error = ObjectProperty(None)
    
    driver = ObjectProperty(None)

    vehicler = ObjectProperty(None)
    
    init_timer = ObjectProperty(None)
    
    router = ObjectProperty(None)

    group_router = ObjectProperty(None)

    init_counter = ObjectProperty(None)
    _popup = ObjectProperty(None)
    
    def __init__(self, close=None):
        super(FormNewDispatch, self).__init__()
        self.close = close
        self.vehicler.bind_button(on_release=self.set_counter)

    # TODO: codigo redundante
    def set_counter(self, btn):
        app = App.get_running_app()
        vehicle_id = self.vehicler.selected.copy()
        init_counter_ids = app.db.search('fleet.vehicle.passenger_counter', [('vehicle.id', '=', vehicle_id['id'])],
                                         order=[('time', 'DESC')],limit=1)
        if init_counter_ids:
            init_counter_id = init_counter_ids[0]
            self.init_counter.text = str(init_counter_id['counter'])
        
    def populate(self):
        app = App.get_running_app()
        self.driver.populate()


    @transaction_nonblock
    def do_dispatch(self):
        app = App.get_running_app()
        vehicle_id = self.vehicler.selected
        if not vehicle_id:
            self.general_error.text = i18n._("Debe indicar vehiculo")
            return
        init_counter_ids = app.db.search('fleet.vehicle.passenger_counter', [('vehicle.id', '=', vehicle_id['id'])],
                                         order=[('time', 'DESC')],limit=1)
        if len(init_counter_ids) == 0:
            self.general_error.text = i18n._("No se ubica registradora para el vehiculo")
            return
        init_counter_id = init_counter_ids[0]

        
        init_time = utils.now(app.timezone).replace(hour=self.init_timer.get_hour(),
                                                    minute=self.init_timer.get_minute())


        trip_daily = app.db_helper.find_trip(vehicle_id, utils.today(app.timezone))
        if not trip_daily or (trip_daily and not trip_daily['init_counter']):
            self.general_error.text = i18n._("El vehiculo no ha sido inicializado para el dia en curso")
            return
        
        if not app.db_helper.finished_trip_date(vehicle_id, (utils.today(app.timezone) - timedelta(days=1))):
            self.general_error.text = i18n._("El viaje x ida del dia anterior no ha sido finalizado")
            return

        if trip_daily['end_counter']:
            self.general_error.text = i18n._("Dia de trabajo finalizado para el vehiculo no se permite nuevos despachos")
            return

        driver_id = None
        if self.driver.selected:
            driver_id = self.driver.selected.copy()
            
        if not self.router.selected:
            self.general_error.text = i18n._("Se requiere Sub Ruta")
            return

        if not self.group_router.selected:
            self.general_error.text = i18n._("Se requiere Ruta")
            return

        groute_id = self.group_router.selected.copy()
        route_id = self.router.selected.copy()
        
        dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.id', '=', vehicle_id['id']),
            ('state', '=', 'dispatched')
        ], order=[('end_time', 'DESC')])
        if len(dispatchs) > 0:
            self.general_error.text = i18n._("Tienen despachos pendientes para el vehiculo indicado")
            return
        
        done_dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.id', '=', vehicle_id['id']),
            ('state', '=', 'done')
        ], limit=1, order=[('end_time', 'DESC')])
        if done_dispatchs:
            last_dispatch, = done_dispatchs
            if init_time <= app.astimezone(last_dispatch['end_time']):
                self.general_error.text = i18n._("Hora y Minuto Inválido el último viaje " \
                    "finalizo a las %s") % (app.astimezone(last_dispatch['end_time']).strftime("%Y-%m%-dT%H:%M"))
                return
            else:
                self.general_error.text = ""

        params = {
            'group_route': groute_id,
            'route': route_id,
            'vehicle': vehicle_id,
            'driver': driver_id,
            'init_counter': init_counter_id,
            'end_counter': None,
            'init_time': utils.as_utc(init_time),
            'end_time': None,
            'state': 'dispatched',
            'turn': None,
            'dispatched_time': utils.now(),
        }
        if app.api_user_id:
            params['dispatched_uid'] = app.api_user_id

        pkey = app.db.create('public_transport.dispatch', None, params)
        app.observer.notify('form.dispatch.new', params)
        app.operator_ui.populate()
        app.webhook.dispatch.notify('dispatched', params)
        app.append_gps_log('public_transport.dispatch', pkey,
                           i18n._('Nuevo despacho para %s') % (vehicle_id.get('rec_name', '')))
        if self.close:
            self.close()
            
class FormDispatch(DispatchProperties, FormPopulate, FloatLayout):
    general_error = ObjectProperty(None)
    driver = ObjectProperty(None)
    # BUG: se permite conductor en blanco
    driver_error = ObjectProperty(None)
    init_timer = ObjectProperty(None)
    vehicler = ObjectProperty(None)
    counter = ObjectProperty(None)
    router = ObjectProperty(None)
    _required_route = BooleanProperty(False)

    def __init__(self, pkey, record, close=None):
        super(FormDispatch, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self.close = close
        self._required_route = True
        
        if self._record.get('route', {}).get('id', None):
            self._required_route = False
            self.router.disabled = True
            self.router.auto_select(self._record['route'])
            
        self.vehicler.bind_button(on_release=self.set_counter)

    def set_counter(self, btn):
        app = App.get_running_app()
        vehicle_id = self.vehicler.selected.copy()
        init_counter_ids = app.db.search('fleet.vehicle.passenger_counter', [('vehicle.id', '=', vehicle_id['id'])],
                                         order=[('time', 'DESC')],limit=1)
        if init_counter_ids:
            init_counter_id = init_counter_ids[0]
            self.counter.text = str(init_counter_id['counter'])

    def domain_route(self):
        return [('group.id', '=', self._record['group_route']['id'])]

    def domain_vehicle(self):
        app = App.get_running_app()
        return [('routes.id', '=', self._record['group_route']['id'])]
        
    def populate(self):
        app = App.get_running_app()
        self.vehicler.auto_select(self._record['vehicle'])

    @transaction_nonblock
    def do_dispatch(self):
        app = App.get_running_app()
        self.general_error.text = ''
        if self._required_route:
            if self.router.selected is None:
                self.general_error.text = i18n._('Se requiere ruta')
                return
        if self.driver_error.text == '' and self.general_error.text == '':
            vehicle_id = None
            #self._record['vehicle'] != self.vehicler
            #hubo cambio de vehiculo para ruta
            if self.vehicler.selected:
                vehicle_id = self.vehicler.selected

            if not vehicle_id:
                self.general_error.text = i18n._('Se requiere vehiculo')
                return
            else:
                self.general_error.text = ''
                
            counters = app.db.search('fleet.vehicle.passenger_counter',
                                     [('vehicle.id', '=', vehicle_id['id'])],
                                     order=[('time', 'DESC')], limit=1)

            if not counters:
                self.general_error.text = i18n._('El vehiculo no tiene registradoras')
                return
            else:
                self.general_error.text = ''

            init_counter_id = None
            for counter in counters:
                init_counter_id = counter
                break
            assert init_counter_id != None

            dispatchs = app.db.search('public_transport.dispatch', [
                ('vehicle.id', '=', vehicle_id['id']),
                ('state', '=', 'dispatched')
            ])

            if dispatchs:
                self.general_error.text = i18n._("Tienen despachos pendientes para el vehiculo indicado")
                return
            else:
                self.general_error.text = ''

            driver_id = None
            if self.driver.selected:
                driver_id = self.driver.selected.copy()
            
            init_time = utils.now(app.timezone).replace(hour=self.init_timer.get_hour(),
                                                        minute=self.init_timer.get_minute())


            trip_daily = app.db_helper.find_trip(vehicle_id, utils.today(app.timezone))
            if not trip_daily or (trip_daily and not trip_daily['init_counter']):
                self.general_error.text = i18n._("El vehiculo no ha sido inicializado para el dia en curso")
                return

            if not app.db_helper.finished_trip_date(vehicle_id, (utils.today(app.timezone) - timedelta(days=1))):
                self.general_error.text = i18n._("El viaje x ida del dia anterior no ha sido finalizado")
                return
        
            if trip_daily['end_counter']:
                self.general_error.text = i18n._("Dia de trabajo finalizado para el vehiculo no se permite nuevos despachos")
                return


            # TODO: codigo reduntante
            done_dispatchs = app.db.search('public_transport.dispatch', [
                ('vehicle.id', '=', vehicle_id['id']),
                ('state', '=', 'done')
            ], limit=1, order=[('end_time', 'DESC')])
            if done_dispatchs:
                last_dispatch, = done_dispatchs
                end_time_last_dispatch = app.astimezone(last_dispatch['end_time'])
                if init_time <= end_time_last_dispatch:
                    self.general_error.text = i18n._("Hora %s Debe ser mayor a ultimo despacho %s" %\
                                                     (utils.str_format_datetime(init_time),
                                                         utils.str_format_datetime(end_time_last_dispatch)))
                    return
                else:
                    self.general_error.text = ""

            last_issues = app.db.search('fleet.vehicle.passenger_counter.issue', [
                ('vehicle.id', '=', vehicle_id['id'])
            ], limit=1, order=[('time', 'DESC')])
            params = copy.copy(self._record)
            params.update({
                'state': 'dispatched',
                'init_counter': init_counter_id,
                'init_time': utils.as_utc(init_time),
                'driver': driver_id,
                'vehicle': vehicle_id
            })

            if self._required_route:
                params['route'] = self.router.selected.copy()
            app.db.save('public_transport.dispatch', self._pkey, params)
            app.observer.notify('form.dispatch', params)
            app.operator_ui.populate()
            app.webhook.dispatch.notify('dispatched', params)
            self.close()

class FormFinalizeOrCancel(DispatchProperties, FloatLayout):
    grid_issues = ObjectProperty()

    def __init__(self, pkey, record, close=None):
        super(FormFinalizeOrCancel, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close
        app = App.get_running_app()
        self.grid_issues.bind(minimum_height=self.grid_issues.setter('height'))
        for record in app.db.search('fleet.vehicle.passenger_counter.issue',
                                    [('vehicle.id', '=', record['vehicle']['id'])],
                                    order=[('time', 'DESC')]):

            msg = "# Reg. Inicial %s # Reg.Final %s el %s" %(str(record['init_counter'].get('counter', '')),
                                                             str(record['end_counter'].get('counter', '')),
                                                             str(utils.str_format_datetime(record['time'])))
            lbl = Label(text=msg, size_hint_y=None, height=20)
            self.grid_issues.add_widget(lbl)

    def dismiss_popup(self):
        self._popup.dismiss()

    def open_finalize(self):
        content = FormFinalize(self._pkey, self._record, close=self.dismiss_popup)
        content.set_defaults()
        content.populate()
        self._popup = ui.Popup(title=i18n._("Finalizar"), content=content, size_hint=(0.9, 0.9))
        self._popup.open()
        if self._close:
            self._close()

    def open_cancel(self):
        content = FormCancel(self._pkey, self._record, close=self.dismiss_popup)
        content.set_defaults()
        content.populate()
        self._popup = ui.Popup(title=i18n._("Cancelar"), content=content, size_hint=(0.9, 0.9))
        self._popup.open()
        if self._close:
            self._close()
  

class FormCancel(DispatchProperties, FormPopulate, FloatLayout):
    canceled_cause = ObjectProperty(None)
    canceled_cause_error = ObjectProperty(None)
    anotaciones = ObjectProperty(None)
    
    def __init__(self, pkey, record, close=None):
        super(FormCancel, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close

    @transaction_nonblock
    def confirm(self):
        if self.canceled_cause.text == '':
            self.canceled_cause_error.text = i18n._('Requiere valor')
        else:
            self.canceled_cause_error.text = ''

        if self.canceled_cause_error.text == '':
            app = App.get_running_app()
            params = copy.copy(self._record)
            params.update({
                'state': 'canceled',
                'canceled_cause': self.canceled_cause.text,
                'canceled_time': utils.now()
            })
            if app.api_user_id:
                params['canceled_uid'] = app.api_user_id
            app.db.save('public_transport.dispatch', self._pkey, params)
            if self.anotaciones and self.anotaciones.selected:
                for anotacion in self.anotaciones.selected:
                    app.db.create('public_transport.dispatch.annotation_dispatch', None, {
                        'dispatch': {
                            '_pkey': params['_pkey'],
                            '_model_': params['_model_'],
                        },
                        'annotation': {
                            '_pkey': anotacion['_pkey'],
                            '_model_': anotacion['_model_']
                        },
                    })
            app.operator_ui.populate()
            app.webhook.dispatch.notify('canceled', params)
            app.append_gps_log('public_transport.dispatch', self._record['_pkey'],
                               i18n._('Cancela despacho para %s') % (self._record['vehicle'].get('rec_name', '')))
            if self._close:
                self._close()

class FormFinalize(DispatchProperties, FormPopulate, FloatLayout):
    """
    Formulario para finalizar despacho.
    
    Se implementa la misma logica que `public_transport.dispatch.exec_finalize.form`.
    """

    end_timer = StringProperty()
    end_counter = ObjectProperty()
    passengers = ObjectProperty()
    passengers_error = ObjectProperty()
    anotaciones = ObjectProperty(None)

    def __init__(self, pkey, record, close=None):
        super(FormFinalize, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close

        app = App.get_running_app()

        self._end_counter = 0
        self._reuse_init_counter = False

        # TODO: se adiciona como parametro el requerimiento del issue #49
        # mientras tintos
        if app.config.getboolean('Server', 'end_counter_equal_to_init_counter'):
            self._reuse_init_counter = True
            self._end_counter = self._record.get('init_counter', {}).get('counter', 0)
            self.end_counter.text = str(self._end_counter)
        self._passengers = None
        self._end_time = utils.now()
        
        self.end_timer = str(app.astimezone(self._end_time).strftime('%Y-%m-%d'))

    def on_pkey(self, instance, value):
        app = App.get_running_app()
        self._record = copy.copy(app.db.get('public_transport.dispatch', value))

    def on_change_end_counter(self, instance, value):
        if value and self._record:
            counter = int(value)
            self._end_counter = counter
            
            passengers = utils.passengers_counter_diff(self._record['init_counter']['counter'], counter)
            self.passengers.text = str(passengers)
            if passengers >= 0:
                max_passangers_for_lap = int(self._record['route']['max_passangers_for_lap']) or _MAX_PASSANGERS_FOR_LAP
                if passengers > max_passangers_for_lap:
                    self.passengers_error.text = i18n._("La cantidad de pasajeros ({passengers}) supera el limite "\
                    "de la ruta ({limit}): Revisar las registradoras").format(passengers=passengers, limit=max_passangers_for_lap)
                else:
                    self.passengers_error.text = ''
                self._passengers = passengers
            elif passengers < 0:
                self.passengers_error.text = i18n._("La cantidad de pasajeros no puede ser negativa ({passengers})").format(passengers=passengers)
        else:
            self.passengers_error.text = ''

    @transaction_nonblock
    def confirm(self):
        app = App.get_running_app()

        if app.allow_dispatch_finalize_time:
            self._end_time = utils.now(app.timezone).replace(hour=self.adjust_end_timer.get_hour(),
                                                             minute=self.adjust_end_timer.get_minute())

        
        if app.astimezone(self._end_time) < app.astimezone(self._record['init_time']):
            raise FormError(i18n._('La hora %s en curso es menor a la hora inicial de despacho %s') \
                            % (utils.str_format_datetime(app.astimezone(self._end_time)),
                               utils.str_format_datetime(app.astimezone(self._record['init_time']))))

        if self.passengers_error.text == '' and self._end_counter > 0:
            init_counter = self._record.get('init_counter').get('counter', 0)
            
            if self._reuse_init_counter and init_counter == self._end_counter:
                end_counter = self._record['init_counter'].copy()
                end_counter['note'] = i18n._('registradora final = registradora inicial')
            elif self._passengers == 0:
                end_counter = self._record['init_counter'].copy()
                end_counter['note'] = i18n._('registradora final = registradora inicial')
            else:
                data = {
                    'time': self._end_time,
                    'vehicle': self._record['vehicle'],
                    'counter': self._end_counter,
                    'note': i18n._('registradora final de despacho'),
                }
                pkey_end_counter = app.db.create('fleet.vehicle.passenger_counter', None, data)
                end_counter =  {
                    '_pkey': pkey_end_counter,
                    '_model_': 'fleet.vehicle.passenger_counter',
                    'time': data['time'],
                    'vehicle': data['vehicle'],
                    'counter': data['counter'],
                    'note': data['note']
                }

            params = copy.copy(self._record)
            params.update({
                'state': 'done',
                'end_time': self._end_time,
                'end_counter': end_counter,
                '_passangers': self._passengers,
                'done_time': utils.now()
            })
            if app.api_user_id:
                params['done_uid'] = app.api_user_id
            app.db.save('public_transport.dispatch', self._pkey, params)
            if self.anotaciones and self.anotaciones.selected:
                for anotacion in self.anotaciones.selected:
                    app.db.create('public_transport.dispatch.annotation_dispatch', None, {
                        'dispatch': {
                            '_pkey': params['_pkey'],
                            '_model_': params['_model_'],
                        },
                        'annotation': {
                            '_pkey': anotacion['_pkey'],
                            '_model_': anotacion['_model_']
                        },
                    })

            app.operator_ui.populate()
            app.observer.notify('form.dispatch.finalize', params)
            app.webhook.dispatch.notify('finalize', params)
            app.append_gps_log('public_transport.dispatch', self._record['_pkey'],
                               i18n._('Finalizado despacho para %s') % (self._record['vehicle'].get('rec_name', '')))
            if self._close:
                self._close()


class FormNewVehicleRegister(FormPopulate, FloatLayout):
    general_error = ObjectProperty(None)
    noter = ObjectProperty(None)
    vehicler = ObjectProperty(None)
    vehicles = ListProperty()

    def __init__(self, close=None):
        super(FormNewVehicleRegister, self).__init__()
        self._close = close

    def on_change_counter(self, text):
        if text:
            counter = int(text)
        else:
            counter = 0
        self._counter = counter

    @transaction_nonblock
    def do_create(self):
        app = App.get_running_app()

        vehicle_id = self.vehicler.selected
        if not vehicle_id:
            self.general_error.text = i18n._("Debe indicar vehiculo")
            return

        dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.rec_name', '=', vehicle_id['rec_name']),
            ('state', '=', 'dispatched')
        ])

        if len(dispatchs) > 0:
            self.general_error.text = i18n._("No es posible, vehiculo en recorrido")
            return

        if not self._counter > 0:
            self.general_error.text = i18n._("Debe indicar el valor de la contadora")
            return

        if self.noter.text == "":
            self.general_error.text = i18n._("Debe indicar la novedad de la registradora")
            return

        app.db.create('fleet.vehicle.passenger_counter', None, {
            'time': utils.now(),
            'counter': self._counter,
            'note': self.noter.text,
            'vehicle': vehicle_id
        })
        app.operator_ui.vehicle_register_panel.populate()
        if self._close:
            self._close()

class FormFilter(FormPopulate, FloatLayout):
    #DEPRECATED: se implementa en ActionBar

    vehicler = ObjectProperty(None)
    dater = ObjectProperty(None)
    
    def __init__(self, close=None):
        super(FormFilter, self).__init__()
        self._close = close

    def populate(self):
        app = App.get_running_app()
        vehicle = app.filters.get('vehicle', None)
        date_filter = app.filters.get('date', None)
        
        if vehicle:
            self.vehicler.auto_select(vehicle)

        if date_filter:
            self.dater.text = date_filter

    def do_apply(self):
        app = App.get_running_app()
        app.filters['vehicle'] = self.vehicler.selected
        app.filters['date'] = self.dater.text
        app.operator_ui.populate()
        if self._close:
            self._close()

class FormEnturn(DispatchProperties, FormPopulate, FloatLayout):
    error = ObjectProperty()
    turner = ObjectProperty()
    init_timer = ObjectProperty()
    vehicler = ObjectProperty()
    to_vehicler = ObjectProperty()

    def __init__(self, pkey, record, close=None):
        super(FormEnturn, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close

        app = App.get_running_app()
        self.turner.text = str(self._record['turn'])
        self.init_timer.text = utils.str_format_datetime(app.astimezone(record['init_time']))
        if 'vehicle' in record and record['vehicle']:
            self.vehicler.text = record['vehicle'].get('rec_name', '')

    def domain_vehicle(self):
        if self.vehicler.text:
            return [('routes.id', '=', self._record['route']['id'])]

        app = App.get_running_app()
        filter_group_route = app.filters.get('group_route', {})
        
        if filter_group_route:
            return [('routes.id', '=', filter_group_route['id'])]
        return []


    @transaction_nonblock
    def do_enturn(self):
        app = App.get_running_app()

        if not self.to_vehicler.selected:
            self.error.text = i18n._('Se requiere vehiculo')
            return

        if not self._record['route']:
            self.error.text = i18n._('Se requiere ruta en despacho')
            return

        trip_daily = app.db_helper.find_trip(self._record['vehicle'], utils.today(app.timezone))
        if not trip_daily or (trip_daily and not trip_daily['init_counter']):
            self.error.text = i18n._("El vehiculo no ha sido inicializado para el dia en curso")
            return
        
        if not app.db_helper.finished_trip_date(self._record['vehicle'], (utils.today(app.timezone) - timedelta(days=1))):
            self.error.text = i18n._("El viaje x ida del dia anterior no ha sido finalizado")
            return

        if trip_daily['end_counter']:
            self.error.text = i18n._("Dia de trabajo finalizado para el vehiculo no se permite nuevos despachos")
            return

        trip_daily = app.db_helper.find_trip(self.to_vehicler.selected, utils.today(app.timezone))
        if not trip_daily or (trip_daily and not trip_daily['init_counter']):
            self.error.text = i18n._("El vehiculo destino no ha sido inicializado para el dia en curso")
            return
        
        if not app.db_helper.finished_trip_date(self.to_vehicler.selected, (utils.today(app.timezone) - timedelta(days=1))):
            self.error.text = i18n._("El viaje x ida del dia anterior del vehiclo destino no ha sido finalizado")
            return

        if trip_daily['end_counter']:
            self.error.text = i18n._("Dia de trabajo del vehiculo destino finalizado para el vehiculo no se permite nuevos despachos")
            return

        self.error.text = ''

        selected_vehicle = copy.copy(self.to_vehicler.selected)
        try:
            unique_enturn = False
            where = [('group_route.id', '=', self._record['group_route']['id'])]
            if self._record['group_route'].get('unique_turn_for_all_route', False):
                where = []
                unique_enturn = True

            edispatchs = utils.enturn(self._pkey, self._record['turn'], True, where, unique_enturn=unique_enturn)

        except Exception as e:
            raise FormError(str(e))
        
        if edispatchs:
            records = app.db.search('public_transport.dispatch', [
                ('state', '=', 'draft'),
                ('init_time', '>=', edispatchs[0]['init_time']),
                ('vehicle.id', '=', selected_vehicle['id'])
            ])
            for record in records:
                record['vehicle'] = {
                    '_model_': 'fleet.vehicle',
                    '_ondelete_': 'SET NULL',
                    'id': 0,
                }
                record['init_counter'] = {
                    '_model_': 'fleet.vehicle.passenger.counter',
                    '_ondelete_': 'SET NULL',
                    'id': 0
                }
                app.db.save('public_transport.dispatch', record['_pkey'], record)

 
        for dispatch in edispatchs:
            records = app.db.search('public_transport.dispatch', [('_pkey', '=', dispatch['_pkey'])], limit=1)
            if records:
                record, = records
                record['vehicle'] = selected_vehicle
                app.db.save('public_transport.dispatch', record['_pkey'], record)
            
            
        app.operator_ui.populate()
        if self._close:
            self._close()


class FormNewIssuePassengerCounter(FormPopulate, FloatLayout):
    general_error = ObjectProperty()
    vehicle = ObjectProperty()
    init_counter = ObjectProperty()
    end_counter = ObjectProperty()
    last_counter = ObjectProperty()
    last_loops = ObjectProperty()
    passengers = ObjectProperty()
    time = ObjectProperty()
    note = ObjectProperty()
    issue_type = ObjectProperty()

    def __init__(self, close=None):
        super(FormNewIssuePassengerCounter, self).__init__()
        self._close = close
        self._last_init_counter = None

    def _on_change_counter(self, init_counter, end_counter):
        self.passengers.text = str(utils.passengers_counter_diff(init_counter, end_counter))
        
    def on_change_end_counter(self, ins, value):
        init_counter = int(self.last_counter.text or 0)
        end_counter = int(value or 0)
        self._on_change_counter(init_counter, end_counter)

    def on_change_vehicle(self, vehicle_id):
        app = App.get_running_app()
        init_counter_ids = app.db.search('fleet.vehicle.passenger_counter',
                                         [('vehicle.id', '=', vehicle_id['id'])],
                                         order=[('time', 'DESC')],limit=2)
        if init_counter_ids:
            init_counter_id = init_counter_ids[0]
            self._last_init_counter = init_counter_id.copy()
            self.last_counter.text = str(init_counter_id['counter'])
            if len(init_counter_ids) == 2:
                self.last_loops.text = str(utils.passengers_counter_diff(init_counter_ids[1]['counter'],
                                                                         init_counter_ids[0]['counter']))

        else:
            self.last_counter.text = ''


    @transaction_nonblock
    def do_create(self):
        init_counter = int(self.last_counter.text or 0)
        end_counter = int(self.end_counter.text or 0)
        self._on_change_counter(init_counter, end_counter)

        app = App.get_running_app()
        vehicle_id = copy.copy(self.vehicler.selected)
        if not vehicle_id:
            self.general_error.text = i18n._("Debe indicar vehiculo")
            return

        dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.rec_name', '=', vehicle_id['rec_name']),
            ('state', '=', 'dispatched')
        ])

        if len(dispatchs) > 0:
            self.general_error.text = i18n._("No es posible, vehiculo en recorrido")
            return

        if not self._last_init_counter:
            self.general_error.text = i18n._("Se requiere registradora inicial")
            return

        if not self.end_counter.text:
            self.general_error.text = i18n._('Se requiere registradora final')
            return

        issue_type_id = copy.copy(self.issue_type.selected)
        if not issue_type_id:
            self.general_error.text = i18n._("Debe indicar Tipo")
            return

        if self.note.text == '':
            self.general_error.text = i18n._("Indique una observacion")
            return

        trip_daily = app.db_helper.find_trip(vehicle_id, utils.today(app.timezone))
        if trip_daily['end_counter']:
            self.general_error.text = i18n._("Dia de trabajo finalizado para el vehiculo no se permite nuevos despachos")
            return

        #se debe crear ya que arroja
        #'La registradora final debe tener una hora posterior a la registradora inicial, así sea 1 segundo'
        end_counter_time = utils.now()
        data_end_counter = {
            'time': end_counter_time,
            'counter': end_counter,
            'note': self.note.text,
            'vehicle': vehicle_id
        }
        pkey_end_counter = app.db.create('fleet.vehicle.passenger_counter', None, data_end_counter)
        data_end_counter['_pkey'] = pkey_end_counter
        data_end_counter['_model_'] = 'fleet.vehicle.passenger_counter'

        pkey_issue = app.db.create('fleet.vehicle.passenger_counter.issue', None, {
            'time': utils.now(),
            'note': self.note.text,
            'init_counter': self._last_init_counter.copy(),
            'end_counter': data_end_counter.copy(),
            'vehicle': vehicle_id,
            'type': issue_type_id
        })
        app.operator_ui.populate()
        app.append_gps_log('fleet.vehicle.passenger_counter.issue', pkey_issue,
                           i18n._('Nueva novedad [%s] para %s') % (issue_type_id.get('rec_name', ''), vehicle_id.get('rec_name', '')))
        if self._close:
            self._close()


class FormDispatchQuantityForDayAction(FormPopulate, FloatLayout):
    lbl_vehicle = ObjectProperty()
    chk_reg_inicial = ObjectProperty()
    chk_reg_final = ObjectProperty()
    chk_reg_cancelar = ObjectProperty()

    def __init__(self, pkey, record, close=None):
        super(FormDispatchQuantityForDayAction, self).__init__()
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close
        self.lbl_vehicle.text = self._record['vehicle'].get('rec_name', '')
        self.chk_reg_inicial.active = self._record['init_counter'] > 0
        self.chk_reg_final.active = self._record['end_counter'] > 0
        self.chk_reg_cancelar.active = self._record['init_counter'] > 0

        app = App.get_running_app()
        
        filter_date = utils.date_from_isoformat(app.filters.get('date', str(utils.today(tzinfo=app.timezone))))

        if filter_date != utils.today(tzinfo=app.timezone) and (utils.today(tzinfo=app.timezone) - timedelta(days=1)) == filter_date:
            return
        else:
            filter_date = (filter_date - timedelta(days=1))

        try:
            trip, = app.db.search('fleet_dispatch_client_backend.trips_daily',
                                  [('vehicle.id', '=', self._record['vehicle']['id']),
                                   ('date', '=', filter_date)],
                                  order=[('end_counter', 'DESC'), ('date', 'DESC')],
                                  limit=1)
            if not trip['end_counter']:
                raise FormError(i18n._('Viaje sin concluir del dia %s, finalizar primero') % str(utils.yesterday()))
        except ValueError:
            pass

    def dismiss_popup(self):
        self._popup.dismiss()
    
    def open_start(self):
        content = FormDispatchQuantityForDayStart(self._pkey, self._record, close=self.dismiss_popup)
        content.populate()
        self._popup = ui.Popup(title=i18n._("Inicio del Dia"), content=content, size_hint=(1, 1))
        self._popup.open()
        if self._close():
            self._close()

    def open_finish(self):
        content = FormDispatchQuantityForDayFinish(self._pkey, self._record, close=self.dismiss_popup)
        content.populate()
        self._popup = ui.Popup(title=i18n._("Finalizacion del Dia"), content=content, size_hint=(1, 1))
        self._popup.open()
        if self._close():
            self._close()

    def open_cancelar(self):
        content = FormDispatchQuantityForDayCancel(self._pkey, self._record, close=self.dismiss_popup)
        content.populate()
        self._popup = ui.Popup(title=i18n._("Cancelacion del Dia"), content=content, size_hint=(1, 1))
        self._popup.open()
        if self._close():
            self._close()


    # observador mirar app.add_observers()
    @classmethod
    def morboso(cls, tag, params):
        app = App.get_running_app()

        if tag not in ['form.dispatch.finalize']:
            return

        try:
            filter_date = utils.today(tzinfo=app.timezone)
            trip_daily, = app.db.search('fleet_dispatch_client_backend.trips_daily',
                                        [('vehicle.id', '=', params['vehicle']['id']),
                                         ('date', '=', filter_date)],
                                        order=[('init_counter', 'DESC'), ('date', 'DESC')],
                                        limit=1)
            
            drecords = app.db_helper.find_dipatchs_by_vehicle_id_and_date(params['vehicle']['id'], filter_date)
            trip_daily['dispatches'] = drecords
            trip_daily['quantity_dispatches'] = len(drecords)
            trip_daily['_dirty'] = True
            if trip_daily['_pkey'] in app.operator_ui.panel.domain_trips.rel_id:
                lid = app.operator_ui.panel.domain_trips.rel_id[trip_daily['_pkey']]
                finalizedp = trip_daily.get('init_counter', 0) > 0  and trip_daily.get('end_counter', 0) > 0
                if finalizedp:
                    finalized = "Si"
                else:
                    finalized = "No"

                row = {
                    '_header': False,
                    '_pkey': trip_daily['_pkey'],
                    'vehicle': params['vehicle'].get('rec_name', ''),
                    'quantity_dispatches': str(trip_daily.get('quantity_dispatches', 0)),
                    'route': params['group_route'].get('rec_name', ''),
                    'date': str(trip_daily.get('date', '')),
                    'init_counter': str(trip_daily.get('init_counter', 0)),
                    'end_counter': str(trip_daily.get('end_counter', 0)),
                    'passengers': str(utils.passengers_counter_diff(trip_daily.get('init_counter', 0), trip_daily.get('end_counter', 0))),
                    'finalized': finalized
                }

                try:
                    app.operator_ui.panel.domain_trips.data[lid] = row
                except IndexError:
                    app.operator_ui.panel.domain_trips.data[1] = row

            app.db.save('fleet_dispatch_client_backend.trips_daily',
                        trip_daily['_pkey'],
                        trip_daily)
        except ValueError:
            pass


class FormDispatchQuantityForDayStart(FormPopulate, FloatLayout):
    registradora_inicial = ObjectProperty()
    chk_registradora_inicial = ObjectProperty()
    confirm_check = ObjectProperty()
    confirm_check_note = ObjectProperty()
    lbl_error = ObjectProperty()

    def __init__(self, pkey, record, close=None, **kw):
        super(FormDispatchQuantityForDayStart, self).__init__(**kw)
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close
        self.last_init_counter = None
        try:
            app = App.get_running_app()
            last_init_counter, = app.db.search('fleet.vehicle.passenger_counter',
                                               [('vehicle.id', '=', self._record['vehicle']['id'])],
                                               order=[('time', 'DESC')],limit=1)
            self.last_init_counter = last_init_counter.copy()
            self.registradora_inicial.text = str(self.last_init_counter['counter'])
        except ValueError:
            pass

    def do_create(self):
        app = App.get_running_app()
        if self._record['init_counter']:
            raise FormError(i18n._('Ya ha indicado la registradora inicial para este viaje'))

        if self.last_init_counter:
            #same_day = self.last_init_counter['time'].date() == utils.today()
            #if not same_day:
            #    self.lbl_error.text = i18n._('# No hay registradora para el dia encurso, tenga presente que se usara la ultima')
            pass
        else:
            self.lbl_error.text = i18n._('No se ubica ultima registrado, ingrese novedad')
            return

        if not self.chk_registradora_inicial.active and not self.confirm_check.active:
            self.lbl_error.text = i18n._('Debe indicar si verifico o aceptar el # de registradora actual')
            return

        if not self.confirm_check.active and not self.confirm_check_note.selected:
            self.lbl_error.text = i18n._('Escoja motivo de la NO confirmacion fisica de #registradora')
            return

        self._record['init_counter'] = self.last_init_counter['counter']
        self._record['open_confirm_check'] = self.confirm_check.active
        if self.confirm_check_note.selected:
            self._record['open_confirm_check_note'] = self.confirm_check_note.selected.get('name', '')
        self._record['_dirty'] = True

        app.db.save('fleet_dispatch_client_backend.trips_daily',
                    self._pkey,
                    self._record,
                    cb=lambda store, k,v: app.operator_ui.populate_trips())
        if self._close():
            self._close()

class FormDispatchQuantityForDayFinish(FormPopulate, FloatLayout):
    registradora_final = ObjectProperty()
    registradora_inicial = ObjectProperty()
    confirm_check = ObjectProperty()
    confirm_check_note = ObjectProperty()
    npasajeros = ObjectProperty()
    chk_registradora_final = ObjectProperty()
    total_trips = ObjectProperty()
    total_loops = ObjectProperty()
    total_trips_canceled = ObjectProperty()
    lbl_error = ObjectProperty()

    def __init__(self, pkey, record, close=None, **kw):
        super(FormDispatchQuantityForDayFinish, self).__init__(**kw)
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close
        self.last_end_counter = None
        if not self._record['init_counter']:
            raise FormError(i18n._('Primero debe inicializar el viaje'))
        try:
            app = App.get_running_app()
            last_end_counter, = app.db.search('fleet.vehicle.passenger_counter',
                                               [('vehicle.id', '=', self._record['vehicle']['id'])],
                                               order=[('time', 'DESC')],limit=1)
            self.last_end_counter = last_end_counter.copy()
            self.registradora_final.text = str(self.last_end_counter['counter'])
            self.registradora_inicial.text = str(self._record['init_counter'])
            self.total_trips.text = str(self._record['quantity_dispatches'])
            self.total_loops.text = str(utils.passengers_counter_diff(self._record['init_counter'],
                                                                      self.last_end_counter['counter']))
            dispatchs_canceled = app.db.search('public_transport.dispatch', [
                ('vehicle.id', '=', self._record['vehicle']['id']),
                ('state', '=', 'canceled')
            ])
            self.total_trips_canceled.text = str(len(dispatchs_canceled))

            issue_type_affects = app.db.search('fleet.vehicle.passenger_counter.issue.type',
                                               [('affects_passangers', '=', True)])
            name_issue_type_affects = [r['name'] for r in issue_type_affects]
            name_issue_type_affects.extend(app.day_work_omit_issue_type_counter)
            npasajeros = utils.passengers_counter_diff(self._record['init_counter'], self.last_end_counter['counter'])
            filter_date = utils.date_from_isoformat(app.filters.get('date', str(utils.today(tzinfo=app.timezone))))
            start_day = utils.as_utc(datetime.combine(filter_date, datetime.min.time()).replace(tzinfo=app.timezone))
            end_day = utils.as_utc(datetime.combine(filter_date, datetime.max.time()).replace(tzinfo=app.timezone))

            last_issues = app.db.search('fleet.vehicle.passenger_counter.issue', [
                ('vehicle.id', '=', self._record['vehicle']['id']),
                ('init_counter.time', '>=', start_day),
                ('end_counter.time', '<=', end_day),
                ('type.name', 'in', name_issue_type_affects)
            ], order=[('time', 'DESC')])
            for issue in last_issues:
                issue_npasajeros = utils.passengers_counter_diff(issue['init_counter']['counter'],
                                                                 issue['end_counter']['counter'])
                npasajeros -= issue_npasajeros
                
            self.npasajeros.text = str(npasajeros)
        except ValueError:
            pass

    def do_create(self):
        app = App.get_running_app()
        if self._record['end_counter']:
            raise FormError(i18n._('Ya ha indicado la registradora final para este viaje'))

        end_counter = int(self.registradora_final.text or '0')

        vehicle_id = copy.copy(self._record['vehicle'])
        if not vehicle_id:
            self.lbl_error.text = i18n._("No se ubica vehiculo!!!")
            return

        dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.rec_name', '=', vehicle_id['rec_name']),
            ('state', '=', 'dispatched')
        ])

        if len(dispatchs) > 0:
            self.lbl_error.text = i18n._("No es posible, vehiculo en recorrido")
            return


        if not self.chk_registradora_final.active and not self.confirm_check.active:
            self.lbl_error.text = i18n._('Debe indicar si verifico o aceptar el # de registradora actual')
            return

        if not self.confirm_check.active and not self.confirm_check_note.selected:
            self.lbl_error.text = i18n._('Escoja motivo de la NO confirmacion fisica de #registradora')
            return
        
        if not self.last_end_counter:
            self.lbl_error.text = i18n._('No se ubica ultima registradora, favor ingresar novedad')
            return


        self._record['end_counter'] = self.last_end_counter['counter']
        self._record['user_confirm_check'] = self.confirm_check.active
        if self.confirm_check_note.selected:
            self._record['user_confirm_check_note'] = self.confirm_check_note.selected.get('name', '')
        self._record['_dirty'] = True
        app.db.save('fleet_dispatch_client_backend.trips_daily',
                    self._pkey,
                    self._record,
                    cb=lambda store, k, v: app.operator_ui.populate_trips())
        if self._close():
            self._close()


class FormDispatchQuantityForDayCancel(FormPopulate, FloatLayout):
    lbl_vehicle = ObjectProperty()
    reason = ObjectProperty()
    note = ObjectProperty()

    
    def __init__(self, pkey, record, close=None, **kw):
        super(FormDispatchQuantityForDayCancel, self).__init__(**kw)
        self._pkey = pkey
        self._record = copy.copy(record)
        self._close = close
        self.lbl_vehicle.text = self._record['vehicle'].get('rec_name', '')

    def do_cancel(self):
        app = App.get_running_app()
        vehicle_id = copy.copy(self._record['vehicle'])
        if not vehicle_id:
            self.lbl_error.text = i18n._("No se ubica vehiculo!!!")
            return

        dispatchs = app.db.search('public_transport.dispatch', [
            ('vehicle.rec_name', '=', vehicle_id['rec_name']),
            ('state', '=', 'dispatched')
        ])

        if len(dispatchs) > 0:
            self.lbl_error.text = i18n._("No es posible, vehiculo en recorrido")
            return

        if not self.reason.selected:
            self.lbl_error.text = i18n._('Debe indicar el motivo de la cancelacion')
            return

        try:
            last_end_counter, = app.db.search('fleet.vehicle.passenger_counter',
                                              [('vehicle.id', '=', self._record['vehicle']['id'])],
                                              order=[('time', 'DESC')],limit=1)
        except ValueError:
            self.lbl_error.text = i18n._('No se ubica ultima registradora de vehiculo')
            return

        self._record['cancel_note'] = self.note.text
        self._record['cancel_reason'] = self.reason.selected['name']
        self._record['init_counter'] = last_end_counter['counter']
        self._record['end_counter'] = last_end_counter['counter']
        self._record['_dirty'] = True
        app.db.save('fleet_dispatch_client_backend.trips_daily',
                    self._pkey,
                    self._record,
                    cb=lambda s, k, v: app.operator_ui.populate_trips())
        if self._close():
            self._close()

        
