# PROPUESTA CLIENTE DESPACHO

Cliente asincrónico para la gestión de despachos del aplicativo basado en **tryton** de Santra.

Para la actualización de datos se utiliza la estrategia [pull](https://en.wikipedia.org/wiki/Pull_technology), a un servidor que expone una [API](despacho-api.yml), con los datos necesarios para la operación del cliente.

Es un aplicativo resiliente a desconexiones más no es un aplicativo para trabajar completamente fuera de linea.
        
La sincronización es usando como base [Command Pattern](https://en.wikipedia.org/wiki/Command_pattern), donde:
    - se encapsulan las operaciones realizadas en la base de datos local -CRUD-.
    - se notifica al servidor el **log** de cambios.
    - el servidor realiza operaciones indicadas para mantener el registro al día.
    - el servidor notifica a Proveedores externos los eventos necesarios -ejemplo, a sonar evento despacho-.
        
-   **Requerimientos:** 
    - python 3.5 o posterior
    - kivy
    - sqlite
    - python-sql
    - trytond:public_transport (actualizado con despacho)
        
-   **Guías:** 
    - <https://github.com/kivy/kivy/wiki/Working-with-Python-threads-inside-a-Kivy-application>
    - <https://kivy.org/doc/stable/api-kivy.html>
    - <https://transaction.readthedocs.io/en/latest/>
    
## ¿POR QUE UN SERVICIO INTERMEDIO?

Delegar al cliente la operación de conectar a proveedores externos implica que:
  - cambios de la api del proveedor externo, implica actualización de todos los clientes y dispositivos.
  - cambios en el flujo del despacho debe actualizar el cliente y todos los clientes.
  - las credenciales al servidor externos se almacenan en el cliente.
  
Por facilidad es tener un servicio intermedio que realice las operaciones necesarias.

## DESARROLLO

Serían dos componentes en el lado del servidor *fleet_dispatch_api* y un cliente multi-plataforma *fleet_dispatch_client*.

### fleet_dispatch_api

Implementa especificación [despacho-api.yml](despacho-api.yml) como un *user_application* de *trytond*.


### fleet_dispatch_client

Implementación en *kivy==1.10.1*.
