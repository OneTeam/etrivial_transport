# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import db
import collections
import itertools
import traceback
import threading
import time
from datetime import timezone
import base64

import glob

from kivy.logger import Logger

import utils
from protocol import fields_for_model


class DispatchSyncronizer():

    pull_model = {
        'fleet_dispatch_client_backend.trips_daily': fields_for_model('fleet_dispatch_client_backend.trips_daily') + (None, ),
        'public_transport.dispatch': fields_for_model('public_transport.dispatch') + (None,),
        'fleet.vehicle.passenger_counter.issue': fields_for_model('fleet.vehicle.passenger_counter.issue') + (None,),
        'fleet.vehicle.passenger_counter.issue.type': fields_for_model('fleet.vehicle.passenger_counter.issue.type') + (None,),
        'public_transport.dispatch.annotation_dispatch': fields_for_model('public_transport.dispatch.annotation_dispatch') + (None, ),
        'public_transport.dispatch.annotation': fields_for_model('public_transport.dispatch.annotation') + (None, ),
        'fleet_dispatch_client_backend.trips_daily-reason_cancel': fields_for_model('fleet_dispatch_client_backend.trips_daily-reason_cancel') + (None,),
        'fleet_dispatch_client_backend.trips_daily-notes': fields_for_model('fleet_dispatch_client_backend.trips_daily-notes') + (None,),
        'fleet.vehicle': fields_for_model('fleet.vehicle') + (None, ),
        'fleet.vehicle.passenger_counter': fields_for_model('fleet.vehicle.passenger_counter') + (None,),
        'company.employee': fields_for_model('company.employee') + (None,),
        'public_transport.route': fields_for_model('public_transport.route') + (None,),
        'public_transport.group_route': fields_for_model('public_transport.group_route') + (None,),
    }

    push_model = {
        'fleet.vehicle.passenger_counter': fields_for_model('fleet.vehicle.passenger_counter') + (None,),
        'fleet.vehicle.passenger_counter.issue': fields_for_model('fleet.vehicle.passenger_counter.issue') + (None,),
        'public_transport.dispatch': fields_for_model('public_transport.dispatch') + (None,),
        'public_transport.dispatch.annotation_dispatch': fields_for_model('public_transport.dispatch.annotation_dispatch') + (None, ),
        'fleet_dispatch_client_backend.trips_daily': fields_for_model('fleet_dispatch_client_backend.trips_daily') + (None, ),
        'fleet_dispatch_client_backend.gps_log': fields_for_model('fleet_dispatch_client_backend.gps_log') + (None,),
    }

    def __init__(self, _db, synchronizer, klass_url_pull=None):
        """
        :param db: base de datos `DBAdapter`.
        :param synchronizer: sincronizador `ModelSynchronizer`.
        """
        assert isinstance(synchronizer, db.ModelSynchronizer) == True
        self.synchronizer = synchronizer
        self.db = _db
        self.klass_url_pull = klass_url_pull

    def pull(self, cb_progress=None, date=None):
        for model_name, (fields, fields_relation, virtual, find_) in self.pull_model.items():
            if cb_progress:
                cb_progress(model_name)
            self.db.adapter.clear_cache(model_name)
            self.synchronizer.pull(model_name, fields,
                                   fields_relation = fields_relation,
                                   virtual = virtual,
                                   find_ = find_,
                                   date = date,
                                   klass_url_pull=self.klass_url_pull)

    def push(self, cb_progress=None, cb_progress_dispatch=None):
        for model_name, _ in self.push_model.items():
            if cb_progress:
                cb_progress(model_name)
            self.synchronizer.push(model_name)
