# GUIA

Guia de introduccion a desarrollo del cliente Kivy en plataforma de despacho.

El cliente de **despachon** esta construido usando **kivy==1.10.1** y 
la estructura general es la siguiente:

  * *form.py* formularios tanto su vista como su logica.
  * *ui.py* vistas auxiliares para ser usadas en 'main.kv'
  * *operator_logic.py* vistas RecycleLayout para renderizar listados de datos,
  y logica del mismo.
  * *db.py* implementa la logica para el almacenamiento local y sincronizacion remota.
  * *db_helper.py* metodos auxiliares para consultas en base de datos local.
  * *main.py* implementacion programa principal, exporta metodos auxiliares
  como filtros, zona horaria.
  * *utils.py* metodos auxiliares para gestion del tiempo, contadoras, hilos, etc..
  * *webhook.py* implementa observador para enviar notificaciones al momento de un
  cambio de estado del despacho.
  * *protocol.py* define los metodos para serializar/desseralizar la comunicacion con 
  **fleet_dispatch_client_backend**, adicionalmente los modelos y los atributos a sincronizar.
  * *protocolv2.py* nuevo esquema de sincronizacion desde version **0.6.93**, se comporta como
  **protocol.py** la diferencia es que usa **search_read** para reducir tiempos de consultas.
  * *sync.py* determina modelos a pull/push en el momento de sincronizacion.
  * *provision.py* sincroniza perfil, esto implica consulta en servidor remoto y actualizacion
  de modelos locales.


# db.py

el archivo **db.py** contiene la implementacion del modelo de sincronizacion usando **proteus** 
de tryton pero este a sido sustituido por sincronizacion con **fleet_dispatch_client_backend**,
la clase acargo es **BackendSynchronizer**, para la gestion local de los datos se usa 
**DirtyAdapter** el cual por medio de diccionarios usando claves definidas 
-_model_,_fkey,_pkey,_dirty,_virtual- lleva el estado de los registros y permite determinar
que se debe sincronizar con el servidor, el esquema general es:

 - operaciones con **DirtyAdapter** atraves del atributo **db** de **MainApp**,
 **DirtyAdapter** extiende la clase abstracta **DBAdapter**, mirar esta para 
 saber que metodos se usan para **create/search/delete/get**.
 - sincronizacion con **BackendSynchronizer** atravez del atributo **db_synchronizer** tanto
 push como pull, la clase **main.py (PullPushAppLogic)** encadena el push/pull.
 
 
# form.py

Formularios de interaccion del usuario,
los formularios son abiertos desde **operator_logic.py**, cuando se da doble click sobre el registro.

## FormReadonly

Este formulario extiende **DispatchPropertiers** el cual define atributos comunes de todo despacho.
Este formulario presenta en solo lectura la informacion.

## FormNewDispatch

Formulario para creacion de nuevos despachos.

## FormDispatch

Formulario para despacho programados.

## FormFinalizeOrCancel

Formulario auxiliar para determinar abrir formulario **FormFinalize** o **FormCancel**

## FormCancel

Formulario para la cancelacion de un despacho.

## FormFinalize

Formulario para la finalizacion de un despacho.

## FormNewVehicleRegister !desactivado

Formulario para la creacion de registradoras de vehiculos.

## FormEnturn

Formulario para enturnar un despacho.


## FormNewIssuePassengerCounter

Formulario para novedades de registradora.

## FormDispatchQuantityForDayAction

Formulario/wizard para dia de trabajo *trips_daily* se mantiene el nombre **QuantityForDay**.

## FormDispatchQuantityForDayStart

Inicia un dia de trabajo.

## FormDispatchQuantityForDayFinish

Finaliza un dia de trabajo

## FormDispatchQuantityForDayCancel

Cancela un dia de trabajo.
