from datetime import datetime, timedelta

import utils

class DBHelper:
    """
    Auxiliares para consultas en db
    """
    def __init__(self, app):
        self.app = app
    
    def find_dipatchs_by_vehicle_id_and_date(self, vehicle_id, date, state='done'):
        start_day = utils.as_utc(datetime.combine(date, datetime.min.time()).replace(tzinfo=self.app.timezone))
        end_day = utils.as_utc(datetime.combine(date, datetime.max.time()).replace(tzinfo=self.app.timezone))
                
        return self.app.db.search('public_transport.dispatch', [
            ('vehicle.id', '=', vehicle_id),
            ('init_time', '>=', start_day),
            ('init_time', '<=', end_day),
            ('state', '=', state)
        ])

            

    def find_trip(self, vehicle, date):
        try:
            trip_daily, = self.app.db.search('fleet_dispatch_client_backend.trips_daily',
                                             [('vehicle.id', '=', vehicle['id']),
                                              ('date', '=', date)],
                                             order=[('init_counter', 'DESC'), ('date', 'DESC')],
                                             limit=1)
            return trip_daily
        except ValueError:
            return None

    def started_trip_date(self, vehicle, date):
        if not self.app.required_trip:
            return True
        try:
            trip_daily, = self.app.db.search('fleet_dispatch_client_backend.trips_daily',
                                             [('vehicle.id', '=', vehicle['id']),
                                              ('date', '=', date)],
                                             order=[('init_counter', 'DESC'), ('date', 'DESC')],
                                             limit=1)
            if not trip_daily['init_counter']:
                return False
        except ValueError:
            pass
        return True


    def finished_trip_date(self, vehicle, date):
        if not self.app.required_trip:
            return True

        try:
            trip_daily, = self.app.db.search('fleet_dispatch_client_backend.trips_daily',
                                             [('vehicle.id', '=', vehicle['id']),
                                              ('date', '=', date)],
                                             order=[('end_counter', 'DESC'), ('date', 'DESC')],
                                             limit=1)
            if not trip_daily['end_counter']:
                return False
        except ValueError:
            pass

        return True

    def validate_vehicle_work_day(self, record, filter_date=None):
        if not filter_date:
            filter_date = self.app.astimezone(utils.today())

        try:
            trip, = self.app.db.search('fleet_dispatch_client_backend.trips_daily',
                                       [('vehicle.id', '=', record['id']),
                                        ('date', '=', filter_date)],
                                       order=[('end_counter', 'DESC'), ('date', 'DESC')],
                                       limit=1)
            if trip['end_counter']:
                return False
        except ValueError:
            pass
        return True

