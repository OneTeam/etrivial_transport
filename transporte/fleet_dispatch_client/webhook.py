# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import pickle
import threading


from kivy.app import App
from kivy.logger import Logger

import utils
from network.urlrequest import UrlRequest

class WebhookAdapter:
    def success(self, data):
        raise NotImplementedError
    
class NullWebhookAdapter:
    def success(self, data):
        Logger.debug("NullWebhookAdapter: " + str(data))

class Webhook:
    
    def __init__(self, adapter=NullWebhookAdapter()):
        super(Webhook, self).__init__()
        self.dispatch = utils.Observer()
        self.dispatch.add_observer(self, func='handle_dispatch')
        self.adapter = adapter

    def handle_dispatch(self, state, params):
        app = App.get_running_app()
        api_key = app.config.get('Server', 'api_key')
        remote_url = app.config.get('Webhook:Dispatch', state, fallback=None)
        _req_headers = {'Content-Type': 'application/octet-stream', 'Accept': 'application/octet-stream'}
        if app.profile:
            api_key = app.get_profile(app.profile)['api_key']

        if api_key:
            _req_headers['Authorization'] = 'Bearer {key}'.format(key=api_key)

        if remote_url:
            Logger.debug("Sending to " + str(state)+ " url " + str(remote_url) + str(params))
            UrlRequest(remote_url,
                       req_body=pickle.dumps(params),
                       on_success=self.on_success,
                       on_error=self.on_error,
                       on_failure=self.on_failure,
                       req_headers=_req_headers)

    def on_error(self, req, error):
        Logger.debug("Failed on webhook:" + str(error))

    def on_success(self, req, res):
        if len(res) > 0:
            self.adapter.success(pickle.loads(res))
        else:
            self.adapter.success(res)

    def on_failure(self, req, res):
        Logger.debug("Failed on webhook:" + str(res))
        
class WebhookFleetDispatchClientBackendAdapter(WebhookAdapter):
    """
    Adapter Para modulo 'fleet_dispatch_client_backed'.
    """
    
    def success(self, data):
        """
        Se espera diccionario del servidor {'sonar_id': ..., '_pkey': ..., '_model_': ...}
        """
        Logger.debug("WebHookBackend get: " + str(data))
        if 'sonar_id' in data and data['_pkey'] and data['_model_']:
            app = App.get_running_app()
            records = app.db.search(data['_model_'], [('_pkey', '=', data['_pkey'])])
            if len(records) == 1:
                params = records[0]
                params['sonar_id'] = data['sonar_id']
                Logger.debug("WebhookBackend update pkey: " + str(records[0]['_pkey']))
                app.db.save(data['_model_'], data['_pkey'], params)
