# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import functools
import re
import os
import copy
from datetime import datetime

import kivy.metrics
from kivy.uix.popup import Popup
from kivy.properties import StringProperty, ObjectProperty, ListProperty, BooleanProperty, NumericProperty

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.behaviors import CompoundSelectionBehavior
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.textinput import TextInput
from kivy.uix.spinner import Spinner
from kivy.uix.button import Button
from kivy.uix.popup import Popup as KPopup
from kivy.uix.label import Label
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.actionbar import ActionButton, ActionItem
from kivy.uix.checkbox import CheckBox
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window, Keyboard
from kivy.uix.vkeyboard import VKeyboard
from kivy.config import Config
import kivy.metrics as metrics
from kivy.clock import Clock
from kivy.app import App

import utils
import i18n

class TimePicker(BoxLayout):

    def __init__(self, *args, **kwargs):
        super(TimePicker, self).__init__(*args, **kwargs)
        app = App.get_running_app()
        self.now = utils.now(tz=app.timezone)
        self.orientation = 'horizontal'
        self.hour = Spinner(
            text=str(self.now.hour),
            values=list(map(str, range(0, 24)))
        )
        self.hour.bind(text=self.change_hour)

        self.minute = Spinner(
            text=str(self.now.minute),
            values=list(map(str, range(0, 60)))
        )
        self.add_widget(self.hour)
        self.add_widget(self.minute)

    def get_hour(self):
        return int(self.hour.text)
    
    def get_minute(self):
        return int(self.minute.text)

    def change_hour(self, spinner, text):
        self.minute.text = '0'

class IntegerInput(TextInput):

    def __init__(self, *args, **kwargs):
        kwargs['input_type'] = 'number'
        kwargs['input_filter'] = 'int'
        kwargs['multiline'] = False
        kwargs['size_hint_y'] = 1
        super(IntegerInput, self).__init__(*args, **kwargs)
        self._dock = False
        try:
            if Config.get('kivy', 'keyboard_mode') == 'dock':
                self._dock = True
        except:
            pass

        self._vkeyboard = None
        self.kb_current_layout = None
        self.bind(focus=self.on_focus)

    def on_focus(self, ins, val):
        if self._dock:
            if val:
                self._set_keyboard_layout()
            else:
                self._close_keyboard_layout()
            

    def _set_keyboard_layout(self):
        kb = Window.request_keyboard(lambda: True, self)
        if kb.widget:
            self._vkeyboard = kb.widget
            self._vkeyboard.font_size = metrics.sp(12)
            self._vkeyboard.key_margin = [1,1,1,1]
            self._vkeyboard.size_hint_y = None
            self._vkeyboard.height = metrics.sp(100)
            self.kb_current_layout = kb.widget.layout
            self._vkeyboard.layout = 'data/keyboards/numeric.json'
        else:
            self._vkeyboard = kb

    def _close_keyboard_layout(self):
        self.focus = False
        if self._vkeyboard:
            if self.kb_current_layout:
                self._vkeyboard.layout = self.kb_current_layout
            self._vkeyboard = None
        return True

class FloatInput(TextInput):

    def __init__(self, *args, **kwargs):
        kwargs['input_filter'] = 'float'
        super(FloatInput, self).__init__(*args, **kwargs)

class SelectableRecycleBoxLayout(LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''
    
class MessagePopup(GridLayout):
    """Basic popup message with a message and 'ok' button."""

    button_text = StringProperty('OK')
    text = StringProperty()

    def __init__(self, *args, **kw):
        kw['cols'] = 1
        super(MessagePopup, self).__init__(*args, **kw)


    def close(self, *_):
        app = App.get_running_app()
        app.popup.dismiss()

class NormalPopup(Popup):
    """Basic popup widget."""
    pass

class AboutPopup(Popup):
    """Basic popup message with a message and 'ok' button."""

    button_text = StringProperty('OK')

    def close(self, *_):
        app = App.get_running_app()
        app.popup.dismiss()

class MainUI(BoxLayout):
    def __init__(self, pull=None, push=None):
        super(MainUI, self).__init__()
        self.pull = pull
        self.push = push

class MainView(object):
    popup = ObjectProperty(None, allownone=True)
    main_layout = ObjectProperty()

    def build(self):
        "LLamado al inicializar APP"

        self.main_layout = MainUI(pull=self.pull, push=self.push)
        return self.main_layout
    

    def popup_message(self, text, title=i18n._('Notification')):
        app = App.get_running_app()
        content = ui.MessagePopup(text=text)
        self.popup = ui.NormalPopup(title=title, content=content, size_hint=(None, None),
                                 size=(app.popup_x, app.button_scale * 4))
        self.popup.open()


    def about(self):

        title = "Santra Despacho"
        self.popup = AboutPopup(title=title)
        self.popup.open()

    
    def pull(self):
        raise NotImplementedError

    def push(self):
        raise NotImplementedError


class RadioGrid(CompoundSelectionBehavior, GridLayout):
    pass


class SelectModel(Button):
    
    allow_empty = BooleanProperty(True)
    selected = ObjectProperty(None, allownone=True)
    records = ListProperty([])
    auto_populate = BooleanProperty(True)
    grid_cols = NumericProperty(5)
    grid_up_count = NumericProperty(5)

    #tal cual en tryton
    order = ListProperty([])
    rec_name = StringProperty('rec_name')
    name = StringProperty('Model')
    model = StringProperty("")
    domain = []
    _popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        kwargs['text'] = i18n._('Seleccionar')
        super(SelectModel, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self.first_populate())
        self.selected = None
        self.bind(model=lambda ins, val: self.populate())
        self.bind(on_release=self._open)
        self.bind(texture_size=self.setter('size'))
        self.padding_x = 2
        self._binders = []

    def bind_button(self, **kwargs):
        self._binders.append(kwargs)

    def first_populate(self):
        if self.auto_populate:
            self.populate()

    def set_empty_selected(self):
        self.selected = None
        self.text = i18n._('Seleccionar')
        
    def set_domain_from_select_model(self, attr, obj):
        if obj and obj.selected:
            self.domain = [(attr, "=", obj.selected['id'])]
            self.populate()

    def populate(self):
        if not self.model:
            return

        app = App.get_running_app()
        if callable(self.domain):
            filter_ = self.domain()
        else:
            filter_ = self.domain

        self.records = app.db.search(self.model, filter_, order=self.order)

        if self.allow_empty:
            self.records.insert(0, {'rec_name': 'Ningun', 'id': None, '_empty_': True})

    def auto_select(self, record):
        if record is None and self.allow_empty:
            self.selected = None
            self.text = self.records[0]['rec_name']
            return

        if self.rec_name in record and '_model_' in record and record['_model_'] == self.model:
            self.selected = record
            self.text = record[self.rec_name]

    def select_record(self, record_idx, instance):
        self._popup.dismiss()
        record = self.records[record_idx]

        # seleccion en blanco
        if '_empty_' in record:
            self.selected = None
        else:
            self.selected = self.records[record_idx]
            
        self.text = self.records[record_idx][self.rec_name]            

        for binder in self._binders:
            try:
                binder['on_release'](instance)
            except KeyError:
                pass

    def _open(self, evt):
        app = App.get_running_app()
        select = RadioGrid(cols=self.grid_cols,
                           up_count=self.grid_up_count,
                           multiselect=False,
                           scroll_count=1)

        for record in self.records:
            btn = Button(text=str(record[self.rec_name]))
            btn.bind(on_press=functools.partial(app.run_once,
                                                  self.select_record,
                                                  self.records.index(record)))
            select.add_widget(btn)
        self._popup = Popup(title=i18n._(self.name), content=select)
        self._popup.open()
            
class SelectVehicle(SelectModel):
    
    def __init__(self, **kwargs):
        kwargs['name'] = i18n._('Vehicle')
        kwargs['model'] = 'fleet.vehicle'
        kwargs['order'] = [('internal_code', 'ASC')]
        if not self.domain:
            self.domain = self._domain
        return super(SelectVehicle, self).__init__(**kwargs)

    def _domain(self):
        app = App.get_running_app()
        filter_group_route = app.filters.get('group_route', {})
        
        if filter_group_route:
            return [('routes.id', '=', filter_group_route['id'])]
        return []
        
class SelectGroupRoute(SelectModel):

    def domain(self):
        app = App.get_running_app()
        group_routes = app.db.search('device-group_routes', [])
        if group_routes:
            return [('id', 'in', [gr['id'] for gr in group_routes])]
        return []
            
    def __init__(self, **kwargs):
        kwargs['name'] = i18n._('Ruta')
        kwargs['model'] = 'public_transport.group_route'
        return super(SelectGroupRoute, self).__init__(**kwargs)

class SelectRoute(SelectModel):
      
    def __init__(self, **kwargs):
        kwargs['name'] = i18n._('Ruta')
        kwargs['model'] = 'public_transport.route'
        return super(SelectRoute, self).__init__(**kwargs)

class SelectDriver(SelectModel):

    def __init__(self, **kwargs):
        kwargs['name'] = i18n._('Conductor')
        kwargs['model'] = 'company.employee'
        self.domain = [('is_driver', '=', True)]
        return super(SelectDriver, self).__init__(**kwargs)

class SelectMultiple(ScrollView):
    model = StringProperty('')
    rec_name = StringProperty('rec_name')
    
    order = ListProperty([])
    domain = []

    def __init__(self, **kwargs):
        super(SelectMultiple, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self.first_populate())
        self.grid = GridLayout(cols=1, size_hint_y=None)
        self.add_widget(self.grid)
        self.selected = []
        self._binders = []

    def bind_button(self, on_press=None):
        if on_press:
            self._binders.append(on_press)
            
    def first_populate(self):
        self.populate()

    def populate(self):
        if not self.model:
            return

        app = App.get_running_app()
        if callable(self.domain):
            filter_ = self.domain()
        else:
            filter_ = self.domain
        self.records = app.db.search(self.model, filter_, order=self.order)

        grid_height = 0
        for record in self.records:
            tg = ToggleButton(text=record[self.rec_name],
                              size_hint_y=None,
                              font_size='15sp',
                              height=kivy.metrics.sp(15))
            grid_height += tg.font_size
            tg.bind(state=functools.partial(self._on_state, record))
            self.grid.add_widget(tg)
        self.grid.height = grid_height

    def _on_state(self, record, instance, val):
        for bind in self._binders:
            bind(instance, record=record)
            
        if val == 'down':
            self.selected.append(record)
        else:
            self.selected.remove(record)

class PopupConfirm(Popup):

    def __init__(self, message, cbsuccess):
        self.cbsuccess = cbsuccess
        
        vbox = BoxLayout(orientation="vertical")
        vbox.add_widget(Label(text=message))
        hbox = BoxLayout(orientation="horizontal", size_hint=(None, None))
        btn_success = Button(text=i18n._("Aceptar"))
        btn_success.bind(on_release=self.success)
        hbox.add_widget(btn_success)
        vbox.add_widget(hbox)

        super(PopupConfirm, self).__init__(title=i18n._("Confirmación Requerida"),
                                           content=vbox)
        
    def success(self, btn):
        if self.cbsuccess:
            self.cbsuccess()
        self.dismiss()

class Popup(KPopup):

    def on_parent(self, popup, window):
        gridlayout, = popup.children

        if len(gridlayout.children) != 3:
            return

        title = gridlayout.children[-1]
        if not isinstance(title, Label):
            return

        gridlayout.remove_widget(title)
        box = BoxLayout(orientation="horizontal",
                        size_hint=(1, None), height=metrics.dp(10))
        box.add_widget(Label(text=title.text))
        box.add_widget(Button(text="  ",
                              border=[1,1,1,1],
                              background_color=[1,0,0,1],
                              size_hint=(None, None),
                              on_release=self.dismiss))
        gridlayout.add_widget(box, index=len(gridlayout.children))

class PopupNull:

    def open(self, *args):
        pass

    def dismiss(self, *args, **kwargs):
        pass
