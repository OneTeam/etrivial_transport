# Copyright © 2019 Jovany Leandro G.C <bit4bit@riseup.net>
#
# This file is part of fleet_dispatch_client.
#
# fleet_dispatch_client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fleet_dispatch_client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fleet_dispatch_client.  If not, see <https://www.gnu.org/licenses/>.

import functools

from kivy.network.urlrequest import UrlRequest
from kivy.app import App

import form
import protocol
import i18n

__all__ = ['provision']

def _on_success(db, request, result):
    provision = protocol.unserialize(result, decoder=protocol.decode_binary)
    db.clear('device-profiles')
    db.clear('device-group_routes')
    for profile in provision.get('profiles', []):
        db.create('device-profiles', profile['_fkey'], profile, fkey=False)
    for dgroup_routes in provision.get('group_routes', []):
        db.create('device-group_routes', dgroup_routes['_fkey'], dgroup_routes, fkey=False)
    db.sync('device-profiles')
    db.sync('device-group_routes')
    raise form.FormError(i18n._('Perfiles sincronizados. Porfavor active la autenticacion y reinicie'))

def _on_failure(request, result):
    if request.resp_status == 401:
        raise form.FormError(i18n._('Token invalido'))
    raise form.FormError(str(result))

def provision(db, machine, base_url, api_key, token):
    headers = {'Content-Type': 'application/octet-stream', 'Accept': 'application/octet-stream'}
    headers['Authorization'] = 'Bearer {key}'.format(key=api_key)
    
    req = UrlRequest(str(protocol.UrlProvision(base_url, machine, token)),
                     on_success = functools.partial(_on_success, db),
                     on_failure = _on_failure,
                     on_error = _on_failure,
                     decode=False,
                     debug=True,
                     req_headers=headers)
    res = req.wait()

                         
