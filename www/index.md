# ETrivial

ERP Trivial, para la gestión del transporte público colectivo de pasajeros y otras actividades. 

## Carácteristicas actuales

 * Cuenta con un conjunto de  [módulos](/dir?ci=1f28502bc4f4487b&name=trytond-modules) para [Tryton5](/http://www.tryton.org)
 * Un [cliente de despacho](/dir?name=trytond-modules/fleet_dispatch_client) desarrollado en [Kivy](http://www.kivy.org) [1.10.](https://github.com/kivy/kivy/releases/tag/1.10.1) compatible con Android [descargar apk](/dir?ci=c941b88340673ac7&name=fleet_dispatch_client/apk) y para PC (en cualquier sistema que pueda correr python 3)
 * Gestión de [sincronización](/dir?ci=97920a0f36b737dd&name=tools/heilhikvision) con camaras de vigilancia (Hikvision), desarrollado en Python y Bash
 * Un webservis para el servicio de Gammu (servicio de envío de SMS) escrito en Python

## Carácterísticas en desarrollo

 * Consumo de datos webservis metro de Medellín
 * Consumo de datos webservis GTPC (AMVA y Secretaría de movilidad de medellín)

## Carácterísticas en cola de desarrollo

 * Gestor para el cumplimiento de la resolución 0479 de 2010 del ministerio de transporte
 * Facturación electrónica para Colombia
 * Nómina
 * Predespachos y despacho remoto

## Módulos y cobertura funcional

### Módulos genéricos para Tryton 5

  * [Gestión de Terceros para Colombia](/dir?ci=d9681750ddd6851b&name=trytond-modules/party_co)
	* Manejo de estandar de documentos de identificación para Colombia:
	  * Primer nombre, segundo nombre, primer apellido y segundo apellido
	  * Registro Civil
	  * Tarjeta de Identidad
	  * Cedula de Ciudadania
      * Tarjeta de Extranjeria
	  * Cedula de Extranjeria
	  * NIT
	  * Pasaporte
	  * Tipo de Documento Extrangero
	  * Sin identificacion en del exterior
    * Tipos de regimenes contributivos:
      * Simplificado
      * Común
      * Gran contribuyente
      * Entidad estatal
	  * Entidad estatal
	  * Domiciliado en el extranjero
    * Tipo de persona:
	  * Natural
      * Jurídica
    * Digito de verificación colombiano
  * [Festivos para Colombia](/dir?ci=6a57138eb0284b51&name=trytond-modules/holidays_co)

### Módulos para el transporte público de pasajeros

  * [Administración de Transporte público de pasajeros](/dir?ci=80f394b7f8dad2a4&name=trytond-modules/public_transport)
    * Manejo de autoridades de transporte
    * Grupos de rutas y subrutas
      * Montaje de trazas de las rutas
      * Tipos de vehículo
      * Capacidad transportadora
    * Programación de despachos:
      * Horarios
      * Duración de viajes
      * Creación e impresión de tablas de programación
    * Depachos:
      * Programados, en recorrido, finalizados y cancelados
	  * Clientes de despacho (para PC o Android)
      * Provisionamiento de dispositivos de despacho y manejo de usuarios
      * Operación de despacho con dispositivos fuera de línea
      * Día de trabajo por vehículo viajes y pasajeros 
      * Despachos desde tablas de programación de viajes
      * Despachos manuales
	  * Manejo de registradoras
	  * Consolidación de despachos por día, rutas por día y despachos por mes.
      * Promedios de la ruta
      * Totaliza pasajeros según sensores de conteo y registradora física
      * Análisis estadistico
    * Monitoreo de rutas:
      * Visualización en mapa del recorrido de un viaje, pasajeros movilizados y traza de la ruta.
	  * Informes de viajes cumplimiento, pasajeros, tiempo, exceso de velocidad etc
    * Conductores
      * Vínculo con módulo de gestión flota y gestión humana
    * Propietarios:
      * Contratos de propietarios
      * Estados de contratos activos, tramite de tarjeta de operación e históricos
      * Vinculo con gestión de flota, gestión humana y conductores
      * Vencimiento de contratos
      

  *  Gestión de Flota

  *  Recursos Humanos
  *  Sincronización proveedor Datos Sonar
  *  Sincronización masiva dispositivos de camara.




## Requerimientos

  * trytond 5
  * postgresql 11
